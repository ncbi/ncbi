# NCBI Project

[![MIT license](https://img.shields.io/badge/license-MIT-e51879.svg)](https://gitlab.coko.foundation/PubSweet/PubSweet/raw/master/LICENSE)
[![mattermost](https://img.shields.io/badge/mattermost_chat-coko%2FPubSweet-blue.svg)](https://mattermost.coko.foundation/coko/channels/PubSweet)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

# Overview

This is the repository for the submission web application for NCBI.

The project is being developed by the [Coko Foundation](https://coko.foundation/) using [PubSweet](https://gitlab.coko.foundation/PubSweet/PubSweet).

# Dependencies

The main requirements for the project are `node` and `yarn`.

If you don't have yarn installed you can make use of NCBI script to install the recommended yarn version locally.

If you opt to use the `Postgres` docker container for the database, you should also have `docker` and `docker-compose` installed.

The current release (`v0.0.1`) has been developed with `node v16.19.1`, `yarn v1.22.4`, `docker 18+` and `docker-compose v1.27+`.

You can try newer versions of these dependencies (or use `npm` instead of `yarn`), but the above setup is the one supported by the development team, as well as the one assumed for these instructions.

# Installation / Start Application

### 1. Cloning the repository

---

First, you'll need to clone the repository:

```sh
git clone https://gitlab.coko.foundation/ncbi/ncbi.git ncbi
```

### 2. Running installation script

---

It is recommended to run the app through docker and not yarn, in that case skip step 2 & 3.

Installation script is located under `/scripts` folder.
Main job of this script is:

- install yarn locally if you don't have it already.
- install all npm package you need for the application
- configure your database (see configuration section)
- start your application fro the first time (see configuration section)

- if you have `yarn` already installed at your system, just make use of scripts at package.json file which will execute the installation script.

```sh
yarn install:ncbi
```

- if you don't have yarn installed. you can first install it locally using the script and then install the app

```sh
$ ./scripts/ncbi --install-yarn
$ source ~/.bashrc
$ yarn install:ncbi
```

or you can just leave it to script take care of installing automatically both application dependencies and yarn if cannot find it.

```sh
$ ./scripts/ncbi --install
$ source ~/.bashrc
```

In case you need to specify certain yarn version to install, you can specify it to configuration. see more below on configuration section

### 3. Start Application

---

Now we are ready to start the application.

```sh
$  yarn start
```

# Application Configuration

In the main folder there is a sample environmental variable file .env.example. By default application will make use of that file, unless finds
an .env file

Below you can see the variables currently exist table explaining and a short explanation.

| Variable              | Default Value                                         |                                                                                                                  Description |
| --------------------- | ----------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------: |
| PUBSWEET_SECRET       | dev_secret                                            |                                                                                 unique key of the pubsweet. Security reasons |
| POSTGRES_USER         | dev                                                   |                           postgres user to be used during installation of postgres either with docker or installation script |
| POSTGRES_PASSWORD     | secretpassword                                        |               postgres password of user to be used during installation of postgres either with docker or installation script |
| POSTGRES_HOST         | localhost                                             |                           postgres host to be used during installation of postgres either with docker or installation script |
| POSTGRES_DB           | ncbi_dev                                              |                       postgres database to be used during installation of postgres either with docker or installation script |
| POSTGRES_PORT         | 5432                                                  |                           postgres port to be used during installation of postgres either with docker or installation script |
| DATABASE_URL          | postgres://dev:secretpassword@localhost:5432/ncbi_dev |                                                                           postgres connection string to be used from the app |
| WAIT_POSTGRES_PORT    | localhost:5432                                        |                                                needed from installation script and app to check the availability of database |
| WAIT_POSTGRES_TIMEOUT | 300                                                   |                                                            how long to wait before throwing error if database is not running |
| http_proxy=           | null                                                  |                                          by default this is null. you can specify this variable in order to be used by yarn. |
| DEV_SERVER_HOST       | localhost                                             |                                       if NODE_ENV is set to `development` specifies in which host the application is running |
| DEV_SERVER_PORT       | 3020                                                  |                                       if NODE_ENV is set to `development` specifies in which port the application is running |
| NODE_ENV              | development                                           | specifies the running mode of the app. other options are: `production`, `test` needs especially for webpack to build the app |
| ADMIN_USERNAME        | admin                                                 |                                                                              the first username of the app in order to login |
| ADMIN_EMAIL           | admin@example.com                                     |                                                                                                  the email fo the first user |
| ADMIN_PASSWORD        | password                                              |                                                                              the first password of the app in order to login |
| YARN_VERSION          | 1.22.4                                                |                            the version of yarn to be installed by installation script in order yarn is missing of the system |
| WORD_TARGET_SERVER    | dev   (for local developlement)                          |   |
| NCBI_DOMAIN_URL       | eg:   http://localhost:3000                        |   this value is for local dev when we want to mock the process of domain creation                |
| USE_RANDOM_STRING_FOR_DOMAIN_NAMES | true/false                               | For local development this is set to true, so that we grenerate random domain names that will be deleted in e future. |
| KAFKA_CONSUMER_GROUP   |  Add some random characters as the value of it   | This value has to be unique for each deplomyment
| DEBUG                 | True/false                                       |                           |
| KAFKA_URL               | Url value                                      |                           |
| DISABLE_FTP           | Set to `true` for local dev                        |                           |
| ENABLE_DEVELOPMENT_LOGIN | Set to `true` for local developement          |                           |

Other variables, which can be set as for your preferences or you leave the default values from docker-compose.yml

`CLIENT_PROTOCOL=http`  
`CLIENT_HOST=0.0.0.0`  
`CLIENT_PORT=3020`  
`SERVER_PROTOCOL=http`  
`SERVER_HOST=localhost`  
`SERVER_PORT=3000`  
`SERVER_SERVE_CLIENT=0`  

Variables for the FTP connections:
`NCBI_FTP_HOST=`  
`NCBI_FTP_USER=`  
`NCBI_FTP_PASSWORD=`  

`APEX_FTP_HOST=`  
`APEX_FTP_USER=`  
`APEX_FTP_PASSWORD=`  


# Local development with Docker

You can run and develop application using just docker and vsCode (Remote Containers). This is very helpful cause enables us
to run the app without think different version of yarn, node, Postgres and of course eliminates the possible bugs because of
different working station environment between developers. Also Windows users are able to run the app quickly just by having
docker installed at their systems.

Docker compose configuration files are available for now is only for development.

Run:

```
docker-compose build (for the first time running the app)
docker-compose up
```

the above command will build the image where the dependencies of the app are installed( npm install ) and then run the app which includes running migration scripts, seeds scripts, bring PubSweet server up and running.

if you need to reinstall the dependencies you can re-build the image by `docker-compose build --no-cache`.

**Attention:**

1. Since application's dependencies are installed inside the container at your folder you can see an empty node_modules folder
2. Any changing happens to your local files will take affect and inside the container since we share volumes between docker container and host.
3. Since node_modules isn't available from the host . you are going to miss helpful executable features like eslint or prettier which are placed inside node_modules. One workaround is to access them through Remote Container plugins of your favorite editor. For
   VsCode you can use Remote Containers plugin (https://code.visualstudio.com/docs/remote/containers)

# FAQ

In case you have problems with installation , try restarting and rebuilding application with docker <br />
Try following commands : <br />

```

docker ps # then kill the docker process that is running for the ncbi

docker-compose down
sudo rm -rf data

docker-compose build --no-cache
docker-compose up

```
