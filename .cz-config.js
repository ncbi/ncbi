const { commitizen } = require('@coko/lint')

commitizen.scopes = [
  'app',
  'webpack',
  'api',
  'models',
  'client',
  'server',
  'config',
  'ui',
  '*',
]

module.exports = commitizen
