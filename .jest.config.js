module.exports = {
  collectCoverage: false,
  collectCoverageFrom: [
    '**/*.{js,jsx}',
    '!**/*test.{js,jsx}',
    '!**/test/**',
    '!**/node_modules/**',
    '!**/config/**',
    '!**/coverage/**',
  ],
  coverageDirectory: '<rootDir>/coverage',
  projects: [
    // {
    //   displayName: 'server model',
    //   rootDir: '<rootDir>/../data-model/',
    //   testEnvironment: 'node',
    //   testRegex: 'src/*/.+test.jsx?$',
    // },
    {
      displayName: 'api',
      rootDir: '<rootDir>/server/api/graphql',
      testEnvironment: 'node',
      testRegex: '/*/.+test.jsx?$',
    },
    {
      displayName: 'services',
      rootDir: '<rootDir>/server/services',
      testEnvironment: 'node',
      testRegex: '/*/.+test.js$',
      setupFiles: ['../../test/jestSetup.js'],
    },
  ],
}
