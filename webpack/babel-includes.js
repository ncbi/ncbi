const path = require('path')

module.exports = [
  // include app folder
  path.join(__dirname, '..', 'app'),
  path.join(__dirname, '..', 'ui'),
  // include pubsweet packages which are published untranspiled
  /pubsweet-[^/\\]+\/(?!node_modules)/,
  /@pubsweet\/[^/\\]+\/(?!node_modules)/,
]
