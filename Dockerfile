# IMAGE FOR BUILDING
FROM node:18.16.0-bullseye-slim

RUN apt-get update && apt-get install -y \
    make \
    g++ \
    python3 \
    git

WORKDIR /home/node/app

COPY package.json .
COPY yarn.lock .

# Install production node modules for server use
RUN yarn install --frozen-lockfile --production=true
# Copy to another folder for later use
RUN mv node_modules production_node_modules

# Install development node modules for building webpack bundle
RUN yarn install --frozen-lockfile --production=false

COPY . .

ARG server_protocol
ARG server_public_url
ARG enable_development_login

ENV NODE_ENV=production
ENV SERVER_SERVE_CLIENT=true
ENV SERVER_PROTOCOL=$server_protocol
ENV SERVER_PUBLIC_URL=$server_public_url
ENV ENABLE_DEVELOPMENT_LOGIN=$enable_development_login

RUN NODE_OPTIONS=--openssl-legacy-provider yarn pubsweet build

######################################################################
######################################################################

# IMAGE FOR RUNNING
FROM node:18.16.0-buster-slim as server
LABEL gov.nih.nlm.ncbi.devops.cistack=nodejs 

RUN apt-get update && apt-get install -y p7zip-full

WORKDIR /home/node/app

RUN chown -R node:node .
USER node

RUN chmod 777 /home/node/app # please fix this!

COPY --chown=node:node ./config ./config
COPY --chown=node:node ./scripts ./scripts
COPY --chown=node:node ./server ./server
COPY --chown=node:node ./startServer.js .

COPY --from=0 /home/node/app/_build/assets ./_build/assets
COPY --from=0 /home/node/app/production_node_modules ./node_modules

ENTRYPOINT ["sh", "./scripts/setupProdServer.sh"]

CMD ["node", "./startServer.js"]
