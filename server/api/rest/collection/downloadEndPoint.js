const { Collection } = require('@pubsweet/models')
const { XmlFactory } = require('../../../services')

module.exports = app => {
  app.get('/downloadCollectionXml', async (req, res, next) => {
    const { id } = req.query
    const collection = await Collection.query().findOne({ id })

    if (collection) {
      const content = await XmlFactory.createCollectionXml(collection)

      res.setHeader(
        'Content-disposition',
        `attachment; filename=${encodeURIComponent(
          `${collection.domain}.xml`,
        )}`,
      )

      res.setHeader('Content-type', 'text/xml')
      res.end(content)
    }
  })
}
