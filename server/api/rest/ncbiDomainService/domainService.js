const createPublisher = require('./mockResponses/createPublisher')
const updatePublisher = require('./mockResponses/updatePublisher')
const createBook = require('./mockResponses/createBook')
const updateBook = require('./mockResponses/updateBook')
const getBook = require('./mockResponses/getBook')

module.exports = app => {
  // Organization Domain
  app.post('/books/domain-service/publishers/', async (req, res) => {
    res.setHeader('Content-type', 'application/json')
    res.status(201)

    res.send(createPublisher)
  })

  app.patch('/books/domain-service/publishers/:id', async (req, res) => {
    res.setHeader('Content-type', 'application/json')
    res.status(204)

    res.send(updatePublisher)
  })

  // Book Domain
  app.get('/books/domain-service/domains/:id', async (req, res) => {
    res.setHeader('Content-type', 'application/json')
    res.status(200)

    res.send(getBook)
  })

  app.post('/books/domain-service/domains/', async (req, res) => {
    res.setHeader('Content-type', 'application/json')
    res.status(201)

    res.send(createBook)
  })

  app.patch('/books/domain-service/domains/:id', async (req, res) => {
    res.setHeader('Content-type', 'application/json')
    res.status(204)

    res.send(updateBook)
  })
}
