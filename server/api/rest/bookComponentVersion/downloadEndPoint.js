/* eslint-disable no-await-in-loop */
const { useTransaction } = require('@coko/server')
const archiver = require('archiver')
const lodash = require('lodash')
const moment = require('moment')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const FileVersion = require('../../../models/fileVersion/fileVersion')
const File = require('../../../models/file/file')
const FileService = require('../../../services/file/fileService')
const ContentProccess = require('../../../services/ContentProccess')

module.exports = app => {
  app.get('/downloadFileVersion', async (req, res, next) => {
    try {
      const { id } = req.query

      if (!id) throw new Error('Provide the id of File to download')

      const { name, oid, mimeType } = await File.query()
        .findOne({ id })
        .throwIfNotFound()

      res.setHeader(
        'Content-disposition',
        `attachment; filename=${encodeURIComponent(name)}`,
      )

      res.setHeader('Content-type', mimeType)

      await useTransaction(async transaction => {
        return new Promise(resolve => {
          FileService.read({
            stream: res,
            oid,
            transaction,
            end: resolve,
          })
        })
      })

      res.end()
    } catch (e) {
      logger.info(e)
      next(e)
    }
  })

  app.post('/downloadFileVersionsAsZip', async (req, res) => {
    useTransaction(async trx => {
      const { bookComponentId, allFiles } = req.query

      const { fileIds } = req.body

      const contentProccess = await ContentProccess.findBestContent(
        bookComponentId,
      )

      const { model: Model, object } = contentProccess

      if (!object) return

      let files = []

      // Find the file objects via the ids passed into the HTTP GET
      // This path includes fileIds that might be old versions (non-latest) of Files
      if (fileIds && fileIds.length > 0 && allFiles === 'false')
        files = await File.query().findByIds(fileIds)
      // else set up the files to be all the linked Files via the Book Component File
      else if (allFiles === 'true') {
        // All the latest versions of the Book Component Files.
        // As written this SQL will never return older versions of Files.
        const bookComponentFiles = await Model.latestComponentFiles(object)

        files = lodash.map(
          bookComponentFiles,
          item => lodash.pick(item, 'file').file,
        )
      }

      const archive = archiver('zip', { zlib: { level: 9 } })

      const tsStr = moment().format('YYYY_MM_DD-hh_mm_ss')

      const fnAlias = object.alias

      const fileNameStr = `bcms-${fnAlias}-version-${object.versionName}-${tsStr}.zip`

      res.setHeader(
        'Content-disposition',
        `attachment; filename=${encodeURIComponent(fileNameStr)}`,
      )

      res.setHeader('Content-type', 'application/zip')

      archive.pipe(res)

      for (let index = 0; index < files.length; index += 1) {
        // Need to get versionName string from bookComponentFile
        // We can't assume the path of allFiles === true BookComponetFiles objects include old versions
        // eslint-disable-next-line no-await-in-loop
        const associatedBookComponentFile = await FileVersion.query().findOne({
          fileId: files[index].id,
        })

        // We do a custom re-naming from category to special name for some categories, for the ZIP folder name
        let folderName = files[index].category

        switch (files[index].category) {
          case 'pdf':
            folderName = 'display-pdfs'
            break
          case 'image':
            folderName = 'images'
            break
          case 'cover':
            folderName = 'metadata'
            break
          case 'comment':
            folderName = 'review'
            break
          case 'supplement':
            folderName = 'suppl'
            break
          default:
            break
        }

        const readStream = await FileService.read({
          oid: files[index].oid,
          transaction: trx,
          id: files[index].id,
        })

        archive.append(readStream, {
          name: `${folderName}/version-${associatedBookComponentFile.versionName}/${files[index].name}`,
        })
      }

      await archive.finalize()
    })
  })
}
