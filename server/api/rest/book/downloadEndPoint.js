const { XmlFactory } = require('../../../services')

module.exports = app => {
  app.get('/downloadTocXml', async (req, res, next) => {
    const { id } = req.query

    const content = await XmlFactory.createTocXml(id)

    res.setHeader(
      'Content-disposition',
      `attachment; filename=${encodeURIComponent('TOC.xml')}`,
    )

    res.setHeader('Content-type', 'text/xml')
    res.end(content)
  })
}
