/* eslint-disable no-return-await */
const { has, isEmpty } = require('lodash')

const {
  getPubsub,
  asyncIterators,
} = require('pubsweet-server/src/graphql/pubsub')

const { BookComponent } = require('@pubsweet/models')

const FileService = require('../../services/file/fileService')

const { ON_UPLOAD_PROGRESS } = asyncIterators

const validateUpload = async contentProccess => {
  const bc = contentProccess.object
  const Model = contentProccess.model
  const [sourceFile] = await Model.latestComponentFiles(bc, 'source')

  const [convertedFile] = await Model.latestComponentFiles(bc, 'converted')

  if (
    (bc.workflow === 'xml' || bc.workflow === 'word') &&
    bc.status === 'converting'
  ) {
    throw new Error('You cannot upload file during Converting')
  }

  if (bc.status === 'loading-preview' || bc.status === 'converting') {
    throw new Error(
      'You cannot upload file during converting or loading preview.',
    )
  }

  if (
    ((sourceFile && sourceFile.status === 'loading-preview') ||
      (sourceFile && sourceFile.status === 'converting') ||
      (convertedFile && convertedFile.status === 'loading-preview') ||
      (convertedFile && convertedFile.status === 'converting')) &&
    bc.status === 'in-review'
  ) {
    throw new Error(
      'You cannot upload file during loading preview of the file and status of bc in Review.',
    )
  }
}

const uploadStreamFile = async (files, category, context) => {
  try {
    const pubsub = await getPubsub()

    const handleFile = async f => {
      const { filename, createReadStream: stream } = f

      pubsub.publish(`${ON_UPLOAD_PROGRESS}.${context.user}`, {
        uploadConvertFileProgress: {
          file: filename,
          percentage: 1,
          status: 'uploading',
          total: files.length,
        },
      })

      const streamFile = stream()

      const file = await FileService.write(
        { stream: streamFile, filename, category },
        context.user,
      )

      pubsub.publish(`${ON_UPLOAD_PROGRESS}.${context.user}`, {
        uploadConvertFileProgress: {
          file: filename,
          percentage: 100,
          status: 'uploaded',
          total: files.length,
        },
      })

      return file
    }

    return Promise.all(
      files.map(async fileObject => {
        const f = await fileObject.data
        return handleFile(f)
      }),
    )
  } catch (e) {
    throw new Error(e)
  }
}

/**
 * Implements common pagination logic for displaying BookComponents nested in a
 * Division or another BookComponent.
 * @param {string} divisionIdField
 *   Parent field identifying the Division to which BookComponents must belong.
 *   If parent is a Division: divisionIdField == "id"
 *   If parent is a BookComponent: divisionIdField == "divisionId"
 * @param {boolean} useLoader
 *   If true, use "ctx.loaders.BookComponent.divisionMap" to load the pre-mapped
 *   list of ALL BookComponents belonging to the current division. This is
 *   faster when loading large, deeply nested BookComponent queries.
 *   If false, query BookComponent directly and retrieve just the immediate
 *   children of the parent. This is faster for shallow queries where there is
 *   no need to resolve nested BookComponents.
 * @param {Division|BookComponent} parent
 *   The parent object.
 *   Retrieve the BookComponents matching "parent.bookComponents".
 * @param {Object} args
 *   The args received from a graphql query.
 * @param {Object} ctx
 *   The ctx received from a graphql query.
 */
const paginatedBookComponents = async (
  parent,
  args,
  ctx,
  divisionIdField,
  useLoader,
) => {
  const { skip = 0, take = -1 } = args
  const results = []
  let total = 0

  // Build "parent.bookComponents"
  if (!isEmpty(parent.bookComponents)) {
    // bookComponentMap: BookComponents for this parent mapped by their ids
    let bookComponentMap

    if (useLoader) {
      // Use loader: assumes bookComponentMap is likely to be needed by other
      // resolver calls; preloads and maps ALL parent's descendants
      bookComponentMap = await ctx.loaders.BookComponent.divisionMapped.load(
        parent[divisionIdField],
      )
    } else {
      // Bypass loader: assumes ONLY this resolver will need bookComponentMap;
      // loads ONLY the parent's immediate children
      bookComponentMap = {}

      // Load ONLY parent.bookComponents
      const bookComponents = await BookComponent.query()
        .where({ divisionId: parent[divisionIdField], parentId: null })
        .whereIn('id', parent.bookComponents)

      // Map parent.bookComponents to bookComponentMap
      bookComponents.forEach(bc => {
        bookComponentMap[bc.id] = bc
      })
    }

    // Get BookComponent ids of the parent's immediate children
    let bookComponentIds = parent.bookComponents
    total = bookComponentIds.length

    if (skip || take > 0) {
      // Apply pagination
      if (take < 0) {
        bookComponentIds = bookComponentIds.slice(skip)
      } else {
        bookComponentIds = bookComponentIds.slice(skip, skip + take)
      }
    }

    bookComponentIds.forEach(bcId => {
      if (has(bookComponentMap, bcId)) {
        results.push({
          ...bookComponentMap[bcId],
          activeSiblingIds: bookComponentIds,
        })
      }
    })
  }

  return {
    metadata: { skip, take, total },
    results,
  }
}

module.exports = {
  uploadStreamFile,
  validateUpload,
  paginatedBookComponents,
}
