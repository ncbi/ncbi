const { Errors, NcbiNotificationMessage } = require('@pubsweet/models')
const { groupBy, forEach } = require('lodash')

const getErrors = async (_, { objectId, showHistory }, ctx) => {
  const errorWhere = showHistory ? { objectId } : { objectId, history: false }

  let errors = await Errors.query().where(errorWhere).orderBy('created')

  const jobs = await NcbiNotificationMessage.query()
    .where({ objectId })
    .orderBy('created', 'desc')

  const groupedErrors = jobs
    .map(job => {
      // eslint-disable-next-line no-param-reassign
      job.errors = errors.filter(err => err.jobId === job.jobId)
      errors = errors.filter(err => err.jobId !== job.jobId)
      if (job.errors.length > 0) return job
      return null
    })
    .filter(jb => !!jb)

  if (errors.length > 0) {
    const ncbiErrors = groupBy(errors, error => error.jobId)

    forEach(ncbiErrors, (value, key) => {
      groupedErrors.push({
        id: key,
        jobId: key,
        type: 'errors',
        updated: value[0].updated,
        errors: value,
      })
    })
  }

  return groupedErrors
}

module.exports = {
  JobReportError: {
    async sessionId(job) {
      if (!job.data.url) return ''
      const url = new URL(job.data.url)
      return url.searchParams.get('sessid')
    },
    async isCurrent(job) {
      if (job.errors.length > 0) return !job.errors[0].history
      return false
    },
    async jobType(job) {
      return null
    },
    async url(job) {
      return job.data.url
    },
    async updated(job) {
      return job.updated
    },
    async status(job) {
      return job.data.status
    },
  },
  BcmsReportError: {
    async isCurrent(bcmsError) {
      if (bcmsError.errors.length > 0) return !bcmsError.errors[0].history
      return false
    },
  },
  ErrorAndJob: {
    // eslint-disable-next-line no-underscore-dangle
    __resolveType(obj) {
      return obj.type === 'errors' ? 'BcmsReportError' : 'JobReportError'
    },
  },
  Query: {
    getErrors,
  },
}
