/* eslint-disable global-require */
const { Errors } = require('@pubsweet/models')

module.exports = {
  model: Errors,
  resolvers: require('./error.resolvers'),
  typeDefs: require('../graphqlLoaderUtil')('error/error.graphql'),
}
