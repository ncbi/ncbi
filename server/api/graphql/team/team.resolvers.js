/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */
const { Team, TeamMember } = require('@pubsweet/models')

const notify = require('../../../services/notify')

const eager = '[members.[user, alias]]'

module.exports = {
  Mutation: {
    async requestAccessToOrganisation(_, { id, members }, ctx) {
      // find Organization member
      const team = await Team.query().findOne({ id })

      const userTeam = await Team.query().findOne({
        objectId: team.objectId,
        role: 'user',
      })

      const teamMembers = await TeamMember.query().where({
        teamId: userTeam.id,
      })

      // check if user is verified to be assigned to a team
      const update = ctx.connectors.Team.update(
        id,
        {
          members: members
            .map(m => {
              const member = teamMembers.find(
                teamMember => teamMember.userId === m.user.id,
              )

              if (member && member.status === 'rejected') {
                return { ...member, status: 'unverified' }
              }

              if (member && member.status === 'unverified') return null

              return { ...m, status: member ? member.status : 'unverified' }
            })
            .filter(el => el != null),
        },
        ctx,
        {
          noDelete: true,
          unrelate: false,
          eager: 'members.user.teams',
        },
      )

      notify('requestAccessToOrganisation', {
        organisationId: userTeam.objectId,
        userId: ctx.user,
      })

      return update
    },
    async assignMembersToTeam(_, { id, members }, ctx) {
      // find Organization member
      const team = await Team.query().findOne({ id })

      const userTeam = await Team.query().findOne({
        objectId: team.objectId,
        role: 'user',
      })

      const isMember = await TeamMember.query().where({
        teamId: userTeam.id,
      })

      // check if user is verified to be assigned to a team
      return ctx.connectors.Team.update(
        id,
        {
          members: members
            .map(m => {
              const user = isMember.find(member => member.userId === m.user.id)

              if (user && user.status === 'unverified') return null

              return { ...m, status: user ? user.status : 'unverified' }
            })
            .filter(el => el != null),
        },
        ctx,
        {
          noDelete: true,
          unrelate: false,
          eager: 'members.user.teams',
        },
      )
    },
  },
  Query: {
    async sysAdmins(_, { status }, ctx) {
      status = status === undefined ? 'onlyEnabledUsers' : status
      return ctx.connectors.Team.fetchAll(
        {
          role: 'sysAdmin',
        },
        ctx,
        { eager: `[members(${status}).[user, alias]]` },
      )
    },
    teams(_, { where }, ctx) {
      where = where || {}

      if (where.users) {
        const { users } = where
        delete where.users
        where._relations = [{ ids: users, relation: 'users' }]
      }

      if (where.alias) {
        const { alias } = where
        delete where.alias
        where._relations = [{ object: alias, relation: 'aliases' }]
      }

      return ctx.connectors.Team.fetchAll(where, ctx, { eager })
    },
  },
}
