/* eslint-disable no-shadow */
/* eslint-disable jest/no-disabled-tests */
const _ = require('lodash')
const { startServer } = require('pubsweet-server')
const supertest = require('supertest')

const authentication = require('pubsweet-server/src/authentication')

const { db } = require('@coko/server')
const { Book, User } = require('@pubsweet/models')
const dbCleaner = require('../../helpers/db_cleaner')
const addSeedFactory = require('../../../../services/dbSeederFactory')

const fixtures = {
  otherUser: {
    email: 'some1@example.com',
    type: 'user',
    username: 'test1',
  },
  otherUser1: {
    email: 'some2@example.com',
    type: 'user',
    username: 'test2',
  },
  user: {
    email: 'test@example.com',
    password: 'test',
    type: 'user',
    username: 'testuser',
  },
}

const createBookAndOrganization = async (orgInput, bookInput, graphqlQuery) => {
  const organisation = await createOrganization(orgInput, graphqlQuery)

  const book = await createBook(
    {
      ...bookInput,
      organisationId: organisation.id,
    },
    graphqlQuery,
  )

  return { book, organisation }
}

const createOrganization = async (input, graphqlQuery) => {
  const { body } = await graphqlQuery(
    `mutation($input: CreateOrganisationInput!) {
        createOrganisation(input: $input) {
          id
          name
          email
        }
      }`,
    {
      input: {
        ...input,
      },
    },
  )

  return body.data.createOrganisation
}

const createBook = async (input, graphqlQuery) => {
  const { body } = await graphqlQuery(
    `mutation($input: CreateBookInput!) {
          createBook(input: $input) {
            id
            organisation {
              id
              name
            }
            updated
            title
            workflow
            status
            settings {
              chapterIndependently
              multiplePublishedVersions
              wholeBook
              eachChapter
            }
          }
        }`,
    {
      input,
    },
  )

  return body
}

/* eslint-disable-next-line jest/no-export */
module.exports = {
  createBook,
  createBookAndOrganization,
  createOrganization,
}

/** expectBookComponentsToMatch
 * Helper function for comparing BookComponents by title and child
 * BookComponents.
 */
const expectBookComponentsToMatch = (
  dbComponents,
  returnedComponents,
  compareFields = ['title'],
) => {
  expect(dbComponents.length).toEqual(returnedComponents.length)

  dbComponents.forEach(dbComponent => {
    // Cannot guarantee the order in which components are returned; use _.find
    const returnedComponent = _.find(returnedComponents, { id: dbComponent.id })

    compareFields.forEach(fieldName => {
      expect(returnedComponent[fieldName]).toEqual(dbComponent[fieldName])
    })

    if (_.isEmpty(dbComponent.bookComponents)) {
      expect(returnedComponent.bookComponents).toEqual([])
    } else {
      // "bookComponentReturned.bookComponents" should be an id array
      // Sort both arrays before comparing them
      expect(_.sortBy(returnedComponent.bookComponents)).toEqual(
        _.sortBy(dbComponent.bookComponents.map(bc => bc.id)),
      )
    }
  })
}

describe('Book GraphQL Query', () => {
  let token
  // let user
  let server
  let seeder
  let graphqlQuery
  let testData
  let allBodyBookComponents

  beforeAll(async () => {
    seeder = addSeedFactory()
    server = await startServer()
    /* await dbCleaner()
    user = await new User(fixtures.user).save()
    token = authentication.token.create(user) */

    graphqlQuery = (query, variables) => {
      const request = supertest(server)
      const req = request.post('/graphql').send({ query, variables })
      if (token) req.set('Authorization', `Bearer ${token}`)
      return req
    }
  })

  afterAll(done => {
    server.close(done)
  })

  afterEach(async () => {
    await db.raw('ROLLBACK')
  })

  beforeEach(async () => {
    await db.raw('BEGIN')

    const { user } = await seeder.insert('addAdmin', {
      password: '123456789',
      username: 'admin',
      email: 'admin@admin.com',
    })

    testData = await seeder.insert('addBookDetailed', {
      user,
      book: {
        settings: {
          toc: { order_chapters_by: 'number' },
        },
      },
      frontMatter: {
        bookComponents: [{ title: 'Preface' }],
      },
      body: {
        bookComponents: [
          { metadata: { chapter_number: 'Chapter 1' } },
          { metadata: { chapter_number: 'Chapter 2' } },
          { metadata: { chapter_number: 'Chapter 3' } },
          { metadata: { chapter_number: 'Chapter 4' } },
          { metadata: { chapter_number: 'Chapter five' } },
          { metadata: { chapter_number: 'Appendix A' } },
        ],
      },
      backMatter: {
        bookComponents: [{ title: 'Glossary' }, { title: 'Index' }],
      },
    })

    allBodyBookComponents = [
      {
        id: testData.body.bookComponents[0].id,
        bookComponents: [],
        metadata: { chapter_number: 'Chapter 1' },
      },
      {
        id: testData.body.bookComponents[1].id,
        bookComponents: [],
        metadata: { chapter_number: 'Chapter 2' },
      },
      {
        id: testData.body.bookComponents[2].id,
        bookComponents: [],
        metadata: { chapter_number: 'Chapter 3' },
      },
      {
        id: testData.body.bookComponents[3].id,
        bookComponents: [],
        metadata: { chapter_number: 'Chapter 4' },
      },
      {
        id: testData.body.bookComponents[4].id,
        bookComponents: [],
        metadata: { chapter_number: 'Chapter five' },
      },
      {
        id: testData.body.bookComponents[5].id,
        bookComponents: [],
        metadata: { chapter_number: 'Appendix A' },
      },
    ]
  })

  it('validates "getBook" query with skip 0', async () => {
    const { body } = await graphqlQuery(
      `{
  getBook(id: "${testData.book.id}", divisionLabel: "body", take: 3, skip: 0)
  {metadata{skip, take, total},
  results{id, divisions{id, label, bookComponents}, bookComponents{id, bookComponents, metadata{chapter_number}}}}
}`,
    )

    expect(body.data.getBook.metadata).toEqual({
      skip: '0',
      take: '3',
      total: '6',
    })

    const bookResult = body.data.getBook
    expect(bookResult.id).toEqual(testData.book.id)

    expect(bookResult.divisions.length).toEqual(3)

    // frontMatter components must be excluded due to 'divisionLabel:"body"'
    expect(bookResult.divisions[0]).toEqual({
      id: testData.frontMatter.id,
      label: testData.frontMatter.label,
      bookComponents: [],
    })

    // Body book components should be paginated but take exceeds total
    expect(testData.body.bookComponents.length).toEqual(6)

    expect(bookResult.divisions[1]).toEqual({
      id: testData.body.id,
      label: testData.body.label,
      bookComponents: [
        testData.body.bookComponents[0].id,
        testData.body.bookComponents[1].id,
        testData.body.bookComponents[2].id,
      ],
    })

    // frontMatter components must be excluded due to 'divisionLabel:"body"'
    expect(bookResult.divisions[2]).toEqual({
      id: testData.backMatter.id,
      label: testData.backMatter.label,
      bookComponents: [],
    })

    // bookResult.bookComponents must not be filtered and should contain every
    // BookComponent linked to the "body" division
    expectBookComponentsToMatch(
      allBodyBookComponents,
      bookResult.bookComponents,
      ['metadata'],
    )
  })

  it('validates last page of the "getBook" query', async () => {
    const { body } = await graphqlQuery(
      `{
  getBook(id: "${testData.book.id}", divisionLabel: "body", take: 4, skip: 4)
  {metadata{skip, take, total},
  results{id, divisions{id, label, bookComponents}, bookComponents{id, bookComponents, metadata{chapter_number}}}}
}`,
    )

    expect(body.data.getBook.metadata).toEqual({
      skip: '4',
      take: '4',
      total: '6',
    })

    const bookResult = body.data.getBook
    expect(bookResult.id).toEqual(testData.book.id)

    expect(bookResult.divisions.length).toEqual(3)

    // frontMatter components must be excluded due to 'divisionLabel:"body"'
    expect(bookResult.divisions[0]).toEqual({
      id: testData.frontMatter.id,
      label: testData.frontMatter.label,
      bookComponents: [],
    })

    // Body book components should be paginated but take exceeds total
    expect(testData.body.bookComponents.length).toEqual(6)

    expect(bookResult.divisions[1]).toEqual({
      id: testData.body.id,
      label: testData.body.label,
      bookComponents: [
        testData.body.bookComponents[4].id,
        testData.body.bookComponents[5].id,
      ],
    })

    // frontMatter components must be excluded due to 'divisionLabel:"body"'
    expect(bookResult.divisions[2]).toEqual({
      id: testData.backMatter.id,
      label: testData.backMatter.label,
      bookComponents: [],
    })

    // bookResult.bookComponents must not be filtered and should contain every
    // BookComponent linked to the "body" division
    expectBookComponentsToMatch(
      allBodyBookComponents,
      bookResult.bookComponents,
      ['metadata'],
    )
  })
})

describe('Book GraphQL Query nested', () => {
  let token
  // let user
  let server
  let seeder
  let graphqlQuery
  let testData

  beforeAll(async () => {
    seeder = addSeedFactory()
    server = await startServer()
    /* await dbCleaner()
    user = await new User(fixtures.user).save()
    token = authentication.token.create(user) */

    graphqlQuery = (query, variables) => {
      const request = supertest(server)
      const req = request.post('/graphql').send({ query, variables })
      if (token) req.set('Authorization', `Bearer ${token}`)
      return req
    }
  })

  afterAll(done => {
    server.close(done)
  })

  afterEach(async () => {
    await db.raw('ROLLBACK')
  })

  beforeEach(async () => {
    await db.raw('BEGIN')

    const { user } = await seeder.insert('addAdmin', {
      password: '123456789',
      username: 'admin',
      email: 'admin@admin.com',
    })

    testData = await seeder.insert('addBookDetailed', {
      user,
      book: {
        settings: {
          toc: { order_chapters_by: 'number' },
        },
      },
      frontMatter: { bookComponents: [{ title: 'Preface' }] },
      body: {
        bookComponents: [
          {
            title: 'Part 1',
            bookComponents: [
              { title: '1.1', metadata: { chapter_number: 'Chapter 1' } },
              { title: '1.2', metadata: { chapter_number: 'Chapter 2' } },
              { title: '1.3', metadata: { chapter_number: 'Chapter 3' } },
              { title: '1.4', metadata: { chapter_number: 'Chapter 4' } },
              { title: '1.5', metadata: { chapter_number: 'Chapter five' } },
            ],
          },
          {
            title: 'Part 2',
            bookComponents: [
              { title: '2.1', metadata: { chapter_number: 'Appendix A' } },
            ],
          },
        ],
      },
      backMatter: { bookComponents: [] },
    })
  })

  it('validates single page "getBook" query with "divisionLabel: body"', async () => {
    const { body } = await graphqlQuery(
      `{
  getBook(id: "${testData.book.id}", divisionLabel:"body", take: 3, skip: 0)
  {metadata{skip, take, total}, results{id, divisions{id, label, bookComponents}, bookComponents{id, title, bookComponents}}}
}`,
    )

    expect(body.data.getBook.metadata).toEqual({
      skip: '0',
      take: '3',
      total: '2', // Total top level BookComponents bound to "body" division
    })

    const bookResult = body.data.getBook
    expect(bookResult.id).toEqual(testData.book.id)

    expect(bookResult.divisions.length).toEqual(3)

    // frontMatter components must be excluded due to 'divisionLabel:"body"'
    expect(bookResult.divisions[0]).toEqual({
      id: testData.frontMatter.id,
      label: testData.frontMatter.label,
      bookComponents: [],
    })

    // Body book components should be paginated but take exceeds total
    expect(bookResult.divisions[1]).toEqual({
      id: testData.body.id,
      label: testData.body.label,
      bookComponents: testData.body.bookComponents.map(bc => bc.id),
    })

    // backMatter has no book components
    expect(bookResult.divisions[2]).toEqual({
      id: testData.backMatter.id,
      label: testData.backMatter.label,
      bookComponents: [],
    })

    // All book components belonging to the body division should be returned
    expectBookComponentsToMatch(
      [
        testData.body.bookComponents[0],
        testData.body.bookComponents[1],
        testData.body.bookComponents[0].bookComponents[0],
        testData.body.bookComponents[0].bookComponents[1],
        testData.body.bookComponents[0].bookComponents[2],
        testData.body.bookComponents[0].bookComponents[3],
        testData.body.bookComponents[0].bookComponents[4],
        testData.body.bookComponents[1].bookComponents[0],
      ],
      bookResult.bookComponents,
    )
  })

  it('validates single page "getBook" query with no "divisionLabel"', async () => {
    const { body } = await graphqlQuery(
      `{
  getBook(id: "${testData.book.id}", take: 5, skip: 1)
  {metadata{skip, take, total}, results{id, divisions{id, label, bookComponents}, bookComponents{id, title, bookComponents}}}
}`,
    )

    expect(body.data.getBook.metadata).toEqual({
      skip: '0', // "skip" is ignored when looking at multiple divisions
      take: '3', // "take" is ignored when looking at multiple divisions
      total: '3', // Total top level BookComponents bound to bookId
    })

    const bookResult = body.data.getBook
    expect(bookResult.id).toEqual(testData.book.id)

    // frontMatter components must be included due to empty 'divisionLabel'
    expect(bookResult.divisions[0]).toEqual({
      id: testData.frontMatter.id,
      label: testData.frontMatter.label,
      bookComponents: [testData.frontMatter.bookComponents[0].id],
    })

    // Body book components should be paginated but take exceeds total
    expect(bookResult.divisions[1]).toEqual({
      id: testData.body.id,
      label: testData.body.label,
      bookComponents: testData.body.bookComponents.map(bc => bc.id),
    })

    // backMatter has no book components
    expect(bookResult.divisions[2]).toEqual({
      id: testData.backMatter.id,
      label: testData.backMatter.label,
      bookComponents: [],
    })

    // All book components belonging to the body division should be returned
    expectBookComponentsToMatch(
      [
        // frontMatter is included because 'divisionLabel' is empty
        testData.frontMatter.bookComponents[0],
        testData.body.bookComponents[0],
        testData.body.bookComponents[1],
        testData.body.bookComponents[0].bookComponents[0],
        testData.body.bookComponents[0].bookComponents[1],
        testData.body.bookComponents[0].bookComponents[2],
        testData.body.bookComponents[0].bookComponents[3],
        testData.body.bookComponents[0].bookComponents[4],
        testData.body.bookComponents[1].bookComponents[0],
      ],
      bookResult.bookComponents,
    )
  })
})

describe.skip('Book GraphQL', () => {
  let token
  let user
  let server
  let graphqlQuery

  beforeAll(async () => {
    server = await startServer()
    await dbCleaner()
    user = await new User(fixtures.user).save()
    token = authentication.token.create(user)

    graphqlQuery = (query, variables) => {
      const request = supertest(server)
      const req = request.post('/graphql').send({ query, variables })
      if (token) req.set('Authorization', `Bearer ${token}`)
      return req
    }
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await dbCleaner()
  })

  it('can get Books', async () => {
    const input = {
      email: 'org@test.org',
      name: 'testorg',
    }

    const organisation = await createOrganization(input, graphqlQuery)

    await new Book({
      organisationId: organisation.id,
      title: 'testorg',
    }).save()

    await new Book({
      organisationId: organisation.id,
      title: 'testorg1',
    }).save()

    const { body } = await graphqlQuery(
      `query ($input: SearchModelInput)
        { getBooks(input: $input) {
          metadata {
            total
            skip
            take
          }
          results {
            id
            title
          }
        }
      }`,
      {
        input: {
          skip: 0,
          take: 20,
        },
      },
    )

    expect(body.data.getBooks.results).toHaveLength(2)
  })

  it('can create a book', async () => {
    const input = {
      email: 'org@test.org',
      name: 'testorg',
    }

    const createInput = {
      settings: {
        chapterIndependently: false,
        wholeBook: false,
        eachChapter: true,
        multiplePublishedVersions: true,
      },
      title: 'test book',
      workflow: 'word',
    }

    const { book, organisation } = await createBookAndOrganization(
      input,
      createInput,
      graphqlQuery,
    )

    expect(book).toMatchObject({
      data: {
        createBook: {
          organisation: { id: organisation.id, name: input.name },
          ...createInput,
        },
      },
    })
  })

  it('can update book ', async () => {
    const input = {
      email: 'org@test.org',
      name: 'testorg',
    }

    const createInput = {
      metadata: {
        language: 'fr',
      },
      settings: {
        chapterIndependently: false,
        wholeBook: false,
        eachChapter: true,
        multiplePublishedVersions: true,
      },
      title: 'test book',
      workflow: 'word',
    }

    const metadata = {
      author: [
        {
          affiliation: 'String',
          degrees: 'String',
          email: 'String',
          givenName: 'String',
          role: 'String',
          suffix: 'String',
          surname: 'String',
        },
      ],
      bookId: 'ncbik359',
      editor: [],
      language: 'en',
      pubDate: '2020-04-15 11:57:53Z',
      pub_loc: 'l',
      pub_name: 'Z',
    }

    const { settings } = createInput

    const updateFields = {
      abstract: 'my abstract',
      edition: '0',
      metadata,
      settings,
      title: 'test book update',
    }

    const { book } = await createBookAndOrganization(
      input,
      createInput,
      graphqlQuery,
    )

    const { body } = await graphqlQuery(
      `mutation ($id: ID!, $input: UpdateInput)
        {
          updateBook(id: $id, input: $input) {
            id
            title
            abstract
            edwholeBook
            settings {
              chapterIndependently
              wholeBook
              eachChapter
              multiplePublishedVersions
            }
            metadata {
              author {
                affiliation
                degrees
                email
                givenName
                role
                suffix
                surname
              }
              bookId
              editor {
                affiliation
                degrees
                email
                givenName
                role
                suffix
                surname
              }
              language
              pubDate
              pub_loc
              pub_name
            }
        }
      }`,
      {
        id: book.data.createBook.id,
        input: updateFields,
      },
    )

    expect(body).toMatchObject({
      data: {
        updateBook: { metadata, ...updateFields },
      },
    })
  })

  it('can delete a book', async () => {
    const input = {
      email: 'org@test.org',
      name: 'testorg',
    }

    const createInput = {
      settings: {
        chapterIndependently: false,
        wholeBook: false,
        eachChapter: true,
        multiplePublishedVersions: true,
      },
      title: 'test book',
      workflow: 'word',
    }

    const { book, organisation } = await createBookAndOrganization(
      input,
      createInput,
      graphqlQuery,
    )

    const {
      data: { createBook: book1 },
    } = await createBook(
      {
        ...createInput,
        organisationId: organisation.id,
        title: 'test book 1',
      },
      graphqlQuery,
    )

    const { body } = await graphqlQuery(
      `mutation($id: [ID]) {
          deleteBook(id: $id)
        }`,
      { id: [book1.id, book.data.createBook.id] },
      token,
    )

    expect(body).toEqual({
      data: { deleteBook: [book1.id, book.data.createBook.id] },
    })
  })
})
