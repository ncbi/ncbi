/* eslint-disable no-shadow */
/* eslint-disable no-unused-vars */
/* eslint-disable jest/no-disabled-tests */
const { Book } = require('@pubsweet/models')

const { db } = require('@coko/server')

const _ = require('lodash')
const addSeedFactory = require('../../../../services/dbSeederFactory')
const createGraphQLServer = require('../../helpers/createTestServer')

describe('Book GraphQL Query', () => {
  let seeder
  let testServer
  let testData
  let allBodyBookComponents

  beforeAll(async () => {
    seeder = addSeedFactory()

    await db.raw('BEGIN')

    const { user } = await seeder.insert('addAdmin', {
      password: '123456789',
      username: 'admin',
      email: 'admin@admin.com',
    })

    testServer = await createGraphQLServer(user.id)

    testData = await seeder.insert('addBookDetailed', {
      user,
      book: {
        settings: {
          toc: { order_chapters_by: 'number' },
        },
      },
      frontMatter: {
        bookComponents: [{ title: 'Preface' }],
      },
      body: {
        bookComponents: [
          { metadata: { chapter_number: 'Chapter 1' } },
          { metadata: { chapter_number: 'Chapter 2' } },
          { metadata: { chapter_number: 'Chapter 3' } },
          { metadata: { chapter_number: 'Chapter 4' } },
          { metadata: { chapter_number: 'Chapter five' } },
          { metadata: { chapter_number: 'Appendix A' } },
        ],
      },
      backMatter: {
        bookComponents: [{ title: 'Glossary' }, { title: 'Index' }],
      },
    })

    allBodyBookComponents = [
      {
        id: testData.body.bookComponents[0].id,
        bookComponents: [],
        metadata: { chapter_number: 'Chapter 1' },
      },
      {
        id: testData.body.bookComponents[1].id,
        bookComponents: [],
        metadata: { chapter_number: 'Chapter 2' },
      },
      {
        id: testData.body.bookComponents[2].id,
        bookComponents: [],
        metadata: { chapter_number: 'Chapter 3' },
      },
      {
        id: testData.body.bookComponents[3].id,
        bookComponents: [],
        metadata: { chapter_number: 'Chapter 4' },
      },
      {
        id: testData.body.bookComponents[4].id,
        bookComponents: [],
        metadata: { chapter_number: 'Chapter five' },
      },
      {
        id: testData.body.bookComponents[5].id,
        bookComponents: [],
        metadata: { chapter_number: 'Appendix A' },
      },
    ]
  })

  afterAll(async done => {
    await db.raw('ROLLBACK')
  })

  it('expects full results for "getBook" query with no filters or pagination', async () => {
    const res = await testServer.executeOperation({
      query: `{
  getBook(id: "${testData.book.id}") {
    id,
    divisions {
      id,
      label,
      fullBookComponents,
      bookComponents {
        metadata{skip, take, total},
        results{id}
      }
    },
    bookComponents {
      id,
      bookComponents {
        metadata{skip, take, total},
        results{id}
      },
      metadata{chapter_number},
      title
    }
  }}`,
    })

    // Validate getBook.id
    const bookResult = res.data.getBook
    expect(bookResult.id).toEqual(testData.book.id)

    // Validate getBook.divisions
    const divisionResult = _.keyBy(bookResult.divisions, 'label')

    expect(_.sortBy(_.keys(divisionResult))).toEqual([
      'backMatter',
      'body',
      'frontMatter',
    ])

    expect(divisionResult.frontMatter.id).toEqual(testData.frontMatter.id)

    expect(divisionResult.frontMatter.fullBookComponents).toEqual([
      testData.frontMatter.bookComponents[0].id,
    ])

    expect(divisionResult.frontMatter.bookComponents.metadata).toEqual({
      skip: '0',
      take: '-1',
      total: '1',
    })

    expect(divisionResult.frontMatter.bookComponents.results).toEqual([
      { id: testData.frontMatter.bookComponents[0].id },
    ])

    expect(divisionResult.body.id).toEqual(testData.body.id)

    expect(divisionResult.body.fullBookComponents).toEqual(
      _.range(0, 6).map(i => testData.body.bookComponents[i].id),
    )

    expect(divisionResult.body.bookComponents.metadata).toEqual({
      skip: '0',
      take: '-1',
      total: '6',
    })

    expect(divisionResult.body.bookComponents.results).toEqual(
      _.range(0, 6).map(i => {
        return { id: testData.body.bookComponents[i].id }
      }),
    )

    expect(divisionResult.backMatter.id).toEqual(testData.backMatter.id)

    expect(divisionResult.backMatter.fullBookComponents).toEqual([
      testData.backMatter.bookComponents[0].id,
      testData.backMatter.bookComponents[1].id,
    ])

    expect(divisionResult.backMatter.bookComponents.metadata).toEqual({
      skip: '0',
      take: '-1',
      total: '2',
    })

    expect(divisionResult.backMatter.bookComponents.results).toEqual([
      { id: testData.backMatter.bookComponents[0].id },
      { id: testData.backMatter.bookComponents[1].id },
    ])

    // Validate getBook.bookComponents
    const bookComponentRefs = _.sortBy(
      bookResult.bookComponents.map(bc =>
        bc.metadata?.chapter_number
          ? `Chpt:${bc.metadata.chapter_number}`
          : `Ttl:${bc.title}`,
      ),
    )

    // getBook.bookComponents is unordered BUT all components must be present
    expect(bookComponentRefs).toEqual([
      'Chpt:Appendix A',
      'Chpt:Chapter 1',
      'Chpt:Chapter 2',
      'Chpt:Chapter 3',
      'Chpt:Chapter 4',
      'Chpt:Chapter five',
      'Ttl:Glossary',
      'Ttl:Index',
      'Ttl:Preface',
    ])

    // There should be no nested BookComponents
    _.range(0, 9).forEach(i => {
      expect(bookResult.bookComponents[i].bookComponents).toEqual({
        metadata: {
          skip: '0',
          take: '-1',
          total: '0',
        },
        results: [],
      })
    })
  })

  it('validates "getBook" query with skip 0', async () => {
    const res = await testServer.executeOperation({
      query: `{
  getBook(id: "${testData.book.id}") {
    id,
    divisions(divisionLabels: ["body"]) {
      id,
      label,
      fullBookComponents,
      bookComponents(take: 3, skip: 0) {
        metadata{skip, take, total},
        results{id}
      }
    },
    bookComponents(divisionLabels: ["body"]){
      id,
      bookComponents {
        metadata{skip, take, total},
        results{id}
      },
      metadata{chapter_number}
    }
  }}`,
    })

    // Validate getBook.id
    const bookResult = res.data.getBook
    expect(bookResult.id).toEqual(testData.book.id)

    // Validate getBook.divisions
    expect(bookResult.divisions.length).toEqual(1)
    const divisionResult = bookResult.divisions[0]
    expect(divisionResult.id).toEqual(testData.body.id)
    expect(divisionResult.label).toEqual('body')

    expect(divisionResult.fullBookComponents).toEqual(
      _.range(0, 6).map(i => testData.body.bookComponents[i].id),
    )

    expect(divisionResult.bookComponents.metadata).toEqual({
      skip: '0',
      take: '3',
      total: '6',
    })

    expect(divisionResult.bookComponents.results).toEqual(
      _.range(0, 3).map(i => {
        return { id: testData.body.bookComponents[i].id }
      }),
    )

    // Validate getBook.bookComponents
    const bookComponentRefs = _.sortBy(
      bookResult.bookComponents.map(bc => bc.metadata.chapter_number),
    )

    // getBook.bookComponents: all body components must be present
    expect(bookComponentRefs).toEqual([
      'Appendix A',
      'Chapter 1',
      'Chapter 2',
      'Chapter 3',
      'Chapter 4',
      'Chapter five',
    ])
  })

  it('validates last page of the "getBook" query', async () => {
    const res = await testServer.executeOperation({
      query: `{
  getBook(id: "${testData.book.id}") {
    id,
    divisions(divisionLabels: ["body"]) {
      id,
      label,
      fullBookComponents,
      bookComponents(take: 4, skip: 4) {
        metadata{skip, take, total},
        results{id}
      }
    }
  }}`,
    })

    // Validate getBook.id
    const bookResult = res.data.getBook
    expect(bookResult.id).toEqual(testData.book.id)

    // Validate getBook.divisions
    expect(bookResult.divisions.length).toEqual(1)
    const divisionResult = bookResult.divisions[0]
    expect(divisionResult.id).toEqual(testData.body.id)
    expect(divisionResult.label).toEqual('body')

    expect(divisionResult.fullBookComponents).toEqual(
      _.range(0, 6).map(i => testData.body.bookComponents[i].id),
    )

    expect(divisionResult.bookComponents.metadata).toEqual({
      skip: '4',
      take: '4',
      total: '6',
    })

    expect(divisionResult.bookComponents.results).toEqual(
      _.range(4, 6).map(i => {
        return { id: testData.body.bookComponents[i].id }
      }),
    )
  })
})

describe('Book GraphQL Query nested', () => {
  let seeder
  let testServer
  let testData

  beforeAll(async () => {
    seeder = addSeedFactory()
    await db.raw('BEGIN')

    const { user } = await seeder.insert('addAdmin', {
      password: '123456789',
      username: 'admin',
      email: 'admin@admin.com',
    })

    testServer = await createGraphQLServer(user.id)

    testData = await seeder.insert('addBookDetailed', {
      user,
      book: {
        settings: {
          toc: { order_chapters_by: 'number' },
        },
      },
      frontMatter: { bookComponents: [{ title: 'Preface' }] },
      body: {
        bookComponents: [
          {
            title: 'Part 1',
            bookComponents: [
              { title: '1.1', metadata: { chapter_number: 'Chapter 1' } },
              { title: '1.2', metadata: { chapter_number: 'Chapter 2' } },
              { title: '1.3', metadata: { chapter_number: 'Chapter 3' } },
              { title: '1.4', metadata: { chapter_number: 'Chapter 4' } },
              { title: '1.5', metadata: { chapter_number: 'Chapter five' } },
            ],
          },
          {
            title: 'Part 2',
            bookComponents: [
              { title: '2.1', metadata: { chapter_number: 'Appendix A' } },
            ],
          },
        ],
      },
      backMatter: { bookComponents: [] },
    })
  })

  afterAll(async () => {
    await db.raw('ROLLBACK')
  })

  it('validates single page "getBook" query with "divisionLabel: body"', async () => {
    const res = await testServer.executeOperation({
      query: `{
  getBook(id: "${testData.book.id}") {
    id,
    divisions {
      id,
      label,
      fullBookComponents,
      bookComponents(take: 3, skip: 0) {
        metadata{skip, take, total},
        results{id}
      }
    },
    bookComponents {
      id,
      bookComponents(take: 3, skip: 0) {
        metadata{skip, take, total},
        results{id}
      },
      title
    }
  }}`,
    })

    // Validate getBook.id
    const bookResult = res.data.getBook
    expect(bookResult.id).toEqual(testData.book.id)

    // Validate getBook.divisions
    const divisionResult = _.keyBy(bookResult.divisions, 'label')

    expect(_.sortBy(_.keys(divisionResult))).toEqual([
      'backMatter',
      'body',
      'frontMatter',
    ])

    expect(divisionResult.frontMatter.id).toEqual(testData.frontMatter.id)

    expect(divisionResult.frontMatter.fullBookComponents).toEqual([
      testData.frontMatter.bookComponents[0].id,
    ])

    expect(divisionResult.frontMatter.bookComponents.metadata).toEqual({
      skip: '0',
      take: '3',
      total: '1',
    })

    expect(divisionResult.frontMatter.bookComponents.results).toEqual([
      { id: testData.frontMatter.bookComponents[0].id },
    ])

    expect(divisionResult.body.id).toEqual(testData.body.id)

    expect(divisionResult.body.fullBookComponents).toEqual([
      testData.body.bookComponents[0].id,
      testData.body.bookComponents[1].id,
    ])

    expect(divisionResult.body.bookComponents.metadata).toEqual({
      skip: '0',
      take: '3',
      total: '2',
    })

    expect(divisionResult.body.bookComponents.results).toEqual([
      { id: testData.body.bookComponents[0].id },
      { id: testData.body.bookComponents[1].id },
    ])

    // backMatter has no book components
    expect(divisionResult.backMatter.id).toEqual(testData.backMatter.id)

    expect(divisionResult.backMatter.fullBookComponents).toEqual([])

    expect(divisionResult.backMatter.bookComponents.metadata).toEqual({
      skip: '0',
      take: '3',
      total: '0',
    })

    expect(divisionResult.backMatter.bookComponents.results).toEqual([])

    // Validate getBook.bookComponents
    const bookComponentRefs = _.keyBy(bookResult.bookComponents, 'title')

    // getBook.bookComponents is unordered BUT all components must be present
    expect(_.sortBy(_.keys(bookComponentRefs))).toEqual([
      '1.1',
      '1.2',
      '1.3',
      '1.4',
      '1.5',
      '2.1',
      'Part 1',
      'Part 2',
      'Preface',
    ])

    // BookComponents with children
    expect(bookComponentRefs['Part 1'].bookComponents).toEqual({
      metadata: {
        skip: '0',
        take: '3',
        total: '5',
      },
      results: _.range(0, 3).map(i => {
        return { id: testData.body.bookComponents[0].bookComponents[i].id }
      }),
    })

    expect(bookComponentRefs['Part 2'].bookComponents).toEqual({
      metadata: {
        skip: '0',
        take: '3',
        total: '1',
      },
      results: [{ id: testData.body.bookComponents[1].bookComponents[0].id }],
    })

    // BookComponents with no children
    const noChildren = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', 'Preface']

    noChildren.forEach(ref => {
      expect(bookComponentRefs[ref].bookComponents).toEqual({
        metadata: {
          skip: '0',
          take: '3',
          total: '0',
        },
        results: [],
      })
    })
  })
})
