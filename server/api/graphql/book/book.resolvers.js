/* eslint-disable no-return-await */
const { isEmpty, join } = require('lodash')
const { db, useTransaction } = require('@coko/server')

const { getPubsub } = require('pubsweet-server/src/graphql/pubsub')

const ON_OBJECT_UPDATE = 'OBJECT_UPDATED'

const {
  Book,
  Channel,
  TeamMember,
  Team,
  BookComponent,
  Errors,
  Division,
  Organisation,
  Collection,
  Toc,
  Issue,
} = require('@pubsweet/models')

const { SearchService, Domain, Notify } = require('../../../services')

const getBooks = async (_, args, ctx) => {
  const searchedBooks = new SearchService(Book, {
    ...args.input,
    filter: args.input.search.criteria,
  })

  const results = await searchedBooks.search()

  return {
    metadata: searchedBooks.metadata,
    results,
  }
}

const createBook = async (_, { input }, ctx) =>
  useTransaction(async transaction => {
    const domain = new Domain({ ...input, user: ctx.user, transaction })
    const book = await domain.executeCommand('create')

    return book
  })

const createBookVersion = async (_, { input }, ctx) =>
  useTransaction(async transaction => {
    const domain = new Domain({ ...input, user: ctx.user, transaction })
    const book = await domain.executeCommand('newVersion')
    return book
  })

const getBook = async (_, { id }, ctx) => {
  return await Book.query().findOne({ id })
}

const getSettingsPublisher = async settings => {
  const publisherId = settings.publisher
  return await Organisation.query().findById(publisherId).throwIfNotFound()
}

const updateBook = async (_, { id, input }, ctx) => {
  return useTransaction(transaction => {
    const domain = new Domain({
      id,
      input,
      user: ctx.user,
      transaction,
    })

    return domain.executeCommand('update')
  })
}

const deleteBook = async (_, { id }, ctx) => {
  await Book.relatedQuery('divisions').delete().whereIn('book_id', id)

  await Book.query().delete().whereIn('id', id)
  return id
}

const checkAllBook = async (_, { action }, ctx) => {
  if (action === 'DELETE') {
    await Book.relatedQuery('divisions').delete()

    await Book.query().delete()

    return 'deleted'
  }

  return true
}

const updateBookTeamMembers = async (_, { input }, ctx) =>
  await Promise.all(
    // eslint-disable-next-line consistent-return
    input.map(async inpt => {
      const { id, team } = inpt
      const newMembers = team.members

      const currentMembers = await TeamMember.query()
        .where({ teamId: id })
        .eager('user')

      const toAdd = newMembers.filter(member => {
        return !currentMembers.find(m => m.user.id === member.user.id)
      })

      const toRemove = currentMembers.filter(member => {
        return !newMembers.find(m => m.user.id === member.user.id)
      })

      if (team.members.includes(null)) {
        await TeamMember.query().delete().whereIn('team_id', [id])
      } else {
        const updated = await ctx.connectors.Team.update(id, team, ctx, {
          eager: 'members.user.teams',
          unrelate: false,
        })

        toAdd.forEach(member => {
          Notify('bookTeamUpdated', {
            userId: member.user.id,
            teamId: inpt.id,
            type: 'add',
          })
        })

        toRemove.forEach(member => {
          Notify('bookTeamUpdated', {
            userId: member.user.id,
            teamId: inpt.id,
            type: 'remove',
          })
        })

        return updated
      }
    }),
  )

const getBookDivisions = async (_, { bookId }, ctx) => {}

const getBookVersions = async (_, { bookId }, ctx) => {
  const book = await Book.query().findOne({ id: bookId })

  return book.getRelatedVersions()
}

const getBookCollection = async book =>
  Collection.query().findById(book.collectionId)

module.exports = {
  Book: {
    async divisions(book, { divisionLabels }, ctx) {
      const divisionsQuery = Division.query()
        .where({ bookId: book.id })
        .orderByRaw(
          `array_position(array['${Division.DivisionConfig.join(
            "'::text,'",
          )}'::text], label)`,
        )

      if (!isEmpty(divisionLabels)) {
        divisionsQuery.whereIn('label', divisionLabels)
      }

      return await divisionsQuery
    },
    async metadata(book) {
      if (book.workflow === 'word') {
        const bookComponent = await BookComponent.query().findOne({
          bookId: book.id,
        })

        if (bookComponent) {
          // eslint-disable-next-line no-param-reassign
          book.metadata.url = bookComponent.metadata.url
        }
      }

      return book.metadata
    },
    async issues(book, _, ctx) {
      const issuesList = await Issue.query()
        .where({ objectId: book.id })
        .orderBy('created')

      return issuesList || []
    },
    async channels(book, _, ctx) {
      return Channel.query().where({
        objectId: book.id,
      })
    },
    async reviewsFiles(book, _, ctx) {
      if (book.workflow === 'word') {
        const bookComponent = await BookComponent.query().findOne({
          bookId: book.id,
        })

        if (!bookComponent) return []

        return await BookComponent.getFiles()
          .where({ 'file_versions.category': 'review' })
          .for([bookComponent.id])
      }

      return await Book.getFiles()
        .where({ 'file_versions.category': 'review' })
        .for([book.id])
    },
    async reviewsResponse(book) {
      return []
    },
    async supplementaryFiles(book) {
      if (book.workflow === 'word') {
        const bookComponent = await BookComponent.query().findOne({
          bookId: book.id,
        })

        if (!bookComponent) return []

        return await BookComponent.getFiles()
          .where({ 'file_versions.category': 'supplement' })
          .for(bookComponent)
          .orderByRaw('file_versions.version_name::int desc')
      }

      return await Book.getFiles()
        .where({ 'file_versions.category': 'supplement' })
        .for(book.id)
        .orderByRaw('file_versions.version_name::int desc')
    },
    async pdf(book) {
      if (book.workflow === 'word') {
        const bookComponent = await BookComponent.query().findOne({
          bookId: book.id,
        })

        if (!bookComponent) return []

        return await BookComponent.getFiles()
          .where({ 'file_versions.category': 'pdf' })
          .for(bookComponent)
          .orderByRaw('file_versions.version_name::int desc')
      }

      return await Book.getFiles()
        .where({ 'file_versions.category': 'pdf' })
        .for(book.id)
        .orderByRaw('file_versions.version_name::int desc')
    },
    async images(book) {
      if (book.workflow === 'word') {
        const bookComponent = await BookComponent.query().findOne({
          bookId: book.id,
        })

        if (!bookComponent) return []

        return await BookComponent.getFiles()
          .where({ 'file_versions.category': 'image' })
          .for(bookComponent)
          .orderByRaw('file_versions.version_name::int desc')
      }

      return await Book.getFiles()
        .where({ 'file_versions.category': 'image' })
        .for(book.id)
        .orderByRaw('file_versions.version_name::int desc')
    },
    async supportFiles(book) {
      if (book.workflow === 'word') {
        const bookComponent = await BookComponent.query().findOne({
          bookId: book.id,
        })

        if (!bookComponent) return []

        return await BookComponent.getFiles()
          .where({ 'file_versions.category': 'support' })
          .for(bookComponent)
          .orderByRaw('file_versions.version_name::int desc')
      }

      return await Book.getFiles()
        .where({ 'file_versions.category': 'support' })
        .for(book.id)
        .orderByRaw('file_versions.version_name::int desc')
    },
    async convertedFile(book) {
      if (book.workflow === 'word') {
        const bookComponent = await BookComponent.query().findOne({
          bookId: book.id,
        })

        if (!bookComponent) return []

        return await BookComponent.getFiles()
          .where({ 'file_versions.category': 'converted' })
          .for(bookComponent)
          .orderByRaw('file_versions.version_name::int desc')
      }

      return await Book.getFiles()
        .where({ 'file_versions.category': 'converted' })
        .for(book.id)
        .orderByRaw('file_versions.version_name::int desc')
    },
    async sourceFiles(book, _, ctx) {
      if (book.workflow === 'word') {
        const bookComponent = await BookComponent.query().findOne({
          bookId: book.id,
        })

        if (!bookComponent) return []

        return await BookComponent.getFiles()
          .where({ 'file_versions.category': 'source' })
          .for(bookComponent)
          .orderByRaw('file_versions.version_name::int desc')
      }

      return await Book.getFiles()
        .where({ 'file_versions.category': 'source' })
        .for(book)
        .orderByRaw('file_versions.version_name::int desc')
    },
    async fileCover(book) {
      const { bookCover } = await new Book(book).getFile()
      return bookCover
    },
    async fileAbstract(book) {
      const { abstractCover } = await new Book(book).getFileAbstract()
      return abstractCover
    },
    async alias(book) {
      return `bcms${book.alias}`
    },
    async bookComponents(book, { divisionLabels }, ctx) {
      let bookComponents = BookComponent.query().where({
        bookId: book.id,
        parentId: null,
      })

      // Filter by division if book.divisions is specified
      if (!isEmpty(divisionLabels)) {
        bookComponents = bookComponents.whereExists(
          db
            .select('id')
            .from('divisions')
            .whereRaw(
              `divisions.id = book_component.division_id AND
               divisions.label IN ('${join(divisionLabels, "', '")}')`,
            ),
        )
      }

      bookComponents = await bookComponents

      // TODO: DO NOT DELETE THIS FUNCTION UNTIL:
      //       1) Move the optimisations below to the loaders
      // See node_modules/pubsweet-server/src/graphql/loaders.js (line 37: component.modelLoaders)
      // If need be, add custom loaders to duplicate the queries below
      // Try to use the exising loaders
      const allIssues = await Issue.query()
        .whereIn(
          'objectId',
          bookComponents.map(bc => bc.id),
        )
        .orderBy('created')

      const allTeams = await Team.query()
        .whereIn(
          'objectId',
          bookComponents.map(bc => bc.id),
        )
        .eager('[members.[user, alias]]')

      const allSourceFiles = await BookComponent.getFiles()
        .where({ 'file_versions.category': 'source' })
        .for(bookComponents.map(bc => bc.id))
        .orderByRaw('file_versions.version_name::int desc')

      const allConvertedFiles = await BookComponent.getFiles()
        .where({ 'file_versions.category': 'converted' })
        .for(bookComponents.map(bc => bc.id))
        .orderByRaw('file_versions.version_name::int desc')

      bookComponents = bookComponents.map(bc => {
        const teams = allTeams.filter(tm => tm.objectId === bc.id)

        const sourceFiles = allSourceFiles.filter(
          src => src.bookComponentId === bc.id,
        )

        const convertedFile = allConvertedFiles.filter(
          conv => conv.bookComponentId === bc.id,
        )

        const issues = allIssues.filter(is => is.objectId === bc.id)

        return {
          ...bc,
          teams,
          sourceFiles,
          convertedFile,
          issues,
        }
      })

      return bookComponents
    },
    async errors(book) {
      const isMultiChapterBook = book?.settings.chapterIndependently
      let errorslist = []

      if (isMultiChapterBook) {
        const bookComponents = await BookComponent.query().where({
          bookId: book.id,
          parentId: null,
        })

        errorslist = await Errors.query()
          .whereIn(
            'objectId',
            bookComponents.map(x => x.id),
          )
          .where({ history: false })
          .orderBy('created')
      } else {
        errorslist = await Errors.query()
          .where({ objectId: book.id, history: false })
          .orderBy('created')
      }

      return errorslist
    },
    async toc(book) {
      return Toc.query().findOne({ bookId: book.id })
    },
    async organisation(book, _, ctx) {
      const teams = await ctx.connectors.Team.fetchAll(
        {
          objectId: book.organisationId,
          objectType: 'Organisation',
        },
        ctx,
        { eager: '[members(onlyEnabledUsers).[user, alias]]' },
      )

      const organisation = await ctx.connectors.Organisation.fetchOne(
        book.organisationId,
        ctx,
      )

      return Object.assign(organisation, {
        teams,
        users: {
          metadata: {},
          results: (
            (teams.find(tm => tm.role === 'user') || {}).members || []
          ).map(member => member.user),
        },
      })
    },
    async parts(book, _, ctx) {
      return await BookComponent.query().where({
        bookId: book.id,
        componentType: 'part',
      })
    },
    async teams(book, _, ctx) {
      return await ctx.connectors.Team.fetchAll(
        {
          objectId: book.id,
        },
        ctx,
        { eager: '[members.[user, alias]]' },
      )
    },
    collection: getBookCollection,
  },
  SettingsBook: {
    publisher: getSettingsPublisher,
  },
  Mutation: {
    checkAllBook,
    createBook,
    deleteBook,
    updateBook,
    updateBookTeamMembers,
    createBookVersion,
  },
  Query: {
    getBook,
    getBookDivisions,
    getBooks,
    getBookVersions,
  },
  Subscription: {
    objectUpdated: {
      subscribe: async (_, vars, context) => {
        const pubsub = await getPubsub()
        return pubsub.asyncIterator(`${ON_OBJECT_UPDATE}.${vars.id}`)
      },
    },
  },
}
