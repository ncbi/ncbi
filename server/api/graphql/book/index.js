/* eslint-disable global-require */

module.exports = {
  resolvers: require('./book.resolvers'),
  typeDefs: require('../graphqlLoaderUtil')('book/book.graphql'),
}
