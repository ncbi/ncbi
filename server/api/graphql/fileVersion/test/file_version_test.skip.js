/* eslint-disable no-unused-vars */
/* eslint-disable jest/no-disabled-tests */
const { User } = require('@pubsweet/models')
// const { createBookAndOrganization } = require('../../book/test/book_test')

const { startServer } = require('pubsweet-server')
// eslint-disable-next-line import/no-extraneous-dependencies
const supertest = require('supertest')

const authentication = require('pubsweet-server/src/authentication')
const dbCleaner = require('../../helpers/db_cleaner')

const fixtures = {
  otherUser: {
    email: 'some1@example.com',
    type: 'user',
    username: 'test1',
  },
  otherUser1: {
    email: 'some2@example.com',
    type: 'user',
    username: 'test2',
  },
  user: {
    email: 'test@example.com',
    password: 'test',
    type: 'user',
    username: 'testuser',
  },
}

describe.skip('BookComponent GraphQL', () => {
  let token
  let user
  let server
  let graphqlQuery
  let graphqlQueryUpload

  beforeAll(async () => {
    server = await startServer()
    await dbCleaner()
    user = await new User(fixtures.user).save()
    token = authentication.token.create(user)

    graphqlQuery = (query, variables) => {
      const request = supertest(server)
      const req = request.post('/graphql').send({ query, variables })
      if (token) req.set('Authorization', `Bearer ${token}`)
      return req
    }

    graphqlQueryUpload = (query, variables) => {
      const request = supertest(server)

      const req = request
        .post('/graphql')
        .field(
          'operations',
          JSON.stringify({
            operationName: null,
            query,
            variables,
          }),
        )
        .field(
          'map',
          JSON.stringify({
            0: ['variables.file.data'],
          }),
        )
        .attach('0', Buffer.from('hello world'), 'hello.txt')

      if (token) req.set('Authorization', `Bearer ${token}`)
      return req
    }
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await dbCleaner()
  })

  /* eslint-disable-next-line jest/no-commented-out-tests */
  // it('can upload a new version of bookComponent', async () => {
  // const input = {
  //   email: 'org@test.org',
  //   name: 'testorg',
  // }

  // const createInput = {
  //   settings: {
  //     chapterIndependently: false,
  //     wholeBook: false,
  //     eachChapter: true,
  //     multiplePublishedVersions: true,
  //   },
  //   title: 'test book',
  //   workflow: 'word',
  // }

  // const { book } = await createBookAndOrganization(
  //   input,
  //   createInput,
  //   graphqlQuery,
  // )

  // const { body } = await graphqlQueryUpload(
  //   `mutation($file: FileUploadInput, $id: ID!) {
  //     uploadBookComponentVersion(file: $file, id: $id) {
  //         id
  //         title
  //         versionName
  //       }
  //     }`,
  //   {
  //     file: { data: '' },
  //     id: bookComponent.model.id,
  //   },
  // )

  // const [previousBookComponent] = await FileVersion.query().where(
  //   'bookComponentId',
  //   '=',
  //   bookComponent.model.id,
  // )

  // expect(previousBookComponent.versionName).toBe('1')
  // expect(body.data.uploadBookComponentVersion.versionName).toBe('2')
  // })
})
