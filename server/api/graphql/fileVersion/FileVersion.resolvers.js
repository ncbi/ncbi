/* eslint-disable camelcase */
/* eslint-disable no-return-await */
/* eslint-disable arrow-body-style */
/* eslint-disable no-param-reassign */

const { UserInputError } = require('apollo-server-express')

const { User, FileVersion, SourceConvertedFile } = require('@pubsweet/models')

const { useTransaction } = require('@coko/server')

const ContentProccess = require('../../../services/ContentProccess')

const reconvertBookComponent = async (_, { ids }, ctx) => {
  for (let i = 0; i < ids.length; i += 1) {
    const {
      model: Model,
      object,
      command: Command,
      // eslint-disable-next-line no-await-in-loop
    } = await ContentProccess.findBestContent(ids[i])

    // eslint-disable-next-line no-await-in-loop
    await useTransaction(async trx => {
      const [sourceFile] = await Model.latestComponentFiles(
        object,
        'source',
        trx,
      )

      const [convertedFile] = await Model.latestComponentFiles(
        object,
        'converted',
        trx,
      )

      if (!convertedFile) {
        throw new UserInputError(
          'You cannot load preview without a converted file',
        )
      }

      if (
        (sourceFile &&
          (sourceFile.status === 'loading-preview' ||
            sourceFile.status === 'converting') &&
          object.status === 'in-review') ||
        (convertedFile &&
          (convertedFile.status === 'loading-preview' ||
            convertedFile.status === 'converting') &&
          object.status === 'in-review')
      ) {
        throw new UserInputError(
          'You cannot load preview of the file and status of bookComponent in Review.',
        )
      }

      const publishedVersions = await Model.hasPublishedVersions(object, trx)

      const content = new Command({
        object,
        transaction: trx,
        publishType:
          publishedVersions.length > 0 ? 'preview-published' : 'preview',
        currentUser: ctx.user,
      })

      await content.executeCommand('ingestToPMC')

      return object
    })
  }

  return []
}

const updateTagFile = async (_, { id, tag }, ctx) => {
  const { bookComponentId, bookId } = await FileVersion.query().findOne({ id })

  const objectClause = bookComponentId ? { bookComponentId } : { bookId }

  await FileVersion.query()
    .patch({ tag: null })
    .where({ ...objectClause, tag })

  return FileVersion.query().patch({ tag }).findOne({ id }).returning('*')
}

module.exports = {
  FileVersion: {
    async owner(bookComponentFile) {
      return await User.query().findOne({
        id: bookComponentFile.ownerId,
      })
    },
    async relatedBookComponentFile({ category, bcfId: sourceId }) {
      if (category !== 'source') return []

      const related = await SourceConvertedFile.query().where({
        sourceId,
      })

      return related.map(r => r.convertedId)
    },
  },
  Mutation: {
    updateTagFile,
    reconvertBookComponent,
  },
}
