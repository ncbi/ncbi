const graphql = require('../graphqlLoaderUtil')
const resolvers = require('./FileVersion.resolvers')

module.exports = {
  resolvers,
  typeDefs: graphql('fileVersion/fileVersion.graphql'),
}
