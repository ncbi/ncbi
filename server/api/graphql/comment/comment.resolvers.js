/* eslint-disable no-return-await */
const { pubsubManager } = require('pubsweet-server')

const { File, Issue } = require('@pubsweet/models')
const util = require('../util')
const ContentProccess = require('../../../services/ContentProccess')

const { getPubsub } = pubsubManager

// Fires immediately when the comment is created
const COMMENT_CREATED = 'COMMENT_CREATED'

const Comment = require('../../../models/comment/comment')
const notify = require('../../../services/notify')

const resolvers = {
  Comment: {
    files(comment, _, ctx) {
      return File.query().whereIn('id', comment.files) || []
    },
  },
  Mutation: {
    createComment: async (
      _,
      { issueId, content, type, files = [] },
      context,
    ) => {
      const pubsub = await getPubsub()

      const fileModels = await util.uploadStreamFile(files, 'comment', context)

      const savedComment = await new Comment({
        issueId,
        content,
        files: (fileModels || []).map(f => f.id),
        userId: context.user,
        type,
      }).save()

      const { objectId } = await Issue.query().findOne({ id: issueId })

      const { object } = await ContentProccess.findBestContent(objectId)

      const regex = new RegExp(
        /<span.*? class="mention-tag" id="([^"]*?)".*?>(.+?)<\/span>/gm,
      )

      const mentions = content.matchAll(regex)

      const mentionedIds = []

      // eslint-disable-next-line no-restricted-syntax
      for (const match of mentions) {
        mentionedIds.push(match[1])
      }

      notify('mentionVendorIssue', {
        title: object.title,
        mentionedIds,
        userId: context.user,
      })

      const comment = await Comment.query()
        .findById(savedComment.id)
        .eager('user')

      pubsub.publish(`${COMMENT_CREATED}.${issueId}`, savedComment.id)

      return comment
    },
  },
  Query: {
    getComment: async (_, { commentId }) => {
      Comment.find(commentId)
    },
    getComments: async (_, { issueId }) => {
      const comments = await Comment.query()
        .where('issueId', issueId)
        .eager('user')
        .orderBy('created', 'asc')

      return comments
    },
  },
  Subscription: {
    commentCreated: {
      resolve: async (commentId, _, context) => {
        const comment = await Comment.query().findById(commentId)
        return comment
      },
      subscribe: async (_, vars, context) => {
        const pubsub = await getPubsub()
        return pubsub.asyncIterator(`${COMMENT_CREATED}.${vars.issueId}`)
      },
    },
  },
}

module.exports = resolvers
