const graphql = require('../graphqlLoaderUtil')
const resolvers = require('./comment.resolvers')

module.exports = {
  resolvers,
  typeDefs: graphql('comment/comment.graphql'),
}
