/* eslint-disable global-require */

module.exports = {
  resolvers: require('./division.resolvers'),
  typeDefs: require('../graphqlLoaderUtil')('division/division.graphql'),
}
