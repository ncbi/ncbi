const { paginatedBookComponents } = require('../util')

const Division = require('../../../models/division/division')
const BookComponent = require('../../../models/bookComponent/bookComponent')

const getDivisionResolver = (_, { id }, ctx) => {
  return Division.query().findById(id).throwIfNotFound()
}

const fullBookComponents = async (division, args, ctx) => {
  return division.bookComponents
}

/**
 * Returns a nested tree structure representation of parts within a book.
 *
 * Because the levels of nesting is arbitrary and we don't know how many levels
 * to ask for client-side, we stringify the result on the server and parse it
 * on the client.
 */
const partsNested = async division => {
  const allParts = await BookComponent.query().where({
    bookId: division.bookId,
    componentType: 'part',
  })

  const findParts = ids => {
    const parts = ids
      .map(id => {
        return allParts.find(p => p.id === id)
      })
      // remove undefined values -- if undefined, it was a chapter, not a part
      .filter(Boolean)

    const withChildren = parts.map(part => {
      const children = findParts(part.bookComponents)

      return {
        id: part.id,
        title: part.title,
        children, // children parts
        bookComponents: part.bookComponents, // all child ids
      }
    })

    return withChildren
  }

  const result = findParts(division.bookComponents)
  const stringified = JSON.stringify(result)
  return stringified
}

const bookComponents = async (division, args, ctx) => {
  const divisionIdField = 'id'
  const useLoader = true // TODO - should be true for nested components
  return paginatedBookComponents(
    division,
    args,
    ctx,
    divisionIdField,
    useLoader,
  )
}

module.exports = {
  Query: {
    getDivision: getDivisionResolver,
  },
  Division: {
    fullBookComponents,
    bookComponents,
    partsNested,
  },
}
