/* eslint-disable no-await-in-loop */
/* eslint-disable no-return-await */
/* eslint-disable no-param-reassign */
// const { useTransaction } = require('@coko/server')

const { Toc, User, Book, Collection, Errors } = require('@pubsweet/models')

const { useTransaction } = require('@coko/server')

const { TocService } = require('../../../services')

const getToc = async (_, { id }) => Toc.query().findOne({ id })

const publishTocs = async (_, { toc }, ctx) => {
  const publishedTocs = []

  // eslint-disable-next-line no-plusplus
  for (let i = 0, len = toc.length; i < len; i++) {
    await useTransaction(async transaction => {
      const chapter = new TocService({
        id: toc[i],
        userId: ctx.user,
        transaction,
      })

      publishedTocs.push(await chapter.executeCommand('publishComponent'))
    })
  }

  return publishedTocs.filter(d => !!d)
}

module.exports = {
  Toc: {
    async canBePublished(toc) {
      return await toc.hasPublishedComponents()
    },
    async alias(toc) {
      return `bcms${toc.alias}`
    },
    async owner(toc) {
      return User.query().findOne({
        id: toc.ownerId,
      })
    },
    async book(toc) {
      return toc.bookId ? await Book.query().findOne({ id: toc.bookId }) : null
    },
    async collection(toc) {
      return toc.collectionId
        ? await Collection.query().findOne({ id: toc.collectionId })
        : null
    },
    async tocFiles(toc) {
      return await Toc.getFiles()
        .where({ 'file_versions.category': 'converted' })
        .for(toc.id)
        .orderByRaw('file_versions.version_name::int desc')
    },
    async errors(toc, _, ctx) {
      const errorslist = await Errors.query()
        .where({ objectId: toc.id, history: false })
        .orderBy('created')

      return errorslist
    },
  },
  Query: {
    getToc,
  },
  Mutation: {
    publishTocs,
  },
}
