const graphql = require('../graphqlLoaderUtil')
const resolvers = require('./toc.resolvers')

module.exports = {
  resolvers,
  typeDefs: graphql('toc/toc.graphql'),
}
