const graphql = require('../graphqlLoaderUtil')
const resolvers = require('./issue.resolvers')

module.exports = {
  resolvers,
  typeDefs: graphql('issue/issue.graphql'),
}
