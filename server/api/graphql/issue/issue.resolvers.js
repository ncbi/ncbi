/* eslint-disable no-await-in-loop */
/* eslint-disable no-return-await */
const { File, Comment, User } = require('@pubsweet/models')

const { SearchService } = require('../../../services')

const Issue = require('../../../models/issue/issue')
const util = require('../util')

const notify = require('../../../services/notify')
const ContentProccess = require('../../../services/ContentProccess')

const resolvers = {
  Issue: {
    files(issue, _, ctx) {
      return File.query().whereIn('id', issue.files) || []
    },
    comments(issue, _, ctx) {
      return (
        Comment.query().whereIn('issue_id', issue.id).orderBy('created') || []
      )
    },
    async user(issue, _, ctx) {
      return (await User.findById(issue.userId)) || {}
    },
  },
  Mutation: {
    updateIssue: async (_, { id, status }, context) => {
      await Issue.query().patch({ status }).whereIn('id', id)
      return await Issue.query().whereIn('id', id)
    },
    createIssue: async (
      _,
      { title, content, status = 'open', objectId, files = [] },
      context,
    ) => {
      const fileModels = await util.uploadStreamFile(files, 'issue', context)

      // todo: get users + roles from content, send email notification that they are mentioned
      // if role is mentioned send email to all users that have that role
      // list of roles that can be mentioned Sys Admins, PDF2XML Vendor,Org Admins
      // !Warning for: Org Admins - send email only to org admins of this organization
      // mentions should be implemented in create comments too

      const { object } = await ContentProccess.findBestContent(objectId)

      const regex = new RegExp(
        /<span.*? class="mention-tag" id="([^"]*?)".*?>(.+?)<\/span>/gm,
      )

      const mentions = content.matchAll(regex)

      const mentionedIds = []

      // eslint-disable-next-line no-restricted-syntax
      for (const match of mentions) {
        mentionedIds.push(match[1])
      }

      notify('mentionVendorIssue', {
        title: object.title,
        mentionedIds,
        userId: context.user,
      })

      const savedIssue = await new Issue({
        title,
        content,
        objectId,
        status,
        files: (fileModels || []).map(f => f.id),

        userId: context.user,
      }).save()

      const issue = await Issue.query().findById(savedIssue.id)

      return issue
    },
  },
  Query: {
    getIssuesDatatable: async (_, args, ctx) => {
      // eslint-disable-next-line no-unused-vars
      const { input } = args

      const searchedissues = new SearchService(Issue, {
        ...input,
        filter: args.input.search.criteria,
      })

      const results = await searchedissues.search()

      return {
        metadata: searchedissues.metadata,
        results,
      }
    },
  },
}

module.exports = resolvers
