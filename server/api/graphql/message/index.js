/* eslint-disable global-require */

module.exports = {
  resolvers: require('./message.resolvers'),
  typeDefs: require('../graphqlLoaderUtil')('message/message.graphql'),
}
