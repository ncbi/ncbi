/* eslint-disable no-return-await */
const { pubsubManager } = require('pubsweet-server')

const { File, Channel } = require('@pubsweet/models')
const ContentProccess = require('../../../services/ContentProccess')

const {
  ReviewService,
  ParseMessage,
  StatusService,
} = require('../../../services')

const util = require('../util')

const { getPubsub } = pubsubManager

// Fires immediately when the message is created
const MESSAGE_CREATED = 'MESSAGE_CREATED'

const Message = require('../../../models/message/message')
const notify = require('../../../services/notify')

const resolvers = {
  Message: {
    files(message, _, ctx) {
      return File.query().whereIn('id', message.files) || []
    },
  },
  Mutation: {
    createMessage: async (
      _,
      { content, channelId, files = [], type },
      context,
    ) => {
      const pubsub = await getPubsub()

      const fileModels = await util.uploadStreamFile(files, 'comment', context)

      const { objectId } = await Channel.query().findOne({
        id: channelId,
      })

      const { model: Model, object } = await ContentProccess.findBestContent(
        objectId,
      )

      await Promise.all(
        fileModels.map(
          async savedFile =>
            await Model.relatedQuery('files').for(object).insert({
              bookComponentVersionId: null,
              category: 'review',
              fileId: savedFile.id,
              parentId: null,
              ownerId: context.user,
              versionName: '1',
            }),
        ),
      )

      const savedMessage = await new Message({
        channelId,
        content,
        files: (fileModels || []).map(f => f.id),
        type,
        userId: context.user,
      }).save()

      // Parse Mentioned Users from Message
      const parseMessage = new ParseMessage(savedMessage)
      const parsedMessage = await parseMessage.getData()

      notify('mention', {
        objectTitle: object.title,
        mentionedIds: parseMessage.mentionsIds,
        userId: context.user,
      })

      // Do Review
      const review = new ReviewService(parsedMessage)
      await review.doReview(context.user)

      let updatedBookComponent

      if (type !== 'comment' && type !== 'requestChanges') {
        // Change Status
        const entity = await Model.query().findOne({
          id: parsedMessage.channel.objectId,
        })

        await StatusService.update({
          entity,
          status: type,
          userId: context.user,
        })
      }

      if (type === 'in-review') {
        notify('reviewRequested', {
          objectId: object.id,
          objectTitle: object.title,
          userId: context.user,
        })
      }

      if (type === 'requestChanges') {
        notify('changesRequested', {
          objectId: object.id,
          objectTitle: object.title,
          userId: context.user,
        })
      }

      if (type === 'approve') {
        const isFinalApproval =
          updatedBookComponent && updatedBookComponent.status === 'approve'

        notify('reviewApproval', {
          objectId: object.id,
          objectTitle: object.title,
          userId: context.user,
          final: isFinalApproval,
        })
      }

      const message = await Message.query()
        .findById(savedMessage.id)
        .eager('user')

      pubsub.publish(`${MESSAGE_CREATED}.${channelId}`, message.id)

      return message
    },
  },
  Query: {
    message: async (_, { messageId }) => {
      Message.find(messageId)
    },
    messages: async (_, { channelId, first = 20, before }) => {
      let messagesQuery = Message.query()
        .where({ channelId })
        .eager('user')
        .limit(first)
        .orderBy('created', 'desc')

      if (before) {
        const firstMessage = await Message.query().findById(before)

        messagesQuery = messagesQuery.where(
          'created',
          '<',
          firstMessage.created,
        )
      }

      const messages = (await messagesQuery).reverse()
      const total = await messagesQuery.resultSize()

      return {
        edges: messages,
        pageInfo: {
          hasPreviousPage: total > first,
          startCursor: messages[0] && messages[0].id,
        },
      }
    },
  },
  Subscription: {
    messageCreated: {
      resolve: async (messageId, _, context) => {
        const message = await Message.query().findById(messageId).eager('user')
        return message
      },
      subscribe: async (_, vars, context) => {
        const pubsub = await getPubsub()
        return pubsub.asyncIterator(`${MESSAGE_CREATED}.${vars.channelId}`)
      },
    },
  },
}

module.exports = resolvers
