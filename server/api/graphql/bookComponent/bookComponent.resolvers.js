/* eslint-disable no-plusplus */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-return-await */
/* eslint-disable no-param-reassign */
const cheerio = require('cheerio')
const { UserInputError } = require('apollo-server-express')
const { raw } = require('objection')

const { flatten, uniq, get, orderBy, isEmpty, remove } = require('lodash')

const moment = require('moment-timezone')
const config = require('config')

const {
  getPubsub,
  asyncIterators,
} = require('pubsweet-server/src/graphql/pubsub')

const { useTransaction, logger } = require('@coko/server')

const {
  Book,
  BookComponent,
  FileVersion,
  Channel,
  Collection,
  Division,
  File,
  User,
  Errors,
  NcbiNotificationMessage,
} = require('@pubsweet/models')

const util = require('../util')
const { getNewAlias } = require('../../../_helpers')

const { paginatedBookComponents } = require('../util')
const ParseUploadedFile = require('../../../services/handleNcbiMessage/parseUploadedFile')

const {
  Chapter,
  PackageCollection,
  SearchService,
  FileService,
  Domain,
  ContentProccess,
} = require('../../../services')

const BookMeta = require('../../../services/xmlBaseModel/builder/bookMeta')

const XmlFactory = require('../../../services/xmlBaseModel/xmlFactory')

const DivisionService = require('../../../services/division/divisionService')
const OrderService = require('../../../services/orderService/orderService')

const { ON_UPLOAD_PROGRESS } = asyncIterators

const CONVERSION_COMPLETED = 'CONVERSION_COMPLETED'

const getBookComponents = async (_, { bookId }, ctx) => {
  const bookComponent = await BookComponent.query().where({ bookId })
  return bookComponent
}

const getBookComponentsById = async (_, { ids }) => {
  return BookComponent.query().whereIn('id', ids)
}

const searchBookComponents = async (_, { input }, ctx) => {
  const filter = get(input, 'search.criteria', [])
  let search = get(remove(filter, { field: 'search' }), '0.operator', {})
  const searchOperator = get(Object.keys(search), '0', 'containsAny')

  search = get(search, searchOperator, '').trim().toLowerCase()

  if (!isEmpty(search)) {
    if (searchOperator !== 'containsAny') {
      throw new Error("'search' is required to use the 'containsAny' operator")
    }

    const orCondition = [
      {
        // Remove all xml tags from title
        field: raw("regexp_replace(title, '<[a-zA-z/][^>]+>', ' ', 'g')"),
        operator: { containsAny: search },
      },
      {
        field: raw("metadata->>'sub_title'"),
        operator: { containsAny: search },
      },
      {
        // Match any search words found in tokenised filename
        field: raw(
          "regexp_replace(metadata->>'filename', '[^a-zA-Z]', ' ', 'g')",
        ),
        operator: { containsAny: search },
      },
      {
        // Match the full or partial (untokenised) filename
        field: raw("metadata->>'filename'"),
        operator: { contains: search },
      },
    ]

    // Identify bcms ids to be compaired to "BookComponent.alias"
    Array.from(
      search.matchAll(/(?<=^|[\s&|])bcms[0-9]+(\.[0-9]*)?(?=$|[\s&|])/g),
    ).forEach(bcmsMatch => {
      let token = bcmsMatch[0].slice(4, bcmsMatch[0].length)

      if (bcmsMatch[1] === undefined) {
        // For containsAny, bcms id requires the full code up to the "."; this
        // bcms id does not include a "." so add it to the end
        token = `${token}.`
      }

      orCondition.push({ field: 'alias', operator: { beginsWith: token } })
    })

    filter.push({ or: orCondition })
  }

  const convertDateToServerTimezone = value => {
    const sourceTimezoneMatch = value.match(/([-+]\d{1,2}):(\d{2})$/)

    if (sourceTimezoneMatch) {
      const sourceTimezoneOffset = `${sourceTimezoneMatch[1]}:${sourceTimezoneMatch[2]}`

      // Extract the date and time part without the timezone offset
      const dateAndTimePart = value.substring(
        0,
        value.lastIndexOf(sourceTimezoneOffset),
      )

      const sourceTimezone = moment.tz.zone(sourceTimezoneOffset)

      if (sourceTimezone) {
        // Parse the date and time part into a moment object
        const inputMoment = moment.tz(
          dateAndTimePart,
          'YYYY-MM-DDTHH:mm:ss.SSS',
          sourceTimezone.name,
        )

        // Apply the source timezone offset
        inputMoment.utcOffset(sourceTimezoneOffset)

        // Specify the target timezone (America/New_York)
        const targetTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone
        // Convert the moment to the target timezone
        const convertedMoment = inputMoment.clone().tz(targetTimezone)

        // Format the converted moment as a string (in the target timezone)
        return convertedMoment.format()
      }

      return value
    }

    return value
  }

  const searchedBookComponents = new SearchService(
    BookComponent.query().where({ parent_id: null }),
    {
      // Override search/sort settings with request preferences
      ...(input || {}),
      filter,
      converters: {
        updated: convertDateToServerTimezone,
        publishedDate: convertDateToServerTimezone,
      },
    },
  )

  const results = await searchedBookComponents.search()

  return {
    metadata: searchedBookComponents.metadata,
    results,
  }
}

const getBookComponent = async (_, { id }, ctx) =>
  BookComponent.query().findOne({ bookComponentVersionId: id })

const getBookComponentById = async (_, { id }, ctx) =>
  BookComponent.query().findById(id).throwIfNotFound()

const createNewPublishedVersion = async (_, { id }, context) => {
  const bookComponent = await BookComponent.query().findOne({ id })

  const chapter = new Chapter({ bookComponent, ownerId: context.user })

  return chapter.executeCommand('newVersion')
}

const getChaptersOverview = async (_, args, ctx) => {
  const searchedChapters = new SearchService(BookComponent, {
    filter: get(args, 'input.search.criteria'),
    sort: get(args, 'input.sort'),
  })

  let results = await searchedChapters.search()

  const searchedBooks = new SearchService(Book, {
    filter: get(args, 'input.search.criteria'),
    sort: get(args, 'input.sort'),
  })

  let resultBooks = await searchedBooks.search()

  resultBooks = resultBooks.filter(
    b => b.settings.chapterIndependently !== true,
  )

  results = results.concat(resultBooks)

  // eslint-disable-next-line operator-assignment
  searchedChapters.metadata.total =
    searchedChapters.metadata.total + searchedBooks.metadata.total

  results = orderBy(results, ['updated'], ['desc'])

  const now = moment(new Date())

  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < results.length; i++) {
    const chapter = results[i]

    const book = await Book.query().findOne({
      id: chapter.bookId || chapter.id,
    })

    chapter.organisationId = book.organisationId

    chapter.notification = await NcbiNotificationMessage.query()
      .findOne({
        objectId: chapter.id,
      })
      .orderByRaw('"ncbi_notification_messages"."created" desc')

    if (chapter.notification) {
      const timeToExpire = moment(chapter.notification.updated).add(4, 'hours')

      const duration = moment.duration(now.diff(timeToExpire))
      chapter.notification.timeout = duration.asHours()

      chapter.notification.options = JSON.stringify(
        chapter.notification.options,
      )
    }
  }

  return {
    metadata: searchedChapters.metadata,
    results,
  }
}

const deleteFile = async (_, { versionName, parentId }) => {
  const deletedBookComponentFile = await FileVersion.query()
    .skipUndefined()
    .delete()
    .where({
      parentId,
      versionName,
    })
    .returning('*')

  Promise.all(
    deletedBookComponentFile.map(
      async deleted => await File.query().deleteById(deleted.fileId),
    ),
  )
}

const uploadFile = async (_, { files, id, category }, context) => {
  const contentProccess = await ContentProccess.findBestContent(id)

  let dbModels = []

  try {
    await util.validateUpload(contentProccess)
    dbModels = await util.uploadStreamFile(files, category, context)
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e)
    throw new UserInputError(e)
  }

  logger.info(
    `Files were uploaded: [${dbModels.map(f => f.name)}] by user ${
      context.user
    } on category ${category} on Model ${contentProccess.object.id}, bcms${
      contentProccess.object.alias
    }`,
  )

  const uploaded = await Promise.all(
    dbModels.map(
      async data =>
        await contentProccess.object.addFile(
          { ...data, ownerId: context.user },
          category,
          'new-upload',
        ),
    ),
  )

  return dbModels.map(data => {
    const bookComponentFile = uploaded.find(bcFile => bcFile.fileId === data.id)

    return {
      bcfId: bookComponentFile.id,
      created: data.created,
      id: data.id,
      mimeType: data.mimeType,
      name: data.name,
      parentId: bookComponentFile.parentId,
      updated: data.updated,
      versionName: bookComponentFile.versionName,
      ownerId: context.user,
    }
  })
}

const createCollectionMeta = async book => {
  const content = await XmlFactory.createTocXml(book.id)

  const tocObject = cheerio.load(content, {
    xmlMode: true,
    decodeEntities: false,
    xml: {
      decodeEntities: false,
    },
  })

  const collectionMeta = []

  tocObject('*')
    .find('collection-meta')
    .each((i, elem) => {
      collectionMeta[i] = `<collection-meta collection-type="${
        elem.attribs['collection-type']
      }">${tocObject(elem).html()}</collection-meta>`
    })

  const collectionMetaContent = collectionMeta.join('')

  return collectionMetaContent
}

const uploadConvertFiles = async (
  _,
  { files, id: bookId, versionName, category },
  context,
) => {
  const dbFileModels = await util.uploadStreamFile(files, category, context)

  return await useTransaction(async trx => {
    let book = await Book.query(trx).findOne({ id: bookId })

    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < dbFileModels.length; i++) {
      let createdBookComponent = null

      if (
        book.settings &&
        book.settings.chapterIndependently === false &&
        book.workflow !== 'word'
      ) {
        const { data } = await Domain.createVersion({
          bookId,
          filename: dbFileModels[i].name,
          fileId: dbFileModels[i].id,
          ownerId: context.user,
          category,
          transaction: trx,
        })

        createdBookComponent = data
      } else {
        const { data } = await Chapter.createVersion({
          bookId,
          versionName,
          componentType: 'chapter',
          filename: dbFileModels[i].name,
          fileId: dbFileModels[i].id,
          ownerId: context.user,
          category,
          transaction: trx,
        })

        createdBookComponent = data
      }

      try {
        const parseFile = new ParseUploadedFile()

        await parseFile.handle({
          data: {
            transaction: trx,
            files: dbFileModels[i],
            category,
            object: createdBookComponent,
          },
        })
      } catch (e) {
        throw new Error(
          `The converted file you uploaded is not correct OR parsing failed with error: ${e} `,
        )
      }
    }

    book = await book.getDivisions(trx)

    let bookComponentsDiv = flatten(
      book.divisions.map(division => division.bookComponents),
    )

    bookComponentsDiv = bookComponentsDiv.slice(0, 50)

    return { ...book, bookComponents: bookComponentsDiv }
  })
}

// This part needs refactoring to add a submitComponent command for Books and Chapters
// independently
const submitBookComponents = async (_, { bookComponents }, context) => {
  const user = await User.query().findById(context.user)
  const isSysAdmin = user.hasGlobalRole('sysAdmin')

  const bcFileMapper = {}

  const dbFileModels = await Promise.all(
    bookComponents.map(async bc => {
      const contentProccess = await ContentProccess.findBestContent(bc)
      const book = await contentProccess.getBook()
      const { model: Model, object } = contentProccess

      let bcf = await Model.latestComponentFiles(object, 'source')

      if (
        bcf &&
        bcf.some(f => f.status !== 'new-upload' && f.status !== 'failed') &&
        book.workflow !== 'pdf' &&
        !isSysAdmin
      ) {
        return null
      }

      if (book.workflow === 'pdf') {
        bcf = bcf.concat(await Model.latestComponentFiles(object, 'support'))

        bcf = bcf.concat(await Model.latestComponentFiles(object, 'supplement'))

        bcf = bcf.concat(await Model.latestComponentFiles(object, 'image'))

        bcf = bcf.concat(await Model.latestComponentFiles(object, 'converted'))

        const [metadataFile] = await Model.latestComponentFiles(
          object,
          'metadata',
        )

        const bookWithAppliedValues = await book.getBookWithAppliedCollectionValues()

        const bookMeta = new BookMeta(
          {
            ...bookWithAppliedValues,
            abstractGraphic: await File.query().findOne({
              id: bookWithAppliedValues.fileAbstract,
            }),
            collection: await Collection.query().findOne({
              id: book.collectionId,
            }),
          },
          {
            isCollection: false,
          },
        )

        const bookMetada = bookMeta.build()

        const collectionMetadata = await createCollectionMeta(
          bookWithAppliedValues,
        )

        const content = `${collectionMetadata}${bookMetada}`

        if (metadataFile) {
          await FileService.update(metadataFile.file.id, {
            content,
            filename: metadataFile.file.name,
          })
        } else {
          const file = await FileService.write({
            content,
            filename: 'metadata.xml',
            category: 'metadata',
          })

          file.bookComponentId =
            contentProccess.type === 'wholeBook' ? object.id : null

          file.bookId =
            contentProccess.type === 'chapterProccessed' ? object.id : null

          await object.addFile(file, 'metadata', 'new-upload')
        }

        bcf = bcf.concat(await Model.latestComponentFiles(object, 'metadata'))
      }

      const fileIds = bcf.map(bcVer => {
        bcFileMapper[bcVer.fileId] = object.id
        return bcVer.fileId
      })

      const file = await File.query().whereIn('id', fileIds)
      return file
    }),
  )

  if (flatten(dbFileModels).filter(f => !!f).length === 0) {
    throw new UserInputError('There are no files to sent for Conversion')
  }

  const contentProccess = await ContentProccess.findBestContent(
    bookComponents[0],
  )

  const book = await contentProccess.getBook()

  let action = ''
  let options = {}

  if (book.workflow === 'word') {
    action = 'wordConversion'
  } else if (book.workflow === 'xml') {
    action = !book.settings.chapterIndependently
      ? 'wholeBookXmlConversion'
      : 'xmlConversion'

    if (!book.settings.chapterIndependently) {
      const bookWithAppliedValues = await book.getBookWithAppliedCollectionValues()
      const bookCollection = await book.getCollection()

      options = {
        collection_id: (bookCollection.collection || {}).domain || null,
        collection_title: (bookCollection.collection || {}).title || null,
        publisher_name: bookWithAppliedValues.metadata.pub_name,
        publisher_loc: bookWithAppliedValues.metadata.pub_loc,
        source_type: bookWithAppliedValues.metadata.sourceType,
      }
    }
  } else if (book.workflow === 'pdf') {
    action = !book.settings.chapterIndependently
      ? 'wholeBookPdfConversion'
      : 'pdfConversion'
  }

  if (['wholeBookPdfConversion', 'wholeBookXmlConversion'].includes(action)) {
    options = {
      ...options,
      version_name: book.fundedContentType,
      version_number: book.version,
    }
  }

  const citationTypeId = config.get('bookSettings.citationTypeIds')[
    book.settings.citationType
  ]

  await Promise.all(
    dbFileModels.map(fileModel =>
      useTransaction(async trx => {
        const objectId = bcFileMapper[fileModel[0].id]
        return await PackageCollection.sendForConversion(
          action,
          fileModel,
          {
            domain: book.domain,
            citation_type: citationTypeId,
            ...options,
          },
          {
            objectId,
            pushToQueue: 'submit-queue',
            userId: context.user,
          },
          trx,
        )
      }),
    ),
  )

  return true
}

const updateBookComponent = async (_, { id, input }, ctx) => {
  let bookComponent = await BookComponent.query().findOne({
    bookComponentVersionId: id,
  })

  if (input.metadata) {
    input.metadata = { ...bookComponent.metadata, ...input.metadata }
  }

  bookComponent = await bookComponent.$query().patchAndFetch(input)

  // if update bookComponent date created and dated updated needs to reorder bookComponents
  const division = await Division.query().findOne({
    bookId: bookComponent.bookId,
    label: 'body',
  })

  await OrderService.model(division, {
    exclude: itm => itm.componentType === 'part',
    deep: true,
  })

  const domains = []

  if (bookComponent.componentType === 'part') {
    const bookComponents = await bookComponent.hasPublishedChildren()

    if (bookComponents && bookComponents.length > 0) {
      domains.push(bookComponent.bookId)
    }
  }

  ctx.connectors.Events.model.notify('BookComponentUpdated', {
    data: bookComponent,
    domains,
  })

  return bookComponent
}

const updateBookComponents = async (_, { bookComponents }, ctx) =>
  await Promise.all(
    bookComponents.map(async component => {
      const { id, ...updateFields } = component

      const book = await BookComponent.query()
        .patch(updateFields)
        .findOne({ id })
        .returning('*')

      return book
    }),
  )

const updateBookComponentOrder = async (
  _,
  { divisionId, index = 0, ids, partId = null, bookId },
  ctx,
) => {
  // Clear divisions of bookComponents
  const divisions = await Division.query().where({ bookId })

  const divisionBookComponents = divisions.map(division => {
    division.bookComponents = division.bookComponents.filter(
      bc => !ids.includes(bc),
    )

    if (division.id === divisionId && partId === null) {
      division.bookComponents.splice(index, 0, ...ids)
    }

    return { id: division.id, bookComponents: division.bookComponents }
  })

  await Promise.all(
    divisionBookComponents.map(async div => {
      return await Division.query()
        .patch({ bookComponents: div.bookComponents })
        .findOne({ id: div.id })
    }),
  )

  const allBookComponent = await BookComponent.query().where({ bookId })

  const bookComponentsOrder = allBookComponent
    .map(bc => {
      const previousLength = bc.bookComponents.length
      bc.bookComponents = bc.bookComponents.filter(el => !ids.includes(el))

      if (previousLength > bc.bookComponents.length) {
        return {
          id: bc.id,
          bookComponents: bc.bookComponents,
        }
      }

      return null
    })
    .filter(bc => bc != null)

  // remove bookcomponents ids from nested previous book components (parts)
  await Promise.all(
    bookComponentsOrder.map(async bc => {
      return await BookComponent.query()
        .patch({ bookComponents: bc.bookComponents })
        .findOne({ id: bc.id })
    }),
  )

  if (partId) {
    const bookPart = await BookComponent.query().findOne({ id: partId })
    bookPart.bookComponents.splice(index, 0, ...ids)

    await BookComponent.query()
      .patch({ bookComponents: uniq(bookPart.bookComponents) })
      .findOne({ id: partId })

    await OrderService.model(bookPart, {
      exclude: itm => itm.componentType === 'part',
    })
  }

  await BookComponent.query().patch({ divisionId }).whereIn('id', ids)

  const book = await Book.query().findOne({ id: bookId })

  ctx.connectors.Events.model.notify('BookComponentOrderUpdated', {
    domains: [book.id],
  })

  const division = await Division.query().findOne({ bookId, label: 'body' })

  await OrderService.model(division, {
    exclude: itm => itm.componentType === 'part',
    deep: true,
  })

  return book
}

const repeatBookComponent = async (
  _,
  { componentId, partIds = [], bookId },
  ctx,
) => {
  // eslint-disable-next-line no-useless-catch
  try {
    const book = await Book.query().findOne({ id: bookId })

    const index = 0

    // 1. remove book component from previous parts if it is not in the partIds
    const bookComponents = await BookComponent.query().where({
      bookId,
    })

    const parentBookComponents = bookComponents.filter(bc =>
      bc.bookComponents?.includes(componentId),
    )

    if (
      parentBookComponents.length > 1 ||
      !partIds.includes(parentBookComponents.id)
    ) {
      // remove book components ids from nested previous parts
      await Promise.all(
        parentBookComponents.map(async bc => {
          const filteredComponents = bc.bookComponents

          const indexToRemove = filteredComponents.findIndex(
            x => x === componentId,
          )

          filteredComponents.splice(indexToRemove, 1)
          return await BookComponent.query()
            .patch({ bookComponents: filteredComponents })
            .findOne({ id: bc.id })
        }),
      )
    }

    // 2. add book component in the parts of parts ids
    if (partIds) {
      const targetParts = await BookComponent.query().whereIn('id', partIds)

      await Promise.all(
        targetParts.map(async target => {
          target.bookComponents.splice(index, 0, componentId)

          await BookComponent.query()
            .patch({ bookComponents: target.bookComponents })
            .findOne({ id: target.id })

          return await OrderService.model(target, {
            exclude: itm => itm.componentType === 'part',
          })
        }),
      )
    }

    const division = await Division.query().findOne({ bookId, label: 'body' })

    await OrderService.model(division, {
      exclude: itm => itm.componentType === 'part',
      deep: true,
    })

    ctx.connectors.Events.model.notify('RepeatBookComponent', {
      domains: [book.id],
    })

    return book
  } catch (e) {
    throw e
  }
}

const publishBookComponents = async (_, { bookComponent }, ctx) => {
  const publishedBookComponents = []

  // eslint-disable-next-line no-plusplus
  for (let i = 0, len = bookComponent.length; i < len; i++) {
    const { command: Command, object } = await ContentProccess.findBestContent(
      bookComponent[i],
    )

    await useTransaction(async transaction => {
      const command = new Command({
        id: object.id,
        userId: ctx.user,
        transaction,
      })

      const published = await command.executeCommand('publishComponent')

      if (published) {
        publishedBookComponents.push({
          id: published.id,
          status: published.status,
        })
      }
    })
  }

  return publishedBookComponents
}

const retry = async (_, { id }, ctx) => {
  // eslint-disable-next-line no-plusplus
  for (let i = 0, len = id.length; i < len; i++) {
    // eslint-disable-next-line no-shadow
    const { retry } = await NcbiNotificationMessage.query().findOne({
      objectId: id[i],
    })

    await NcbiNotificationMessage.query()
      .patch({ retry: parseInt(retry || 0, 10) + 1 })
      .findOne({
        objectId: id[i],
      })
      .orderBy('created', 'desc')
  }

  return []
}

const addBookComponent = async (_, { id: bookId, input }, context) => {
  const division = await Division.query().findOne({ bookId, label: 'body' })

  const createdBookComponent = await BookComponent.query().insert({
    abstract: input.content,
    bookId,
    componentType: 'part',
    divisionId: division.id,
    metadata: {
      editor: input.metadata.editor,
    },
    ownerId: context.user,
    title: input.title,
    alias: await getNewAlias({}),
  })

  const divisionService = await DivisionService.getDivision({
    bookId,
    label: 'body',
  })

  await divisionService.addBookComponents([createdBookComponent])

  return createdBookComponent
}

const getNextPrevious = async (_, { id, partId }, context) => {
  const bookComponent = await BookComponent.query().findOne({
    bookComponentVersionId: id,
  })

  const divisions = await Division.query()
    .where({ bookId: bookComponent.bookId })
    .orderByRaw(
      `array_position(array['${Division.DivisionConfig.join(
        "'::text,'",
      )}'::text], label)`,
    )

  const parts = await BookComponent.query().where({
    componentType: 'part',
    bookId: bookComponent.bookId,
  })

  const rootBookComponents = flatten(divisions.map(div => div.bookComponents))

  const orderedArray = []

  const searchChildren = (bc, pId) => {
    orderedArray.push({ id: bc, partId: pId })
    const foundChildren = parts.find(p => p.id === bc)

    if (foundChildren) {
      for (let i = 0; i < foundChildren.bookComponents.length; i++) {
        searchChildren(foundChildren.bookComponents[i], foundChildren.id)
      }
    }
  }

  rootBookComponents.map(bc => searchChildren(bc, null))

  let index = orderedArray.findIndex(bc => bc.id === bookComponent.id)

  if (partId) {
    const partIndex = orderedArray.findIndex(bc => bc.id === partId)

    index = orderedArray.findIndex(
      (item, idx) => item.id === bookComponent.id && idx >= partIndex,
    )
  }

  let previousBookComponent = null
  let nextBookComponent = null

  if (orderedArray[index + 1]) {
    const next = await BookComponent.query().findOne({
      id: orderedArray[index + 1].id,
    })

    nextBookComponent = orderedArray[index + 1]
    nextBookComponent.id = next.bookComponentVersionId
  }

  if (orderedArray[index - 1]) {
    const previous = await BookComponent.query().findOne({
      id: orderedArray[index - 1].id,
    })

    previousBookComponent = orderedArray[index - 1]
    previousBookComponent.id = previous.bookComponentVersionId
  }

  return [previousBookComponent, nextBookComponent]
}

const moveInsidePart = async (_, { input }) => {
  const { componentIds, targetId } = input

  const componentsToMove = await Promise.all(
    componentIds.map(async id => BookComponent.query().findById(id)),
  )

  const { bookId } = componentsToMove[0]

  const targetComponent = await BookComponent.query().findById(targetId)

  if (targetComponent) {
    const updatedComponents = [
      ...componentIds,
      ...targetComponent.bookComponents,
    ]

    const uniqueComponents = updatedComponents.filter((value, index, array) => {
      if (componentIds.includes(value)) {
        return array.indexOf(value) === index
      }

      return true
    })

    await targetComponent.$query().patch({
      bookComponents: uniqueComponents,
    })
  }

  const Divisions = await Division.query()
    .findOne({ bookId })
    .where('label', 'body')

  await Promise.all(
    componentIds.map(async id => {
      const indexToRemove = Divisions.bookComponents.indexOf(id)

      if (indexToRemove !== -1) {
        Divisions.bookComponents.splice(indexToRemove, 1)

        await Division.query().patch(Divisions).findById(Divisions.id)
      }
    }),
  )

  const bookComponents = await BookComponent.query().where({
    bookId,
    componentType: 'part',
  })

  await Promise.all(
    componentIds.map(async id => {
      const findAllComponentsWithcomponentIdExcludeTargetId = (
        bookComponentsWithParts,
        componentId,
        // eslint-disable-next-line no-shadow
        targetId,
      ) => {
        const matchingComponents = []

        bookComponentsWithParts.map(async component => {
          if (
            component.id !== targetId &&
            component.bookComponents &&
            component.bookComponents.includes(componentId)
          ) {
            matchingComponents.push(component)
          }
        })

        return matchingComponents
      }

      const foundComponents = findAllComponentsWithcomponentIdExcludeTargetId(
        bookComponents,
        id,
        targetId,
      )

      foundComponents.map(async component => {
        await Promise.all(
          // eslint-disable-next-line no-shadow
          componentIds.map(async id => {
            const indexToRemove = component.bookComponents.indexOf(id)

            if (indexToRemove !== -1) {
              component.bookComponents.splice(indexToRemove, 1)

              await BookComponent.query()
                .patch(component)
                .findById(component.id)
            }
          }),
        )
      })
    }),
  )

  return componentsToMove
}

const moveComponent = async (
  componentIds,
  targetId,
  direction = 'above',
  partId,
) => {
  const componentsToMove = await Promise.all(
    componentIds.map(async id => BookComponent.query().findById(id)),
  )

  const { bookId } = componentsToMove[0]

  const parentDivision = await Division.query().findById(
    componentsToMove[0].divisionId,
  )

  const bookComponentsWithParts = await BookComponent.query().where({
    bookId,
    componentType: 'part',
  })

  const targetIdExistInPart = []

  bookComponentsWithParts.map(async component => {
    const targetIdExist = component.bookComponents.includes(targetId)

    if (targetIdExist) {
      targetIdExistInPart.push(component)
    }
  })

  const deleteComponent = async () => {
    await Promise.all(
      componentIds.map(async id => {
        const componentIdExistsInDivision = parentDivision.bookComponents.includes(
          id,
        )

        if (componentIdExistsInDivision) {
          const indexOfComponentId = parentDivision.bookComponents.indexOf(id)

          parentDivision.bookComponents.splice(indexOfComponentId, 1)

          await parentDivision.$query().patch({
            bookComponents: parentDivision.bookComponents,
          })
        } else {
          bookComponentsWithParts.map(async component => {
            const componentIdExistsInBookComponent = component.bookComponents.includes(
              id,
            )

            if (componentIdExistsInBookComponent) {
              const indexOfComponentId = component.bookComponents.indexOf(id)

              component.bookComponents.splice(indexOfComponentId, 1)

              await component.$query().patch({
                bookComponents: component.bookComponents,
              })
            }
          })
        }
      }),
    )
  }

  if (targetIdExistInPart.length > 1 && !partId) {
    throw new Error('partId is missing')
  }

  await deleteComponent()

  const targetIdExistsInDivision = parentDivision.bookComponents.includes(
    targetId,
  )

  if (targetIdExistsInDivision && !partId) {
    const indexOfTargetId = parentDivision.bookComponents.indexOf(targetId)

    if (direction === 'above') {
      componentIds.map(id =>
        parentDivision.bookComponents.splice(indexOfTargetId, 0, id),
      )
    } else {
      componentIds.map(id =>
        parentDivision.bookComponents.splice(indexOfTargetId + 1, 0, id),
      )
    }

    const unique = parentDivision.bookComponents.filter(
      (value, index, array) => array.indexOf(value) === index,
    )

    await parentDivision.$query().patch({
      bookComponents: unique,
    })
  } else {
    // eslint-disable-next-line no-shadow
    const updateBookComponent = async component => {
      const indexOfTargetId = component.bookComponents.indexOf(targetId)

      if (direction === 'above') {
        componentIds.map(id =>
          component.bookComponents.splice(indexOfTargetId, 0, id),
        )
      } else {
        componentIds.map(id =>
          component.bookComponents.splice(indexOfTargetId + 1, 0, id),
        )
      }

      const uniqueComponents = component.bookComponents.filter(
        (value, index, array) => {
          if (componentIds.includes(value)) {
            return array.indexOf(value) === index
          }

          return true
        },
      )

      await component.$query().patch({
        bookComponents: uniqueComponents,
      })
    }

    if (targetIdExistInPart.length > 1) {
      if (!partId) {
        throw new Error('partId is required')
      }

      targetIdExistInPart.map(async component => {
        const targetBookComponent = component.id === partId

        if (targetBookComponent) {
          await updateBookComponent(component)
        }
      })
    } else {
      bookComponentsWithParts.map(async component => {
        const targetIdExistsInBookComponent = component.bookComponents.includes(
          targetId,
        )

        if (targetIdExistsInBookComponent) {
          await updateBookComponent(component)
        }
      })
    }
  }

  return componentsToMove
}

const moveBelow = async (_, { input }, context) => {
  const { componentIds, targetId, partId } = input

  const componentsToMove = await moveComponent(
    componentIds,
    targetId,
    'below',
    partId,
  )

  return componentsToMove
}

const moveAbove = async (_, { input }, context) => {
  const { componentIds, targetId, partId } = input

  const componentsToMove = await moveComponent(
    componentIds,
    targetId,
    'above',
    partId,
  )

  return componentsToMove
}

// TODO - optimise
// This is not optimal for deeply nested BookComponent queries but works
// for the current case where we only load one level at a time
const loadChildrenPreloadCousins = async (bookComponent, loader) => {
  if (bookComponent.activeSiblingIds) {
    // Pre-load ALL sibling issues to minimise database queries
    await loader.loadMany(bookComponent.activeSiblingIds)
  }

  const objectList = await loader.load(bookComponent.id)

  return objectList || []
}

module.exports = {
  BookComponent: {
    async alias(bookComponent) {
      if (isEmpty(bookComponent.alias)) return ''
      return `bcms${bookComponent.alias}`
    },
    async bookComponents(bookComponent, args, ctx) {
      const divisionIdField = 'divisionId'
      const useLoader = false // TODO - should be true for nested components

      return await paginatedBookComponents(
        bookComponent,
        args,
        ctx,
        divisionIdField,
        useLoader,
      )
    },
    async isDuplicate(bookComponent, _, ctx) {
      const parts = await ctx.loaders.Book.getParts.load(bookComponent.bookId)

      let bookComponentsFlat = []

      parts.forEach(bc => {
        bookComponentsFlat.push(bc.id)
        bookComponentsFlat = bookComponentsFlat.concat(bc.bookComponents)
      })

      let count = 0

      bookComponentsFlat.forEach(bc => {
        if (bc === bookComponent.id) {
          count += 1
        }
      })

      return count > 1
    },
    async division(bookComponent) {
      return Division.query()
        .findById(bookComponent.divisionId)
        .throwIfNotFound()
    },
    async channels(bookComponent, _, ctx) {
      return Channel.query().where({
        objectId: bookComponent.id,
      })
    },
    async publishedVersions(bookComponent, _, ctx) {
      if (bookComponent.publishedVersions)
        return bookComponent.publishedVersions
      return BookComponent.publishedVersions(bookComponent)
    },
    async teams(bookComponent, _, ctx) {
      // if it is loaded from Book Resolver
      if (bookComponent.teams) return bookComponent.teams

      return await loadChildrenPreloadCousins(
        bookComponent,
        ctx.loaders.Team.forBookComponents,
      )
    },
    async owner(bookComponent, _, ctx) {
      if (!bookComponent.ownerId) return null
      return ctx.loaders.User.load(bookComponent.ownerId)
    },
    async reviewsFiles(bookComponent, _, ctx) {
      if (bookComponent.componentType === 'part') return []

      return await BookComponent.getFiles()
        .where({ 'file_versions.category': 'review' })
        .for([bookComponent.id])
    },
    async reviewsResponse(bookComponent) {
      return []
    },
    async supplementaryFiles(bookComponent) {
      return await BookComponent.getFiles()
        .where({ 'file_versions.category': 'supplement' })
        .for(bookComponent.id)
        .orderByRaw('file_versions.version_name::int desc')
    },
    async pdf(bookComponent) {
      return await BookComponent.getFiles()
        .where({ 'file_versions.category': 'pdf' })
        .for(bookComponent.id)
        .orderByRaw('file_versions.version_name::int desc')
    },
    async images(bookComponent) {
      return await BookComponent.getFiles()
        .where({ 'file_versions.category': 'image' })
        .for(bookComponent.id)
        .orderByRaw('file_versions.version_name::int desc')
    },
    async supportFiles(bookComponent) {
      return await BookComponent.getFiles()
        .where({ 'file_versions.category': 'support' })
        .for(bookComponent.id)
        .orderByRaw('file_versions.version_name::int desc')
    },
    async convertedFile(bookComponent, _, ctx) {
      if (bookComponent.convertedFile) return bookComponent.convertedFile
      return await loadChildrenPreloadCousins(
        bookComponent,
        ctx.loaders.File.convertedForBookComponents,
      )
    },
    async sourceFiles(bookComponent, _, ctx) {
      if (bookComponent.sourceFiles) return bookComponent.sourceFiles
      return await loadChildrenPreloadCousins(
        bookComponent,
        ctx.loaders.File.sourceForBookComponents,
      )
    },
    async errors(bookComponent, _, ctx) {
      if (bookComponent.errors) return bookComponent.errors

      const errorslist = await Errors.query()
        .where({ objectId: bookComponent.id, history: false })
        .orderBy('created')

      return errorslist
    },
    async issues(bookComponent, _, ctx) {
      if (bookComponent.issues) return bookComponent.issues

      return await loadChildrenPreloadCousins(
        bookComponent,
        ctx.loaders.Issue.forBookComponents,
      )
    },
    async parts(bookComponent, _, ctx) {
      const { bookId, id: bookComponentId } = bookComponent

      const allParts = await ctx.loaders.Book.getParts.load(bookId)
      return allParts.filter(
        part =>
          part.bookComponents && part.bookComponents.includes(bookComponentId),
      )
    },
  },
  Mutation: {
    addBookComponent,
    createNewPublishedVersion,
    deleteFile,
    moveAbove,
    moveBelow,
    moveInsidePart,
    publishBookComponents,
    updateBookComponent,
    updateBookComponentOrder,
    repeatBookComponent,
    updateBookComponents,
    uploadConvertFiles,
    retry,
    uploadFile,
    submitBookComponents,
  },
  Query: {
    getBookComponent,
    getBookComponentById,
    getBookComponents,
    getBookComponentsById,
    searchBookComponents,
    getChaptersOverview,
    getNextPrevious,
  },
  Subscription: {
    conversionCompleted: {
      subscribe: async (_, vars, context) => {
        const pubsub = await getPubsub()
        return pubsub.asyncIterator(`${CONVERSION_COMPLETED}.${vars.bookId}`)
      },
    },
    uploadConvertFileProgress: {
      subscribe: async (_, vars, context) => {
        const pubsub = await getPubsub()
        return pubsub.asyncIterator(`${ON_UPLOAD_PROGRESS}.${context.user}`)
      },
    },
  },
}
