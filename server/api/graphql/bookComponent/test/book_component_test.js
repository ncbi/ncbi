/* eslint-disable no-unused-vars */
/* eslint-disable jest/no-disabled-tests */
const { BookComponent } = require('@pubsweet/models')

const { db } = require('@coko/server')

const _ = require('lodash')
const addSeedFactory = require('../../../../services/dbSeederFactory')
const createGraphQLServer = require('../../helpers/createTestServer')

describe('searchBookComponents GraphQL', () => {
  let seeder
  let testServer
  let testBook1
  let testBook2

  const makeSearchQuery = ({ skip, take, sort, search, criteria = [] }) => {
    // Convert nested arguments to graphql arguments
    const argsString = value => {
      if (value === 0) {
        return '0'
      }

      if (value.constructor === String) {
        return `"${value}"`
      }

      if (value.constructor === Object) {
        const mapped = Object.keys(value).map(
          k => `${k}: ${argsString(value[k])}`,
        )

        return `{${_.join(mapped, ', ')}}`
      }

      if (value instanceof Array) {
        return `[${_.join(
          value.map(o => argsString(o)),
          ', ',
        )}]`
      }

      return value || ''
    }

    // Prepare template arguments
    let input = {}

    if (skip || skip === 0) {
      input.skip = skip
    }

    if (take || take === 0) {
      input.take = take
    }

    if (sort) {
      input.sort = sort
    }

    if (search) {
      criteria.push({
        field: 'search',
        operator: { ...{}, [search[0]]: search[1] },
      })
    }

    if (criteria) {
      _.set(input, 'search.criteria', criteria)
    }

    input = argsString(input)

    // Render and return template
    return {
      query: `{searchBookComponents(input: ${input}){
  metadata{skip, take, total}, results{id, updated, bookId, divisionId, title,
  alias, metadata{filename, sub_title}}}}`,
    }
  }

  beforeAll(async () => {
    seeder = addSeedFactory()

    await db.raw('BEGIN')

    const { user } = await seeder.insert('addAdmin', {
      password: '123456789',
      username: 'admin',
      email: 'admin@admin.com',
    })

    testServer = await createGraphQLServer(user.id)

    testBook1 = await seeder.insert('addBookDetailed', {
      user,
      book: { settings: { toc: { order_chapters_by: 'number' } } },
      frontMatter: {
        bookComponents: [
          {
            title: 'Front matter 1: Apple and banana jam',
            alias: '1001.1',
            metadata: {
              sub_title: 'the home kitchen',
            },
          },
          {
            title: 'Front matter 2: Cats and dogs',
            alias: '1002.1',
            metadata: {
              sub_title: 'keeping the kitchen clean',
              filename: 'file.docx',
            },
          },
        ],
      },
      body: {
        bookComponents: [
          {
            title: 'Part 1: Basic cooking skills',
            bookComponents: [
              {
                title: 'Basic cooking: Apple and banana jam',
                alias: '2001.1',
                metadata: {
                  filename: 'fruit2.docx',
                },
              },
              {
                title: 'Basic cooking: Roasting, baking and stuff',
                alias: '2002.1',
                metadata: {
                  sub_title: 'Beyond jam!',
                  filename: 'kitchen_skills.docx',
                },
              },
            ],
          },
          {
            title: 'Part 2: Advanced cooking skills',
            bookComponents: [
              { title: 'Advanced cooking: Everything else', alias: '2003.1' },
            ],
          },
        ],
      },
      backMatter: { bookComponents: [{ title: 'Appendix', alias: '3001.1' }] },
    })

    testBook2 = await seeder.insert('addBookDetailed', {
      user,
      book: { settings: { toc: { order_chapters_by: 'number' } } },
      frontMatter: { bookComponents: [] },
      body: {
        bookComponents: [
          {
            title: 'ABC: Apple to zebra',
            alias: '4001.1',
            metadata: { filename: 'abc.docx' },
          },
        ],
      },
      backMatter: { bookComponents: [] },
    })

    // Randomly update all BookComponent records; for testing order by updated
    // This ensures that no records have the same updated value
    const bookComponents = _.shuffle(await BookComponent.query())

    const updateBookComponents = async (i = 0) => {
      if (i >= bookComponents.length) {
        return
      }

      await BookComponent.query()
        .findById(bookComponents[i].id)
        .patch({ abstract: 'Something' })

      const sleep = delay => new Promise(resolve => setTimeout(resolve, delay))
      // Place at least 5 milliseconds between updates
      await sleep(5)
      await updateBookComponents(i + 1)
    }

    await updateBookComponents(0)
  })

  afterAll(async done => {
    await db.raw('ROLLBACK')
  })

  it('returns an error because operator is not "containsAny"', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        skip: 0,
        take: 15,
        search: ['contains', 'throw an error'],
      }),
    )

    expect(res.data).toBe(null)

    expect(res.errors.length).toEqual(1)
    expect(res.errors[0].constructor.name).toEqual('GraphQLError')
    expect(res.errors[0].path).toEqual(['searchBookComponents'])

    expect(res.errors[0].message).toEqual(
      "'search' is required to use the 'containsAny' operator",
    )

    expect(res.errors[0].extensions).toEqual({ code: 'INTERNAL_SERVER_ERROR' })
  })

  it('returns book components from multiple books and divisions', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        search: ['containsAny', 'apple'],
        sort: { field: ['updated'], direction: 'desc' },
      }),
    )

    expect(res.errors).toBe(undefined)
    expect(Object.keys(res.data)).toEqual(['searchBookComponents'])

    expect(Object.keys(res.data.searchBookComponents)).toEqual([
      'metadata',
      'results',
    ])

    expect(res.data.searchBookComponents.metadata).toEqual({
      skip: '0',
      take: '-1',
      total: '3',
    })

    const { results } = res.data.searchBookComponents
    expect(results instanceof Array).toBe(true)
    expect(results.length).toEqual(3)

    // Should be ordered by most recently updated
    expect(results[0].updated > results[1].updated).toBe(true)
    expect(results[1].updated > results[2].updated).toBe(true)

    // "apple" only occurs once per division
    expect(results[0].divisionId !== results[1].divisionId).toBe(true)
    expect(results[0].divisionId !== results[2].divisionId).toBe(true)
    expect(results[2].divisionId !== results[1].divisionId).toBe(true)

    // "apple" is only found in titles so check that titles match
    expect(results[0].title.toLowerCase().search('apple') > -1).toBe(true)
    expect(results[1].title.toLowerCase().search('apple') > -1).toBe(true)
    expect(results[2].title.toLowerCase().search('apple') > -1).toBe(true)
  })

  it('returns book components from multiple divisions in 1 book', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        search: ['containsAny', 'apple file bcms2003'],
        criteria: [{ field: 'bookId', operator: { eq: testBook1.book.id } }],
        sort: { field: ['updated'], direction: 'desc' },
      }),
    )

    expect(res.errors).toBe(undefined)
    expect(Object.keys(res.data)).toEqual(['searchBookComponents'])

    expect(Object.keys(res.data.searchBookComponents)).toEqual([
      'metadata',
      'results',
    ])

    expect(res.data.searchBookComponents.metadata).toEqual({
      skip: '0',
      take: '-1',
      total: '4',
    })

    const { results } = res.data.searchBookComponents
    expect(results instanceof Array).toBe(true)
    expect(results.length).toEqual(4)

    // Should be ordered by most recently updated
    expect(results[0].updated > results[1].updated).toBe(true)
    expect(results[1].updated > results[2].updated).toBe(true)
    expect(results[2].updated > results[3].updated).toBe(true)

    const appleMatch1 = _.find(results, {
      title: 'Front matter 1: Apple and banana jam',
    })

    const appleMatch2 = _.find(results, {
      title: 'Basic cooking: Apple and banana jam',
    })

    const fileMatch = _.find(results, {
      title: 'Front matter 2: Cats and dogs',
    })

    const bcmsMatch = _.find(results, {
      title: 'Advanced cooking: Everything else',
    })

    // Run some checks to confirm the data is good
    expect(appleMatch1.divisionId).toEqual(testBook1.frontMatter.id)
    expect(appleMatch2.divisionId).toEqual(testBook1.body.id)
    expect(fileMatch.metadata.filename).toEqual('file.docx')
    expect(bcmsMatch.alias).toEqual('bcms2003.1')
  })

  it('sorts and paginates with skip 0', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        skip: 0,
        take: 3,
        sort: { field: ['title'], direction: 'desc' },
        search: ['containsAny', 'bcms2003.1 kitchen bcms2001'],
      }),
    )

    expect(res.errors).toBe(undefined)
    expect(Object.keys(res.data)).toEqual(['searchBookComponents'])

    expect(Object.keys(res.data.searchBookComponents)).toEqual([
      'metadata',
      'results',
    ])

    expect(res.data.searchBookComponents.metadata).toEqual({
      skip: '0',
      take: '3',
      total: '5',
    })

    const { results } = res.data.searchBookComponents

    expect(
      results.map(r => _.omit(r, ['id', 'updated', 'bookId', 'divisionId'])),
    ).toEqual([
      // Matches "kitchen" on subtitle
      {
        title: 'Front matter 2: Cats and dogs',
        alias: 'bcms1002.1',
        metadata: {
          filename: 'file.docx',
          sub_title: 'keeping the kitchen clean',
        },
      },
      // Matches "kitchen" on subtitle
      {
        title: 'Front matter 1: Apple and banana jam',
        alias: 'bcms1001.1',
        metadata: { filename: '', sub_title: 'the home kitchen' },
      },
      // Matches "kitchen" on metadata.filename
      {
        title: 'Basic cooking: Roasting, baking and stuff',
        alias: 'bcms2002.1',
        metadata: { filename: 'kitchen_skills.docx', sub_title: 'Beyond jam!' },
      },
    ])
  })

  it('sorts and paginates with skip 3', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        skip: 3,
        take: 3,
        sort: { field: ['title'], direction: 'desc' },
        search: ['containsAny', 'bcms2003.1 kitchen bcms2001'],
      }),
    )

    expect(res.data.searchBookComponents.metadata).toEqual({
      skip: '3',
      take: '3',
      total: '5',
    })

    const { results } = res.data.searchBookComponents

    expect(
      results.map(r => _.omit(r, ['id', 'updated', 'bookId', 'divisionId'])),
    ).toEqual([
      // Matches on bcms id
      {
        title: 'Basic cooking: Apple and banana jam',
        alias: 'bcms2001.1',
        metadata: { filename: 'fruit2.docx', sub_title: null },
      },
      // Matches on bcms id
      {
        title: 'Advanced cooking: Everything else',
        alias: 'bcms2003.1',
        metadata: { filename: '', sub_title: '' },
      },
    ])
  })

  it('includes BookComponents by id array', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['title'], direction: 'desc' },
        search: ['containsAny', 'bcms2003.1 kitchen bcms2001'],
        criteria: [
          {
            field: 'id',
            operator: {
              in: [
                testBook1.frontMatter.bookComponents[0].id,
                testBook1.frontMatter.bookComponents[1].id,
              ],
            },
          },
        ],
      }),
    )

    expect(res.data.searchBookComponents.metadata).toEqual({
      skip: '0',
      take: '-1',
      total: '2',
    })

    const { results } = res.data.searchBookComponents

    expect(
      results.map(r => _.omit(r, ['id', 'updated', 'bookId', 'divisionId'])),
    ).toEqual([
      // Matches "kitchen" on subtitle; matches "id in" filter
      {
        title: 'Front matter 2: Cats and dogs',
        alias: 'bcms1002.1',
        metadata: {
          filename: 'file.docx',
          sub_title: 'keeping the kitchen clean',
        },
      },
      // Matches "kitchen" on subtitle; matches "id in" filter
      {
        title: 'Front matter 1: Apple and banana jam',
        alias: 'bcms1001.1',
        metadata: { filename: '', sub_title: 'the home kitchen' },
      },
    ])
  })

  it('excludes BookComponents by id array', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['title'], direction: 'desc' },
        search: ['containsAny', 'bcms2003.1 kitchen bcms2001'],
        criteria: [
          {
            field: 'id',
            operator: {
              notin: [
                testBook1.frontMatter.bookComponents[0].id,
                testBook1.frontMatter.bookComponents[1].id,
              ],
            },
          },
        ],
      }),
    )

    expect(res.data.searchBookComponents.metadata).toEqual({
      skip: '0',
      take: '-1',
      total: '3',
    })

    const { results } = res.data.searchBookComponents

    expect(
      results.map(r => _.omit(r, ['id', 'updated', 'bookId', 'divisionId'])),
    ).toEqual([
      // Matches "kitchen" on filename; matches "id notin" filter
      {
        title: 'Basic cooking: Roasting, baking and stuff',
        alias: 'bcms2002.1',
        metadata: { filename: 'kitchen_skills.docx', sub_title: 'Beyond jam!' },
      },
      // Matches bcms search; matches "id notin" filter
      {
        title: 'Basic cooking: Apple and banana jam',
        alias: 'bcms2001.1',
        metadata: { filename: 'fruit2.docx', sub_title: null },
      },
      // Matches bcms search; matches "id notin" filter
      {
        title: 'Advanced cooking: Everything else',
        alias: 'bcms2003.1',
        metadata: { filename: '', sub_title: '' },
      },
    ])
  })

  it('extends search with a fullSearch on title', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['title'], direction: 'desc' },
        search: ['containsAny', 'jam'],
        criteria: [{ field: 'title', operator: { fullSearch: 'apple&basic' } }],
      }),
    )

    expect(res.data.searchBookComponents.metadata).toEqual({
      skip: '0',
      take: '-1',
      total: '1',
    })

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(1)
    expect(results[0].title).toEqual('Basic cooking: Apple and banana jam')
  })

  it('extends search with title != "Basic cooking..."', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['title'], direction: 'asc' },
        search: ['containsAny', 'jam'],
        criteria: [
          {
            field: 'title',
            operator: { noteq: 'Basic cooking: Apple and banana jam' },
          },
        ],
      }),
    )

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(2)

    // Not excluded by title
    expect(results[0].title).toEqual(
      'Basic cooking: Roasting, baking and stuff',
    )

    // Matches "jam" on subtitle
    expect(results[0].metadata.sub_title).toEqual('Beyond jam!')
    // Not excluded by title; matches "jam" on title
    expect(results[1].title).toEqual('Front matter 1: Apple and banana jam')
  })

  it('extends search with alias < 1002.1', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['alias'], direction: 'asc' },
        search: ['containsAny', 'jam kitchen'],
        criteria: [{ field: 'alias', operator: { lt: '1002.1' } }],
      }),
    )

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(1)
    expect(results[0].title).toEqual('Front matter 1: Apple and banana jam')
    expect(results[0].alias).toEqual('bcms1001.1')
  })

  it('extends search with alias <= 1002.1', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['alias'], direction: 'asc' },
        search: ['containsAny', 'jam kitchen'],
        criteria: [{ field: 'alias', operator: { le: '1002.1' } }],
      }),
    )

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(2)
    expect(results[0].title).toEqual('Front matter 1: Apple and banana jam')
    expect(results[0].alias).toEqual('bcms1001.1')
    expect(results[1].metadata.sub_title).toEqual('keeping the kitchen clean')
    expect(results[1].alias).toEqual('bcms1002.1')
  })

  it('extends search with alias > 2003.1', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['alias'], direction: 'asc' },
        search: ['containsAny', 'cooking abc'],
        criteria: [{ field: 'alias', operator: { gt: '2003.1' } }],
      }),
    )

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(1)
    expect(results[0].metadata.filename).toEqual('abc.docx')
    expect(results[0].alias).toEqual('bcms4001.1')
  })

  it('extends search with alias >= 2003.1', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['alias'], direction: 'asc' },
        search: ['containsAny', 'cooking abc'],
        criteria: [{ field: 'alias', operator: { ge: '2003.1' } }],
      }),
    )

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(2)
    expect(results[0].title).toEqual('Advanced cooking: Everything else')
    expect(results[0].alias).toEqual('bcms2003.1')
    expect(results[1].metadata.filename).toEqual('abc.docx')
    expect(results[1].alias).toEqual('bcms4001.1')
  })

  it('extends search with title contains "cooking: Everything"', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        search: ['containsAny', 'cooking abc'],
        criteria: [
          { field: 'title', operator: { contains: 'cooking: Everything' } },
        ],
        sort: { field: ['updated'], direction: 'desc' },
      }),
    )

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(1)
    expect(results[0].title).toEqual('Advanced cooking: Everything else')
  })

  it('extends search with title notContains "cooking: Everything"', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['title'], direction: 'asc' },
        search: ['containsAny', 'cooking abc'],
        criteria: [
          { field: 'title', operator: { notContains: 'cooking: Everything' } },
        ],
      }),
    )

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(5)
    expect(results[0].title).toEqual('ABC: Apple to zebra')
    expect(results[1].title).toEqual('Basic cooking: Apple and banana jam')

    expect(results[2].title).toEqual(
      'Basic cooking: Roasting, baking and stuff',
    )

    expect(results[3].title).toEqual('Part 1: Basic cooking skills')
    expect(results[4].title).toEqual('Part 2: Advanced cooking skills')
  })

  it('extends search with title beginsWith "basic cooking:"', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['title'], direction: 'asc' },
        search: ['containsAny', 'cooking abc'],
        criteria: [
          { field: 'title', operator: { beginsWith: 'basic cooking:' } },
        ],
      }),
    )

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(2)
    expect(results[0].title).toEqual('Basic cooking: Apple and banana jam')

    expect(results[1].title).toEqual(
      'Basic cooking: Roasting, baking and stuff',
    )
  })

  it('extends search with title endsWith " zebra"', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['title'], direction: 'asc' },
        search: ['containsAny', 'cooking abc'],
        criteria: [{ field: 'title', operator: { endsWith: ' zebra' } }],
      }),
    )

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(1)
    expect(results[0].title).toEqual('ABC: Apple to zebra')
  })

  it('extends search with title containsAll "apple ZEBRA"', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['title'], direction: 'asc' },
        search: ['containsAny', 'cooking abc'],
        criteria: [
          { field: 'title', operator: { containsAll: ' apple ZEBRA ' } },
        ],
      }),
    )

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(1)
    expect(results[0].title).toEqual('ABC: Apple to zebra')
  })

  it('extends search with title containsNone "apple ZEBRA"', async () => {
    const res = await testServer.executeOperation(
      makeSearchQuery({
        sort: { field: ['title'], direction: 'asc' },
        search: ['containsAny', 'cooking abc'],
        criteria: [
          { field: 'title', operator: { containsNone: ' apple ZEBRA ' } },
        ],
      }),
    )

    const { results } = res.data.searchBookComponents
    expect(results.length).toEqual(4)
    expect(results[0].title).toEqual('Advanced cooking: Everything else')

    expect(results[1].title).toEqual(
      'Basic cooking: Roasting, baking and stuff',
    )

    expect(results[2].title).toEqual('Part 1: Basic cooking skills')
    expect(results[3].title).toEqual('Part 2: Advanced cooking skills')
  })
})
