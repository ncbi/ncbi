const graphql = require('../graphqlLoaderUtil')
const resolvers = require('./bookComponent.resolvers')

module.exports = {
  resolvers,
  typeDefs: graphql('bookComponent/bookComponent.graphql'),
}
