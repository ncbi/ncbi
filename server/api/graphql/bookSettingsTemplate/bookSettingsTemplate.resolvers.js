const config = require('config')
const cloneDeep = require('lodash/cloneDeep')
const pickBy = require('lodash/pickBy')
const merge = require('lodash/merge')

const { logger } = require('@coko/server')

const {
  BookSettingsTemplate,
  Collection,
  Organisation,
} = require('@pubsweet/models')

const Domain = require('../../../services/CommandService/domainCommands/domain')
const DomainTranslation = require('../../../services/domainTranslation/domainTranslation')
const { domainRequest } = require('../../../services/axiosService')
const { flattenKeys, unflattenKeys } = require('../../../_helpers')

const BASE_MESSAGE = 'Book settings template resolver:'

// #region HELPERS

/**
 * Grabs a list of template settings and returns only the settings which need
 * to be stored on our db (as opposed to the domain)
 */

const ownSettings = settings => {
  const BCMSSettings = config.get('bookSettings.BCMSTemplateSettings')
  const flattened = flattenKeys(settings)
  const filtered = pickBy(flattened, (value, key) => BCMSSettings.includes(key))
  const unflattened = unflattenKeys(filtered)
  return unflattened
}

/**
 * Accepts a BookSettingsTemplate as found in the db, and returns the same
 * book settings template, but with all the values stored on the NCBI domain
 * service as well.
 */
const getTemplateFromNCBI = async template => {
  try {
    const { domainId } = template

    const { data: detailsData } = await domainRequest(
      `domains/${domainId}`,
      'GET',
    )

    if (detailsData.meta.status !== 'ok') {
      logger.error(detailsData.body)
      throw new Error('Get domain details request failed')
    }

    const domainTemplate = detailsData.body

    const translatedDomainValues = DomainTranslation.domainToBookSettings(
      domainTemplate,
    )

    const fullTemplate = cloneDeep(template)
    fullTemplate.values = merge({}, translatedDomainValues, fullTemplate.values)

    return fullTemplate
  } catch (error) {
    error.message = `Get template from NCBI: ${error.message}`
    throw error
  }
}

/**
 * If there is a stored template of type X for the organization, return that.
 * If not, return the ncbi default template for that type (as stored in the config).
 */
const getTemplates = async (organizationId, collectionId = null) => {
  try {
    const response = []
    const types = ['chapterProcessed', 'wholeBook']
    const bcmsDefaults = config.get('bookSettings.BCMSTemplateDefaults')

    const foundTemplates = await BookSettingsTemplate.query().where({
      organizationId,
      collectionId,
    })

    await Promise.all(
      types.map(async type => {
        let toReturn
        const foundTemplate = foundTemplates.find(t => t.templateType === type)

        if (foundTemplate) {
          toReturn = await getTemplateFromNCBI(foundTemplate)
          toReturn.isCustom = true
        } else {
          const defaultTemplate = config.get(
            `bookSettings.domainTemplates.${type}`,
          )

          const translatedTemplate = DomainTranslation.domainToBookSettings(
            defaultTemplate,
          )

          toReturn = {
            id: `${collectionId || organizationId}-default-${type}`,
            isCustom: false,
            templateType: type,
            values: merge({}, bcmsDefaults, translatedTemplate),
          }
        }

        response.push(toReturn)
      }),
    )

    return response
  } catch (error) {
    logger.error(`${BASE_MESSAGE} Get book settings templates: ${error}`)

    throw error
  }
}

// #endregion HELPERS

const getBookSettingsTemplates = (_, args) => {
  const { organizationId, collectionId } = args

  try {
    return getTemplates(organizationId, collectionId)
  } catch (error) {
    logger.error(`${BASE_MESSAGE} Get book settings templates: ${error}`)
    throw error
  }
}

const createBookSettingsTemplate = async (_, args) => {
  try {
    const { collectionId, organizationId, type, values } = args.input
    const settingsToStore = ownSettings(values)

    const organization = await Organisation.query()
      .findById(organizationId)
      .throwIfNotFound()

    let collection

    if (collectionId) {
      collection = await Collection.query()
        .findById(collectionId)
        .throwIfNotFound()

      if (collection.organisationId !== organization.id) {
        throw new Error(
          `Collection with id ${collectionId} does not belond to organization ${organization.id}`,
        )
      }

      if (collection.collectionType !== 'bookSeries') {
        throw new Error(
          `Collection ${collectionId} is not a book series collection. 
          Only book series collections are allowed to have settings templates for their books.`,
        )
      }
    }

    // send domain data to ncbi service

    const domain = new Domain({
      data: {
        abbreviation: organization.abbreviation,
        collectionDomainId: collection && collection.domainId,
        publisherId: organization.publisherId,
        templateType: type,
        values,
      },
    })

    const domainResponse = await domain.executeCommand('createDomainTemplate')
    const { domainId, domainName } = domainResponse

    // store own data in db

    const template = await BookSettingsTemplate.query().insert({
      organizationId,
      collectionId,
      templateType: type,
      domainId,
      domainName,
      values: settingsToStore,
    })

    // get full list of values and return

    const templateWithNCBIData = await getTemplateFromNCBI(template)

    return {
      ...templateWithNCBIData,
      isCustom: true,
    }
  } catch (error) {
    error.message = `${BASE_MESSAGE} Create book settings template: ${error.message}`
    throw error
  }
}

const updateBookSettingsTemplate = async (_, args) => {
  const { templateId, values } = args.input
  const settingsToStore = ownSettings(values)

  /**
   * Update existing book settings template
   */

  const existingTemplate = await BookSettingsTemplate.query()
    .findById(templateId)
    .throwIfNotFound()

  // update data on ncbi service

  const domain = new Domain({
    data: {
      domainId: existingTemplate.domainId,
      values,
    },
  })

  await domain.executeCommand('updateDomainTemplate')

  // update own data in db

  const template = await BookSettingsTemplate.query().patchAndFetchById(
    templateId,
    {
      values: settingsToStore,
    },
  )

  // get full list of values and return

  const templateWithNCBIData = await getTemplateFromNCBI(template)

  return {
    ...templateWithNCBIData,
    isCustom: true,
  }
}

module.exports = {
  Query: {
    bookSettingsTemplates: getBookSettingsTemplates,
  },
  Mutation: {
    createBookSettingsTemplate,
    updateBookSettingsTemplate,
  },
}
