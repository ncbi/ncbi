/* eslint-disable global-require */

module.exports = {
  resolvers: require('./bookSettingsTemplate.resolvers'),
  typeDefs: require('../graphqlLoaderUtil')(
    'bookSettingsTemplate/bookSettingsTemplate.graphql',
  ),
}
