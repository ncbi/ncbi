/* eslint-disable no-return-await */
const cloneDeep = require('lodash/cloneDeep')
const isNan = require('lodash/isNaN')
const uniqBy = require('lodash/uniqBy')
const startsWith = require('lodash/startsWith')
const { useTransaction } = require('@coko/server')

const {
  Book,
  Collection,
  DashboardView,
  File,
  Toc,
} = require('@pubsweet/models')

const { SearchService, OrderService, Domain } = require('../../../services')

const getCollections = async (_, args, ctx) => {
  const searchedCollections = new SearchService(Collection, {
    ...args.input,
    filter: args.input.search.criteria,
  })

  const results = await searchedCollections.search()

  return {
    metadata: searchedCollections.metadata,
    results,
  }
}

/* eslint-disable no-param-reassign */
const getCollection = async (_, { id }, ctx) =>
  Collection.query().findById(id).throwIfNotFound()

const updateCollection = async (_, { id, input }, ctx) => {
  const { collection, cover } = await useTransaction(async transaction => {
    const domain = new Domain({ id, input, user: ctx.user, transaction })
    return await domain.executeCommand('updateCollection')
  })

  ctx.connectors.Events.model.notify('CollectionUpdated', {
    domains: [collection.id],
    cover,
    bcmsDomains: [collection.domain],
  })

  // Contact all existing books in the collection
  // and send a event notificatin to load the cover
  // to PMC.
  //
  await useTransaction(async trx => {
    const col = await Collection.query()
      .findOne({ collection_id: collection.id })
      .joinEager('books')

    if (col && col.books && col.metadata.applyCoverToBooks === true) {
      const domains = col.books.map(book => {
        return book.alias
      })

      const bcmsDomains = col.books.map(book => {
        return book.domain
      })

      ctx.connectors.Events.model.notify('BookUpdated', {
        domains,
        cover,
        bcmsDomains,
      })
    }
  })

  return collection
}

const createCollection = async (_, { input }, ctx) => {
  const collection = await useTransaction(async transaction => {
    const domain = new Domain({ ...input, user: ctx.user, transaction })
    return await domain.executeCommand('createCollection')
  })

  ctx.connectors.Events.model.notify('CollectionCreated', {
    domains: [collection.id],
  })

  return collection
}

const modifyAliasInput = input => {
  let incomingAliasValue = input.operator.eq.toLowerCase()
  const startsWithBCMS = startsWith(incomingAliasValue, 'bcms')
  if (startsWithBCMS) incomingAliasValue = incomingAliasValue.substring(4)
  const coercedToInt = parseInt(incomingAliasValue, 10)

  if (isNan(coercedToInt)) return {}

  const newCriterion = cloneDeep(input)
  newCriterion.operator.eq = coercedToInt
  return newCriterion
}

const getDashboard = async (_, args, ctx) => {
  // handle alias being a string in input, but an int in db
  const modifiedCriteria = args.input.search.criteria.map(i => {
    if (i.field === 'alias' && i.operator.eq) {
      return modifyAliasInput(i)
    }

    if (i.or) {
      const mod = i.or.map(c => {
        if (c.field === 'alias' && c.operator.eq) {
          return modifyAliasInput(c)
        }

        return c
      })

      return { or: mod }
    }

    return i
  })

  const searchedCollectionBooks = new SearchService(DashboardView, {
    ...args.input,
    filter: modifiedCriteria,
  })

  const results = await searchedCollectionBooks.search()

  return {
    metadata: searchedCollectionBooks.metadata,
    results,
  }
}

// create group
const createCustomGroup = async (_, { id, input }, ctx) => {
  return useTransaction(async trx => {
    // eslint-disable-next-line no-useless-catch
    try {
      const newCollectionGroup = await Collection.query(trx).insert({
        ...input,
        ownerId: ctx.user,
        settings: {
          isCollectionGroup: true,
        },
      })

      const parentCollection = (await Collection.query(trx).findOne({ id }))
        .components

      const componentsList =
        parentCollection && parentCollection.length ? parentCollection : []

      componentsList.unshift(newCollectionGroup.id)

      await Collection.query(trx)
        .patch({
          components: componentsList,
        })
        .where('id', id)

      return newCollectionGroup
    } catch (e) {
      throw e
    }
  })
}

// update group
const updateCustomGroup = async (_, { input, id }, ctx) =>
  Collection.query().patch(input).findOne({ id }).returning('*')

const updateCollectionOrder = async (
  _,
  { parentCollectionId, collectionId, ids, index = 0 },
  ctx,
) => {
  return useTransaction(async trx => {
    // book object of ids
    const books = await Book.query(trx).whereIn('id', ids)

    const collection = await Collection.query(trx).findOne({
      id: collectionId,
    })

    // if ids are not books -> changing groups positions
    if (books.length === 0) {
      // collection where [ids] are going

      const newComponents = cloneDeep(collection.components) || []
      const oldIndex = newComponents.findIndex(x => x === ids[0])
      newComponents.splice(oldIndex, 1)
      newComponents.splice(index, 0, ids[0])

      await Collection.query(trx)
        .patch({
          components: newComponents,
        })
        .where('id', collectionId)

      return await Collection.query(trx).findOne({ id: collectionId })
    }

    let parentCollection = await Collection.query(trx).findOne({
      id: parentCollectionId,
    })

    const allCustomGroups =
      (await Collection.query().whereIn('id', [
        ...parentCollection.components,
        parentCollectionId,
      ])) || []

    // adding books to target collection and removing from existing ones

    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < allCustomGroups.length; i++) {
      const group = allCustomGroups[i]

      if (group.id !== collectionId) {
        const thisComponents =
          cloneDeep(group.components).filter(x => !ids.includes(x)) || []

        // remove components if exist
        if (thisComponents.length !== group.components.length)
          // eslint-disable-next-line no-await-in-loop
          await Collection.query(trx)
            .patch({
              components: thisComponents.filter((value, inx, self) => {
                return self.indexOf(value) === inx
              }),
            })
            .where('id', group.id)
      } else {
        // add component to targed group
        const newComponents = cloneDeep(group.components) || []
        newComponents.splice(index, 0, ...ids)

        // eslint-disable-next-line no-await-in-loop
        await Collection.query(trx)
          .patch({
            components: newComponents.filter((value, inx, self) => {
              return self.indexOf(value) === inx
            }),
          })
          .where('id', collectionId)
      }
    }

    parentCollection = await Collection.query(trx).findOne({
      id: parentCollectionId,
    })

    await OrderService.model(parentCollection, { transaction: trx, deep: true })

    return await Collection.query(trx).findOne({ id: collectionId })
  })
}

const getFundedCollectionsOfOtherOrganisations = (_, { organisationId }) => {
  return Collection.query()
    .select('collections.*')
    .leftJoin(
      'organisations',
      'organisations.id',
      'collections.organisation_id',
    )
    .whereJsonSupersetOf('organisations.settings:type', { funder: true })
    .whereNot('organisationId', organisationId)
    .orderBy('organisations.name')
}

const collectionFileCover = async collection =>
  File.query().findById(collection.fileCover)

module.exports = {
  Collection: {
    // async teams(collection, _, ctx) {
    //   return ctx.connectors.Team.fetchAll(
    //     {
    //       objectId: collection.id,
    //     },
    //     ctx,
    //     { eager: '[members.[user, alias]]' },
    //   )
    // },
    async toc(collection) {
      return Toc.query().findOne({ collectionId: collection.id })
    },
    async workflow(collection) {
      //   console.log(collection.books)
      const books = await Book.query().where({ collectionId: collection.id })

      const booksCol = uniqBy(
        await Book.query().whereIn(
          'id',
          books.map(b => b.id),
        ),
        'workflow',
      )

      return booksCol.length === 1 ? booksCol[0].workflow : null
    },
    async books(collection, _, ctx) {
      const allBookIds = []

      if (collection.components) {
        allBookIds.push(...collection.components)
      }

      const allCollections = await Collection.query().whereIn(
        'id',
        collection.components,
      )

      allCollections.forEach(element => {
        if (element.components) {
          allBookIds.push(...element.components)
        }
      })

      return Book.query().whereIn('id', allBookIds)
    },
    async customGroups(collection, _, ctx) {
      return Collection.query().whereIn('id', collection.components)
    },
    async organisation(collection, _, ctx) {
      const teams = await ctx.connectors.Team.fetchAll(
        {
          objectId: collection.organisationId,
          objectType: 'Organisation',
        },
        ctx,
        { eager: '[members(onlyEnabledUsers).[user, alias]]' },
      )

      const organisation = await ctx.connectors.Organisation.fetchOne(
        collection.organisationId,
        ctx,
      )

      return Object.assign(organisation, {
        teams,
        users: {
          metadata: {},
          results: teams
            .find(tm => tm.role === 'user')
            .members.map(member => member.user),
        },
      })
    },
    fileCover: collectionFileCover,
  },
  BookCollection: {
    // eslint-disable-next-line no-underscore-dangle
    __resolveType(obj) {
      return obj.type.charAt(0).toUpperCase() + obj.type.slice(1)
    },
  },
  Mutation: {
    createCollection,
    updateCollection,
    createCustomGroup,
    updateCustomGroup,
    updateCollectionOrder,
  },
  Query: {
    getCollections,
    getCollection,
    getDashboard,
    getFundedCollectionsOfOtherOrganisations,
  },
}
