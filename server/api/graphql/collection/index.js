const graphql = require('../graphqlLoaderUtil')
const resolvers = require('./collection.resolvers')

module.exports = {
  resolvers,
  typeDefs: graphql('collection/collection.graphql'),
}
