/* eslint-disable camelcase */
/* eslint-disable no-return-await */
/* eslint-disable no-param-reassign */
const fetch = require('node-fetch')
const authentication = require('pubsweet-server/src/authentication')
const { parseString } = require('xml2js')
const { forEach, get } = require('lodash')

const {
  useTransaction,
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const { Team, TeamMember } = require('@pubsweet/models')

const notify = require('../../../services/notify')
const { SearchService, TeamMemberFactory } = require('../../../services')

const clientAuth = async (parent, __, ctx) => {
  const user = parent

  /* Global */

  const isSysAdmin = await user.hasGlobalRole('sysAdmin')
  const isPDF2XMLVendor = await user.hasGlobalRole('pdf2xmlVendor')

  /* Organisation */

  const sysAdminForOrganisation = await user.findObjectsThatUserHasRoleOn(
    'sysAdmin',
    'Organisation',
  )

  // TODO -- take care of "unverified status"
  const orgAdminForOrganisation = await user.findObjectsThatUserHasRoleOn(
    'orgAdmin',
    'Organisation',
    'enabled',
  )

  const editorForOrganisation = await user.findObjectsThatUserHasRoleOn(
    'editor',
    'Organisation',
  )

  const awardeeForOrganisation = await user.findObjectsThatUserHasRoleOn(
    'awardee',
    'Organisation',
  )

  const userForOrganisation = await user.findObjectsThatUserHasRoleOn(
    'user',
    'Organisation',
  )

  /* Book */

  const editorForBook = await user.findObjectsThatUserHasRoleOn(
    'editor',
    'book',
  )

  const authorForBook = await user.findObjectsThatUserHasRoleOn(
    'author',
    'book',
  )

  const previewerForBook = await user.findObjectsThatUserHasRoleOn(
    'previewer',
    'book',
  )

  /* Book component */

  const authorForBookComponent = await user.findObjectsThatUserHasRoleOn(
    'author',
    'bookComponent',
  )

  const previewerForBookComponent = await user.findObjectsThatUserHasRoleOn(
    'previewer',
    'bookComponent',
  )

  const editorForBookComponent = await user.findObjectsThatUserHasRoleOn(
    'editor',
    'bookComponent',
  )

  return {
    // global
    isSysAdmin,
    isPDF2XMLVendor,

    // organisation
    sysAdminForOrganisation,
    orgAdminForOrganisation,
    editorForOrganisation,
    awardeeForOrganisation,
    userForOrganisation,
    // organisation author?

    // collection
    // editorForCollection,
    // other roles?

    // book
    editorForBook,
    authorForBook,
    previewerForBook,
    // awardee for book?

    // book component
    authorForBookComponent,
    editorForBookComponent,
    previewerForBookComponent,
    // awardee for book component?
  }
}

const validateUser = async (
  allUsers,
  { username, givenName, surname, email, ncbiAccountId },
) => {
  const errors = []

  forEach(allUsers, user => {
    if (user.username === username) {
      errors.push('username')
    }

    if (user.email === email) {
      errors.push('email')
    }
  })

  if (errors.length !== 0) {
    throw new Error(`User with same ${errors} already exists!`)
  }
}

const checkUser = async (_, { search, exclude }, ctx, info) =>
  await ctx.connectors.User.model
    .query()
    .where({ username: search })
    .limit(1)
    .first()

const loginNcbiUser = async (_, { cookie }, ctx, info) => {
  const { ncbiAccountId, errors } = await getNcbiUserDetails(cookie)

  if (errors || !ncbiAccountId) {
    return { cookieStatus: 'loggedOut' }
  }

  const user = await ctx.connectors.User.model
    .query()
    .where({ ncbiAccountId })
    .limit(1)
    .first()

  if (!user) {
    return { cookieStatus: 'newUser' }
  }

  return { cookieStatus: 'loggedIn', token: authentication.token.create(user) }
}

// NO AUTH
const updatePassword = async (_, { input }, ctx) => {
  const userId = ctx.user
  const { currentPassword, newPassword } = input
  const User = ctx.connectors.User.model

  try {
    const u = await User.updatePassword(userId, currentPassword, newPassword)
    return u.id
  } catch (e) {
    throw new Error(e)
  }
}

const updateNcbiUser = async (
  _,
  {
    id,
    input: { givenName, surname, username, email, teams, pdf2xmlVendor, admin },
  },
  ctx,
) => {
  const User = ctx.connectors.User.model

  if (teams) {
    await Promise.all(
      teams.map(async tm => {
        return await ctx.connectors.Team.update(
          tm.id,
          {
            members: tm.members,
          },
          ctx,
          {
            unrelate: false,
            eager: 'members.user.teams',
          },
        )
      }),
    )

    logger.info(
      `Teams were updated for User: ${id}. New Teams: ${JSON.stringify(teams)}`,
    )
  }

  const updateData = {
    email,
    givenName,
    surname,
    username,
  }

  const adminInput = admin
  const vendorInput = pdf2xmlVendor

  try {
    return useTransaction(async trx => {
      const updatedUser = await User.query(trx).patchAndFetchById(
        id,
        updateData,
      )

      logger.info(
        `User with id ${id} was updated.Data ${JSON.stringify(updateData)}`,
      )

      // Get team data

      const sysAdminTeam = await Team.query(trx).findOne({
        role: 'sysAdmin',
        global: true,
      })

      const sysAdminMember = await TeamMember.query(trx).findOne({
        userId: id,
        teamId: sysAdminTeam.id,
      })

      const vendorTeam = await Team.query(trx).findOne({
        role: 'pdf2xmlVendor',
        global: true,
      })

      const vendorMember = await TeamMember.query(trx).findOne({
        userId: id,
        teamId: vendorTeam.id,
      })

      const isAlreadySysAdmin = !!sysAdminMember
      const isAlreadyVendor = !!vendorMember

      // Do not allow any scenario where the user will end up being both admin and vendor

      if (adminInput && vendorInput)
        throw new Error(
          'User cannot be set to have both a sysadmin and a pdf2xmlvendor role!',
        )

      // Handle sysadmin status

      if (adminInput && !isAlreadySysAdmin) {
        await TeamMember.query(trx).insert({
          userId: id,
          teamId: sysAdminTeam.id,
        })

        logger.info(`User with id : ${id} is now sysAdmin}`)
      }

      if (!adminInput && isAlreadySysAdmin) {
        await TeamMember.query(trx).deleteById(sysAdminMember.id)
      }

      // Handle vendor team status

      if (vendorInput && !isAlreadyVendor) {
        await TeamMember.query(trx).insert({
          userId: id,
          teamId: vendorTeam.id,
        })

        logger.info(`User with id : ${id} is now in pdf2xmlVendor Team}`)
      }

      if (!vendorInput && isAlreadyVendor) {
        await TeamMember.query(trx).deleteById(vendorMember.id)
      }

      return updatedUser
    })
  } catch (e) {
    throw new Error(e)
  }
}

const createCokoUser = async (_, { input }, ctx) => {
  const User = ctx.connectors.User.model

  const allUsers = await User.all()

  try {
    await validateUser(allUsers, input)
  } catch (e) {
    throw new Error(e)
  }

  const query = async trx => {
    if (input.password) {
      input.passwordHash = await User.hashPassword(input.password)
      delete input.password
    }

    // const user = await ctx.connectors.User.create(props, ctx)
    const user = await User.query(trx).insert(input).returning('*')
    logger.info(`New user Created ${user.id}`)

    return user
  }

  return useTransaction(query)
}

const getNcbiUserDetails = async cookie => {
  const { eMyNCBIResult } = await fetch(
    `${process.env.MY_NCBI}&WebCubbyUser=${cookie}`,
  )
    .then(res => res.text())
    .then(
      xml =>
        new Promise((resolve, reject) =>
          parseString(xml, (error, result) => {
            if (error) reject(error)
            resolve(result)
          }),
        ),
    )

  const firstValueOr = (testArray, orValue) =>
    testArray && testArray.length ? testArray[0] : orValue

  const {
    UserId,
    UserName,
    Email,
    SignInURL,
    SignOutURL,
    RegisterURL,
    HomePageURL,
    ERROR,
  } = eMyNCBIResult

  return {
    ncbiAccountId: firstValueOr(UserId, null),
    username: firstValueOr(UserName, null),
    email: firstValueOr(Email, null),
    signInURL: firstValueOr(SignInURL, null),
    signOutURL: firstValueOr(SignOutURL, null),
    registerURL: firstValueOr(RegisterURL, null),
    homePageURL: firstValueOr(HomePageURL, null),
    errors: firstValueOr(ERROR, null),
  }
}

const getNcbiUrls = async (_, { cookie }, ctx) => {
  const {
    signInURL,
    signOutURL,
    registerURL,
    homePageURL,
    errors,
  } = await getNcbiUserDetails(cookie)

  return { signInURL, signOutURL, registerURL, homePageURL, errors }
}

const createMyNcbiUser = async (_, { input }, ctx) => {
  const { cookie, ...userData } = input
  const User = ctx.connectors.User.model

  const { username, email, ncbiAccountId, errors } = await getNcbiUserDetails(
    cookie,
  )

  if (errors) {
    throw new Error(errors)
  }

  const user = await ctx.connectors.User.model
    .query()
    .where({ ncbiAccountId })
    .limit(1)
    .first()

  if (user) {
    throw new Error('User already exists')
  }

  const insertValues = { username, email, ncbiAccountId, ...userData }

  const query = async trx => {
    const newUser = await User.query(trx).insert(insertValues).returning('*')

    logger.info(`New user Created ${newUser.id}`)

    return newUser
  }

  return useTransaction(query)
}

const verifyUsers = async (_, { id: users, organisationId }, ctx) => {
  const organisation = await ctx.connectors.Organisation.model
    .query()
    .findOne({ id: organisationId })

  const orgTeamUser = await ctx.connectors.Team.model.query().findOne({
    object_id: organisationId,
    object_type: 'Organisation',
    role: 'user',
  })

  const userTeamId = users.map(user => [user, orgTeamUser.id, 'unverified'])

  await ctx.connectors.TeamMember.model
    .query()
    .patch({ status: 'enabled' })
    .whereIn(['user_id', 'team_id', 'status'], userTeamId)

  logger.info(
    `Users Verified for Organization ${organisation.name} : [${users.join(
      ',',
    )}]`,
  )

  const result = await ctx.connectors.User.model.query().whereIn('id', users)

  notify('verifyUsers', {
    organisationId,
    users: result,
  })

  return result
}

const rejectUsers = async (_, { id: users, organisationId }, ctx) => {
  const organisation = await ctx.connectors.Organisation.model
    .query()
    .findOne({ id: organisationId })

  const orgTeamUser = await ctx.connectors.Team.model.query().findOne({
    object_id: organisationId,
    object_type: 'Organisation',
    role: 'user',
  })

  const userTeamId = users.map(user => [user, orgTeamUser.id, 'unverified'])

  await ctx.connectors.TeamMember.model
    .query()
    .patch({ status: 'rejected' })
    .whereIn(['user_id', 'team_id', 'status'], userTeamId)

  logger.info(
    `Users rejected for Organisation ${organisation.name}: [${users.join(
      ',',
    )}]`,
  )

  const result = await ctx.connectors.User.model.query().whereIn('id', users)

  notify('rejectUsers', {
    organisationId,
    users: result,
  })

  return result
}

const changeStatusUser = async (
  _,
  { id: users, organisationId, status },
  ctx,
) => {
  const orgTeamsUser = await ctx.connectors.Team.model.query().where({
    object_id: organisationId,
    object_type: 'Organisation',
  })

  const userTeamId = []

  users.forEach(user => {
    orgTeamsUser.forEach(team => {
      userTeamId.push([user, team.id, status ? 'enabled' : 'disabled'])
    })
  })

  await this.query
    .patch({ status: 'rejected' })
    .whereIn(['user_id', 'team_id', 'status'], userTeamId)

  return await ctx.connectors.User.model.query().whereIn('id', users)
}

const checkAllOrganizationUsers = async (
  _,
  { input: { organisationId, role }, action },
  ctx,
) => {
  const organisation = await ctx.connectors.Organisation.model
    .query()
    .findOne({ id: organisationId })

  const orgTeamUser = await ctx.connectors.Team.model.query().where({
    object_id: organisationId,
    object_type: 'Organisation',
    role: 'user',
  })

  if (action === 'VERIFY') {
    logger.info(`All users of Organisation ${organisation.name}  are verified`)

    await this.query
      .patch({ status: 'enabled' })
      .whereIn(['team_id', 'status'], [[orgTeamUser.id, 'unverified']])

    return 'verify'
  }

  if (action === 'REJECT') {
    logger.info(`All users of Organisation ${organisation.name}  are rejected`)

    await this.query
      .patch({ status: 'enabled' })
      .whereIn(['team_id', 'status'], [[orgTeamUser.id, 'rejected']])

    return 'reject'
  }

  if (action === 'ASSIGNTEAM') {
    const team = { object_id: organisationId, objectType: 'Organisation', role }
    await TeamMemberFactory.assign(team, [])

    logger.info(
      `All users of Organisation ${organisation.name}  are assigned to role: ${role}`,
    )

    return 'assignteam'
  }

  return true
}

const searchUsers = async (_, args, ctx) => {
  const User = ctx.connectors.User.model

  const searchedUsers = new SearchService(User, {
    filter: get(args, 'input.search.criteria'),
    sort: get(args, 'input.sort'),
  })

  const results = await searchedUsers.search()

  return {
    metadata: searchedUsers.metadata,
    results,
  }
}

module.exports = {
  Mutation: {
    changeStatusUser,
    checkAllOrganizationUsers,
    createCokoUser,
    createMyNcbiUser,
    rejectUsers,
    updateNcbiUser,
    updatePassword,
    verifyUsers,
    loginNcbiUser,
    getNcbiUrls,
  },
  Query: {
    checkUser,
    searchUsers,
  },
  User: {
    auth: clientAuth,
    // teams: (parent, _, ctx) => parent.teams,
    organisations: async (parent, _, ctx) => {
      const OrganizationIds = (
        await ctx.connectors.Team.model
          .query()
          .withGraphJoined('members')
          .where({ userId: parent.id, role: 'user' })
          .andWhere(builder => {
            return builder.whereNot({ status: 'unverified' })
          })
      ).map(team => team.objectId)

      return ctx.connectors.Organisation.model
        .query()
        .whereIn('id', OrganizationIds)
    },
  },
}
