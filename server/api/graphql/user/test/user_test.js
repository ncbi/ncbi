/* eslint-disable jest/no-disabled-tests */
const omit = require('lodash/omit')
const { User } = require('@pubsweet/models')
const { startServer } = require('pubsweet-server')
const supertest = require('supertest')

const authentication = require('pubsweet-server/src/authentication')
const dbCleaner = require('../../helpers/db_cleaner')

const fixtures = {
  otherUser: {
    email: 'some1@example.com',
    givenName: 'givename',
    password: 'password',
    surname: 'testsurname',
    username: 'test1',
  },
  otherUser1: {
    email: 'some2@example.com',
    type: 'user',
    username: 'test2',
  },
  user: {
    email: 'test@example.com',
    password: 'test',
    type: 'user',
    username: 'testuser',
  },
}

describe.skip('Users queries for NCBI', () => {
  let token
  let user
  let server
  let graphqlQuery

  beforeAll(async () => {
    server = await startServer()
    await dbCleaner()
    user = await new User(fixtures.user).save()
    token = authentication.token.create(user)

    graphqlQuery = (query, variables) => {
      const request = supertest(server)
      const req = request.post('/graphql').send({ query, variables })
      if (token) req.set('Authorization', `Bearer ${token}`)
      return req
    }
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await dbCleaner()
  })

  const createUser = async () => {
    const organisation = await createOrganisation({
      email: 'org@test.org',
      name: 'testorg',
    })

    const organisation1 = await createOrganisation({
      email: 'org1@test.org',
      name: 'testorg1',
    })

    const { body } = await graphqlQuery(
      `mutation($input: CokoUserCreateInput) {
            createCokoUser(input: $input) {
                  id
                  username
                  email
                  givenName
                  surname
                }
              }`,
      {
        input: {
          ...fixtures.otherUser,
          organisations: [organisation1.id, organisation.id],
        },
      },
    )

    return { body, organisation, organisation1 }
  }

  const updateUser = async (id, input) => {
    const { body } = await graphqlQuery(
      `mutation($id: ID, $input: NcbiUserUpdateInput) {
            updateNcbiUser(id: $id, input: $input) {
                  id
                  username
                  email
                  givenName
                  surname
                }
              }`,
      {
        id,
        input,
      },
    )

    return body.data.updateNcbiUser
  }

  const verifyRejectUser = async (id, organisationId, request) => {
    const { body } = await graphqlQuery(
      `mutation($id: [ID!]!, $organisationId: ID!) {
        ${request}(id: $id, organisationId: $organisationId) {
                  id
                  username
                  email
                  givenName
                  surname
                  status
                }
              }`,
      {
        id,
        organisationId,
      },
    )

    return body
  }

  const createOrganisation = async input => {
    const { body } = await graphqlQuery(
      `mutation($input: CreateOrganisationInput!) {
          createOrganisation(input: $input) {
              id
              name
              email
            }
          }`,
      {
        input: {
          ...input,
        },
      },
    )

    return body.data.createOrganisation
  }

  it('can update user and teams', async () => {
    const { body, organisation } = await createUser()

    await verifyRejectUser(
      [body.data.createCokoUser.id],
      organisation.id,
      'verifyUsers',
    )

    await updateUser(body.data.createCokoUser.id, {
      email: 'updatedEmail@gmail.com',
      givenName: 'updatedGivenName',
      surname: 'UpdatedSurname',
      teams: {
        assign: [],
        remove: [],
        status: false,
      },
      username: 'updatedUsername',
    })

    expect(body).toMatchObject({
      data: {
        createCokoUser: omit(fixtures.otherUser, ['password']),
      },
    })
  })
})
