/* eslint-disable no-return-await */
/* eslint-disable jest/no-disabled-tests */
const omit = require('lodash/omit')
const { startServer } = require('pubsweet-server')
// eslint-disable-next-line import/no-extraneous-dependencies
const supertest = require('supertest')
const { Team, User } = require('@pubsweet/models')
const authentication = require('pubsweet-server/src/authentication')
const dbCleaner = require('../../helpers/db_cleaner')

const fixtures = {
  otherUser: {
    email: 'some1@example.com',
    givenName: 'givename',
    password: 'password',
    surname: 'testsurname',
    username: 'test1',
  },
  otherUser1: {
    email: 'some2@example.com',
    givenName: 'givename2',
    password: 'password2',
    surname: 'testsurname2',
    type: 'user',
    username: 'test2',
  },
  otherUser2: {
    email: 'some3@example.com',
    givenName: 'givename3',
    password: 'password3',
    surname: 'testsurname3',
    type: 'user',
    username: 'test3',
  },
  otherUser3: {
    email: 'some4@example.com',
    givenName: 'givename4',
    password: 'password4',
    surname: 'testsurname4',
    type: 'user',
    username: 'test4',
  },
  user: {
    email: 'test@example.com',
    password: 'test',
    type: 'user',
    username: 'testuser',
  },
}

const getOrganizationTeams = async id =>
  await Team.query()
    .findOne({
      objectId: id,
    })
    .eager('[members.[user, alias]]')

describe.skip('Users queries for Teams GraphQL', () => {
  let token
  let user
  let server
  let graphqlQuery

  beforeAll(async () => {
    server = await startServer()
    await dbCleaner()
    user = await new User(fixtures.user).save()
    token = authentication.token.create(user)

    graphqlQuery = (query, variables) => {
      const request = supertest(server)
      const req = request.post('/graphql').send({ query, variables })
      if (token) req.set('Authorization', `Bearer ${token}`)
      return req
    }
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await dbCleaner()
  })

  const createUser = async () => {
    const organisation = await createOrganisation({
      email: 'org@test.org',
      name: 'testorg',
    })

    const organisation1 = await createOrganisation({
      email: 'org1@test.org',
      name: 'testorg1',
    })

    const { body } = await graphqlQuery(
      `mutation($input: CokoUserCreateInput) {
            createCokoUser(input: $input) {
                  id
                  username
                  email
                  givenName
                  surname
                }
              }`,
      {
        input: {
          ...fixtures.otherUser,
          organisations: [organisation1.id, organisation.id],
        },
      },
    )

    return { body, organisation, organisation1 }
  }

  const verifyRejectUser = async (id, organisationId, request) => {
    const { body } = await graphqlQuery(
      `mutation($id: [ID!]!, $organisationId: ID!) {
        ${request}(id: $id, organisationId: $organisationId) {
                  id
                  username
                  email
                  givenName
                  surname
                }
              }`,
      {
        id,
        organisationId,
      },
    )

    return body
  }

  const changeStatus = async (id, organisationId, status) => {
    await graphqlQuery(
      `mutation($id: [ID!]!, $organisationId: ID!, $status: Boolean!) {
          changeStatusUser(id: $id, organisationId: $organisationId, status: $status) {
                      id
                      username
                      email
                      givenName
                      surname
                    }
                  }`,
      {
        id: [id],
        organisationId,
        status,
      },
    )
  }

  const createOrganisation = async input => {
    const { body } = await graphqlQuery(
      `mutation($input: CreateOrganisationInput!) {
          createOrganisation(input: $input) {
              id
              name
              email
            }
          }`,
      {
        input: {
          ...input,
        },
      },
    )

    return body.data.createOrganisation
  }

  it('can create user and assigned to organisation', async () => {
    const { body, organisation, organisation1 } = await createUser()

    const teams = getOrganizationTeams(organisation.id)
    const userTeam = teams.find(team => team.role === 'user')

    const teams1 = getOrganizationTeams(organisation1.id)
    const userTeam1 = teams1.find(team => team.role === 'user')

    expect(userTeam.members[0]).toMatchObject({
      status: 'unverified',
      userId: body.data.createCokoUser.id,
    })

    expect(userTeam1.members[0]).toMatchObject({
      status: 'unverified',
      userId: body.data.createCokoUser.id,
    })

    expect(body).toMatchObject({
      data: {
        createCokoUser: omit(fixtures.otherUser, ['password']),
      },
    })
  })

  it('can verify a user of an organisation', async () => {
    const { body, organisation, organisation1 } = await createUser()

    const verified = await verifyRejectUser(
      [body.data.createCokoUser.id],
      organisation.id,
      'verifyUsers',
    )

    const teams = getOrganizationTeams(organisation.id)
    const userTeam = teams.find(team => team.role === 'user')

    const teams1 = getOrganizationTeams(organisation1.id)
    const userTeam1 = teams1.find(team => team.role === 'user')

    expect(userTeam.members[0]).toMatchObject({
      status: 'enabled',
      userId: body.data.createCokoUser.id,
    })

    expect(userTeam1.members[0]).toMatchObject({
      status: 'unverified',
      userId: body.data.createCokoUser.id,
    })

    expect(verified).toMatchObject({
      data: {
        verifyUsers: [omit(fixtures.otherUser, ['password'])],
      },
    })
  })

  it('can reject a user of an organisation', async () => {
    const { body, organisation, organisation1 } = await createUser()

    const verified = await verifyRejectUser(
      [body.data.createCokoUser.id],
      organisation.id,
      'rejectUsers',
    )

    const teams = getOrganizationTeams(organisation.id)
    const userTeam = teams.find(team => team.role === 'user')

    const teams1 = getOrganizationTeams(organisation1.id)
    const userTeam1 = teams1.find(team => team.role === 'user')

    expect(userTeam.members).toHaveLength(0)

    expect(userTeam1.members[0]).toMatchObject({
      status: 'unverified',
      userId: body.data.createCokoUser.id,
    })

    expect(verified).toMatchObject({
      data: {
        rejectUsers: [omit(fixtures.otherUser, ['password'])],
      },
    })
  })

  it('can reject a user and remove it from team_members only', async () => {
    const { body, organisation, organisation1 } = await createUser()

    const verified = await verifyRejectUser(
      [body.data.createCokoUser.id],
      organisation.id,
      'rejectUsers',
    )

    const verified1 = await verifyRejectUser(
      [body.data.createCokoUser.id],
      organisation1.id,
      'rejectUsers',
    )

    const teams = getOrganizationTeams(organisation.id)
    const userTeam = teams.find(team => team.role === 'user')

    const teams1 = getOrganizationTeams(organisation1.id)
    const userTeam1 = teams1.find(team => team.role === 'user')

    expect(userTeam.members).toHaveLength(0)
    expect(userTeam1.members).toHaveLength(0)

    expect(verified1).toMatchObject({
      data: {
        rejectUsers: [],
      },
    })

    expect(verified).toMatchObject({
      data: {
        rejectUsers: [{ id: body.data.createCokoUser.id }],
      },
    })
  })

  it('can change status of an user for specific organisation', async () => {
    const { body, organisation, organisation1 } = await createUser()

    await verifyRejectUser(
      [body.data.createCokoUser.id],
      organisation.id,
      'verifyUsers',
    )

    await changeStatus(body.data.createCokoUser.id, organisation.id, false)

    let teams = getOrganizationTeams(organisation.id)
    let userTeam = teams.find(team => team.role === 'user')

    let teams1 = getOrganizationTeams(organisation1.id)
    let userTeam1 = teams1.find(team => team.role === 'user')

    expect(userTeam.members[0]).toMatchObject({
      status: 'disabled',
      userId: body.data.createCokoUser.id,
    })

    expect(userTeam1.members[0]).toMatchObject({
      status: 'unverified',
      userId: body.data.createCokoUser.id,
    })

    await changeStatus(body.data.createCokoUser.id, organisation.id, true)

    teams = getOrganizationTeams(organisation.id)
    userTeam = teams.find(team => team.role === 'user')

    teams1 = getOrganizationTeams(organisation1.id)
    userTeam1 = teams1.find(team => team.role === 'user')

    expect(userTeam.members[0]).toMatchObject({
      status: 'enabled',
      userId: body.data.createCokoUser.id,
    })

    expect(userTeam1.members[0]).toMatchObject({
      status: 'unverified',
      userId: body.data.createCokoUser.id,
    })
  })

  it('can not change status of an unverified user', async () => {
    const { body, organisation, organisation1 } = await createUser()

    await changeStatus(body.data.createCokoUser.id, organisation.id, false)

    const teams = getOrganizationTeams(organisation.id)
    const userTeam = teams.find(team => team.role === 'user')

    const teams1 = getOrganizationTeams(organisation1.id)
    const userTeam1 = teams1.find(team => team.role === 'user')

    expect(userTeam.members[0]).toMatchObject({
      status: 'unverified',
      userId: body.data.createCokoUser.id,
    })

    expect(userTeam1.members[0]).toMatchObject({
      status: 'unverified',
      userId: body.data.createCokoUser.id,
    })
  })

  it('can assign users to a team of specific organisation', async () => {
    const { body, organisation, organisation1 } = await createUser()

    await verifyRejectUser(
      [body.data.createCokoUser.id],
      organisation.id,
      'verifyUsers',
    )

    await graphqlQuery(
      `mutation($id: [ID!]!, $organisationId: ID!, $role: String!) {
          assignUsersToTeam(id: $id, organisationId: $organisationId, role: $role) {
                          id
                          name
                        }
                      }`,
      {
        id: [body.data.createCokoUser.id],
        organisationId: organisation.id,
        role: 'adminOrganisation',
      },
    )

    const teams = getOrganizationTeams(organisation.id)

    const teams1 = getOrganizationTeams(organisation1.id)

    const adminOrganisation = teams.find(
      team => team.role === 'adminOrganisation',
    )

    const adminOrganisation1 = teams1.find(
      team => team.role === 'adminOrganisation',
    )

    const unverified = teams1.find(team => team.role === 'user')

    expect(adminOrganisation.members[0]).toMatchObject({
      status: 'enabled',
      userId: body.data.createCokoUser.id,
    })

    expect(adminOrganisation1.members).toHaveLength(0)

    expect(unverified.members[0]).toMatchObject({
      status: 'unverified',
      userId: body.data.createCokoUser.id,
    })
  })
})
