const resolvers = require('./user.resolvers')
const graphql = require('../graphqlLoaderUtil')

module.exports = {
  resolvers,
  typeDefs: graphql('user/user.graphql'),
}
