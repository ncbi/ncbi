/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
const merge = require('lodash/merge')
const fs = require('fs')
const path = require('path')
const logger = require('@pubsweet/logger')

const Defs = []
let Rslvs = {}

const normalizedPath = path.join(__dirname)

fs.readdirSync(normalizedPath)
  .filter(file => fs.statSync(path.join(normalizedPath, file)).isDirectory())
  .forEach(folder => {
    try {
      if (fs.existsSync(`${normalizedPath}/${folder}/index.js`)) {
        const {
          typeDefs,
          resolvers,
        } = require(`${normalizedPath}/${folder}/index.js`)

        if (typeDefs) {
          Defs.push(typeDefs)
        }

        if (resolvers) {
          Rslvs = merge({}, Rslvs, resolvers)
        }
      }
      // eslint-disable-next-line no-empty
    } catch (e) {
      logger.error(e)
    }
  })

module.exports = {
  resolvers: Rslvs,
  typeDefs: Defs.join(' '),
}
