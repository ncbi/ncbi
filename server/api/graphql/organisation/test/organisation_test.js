/* eslint-disable no-return-await */
/* eslint-disable jest/no-disabled-tests */

const config = require('config')
const { startServer } = require('pubsweet-server')
const supertest = require('supertest')
const { Organisation, Team, User } = require('@pubsweet/models')

const authentication = require('pubsweet-server/src/authentication')
const dbCleaner = require('../../helpers/db_cleaner')

const fixtures = {
  otherUser: {
    email: 'some1@example.com',
    type: 'user',
    username: 'test1',
  },
  otherUser1: {
    email: 'some2@example.com',
    type: 'user',
    username: 'test2',
  },
  user: {
    email: 'test@example.com',
    password: 'test',
    type: 'user',
    username: 'testuser',
  },
}

const getOrganizationTeams = async id =>
  await Team.query()
    .findOne({
      objectId: id,
    })
    .eager('[members.[user, alias]]')

describe.skip('Organisation GraphQL', () => {
  let token
  let user
  let server
  let graphqlQuery

  beforeAll(async () => {
    server = await startServer()
    await dbCleaner()
    user = await new User(fixtures.user).save()
    token = authentication.token.create(user)

    graphqlQuery = (query, variables) => {
      const request = supertest(server)
      const req = request.post('/graphql').send({ query, variables })
      if (token) req.set('Authorization', `Bearer ${token}`)
      return req
    }
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await dbCleaner()
  })

  it('can get Organisations', async () => {
    await new Organisation({
      email: 'org@test.org',
      name: 'testorg',
    }).save()

    const { body } = await graphqlQuery(
      `query ($input: SearchModelInput)
        { getOrganisations (input: $input) {
          metadata {
            total
            skip
            take
          }
          results {
            id
            name
            email
            agreement
          }
        }
      }`,
      {
        input: {
          skip: 0,
          take: 20,
        },
      },
    )

    expect(body.data.getOrganisations.results).toHaveLength(1)
  })

  it('can create an organisation', async () => {
    const input = {
      email: 'org@test.org',
      name: 'testorg',
    }

    const { body } = await graphqlQuery(
      `mutation($input: CreateOrganisationInput!) {
        createOrganisation(input: $input) {
            id
            name
            email
          }
        }`,
      {
        input: {
          ...input,
        },
      },
    )

    const teams = getOrganizationTeams(body.data.createOrganisation.id)

    const configTeams = Object.keys(config.authsome.teams)

    teams.map(team => expect(configTeams).toContain(team.role))

    expect(body).toEqual({
      data: {
        createOrganisation: {
          id: expect.any(String),
          ...input,
        },
      },
    })
  })

  it('can update organisation ', async () => {
    const { id } = await new Organisation({
      email: 'Email',
      name: 'Org',
    }).save()

    const input = [
      {
        email: 'OrgEmail',
        id,
        name: 'OrgName',
      },
    ]

    const { body } = await graphqlQuery(
      `mutation($input: [UpdateOrganisationInput]!) {
        updateOrganisation(input: $input) {
            id
            name
            email
          }
        }`,
      {
        input,
      },
    )

    expect(body).toEqual({
      data: {
        updateOrganisation: input,
      },
    })
  })

  it('can delete an organisation', async () => {
    const { id } = await new Organisation({
      email: 'emailOrg',
      name: 'To delete',
    }).save()

    const { body } = await graphqlQuery(
      `mutation($id: [ID]) {
          deleteOrganisation(id: $id)
        }`,
      { id: [id] },
      token,
    )

    expect(body).toEqual({
      data: { deleteOrganisation: [id] },
    })
  })

  /* eslint-disable-next-line jest/no-commented-out-tests */
  // it('can get organisation and users Teams', async () => {
  //   const otherUser = await new User(fixtures.otherUser).save()
  //   const otherUser1 = await new User(fixtures.otherUser1).save()
  //   const members = [
  //     { status: 'unverified', user: { id: otherUser1.id } },
  //     { status: 'enabled', user: { id: otherUser.id } },
  //   ]
  //   const { id } = await OrganisationCreateService.createWithMembers(
  //     {
  //       email: 'test@test.com',
  //       name: 'Test',
  //     },
  //     members,
  //   )

  //   await new Organisation({ name: 'other Test' }).save()

  //   const { body } = await graphqlQuery(
  //     `query ($id: ID!, $input: SearchModelInput)
  //       {
  //         getOrganisation (id: $id, input: $input) {
  //           id
  //           name
  //           email
  //           teams {
  //             id
  //             role
  //           }
  //           users {
  //             metadata {
  //               total
  //               skip
  //               take
  //             }
  //             results {
  //               id

  //             }
  //           }
  //       }
  //     }`,
  //     {
  //       id,
  //       input: {
  //         skip: 0,
  //         take: 10,
  //       },
  //     },
  //   )

  //   expect(body.data.getOrganisation.users.metadata.total).toBe('2')
  // })
})
