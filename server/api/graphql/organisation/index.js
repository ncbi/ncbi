/* eslint-disable global-require */

module.exports = {
  resolvers: require('./organisation.resolvers'),
  typeDefs: require('../graphqlLoaderUtil')(
    'organisation/organisation.graphql',
  ),
}
