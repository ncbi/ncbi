/* eslint-disable no-return-await */
/* eslint-disable no-param-reassign */

const { get } = require('lodash')

const { Organisation, Team } = require('@pubsweet/models')
const User = require('../../../models/user/user')

const { SearchService, Publisher } = require('../../../services')

const getOrganisations = async (_, args) => {
  const searchedOrganisations = new SearchService(Organisation, {
    filter: get(args, 'input.search.criteria'),
    sort: get(args, 'input.sort'),
  })

  let results = await searchedOrganisations.search()

  results = results.map(user => {
    user.teams = (user.teams || []).filter(team => team.role !== 'user')
    return user
  })

  return {
    metadata: searchedOrganisations.metadata,
    results,
  }
}

// Returns the organizations that this organization can set as publishers
// If the org is only a publisher org, then they need to choose themselves
const publisherOptions = async organization => {
  if (!organization.settings.type.funder) {
    return [organization]
  }

  return Organisation.query().whereJsonSupersetOf('settings:type', {
    publisher: true,
  })
}

const createOrganisation = async (_, { input }, ctx) => {
  const publisher = new Publisher({ input, userId: ctx.user })
  return publisher.executeCommand('create')
}

const organisationTeams = async (organisation, __, ctx) =>
  ctx.connectors.Team.model.query().where({
    objectId: organisation.id,
  })

const organisationCollections = async (organisation, __, ctx) =>
  ctx.connectors.Collection.model
    .query()
    .where({
      organisationId: organisation.id,
    })
    .whereJsonSupersetOf('settings', {
      isCollectionGroup: false,
    })
    .orderBy('title')

const getOrganisation = async (_, args, ctx) => {
  const { id } = args
  return Organisation.query().findById(id).throwIfNotFound()
}

const organisationUsersQuery = organisationId => {
  const query = User.query()
    .leftJoin('team_members', 'users.id', 'team_members.user_id')
    .leftJoin('teams', 'teams.id', 'team_members.team_id')
    .where({
      'teams.objectId': organisationId,
      'teams.role': 'user',
    })
    .whereNot('team_members.status', 'rejected')
    .orderByRaw('lower("given_name"), lower("surname"), lower("username")')

  return query
}

const getOrganisationUsers = async organisation => {
  const query = organisationUsersQuery(organisation.id)
  const results = await query

  return {
    metadata: { skip: 0, take: 10, total: results.length },
    results,
  }
}

const searchOrganisationUsers = async (_, args, ctx) => {
  const { organisationId, searchValue } = args

  const query = organisationUsersQuery(organisationId)

  if (searchValue) {
    // ilike email explicitly otherwise it only works with an exact full match
    query.andWhere(builder => {
      builder.whereRaw(`document @@ websearch_to_tsquery('${searchValue}:*')`)
      builder.orWhere('email', 'ilike', `%${searchValue}%`)
    })
  }

  const results = await query

  return {
    metadata: { skip: 0, take: 10, total: results.length },
    results,
  }
}

const updateOrganisation = async (_, { input }, ctx) => {
  const publisher = new Publisher({ input, userId: ctx.user })

  return publisher.executeCommand('update')
}

const deleteOrganisation = async (_, { id }, ctx) => {
  await Team.query()
    .delete()
    .whereIn('object_id', id)
    .andWhere(query => {
      query.where('object_type', '=', 'Organisation')
    })

  await Organisation.query().delete().whereIn('id', id)
  return id
}

const checkAll = async (_, { input, action }, ctx) => {
  if (action === 'DELETE') {
    await Team.query()
      .delete()
      .andWhere(query => {
        query.where('object_type', '=', 'Organisation')
      })

    await Organisation.query().delete()

    return 'deleted'
  }

  if (action === 'UPDATE') {
    await Organisation.query().skipUndefined().patch(input)

    return 'updated'
  }

  return true
}

module.exports = {
  Mutation: {
    checkAll,
    createOrganisation,
    deleteOrganisation,
    updateOrganisation,
  },
  Query: {
    getOrganisation,
    getOrganisations,
    searchOrganisationUsers,
  },
  Organisation: {
    collections: organisationCollections,
    publisherOptions,
    teams: organisationTeams,
    users: getOrganisationUsers,
  },
}
