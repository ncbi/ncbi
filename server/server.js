/* eslint-disable no-return-await */
/* eslint-disable no-await-in-loop */
const { app } = require('@coko/server')

const connectKafka = require('./services/kafkaService/kafka')

const ScheduledTaskService = require('./services/scheduledTaskService')

const PgBossService = require('./services/pgBossService/pgBossService')

connectKafka()

// eslint-disable-next-line padding-line-between-statements
;(async () => {
  await PgBossService.getInstance()
})()

// ScheduledTaskService.sendTocXML('0 2 * * *', { scheduled: true })

ScheduledTaskService.forcefailChapters('*/5 * * * *', { scheduled: true })

module.exports = app
