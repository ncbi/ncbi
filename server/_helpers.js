const { cloneDeep, set, isEmpty, isNil } = require('lodash')
const db = require('@pubsweet/db-manager/src/db')
const moment = require('moment')
const sax = require('sax')

/**
 * Flattens the keys of an object
 *
 * eg.
 *
 * Initial: {
 *    one: 'one',
 *    two: {
 *      three: 'three',
 *      four: 'four'
 *    }
 * }
 *
 * Final: {
 *    one: 'one',
 *    'two.three': 'three',
 *    'two.four': 'four'
 * }
 *
 * https://stackoverflow.com/questions/19098797/fastest-way-to-flatten-un-flatten-nested-json-objects/19101235#19101235
 */

const flattenKeys = data => {
  const result = {}

  const recurse = (cur, prop) => {
    if (Object(cur) !== cur) {
      result[prop] = cur
    } else if (Array.isArray(cur)) {
      cur.forEach(i => recurse(cur[i], `${prop}[${i}]`))
      if (cur.length === 0) result[prop] = []
    } else {
      let empty = true

      Object.keys(cur).forEach(p => {
        empty = false
        recurse(cur[p], prop ? `${prop}.${p}` : p)
      })

      if (empty && prop) result[prop] = {}
    }
  }

  recurse(cloneDeep(data), '')
  return result
}

/**
 * Reverses flattened keys
 */
const unflattenKeys = data => {
  const values = cloneDeep(data)

  const obj = {}

  Object.keys(values).forEach(key => {
    set(obj, key, values[key])
  })

  return obj
}

const makeRandomString = length => {
  let result = ''
  const characters = 'abcdefghijklmnopqrstuvwxyz'
  const charactersLength = characters.length

  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }

  return result
}

const getNewAlias = async ({ from = false, withVersion = true }) => {
  if (from) {
    const [sequenceId, currentVersion] = from.split('.')
    const newVersion = parseInt(currentVersion, 10) + 1

    return withVersion ? `${sequenceId}.${newVersion}` : sequenceId.toString()
  }

  const {
    rows: [{ nextval }],
  } = await db.raw(`SELECT NEXTVAL('public.universal_bcms_id')`)

  return withVersion ? `${nextval}.1` : nextval.toString()
}

const getAliasWithoutVersion = () => getNewAlias({ withVersion: false })

const replaceSpecialChars = str => {
  return str.toString().replaceAll('&', '&amp;')
}

const isStringNotEmptyOrNull = str => {
  return !isEmpty(str) && !isNil(str)
}

const getAllProcessingInstructions = content => {
  const strict = true
  const parser = sax.parser(strict)

  const processingInstructions = []

  parser.onprocessinginstruction = attr => {
    processingInstructions.push(attr.name)
  }

  parser.write(content)

  return processingInstructions
}

const formatDate = inputDate => {
  // Parse the input date string
  const parsedDate = moment(inputDate, [
    'YYYY',
    'YYYY-MM',
    'YYYY-M',
    'YYYY-MM-DD',
    'YYYY-M-DD',
    'YYYY-MM-D',
  ])

  // Check if the parsed date is valid
  if (parsedDate.isValid()) {
    // Format the date to "yyyymmdd"
    return parsedDate.format('YYYYMMDD')
  }

  return false
}

module.exports = {
  formatDate,
  flattenKeys,
  makeRandomString,
  unflattenKeys,
  getNewAlias,
  getAliasWithoutVersion,
  replaceSpecialChars,
  isStringNotEmptyOrNull,
  getAllProcessingInstructions,
}
