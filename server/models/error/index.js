/* eslint-disable global-require */
const model = require('./errors')

module.exports = {
  model,
  modelName: 'Errors',
}
