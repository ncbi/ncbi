const BaseModel = require('@pubsweet/base-model')

const { idNullable } = require('../_helpers/types')

class Errors extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'errors'
  }

  static get tableName() {
    return 'errors'
  }

  static get schema() {
    return {
      properties: {
        assignee: {
          type: ['string', 'null'],
        },
        objectId: idNullable,
        message: {
          type: ['string', 'null'],
        },
        noticeTypeName: {
          type: ['string', 'null'],
        },
        severity: {
          type: ['string', 'null'],
        },
        errorType: {
          type: ['string', 'null'],
        },
        errorCategory: {
          type: ['string', 'null'],
        },
        jobId: {
          type: ['string', 'null'],
        },
        history: {
          default: false,
          type: ['boolean', false],
        },
      },
      type: 'object',
    }
  }
}

module.exports = Errors
