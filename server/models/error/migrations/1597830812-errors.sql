create table errors (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,

  -- own
  notice_type_name text,
  severity text,
  assignee text,
  error_type text,
  message text,
  error_category text,
  history boolean default false,
  book_component_id uuid REFERENCES book_component(id) INITIALLY DEFERRED, 
  job_id text
);
