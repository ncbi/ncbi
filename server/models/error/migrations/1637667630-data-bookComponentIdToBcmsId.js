/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Errors = require(`${process.cwd()}/server/models/error/errors`)
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const errors = await Errors.query(trx)

      // eslint-disable-next-line no-plusplus
      for (let i = 0, len = errors.length; i < len; i++) {
        // eslint-disable-next-line no-await-in-loop
        const bookComponent = await BookComponent.query(trx).findOne({
          id: errors[i].bookComponentId,
        })

        if (bookComponent) {
          // eslint-disable-next-line no-await-in-loop
          await Errors.query(trx)
            .patch({ bcmsId: bookComponent.alias })
            .findOne({ id: errors[i].id })
        }
      }
    })
  } catch (error) {
    throw new Error(error)
  }
}
