/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-return-await */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Errors = require(`${process.cwd()}/server/models/error/errors`)
const Book = require(`${process.cwd()}/server/models/book/book`)
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)
const Toc = require(`${process.cwd()}/server/models/toc/toc`)

exports.up = async knex => {
  await addObjectIdColumn(knex)
  await populateObjectIdsFromBcmsId(knex)
  await dropBcmsIdColumn(knex)
}

const addObjectIdColumn = knex =>
  knex.schema.hasColumn('errors', 'object_id').then(exists => {
    if (!exists) {
      Promise.all([
        knex.schema.table('errors', table => {
          table.uuid('object_id').nullable()
        }),
      ])
    }
  })

const populateObjectIdsFromBcmsId = async knex => {
  const findRelatedObject = async error => {
    const { bcmsId: alias } = error

    const book = await Book.query().findOne({ alias })

    if (book) {
      return book
    }

    const bookComponent = await BookComponent.query().findOne({ alias })

    if (bookComponent) {
      return bookComponent
    }

    const toc = await Toc.query().findOne({ alias })

    if (toc) {
      return toc
    }

    return null
  }

  try {
    return useTransaction(async trx => {
      const errors = await Errors.query(trx).whereNotNull('bcms_id')

      return Promise.all(
        errors.map(async error => {
          const related = await findRelatedObject(error) // Book, BookComponent, or Toc.

          if (related) {
            return error.$query(trx).patch({ objectId: related.id })
          }

          return error
        }),
      )
    })
  } catch (err) {
    throw new Error(err)
  }
}

const dropBcmsIdColumn = knex =>
  knex.schema.table('errors', table => {
    table.dropColumn('bcms_id')
  })
