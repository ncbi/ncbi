/* eslint-disable import/no-unresolved */
/* eslint-disable no-return-await */
/* eslint-disable global-require */

const { Model } = require('objection')
const BaseModel = require('@pubsweet/base-model')
const uuid = require('uuid')

const {
  idNullable,
  stringNullableDefaultEmpty,
  stringNullable,
} = require('../_helpers/types')

class Toc extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'toc'
  }

  static get tableName() {
    return 'tocs'
  }

  static get relationMappings() {
    const { model: Book } = require('../book')
    const { model: fileVersionModel } = require('../fileVersion')
    const { model: FileModel } = require('../file')
    const { model: Collection } = require('../collection')

    const {
      models: [{ model: User }],
    } = require('@pubsweet/model-user')

    return {
      book: {
        join: {
          from: 'tocs.book_id',
          to: 'book.id',
        },
        modelClass: Book,
        relation: Model.BelongsToOneRelation,
      },
      files: {
        join: {
          from: 'tocs.id',
          to: 'file_versions.toc_id',
        },
        modelClass: fileVersionModel,
        relation: BaseModel.HasManyRelation,
      },
      collection: {
        join: {
          from: 'tocs.collection_id',
          to: 'collections.id',
        },
        modelClass: Collection,
        relation: Model.BelongsToOneRelation,
      },
      owner: {
        join: {
          from: 'book_component.owner_id',
          to: 'users.id',
        },
        modelClass: User,
        relation: Model.HasOneRelation,
      },
      tocFiles: {
        join: {
          from: 'tocs.id',
          through: {
            from: 'file_versions.toc_id',
            to: 'file_versions.file_id',
          },
          to: 'files.id',
        },
        modelClass: FileModel,
        relation: BaseModel.ManyToManyRelation,
      },
    }
  }

  static get schema() {
    return {
      type: 'object',
      required: ['bookId', 'title'],
      properties: {
        alias: stringNullable,
        metadata: {
          type: 'object',
          additionalProperties: false,
          default: {
            fileId: null,
            filename: '',
            url: null,
          },
          properties: {
            url: stringNullableDefaultEmpty,
            fileId: idNullable,
            filename: stringNullableDefaultEmpty,
          },
        },
        title: stringNullable,
        bookId: stringNullable,
        collectionId: stringNullable,
        ownerId: stringNullable,
        publishedDate: stringNullable,
        status: {
          type: 'string',
          default: 'new-upload',
          enum: [
            'submission-errors',
            'new-upload',
            'converting',
            'tagging',
            'failed',
            'conversion-errors',
            'loading-preview',
            'loading-errors',
            'preview',
            'in-review',
            'approve',
            'unpublished',
            'published',
            'publishing',
            'publish-failed',
            'error', // TODO deprecated after changing the errors
            'new-version',
          ],
        },
        versionName: {
          type: 'string',
          default: '1',
        },
      },
    }
  }

  static getFiles() {
    return Toc.relatedQuery('tocFiles').select([
      'files.id',
      'files.created',
      'files.updated',
      'files.mimeType',
      'files.name',
      'files.category',
      'file_versions.copied',
      'file_versions.tag',
      'file_versions.version_name',
      'file_versions.id as bcfId',
      'file_versions.parentId',
      'file_versions.status',
      'file_versions.ownerId',
    ])
  }

  async addFile(file, category, status = null, transaction = null) {
    const { model: FileVersion } = require('../fileVersion')

    const VersionQuery = FileVersion.query(transaction)
      .alias('fileVersion')
      .joinEager('file')
      .where({
        tocId: this.id,
        'fileVersion.category': category,
      })

    if (category === 'source') {
      VersionQuery.andWhere(builder => {
        builder.where('name', 'like', `${file.name.replace(/\.[^/.]+$/, '')}.%`)
      })
    } else {
      VersionQuery.andWhere(builder => {
        builder.where({ name: file.name })
      })
    }

    const FileVersionExists = await VersionQuery

    let parentId = uuid.v4()
    let versionName = '1'
    let maxElement = { tag: null }

    if (FileVersionExists.length > 0) {
      parentId = FileVersionExists[0].parentId
      versionName = (FileVersionExists.length + 1).toString()

      maxElement = FileVersionExists.reduce(
        (prev, current) =>
          Number(prev.versionName) > Number(current.versionName)
            ? prev
            : current,
        1,
      )
    }

    return await Toc.relatedQuery('files', transaction)
      .for(this)
      .insert({
        bookComponentId: null,
        bookComponentVersionId: null,
        category,
        fileId: file.id,
        status,
        parentId,
        ownerId: file.ownerId,
        versionName,
        tag: file.tag || maxElement.tag,
      })
      .returning('*')
  }

  static async latestComponentFiles(entity, category = null, trx) {
    const { FileVersion } = require('@pubsweet/models')

    const search = {
      tocId: entity.id,
    }

    if (category) {
      search['file_versions.category'] = category
    }

    // eslint-disable-next-line no-return-await
    return await FileVersion.query(trx)
      .select(['*'])
      .distinctOn('file_versions.parent_id')
      .joinEager('file')
      .where(search)
      .orderByRaw(
        '"file_versions"."parent_id" , "file_versions"."created" desc',
      )
  }

  async hasPublishedComponents(trx) {
    const { model: Book } = require('../book')
    const { model: BookComponent } = require('../bookComponent')
    const { model: Collection } = require('../collection')

    let books = []

    if (this.collectionId) {
      const collection = await Collection.query(trx).findOne({
        id: this.collectionId,
      })

      books = await Promise.all(
        collection.components.map(
          async component => await Book.query(trx).findOne({ id: component }),
        ),
      )
    } else if (this.bookId) {
      books = await Book.query(trx).where({ id: this.bookId })
    }

    const hasPublishedBookOrToc = await Promise.all(
      books.map(async book => {
        const isWholeBook = !book.settings.chapterIndependently

        if (isWholeBook) {
          return book.status === 'published'
        }

        // If this is a collection TOC, check if the
        // chapter-processed book's TOC has been published.
        if (this.collectionId) {
          const bookToc = await Toc.query(trx).findOne({ bookId: book.id })
          return bookToc.status === 'published'
        }

        // If this is a chapter-processed book's TOC, check
        // if at least one book component has been published.
        const bookComponents = await BookComponent.query(trx).where({
          bookId: book.id,
        })

        return bookComponents.some(
          bookComponent => bookComponent.status === 'published',
        )
      }),
    )

    return hasPublishedBookOrToc.includes(true)
  }
}

module.exports = Toc
