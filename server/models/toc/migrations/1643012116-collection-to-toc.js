/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Collection = require(`${process.cwd()}/server/models/collection/collection`)
const Toc = require(`${process.cwd()}/server/models/toc/toc.js`)
const FileService = require(`${process.cwd()}/server/services/file/fileService`)
const XmlFactory = require(`${process.cwd()}/server/services/xmlBaseModel/xmlFactory`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const Collections = await Collection.query(trx)

      const collectionWithoutGroups = Collections.filter(
        col => !col.settings.isCollectionGroup,
      )

      // eslint-disable-next-line no-plusplus
      for (let i = 0, len = collectionWithoutGroups.length; i < len; i++) {
        // eslint-disable-next-line no-await-in-loop
        const toc = await Toc.query(trx).insertAndFetch({
          bookId: null,
          collectionId: collectionWithoutGroups[i].id,
          status: 'unpublished',
          title: 'Table of contents',
          ownerId: collectionWithoutGroups[i].ownerId,
        })

        if (toc) {
          // eslint-disable-next-line no-await-in-loop
          const content = await XmlFactory.createCollectionXml(
            collectionWithoutGroups[i],
          )

          // eslint-disable-next-line no-await-in-loop
          const file = await FileService.write({
            content,
            filename: `${collectionWithoutGroups[i].domain}.xml`,
            category: 'converted',
          })

          // eslint-disable-next-line no-await-in-loop
          await toc.addFile(
            { ...file, ownerId: toc.ownerId },
            'converted',
            'loading-preview',
          )
        }
      }
    })
  } catch (error) {
    throw new Error(error)
  }
}
