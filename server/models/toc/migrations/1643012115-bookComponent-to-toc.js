/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-await-in-loop */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)
const Toc = require(`${process.cwd()}/server/models/toc/toc.js`)
const FileService = require(`${process.cwd()}/server/services/file/fileService`)
const XmlFactory = require(`${process.cwd()}/server/services/xmlBaseModel/xmlFactory`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const BookComponentTocs = await BookComponent.query(trx).where({
        componentType: 'toc',
      })

      // eslint-disable-next-line no-plusplus
      for (let i = 0, len = BookComponentTocs.length; i < len; i++) {
        // eslint-disable-next-line no-await-in-loop
        const toc = await Toc.query(trx).insertAndFetch({
          bookId: BookComponentTocs[i].bookId,
          collectionId: null,
          status: 'unpublished',
          title: 'Table of contents',
          ownerId: BookComponentTocs[i].ownerId,
        })

        if (toc) {
          const content = await XmlFactory.createTocXml(
            BookComponentTocs[i].bookId,
          )

          const file = await FileService.write({
            content,
            filename: 'TOC.xml',
            category: 'converted',
          })

          await toc.addFile(
            { ...file, ownerId: toc.ownerId },
            'converted',
            'loading-preview',
          )
        }
      }
    })
  } catch (error) {
    throw new Error(error)
  }
}
