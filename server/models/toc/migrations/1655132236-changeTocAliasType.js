/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Toc = require(`${process.cwd()}/server/models/toc/toc`)

exports.up = async knex => {
  await changeAliasColumnType(knex)
  await updateTocAliasValue(knex)
}

const changeAliasColumnType = knex =>
  knex.schema.table('tocs', table => {
    table.string('alias').alter()
  })

const updateTocAliasValue = async knex => {
  const getNewAliasValue = toc =>
    toc.versionName ? `${toc.alias}.${toc.versionName}` : `${toc.alias}.1`

  try {
    return useTransaction(async trx => {
      const tocs = await Toc.query(trx)

      return Promise.all(
        tocs.map(toc => {
          const alias = getNewAliasValue(toc)
          return toc.$query(trx).patch({ alias })
        }),
      )
    })
  } catch (err) {
    throw new Error(err)
  }
}
