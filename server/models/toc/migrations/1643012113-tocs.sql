CREATE SEQUENCE IF NOT EXISTS universal_bcms_id;

create table tocs (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,
  
  -- foreign
  owner_id uuid references users,
  collection_id uuid references collections,
  book_id uuid references books,

  -- own
  title text,
  status text,
  version_name text,
  published_date timestamp with time zone default null,
  metadata jsonb,
  alias integer NOT NULL DEFAULT nextval('universal_bcms_id')
);
