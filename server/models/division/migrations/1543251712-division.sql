create table divisions (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,
  
  --foreign
  book_id uuid not null references books,
  /* Same note as divisions in book */
  book_components jsonb not null,

  --own
  label text not null
);
