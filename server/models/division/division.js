/* eslint-disable global-require */
const { Model } = require('objection')
const BaseModel = require('@pubsweet/base-model')
const _ = require('lodash')

class Division extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'division'
  }

  static get orderMappings() {
    const { model: Book } = require('../book')
    const { model: BookComponent } = require('../bookComponent')

    return {
      field: {
        idColumn: 'id',
        column: 'bookComponents',
        foreignColumn: 'id',
        modelClass: BookComponent,
      },
      predicate: {
        modelClass: Book,
        idColumn: 'id',
        foreignColumn: 'bookId',
        field: book => {
          const value = (book.settings.toc || {}).order_chapters_by || 'manual'
          const numberValues = { number: 'asc', number_desc: 'desc' }

          if (_.has(numberValues, value))
            return {
              field: 'metadata.chapter_number',
              type: 'Number',
              order: numberValues[value],
              value: v => (v ? v.replace(/\D/g, '') : 0),
            }
          // Sort by publication date, updated date, creation date in that order
          // NB: these are the dates relating to the real world book/chapter
          // and NOT the dates they were created or published to the database
          if (value === 'date_desc')
            return {
              field: chapter => {
                return (
                  _.get(chapter, 'metadata.date_publication', null) ||
                  _.get(chapter, 'metadata.date_updated', null) ||
                  _.get(chapter, 'metadata.date_created', null)
                )
              },
              type: 'Date',
              order: 'desc',
            }
          if (value === 'title')
            return {
              field: 'title',
              type: 'String',
              order: 'asc',
              value: v => {
                return v === 'Untitled' ? null : v
              },
            }

          return {
            field: value,
            type: 'String',
            order: 'asc',
          }
        },
      },
    }
  }

  static get tableName() {
    return 'divisions'
  }

  static get schema() {
    return {
      properties: {
        bookComponents: {
          default: [],
          items: {
            format: 'uuid',
            type: 'string',
          },
          type: 'array',
        },
        bookId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
        label: {
          minLength: 1,
          type: 'string',
        },
      },
      required: ['bookId', 'label'],
      type: 'object',
    }
  }

  static get relationMappings() {
    const { model: Book } = require('../book')

    return {
      book: {
        join: {
          from: 'Division.bookId',
          to: 'Book.id',
        },
        modelClass: Book,
        relation: Model.BelongsToOneRelation,
      },
    }
  }

  getBook() {
    return this.$relatedQuery('book')
  }

  static get DivisionConfig() {
    return ['frontMatter', 'body', 'backMatter']
  }
}

module.exports = Division
