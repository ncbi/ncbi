const path = require('path')

const pathToComponent = path.resolve(__dirname, '..', 'division')
const pathToBook = path.resolve(__dirname, '..', '..', 'book')
const pathToOrganisation = path.resolve(__dirname, '..', '..', 'organisation')

process.env.NODE_CONFIG = `{"pubsweet":{
  "components":[
    "@pubsweet/model-user",
    "@pubsweet/model-team",
    "${pathToComponent}",
    "${pathToBook}",
    "${pathToOrganisation}"
  ]
}}`

const Division = require('../division')

const { model: Book } = require('../../book')
const { model: Organisation } = require('../../organisation')

const dbCleaner = require('../../helpers/db_cleaner')

let book

describe('Division', () => {
  beforeEach(async () => {
    await dbCleaner()
    const organisation = await new Organisation({ name: 'Test' }).save()

    book = await new Book({
      organisationId: organisation.id,
      title: 'Test',
    }).save()
  })

  it('has updated set when created', async () => {
    const division = await new Division({
      bookId: book.id,
      label: 'Test',
    }).save()

    expect(division.label).toEqual('Test')
    const now = new Date().toISOString()
    expect(division.updated.toISOString()).toHaveLength(now.length)
  })

  it('can be saved and found and deleted', async () => {
    const division = await new Division({
      bookId: book.id,
      label: 'Test',
    }).save()

    expect(division.label).toEqual('Test')

    const foundDivision = await Division.find(division.id)
    expect(foundDivision.label).toEqual('Test')

    await foundDivision.delete()

    async function tryToFind() {
      await Division.find(foundDivision.id)
    }

    await expect(tryToFind()).rejects.toThrow('Object not found')
  })

  it('can be found by property', async () => {
    await new Division({ bookId: book.id, label: 'Test' }).save()
    const division = await Division.findOneByField('label', 'Test')
    expect(division.label).toEqual('Test')

    let divisions = await Division.findByField('label', 'Test')
    expect(divisions[0].label).toEqual('Test')

    async function findMissing() {
      await Division.findOneByField('label', 'Does not exist')
    }

    await expect(findMissing()).rejects.toThrow('Object not found')

    divisions = await Division.findByField('label', 'Does not exist')
    expect(divisions).toEqual([])
  })

  it('can not be saved with non-valid properties', async () => {
    async function createNonValidDivision() {
      await new Division({ mumbo: 'jumbo' }).save()
    }

    await expect(createNonValidDivision()).rejects.toThrow(
      'mumbo: is an invalid additional property',
    )
  })

  it('can save new entity with known ID', async () => {
    const id = '1838d074-fb9d-4ed6-9c63-39e6bc7429ce'

    const division = await new Division({
      bookId: book.id,
      id,
      label: 'Test',
    }).save()

    expect(division.id).toEqual(id)
  })
})
