exports.up = async knex => {
  return knex.schema.alterTable('users', table => {
    table.dropUnique([], 'users_email_key')
  })
}

exports.down = async knex => {
  return knex.schema.alterTable('users', table => {
    table.unique(['email'])
  })
}
