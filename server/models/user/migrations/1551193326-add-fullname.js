const addUserIndex = `
ALTER TABLE public.users ADD "document" tsvector;
CREATE FUNCTION update_search_document()
RETURNS trigger AS $$
BEGIN
  NEW.document := to_tsvector(NEW."given_name" || ' ' || NEW.email || ' ' || NEW.surname || ' ' || NEW.username);
  RETURN NEW;
END $$ LANGUAGE 'plpgsql';
CREATE TRIGGER update_search_document
BEFORE INSERT ON public.users
FOR EACH ROW
EXECUTE PROCEDURE update_search_document();
CREATE INDEX idx_fts_user ON public.users USING gin(document);
`

exports.up = async knex =>
  Promise.all([
    knex.schema
      .table('users', table => {
        table.string('givenName')
        table.string('surname')
      })
      .then(() => knex.schema.raw(addUserIndex)),
  ])

const removeUserIndex = `
  DROP FUNCTION IF EXISTS update_search_document();
`

exports.down = knex =>
  Promise.all([
    knex.schema.dropTableIfExists('users'),
    knex.schema.raw(removeUserIndex),
  ])
