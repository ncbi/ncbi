const {
  models: [{ model: User }],
} = require('@pubsweet/model-user')

const { ValidationError } = require('@pubsweet/errors')
const { logger } = require('@coko/server')

const TeamMember = require('../team/teamMember')

class NcbiUser extends User {
  static get virtualAttributes() {
    return ['status', 'verified']
  }

  static get schema() {
    return {
      properties: {
        givenName: {
          type: 'string',
        },
        surname: {
          type: 'string',
        },
        ncbiAccountId: {
          type: 'string',
        },
        pdf2xmlVendor: {
          type: ['boolean', false],
        },
      },
      type: 'object',
    }
  }

  static async findById(id) {
    return this.find(id)
  }

  static async updatePassword(userId, currentPassword, newPassword) {
    const user = await User.query().findById(userId)
    const isCurrentPasswordValid = await user.validPassword(currentPassword)

    if (!isCurrentPasswordValid) {
      throw new ValidationError(
        'Update password: Current password is not valid',
      )
    }

    if (await user.validPassword(newPassword)) {
      throw new ValidationError(
        'Update password: New password must be different from current password',
      )
    }

    const passwordHash = await User.hashPassword(newPassword)

    return user.$query().patchAndFetch({
      passwordHash,
    })
  }

  static async hasGlobalRole(userId, role) {
    try {
      const isMember = await TeamMember.query()
        .leftJoin('teams', 'team_members.teamId', 'teams.id')
        .findOne({
          global: true,
          role,
          userId,
        })

      return !!isMember
    } catch (e) {
      logger.error('User model: hasGlobalRole failed', e)
      throw new Error(e)
    }
  }

  async hasGlobalRole(role) {
    return NcbiUser.hasGlobalRole(this.id, role)
  }

  static async hasRoleOnObject(userId, role, objectId) {
    try {
      const isMember = await TeamMember.query()
        .leftJoin('teams', 'team_members.teamId', 'teams.id')
        .findOne({
          role,
          userId,
          objectId,
        })

      return !!isMember
    } catch (e) {
      logger.error('User model: hasRoleOnObject failed', e)
      throw new Error(e)
    }
  }

  async hasRoleOnObject(role, objectId) {
    return NcbiUser.hasRoleOnObject(this.id, role, objectId)
  }

  static async findObjectsThatUserHasRoleOn(userId, role, objectType, status) {
    try {
      const where = {
        role,
        objectType,
        userId,
      }

      if (status) where.status = status

      const isMember = await TeamMember.query()
        .select('objectId')
        .leftJoin('teams', 'team_members.teamId', 'teams.id')
        .where(where)

      return isMember.map(item => item.objectId)
    } catch (e) {
      logger.error('User model: findObjectsThatUserHasRoleOn failed', e)
      throw new Error(e)
    }
  }

  async findObjectsThatUserHasRoleOn(role, objectType, status) {
    return NcbiUser.findObjectsThatUserHasRoleOn(
      this.id,
      role,
      objectType,
      status,
    )
  }
}

module.exports = NcbiUser
