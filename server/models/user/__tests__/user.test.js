const { Team } = require('@pubsweet/models')
// const { v4: uuid } = require('uuid')

const clearDb = require('../../_helpers/clearDb')

const Book = require('../../book/book')
const Organisation = require('../../organisation/organisation')
const TeamMember = require('../../team/teamMember')
const User = require('../user')

describe('User model', () => {
  beforeAll(() => clearDb())
  afterEach(() => clearDb())

  afterAll(() => {
    const knex = User.knex()
    knex.destroy()
  })

  test('has global role', async () => {
    const user = await User.query().insert({})

    const globalTeam = await Team.query().insert({
      role: 'sysAdmin',
      global: true,
    })

    await TeamMember.query().insert({
      userId: user.id,
      teamId: globalTeam.id,
    })

    const isUserAdminStatic = await User.hasGlobalRole(user.id, 'sysAdmin')
    expect(isUserAdminStatic).toBe(true)

    const isUserAdminNonStatic = await user.hasGlobalRole('sysAdmin')
    expect(isUserAdminNonStatic).toBe(true)
  })

  test('has role on object', async () => {
    const user = await User.query().insert({})
    const org = await Organisation.query().insert({})

    const book = await Book.query().insert({
      organisationId: org.id,
      title: 'My book',
    })

    const team = await Team.query().insert({
      role: 'author',
      objectId: book.id,
      objectType: 'book',
    })

    await TeamMember.query().insert({
      userId: user.id,
      teamId: team.id,
    })

    const isAuthorStatic = await User.hasRoleOnObject(
      user.id,
      'author',
      book.id,
    )

    expect(isAuthorStatic).toBe(true)

    const isAuthorNonStatic = await user.hasRoleOnObject('author', book.id)
    expect(isAuthorNonStatic).toBe(true)

    expect(true).toBeTruthy()
  })

  test('find objects that user has role on', async () => {
    const user = await User.query().insert({})
    const org = await Organisation.query().insert({})

    const book = await Book.query().insert({
      organisationId: org.id,
      title: 'My book',
    })

    const anotherBook = await Book.query().insert({
      organisationId: org.id,
      title: 'My other book',
    })

    const team = await Team.query().insert({
      role: 'author',
      objectId: book.id,
      objectType: 'book',
    })

    await TeamMember.query().insert({
      userId: user.id,
      teamId: team.id,
    })

    const authorObjectsStatic = await User.findObjectsThatUserHasRoleOn(
      user.id,
      'author',
      'book',
    )

    expect(authorObjectsStatic).toHaveLength(1)
    expect(authorObjectsStatic).toContain(book.id)
    expect(authorObjectsStatic).not.toContain(anotherBook.id)

    const authorObjectsNonStatic = await user.findObjectsThatUserHasRoleOn(
      'author',
      'book',
    )

    expect(authorObjectsNonStatic).toHaveLength(1)
    expect(authorObjectsNonStatic).toContain(book.id)
    expect(authorObjectsStatic).not.toContain(anotherBook.id)

    expect(true).toBeTruthy()
  })
})
