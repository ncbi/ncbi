/* eslint-disable no-return-await */
/* eslint-disable no-param-reassign */
/* eslint-disable global-require */
const { Model } = require('objection')
const uuid = require('uuid')

const cloneDeep = require('lodash/cloneDeep')

const BaseModel = require('@pubsweet/base-model')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const {
  models: [{ model: Team }],
} = require('@pubsweet/model-team')

// const { StatusService } = require('../../services/statusService/statusService')
const {
  boolean,
  booleanDefaultFalse,
  booleanNullable,
  id,
  idNullable,
  string,
  stringNullable,
  stringNullableDefaultNull,
  stringNullableDefaultEmpty,
  arrayOfStringsDefaultEmpty,
} = require('../_helpers/types')

class Book extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'book'
  }

  static get tableName() {
    return 'books'
  }

  // static async beforeInsert({ transaction, inputItems }) {
  //   super.beforeInsert()
  //   const item = [inputItems]

  //   if (item.status) {
  //     this.status = await StatusService.updateEntity(item, transaction)
  //   }
  // }

  // static async beforeUpdate({ transaction, asFindQuery }) {
  //   super.beforeUpdate()

  //   if (this.status) {
  //     const [entity] = await asFindQuery()
  //     this.status = await StatusService.updateEntity(entity, transaction)
  //   }
  // }

  static get relationMappings() {
    const { model: Organisation } = require('../organisation')
    const { model: File } = require('../file')
    const { model: fileVersionModel } = require('../fileVersion')
    const { model: Division } = require('../division')
    const { model: Collection } = require('../collection')

    return {
      divisions: {
        join: {
          from: 'books.id',
          to: 'divisions.book_id',
        },
        modelClass: Division,
        relation: Model.HasManyRelation,
      },
      collection: {
        join: {
          from: 'books.collection_id',
          to: 'collections.id',
        },
        modelClass: Collection,
        relation: Model.BelongsToOneRelation,
      },
      files: {
        join: {
          from: 'books.id',
          to: 'file_versions.book_id',
        },
        modelClass: fileVersionModel,
        relation: BaseModel.HasManyRelation,
      },
      organisation: {
        join: {
          from: 'books.organisation_id',
          to: 'organisations.id',
        },
        modelClass: Organisation,
        relation: Model.BelongsToOneRelation,
      },
      bookCover: {
        join: {
          from: 'books.file_cover',
          to: 'files.id',
        },
        modelClass: File,
        relation: Model.HasOneRelation,
      },
      abstractCover: {
        join: {
          from: 'books.file_abstract',
          to: 'files.id',
        },
        modelClass: File,
        relation: Model.HasOneRelation,
      },
      bookComponentFiles: {
        join: {
          from: 'books.id',
          through: {
            from: 'file_versions.book_id',
            to: 'file_versions.file_id',
          },
          to: 'files.id',
        },
        modelClass: File,
        relation: BaseModel.ManyToManyRelation,
      },
    }
  }

  static get schema() {
    return {
      type: 'object',
      required: ['organisationId', 'title'],
      default: {
        agreement: false,
        publishedDate: null,
      },
      properties: {
        abstract: stringNullable,
        abstractTitle: stringNullable,
        agreement: boolean,
        alias: stringNullable,
        altTitle: stringNullable,
        bookSubmitId: stringNullable,
        collectionId: idNullable,
        divisions: {
          type: ['array', 'null'],
          default: [],
          items: idNullable,
        },
        domain: stringNullable,
        domainId: stringNullable,
        edition: stringNullable,
        fileAbstract: stringNullable,
        fileCover: idNullable,
        metadata: {
          type: 'object',
          additionalProperties: false,
          default: {
            author: [],
            collaborativeAuthors: [],
            copyrightHolder: '',
            copyrightStatement: '',
            copyrightYear: '',
            dateCreated: { day: null, month: null, year: null },
            dateUpdated: { day: null, month: null, year: null },
            doi: '',
            editor: [],
            grants: [],
            isbn: '',
            issn: '',
            language: 'en',
            licenseStatement: '',
            licenseType: '',
            licenseUrl: '',
            nlmId: '',
            notes: [],
            openAccess: false,
            openAccessLicense: false,
            otherLicenseType: '',
            pub_loc: '',
            pub_name: '',
            pubDate: {
              date: {
                day: null,
                month: null,
                year: null,
              },
              dateRange: {
                startYear: null,
                startMonth: null,
                endYear: null,
                endMonth: null,
              },
              dateType: null,
              publicationFormat: null,
            },
            sourceType: '',
            volume: '',
            url: null,
            fileId: null,
            filename: '',
          },
          properties: {
            author: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                additionalProperties: false,
                properties: {
                  affiliation: arrayOfStringsDefaultEmpty,
                  degrees: stringNullable,
                  email: stringNullable,
                  givenName: stringNullable,
                  role: stringNullable,
                  suffix: stringNullable,
                  surname: stringNullable,
                },
              },
            },
            collaborativeAuthors: {
              default: [],
              items: {
                type: 'object',
                additionalProperties: false,
                properties: {
                  givenName: stringNullable,
                },
              },
              type: 'array',
            },
            copyrightHolder: stringNullable,
            copyrightStatement: stringNullable,
            copyrightYear: stringNullable,
            dateCreated: {
              type: 'object',
              additionalProperties: false,
              default: {
                day: null,
                month: null,
                year: null,
              },
              properties: {
                day: stringNullable,
                month: stringNullable,
                year: stringNullable,
              },
            },
            dateUpdated: {
              type: 'object',
              additionalProperties: false,
              default: {
                day: null,
                month: null,
                year: null,
              },
              properties: {
                day: stringNullable,
                month: stringNullable,
                year: stringNullable,
              },
            },
            doi: stringNullable,
            editor: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                additionalProperties: false,
                properties: {
                  affiliation: arrayOfStringsDefaultEmpty,
                  degrees: stringNullable,
                  email: stringNullable,
                  givenName: stringNullable,
                  role: stringNullable,
                  suffix: stringNullable,
                  surname: stringNullable,
                },
              },
            },
            grants: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                additionalProperties: false,
                required: [
                  'id',
                  'country',
                  'institution_acronym',
                  'institution_code',
                  'institution_name',
                  'number',
                ],
                properties: {
                  id,
                  apply: booleanDefaultFalse,
                  country: string,
                  institution_acronym: string,
                  institution_code: string,
                  institution_name: string,
                  number: string,
                },
              },
            },
            isbn: string,
            issn: stringNullableDefaultNull,
            language: string,
            licenseStatement: stringNullable,
            licenseType: stringNullable,
            licenseUrl: stringNullable,
            nlmId: stringNullable,
            notes: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                additionalProperties: false,
                required: ['type', 'description'],
                properties: {
                  type: string,
                  title: stringNullable,
                  description: string,
                },
              },
            },
            openAccess: booleanNullable,
            openAccessLicense: booleanNullable,
            otherLicenseType: stringNullable,
            pub_loc: stringNullable,
            pub_name: stringNullable,
            pubDate: {
              type: 'object',
              additionalProperties: false,
              default: {
                dateType: null,
                publicationFormat: null,
                date: {
                  day: null,
                  month: null,
                  year: null,
                },
                dateRange: {
                  startYear: null,
                  startMonth: null,
                  endYear: null,
                  endMonth: null,
                },
              },
              properties: {
                date: {
                  type: 'object',
                  additionalProperties: false,
                  default: {
                    day: null,
                    month: null,
                    year: null,
                  },
                  properties: {
                    day: stringNullable,
                    month: stringNullable,
                    year: stringNullable,
                  },
                },
                dateRange: {
                  type: 'object',
                  additionalProperties: false,
                  default: {
                    startYear: null,
                    startMonth: null,
                    endYear: null,
                    endMonth: null,
                  },
                  properties: {
                    startMonth: stringNullable,
                    startYear: stringNullable,
                    endYear: stringNullable,
                    endMonth: stringNullable,
                  },
                },
                dateType: {
                  ...stringNullable,
                  default: null,
                  enum: ['pub', 'pubr', null],
                },
                publicationFormat: {
                  ...stringNullable,
                  default: null,
                  enum: ['electronic', 'print', null],
                },
              },
            },
            sourceType: stringNullable,
            volume: stringNullable,
            url: stringNullable,
            fileId: idNullable,
            filename: stringNullableDefaultEmpty,
          },
        },
        organisationId: id,
        ownerId: idNullable,
        publishedDate: stringNullable,
        settings: {
          type: 'object',
          additionalProperties: false,
          default: {
            approvalOrgAdminEditor: false,
            approvalPreviewer: false,
            bookLevelLinks: false,
            chapterIndependently: false,
            createPdf: false,
            createVersionLink: false,
            createWholebookPdf: false,
            multiplePublishedVersions: false,
            toc: {
              add_body_to_parts: false,
              allTitle: false,
              contributors: false,
              documentHistory: false,
              group_chapters_in_parts: false,
              order_chapters_by: 'manual',
              subtitle: false,
              tocMaxDepth: null,
            },
          },
          properties: {
            alternateVersionsPdf: boolean,
            alternateVersionsPdfBook: boolean,
            approvalOrgAdminEditor: boolean,
            approvalPreviewer: boolean,
            bookLevelAffiliationStyle: stringNullable,
            bookLevelLinks: boolean,
            bookLevelLinksMarkdown: stringNullable,
            chapterIndependently: boolean,
            chapterLevelAffiliationStyle: stringNullable,
            citationSelfUrl: stringNullable,
            citationType: stringNullable,
            createPdf: boolean,
            createVersionLink: boolean,
            createWholebookPdf: boolean,
            displayObjectsLocation: stringNullable,
            footnotesDecimal: boolean,
            formatChapterDate: stringNullable,
            formatPublicationDate: stringNullable,
            indexChaptersInPubmed: boolean,
            indexInPubmed: boolean,
            multiplePublishedVersions: boolean,
            publisher: id,
            publisherUrl: stringNullable,
            questionAnswerStyle: stringNullable,
            referenceListStyle: stringNullable,
            specialPublisherLinkText: stringNullable,
            specialPublisherLinkUrl: stringNullable,
            toc: {
              type: 'object',
              additionalProperties: false,
              // default: {
              //   add_body_to_parts: false,
              //   allTitle: false,
              //   contributors: false,
              //   documentHistory: false,
              //   group_chapters_in_parts: false,
              //   order_chapters_by: 'manual',
              //   subtitle: false,
              //   tocMaxDepth: null,
              // },
              properties: {
                add_body_to_parts: boolean,
                allTitle: boolean,
                contributors: boolean,
                documentHistory: boolean,
                group_chapters_in_parts: boolean,
                order_chapters_by: stringNullable,
                subtitle: boolean,
                tocMaxDepth: stringNullable,
              },
            },
            tocExpansionLevel: stringNullable,
            UKPMC: boolean,
            versionLinkText: stringNullable,
            versionLinkUri: stringNullable,
            xrefAnchorStyle: stringNullable,
            // qaStatus: 'journal_qa_status',
            // releaseStatus: 'live_in_pmc',
          },
        },
        status: {
          default: 'new-book',
          enum: [
            'new-book',
            'submission-errors',
            'new-upload',
            'in-production',
            'converting',
            'tagging',
            'failed',
            'conversion-errors',
            'loading-preview',
            'loading-errors',
            'preview',
            'in-review',
            'approve',
            'published',
            'publishing',
            'publish-failed',
            'error', // TODO deprecated after changing the errors
            'new-version',
            'tagging-errors',
          ],
          ...string,
        },
        subTitle: {
          type: 'array',
          default: [],
        },
        title: stringNullable,
        fundedContentType: stringNullable,
        workflow: {
          default: 'word',
          ...string,
        },
        version: {
          default: '1',
          ...stringNullable,
        },
        parentId: idNullable,
      },
    }
  }

  getOrganisation() {
    return this.$query().joinEager('organisation')
  }

  getDivisions(trx) {
    const { model: Division } = require('../division')

    return this.$query(trx)
      .joinEager('divisions')
      .orderByRaw(
        `array_position(array['${Division.DivisionConfig.join(
          "'::text,'",
        )}'::text], label)`,
      )
  }

  static getBookComponentsByVersion(bookId, versionName) {
    const { model: BookComponent } = require('../bookComponent')

    const whereClause = versionName
      ? { bookId, versionName }
      : { bookId, parentId: null }

    return BookComponent.query().where(whereClause)
  }

  getVersion(fundedContentType, version, transaction) {
    const whereClause = {
      fundedContentType,
      version,
    }

    const idToSearch = this.parentId || this.id

    return Book.query(transaction)
      .where(whereClause)
      .where(builder =>
        builder.where('parentId', idToSearch).orWhere('id', idToSearch),
      )
      .first()
  }

  isInitialVersion() {
    return !this.parentId
  }

  getRelatedVersions(args = {}, transaction = null) {
    const idToSearch = this.isInitialVersion() ? this.id : this.parentId
    const { limit = false, orderBy = ['created'] } = args

    const query = Book.query(transaction)
      .where('parentId', idToSearch)
      .orWhere('id', idToSearch)
      .orderBy(...orderBy)

    if (limit) {
      query.limit(limit)
    }

    return query
  }

  async getLatestBookVersion(transaction = null) {
    return this.getRelatedVersions(
      {
        limit: 1,
        orderBy: ['created', 'desc'],
      },
      transaction,
    ).first()
  }

  getCollection(trx) {
    return this.$query(trx).joinEager('collection')
  }

  getFile(trx) {
    return this.$query(trx).joinEager('bookCover')
  }

  getFileAbstract() {
    return this.$query().joinEager('abstractCover')
  }

  static getFiles() {
    return Book.relatedQuery('bookComponentFiles').select([
      'files.id',
      'files.created',
      'files.updated',
      'files.mimeType',
      'files.name',
      'files.category',
      'file_versions.copied',
      'file_versions.tag',
      'file_versions.version_name',
      'file_versions.bookId',
      'file_versions.id as bcfId',
      'file_versions.parentId',
      'file_versions.status',
      'file_versions.ownerId',
    ])
  }

  static async publishedVersions(book, trx) {
    const parentId = book.parentId ? book.parentId : book.id

    const bookComponentModel = await Book.query(trx)
      .where({
        parentId,
      })
      .orWhere({
        id: parentId,
        parentId: null,
      })
      .orderByRaw('books.version::int asc')

    return bookComponentModel
  }

  static async hasPublishedVersions(book, trx) {
    const { model: FileVersion } = require('../fileVersion')

    const BookComponentVersions = await Book.publishedVersions(book, trx)

    // eslint-disable-next-line no-return-await
    return await FileVersion.query(trx)
      .whereIn(
        'bookId',
        BookComponentVersions.map(bc => bc.id),
      )
      .andWhere(query => {
        query.where({ status: 'published' })
      })
  }

  static async latestComponentFiles(entity, category = null, trx) {
    const { FileVersion } = require('@pubsweet/models')

    const search = {
      bookId: entity.id,
    }

    if (category) {
      search['file_versions.category'] = category
    }

    // eslint-disable-next-line no-return-await
    return await FileVersion.query(trx)
      .select(['*'])
      .distinctOn('file_versions.parent_id')
      .withGraphJoined('file')
      .where(search)
      .orderByRaw(
        '"file_versions"."parent_id" , "file_versions"."created" desc',
      )
  }

  async addFile(file, category, status = null, transaction = null) {
    const { model: FileVersion } = require('../fileVersion')
    const matchOnlyNameCategories = ['source', 'image', 'supplement', 'support']

    const bookComponentQuery = FileVersion.query(transaction)
      .alias('bookComponentFile')
      .joinEager('file')
      .where({
        bookId: this.id,
        'bookComponentFile.category': category,
      })

    if (matchOnlyNameCategories.includes(category)) {
      bookComponentQuery.andWhere(builder => {
        builder.where('name', 'like', `${file.name.replace(/\.[^/.]+$/, '')}.%`)
      })
    } else if (category !== 'converted') {
      bookComponentQuery.andWhere(builder => {
        builder.where({ name: file.name })
      })
    }

    const bookComponentFileExists = await bookComponentQuery

    let parentId = uuid.v4()
    let versionName = '1'
    let maxElement = { tag: null }

    if (bookComponentFileExists.length > 0) {
      parentId = bookComponentFileExists[0].parentId

      maxElement = bookComponentFileExists.reduce(
        (prev, current) =>
          Number(prev.versionName) > Number(current.versionName)
            ? prev
            : current,
        1,
      )

      versionName = (parseInt(maxElement.versionName, 10) + 1).toString()
    }

    return await Book.relatedQuery('files', transaction)
      .for(this)
      .insert({
        bookComponentVersionId: null,
        category,
        fileId: file.id,
        status,
        parentId,
        ownerId: file.ownerId,
        versionName,
        tag: file.tag || maxElement.tag,
      })
      .returning('*')
  }

  async updateStatus(transaction = null) {
    const { model: BookComponent } = require('../bookComponent')
    let status = null

    const bookComponents = await BookComponent.query(transaction).where({
      bookId: this.id,
      parentId: null,
    })

    if (this.settings.chapterIndependently || this.workflow === 'word') {
      const noPartComponents = bookComponents.filter(
        x => x.componentType !== 'part',
      )

      if (
        noPartComponents.every(bc => bc.status === noPartComponents[0].status)
      ) {
        status = noPartComponents[0].status
      } else {
        status = 'in-production'
      }
    } else {
      const bookComponentFiles = await Book.latestComponentFiles(
        { id: this.id },
        null,
        transaction,
      )

      const sourceAndConverted = bookComponentFiles.filter(
        bcf => bcf.category === 'source' || bcf.category === 'converted',
      )

      if (sourceAndConverted.length > 0) {
        status = sourceAndConverted[0].status
      }
    }

    if (status) {
      await Book.query(transaction).patch({ status }).findOne({ id: this.id })
      const book = await Book.query(transaction).findOne({ id: this.id })

      logger.info(
        `Status of Book id: ${book.id}, bcms${book.alias}, ${book.domain} is updated to ${status}`,
      )

      if (book.collectionId) {
        const { collection } = await this.$query(transaction).joinEager(
          'collection',
        )

        await collection.updateCollectionStatus(transaction)
      }
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async filterUUIDsByLatestPyublishedVersion(uuidx) {
    const { model: BookComponent } = require('../bookComponent')

    const latestPublishedVersion = await BookComponent.query()
      .where({
        status: 'published',
      })
      .andWhere(builder => {
        builder.orWhere({
          id: uuidx,
          parentId: null,
        })

        builder.orWhere({
          parentId: uuidx,
        })
      })
      .orderByRaw('book_component.version_name::int desc')
      .limit(1)

    if (latestPublishedVersion && latestPublishedVersion.length === 1) {
      return latestPublishedVersion[0].id
    }

    return uuidx
  }

  // eslint-disable-next-line class-methods-use-this
  async loadBookComponents(bookComponentUUIDArray, options) {
    const { model: BookComponent } = require('../bookComponent')
    const partsOn = options.group_chapters_in_parts === true

    bookComponentUUIDArray = await Promise.all(
      bookComponentUUIDArray.map(async uuidx => {
        return this.filterUUIDsByLatestPyublishedVersion(uuidx)
      }),
    )

    const bcs = await BookComponent.query()
      .skipUndefined()
      .whereIn('id', bookComponentUUIDArray)
      .andWhere(builder => {
        if (((options || {}).statusfilter || []).length > 0) {
          builder.orWhereIn('status', (options || {}).statusfilter)
        }

        if (partsOn) {
          builder.orWhereIn('component_type', ['part'])
        }
      })
      .orderByRaw(
        `array_position(array['${bookComponentUUIDArray.join(
          "'::uuid,'",
        )}'::uuid], id)`,
      )

    return await Promise.all(
      (bcs || []).map(async bookComponent => {
        bookComponent.convertedFile = await BookComponent.getFiles()
          .where({ 'file_versions.category': 'converted' })
          .for(bookComponent.id)
          .orderByRaw('file_versions.version_name::int desc')

        return bookComponent
      }),
    )
  }

  async getBookComponentsNested(bcIds, options) {
    let secondLevel = []

    if (bcIds.length > 0) {
      secondLevel = await this.loadBookComponents(bcIds, options)

      if (secondLevel.length > 0) {
        secondLevel = await Promise.all(
          secondLevel.map(async bc2nd => {
            bc2nd.secondLevel = await this.getBookComponentsNested(
              bc2nd.bookComponents,
              options,
            )

            return bc2nd
          }),
        )
      }
    }

    return secondLevel
  }

  async getBookComponentsByDivision(options) {
    const { divisions } = await this.getDivisions()

    const partsOn = options.group_chapters_in_parts === true

    return Promise.all(
      divisions.map(async division => {
        if (division.bookComponents.length > 0) {
          division.bookComponents = await this.loadBookComponents(
            division.bookComponents,
            options,
          )
        }

        // Second level of hierarchy

        if (partsOn && division.bookComponents) {
          division.bookComponents = await Promise.all(
            division.bookComponents.map(async bc => {
              bc.secondLevel = await this.getBookComponentsNested(
                bc.bookComponents,
                options,
              )

              return bc
            }),
          )
        }

        return division
      }),
    )
  }

  static async create({ transaction, ...data }) {
    const { model: Division } = require('../division')
    const { model: Toc } = require('../toc')
    const Channel = require('../channel/channel')
    // Create Book
    const book = new Book(data)
    const savedBook = await book.save(transaction)

    // Create Division
    const createdDivisions = await Promise.all(
      Division.DivisionConfig.map(
        async label =>
          // eslint-disable-next-line no-return-await
          await new Division({
            bookComponents: [],
            bookId: book.id,
            label,
          }).save(transaction),
      ),
    )

    // Update Divisions to the book
    savedBook.divisions = createdDivisions.map(d => d.id)

    await savedBook.save(transaction)

    const bookTeams = Book.Teams()

    savedBook.teams = await Promise.all(
      Object.keys(bookTeams).map(
        async team =>
          // eslint-disable-next-line no-return-await
          await Team.query(transaction).upsertGraphAndFetch({
            name: bookTeams[team].name,
            objectId: savedBook.id,
            objectType: savedBook.type,
            role: team,
          }),
      ),
    )

    // create toc
    if (data.settings.chapterIndependently) {
      await Toc.query(transaction).insertAndFetch({
        bookId: book.id,
        collectionId: null,
        status: 'unpublished',
        title: 'Table of contents',
        ownerId: this.userId,
      })
    }

    // create channels if wholebook
    if (data.settings.chapterIndependently === false) {
      const members = this.userId ? [{ user: { id: this.userId } }] : []

      const feedbackPanel = await Channel.query(
        transaction,
      ).upsertGraphAndFetch(
        {
          objectId: savedBook.id,
          members,
          topic: 'feedback-channel',
        },
        { relate: true },
      )

      const errorChannel = await Channel.query(transaction).upsertGraphAndFetch(
        {
          objectId: savedBook.id,
          members,
          topic: 'error-channel',
        },
        { relate: true },
      )

      await Promise.all([feedbackPanel, errorChannel])
    }

    return savedBook
  }

  async getBookWithAppliedCollectionValues() {
    if (!this.collectionId) return this

    const { model: Collection } = require('../collection')

    try {
      const collection = await Collection.query()
        .findById(this.collectionId)
        .throwIfNotFound()

      const modifiedBook = cloneDeep(this)
      const isChapterProcessed = modifiedBook.settings.chapterIndependently
      const isWholeBook = !isChapterProcessed
      const isPdf = modifiedBook.workflow === 'pdf'

      // TO DO -- APPLY COVER

      const {
        applyCoverToBooks,
        applyPublisherToBooks,
        applyPermissionsToBooks,

        notes,

        // PUBLISHER SECTION
        /* eslint-disable camelcase */
        pub_loc,
        pub_name,
        /* eslint-enable camelcase */

        // PERMISSIONS SECTION
        copyrightStatement,
        licenseStatement,
        licenseType,
        licenseUrl,
        openAccessLicense,

        // SOURCE TYPE SECTION
        chapterProcessedSourceType,
        wholeBookSourceType,
      } = collection.metadata

      if (applyCoverToBooks) {
        modifiedBook.fileCover = collection.fileCover
      }

      if (applyPublisherToBooks && isChapterProcessed) {
        /* eslint-disable camelcase */
        modifiedBook.metadata.pub_loc = pub_loc
        modifiedBook.metadata.pub_name = pub_name
        /* eslint-enable camelcase */
      }

      if (applyPermissionsToBooks && isPdf) {
        modifiedBook.metadata.copyrightStatement = copyrightStatement
        modifiedBook.metadata.licenseStatement = licenseStatement
        modifiedBook.metadata.licenseType = licenseType
        modifiedBook.metadata.licenseUrl = licenseUrl
        modifiedBook.metadata.openAccessLicense = openAccessLicense
      }

      if (isPdf) {
        const appliedNotes = notes.filter(n => n.applyNotePdf) || []

        modifiedBook.metadata.notes = [
          ...appliedNotes,
          ...modifiedBook.metadata.notes,
        ]
      }

      if (isChapterProcessed) {
        modifiedBook.metadata.sourceType = chapterProcessedSourceType
      }

      if (isWholeBook) {
        modifiedBook.metadata.sourceType = wholeBookSourceType
      }

      return modifiedBook
    } catch (e) {
      throw new Error(e)
    }
  }

  static Teams() {
    return {
      author: {
        name: 'Author',
      },
      // copyeditor: {
      //   name: 'Copy Editor',
      // },
      editor: {
        name: 'Editor',
      },
      // peerReviewer: {
      //   name: 'Peer Reviewer',
      // },
      previewer: {
        name: 'Previewer',
      },
    }
  }
}

module.exports = Book
