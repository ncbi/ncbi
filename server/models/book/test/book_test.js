const path = require('path')

const pathToComponent = path.resolve(__dirname, '..', 'book')
const pathToOrganisation = path.resolve(__dirname, '..', '..', 'organisation')

process.env.NODE_CONFIG = `{"pubsweet":{
  "components":[
    "@pubsweet/model-user",
    "@pubsweet/model-team",
    "${pathToOrganisation}",
    "${pathToComponent}"
  ]
}}`

const Book = require('../book')

const { model: Organisation } = require('../../organisation')

const dbCleaner = require('../../helpers/db_cleaner')

let organisationId

describe('Book', () => {
  beforeEach(async () => {
    await dbCleaner()

    const organisation = await new Organisation({ name: 'Test' }).save()
    organisationId = organisation.id
  })

  it('has updated set when created', async () => {
    const book = await new Book({ organisationId, title: 'Test' }).save()
    expect(book.title).toEqual('Test')
    const now = new Date().toISOString()
    expect(book.updated.toISOString()).toHaveLength(now.length)
  })

  it('can be saved and found and deleted', async () => {
    const book = await new Book({ organisationId, title: 'Test' }).save()
    expect(book.title).toEqual('Test')

    const foundBook = await Book.find(book.id)
    expect(foundBook.title).toEqual('Test')

    await foundBook.delete()

    async function tryToFind() {
      await Book.find(foundBook.id)
    }

    await expect(tryToFind()).rejects.toThrow('Object not found')
  })

  it('can be found by property', async () => {
    await new Book({ organisationId, title: 'Test' }).save()
    const book = await Book.findOneByField('title', 'Test')
    expect(book.title).toEqual('Test')

    let books = await Book.findByField('title', 'Test')
    expect(books[0].title).toEqual('Test')

    async function findMissing() {
      await Book.findOneByField('title', 'Does not exist')
    }

    await expect(findMissing()).rejects.toThrow('Object not found')

    books = await Book.findByField('title', 'Does not exist')
    expect(books).toEqual([])
  })

  it('can not be saved with non-valid properties', async () => {
    async function createNonValidBook() {
      await new Book({ mumbo: 'jumbo' }).save()
    }

    await expect(createNonValidBook()).rejects.toThrow(
      'mumbo: is an invalid additional property',
    )
  })

  it('can save new entity with known ID', async () => {
    const id = '1838d074-fb9d-4ed6-9c63-39e6bc7429ce'

    const book = await new Book({
      id,
      organisationId,
      title: 'Test',
    }).save()

    expect(book.id).toEqual(id)
  })

  it('can get organisation of current book', async () => {
    const book = await new Book({
      organisationId,
      title: 'Test',
    }).save()

    const bookOrganisation = await book.getOrganisation()

    expect(bookOrganisation.organisation.id).toEqual(organisationId)
  })
})
