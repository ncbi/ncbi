/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')
const { omit } = require('lodash')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      return Promise.all(
        books.map(book => {
          const { settings } = book

          /**
           * Patch the settings
           */

          return book.$query(trx).patch({
            fundedContentType: settings.fundedContentType,
            settings: omit(settings, ['fundedContentType']),
          })
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
