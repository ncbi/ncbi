/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)

const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)
const _ = require('lodash')

const affiliationToArray = contributor => {
  if (
    typeof contributor.affiliation === 'string' ||
    contributor.affiliation === null
  ) {
    return {
      ...contributor,
      affiliation: contributor.affiliation ? [contributor.affiliation] : [],
    }
  }

  return contributor
}

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      await Promise.all(
        books.map(book => {
          const { metadata } = book
          const authors = metadata.author.map(affiliationToArray)
          const editors = metadata.editor.map(affiliationToArray)

          if (
            _.isEqual(metadata.author, authors) &&
            _.isEqual(metadata.editor, editors)
          ) {
            return book
          }

          return book.$query(trx).patch({
            metadata: { ...metadata, author: authors, editor: editors },
          })
        }),
      )

      const bookComponents = await BookComponent.query(trx)
      return Promise.all(
        bookComponents.map(bookComponent => {
          const { metadata } = bookComponent
          const authors = metadata.author.map(affiliationToArray)
          const editors = metadata.editor.map(affiliationToArray)

          if (
            _.isEqual(metadata.author, authors) &&
            _.isEqual(metadata.editor, editors)
          ) {
            return bookComponent
          }

          return bookComponent.$query(trx).patch({
            metadata: { ...metadata, author: authors, editor: editors },
          })
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
