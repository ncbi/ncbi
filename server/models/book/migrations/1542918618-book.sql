CREATE SEQUENCE IF NOT EXISTS universal_bcms_id;

create table books (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,

  -- foreign
  organisation_id uuid not null references organisations,
  file_cover uuid references files,
  file_abstract uuid references files,
  owner_id uuid references users,
  divisions jsonb,
  collections jsonb,
  
  -- own
  status text default 'new-book',
  agreement Boolean NOT NULL default false,
  domain_id text,
  domain text,
  book_submit_id text,
  abstract text,
  abstract_title text,
  title text,
  workflow text,
  edition text,
  sub_title text,
  alt_title text,
  settings jsonb,
  metadata jsonb,
  alias integer NOT NULL DEFAULT nextval('universal_bcms_id')
);
