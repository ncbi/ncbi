/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')
const { has, get } = require('lodash')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      const fixSourceTypeCase = sourceType => {
        return sourceType.charAt(0).toUpperCase() + sourceType.slice(1)
      }

      return Promise.all(
        books.map(book => {
          const { metadata } = book
          let sourceType = null

          if (has(metadata, 'sourceType')) {
            sourceType = get(metadata, 'sourceType')
            sourceType = fixSourceTypeCase(sourceType)
          }

          /**
           * Patch the settings
           */
          if (sourceType) {
            return book.$query(trx).patch({
              metadata: {
                ...metadata,
                sourceType,
              },
            })
          }

          return true
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
