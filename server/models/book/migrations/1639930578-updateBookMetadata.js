/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')
const { has, isBoolean, mergeWith } = require('lodash')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  try {
    const modelMetadata = Book.schema.properties.metadata
    const defaultMetadata = modelMetadata.default

    const fieldsToDelete = [
      'catalogId',
      'licenceType',
      'licenceStatement',
      'otherLicenceType',
    ]

    return useTransaction(async trx => {
      const books = await Book.query(trx)

      return Promise.all(
        books.map(book => {
          const { metadata } = book
          const modifiedMetadata = { ...metadata }

          fieldsToDelete.forEach(field => {
            if (has(metadata, field)) {
              delete modifiedMetadata[field]
            }
          })

          if (has(metadata, 'ccLicenseType')) {
            modifiedMetadata.licenseType = metadata.ccLicenseType
            delete modifiedMetadata.ccLicenseType
          }

          const newMetadata = mergeWith(
            {},
            defaultMetadata,
            modifiedMetadata,
            /* eslint-disable-next-line consistent-return */
            (originalValue, newValue, key) => {
              // don't let non-boolean values (eg. null) override boolean properties
              if (isBoolean(defaultMetadata[key] && !isBoolean(newValue))) {
                return originalValue
              }

              return newValue
            },
          )

          const patchData = {
            metadata: newMetadata,
          }

          return book.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
