/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      return Promise.all(
        books.map(async book => {
          const {
            settings: { chapterIndependently },
            metadata,
          } = book

          let url = null

          if (!chapterIndependently) {
            const bookComponent = await BookComponent.query(trx).findOne({
              bookId: book.id,
            })

            if (bookComponent) {
              url = bookComponent.metadata.url
            }
          }
          /**
           * Patch the settings
           */

          if (url) {
            return book.$query(trx).patch({
              metadata: {
                ...metadata,
                url,
              },
            })
          }

          return false
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
