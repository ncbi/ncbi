/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  try {
    const modelMetadata = Book.schema.properties.metadata
    const defaultMetadata = modelMetadata.default

    const fieldsToClean = ['author', 'editor', 'collaborativeAuthors']

    return useTransaction(async trx => {
      const books = await Book.query(trx)

      return Promise.all(
        books.map(book => {
          const { metadata: existingMetadata } = book
          const modifiedMetadata = { ...existingMetadata }

          fieldsToClean.forEach(field => {
            if (!Array.isArray(existingMetadata[field])) {
              modifiedMetadata[field] = []
            }
          })

          const newMetadata = {
            ...defaultMetadata,
            ...modifiedMetadata,
          }

          const patchData = {
            metadata: newMetadata,
          }

          return book.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
