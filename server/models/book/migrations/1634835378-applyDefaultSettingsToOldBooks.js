/* eslint-disable import/no-dynamic-require */
const config = require('config')

const { logger, useTransaction } = require('@coko/server')
const { cloneDeep, merge, mergeWith, isNil } = require('lodash')

// Paths are relative to the generated migrations folder

const Book = require(`${process.cwd()}/server/models/book/book`)
const BookSettingsTemplate = require(`${process.cwd()}/server/models/bookSettingsTemplate/bookSettingsTemplate`)

const {
  domainRequest,
} = require(`${process.cwd()}/server/services/axiosService`)

const DomainTranslation = require(`${process.cwd()}/server/services/domainTranslation/domainTranslation`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      return Promise.all(
        books.map(async book => {
          const {
            metadata: existingMetadata,
            organisationId: organizationId,
            settings: existingSettings,
            workflow,
          } = book

          // find the submission type
          const submissionType = existingSettings.chapterIndependently
            ? 'chapterProcessed'
            : 'wholeBook'

          // is there a custom template for this submission type?
          const customTemplate = await BookSettingsTemplate.query(trx).findOne({
            organizationId,
            templateType: submissionType,
          })

          let templateData

          // if there is, get the data for the custom template
          if (customTemplate) {
            const { data: detailsData } = await domainRequest(
              `domains/${customTemplate.domainId}`,
              'GET',
            )

            if (detailsData.meta.status !== 'ok') {
              logger.error(detailsData.body)
              throw new Error('Get domain details request failed')
            }

            templateData = detailsData.body
          } else {
            // if not, grab the default template for this type
            if (!config.has(`bookSettings.domainTemplates.${submissionType}`)) {
              throw new Error(
                `Default template for ${submissionType} missing from config!`,
              )
            }

            templateData = config.get(
              `bookSettings.domainTemplates.${submissionType}`,
            )
          }

          // apply extra rules
          let extraPatchData = {
            settings: {},
          }

          if (submissionType === 'chapterProcessed' && workflow === 'word') {
            extraPatchData = {
              settings: {
                createPdf: true,
              },
            }
          }

          const templateValues = DomainTranslation.domainToBookSettings(
            templateData,
          )

          // get non-domain settings default values
          const ownDefaultValues = config.get('bookSettings.BCMSBookDefaults')

          // merge non-domain default values with non-domain values from template
          let ownValues

          if (customTemplate)
            ownValues = merge({}, ownDefaultValues, customTemplate.values)
          else ownValues = cloneDeep(ownDefaultValues)

          if (workflow === 'word') {
            ownValues.settings.approvalOrgAdminEditor = true
          }

          // merge it all into one settings object
          const patchData = {
            settings: mergeWith(
              {},
              templateValues.settings,
              ownValues.settings,
              existingSettings,
              extraPatchData.settings,
              /* eslint-disable-next-line consistent-return */
              (objValue, srcValue) => {
                // don't let null values override non-null values
                if (isNil(objValue) && !isNil(srcValue)) return srcValue
                if (isNil(srcValue) && !isNil(objValue)) return objValue
              },
            ),
          }

          const openAccess = isNil(existingMetadata.openAccess)
            ? templateValues.metadata.openAccess
            : existingMetadata.openAccess

          patchData.metadata = { openAccess }

          /**
           * Patch the settings
           */

          return book.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
