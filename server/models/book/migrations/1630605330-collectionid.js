exports.up = async knex =>
  Promise.all([
    knex.schema.table('books', table => {
      table.dropColumn('collections')
      table.uuid('collection_id').references('id').inTable('collections')
    }),
  ])
