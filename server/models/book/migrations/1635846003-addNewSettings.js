/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')
const { has } = require('lodash')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      return Promise.all(
        books.map(book => {
          const { settings } = book

          if (!settings) return null
          const patchData = { ...settings }

          if (!has(settings, 'citationSelfUrl')) {
            patchData.citationSelfUrl = ''
          }

          if (!has(settings, 'specialPublisherLinkText')) {
            patchData.specialPublisherLinkText = ''
          }

          if (!has(settings, 'specialPublisherLinkUrl')) {
            patchData.specialPublisherLinkUrl = ''
          }

          /**
           * Patch the settings
           */

          return book.$query(trx).patch({
            settings: patchData,
          })
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
