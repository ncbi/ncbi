/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')
const { has, set } = require('lodash')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      return Promise.all(
        books.map(book => {
          const { metadata } = book
          const modifiedMetadata = { ...metadata }

          if (!has(modifiedMetadata, 'pubDate.dateRange.startMonth')) {
            set(modifiedMetadata, 'pubDate.dateRange.startMonth', null)
          }

          if (!has(modifiedMetadata, 'pubDate.dateRange.endMonth')) {
            set(modifiedMetadata, 'pubDate.dateRange.endMonth', null)
          }

          const patchData = {
            metadata: modifiedMetadata,
          }

          return book.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
