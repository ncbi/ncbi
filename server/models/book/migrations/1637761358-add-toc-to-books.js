/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-await-in-loop */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)
const Division = require(`${process.cwd()}/server/models/division/division`)
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      // eslint-disable-next-line no-plusplus
      for (let i = 0, len = books.length; i < len; i++) {
        if (books[i].settings.chapterIndependently) {
          const division = await Division.query(trx).findOne({
            bookId: books[i].id,
            label: 'frontMatter',
          })

          const TocFound = await BookComponent.query(trx).findOne({
            bookId: books[i].id,
            componentType: 'toc',
          })

          if (!TocFound) {
            await BookComponent.query(trx).insertAndFetch({
              bookId: books[i].id,
              componentType: 'toc',
              status: 'unpublished',
              title: 'Table of contents',
              divisionId: division.id,
              ownerId: this.userId,
            })
          }
        }
      }

      return true
    })
  } catch (error) {
    throw new Error(error)
  }
}
