exports.up = async knex =>
  knex.schema.hasColumn('books', 'parent_id').then(exists => {
    if (!exists) {
      Promise.all([
        knex.schema.table('books', table => {
          table.uuid('parentId').nullable()
        }),
      ])
    }
  })
