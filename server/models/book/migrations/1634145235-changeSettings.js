/* eslint-disable import/no-dynamic-require */
const config = require('config')

const { useTransaction } = require('@coko/server')
const { has, isNumber } = require('lodash')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      return Promise.all(
        books.map(book => {
          const { organisationId, settings } = book

          if (!settings) return null
          const patchData = { ...settings }
          const organizationId = organisationId

          if (!organisationId) {
            throw new Error(`Organization id not found for book ${book.id}`)
          }

          /**
           * citationType:
           * We used to store the value id as an int.
           * Now we store the value itself as a string.
           */

          if (has(settings, 'citationType')) {
            const citationTypeValueId = settings.citationType

            if (isNumber(citationTypeValueId)) {
              const citationTypeIds = config.get('bookSettings.citationTypeIds')

              const citationTypeValue = Object.keys(citationTypeIds).find(
                key => citationTypeIds[key] === citationTypeValueId,
              )

              patchData.citationType = citationTypeValue
            }
          }

          /**
           * createPdf:
           * We used to have a value called 'create-pdf'.
           * This is now converted to camelCase.
           * We also used to have a value called `eachChapter` that captured
           * the exact same concept, so these two are now merged.
           */

          if (has(settings, 'create-pdf')) {
            patchData.createPdf = settings['create-pdf']
            delete patchData['create-pdf']
          }

          if (has(settings, 'eachChapter')) {
            // in case both values were set, let eachChapter override create-pdf
            patchData.createPdf = settings.eachChapter
            delete patchData.eachChapter
          }

          /**
           * createWholebookPdf:
           * We used to have a value called 'create-wholebook-pdf'.
           * This is now converted to camelCase.
           * We also used to have a value called `wholebook` that captured
           * the exact same concept, so these two are now merged.
           */

          if (has(settings, 'create-wholebook-pdf')) {
            patchData.createWholebookPdf = settings['create-wholebook-pdf']
            delete patchData['create-wholebook-pdf']
          }

          if (has(settings, 'wholeBook')) {
            // in case both values were set, let wholebook override create-wholebook-pdf
            patchData.createWholebookPdf = settings.wholeBook
            delete patchData.wholeBook
          }

          /**
           * alternate-versions-pdf & alternate-versions-pdf-book:
           * Convert to camelcase.
           */

          if (has(settings, 'alternate-versions-pdf')) {
            patchData.alternateVersionsPdf = settings['alternate-versions-pdf']

            delete patchData['alternate-versions-pdf']
          }

          if (has(settings, 'alternate-versions-pdf-book')) {
            patchData.alternateVersionsPdfBook =
              settings['alternate-versions-pdf-book']

            delete patchData['alternate-versions-pdf-book']
          }

          /**
           * multiplePublishedVersions:
           * We used to have a field called "issue-versions", but also had
           * a "mutliplePublishedVersions" field (only set in the defaults in the model).
           * Merge "issue-versions" into "multiplePublishedVersions".
           */

          if (has(settings, 'issue-versions')) {
            patchData.multiplePublishedVersions = settings['issue-versions']
            delete patchData['issue-versions']
          }

          /**
           * group_chapters_in_parts & add_body_to_parts:
           * There were duplicate settings for these two, both at the settings
           * top level, and within the "toc" object.
           * Only keep the settings under "toc".
           * If both are set in incoming data, the top level ones will override
           * the ones under "toc".
           */

          if (has(settings, 'group_chapters_in_parts')) {
            patchData.toc.group_chapters_in_parts =
              settings.group_chapters_in_parts

            delete patchData.group_chapters_in_parts
          }

          if (has(settings, 'add_body_to_parts')) {
            patchData.toc.add_body_to_parts = settings.add_body_to_parts
            delete patchData.add_body_to_parts
          }

          /**
           * publishPdfDownload & ignoreBookOrderFile:
           * These settings are not used anymore.
           */

          if (has(settings, 'publishPdfDownload')) {
            delete patchData.publishPdfDownload
          }

          if (has(settings, 'ignoreBookOrderFile')) {
            delete patchData.ignoreBookOrderFile
          }

          /**
           * There must have been some iterations where an invalid setting
           * "requireApprovalPreview" was being written in the book settings.
           * Remove it completely. No need to reassign to a new value, as the
           * value exists in the "approvalPreviewer" setting.
           */

          if (has(settings, 'requireApprovalPreview')) {
            delete patchData.requireApprovalPreview
          }

          /**
           * This invalid setting existed on the staging deployment
           */

          if (has(settings, 'submitFilesByUpload')) {
            delete patchData.submitFilesByUpload
          }

          /**
           * Existing books don't have a publisher field in their settings.
           * This field is an organization id, which when asked for by the UI
           * will run a `findById`, so it needs to exist.
           *
           * Make the publisher for all existing books the organization that
           * the book belongs to.
           */

          patchData.publisher = organizationId

          /**
           * Patch the settings
           */

          return book.$query(trx).patch({
            settings: patchData,
          })
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
