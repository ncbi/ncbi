/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      return Promise.all(
        books.map(book => {
          const { settings, version } = book

          if (settings.chapterIndependently === false && version === null) {
            return book.$query(trx).patch({ version: '1' })
          }

          return true
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
