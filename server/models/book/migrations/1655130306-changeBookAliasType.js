/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  await changeAliasColumnType(knex)
  await updateBookAliasValue(knex)
}

const changeAliasColumnType = knex =>
  knex.schema.table('books', table => {
    table.string('alias').alter()
  })

const updateBookAliasValue = async knex => {
  const getNewAliasValue = book =>
    book.version ? `${book.alias}.${book.version}` : `${book.alias}.1`

  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      return Promise.all(
        books.map(book => {
          const alias = getNewAliasValue(book)
          return book.$query(trx).patch({ alias })
        }),
      )
    })
  } catch (err) {
    throw new Error(err)
  }
}
