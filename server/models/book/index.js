const model = require('./book')
const BookComponent = require('../bookComponent/bookComponent')

module.exports = {
  model,
  modelName: 'Book',
  modelLoaders: {
    getParts: async bookId => {
      const parts = await BookComponent.query()
        .whereIn('bookId', bookId)
        .andWhere({
          componentType: 'part',
        })

      return bookId.map(id => parts.filter(result => result.bookId === id))
    },
  },
}
