create table files (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,
  -- own

  file_size int,
  mime_type text,
  name text not null,
  category text,
  source bytea
);