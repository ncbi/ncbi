/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-return-await */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const File = require(`${process.cwd()}/server/models/file/file`)
const FileService = require(`${process.cwd()}/server/services/file/fileService`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      let files = await File.query(trx)
        .select(['id'])
        .whereNotNull('source')
        .andWhere(builder => {
          builder.whereNull('oid')
        })

      files = files.slice(0, 1000)

      await Promise.all(
        files.map(async file => {
          const { source: content, name } = await File.query(trx).findOne({
            id: file.id,
          })

          return await FileService.update(file.id, { content, filename: name })
        }),
      )

      return true
    })
  } catch (error) {
    throw new Error(error)
  }
}
