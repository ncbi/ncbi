/* eslint-disable global-require */
/* eslint-disable no-return-await */
const model = require('./file')
const { mapResultsToInputIds } = require('../util')

module.exports = {
  model,
  modelName: 'File',
  server: () => app => require('./getImage')(app),
  modelLoaders: {
    sourceForBookComponents: async bookComponentIds => {
      const { BookComponent } = require('@pubsweet/models')
      return await mapResultsToInputIds(
        bookComponentIds,
        'bookComponentId',
        BookComponent.getFiles()
          .where({ 'file_versions.category': 'source' })
          .for(bookComponentIds)
          .orderByRaw('file_versions.version_name::int desc'),
      )
    },
    convertedForBookComponents: async bookComponentIds => {
      const { BookComponent } = require('@pubsweet/models')
      return await mapResultsToInputIds(
        bookComponentIds,
        'bookComponentId',
        BookComponent.getFiles()
          .where({ 'file_versions.category': 'converted' })
          .for(bookComponentIds)
          .orderByRaw('file_versions.version_name::int desc'),
      )
    },
  },
}
