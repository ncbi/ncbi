const { Model } = require('objection')
const BaseModel = require('@pubsweet/base-model')

class File extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'file'
  }

  static get tableName() {
    return 'files'
  }

  static get relationMappings() {
    // eslint-disable-next-line global-require
    const { model: Book } = require('../book')

    return {
      book: {
        join: {
          from: 'File.bookId',
          to: 'Book.id',
        },
        modelClass: Book,
        relation: Model.BelongsToOneRelation,
      },
    }
  }

  static get schema() {
    return {
      properties: {
        category: {
          type: ['string', 'null'],
        },
        mimeType: {
          default: 'application/octet-stream',
          type: 'string',
        },
        name: {
          minLength: 1,
          type: 'string',
        },
        oid: {
          type: ['integer', 'null'],
        },
      },
      required: ['name'],
      type: 'object',
    }
  }
}

module.exports = File
