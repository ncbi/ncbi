const { useTransaction } = require('@coko/server')
const File = require('./file')
const FileService = require('../../services/file/fileService')

module.exports = app => {
  // const authBearer = app.locals.passport.authenticate('bearer', {
  //   session: false,
  // })

  app.get('/getImage', async (req, res, next) => {
    const { id, category } = req.query

    const file = await File.query().skipUndefined().findOne({ category, id })

    if (file) {
      res.setHeader('Content-type', file.mimeType)

      await useTransaction(async transaction => {
        return new Promise(resolve => {
          FileService.read({
            stream: res,
            oid: file.oid,
            transaction,
            end: resolve,
          })
        })
      })
    }

    res.end()
  })
}
