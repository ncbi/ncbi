/* eslint-disable global-require */
const model = require('./fileVersion')

module.exports = {
  model,
  modelName: 'FileVersion',
}
