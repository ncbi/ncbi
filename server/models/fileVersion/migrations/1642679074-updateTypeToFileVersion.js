/* eslint-disable import/no-dynamic-require */
// Paths are relative to the generated migrations folder
const FileVersion = require(`${process.cwd()}/server/models/fileVersion/fileVersion`)

exports.up = async knex => {
  try {
    await FileVersion.query().patch({ type: 'fileVersion' })
  } catch (error) {
    throw new Error(error)
  }
}
