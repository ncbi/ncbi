/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder

const Book = require(`${process.cwd()}/server/models/book/book`)
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)
const FileVersion = require(`${process.cwd()}/server/models/fileVersion/fileVersion`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const books = await Book.query(trx)

      return Promise.all(
        books.map(async book => {
          // find the submission type
          if (!book.settings.chapterIndependently) {
            const bookComponents = await BookComponent.query(trx)
              .whereNotIn('componentType', ['toc', 'part'])
              .andWhere('bookId', book.id)

            if (bookComponents.length === 1) {
              await FileVersion.query(trx)
                .patch({ bookId: book.id, bookComponentId: null })
                .where({ bookComponentId: bookComponents[0].id })
            }
          }
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
