create table book_component_files (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,
  -- own

    -- foreign
  book_component_id uuid references book_component,
  book_component_version_id uuid,
  file_id uuid references files,
  parent_id uuid,

  -- own
  category text,
  tag text,
  version_name text,
  status text,
  copied boolean default false
);