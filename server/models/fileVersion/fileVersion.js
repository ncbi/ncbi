/* eslint-disable global-require */
const { Model } = require('objection')
const BaseModel = require('@pubsweet/base-model')

const StatusService = require('../../services/statusService/statusService')

class FileVersion extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'fileVersion'
  }

  static get tableName() {
    return 'file_versions'
  }

  static async beforeInsert({ transaction, inputItems }) {
    super.beforeInsert()
    const [item] = inputItems

    if (item && item.status && item.toc_id === null) {
      item.status = await StatusService.updateEntity(item, item, transaction)
    }
  }

  static async beforeUpdate({ transaction, inputItems, asFindQuery, context }) {
    super.beforeUpdate()

    const [item] = inputItems

    if (
      (item.status && context.ignoreUpdate === undefined) ||
      context.ignoreUpdate === false
    ) {
      const [entity] = await asFindQuery()

      if (item.status !== entity.status && entity.toc_id === null) {
        item.status = await StatusService.updateEntity(
          entity,
          item,
          transaction,
        )
      }
    }
  }

  static get relationMappings() {
    // eslint-disable-next-line global-require
    const { model: File } = require('../file')

    return {
      file: {
        join: {
          from: 'file_versions.fileId',
          to: 'files.id',
        },
        modelClass: File,
        relation: Model.HasOneRelation,
      },
    }
  }

  static get schema() {
    return {
      properties: {
        bookComponentId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
        bookId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
        bookComponentVersionId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
        tocId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
        category: {
          minLength: 1,
          type: 'string',
        },
        fileId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
        parentId: {
          type: ['string', 'null'],
        },
        versionName: {
          default: '1',
          type: ['string', 'null'],
        },
        status: {
          enum: [
            'new-book',
            'submission-errors',
            'new-upload',
            'converting',
            'tagging',
            'failed',
            'conversion-errors',
            'loading-preview',
            'loading-errors',
            'preview',
            'approve',
            'published',
            'publishing',
            'publish-failed',
            'error', // TODO deprecated after changing the errors
            'tagging-errors',
          ],
          type: ['string', 'new-upload'],
        },
        tag: {
          type: ['string', 'null'],
        },
        copied: {
          default: false,
          type: ['boolean', false],
        },
        ownerId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
      },
      required: ['fileId'],
      type: 'object',
    }
  }

  getFile() {
    return this.$query()
      .joinEager('file')
      .select([
        'files.id',
        'files.created',
        'files.updated',
        'files.mimeType',
        'files.name',
        'files.category',
        'file_versions.copied',
        'file_versions.version_name',
        'file_versions.status',
        'file_versions.id as bcfId',
        'file_versions.parentId',
      ])
  }
}

module.exports = FileVersion
