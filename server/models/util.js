const { has } = require('lodash')

/**
 * Helper function for defining loaders.
 * @param {ID[]} loadIds
 *   The ids/lookup keys of the objects to be loaded.
 * @param {string} resultIdField
 *   The name of the id field which binds results to "loadIds".
 * @param {Query} query
 *   The query object which will fetch uncached results from the database.
 * @returns {Object[]}
 *   The list of query results in the same order as "loadIds".
 *   Unmatched "loadIds" will be returned as undefined.
 */
const mapResultsToInputIds = async (loadIds, resultIdField, query) => {
  const results = await query

  // Map results to their input ids
  const resultMap = {}

  results.forEach(result => {
    const resultId = result[resultIdField]

    if (!has(resultMap, resultId)) {
      resultMap[resultId] = []
    }

    resultMap[resultId].push(result)
  })

  // We map over ids so that the DataLoader API is always matched,
  // i.e. array of keys in, array of results out (even if some records are not found)
  return loadIds.map(inputId => resultMap[inputId])
}

module.exports = { mapResultsToInputIds }
