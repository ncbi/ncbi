/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const NcbiNotificationMessage = require(`${process.cwd()}/server/models/ncbiNotificationMessage/ncbiNotificationMessage`)

const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const ncbiNotificationMessage = await NcbiNotificationMessage.query(trx)

      // eslint-disable-next-line no-plusplus
      for (let i = 0, len = ncbiNotificationMessage.length; i < len; i++) {
        // eslint-disable-next-line no-await-in-loop
        const bookComponent = await BookComponent.query(trx).findOne({
          id: ncbiNotificationMessage[i].bookComponentId,
        })

        if (bookComponent) {
          // eslint-disable-next-line no-await-in-loop
          await NcbiNotificationMessage.query(trx)
            .patch({ bcmsId: bookComponent.alias })
            .findOne({ id: ncbiNotificationMessage[i].id })
        }
      }
    })
  } catch (error) {
    throw new Error(error)
  }
}
