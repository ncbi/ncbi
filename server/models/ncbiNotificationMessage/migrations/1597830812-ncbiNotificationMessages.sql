create table ncbi_notification_messages (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,

  -- own
  data jsonb,
  topic text,
  book_component_id uuid,
  job_id text,
  proccessed boolean default false,
  uploaded boolean
);
