/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-return-await */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const NcbiNotificationMessage = require(`${process.cwd()}/server/models/ncbiNotificationMessage/ncbiNotificationMessage`)
const Book = require(`${process.cwd()}/server/models/book/book`)
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)
const Toc = require(`${process.cwd()}/server/models/toc/toc`)

exports.up = async knex => {
  await addObjectIdColumn(knex)
  await populateObjectIdsFromBcmsId(knex)
  await dropBcmsIdColumn(knex)
}

const addObjectIdColumn = knex =>
  knex.schema.table('ncbi_notification_messages', table => {
    table.uuid('object_id').nullable()
  })

const populateObjectIdsFromBcmsId = async knex => {
  const findRelatedObject = async ncbiNotificationMessage => {
    const { bcmsId: alias } = ncbiNotificationMessage

    const book = await Book.query().findOne({ alias })

    if (book) {
      return book
    }

    const bookComponent = await BookComponent.query().findOne({ alias })

    if (bookComponent) {
      return bookComponent
    }

    const toc = await Toc.query().findOne({ alias })

    if (toc) {
      return toc
    }

    return null
  }

  try {
    return useTransaction(async trx => {
      const ncbiNotificationMessages = await NcbiNotificationMessage.query(
        trx,
      ).whereNotNull('bcms_id')

      return Promise.all(
        ncbiNotificationMessages.map(async ncbiNotificationMessage => {
          const related = await findRelatedObject(ncbiNotificationMessage) // Book, BookComponent, or Toc.

          if (related) {
            return ncbiNotificationMessage
              .$query(trx)
              .patch({ objectId: related.id })
          }

          return ncbiNotificationMessage
        }),
      )
    })
  } catch (err) {
    throw new Error(err)
  }
}

const dropBcmsIdColumn = knex =>
  knex.schema.table('ncbi_notification_messages', table => {
    table.dropColumn('bcms_id')
  })
