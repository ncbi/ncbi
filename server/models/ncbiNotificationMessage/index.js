/* eslint-disable global-require */
const model = require('./ncbiNotificationMessage')

module.exports = {
  model,
  modelName: 'NcbiNotificationMessage',
}
