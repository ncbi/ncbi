const BaseModel = require('@pubsweet/base-model')
const { idNullable } = require('../_helpers/types')

class NcbiNotificationMessage extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'ncbiNotificationMessage'
  }

  static get tableName() {
    return 'ncbi_notification_messages'
  }

  static get schema() {
    return {
      properties: {
        objectId: idNullable,
        data: {
          default: {
            converted_files: null,
            job_id: '',
            notices: [],
            package_id: '',
            status: 0,
            timestamp: '',
          },
          properties: {
            converted_files: {
              type: ['string', 'null'],
            },
            job_id: {
              type: ['string', 'null'],
            },
            notices: {
              default: [],
              items: {
                properties: {
                  assignee: {
                    type: ['string', 'null'],
                  },
                  filename: {
                    type: ['string', 'null'],
                  },
                  message: {
                    type: ['string', 'null'],
                  },
                  name: {
                    type: ['string', 'null'],
                  },
                  severity: {
                    type: ['string', 'null'],
                  },
                },
                type: ['object'],
              },
              type: 'array',
            },

            package_id: {
              type: ['string', 'null'],
            },
            status: {
              type: 'integer',
            },
            timestamp: {
              type: ['string', 'null'],
            },
          },
        },
        jobId: {
          minLength: 1,
          type: 'string',
        },
        packageType: {
          type: ['string', 'null'],
        },
        options: {
          minLength: 1,
          type: 'string',
        },
        proccessed: {
          default: false,
          type: ['boolean', false],
        },
        topic: {
          minLength: 1,
          type: 'string',
        },
        uploaded: {
          default: false,
          type: ['boolean', false],
        },
        timeout: {
          default: false,
          type: ['boolean', false],
        },
        retry: {
          default: 0,
          type: ['integer', false],
        },
      },
      type: 'object',
    }
  }

  static async expiredNotifications(objectId, trx) {
    let notProcessed = await NcbiNotificationMessage.query(trx).where({
      objectId,
      proccessed: false,
      uploaded: true,
    })

    const notifications = await NcbiNotificationMessage.activeNotifications(
      objectId,
      trx,
    )

    if (notifications.length > 0) {
      notProcessed = notProcessed.filter(
        n => !notifications.some(not => not.id === n.id),
      )
    }

    if (notProcessed.length) {
      await NcbiNotificationMessage.query(trx)
        .patch({ proccessed: true, timeout: true })
        .whereIn(
          'id',
          notProcessed.map(np => np.id),
        )
    }

    return notProcessed
  }

  static activeNotifications(objectId, trx) {
    return NcbiNotificationMessage.query(
      trx,
    ).whereRaw(
      "object_id = ? and updated > now() - interval '4 hour' and proccessed is False and uploaded is True",
      [objectId],
    )
  }
}

module.exports = NcbiNotificationMessage
