/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)

exports.up = async () => {
  try {
    return useTransaction(async trx => {
      const bookComponents = await BookComponent.query(trx)

      await Promise.all(
        bookComponents.map(bc => BookComponent.updateErrorSummary(bc.id, trx)),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
