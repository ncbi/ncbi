ALTER TABLE public.book_component ADD has_warnings boolean NULL DEFAULT false;
ALTER TABLE public.book_component ADD assignee_errors jsonb NULL;