/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')
const { get, has } = require('lodash')

// Paths are relative to the generated migrations folder
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)

exports.up = async knex => {
  try {
    const modelMetadata = BookComponent.schema.properties.metadata
    const defaultMetadata = modelMetadata.default

    const fieldsToDelete = [
      // duplicate values (also exist in top level)
      'abstract',
      'title',

      // unused values
      'dateType',
      'publicationFormat',
      'pubDate',

      'pub_loc',
      'pub_name',

      'edition',
      'volume',
    ]

    const contributorFields = ['author', 'editor', 'collaborativeAuthors']

    return useTransaction(async trx => {
      const bookComponents = await BookComponent.query(trx)

      return Promise.all(
        bookComponents.map(bookComponent => {
          const { metadata: existingMetadata } = bookComponent
          const modifiedMetadata = { ...existingMetadata }

          contributorFields.forEach(field => {
            if (!Array.isArray(existingMetadata[field])) {
              modifiedMetadata[field] = []
            }
          })

          // deconstruct existing abstract objects into separate fields
          // abstract.text does not need assignment, since it already exists
          // at the top level
          modifiedMetadata.abstractId =
            get(existingMetadata, 'abstract.id') || null

          modifiedMetadata.abstractTitle =
            get(existingMetadata, 'abstract.title') || null

          // fix section details being an array of arrays instead of an array
          // of objects
          modifiedMetadata.section_details = Array.isArray(
            existingMetadata.section_details,
          )
            ? existingMetadata.section_details.map(item =>
                Array.isArray(item) ? item[0] : item,
              )
            : []

          // fix null titles in section details
          modifiedMetadata.section_details = modifiedMetadata.section_details.map(
            detail => {
              const { title } = detail

              return {
                ...detail,
                title: title || '',
              }
            },
          )

          // fix null array items in section titles
          if (has(existingMetadata, 'section_titles')) {
            const problem = existingMetadata.section_titles.find(
              i => typeof i !== 'string',
            )

            if (typeof problem !== 'undefined') {
              modifiedMetadata.section_titles = existingMetadata.section_titles.filter(
                i => i !== null,
              )
            }
          }

          fieldsToDelete.forEach(field => {
            if (has(existingMetadata, field)) {
              delete modifiedMetadata[field]
            }
          })

          const newMetadata = {
            ...defaultMetadata,
            ...modifiedMetadata,
          }

          const patchData = {
            metadata: newMetadata,
          }

          return bookComponent.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
