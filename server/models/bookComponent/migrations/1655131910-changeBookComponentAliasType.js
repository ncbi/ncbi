/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)

exports.up = async knex => {
  await changeAliasColumnType(knex)
  await updateBookComponentAliasValue(knex)
}

const changeAliasColumnType = knex =>
  knex.schema.table('book_component', table => {
    table.string('alias').alter()
  })

const updateBookComponentAliasValue = async knex => {
  const getNewAliasValue = bookComponent =>
    bookComponent.versionName
      ? `${bookComponent.alias}.${bookComponent.versionName}`
      : `${bookComponent.alias}.1`

  try {
    return useTransaction(async trx => {
      const bookComponents = await BookComponent.query(trx)

      return Promise.all(
        bookComponents.map(bookComponent => {
          const alias = getNewAliasValue(bookComponent)
          return bookComponent.$query(trx).patch({ alias })
        }),
      )
    })
  } catch (err) {
    throw new Error(err)
  }
}
