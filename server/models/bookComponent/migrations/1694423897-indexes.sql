CREATE INDEX book_component_book_id_published_date_idx ON public.book_component USING btree (book_id, published_date);
CREATE INDEX book_component_book_id_updated_idx ON public.book_component USING btree (book_id, updated);
CREATE INDEX book_component_component_type_idx ON public.book_component USING btree (component_type, book_id);