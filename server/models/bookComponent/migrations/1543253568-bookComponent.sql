CREATE SEQUENCE IF NOT EXISTS universal_bcms_id;

create table book_component (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,
  
  -- foreign
  division_id uuid not null references divisions,
  book_id uuid not null references books,
  book_component_version_id uuid not null,
  parent_id uuid,
  owner_id uuid references users,

  -- own
  book_components jsonb not null,
  abstract text,
  status text,
  version_name text,
  component_type text,
  published_date timestamp with time zone default current_timestamp,
  title text,
  metadata jsonb,
  unique(book_component_version_id),
  alias integer NOT NULL DEFAULT nextval('universal_bcms_id')
);
