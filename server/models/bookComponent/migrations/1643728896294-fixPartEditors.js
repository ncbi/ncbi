/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const bookComponents = await BookComponent.query(
        trx,
      ).whereIn('component_type', ['part'])

      return Promise.all(
        bookComponents.map(part => {
          const { metadata } = part

          const editors = metadata.author
          const newEditors = editors

          const patchData = {
            metadata: {
              ...metadata,
              author: [],
              editor: newEditors,
            },
          }

          return part.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
