const { has } = require('lodash')
const model = require('./bookComponent')

module.exports = {
  model,
  modelName: 'BookComponent',
  modelLoaders: {
    divisionMapped: async divisionIds => {
      const bookComponents = await model
        .query()
        .where({ parentId: null })
        .whereIn('divisionId', divisionIds)

      // Map book components to their divisions
      const bcDivisionMap = {}

      bookComponents.forEach(bc => {
        if (!has(bcDivisionMap, bc.divisionId)) {
          bcDivisionMap[bc.divisionId] = {}
        }

        bcDivisionMap[bc.divisionId][bc.id] = bc
      })

      // We map over ids so that the DataLoader API is always matched,
      // i.e. array of keys in, array of results out (even if some records are not found)
      return divisionIds.map(divisionId => bcDivisionMap[divisionId])
    },
  },
}
