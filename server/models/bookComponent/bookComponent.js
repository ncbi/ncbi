/* eslint-disable no-shadow */
/* eslint-disable import/no-unresolved */
/* eslint-disable no-return-await */
/* eslint-disable global-require */

const { Model } = require('objection')
const BaseModel = require('@pubsweet/base-model')
const uuid = require('uuid')
const { has, uniq, get } = require('lodash')

const {
  arrayOfIdsDefaultEmpty,
  id,
  idNullable,
  string,
  stringNullableDefaultEmpty,
  stringNullable,
  booleanDefaultFalse,
  arrayOfStringsDefaultEmpty,
  arrayOfStringOrNullsDefaultEmpty,
} = require('../_helpers/types')

const StatusService = require('../../services/statusService/statusService')
const Errors = require('../error/errors')

class BookComponent extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'bookComponent'
  }

  static get tableName() {
    return 'book_component'
  }

  static get orderMappings() {
    const { model: Book } = require('../book')
    // See "orderModel.js" for details on the return value
    return {
      field: {
        idColumn: 'id',
        column: 'bookComponents',
        foreignColumn: 'id',
        modelClass: BookComponent,
      },
      predicate: {
        modelClass: Book,
        idColumn: 'id',
        foreignColumn: 'bookId',
        field: book => {
          const value = (book.settings.toc || {}).order_chapters_by || 'manual'
          const numberValues = { number: 'asc', number_desc: 'desc' }

          if (has(numberValues, value))
            return {
              field: 'metadata.chapter_number',
              type: 'Number',
              order: numberValues[value],
              value: v => (v ? v.replace(/\D/g, '') : 0),
            }
          // Sort by publication date, updated date, creation date in that order
          // NB: these are the dates relating to the real world book/chapter
          // and NOT the dates they were created or published to the database
          if (value === 'date_desc')
            return {
              field: chapter => {
                return (
                  get(chapter, 'metadata.date_publication', null) ||
                  get(chapter, 'metadata.date_updated', null) ||
                  get(chapter, 'metadata.date_created', null)
                )
              },
              type: 'Date',
              order: 'desc',
            }
          if (value === 'title')
            return {
              field: 'title',
              type: 'String',
              order: 'asc',
              value: v => {
                return v === 'Untitled' ? null : v
              },
            }

          return {
            field: value,
            type: 'String',
            order: 'asc',
          }
        },
      },
    }
  }

  $beforeInsert() {
    super.$beforeInsert()
    this.bookComponentVersionId = this.bookComponentVersionId || uuid.v4()
  }

  static async beforeInsert({ transaction, inputItems }) {
    super.beforeInsert()
    const [item] = inputItems

    if (item && item.status) {
      item.status = await StatusService.updateEntity(item, item, transaction)
    }
  }

  static async beforeUpdate({ transaction, inputItems, asFindQuery, context }) {
    super.beforeUpdate()

    const [item] = inputItems

    if (
      (item.status && context.ignoreUpdate === undefined) ||
      context.ignoreUpdate === false
    ) {
      const [entity] = await asFindQuery()

      if (item.status !== entity.status) {
        item.status = await StatusService.updateEntity(
          entity,
          item,
          transaction,
        )
      }
    }

    // if (item.metadata) {
    //   const [current] = await asFindQuery()

    //   item.metadata = {
    //     ...current.metadata,
    //     ...item.metadata,
    //   }
    // }
  }

  static get relationMappings() {
    const { model: Book } = require('../book')
    const { model: fileVersionModel } = require('../fileVersion')
    const { model: FileModel } = require('../file')
    const { model: Division } = require('../division')
    const { model: User } = require('../user')

    return {
      book: {
        join: {
          from: 'book_component.book_id',
          to: 'book.id',
        },
        modelClass: Book,
        relation: Model.BelongsToOneRelation,
      },
      division: {
        join: {
          from: 'BookComponent.divisionId',
          to: 'Division.id',
        },
        modelClass: Division,
        relation: Model.BelongsToOneRelation,
      },
      files: {
        join: {
          from: 'book_component.id',
          to: 'file_versions.book_component_id',
        },
        modelClass: fileVersionModel,
        relation: BaseModel.HasManyRelation,
      },
      owner: {
        join: {
          from: 'book_component.owner_id',
          to: 'users.id',
        },
        modelClass: User,
        relation: Model.HasOneRelation,
      },
      bookComponentFiles: {
        join: {
          from: 'book_component.id',
          through: {
            from: 'file_versions.book_component_id',
            to: 'file_versions.file_id',
          },
          to: 'files.id',
        },
        modelClass: FileModel,
        relation: BaseModel.ManyToManyRelation,
      },
    }
  }

  static get schema() {
    return {
      type: 'object',
      required: ['bookId'],
      properties: {
        abstract: stringNullable,
        alias: stringNullable,
        bookComponents: arrayOfIdsDefaultEmpty,
        bookComponentVersionId: idNullable,
        bookId: idNullable,
        componentType: stringNullable,
        divisionId: stringNullable,
        metadata: {
          type: 'object',
          additionalProperties: false,
          default: {
            abstractId: null,
            abstractTitle: null,
            alt_title: '',
            author: [],
            collaborativeAuthors: [],
            book_component_id: '',
            chapter_number: null,
            date_created: null,
            date_updated: null,
            date_revised: null,
            date_publication: null,
            date_publication_format: null,
            doi: null,
            editor: [],
            fileId: null,
            filename: '',
            grants: [],
            language: 'en',
            section_details: [],
            section_titles: [],
            sub_title: '',
            url: null,
            hideInTOC: false,
          },
          properties: {
            abstractId: stringNullable,
            abstractTitle: stringNullable,
            alt_title: stringNullable,
            author: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                properties: {
                  affiliation: arrayOfStringOrNullsDefaultEmpty,
                  degrees: stringNullable,
                  email: stringNullable,
                  givenName: stringNullable,
                  role: stringNullable,
                  suffix: stringNullable,
                  surname: stringNullable,
                },
              },
            },
            collaborativeAuthors: {
              default: [],
              type: 'array',
            },
            book_component_id: stringNullable,
            chapter_number: stringNullable,
            date_created: stringNullableDefaultEmpty,
            date_publication: stringNullableDefaultEmpty,
            date_publication_format: stringNullableDefaultEmpty,
            date_revised: stringNullableDefaultEmpty,
            date_updated: stringNullableDefaultEmpty,
            doi: stringNullable,
            editor: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                properties: {
                  affiliation: arrayOfStringOrNullsDefaultEmpty,
                  degrees: stringNullable,
                  email: stringNullable,
                  givenName: stringNullable,
                  role: stringNullable,
                  suffix: stringNullable,
                  surname: stringNullable,
                },
              },
            },
            fileId: idNullable,
            filename: stringNullableDefaultEmpty,
            grants: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                additionalProperties: false,
                required: [
                  'id',
                  'country',
                  'institution_acronym',
                  'institution_code',
                  'institution_name',
                  'number',
                ],
                properties: {
                  id,
                  country: string,
                  institution_acronym: string,
                  institution_code: string,
                  institution_name: string,
                  number: string,
                },
              },
            },
            language: {
              default: 'en',
              ...stringNullable,
            },
            section_details: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                additionalProperties: false,
                required: ['id', 'title'],
                properties: {
                  id: string,
                  label: stringNullable,
                  title: string,
                },
              },
            },
            section_titles: {
              type: 'array',
              default: [],
              items: string,
            },
            sub_title: stringNullable,
            url: stringNullable,
            hideInTOC: booleanDefaultFalse,
          },
        },
        ownerId: stringNullable,
        parentId: stringNullable,
        publishedDate: stringNullable,
        assigneeErrors: arrayOfStringsDefaultEmpty,
        hasWarnings: booleanDefaultFalse,
        status: {
          type: 'string',
          default: 'new-upload',
          enum: [
            'submission-errors',
            'new-upload',
            'converting',
            'tagging',
            'tagging-errors',
            'failed',
            'conversion-errors',
            'loading-preview',
            'loading-errors',
            'preview',
            'in-review',
            'approve',
            'unpublished',
            'published',
            'publishing',
            'publish-failed',
            'error', // TODO deprecated after changing the errors
            'new-version',
          ],
        },
        title: stringNullable,
        versionName: {
          type: 'string',
          default: '1',
        },
      },
    }
  }

  getBook() {
    return this.$query().joinEager('book')
  }

  getDivision() {
    return this.$query().joinEager('division')
  }

  static async publishedVersions(bookComponent, trx) {
    const parentId = bookComponent.parentId
      ? bookComponent.parentId
      : bookComponent.id

    const bookComponentModel = await BookComponent.query(trx)
      .where({
        parentId,
      })
      .orWhere({
        id: parentId,
        parentId: null,
      })
      .orderByRaw('book_component.version_name::int asc')

    return bookComponentModel
  }

  static async hasPublishedVersions(bookComponent, trx) {
    const { model: FileVersion } = require('../fileVersion')

    const BookComponentVersions = await BookComponent.publishedVersions(
      bookComponent,
      trx,
    )

    // eslint-disable-next-line no-return-await
    return await FileVersion.query(trx)
      .whereIn(
        'bookComponentId',
        BookComponentVersions.map(bc => bc.id),
      )
      .andWhere(query => {
        query.where({ status: 'published' })
      })
  }

  static getFiles() {
    return BookComponent.relatedQuery('bookComponentFiles').select([
      'files.id',
      'files.created',
      'files.updated',
      'files.mimeType',
      'files.name',
      'files.category',
      'file_versions.copied',
      'file_versions.tag',
      'file_versions.version_name',
      'file_versions.bookComponentId',
      'file_versions.id as bcfId',
      'file_versions.parentId',
      'file_versions.status',
      'file_versions.ownerId',
    ])
  }

  static typesPerDivision() {
    return {
      backMatter: ['appendix', 'glossary', 'ref-list'],
      body: ['chapter', 'section', 'part'],
      frontMatter: ['dedication', 'foreword', 'front-matter-part', 'preface'],
    }
  }

  async addFile(file, category, status = null, transaction = null) {
    const { model: FileVersion } = require('../fileVersion')
    const matchOnlyNameCategories = ['source', 'image', 'supplement', 'support']

    const bookComponentQuery = FileVersion.query(transaction)
      .alias('bookComponentFile')
      .joinEager('file')
      .where({
        bookComponentId: this.id,
        'bookComponentFile.category': category,
      })

    if (matchOnlyNameCategories.includes(category)) {
      bookComponentQuery.andWhere(builder => {
        builder.where('name', 'like', `${file.name.replace(/\.[^/.]+$/, '')}.%`)
      })
    } else if (category !== 'converted') {
      bookComponentQuery.andWhere(builder => {
        builder.where({ name: file.name })
      })
    }

    const bookComponentFileExists = await bookComponentQuery

    let parentId = uuid.v4()
    let versionName = '1'
    let maxElement = { tag: null }

    if (bookComponentFileExists.length > 0) {
      parentId = bookComponentFileExists[0].parentId

      maxElement = bookComponentFileExists.reduce(
        (prev, current) =>
          Number(prev.versionName) > Number(current.versionName)
            ? prev
            : current,
        1,
      )

      versionName = (parseInt(maxElement.versionName, 10) + 1).toString()
    }

    return await BookComponent.relatedQuery('files', transaction)
      .for(this)
      .insert({
        bookComponentVersionId: null,
        category,
        fileId: file.id,
        status,
        parentId,
        ownerId: file.ownerId,
        versionName,
        tag: file.tag || maxElement.tag,
      })
      .returning('*')
  }

  static async latestComponentFiles(entity, category = null, trx) {
    const { FileVersion } = require('@pubsweet/models')

    const search = {
      bookComponentId: entity.id,
    }

    if (category) {
      search['file_versions.category'] = category
    }

    // eslint-disable-next-line no-return-await
    return await FileVersion.query(trx)
      .select(['*'])
      .distinctOn('file_versions.parent_id')
      .withGraphJoined('file')
      .where(search)
      .orderByRaw(
        '"file_versions"."parent_id" , "file_versions"."created" desc',
      )
  }

  static async updateErrorSummary(id, trx) {
    const errors = await Errors.query(trx)
      .where({ objectId: id, history: false })
      .orderBy('created')

    const asignes = errors
      .filter(x => x.severity === 'error')
      .map(err => err.assignee)
      .sort()

    const assigneeErrors = (uniq(asignes) || []).filter(
      x => x.toLowerCase() !== 'unknown',
    )

    const warnings = errors.filter(err => err.severity === 'query')

    const hasWarnings = !!warnings.length

    await BookComponent.query(trx)
      .patch({ assigneeErrors, hasWarnings })
      .findOne({ id })
  }

  async hasPublishedChildren() {
    let published = []

    const children = await BookComponent.query().whereIn(
      'id',
      this.bookComponents,
    )

    const filtered = children.filter(child => child.componentType === 'part')

    const filterPublished = children.filter(
      child =>
        child.componentType !== 'part' &&
        child.componentType !== 'toc' &&
        child.status === 'published',
    )

    if (filterPublished.length > 0) {
      published = published.concat(filterPublished)
    }

    published = published.concat(
      await Promise.all(
        filtered.map(async bc => await bc.hasPublishedChildren()),
      ),
    )

    return published.length > 0 ? published.filter(pub => pub !== null) : null
  }

  async createTeams(trx) {
    const {
      models: [{ model: Team }],
    } = require('@pubsweet/model-team')

    const teams = [
      {
        role: 'author',
        name: 'Author',
        objectType: this.type,
        objectId: this.id,
      },
      {
        role: 'previewer',
        name: 'Previewer',
        objectType: this.type,
        objectId: this.id,
      },
      {
        role: 'editor',
        name: 'Editor',
        objectType: this.type,
        objectId: this.id,
      },
    ]

    // eslint-disable-next-line no-return-await
    return await Team.query(trx).insert(teams).returning('*')
  }
}

module.exports = BookComponent
