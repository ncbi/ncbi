const BookComponent = require('../bookComponent')
const Book = require('../../book/book')
const Organisation = require('../../organisation/organisation')
const Division = require('../../division/division')

const clearDb = require('../../_helpers/clearDb')

const createBookComponent = async data => {
  const org = await Organisation.query().insert({})

  const book = await Book.query().insert({
    organisationId: org.id,
    title: 'my book',
  })

  const division = await Division.query().insert({
    bookId: book.id,
    label: 'my division',
  })

  const bc = await BookComponent.query().insert({
    bookId: book.id,
    divisionId: division.id,
    ...data,
  })

  return bc
}

describe('Book component', () => {
  beforeEach(() => clearDb())

  afterAll(async () => {
    await clearDb()
    const knex = BookComponent.knex()
    knex.destroy()
  })

  test('adds full set of metadata on insert when no metadata is given', async () => {
    const bc = await createBookComponent({})
    expect(bc.metadata.grants).toEqual([])
  })

  test('adds full set of metadata on insert when partial metadata is given', async () => {
    const bc = await createBookComponent({
      metadata: {
        sub_title: 'sub',
      },
    })

    expect(bc.metadata.grants).toEqual([])
  })

  test('retains full set of metadata on partial metadata patch', async () => {
    const bc = await createBookComponent({})

    expect(bc.metadata.grants).toEqual([])

    // try static method
    const updated = await BookComponent.query().patchAndFetchById(bc.id, {
      metadata: {
        sub_title: 'sub',
      },
    })

    expect(updated.metadata.sub_title).toEqual('sub')
    expect(updated.metadata.grants).toEqual([])

    // try instance method
    const modified = await updated.$query().patchAndFetch({
      metadata: {
        alt_title: 'alt',
      },
    })

    expect(modified.metadata.alt_title).toEqual('alt')
    expect(modified.metadata.sub_title).toEqual('sub')
    expect(modified.metadata.grants).toEqual([])
  })
})
