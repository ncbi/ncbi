const BaseModel = require('@pubsweet/base-model')

class DashboardView extends BaseModel {
  static get tableName() {
    return 'dashboard'
  }
}

module.exports = DashboardView
