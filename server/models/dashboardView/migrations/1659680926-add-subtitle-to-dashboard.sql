DROP VIEW IF EXISTS dashboard;

CREATE VIEW dashboard (id, type, created, updated, workflow, organisation_id, title, metadata, settings) AS
Select id, type, created, updated, workflow, organisation_id, title, metadata, settings, status, alias, published_date, funded_content_type, version, sub_title from Books union all
Select id, type, created, updated, workflow, organisation_id, title, metadata, settings, status, alias, NULL as published_date, NULL as funded_content_type, NULL as version, NULL as sub_title from Collections
WHERE settings @> '{"isCollectionGroup": false}'
;
