CREATE VIEW dashboard (id, type, created, updated, workflow, organisation_id, title, metadata, settings)
  AS Select id, type, created, updated, workflow, organisation_id, title, metadata, settings, status, alias from Books union all
  Select id, type, created, updated, workflow, organisation_id, title, metadata, settings, status, alias from Collections
;
