DROP VIEW IF EXISTS dashboard;

CREATE VIEW dashboard (id, type, created, updated, workflow, organisation_id, title, metadata, settings)
  AS Select id, type, created, updated, workflow, organisation_id, title, metadata, settings, status, alias, published_date from Books union all
  Select id, type, created, updated, workflow, organisation_id, title, metadata, settings, status, alias, NULL as published_date from Collections WHERE collection_type = 'collection';
