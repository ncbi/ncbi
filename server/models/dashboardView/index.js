const model = require('./dashboardView')

module.exports = {
  model,
  modelName: 'DashboardView',
}
