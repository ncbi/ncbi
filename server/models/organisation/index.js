const model = require('./organisation')

module.exports = {
  model,
  modelName: 'Organisation',
}
