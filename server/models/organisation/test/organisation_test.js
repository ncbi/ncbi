/* eslint-disable no-return-await */
const path = require('path')
const { Book, Organisation } = require('@pubsweet/models')

const pathToComponent = path.resolve(__dirname, '..', 'organisation')
const pathToBook = path.resolve(__dirname, '..', '..', 'book')

process.env.NODE_CONFIG = `{"pubsweet":{
  "components":[
    "@pubsweet/model-user",
    "@pubsweet/model-team",
    "${pathToBook}",
    "${pathToComponent}"
  ]
}}`

const dbCleaner = require('../../helpers/db_cleaner')

describe('Organisation', () => {
  beforeEach(async () => {
    await dbCleaner()
  })

  it('has updated set when created', async () => {
    const organisation = await new Organisation({ name: 'Test' }).save()
    expect(organisation.name).toEqual('Test')
    const now = new Date().toISOString()
    expect(organisation.updated.toISOString()).toHaveLength(now.length)
  })

  it('can be saved and found and deleted', async () => {
    const organisation = await new Organisation({ name: 'Test' }).save()
    expect(organisation.name).toEqual('Test')

    const foundOrganisation = await Organisation.find(organisation.id)
    expect(foundOrganisation.name).toEqual('Test')

    await foundOrganisation.delete()

    async function tryToFind() {
      await Organisation.find(foundOrganisation.id)
    }

    await expect(tryToFind()).rejects.toThrow('Object not found')
  })

  it('can be found by property', async () => {
    await new Organisation({ name: 'Test' }).save()
    const organisation = await Organisation.findOneByField('name', 'Test')
    expect(organisation.name).toEqual('Test')

    let organisations = await Organisation.findByField('name', 'Test')
    expect(organisations[0].name).toEqual('Test')

    async function findMissing() {
      await Organisation.findOneByField('name', 'Does not exist')
    }

    await expect(findMissing()).rejects.toThrow('Object not found')

    organisations = await Organisation.findByField('name', 'Does not exist')
    expect(organisations).toEqual([])
  })

  it('can not be saved with non-valid properties', async () => {
    async function createNonValidOrganisation() {
      await new Organisation({ mumbo: 'jumbo' }).save()
    }

    await expect(createNonValidOrganisation()).rejects.toThrow(
      'mumbo: is an invalid additional property',
    )
  })

  it('can save new entity with known ID', async () => {
    const id = '1838d074-fb9d-4ed6-9c63-39e6bc7429ce'
    const organisation = await new Organisation({ id, name: 'Test' }).save()
    expect(organisation.id).toEqual(id)
  })

  it('can get books of organisation', async () => {
    const organisation = await new Organisation({ name: 'Test' }).save()

    const otherOrganisation = await new Organisation({
      name: 'other Test',
    }).save()

    const book1 = await new Book({
      organisationId: organisation.id,
      title: 'book',
    }).save()

    await new Book({
      organisationId: organisation.id,
      title: 'book',
    }).save()

    await new Book({
      organisationId: otherOrganisation.id,
      title: 'book3',
    }).save()

    const { books } = await organisation.getBooks()

    expect(books).toHaveLength(2)

    books.forEach(book => {
      expect(book.title).toEqual(book1.title)
    })
  })

  /* eslint-disable-next-line jest/no-commented-out-tests */
  // it('can get users of organisation', async () => {
  //   const user = await new User({
  //     email: 'some@example.com',
  //     username: 'test1',
  //   }).save()

  //   const otherUser = await new User({
  //     email: 'some1@example.com',
  //     username: 'test2',
  //   }).save()

  //   const members = [{ user: { id: user.id } }, { user: { id: otherUser.id } }]

  //   const { id } = await new Organisation({ name: 'Test' }).save()
  //   await new Organisation({ name: 'other Test' }).save()

  //   await Team.query().upsertGraphAndFetch(
  //     {
  //       members,
  //       name: 'Organisation Admin',
  //       objectId: id,
  //       objectType: 'Organisation',
  //       role: 'OrganisationAdmin',
  //     },
  //     {
  //       relate: true,
  //       unrelate: true,
  //     },
  //   )

  //   // members.forEach(mem => {
  //   //   expect(users.map(u => u.userId)).toContain(mem.user.id)
  //   // })
  // })
})
