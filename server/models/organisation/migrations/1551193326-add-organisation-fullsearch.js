const addOrganizationIndex = `
CREATE FUNCTION update_search_organization_document()
RETURNS trigger AS $$
BEGIN
  NEW.document := to_tsvector(NEW."name" || ' ' || NEW.email);
  RETURN NEW;
END $$ LANGUAGE 'plpgsql';
CREATE TRIGGER update_search_organization_document
BEFORE INSERT ON public.organisations
FOR EACH ROW
EXECUTE PROCEDURE update_search_organization_document();
CREATE INDEX idx_fts_organization ON public.organisations USING gin(document);
`

exports.up = async knex => knex.schema.raw(addOrganizationIndex)

const removeUserIndex = `
  DROP FUNCTION IF EXISTS update_search_organization_document();
`

exports.down = knex =>
  Promise.all([
    knex.schema.dropTableIfExists('organisations'),
    knex.schema.raw(removeUserIndex),
  ])
