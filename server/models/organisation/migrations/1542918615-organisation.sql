create table organisations (
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,
  name varchar(255),
  email varchar(255),
  abbreviation varchar(255),
  agreement Boolean NOT NULL default false,
  publisher_id text,
  settings jsonb,
  book_count text default 0,
  document tsvector
);
