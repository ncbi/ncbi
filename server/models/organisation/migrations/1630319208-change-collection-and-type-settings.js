const { logger, useTransaction } = require('@coko/server')
const cloneDeep = require('lodash/cloneDeep')

// eslint-disable-next-line import/no-dynamic-require
const Organisation = require(`${process.cwd()}/server/models/organisation/organisation`)

exports.up = async knex => {
  try {
    await useTransaction(async trx => {
      const organisations = await Organisation.query(trx)

      await Promise.all(
        organisations.map(async org => {
          const { collections, type } = org.settings

          let typeSettings = type

          if (typeof typeSettings === 'string') {
            typeSettings = {
              publisher: type === 'publisher',
              funder: type === 'funder',
            }
          }

          let collectionsSettings = collections

          if (typeof collectionsSettings === 'boolean') {
            collectionsSettings = {
              funded: false,
              bookSeries: !!collections, // collections is a boolean
            }
          }

          const newSettings = cloneDeep(org.settings)
          newSettings.type = typeSettings
          newSettings.collections = collectionsSettings

          await Organisation.query(trx)
            .patch({
              settings: newSettings,
            })
            .findById(org.id)
        }),
      )
    })
  } catch (e) {
    logger.error(
      `Organization type and collection settings: Migration failed! Rolling back...`,
    )

    throw new Error(e)
  }
}
