/* eslint-disable global-require */
const BaseModel = require('@pubsweet/base-model')

const { Model } = require('objection')
const useTransaction = require('../helpers/useTransaction')

class Organisation extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'Organisation'
  }

  static get tableName() {
    return 'organisations'
  }

  static get schema() {
    return {
      properties: {
        agreement: {
          type: ['boolean', false],
        },

        name: {
          type: ['string', 'null'],
        },
        abbreviation: {
          type: ['string', 'null'],
        },
        publisherId: {
          type: ['string', 'null'],
        },
        settings: {
          default: {
            xml: false,
            pdf: false,
            word: false,
            collections: { funded: false, bookSeries: false },
            type: {
              publisher: false,
              funder: false,
            },
          },
          type: 'object',
        },
        required: ['name'],
      },
      type: 'object',
    }
  }

  static get relationMappings() {
    const Book = require('../book/book')

    const {
      models: [{ model: Team }],
    } = require('@pubsweet/model-team')

    return {
      books: {
        join: {
          from: 'organisations.id',
          to: 'books.organisation_id',
        },
        modelClass: Book,
        relation: Model.HasManyRelation,
      },
      teams: {
        filter(builder) {
          builder.where('objectType', 'Organisation')
        },
        join: {
          from: 'organisations.id',
          to: 'teams.object_id',
        },
        modelClass: Team,
        relation: Model.HasManyRelation,
      },
    }
  }

  getBooks() {
    return this.$query().joinEager('books')
  }

  getTeams() {
    return this.$query().joinEager('teams')
  }

  static async create(data) {
    const {
      models: [{ model: Team }],
    } = require('@pubsweet/model-team')

    const newOrg = await useTransaction(async trx => {
      const organisation = await Organisation.query(trx).insert(data)

      const teams = [
        {
          role: 'orgAdmin',
          name: 'Org Admin',
          objectType: 'Organisation',
          objectId: organisation.id,
        },
        {
          role: 'sysAdmin',
          name: 'Sys Admin',
          objectType: 'Organisation',
          objectId: organisation.id,
        },
        {
          role: 'user',
          name: 'Org User',
          objectType: 'Organisation',
          objectId: organisation.id,
        },
        {
          role: 'editor',
          name: 'Editor',
          objectType: 'Organisation',
          objectId: organisation.id,
        },
        {
          role: 'author',
          name: 'Author',
          objectType: 'Organisation',
          objectId: organisation.id,
        },
        {
          role: 'previewer',
          name: 'Previewer',
          objectType: 'Organisation',
          objectId: organisation.id,
        },
        {
          role: 'awardee',
          name: 'Awardee',
          objectType: 'Organisation',
          objectId: organisation.id,
        },
      ]

      await Team.query(trx).insert(teams)
      return organisation
    })

    return newOrg
  }
}

module.exports = Organisation
