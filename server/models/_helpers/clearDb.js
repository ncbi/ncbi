const {
  BookComponent,
  FileVersion,
  BookSettingsTemplate,
  Channel,
  ChannelMember,
  Collection,
  Division,
  Errors,
  Organisation,
  SourceConvertedFile,
  Team,
  TeamMember,
} = require('@pubsweet/models')

const Book = require('../book/book')
const User = require('../user/user')

const clear = async () => {
  await ChannelMember.query().delete()
  await Channel.query().delete()
  await Errors.query().delete()
  await SourceConvertedFile.query().delete()
  await FileVersion.query().delete()
  await BookComponent.query().delete()
  await Division.query().delete()
  await Book.query().delete()
  await TeamMember.query().delete()
  await Team.query().delete()
  await BookSettingsTemplate.query().delete()
  await Collection.query().delete()
  await User.query().delete()
  await Organisation.query().delete()
}

module.exports = clear
