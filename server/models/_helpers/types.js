const boolean = {
  type: 'boolean',
}

const booleanNullable = {
  type: ['boolean', null],
}

const booleanDefaultFalse = {
  ...boolean,
  default: false,
}

const id = {
  type: 'string',
  format: 'uuid',
}

const idNullable = {
  type: ['string', null],
  format: 'uuid',
}

const integerNullable = {
  type: ['integer', null],
}

const number = {
  type: 'number',
}

const string = {
  type: 'string',
}

const stringNullable = {
  type: ['string', null],
}

const stringNullableDefaultEmpty = {
  ...stringNullable,
  default: '',
}

const stringNullableDefaultNull = {
  ...stringNullable,
  default: null,
}

const arrayOfIdsDefaultEmpty = {
  default: [],
  items: id,
  type: 'array',
}

const arrayOfStringsDefaultEmpty = {
  default: [],
  items: string,
  type: 'array',
}

const dateTime = {
  type: ['object', null],
  format: 'date-time',
}

const arrayOfStringOrNullsDefaultEmpty = {
  default: [],
  items: string,
  type: ['array', 'null'],
}

module.exports = {
  arrayOfIdsDefaultEmpty,
  arrayOfStringsDefaultEmpty,
  arrayOfStringOrNullsDefaultEmpty,
  boolean,
  booleanNullable,
  booleanDefaultFalse,
  dateTime,
  id,
  idNullable,
  integerNullable,
  number,
  string,
  stringNullable,
  stringNullableDefaultEmpty,
  stringNullableDefaultNull,
}
