/* eslint-disable global-require */
const BaseModel = require('@pubsweet/base-model')

class Channel extends BaseModel {
  static get tableName() {
    return 'channels'
  }

  static get relationMappings() {
    const {
      models: [{ model: Team }],
    } = require('@pubsweet/model-team')

    const User = require('../user/user')
    const Message = require('../message/message')
    const ChannelMember = require('./channel_member')

    return {
      messages: {
        relation: BaseModel.HasManyRelation,
        modelClass: Message,
        join: {
          from: 'channels.id',
          to: 'messages.channelId',
        },
      },
      members: {
        relation: BaseModel.HasManyRelation,
        modelClass: ChannelMember,
        join: {
          from: 'channels.id',
          to: 'channel_members.channelId',
        },
      },
      team: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: Team,
        join: {
          from: 'channels.teamId',
          to: 'teams.id',
        },
      },
      users: {
        relation: BaseModel.ManyToManyRelation,
        modelClass: User,
        join: {
          from: 'channels.id',
          through: {
            modelClass: require.resolve('./channel_member'),
            from: 'channel_members.channelId',
            to: 'channel_members.userId',
          },
          to: 'users.id',
        },
      },
    }
  }

  static get schema() {
    return {
      properties: {
        topic: { type: 'string' },
        teamId: { type: ['string', 'null'], format: 'uuid' },
        objectId: { type: ['string', 'null'], format: 'uuid' },
      },
    }
  }
}

module.exports = Channel
