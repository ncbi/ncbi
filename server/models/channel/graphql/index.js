const typeDefs = `
  type Channel {
    id: String
    objectId: ID
    topic: String
  }
`

module.exports = { typeDefs }
