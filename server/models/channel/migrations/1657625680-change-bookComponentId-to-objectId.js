/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-return-await */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Channel = require(`${process.cwd()}/server/models/channel/channel`)
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  await addObjectIdColumn(knex)
  await populateObjectIdFromBookComponentId(knex)
  await dropBookComponentIdColumn(knex)
}

const addObjectIdColumn = knex =>
  knex.schema.table('channels', table => {
    table.uuid('object_id').nullable()
  })

const populateObjectIdFromBookComponentId = knex => {
  const getObjectIdForChannel = async (channel, trx) => {
    const bookComponent = await BookComponent.query(trx)
      .where({ id: channel.bookComponentId })
      .first()

    const book = await Book.query(trx)
      .where({ id: bookComponent.bookId })
      .first()

    return !book.settings.chapterIndependently ? book.id : bookComponent.id
  }

  try {
    return useTransaction(async trx => {
      const channels = await Channel.query(trx)

      return Promise.all(
        channels.map(async channel => {
          const objectId = await getObjectIdForChannel(channel, trx)
          return channel.$query(trx).patchAndFetch({ objectId })
        }),
      )
    })
  } catch (err) {
    throw new Error(err)
  }
}

const dropBookComponentIdColumn = knex =>
  knex.schema.table('channels', table => {
    table.dropColumn('book_component_id')
  })
