CREATE TABLE book_settings_templates (
  -- base
  id UUID PRIMARY KEY,
  type TEXT NOT NULL,
  created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  updated TIMESTAMP WITH TIME ZONE,

  -- own
  organization_id UUID REFERENCES organisations,
  collection_id UUID REFERENCES collections,
  domain_id TEXT NOT NULL,
  domain_name TEXT NOT NULL,
  template_type TEXT NOT NULL,
  values JSONB
)