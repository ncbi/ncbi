CREATE UNIQUE INDEX one_collection_members_template_type_per_collection
ON book_settings_templates (collection_id, template_type)
WHERE collection_id IS NOT NULL;
