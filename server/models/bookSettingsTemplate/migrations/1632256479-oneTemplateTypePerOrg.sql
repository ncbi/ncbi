CREATE UNIQUE INDEX one_template_type_per_org
ON book_settings_templates (organization_id, template_type)
WHERE collection_id IS NULL;
