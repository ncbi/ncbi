const { v4: uuid } = require('uuid')

const Organization = require('../../organisation/organisation')
const Collection = require('../../collection/collection')
const BookSettingsTemplate = require('../bookSettingsTemplate')
const User = require('../../user/user')

const clearDb = require('../../_helpers/clearDb')

describe('Template', () => {
  beforeEach(() => clearDb())

  afterAll(async () => {
    await clearDb()
    const knex = BookSettingsTemplate.knex()
    knex.destroy()
  })

  test('creates template', async () => {
    const org = await Organization.query().insert({})

    const template = await BookSettingsTemplate.query().insert({
      organizationId: org.id,
      templateType: 'chapterProcessed',
      domainId: 'ipsum',
      domainName: 'lorem',
      values: {
        settings: {
          toc: {
            add_body_to_parts: true,
          },
        },
      },
    })

    expect(template.organizationId).toBe(org.id)
    expect(template.type).toBe('bookSettingsTemplate')
    expect(template.templateType).toBe('chapterProcessed')
    expect(template.values.settings.toc.add_body_to_parts).toBe(true)
  })

  test('does not allow missing organization id', async () => {
    const createTemplate = () =>
      BookSettingsTemplate.query().insert({
        templateType: 'chapterProcessed',
        domainId: 'ipsum',
        domainName: 'lorem',
        values: {
          settings: {
            toc: {
              add_body_to_parts: true,
            },
          },
        },
      })

    await expect(createTemplate()).rejects.toThrow()
  })

  test('does not allow missing values', async () => {
    const org = await Organization.query().insert({})

    const createTemplate = () =>
      BookSettingsTemplate.query().insert({
        organizationId: org.id,
        templateType: 'chapterProcessed',
        domainId: 'ipsum',
        domainName: 'lorem',
      })

    await expect(createTemplate()).rejects.toThrow()
  })

  test('does not allow missing template type', async () => {
    const org = await Organization.query().insert({})

    const createTemplate = () =>
      BookSettingsTemplate.query().insert({
        organizationId: org.id,
        domainId: 'ipsum',
        domainName: 'lorem',
        values: {
          settings: {
            toc: {
              add_body_to_parts: true,
            },
          },
        },
      })

    await expect(createTemplate()).rejects.toThrow()
  })

  test('does not allow missing domain name', async () => {
    const org = await Organization.query().insert({})

    const createTemplate = () =>
      BookSettingsTemplate.query().insert({
        organizationId: org.id,
        templateType: 'chapterProcessed',
        domainId: 'ipsum',
        values: {},
      })

    await expect(createTemplate()).rejects.toThrow()
  })

  test('does not allow missing domain id', async () => {
    const org = await Organization.query().insert({})

    const createTemplate = () =>
      BookSettingsTemplate.query().insert({
        organizationId: org.id,
        templateType: 'chapterProcessed',
        domainName: 'lorem',
        values: {},
      })

    await expect(createTemplate()).rejects.toThrow()
  })

  test('does not allow invalid organization id', async () => {
    const createTemplate = () =>
      BookSettingsTemplate.query().insert({
        organizationId: uuid(),
        templateType: 'chapterProcessed',
        domainId: 'ipsum',
        domainName: 'lorem',
        values: {
          settings: {
            toc: {
              add_body_to_parts: true,
            },
          },
        },
      })

    await expect(createTemplate()).rejects.toThrow()
  })

  test('does not allow invalid template type', async () => {
    const org = await Organization.query().insert({})

    const createTemplate = () =>
      BookSettingsTemplate.query().insert({
        organizationId: org.id,
        templateType: 'randomstring',
        domainId: 'ipsum',
        domainName: 'lorem',
        values: {
          settings: {
            toc: {
              add_body_to_parts: true,
            },
          },
        },
      })

    await expect(createTemplate()).rejects.toThrow()
  })

  test('does not allow invalid values', async () => {
    const org = await Organization.query().insert({})

    const createTemplate = () =>
      BookSettingsTemplate.query().insert({
        organizationId: org.id,
        templateType: 'chapterProcessed',
        domainId: 'ipsum',
        domainName: 'lorem',
        values: {
          settings: {
            toc: {
              add_body_to_parts: true,
            },
          },
          random: true,
        },
      })

    await expect(createTemplate()).rejects.toThrow()
  })

  test('does not allow multiple templates of the same type for org', async () => {
    const org = await Organization.query().insert({})

    // first chapter processed
    await BookSettingsTemplate.query().insert({
      organizationId: org.id,
      templateType: 'chapterProcessed',
      domainId: 'ipsum',
      domainName: 'lorem',
      values: {
        settings: {
          toc: {
            add_body_to_parts: true,
          },
        },
      },
    })

    // second chapter processed
    const createTemplate = () =>
      BookSettingsTemplate.query().insert({
        organizationId: org.id,
        templateType: 'chapterProcessed',
        domainId: 'ipsum',
        domainName: 'lorem',
        values: {
          settings: {
            toc: {
              add_body_to_parts: true,
            },
          },
        },
      })

    await expect(createTemplate()).rejects.toThrow()
  })

  test('does not allow multiple templates of the same type for collection', async () => {
    const user = await User.query().insert({})
    const org = await Organization.query().insert({})

    const collection = await Collection.query().insert({
      title: 'collection',
      ownerId: user.id,
      organisationId: org.id,
      collectionType: 'bookSeries',
    })

    // first chapter processed
    await BookSettingsTemplate.query().insert({
      organizationId: org.id,
      collectionId: collection.id,
      templateType: 'chapterProcessed',
      domainId: 'ipsum',
      domainName: 'lorem',
      values: {
        settings: {
          toc: {
            add_body_to_parts: true,
          },
        },
      },
    })

    // second chapter processed
    const createTemplate = () =>
      BookSettingsTemplate.query().insert({
        organizationId: org.id,
        collectionId: collection.id,
        templateType: 'chapterProcessed',
        domainId: 'ipsum',
        domainName: 'lorem',
        values: {
          settings: {
            toc: {
              add_body_to_parts: true,
            },
          },
        },
      })

    await expect(createTemplate()).rejects.toThrow()
  })

  test('allows a collection id if the collection is a book series collection', async () => {
    const user = await User.query().insert({})
    const org = await Organization.query().insert({})

    const collection = await Collection.query().insert({
      title: 'collection',
      ownerId: user.id,
      organisationId: org.id,
      collectionType: 'bookSeries',
    })

    const template = await BookSettingsTemplate.query().insert({
      organizationId: org.id,
      collectionId: collection.id,
      templateType: 'chapterProcessed',
      domainId: 'ipsum',
      domainName: 'lorem',
      values: {
        settings: {
          toc: {
            add_body_to_parts: true,
          },
        },
      },
    })

    expect(template.collectionId).toEqual(collection.id)
  })

  test('does not allow a collection id if the collection is not a book series collection', async () => {
    const user = await User.query().insert({})
    const org = await Organization.query().insert({})

    const collection = await Collection.query().insert({
      title: 'collection',
      ownerId: user.id,
      organisationId: org.id,
      collectionType: 'funded',
    })

    const createTemplate = () =>
      BookSettingsTemplate.query().insert({
        organizationId: org.id,
        collectionId: collection.id,
        templateType: 'chapterProcessed',
        domainId: 'ipsum',
        domainName: 'lorem',
        values: {
          settings: {
            toc: {
              add_body_to_parts: true,
            },
          },
        },
      })

    await expect(createTemplate()).rejects.toThrow()
  })
})
