const model = require('./bookSettingsTemplate')

module.exports = {
  model,
  modelName: 'BookSettingsTemplate',
}
