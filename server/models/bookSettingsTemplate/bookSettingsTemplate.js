const BaseModel = require('@pubsweet/base-model')

const {
  booleanNullable,
  id,
  string,
  stringNullable,
} = require('../_helpers/types')

class BookSettingsTemplate extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'bookSettingsTemplate'
  }

  static get tableName() {
    return 'book_settings_templates'
  }

  async $beforeInsert(queryContext) {
    await super.$beforeInsert(queryContext)
    const { collectionId } = this

    if (collectionId) {
      /* eslint-disable-next-line global-require */
      const Collection = require('../collection/collection')

      try {
        const collection = await Collection.query()
          .findById(collectionId)
          .throwIfNotFound()

        const acceptedCollectionTypes = ['bookSeries']

        if (!acceptedCollectionTypes.includes(collection.collectionType)) {
          throw new Error(
            `
              Book settings templates: Templates for colllection members can 
              only exist for collections of the following types: 
              ${acceptedCollectionTypes.join(', ')}
            `,
          )
        }
      } catch (e) {
        throw new Error(e)
      }
    }
  }

  static get schema() {
    return {
      type: 'object',
      required: [
        'organizationId',
        'templateType',
        'domainId',
        'domainName',
        'values',
      ],
      properties: {
        organizationId: id,
        collectionId: id,
        templateType: {
          enum: ['chapterProcessed', 'wholeBook'],
        },
        domainId: string,
        domainName: string,
        values: {
          type: 'object',
          required: [],
          additionalProperties: false,
          properties: {
            settings: {
              type: 'object',
              additionalProperties: false,
              properties: {
                approvalOrgAdminEditor: booleanNullable,
                approvalPreviewer: booleanNullable,
                citationType: stringNullable,
                toc: {
                  add_body_to_parts: booleanNullable,
                  group_chapters_in_parts: booleanNullable,
                  order_chapters_by: stringNullable,
                },
              },
            },
            metadata: {
              type: 'object',
              additionalProperties: false,
              properties: {
                openAccess: booleanNullable,
              },
            },
          },
        },
      },
    }
  }
}

module.exports = BookSettingsTemplate
