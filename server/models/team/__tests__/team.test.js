const { v4: uuid } = require('uuid')

const { Team } = require('@pubsweet/models')

const clearDb = require('../../_helpers/clearDb')

describe('Template', () => {
  beforeEach(() => clearDb())

  afterAll(async () => {
    await clearDb()
    const knex = Team.knex()
    knex.destroy()
  })

  test('global teams must be unique', async () => {
    await Team.query().insert({
      role: 'sysAdmin',
      global: true,
    })

    // same role, also global
    const addAnother = async () =>
      Team.query().insert({
        role: 'sysAdmin',
        global: true,
      })

    await expect(addAnother()).rejects.toThrow()
  })

  test('non-global teams must be unique per object', async () => {
    const objectId = uuid()

    const data = {
      role: 'sysAdmin',
      objectId,
      objectType: 'fake',
      global: false,
    }

    await Team.query().insert(data)

    // same role, same object, also not global
    const addAnother = async () => Team.query().insert(data)

    await expect(addAnother()).rejects.toThrow()
  })

  test('non-global teams must have object id and type', async () => {
    const objectId = uuid()

    const data = {
      role: 'sysAdmin',
      global: false,
    }

    const teamWithoutId = async () =>
      Team.query().insert({
        ...data,
        objectType: 'fake',
      })

    await expect(teamWithoutId()).rejects.toThrow()

    const teamWithoutType = async () =>
      Team.query().insert({
        ...data,
        objectId,
      })

    await expect(teamWithoutType()).rejects.toThrow()
  })

  test('global teams must not have an object', async () => {
    const objectId = uuid()

    const data = {
      role: 'sysAdmin',
      global: true,
    }

    const teamWithId = async () =>
      Team.query().insert({
        ...data,
        objectType: 'fake',
      })

    await expect(teamWithId()).rejects.toThrow()

    const teamWithType = async () =>
      Team.query().insert({
        ...data,
        objectId,
      })

    await expect(teamWithType()).rejects.toThrow()

    const teamWithBoth = async () =>
      Team.query().insert({
        ...data,
        objectId,
        objectType: 'fake',
      })

    await expect(teamWithBoth()).rejects.toThrow()
  })
})
