/* eslint-disable no-return-await */
const { Team } = require('@pubsweet/models')
const model = require('./teamMember')
const { mapResultsToInputIds } = require('../util')

module.exports = {
  models: [
    {
      model,
      modelName: 'TeamMember',
    },
    {
      model: Team,
      modelName: 'Team',
      modelLoaders: {
        forBookComponents: async bookComponentIds => {
          return await mapResultsToInputIds(
            bookComponentIds,
            'objectId',
            Team.query()
              .whereIn('objectId', bookComponentIds)
              .eager('[members.[user, alias]]'),
          )
        },
      },
    },
  ],
}
