const {
  models: [, { model: TeamMember }],
} = require('@pubsweet/model-team')

class NcbiTeamMember extends TeamMember {
  static get modifiers() {
    return {
      allButRejectedUsers(builder) {
        builder.whereNot('status', 'rejected').orderBy('userId')
      },
      onlyEnabledUsers(builder) {
        builder.where('status', 'enabled')
      },
    }
  }

  static get schema() {
    return {
      properties: {
        awardNumber: {
          type: ['string', 'null'],
        },
        awardeeAffiliation: {
          type: ['string', 'null'],
        },
      },
      type: 'object',
    }
  }
}

module.exports = NcbiTeamMember
