const { logger } = require('@coko/server')

exports.up = knex => {
  try {
    return knex.schema
      .raw(
        'CREATE UNIQUE INDEX "unique_team_id_user_id" ON "team_members" (team_id, user_id);',
      )
      .catch(err => {
        throw new Error(err)
      })
  } catch (e) {
    logger.error('Teams members: Constraints migration failed!')
    throw new Error(e)
  }
}
