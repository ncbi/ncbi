const { logger } = require('@coko/server')

exports.up = knex => {
  try {
    return Promise.all([
      knex.schema
        .raw(
          'CREATE UNIQUE INDEX "unique_global_team" ON "teams" (role) WHERE "global" = true;',
        )
        .catch(err => {
          logger.error(err.stack)
        }),

      knex.schema
        .raw(
          'CREATE UNIQUE INDEX "unique_non_global_team_per_object" ON "teams" (role, object_id) WHERE "global" = false;',
        )
        .catch(err => {
          logger.error(err.stack)
        }),

      knex.schema
        .raw(
          'ALTER TABLE "teams" ADD CONSTRAINT "global_teams_must_not_have_associated_objects_other_teams_must_have_them" CHECK ( (global = true AND object_id IS NULL AND object_type IS NULL) or (global = false AND object_id IS NOT NULL AND object_type IS NOT NULL));',
        )
        .catch(err => {
          logger.error(err.stack)
        }),
    ])
  } catch (e) {
    logger.error('Teams: Constraints migration failed!')
    throw new Error(e)
  }
}
