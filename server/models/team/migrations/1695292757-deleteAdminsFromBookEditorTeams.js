// eslint-disable-next-line func-names
exports.up = function (knex) {
  const deleteTeamMemberQuery = `
    DELETE FROM team_members WHERE team_members.id IN (
      SELECT DISTINCT tm.id
          FROM
              team_members tm
              INNER JOIN teams t ON tm.team_id = t.id
              INNER JOIN book_component on book_component.owner_id = tm.user_id
          WHERE
              t.role = 'editor'
              AND t.object_type != 'Organisation'
              AND tm.user_id IN (
                  SELECT DISTINCT tm1.user_id
                  FROM team_members tm1
                  INNER JOIN teams tm2 ON tm1.team_id = tm2.id
                  WHERE tm2.role IN ('sysAdmin', 'orgAdmin')
              )
              AND tm.user_id = book_component.owner_id
              AND t.object_id = book_component.id
      UNION
      SELECT DISTINCT tm.id
          FROM
              team_members tm
              INNER JOIN teams t ON tm.team_id = t.id
              INNER JOIN books on books.owner_id = tm.user_id
          WHERE
              t.role = 'editor'
              AND t.object_type != 'Organisation'
              AND tm.user_id IN (
                  SELECT DISTINCT tm1.user_id
                  FROM team_members tm1
                  INNER JOIN teams tm2
                  ON tm1.team_id = tm2.id
                  WHERE tm2.role IN ('sysAdmin', 'orgAdmin')
              )
              AND tm.user_id = books.owner_id
              AND t.object_id = books.id
      )
    `

  return knex.schema.raw(deleteTeamMemberQuery)
}
