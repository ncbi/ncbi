/* eslint-disable global-require */
const BaseModel = require('@pubsweet/base-model')

class Issue extends BaseModel {
  static get tableName() {
    return 'issues'
  }

  static get relationMappings() {
    const User = require('../user/user')
    const Comment = require('../comment/comment')

    return {
      comments: {
        relation: BaseModel.HasManyRelation,
        modelClass: Comment,
        join: {
          from: 'issues.id',
          to: 'comments.issueId',
        },
        user: {
          relation: BaseModel.HasOneRelation,
          modelClass: User,
          join: {
            from: 'issues.user_id',
            to: 'users.id',
          },
        },
      },
    }
  }

  static get schema() {
    return {
      properties: {
        title: { type: 'string' },
        content: { type: 'string' },
        status: { type: 'string' },
        objectId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
        files: {
          default: [],
          items: {
            format: 'uuid',
            type: 'string',
          },
          type: 'array',
        },
        userId: { type: ['string', 'null'], format: 'uuid' },
      },
    }
  }
}

module.exports = Issue
