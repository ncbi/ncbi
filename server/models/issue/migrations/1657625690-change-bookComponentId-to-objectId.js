/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-return-await */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Issue = require(`${process.cwd()}/server/models/issue/issue`)
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  await addObjectIdColumn(knex)
  await populateObjectIdFromBookComponentId(knex)
  await dropBookComponentIdColumn(knex)
}

const addObjectIdColumn = knex =>
  knex.schema.table('issues', table => {
    table.uuid('object_id').nullable()
  })

const populateObjectIdFromBookComponentId = knex => {
  const getObjectIdForIssue = async (issue, trx) => {
    const bookComponent = await BookComponent.query(trx)
      .where({ id: issue.bookComponentId })
      .first()

    const book = await Book.query(trx)
      .where({ id: bookComponent.bookId })
      .first()

    return !book.settings.chapterIndependently ? book.id : bookComponent.id
  }

  try {
    return useTransaction(async trx => {
      const issues = await Issue.query(trx)

      return Promise.all(
        issues.map(async issue => {
          const objectId = await getObjectIdForIssue(issue, trx)
          return issue.$query(trx).patchAndFetch({ objectId })
        }),
      )
    })
  } catch (err) {
    throw new Error(err)
  }
}

const dropBookComponentIdColumn = knex =>
  knex.schema.table('issues', table => {
    table.dropColumn('book_component_id')
  })
