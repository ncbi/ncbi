CREATE TABLE issues (
  id UUID PRIMARY KEY,
  title TEXT,
  content TEXT,
  status TEXT,
  files jsonb not null,
  user_id uuid NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  book_component_id uuid REFERENCES book_component(id) INITIALLY DEFERRED, 
  created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  updated TIMESTAMP WITH TIME ZONE,
  topic TEXT
);
 