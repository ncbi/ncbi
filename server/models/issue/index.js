/* eslint-disable global-require */
/* eslint-disable no-return-await */
const model = require('./issue')
const { mapResultsToInputIds } = require('../util')

module.exports = {
  ...require('./graphql'),
  models: [
    {
      modelName: 'Issue',
      model,
      modelLoaders: {
        forBookComponents: async bookComponentIds => {
          return await mapResultsToInputIds(
            bookComponentIds,
            'objectId',
            model
              .query()
              .whereIn('objectId', bookComponentIds)
              .orderBy('created'),
          )
        },
      },
    },
  ],
}
