const model = require('./collection')

module.exports = {
  model,
  modelName: 'Collection',
}
