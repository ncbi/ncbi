const Book = require('../../book/book')
const Collection = require('../collection')
const File = require('../../file/file')
const Organization = require('../../organisation/organisation')
const User = require('../../user/user')

const clearDb = require('../../_helpers/clearDb')

describe('Collection', () => {
  beforeEach(() => clearDb())

  afterAll(async () => {
    await clearDb()
    const knex = Collection.knex()
    knex.destroy()
  })

  // NOTE: this won't work unless the domain calls are disabled in the addBookToCollection method
  test('apply collection metadata to book when apply settings are true', async () => {
    const user = await User.query().insert({})

    const organization = await Organization.query().insert({
      name: 'fldsjlfds',
    })

    const file = await File.query().insert({
      name: 'my file',
    })

    const publisherLocation = 'some location'
    const publisherName = 'some publisher name'

    const copyrightStatement = 'some copyright statement'
    const licenseType = 'CC BY'
    const licenseStatement = 'some license statement'

    const collection = await Collection.query().insert({
      title: 'My collection',
      organisationId: organization.id,
      ownerId: user.id,
      fileCover: file.id,
      metadata: {
        applyCoverToBooks: true,

        pub_loc: publisherLocation,
        pub_name: publisherName,
        applyPublisherToBooks: true,

        copyrightStatement,
        openAccessLicense: true,
        licenseType,
        licenseStatement,
        applyPermissionsToBooks: true,
      },
    })

    let book = await Book.query().insert({
      organisationId: organization.id,
      title: 'My book',
    })

    await Collection.addBookToCollection({
      collectionId: collection.id,
      bookId: book.id,
    })

    book = await Book.query().findById(book.id)

    expect(book.metadata.pub_loc).toBe(publisherLocation)
    expect(book.metadata.pub_name).toBe(publisherName)

    expect(book.metadata.copyrightStatement).toBe(copyrightStatement)
    expect(book.metadata.openAccessLicense).toBe(true)
    expect(book.metadata.licenseType).toBe(licenseType)
    expect(book.metadata.licenseStatement).toBe(licenseStatement)

    // expect(book.fileCover).toBe(file.id)
  })
})
