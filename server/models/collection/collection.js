/* eslint-disable global-require */
const { Model } = require('objection')
const { without } = require('lodash')

const BaseModel = require('@pubsweet/base-model')
const { logger } = require('@coko/server')

const DomainTranslation = require('../../services/domainTranslation/domainTranslation')
const { domainRequest } = require('../../services/axiosService')

const OrderService = require('../../services/orderService/orderService')

const { model: Organisation } = require('../organisation')
const { model: File } = require('../file')

const {
  arrayOfIdsDefaultEmpty,
  boolean,
  booleanDefaultFalse,
  booleanNullable,
  id,
  idNullable,
  string,
  stringNullable,
  stringNullableDefaultNull,
} = require('../_helpers/types')

class Collection extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'collection'
  }

  static get tableName() {
    return 'collections'
  }

  static get orderMappings() {
    const { model: Book } = require('../book')

    return {
      field: {
        idColumn: 'id',
        column: 'components',
        foreignColumn: 'id',
        modelClass: [Book, Collection],
      },
      predicate: {
        modelClass: Collection,
        idColumn: 'id',
        foreignColumn: 'id',
        field: col => {
          const value = (col.settings || {}).orderBooksBy || 'manual'
          if (value === 'volume')
            return {
              field: 'metadata.volume',
              type: 'Number',
              order: 'asc',
              value: v => v || Infinity,
            }

          if (value === 'date')
            return {
              field: 'metadata.pubDate',
              type: 'Date',
              order: 'desc',
              value: ({ date, dateRange }) => {
                if (date && date.year) {
                  return `${date.year}-${parseInt(date.month, 10) || '01'}-${
                    date.day || '01'
                  }`
                }

                if (dateRange && dateRange.startYear) {
                  return `${dateRange.startYear}-${
                    parseInt(dateRange.startMonth, 10) || '01'
                  }-01`
                }

                if (dateRange && dateRange.endYear) {
                  return `${dateRange.endYear}-${
                    parseInt(dateRange.endMonth, 10) || '01'
                  }-01`
                }

                // this number is based to https://262.ecma-international.org/5.1/#sec-15.9.1.1
                // points to the max number for a date in milliseconds
                return 8640000000000000
              },
            }

          if (value === 'title')
            return {
              field: 'title',
              type: 'String',
              order: 'asc',
              value: v => {
                const title = v
                  .trim()
                  .toLowerCase()
                  .replace(/^(the\b|^a\b|^an\b)/s, '')
                  .trim()

                return title === 'Untitled' ? null : title
              },
            }

          return {
            field: value,
            type: 'String',
            order: 'asc',
          }
        },
      },
    }
  }

  static get relationMappings() {
    const Book = require('../book/book')

    return {
      books: {
        relation: Model.HasManyRelation,
        modelClass: Book,
        join: {
          from: 'collections.id',
          to: 'books.collection_id',
        },
      },
      organisation: {
        join: {
          from: 'collections.organisation_id',
          to: 'organisations.id',
        },
        modelClass: Organisation,
        relation: Model.BelongsToOneRelation,
      },
      cover: {
        join: {
          from: 'collections.file_cover',
          to: 'files.id',
        },
        modelClass: File,
        relation: Model.HasOneRelation,
      },
    }
  }

  static get schema() {
    return {
      type: 'object',
      required: ['title'],
      properties: {
        abstract: stringNullable,
        abstractTitle: stringNullable,
        alias: stringNullable,
        collectionType: {
          enum: ['bookSeries', 'funded'],
          type: 'string',
        },
        components: arrayOfIdsDefaultEmpty,
        domain: stringNullable,

        domainId: stringNullable,
        fileCover: idNullable,
        metadata: {
          additionalProperties: false,
          default: {
            applyCoverToBooks: false,
            applyPermissionsToBooks: false,
            applyPublisherToBooks: false,
            author: [],
            chapterProcessedSourceType: null,
            copyrightHolder: null,
            copyrightStatement: null,
            copyrightYear: null,
            editor: [],
            grants: [],
            issn: null,
            eissn: null,
            licenseStatement: null,
            licenseType: null,
            nlmId: null,
            notes: [],
            openAccess: false,
            openAccessLicense: false,
            otherLicenseType: null,
            pubDate: {
              date: {
                day: null,
                month: null,
                year: null,
              },
              dateRange: {
                startYear: null,
                startMonth: null,
                endYear: null,
                endMonth: null,
              },
              dateType: null,
              publicationFormat: null,
            },
            pub_loc: null,
            pub_name: null,
            publisherBookSeriesTitle: null,
            sourceType: null,
            wholeBookSourceType: null,
          },
          properties: {
            applyCoverToBooks: booleanDefaultFalse,
            applyPermissionsToBooks: booleanDefaultFalse,
            applyPublisherToBooks: booleanDefaultFalse,
            author: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                additionalProperties: false,
                properties: {
                  givenName: stringNullable,
                },
              },
            },
            chapterProcessedSourceType: stringNullableDefaultNull,
            copyrightHolder: stringNullableDefaultNull,
            copyrightStatement: stringNullableDefaultNull,
            copyrightYear: stringNullableDefaultNull,
            editor: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                additionalProperties: false,
                properties: {
                  givenName: stringNullable,
                  surname: stringNullable,
                  role: stringNullable,
                  degrees: stringNullable,
                },
              },
            },
            grants: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                additionalProperties: false,
                required: [
                  'id',
                  'country',
                  'institution_acronym',
                  'institution_code',
                  'institution_name',
                  'number',
                ],
                properties: {
                  id,
                  apply: booleanDefaultFalse,
                  country: string,
                  institution_acronym: string,
                  institution_code: string,
                  institution_name: string,
                  number: string,
                },
              },
            },
            issn: stringNullableDefaultNull,
            eissn: stringNullableDefaultNull,
            licenseStatement: stringNullableDefaultNull,
            licenseType: stringNullableDefaultNull,
            nlmId: stringNullableDefaultNull,
            notes: {
              type: 'array',
              default: [],
              items: {
                type: 'object',
                additionalProperties: false,
                required: ['type', 'description'],
                properties: {
                  type: string,
                  title: stringNullable,
                  description: string,
                  applyNotePdf: booleanNullable,
                },
              },
            },
            openAccess: booleanDefaultFalse,
            openAccessLicense: booleanDefaultFalse,
            otherLicenseType: stringNullableDefaultNull,
            pub_loc: stringNullableDefaultNull,
            pub_name: stringNullableDefaultNull,
            pubDate: {
              type: 'object',
              default: {
                date: {
                  day: null,
                  month: null,
                  year: null,
                },
                dateRange: {
                  startYear: null,
                  startMonth: null,
                  endYear: null,
                  endMonth: null,
                },
                dateType: null,
                publicationFormat: null,
              },
              properties: {
                date: {
                  type: 'object',
                  additionalProperties: false,
                  default: {
                    day: null,
                    month: null,
                    year: null,
                  },
                  properties: {
                    day: stringNullableDefaultNull,
                    month: stringNullableDefaultNull,
                    year: stringNullableDefaultNull,
                  },
                },
                dateRange: {
                  type: 'object',
                  additionalProperties: false,
                  default: {
                    startYear: null,
                    startMonth: null,
                    endYear: null,
                    endMonth: null,
                  },
                  properties: {
                    startYear: stringNullableDefaultNull,
                    startMonth: stringNullableDefaultNull,
                    endYear: stringNullableDefaultNull,
                    endMonth: stringNullableDefaultNull,
                  },
                },
                dateType: {
                  ...stringNullable,
                  default: null,
                  enum: ['pub', 'pubr', null],
                },
                publicationFormat: {
                  ...stringNullable,
                  default: null,
                  enum: ['electronic', 'print', null],
                },
              },
            },
            publisherBookSeriesTitle: stringNullableDefaultNull,
            sourceType: stringNullableDefaultNull,
            wholeBookSourceType: stringNullableDefaultNull,
          },
          type: 'object',
        },
        organisationId: idNullable,
        ownerId: idNullable,
        parentId: idNullable,
        settings: {
          additionalProperties: false,
          default: {
            approvalOrgAdminEditor: false,
            citationSelfUrl: null,
            groupBooks: false,
            groupBooksBy: null,
            isCollectionGroup: false,
            orderBooksBy: null,
            publisherUrl: null,
            toc: {
              contributors: false,
              fullBookCitations: false,
              publisher: false,
            },
            UKPMC: false,
          },
          properties: {
            approvalOrgAdminEditor: booleanDefaultFalse,
            citationSelfUrl: stringNullable,
            groupBooks: booleanDefaultFalse,
            groupBooksBy: stringNullable,
            isCollectionGroup: boolean,
            orderBooksBy: stringNullable,
            publisherUrl: stringNullable,
            toc: {
              additionalProperties: false,
              default: {
                contributors: false,
                fullBookCitations: false,
                publisher: false,
              },
              properties: {
                contributors: booleanDefaultFalse,
                fullBookCitations: booleanDefaultFalse,
                publisher: booleanDefaultFalse,
              },
              type: ['object'],
            },
            UKPMC: booleanDefaultFalse,
          },
          type: ['object'],
        },
        status: {
          default: 'new-collection',
          enum: [
            'new-collection',
            'new-book',
            'submission-errors',
            'new-upload',
            'in-production',
            'converting',
            'tagging',
            'failed',
            'conversion-errors',
            'loading-preview',
            'loading-errors',
            'preview',
            'in-review',
            'approve',
            'published',
            'publishing',
            'publish-failed',
            'new-version',
          ],
          type: 'string',
        },
        title: stringNullable,
        workflow: stringNullable,
      },
    }
  }

  getOrganisation() {
    return this.$query().joinEager('organisation')
  }

  getBooks(trx) {
    return this.$query(trx).joinEager('books')
  }

  static Teams() {
    return {
      author: {
        name: 'Author',
      },

      editor: {
        name: 'Editor',
      },

      previewer: {
        name: 'Previewer',
      },
    }
  }

  async updateCollectionStatus(transaction) {
    const { books } = await this.getBooks(transaction)

    let collectionStatus = null

    if (books.every(x => x.status === books[0].status)) {
      collectionStatus = books[0].status
    } else {
      collectionStatus = 'in-production'
    }

    if (collectionStatus !== this.status) {
      await this.$query(transaction).patch({
        status: collectionStatus,
      })

      logger.info(
        `Status of Collection id: ${this.id}, ${this.alias}, ${this.domain} is updated to ${collectionStatus}`,
      )
    }
  }

  static async addBookToCollection({ collectionId, bookId }, transaction) {
    const Book = require('../book/book')

    const book = await Book.query(transaction)
      .findById(bookId)
      .throwIfNotFound()

    const currentCollection = await Collection.query(transaction).findOne({
      id: collectionId,
    })

    if (currentCollection.components.includes(bookId)) return currentCollection

    const { data: bookTemplateResponse } = await domainRequest(
      `domains/${book.domainId}`,
      'GET',
    )

    if (
      !bookTemplateResponse.meta ||
      bookTemplateResponse.meta.status !== 'ok'
    ) {
      logger.error(bookTemplateResponse.body)
      throw new Error('Get domain details request failed')
    }

    const bookTemplate = bookTemplateResponse.body

    const bookCollectionNamePatch = DomainTranslation.createBookCollectionNameUpdateObject(
      bookTemplate,
      currentCollection.domain,
    )

    const { data: updatedData } = await domainRequest(
      `domains/${book.domainId}/`,
      'PATCH',
      bookCollectionNamePatch,
    )

    if (updatedData.meta && updatedData.meta.status !== 'ok') {
      logger.error(updatedData.body)
      throw new Error('Update domain request failed')
    }

    currentCollection.components.unshift(bookId)

    const col = await Collection.query(transaction)
      .patch({
        components: currentCollection.components,
      })
      .findById(collectionId)
      .returning('*')

    await OrderService.model(col, {
      transaction,
      exclude: itm => itm.settings.isCollectionGroup,
      deep: true,
    })

    await currentCollection.updateCollectionStatus(transaction)

    // Update book settings
    const currentCollectionSettings = currentCollection.settings
    const { approvalOrgAdminEditor } = currentCollectionSettings

    const currentBookSettings = book.settings
    const currentBookMetadata = book.metadata

    // these are the settings where the collection overrides the book settings
    // if you edit this, make sure to also edit the update collection command
    // (if the overriding value changes in the collection, it should change in
    // all the collection books)
    const bookPatchData = {
      settings: {
        ...currentBookSettings,
      },
      metadata: {
        ...currentBookMetadata,
      },
    }

    const isWholeBook = !currentBookSettings.chapterIndependently

    if (isWholeBook) {
      bookPatchData.settings.approvalOrgAdminEditor = approvalOrgAdminEditor
    }

    await Book.query(transaction).findById(bookId).patch(bookPatchData)

    return currentCollection
  }

  static async removeBookFromCollection({ collectionId, bookId }, transaction) {
    try {
      const Book = require('../book/book')

      const book = await Book.query(transaction)
        .findById(bookId)
        .throwIfNotFound()

      // update book-collection-name in domain service
      const { data: bookTemplateResponse } = await domainRequest(
        `domains/${book.domainId}`,
        'GET',
      )

      if (
        !bookTemplateResponse.meta ||
        bookTemplateResponse.meta.status !== 'ok'
      ) {
        logger.error(bookTemplateResponse.body)
        throw new Error('Get domain details request failed')
      }

      const bookTemplate = bookTemplateResponse.body

      const bookCollectionNamePatch = DomainTranslation.createBookCollectionNameUpdateObject(
        bookTemplate,
        'NONE',
      )

      const { data: updatedData } = await domainRequest(
        `domains/${book.domainId}/`,
        'PATCH',
        bookCollectionNamePatch,
      )

      if (updatedData.meta && updatedData.meta.status !== 'ok') {
        logger.error(updatedData.body)
        throw new Error('Update domain request failed')
      }

      // update collection components
      const currentCollection = await Collection.query(transaction).findOne({
        id: collectionId,
      })

      const newComponents = without(currentCollection.components, bookId)

      await Collection.query(transaction).findById(collectionId).patch({
        components: newComponents,
      })

      const containingCollections = await Collection.query(transaction).whereIn(
        'id',
        currentCollection.components,
      )

      await Promise.all(
        containingCollections.map(async collect => {
          const thisComponents = without(collect.components, bookId)

          await Collection.query(transaction).findById(collect.id).patch({
            components: thisComponents,
          })
        }),
      )
    } catch (err) {
      throw new Error(err)
    }
  }
}

module.exports = Collection
