/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')
const { has } = require('lodash')

// Paths are relative to the generated migrations folder
const Collection = require(`${process.cwd()}/server/models/collection/collection`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const collections = await Collection.query(trx)

      return Promise.all(
        collections.map(collection => {
          const { metadata } = collection

          const newPubDate = metadata.pubDate

          if (has(metadata, 'pubDate.day')) {
            newPubDate.date = metadata.pubDate.day
            delete newPubDate.day
          }

          const patchData = {
            metadata: {
              ...metadata,
              pubDate: newPubDate,
            },
          }

          return collection.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
