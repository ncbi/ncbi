/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')
const { has, get } = require('lodash')

// Paths are relative to the generated migrations folder
const Collection = require(`${process.cwd()}/server/models/collection/collection`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const collections = await Collection.query(trx)

      const fixBookSourceTypeCase = value => {
        if (!value) return null
        return value.charAt(0).toUpperCase() + value.slice(1)
      }

      return Promise.all(
        collections.map(collection => {
          const { metadata } = collection
          let wholeBookSourceType = null
          let chapterProcessedSourceType = null

          const updatedMetadata = {}

          if (has(metadata, 'wholeBookSourceType')) {
            wholeBookSourceType = get(metadata, 'wholeBookSourceType')

            if (wholeBookSourceType === '') {
              updatedMetadata.wholeBookSourceType = 'Book'
            } else {
              updatedMetadata.wholeBookSourceType = fixBookSourceTypeCase(
                wholeBookSourceType,
              )
            }
          }

          if (has(metadata, 'chapterProcessedSourceType')) {
            chapterProcessedSourceType = get(
              metadata,
              'chapterProcessedSourceType',
            )

            if (chapterProcessedSourceType === '') {
              updatedMetadata.chapterProcessedSourceType = 'Book'
            } else {
              updatedMetadata.chapterProcessedSourceType = fixBookSourceTypeCase(
                chapterProcessedSourceType,
              )
            }
          }

          /**
           * Patch the settings
           */

          return collection.$query(trx).patch({
            metadata: {
              ...metadata,
              ...updatedMetadata,
            },
          })
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
