/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Collection = require(`${process.cwd()}/server/models/collection/collection`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const collections = await Collection.query(trx)

      return Promise.all(
        collections.map(collection => {
          const { metadata } = collection
          const modifiedMetadata = { ...metadata }

          if (metadata && metadata.notes && metadata.notes.length) {
            modifiedMetadata.notes = metadata.notes.map(note => {
              const { type, ...rest } = note
              let newType = type

              if (type === 'author-notes') {
                newType = 'author-note'
              } else if (['editorial-notes', 'none'].includes(type)) {
                newType = 'generic'
              }

              return {
                type: newType,
                ...rest,
              }
            })
          }

          const patchData = {
            metadata: modifiedMetadata,
          }

          return collection.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
