/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')
const { has } = require('lodash')

// Paths are relative to the generated migrations folder
const Collection = require(`${process.cwd()}/server/models/collection/collection`)
const Book = require(`${process.cwd()}/server/models/book/book`)
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const collections = await Collection.query(trx)
      const books = await Book.query(trx)
      const bookComponents = await BookComponent.query(trx)

      const combined = [...collections, ...books, ...bookComponents]

      return Promise.all(
        combined.map(entity => {
          const { metadata } = entity
          const modifiedMetadata = { ...metadata }

          if (!has(metadata, 'grants')) {
            modifiedMetadata.grants = []
          }

          const patchData = {
            metadata: modifiedMetadata,
          }

          return entity.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
