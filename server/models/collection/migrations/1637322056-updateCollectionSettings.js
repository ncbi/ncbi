/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')
const { has, isNil, set } = require('lodash')

// Paths are relative to the generated migrations folder
const Collection = require(`${process.cwd()}/server/models/collection/collection`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const collections = await Collection.query(trx)

      return Promise.all(
        collections.map(collection => {
          const { collectionType, settings } = collection
          const settingsPatch = { ...settings }

          // make collections that don't have a type book series
          let newCollectionType = 'bookSeries'

          if (!has(settings, 'approvalOrgAdminEditor')) {
            settingsPatch.approvalOrgAdminEditor = false
          }

          if (!has(settings, 'citationSelfUrl')) {
            settingsPatch.citationSelfUrl = null
          }

          if (!has(settings, 'publisherUrl')) {
            settingsPatch.publisherUrl = null
          }

          if (!has(settings, 'UKPMC')) {
            settingsPatch.UKPMC = false
          }

          if (!has(settings, 'toc.contibutors')) {
            set(settingsPatch, 'toc.contributors', false)
          }

          if (!has(settings, 'toc.fullBookCitations')) {
            set(settingsPatch, 'toc.fullBookCitations', false)
          }

          if (!has(settings, 'toc.publisher')) {
            set(settingsPatch, 'toc.publisher', false)
          }

          if (collectionType === 'collection') {
            settingsPatch.isCollectionGroup = false
          } else if (collectionType === 'customGroup') {
            settingsPatch.isCollectionGroup = true
          }

          if (has(settings, 'collectionType')) {
            newCollectionType = settings.collectionType
            delete settingsPatch.collectionType
          }

          if (has(settings, 'author_manuscript')) {
            delete settingsPatch.author_manuscript
          }

          if (has(settings, 'prepublication_draft')) {
            delete settingsPatch.prepublication_draft
          }

          if (has(settings, 'published_pdf')) {
            delete settingsPatch.published_pdf
          }

          if (has(settings, 'group_books') && !isNil(settings.group_books)) {
            settingsPatch.groupBooks = settings.group_books
            delete settingsPatch.group_books
          } else {
            settingsPatch.groupBooks = false
          }

          if (has(settings, 'group_books_by')) {
            settingsPatch.groupBooksBy = settings.group_books_by
            delete settingsPatch.group_books_by
          } else {
            settingsPatch.groupBooksBy = null
          }

          if (has(settings, 'order_books_by')) {
            settingsPatch.orderBooksBy = settings.order_books_by
            delete settingsPatch.order_books_by
          } else {
            settingsPatch.orderBooksBy = null
          }

          const patchData = {
            collectionType: newCollectionType,
            settings: settingsPatch,
          }

          /**
           * Patch the settings
           */

          return collection.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
