/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')
const { has, isBoolean, merge, mergeWith } = require('lodash')

// Paths are relative to the generated migrations folder
const Collection = require(`${process.cwd()}/server/models/collection/collection`)

exports.up = async knex => {
  try {
    const modelMetadata = Collection.schema.properties.metadata
    const defaultMetadata = modelMetadata.default
    const defaultPubDate = defaultMetadata.pubDate

    const fieldsToDelete = [
      'catalogId',
      'ccLicenseType',
      'custom',
      'pub_id',
      'bookSeriesId',
      'bookSeriesTitle',
      'publicationType',
    ]

    return useTransaction(async trx => {
      const collections = await Collection.query(trx)

      return Promise.all(
        collections.map(collection => {
          const { metadata } = collection
          const modifiedMetadata = { ...metadata }

          fieldsToDelete.forEach(field => {
            if (has(metadata, field)) {
              delete modifiedMetadata[field]
            }
          })

          if (has(metadata, 'pub_date')) {
            if (metadata.pub_date) {
              const splitValues = metadata.pub_date.split('-')

              if (splitValues.length === 0 || splitValues.length > 2) {
                throw new Error(
                  'Existing pub_date value invalid:',
                  metadata.pub_date,
                )
              }

              const [startYear, endYear] = splitValues

              const newDate = {
                dateRange: {
                  startYear,
                },
                dateType: 'pubr',
                publicationFormat: 'electronic',
              }

              if (endYear) newDate.dateRange.endYear = endYear

              modifiedMetadata.pubDate = merge({}, defaultPubDate, newDate)
            }

            delete modifiedMetadata.pub_date
          }

          if (has(metadata, 'licenceType')) {
            modifiedMetadata.licenseType = metadata.licenceType
            delete modifiedMetadata.licenceType
          }

          if (has(metadata, 'licenceStatement')) {
            modifiedMetadata.licenseStatement = metadata.licenceStatement
            delete modifiedMetadata.licenceStatement
          }

          if (has(metadata, 'otherLicenceType')) {
            modifiedMetadata.otherLicenseType = metadata.otherLicenceType
            delete modifiedMetadata.otherLicenceType
          }

          const newMetadata = mergeWith(
            {},
            defaultMetadata,
            modifiedMetadata,
            /* eslint-disable-next-line consistent-return */
            (originalValue, newValue, key) => {
              // don't let non-boolean values (eg. null) override boolean properties
              if (isBoolean(defaultMetadata[key] && !isBoolean(newValue))) {
                return originalValue
              }

              return newValue
            },
          )

          const patchData = {
            metadata: newMetadata,
          }

          return collection.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
