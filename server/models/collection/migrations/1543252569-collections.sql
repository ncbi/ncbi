CREATE SEQUENCE IF NOT EXISTS universal_bcms_id;

create table collections (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,
  
  -- foreign
  owner_id uuid not null references users,
  organisation_id uuid not null references organisations,
  file_cover uuid references files,

  -- own
  status text default 'new-book',
  workflow text,
  title text,
  domain text,
  abstract text,
 abstract_title text,
  parent_id uuid,
  books jsonb not null,
  metadata jsonb,
  settings jsonb,
  alias integer NOT NULL DEFAULT nextval('universal_bcms_id')
);
