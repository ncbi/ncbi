/* eslint-disable import/no-dynamic-require */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Collection = require(`${process.cwd()}/server/models/collection/collection`)

exports.up = async knex => {
  try {
    return useTransaction(async trx => {
      const collections = await Collection.query(trx)

      return Promise.all(
        collections.map(collection => {
          const { metadata } = collection

          const patchData = {
            metadata: {
              ...metadata,
              sourceType: 'Collection',
            },
          }

          return collection.$query(trx).patch(patchData)
        }),
      )
    })
  } catch (error) {
    throw new Error(error)
  }
}
