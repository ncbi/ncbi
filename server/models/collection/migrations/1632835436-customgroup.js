exports.up = async knex =>
  Promise.all([
    knex.schema.table('collections', table => {
      table.text('collection_type').defaultTo('collection')
      table.renameColumn('books', 'components')
    }),
  ])
