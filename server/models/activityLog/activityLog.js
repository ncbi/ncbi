const BaseModel = require('@pubsweet/base-model')

class ActivityLog extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'activityLog'
  }

  static get tableName() {
    return 'activity_log'
  }

  static get schema() {
    return {
      properties: {
        action: {
          minLength: 1,
          type: 'string',
        },
        description: {
          minLength: 1,
          type: 'string',
        },
        data: {
          minLength: 1,
          type: 'string',
        },
        objectId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
        tableName: {
          minLength: 1,
          type: 'string',
        },
        userId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
      },
      required: ['action'],
      type: 'object',
    }
  }
}

module.exports = ActivityLog
