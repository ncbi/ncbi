create table activity_log (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,
  
  -- foreign
  user_id uuid references users,

  -- own
  object_id uuid,
  table_name text,
  action text,
  description text,
  data text
);
