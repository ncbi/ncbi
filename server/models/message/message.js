/* eslint-disable global-require */
const BaseModel = require('@pubsweet/base-model')

class Message extends BaseModel {
  static get tableName() {
    return 'messages'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        channelId: { type: 'string' },
        content: { type: 'string' },
        files: {
          default: [],
          items: {
            format: 'uuid',
            type: 'string',
          },
          type: 'array',
        },
        type: {
          default: 'comment',
          type: 'string',
        },
        userId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    const User = require('../user/user')
    const Channel = require('../channel/channel')

    return {
      channel: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: Channel,
        join: {
          from: 'messages.channelId',
          to: 'channels.id',
        },
      },
      user: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'messages.userId',
          to: 'users.id',
        },
      },
    }
  }
}

module.exports = Message
