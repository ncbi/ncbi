/* eslint-disable global-require */
const BaseModel = require('@pubsweet/base-model')

class Comment extends BaseModel {
  static get tableName() {
    return 'comments'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        issueId: { type: 'string' },
        content: { type: 'string' },
        files: {
          default: [],
          items: {
            format: 'uuid',
            type: 'string',
          },
          type: 'array',
        },
        type: {
          default: 'comment',
          type: 'string',
        },
        userId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    const Channel = require('../channel/channel')
    const User = require('../user/user')

    return {
      channel: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: Channel,
        join: {
          from: 'comments.issueId',
          to: 'issues.id',
        },
      },
      user: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'comments.userId',
          to: 'users.id',
        },
      },
    }
  }
}

module.exports = Comment
