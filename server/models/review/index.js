/* eslint-disable global-require */
const model = require('./review')

module.exports = {
  model,
  modelName: 'Review',
}
