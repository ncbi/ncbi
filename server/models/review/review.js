/* eslint-disable global-require */
const BaseModel = require('@pubsweet/base-model')

class Review extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'review'
  }

  static get tableName() {
    return 'reviews'
  }

  static get schema() {
    return {
      properties: {
        objectId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
        decision: {
          enum: [null, 'requestChanges', 'approve'],
          type: ['string', 'null'],
        },
        messageId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
        userId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
      },
      required: ['objectId', 'messageId', 'userId'],
      type: 'object',
    }
  }
}

module.exports = Review
