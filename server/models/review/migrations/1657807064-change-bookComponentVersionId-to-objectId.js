/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-return-await */
const { useTransaction } = require('@coko/server')

// Paths are relative to the generated migrations folder
const Review = require(`${process.cwd()}/server/models/review/review`)
const BookComponent = require(`${process.cwd()}/server/models/bookComponent/bookComponent`)
const Book = require(`${process.cwd()}/server/models/book/book`)

exports.up = async knex => {
  await addObjectIdColumn(knex)
  await populateObjectIdFromBookComponentId(knex)
  await dropBookComponentIdColumn(knex)
}

const addObjectIdColumn = knex =>
  knex.schema.table('reviews', table => {
    table.uuid('object_id').nullable()
  })

const populateObjectIdFromBookComponentId = knex => {
  const getObjectIdForReview = async (review, trx) => {
    const bookComponent = await BookComponent.query(trx)
      .where({ bookComponentVersionId: review.bookComponentVersionId })
      .first()

    const book = await Book.query(trx)
      .where({ id: bookComponent.bookId })
      .first()

    return !book.settings.chapterIndependently ? book.id : bookComponent.id
  }

  try {
    return useTransaction(async trx => {
      const reviews = await Review.query(trx)

      return Promise.all(
        reviews.map(async review => {
          const objectId = await getObjectIdForReview(review, trx)
          return review.$query(trx).patchAndFetch({ objectId })
        }),
      )
    })
  } catch (err) {
    throw new Error(err)
  }
}

const dropBookComponentIdColumn = knex =>
  knex.schema.table('reviews', table => {
    table.dropColumn('book_component_version_id')
  })
