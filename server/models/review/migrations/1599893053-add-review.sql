
create table reviews (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,

  -- foreign
  book_component_version_id uuid REFERENCES book_component(id) INITIALLY DEFERRED,
  user_id uuid NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  message_id uuid NOT NULL REFERENCES messages(id),
  -- own
  decision text default null
);
