/* eslint-disable global-require */
const BaseModel = require('@pubsweet/base-model')

class SourceConvertedFile extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'sourceConvertedFiles'
  }

  static get tableName() {
    return 'source_converted_files'
  }

  static get schema() {
    return {
      properties: {
        sourceId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
        convertedId: {
          format: 'uuid',
          type: ['string', 'null'],
        },
      },
      required: ['sourceId', 'convertedId'],
      type: 'object',
    }
  }
}

module.exports = SourceConvertedFile
