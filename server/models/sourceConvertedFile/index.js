/* eslint-disable global-require */
const model = require('./sourceConvertedFile')

module.exports = {
  model,
  modelName: 'SourceConvertedFile',
}
