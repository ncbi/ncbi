create table source_converted_files (
  -- base
  id uuid primary key,
  type text not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,
  -- own

    -- foreign
  source_id uuid references book_component_files,
  converted_id uuid references book_component_files
);