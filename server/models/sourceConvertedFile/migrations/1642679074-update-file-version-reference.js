exports.up = async knex =>
  Promise.all([
    knex.schema.alterTable('source_converted_files', t => {
      t.uuid('source_id').references('id').inTable('file_versions').alter()
      t.uuid('converted_id').references('id').inTable('file_versions').alter()
    }),
  ])
