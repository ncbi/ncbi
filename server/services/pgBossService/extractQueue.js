/* eslint-disable class-methods-use-this */
const {
  logger: { getRawLogger },
} = require('@coko/server')

const omit = require('lodash/omit')

const logger = getRawLogger()

const ExtractFactory = require('../ExtractFiles/ExtractFactory')

class ExtractQueue {
  constructor(boss) {
    this.boss = boss
  }

  static instance

  static name() {
    return 'extract-queue'
  }

  /**
   * data = {
   *     objectId, Related objectId
   *     userId, The user who submitted
   * }
   */
  async handle({ data }) {
    logger.info('Start Handling job extract-queue')

    const { files, pushToQueueData, pushToQueue } = data

    const { notification } = pushToQueueData
    let extractedFiles = {}

    try {
      const extractFiles = await ExtractFactory.create(
        notification,
        files[0].local,
      )

      if (extractFiles) {
        await extractFiles.start()
        extractedFiles = await extractFiles.transformFiles(notification.id)
      }
    } catch (e) {
      logger.error(e)
    }

    const forwardData = omit(pushToQueueData, ['notification'])

    if (pushToQueue) {
      this.boss.send(pushToQueue, { ...forwardData, extractedFiles })
    }
  }

  static getInstance(boss) {
    if (ExtractQueue.instance) {
      return ExtractQueue.instance
    }

    ExtractQueue.instance = new ExtractQueue(boss)
    return ExtractQueue.instance
  }
}

module.exports = ExtractQueue
