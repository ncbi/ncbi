const {
  logger: { getRawLogger },
} = require('@coko/server')

const { useTransaction } = require('@coko/server')

const { pubsubManager } = require('pubsweet-server')

const ContentProccess = require('../ContentProccess')

const { getPubsub } = pubsubManager

const OBJECT_UPDATED = 'OBJECT_UPDATED'
/* eslint-disable class-methods-use-this */

const logger = getRawLogger()

class SubmitQueueJob {
  constructor(boss) {
    this.boss = boss
  }

  static instance

  static name() {
    return 'submit-queue'
  }

  /**
   * data = {
   *     objectId, Related objectId
   *     userId, The user who submitted
   * }
   */
  async handle({ data }) {
    logger.info('Start Handling job submit-queue')
    const pubsub = await getPubsub()

    let book = null
    let contentProccess = null

    await useTransaction(async trx => {
      contentProccess = await ContentProccess.findBestContent(data.objectId)

      const Command = contentProccess.command

      const content = new Command({
        id: data.objectId,
        transaction: trx,
        currentUser: data.userId,
      })

      await content.executeCommand('submitComponent')
    })

    book = await contentProccess.getBook()

    if (contentProccess.object.type === 'bookComponent') {
      pubsub.publish(`${OBJECT_UPDATED}.${data.objectId}`, {
        objectUpdated: {
          ...contentProccess.object,
        },
      })

      pubsub.publish(`${OBJECT_UPDATED}.${book.id}`, {
        objectUpdated: {
          ...book,
        },
      })
    } else if (contentProccess.object === 'book') {
      pubsub.publish(`${OBJECT_UPDATED}.${book.id}`, {
        objectUpdated: {
          ...book,
        },
      })
    }
  }

  static getInstance(boss) {
    if (SubmitQueueJob.instance) {
      return SubmitQueueJob.instance
    }

    SubmitQueueJob.instance = new SubmitQueueJob(boss)
    return SubmitQueueJob.instance
  }
}

module.exports = SubmitQueueJob
