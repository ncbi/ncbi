const {
  logger: { getRawLogger },
} = require('@coko/server')

const PgBoss = require('pg-boss')

const logger = getRawLogger()

const FtpQueueJob = require('./ftpQueueJob')
const SubmitQueueJob = require('./submitQueueJob')
const ExtractQueue = require('./extractQueue')
const HandleMessageQueue = require('./handleMessageQueue')

class PgBossService {
  static instance
  // eslint-disable-next-line class-methods-use-this
  async start() {
    const connectionString = `postgres://${process.env.POSTGRES_USER}:${process.env.POSTGRES_PASSWORD}@${process.env.POSTGRES_HOST}:${process.env.POSTGRES_PORT}/${process.env.POSTGRES_DB}`
    this.boss = new PgBoss(connectionString)
    this.boss.on('error', error => console.error(error))
    // eslint-disable-next-line no-return-await
    return await this.boss.start()
  }

  async startServerQueue() {
    await this.boss.work(FtpQueueJob.name(), async data => {
      const ftpQueue = FtpQueueJob.getInstance(this.boss)
      await ftpQueue.handle(data)
    })

    logger.info(`${FtpQueueJob.name()} queue is subscribed`)

    await this.boss.work(SubmitQueueJob.name(), async data => {
      const submitQueue = SubmitQueueJob.getInstance(this.boss)
      await submitQueue.handle(data)
    })

    logger.info(`${SubmitQueueJob.name()} queue is subscribed`)

    await this.boss.work(ExtractQueue.name(), async data => {
      const extractQueue = ExtractQueue.getInstance(this.boss)
      await extractQueue.handle(data)
    })

    logger.info(`${ExtractQueue.name()} queue is subscribed`)

    await this.boss.work(HandleMessageQueue.name(), async data => {
      const handleMessageQueue = HandleMessageQueue.getInstance(this.boss)
      await handleMessageQueue.handle(data)
    })

    logger.info(`${HandleMessageQueue.name()} queue is subscribed`)
  }

  static async getInstance() {
    if (PgBossService.instance) {
      return PgBossService.instance
    }

    const bossService = new PgBossService()

    PgBossService.instance = await bossService.start()
    await bossService.startServerQueue()
    return PgBossService.instance
  }
}

module.exports = PgBossService
