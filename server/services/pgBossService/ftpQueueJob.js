const fs = require('fs')
const { NcbiNotificationMessage, Errors } = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const ContentProccess = require('../ContentProccess')

/* eslint-disable class-methods-use-this */

const logger = getRawLogger()

const NcbiService = require('../ncbiConnectionManager/ncbiService')

class FtpQueueJob {
  constructor(boss) {
    this.boss = boss
    this.service = null
  }

  static instance

  static name() {
    return 'ftp-queue'
  }

  /**
   * data = {
   *     isApex: false|true
   *     action: upload|download
   *     objectId, Related objectId
   *     pushToQueue, TriggerAnotherPush on successfull Transfer
   *     userId, the user that triggered the job
   *     notificationId, Notification to update that the file has been sent
   *     files: [{local: '', remote: ''}]
   * }
   */
  async handle({ data }) {
    logger.info('Start Handling job ftp-queue')

    const {
      objectId,
      notificationId,
      action,
      pushToQueue,
      pushToQueueData,
    } = data

    this.service = NcbiService.loadFtpAdapter(data.isApex)
    await this.service.connection()

    if (action === 'download') {
      await this.download(data.files)
    } else if (action === 'upload') {
      await this.upload(data.files)

      await Errors.query().patch({ history: true }).where({ objectId })

      /** We have normalize only the BookComponent Errors
       *  For that we will use ContentProccess to check the type of the object
       *  and if it is bookComponent then we will update the ErrorSummary
       * */
      const contentProccess = await ContentProccess.findBestContent(objectId)

      if (contentProccess.type === 'chapterProccessed') {
        await contentProccess.model.updateErrorSummary(objectId)
      }

      await NcbiNotificationMessage.query()
        .patch({ objectId, uploaded: true })
        .findOne({ id: notificationId })
    }

    await this.service.close()

    if (pushToQueue) {
      this.boss.send(pushToQueue, { ...pushToQueueData, files: data.files })
    }
  }

  async download(files) {
    await this.service.receive(files[0].local, files[0].remote)
  }

  async upload(files) {
    await this.service.send(files[0].local, files[0].remote)
    await this.service.send(files[1].local, files[1].remote)

    if (fs.existsSync(files[0].local)) {
      await fs.unlinkSync(files[0].local)
    }

    if (fs.existsSync(files[1].local)) {
      await fs.unlinkSync(files[1].local)
    }

    return true
  }

  static getInstance(boss) {
    if (FtpQueueJob.instance) {
      return FtpQueueJob.instance
    }

    FtpQueueJob.instance = new FtpQueueJob(boss)
    return FtpQueueJob.instance
  }
}

module.exports = FtpQueueJob
