/* eslint-disable global-require */
/* eslint-disable class-methods-use-this */
const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const { ActivityLog } = require('@pubsweet/models')

class HandleMessageQueue {
  constructor(boss) {
    this.boss = boss
  }

  static instance

  static name() {
    return 'handle-message-queue'
  }

  /**
   * data = {
   *     message, Incomming message from Kafka
   *     topic, topic of kafka
   *     savedActivityId, saved Activity Id
   * }
   */
  // eslint-disable-next-line consistent-return
  async handle({ data }) {
    const HandleNcbiMessage = require('../handleNcbiMessage/handleNcbiMessage')
    const HandleMessageFactory = require('../handleNcbiMessage/HandleMessageFactory')
    const NcbiParse = require('../handleNcbiMessage/ncbiParse')

    logger.info('Start Handling job handle-message-queue')

    const { topic, message, savedActivityId, extractedFiles } = data

    try {
      const ncbiParse = new NcbiParse(topic)

      const notification = await ncbiParse.parseMessage(message)

      const handleMessage = new HandleNcbiMessage(ncbiParse)

      const entity = await HandleMessageFactory.create(handleMessage)

      const validNotification = entity.validateMessage
        ? entity.validateMessage()
        : handleMessage.validateMessage()

      if (notification && validNotification) {
        await ActivityLog.query()
          .patch({
            objectId: notification.id,
          })
          .findOne({
            id: savedActivityId,
          })

        try {
          entity.setFiles(extractedFiles || {})
          await entity.handleMessage()
          await handleMessage.messageProcessed()
        } catch (e) {
          logger.error(` HandleMessage Error ${e}`)
          return false
        }

        await entity.triggerEvent()
      }
    } catch (e) {
      logger.error(e)
    }
  }

  static getInstance(boss) {
    if (HandleMessageQueue.instance) {
      return HandleMessageQueue.instance
    }

    HandleMessageQueue.instance = new HandleMessageQueue(boss)
    return HandleMessageQueue.instance
  }
}

module.exports = HandleMessageQueue
