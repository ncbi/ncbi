const FtpAdapter = require('./ftp')

class ApexAdapter extends FtpAdapter {
  constructor() {
    super()
    this.config = {}

    if (process.env.APEX_FTP_HOST) {
      this.config.host = process.env.APEX_FTP_HOST

      if (process.env.APEX_FTP_PASSWORD) {
        this.config.password = process.env.APEX_FTP_PASSWORD
      }

      if (process.env.APEX_FTP_USER) {
        this.config.user = process.env.APEX_FTP_USER
      }
    }
  }
}
module.exports = ApexAdapter
