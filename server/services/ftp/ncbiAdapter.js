const FtpAdapter = require('./ftp')

class NcbiAdapter extends FtpAdapter {
  constructor() {
    super()
    this.config = {}

    this.config.host = process.env.NCBI_FTP_HOST

    if (process.env.NCBI_FTP_PASSWORD) {
      this.config.password = process.env.NCBI_FTP_PASSWORD
    }

    if (process.env.NCBI_FTP_USER) {
      this.config.user = process.env.NCBI_FTP_USER
    }
  }
}
module.exports = NcbiAdapter
