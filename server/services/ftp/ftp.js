/* eslint-disable no-return-await */
const ftp = require('basic-ftp')
const logger = require('@pubsweet/logger')

class Ftp {
  async connection() {
    this.client = new ftp.Client(300000)
    this.client.ftp.verbose = true

    try {
      await this.client.access(this.config)
      logger.info('FTP connected')
    } catch (error) {
      logger.error(error)
      throw error
    }

    return this.client
  }

  async send(localPath, remotePath) {
    try {
      return await this.client.uploadFrom(localPath, remotePath)
    } catch (error) {
      logger.error(error)
      throw error
    }
  }

  async receive(localPath, remotePath) {
    try {
      logger.info(`Download file ${remotePath}`)
      return await this.client.downloadTo(localPath, remotePath)
    } catch (error) {
      logger.error(error)
      throw error
    }
  }

  close() {
    if (this.client) this.client.close()
    this.client = null
    logger.info(`Connection has been Closed`)
  }

  get isClosed() {
    if (this.client) return this.client.closed
    return true
  }
}
module.exports = Ftp
