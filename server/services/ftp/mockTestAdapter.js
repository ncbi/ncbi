/* eslint-disable class-methods-use-this */
/* eslint-disable no-return-await */

const FtpAdapter = require('./ftp')

class MockTest extends FtpAdapter {
  async connection() {
    return true
  }

  async send() {
    return true
  }

  async receive() {
    return true
  }
}

module.exports = MockTest
