/* eslint-disable no-return-await */
/* eslint-disable no-plusplus */
/* eslint-disable no-cond-assign */

const { Channel, Team, User } = require('@pubsweet/models')

const { flatten, uniqBy } = require('lodash')

class ParseMessage {
  constructor(message) {
    this.message = message
    this.users = []
    this.parseMentions()
  }

  async getData() {
    const channel = await Channel.query().findById(this.message.channelId)

    await this.getUsers()
    await this.getTeams()

    this.users = uniqBy(this.users, 'id')

    return {
      channel,
      id: this.message.id,
      type: this.message.type,
      users: this.users,
    }
  }

  parseMentions() {
    // Get Users
    const removeNewLine = /(\r\n|\n|\r)/gm
    const regexMentions = /(?:^|[ ])@\[[0-9a-zA-Z ]+\]\(([0-9a-z-]+)\)/gi

    const content = this.message.content.replace(removeNewLine, '')
    const mentions = content.match(regexMentions)

    this.mentionsIds = (mentions || []).map(
      mention => mention.match(/\(([^)]+)\)/)[1],
    )
  }

  async getUsers() {
    this.users = this.users.concat(
      flatten(
        await Promise.all(
          this.mentionsIds.map(
            async mention => await User.query().findOne({ id: mention }),
          ),
        ),
      ).filter(user => user !== undefined),
    )
  }

  async getTeams() {
    this.users = this.users.concat(
      flatten(
        (
          await Promise.all(
            this.mentionsIds.map(
              async mention =>
                await Team.query()
                  .findOne({ id: mention })
                  .eager('[members.[user, alias]]'),
            ),
          )
        )
          .filter(team => team !== undefined)
          .map(team => team.members.map(teamMember => teamMember.user)),
      ),
    )
  }
}

module.exports = ParseMessage
