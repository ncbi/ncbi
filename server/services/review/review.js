/* eslint-disable consistent-return */
/* eslint-disable no-return-await */

const pick = require('lodash/pick')
const flatten = require('lodash/flatten')
const { Team } = require('@pubsweet/models')

const { model: Review } = require('../../models/review')
const ContentProccess = require('../ContentProccess')

class ReviewService {
  constructor(message = {}) {
    this.message = message

    this.contentProcess = ContentProccess.findBestContent(
      message.channel.objectId,
    )

    this.type = message.type
  }

  async userHasReview(userId) {
    const usersReview = await this.loadReview()
    return usersReview.find(review => review.userId === userId)
  }

  async loadReview() {
    const usersReview = await Review.query().where({
      objectId: this.message.channel.objectId,
    })

    return usersReview || []
  }

  async getPreviewers() {
    let bookComponentTeamMembers = []
    let bookTeamMembers = []

    if (this.contentProcess.object.type === 'bookComponent') {
      bookComponentTeamMembers = await Team.query()
        .where({
          objectId: this.contentProcess.object.id,
          objectType: 'bookComponent',
          role: 'previewer',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')

      bookTeamMembers = await Team.query()
        .where({
          objectId: this.contentProcess.object.bookId,
          objectType: 'book',
          role: 'previewer',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')
    } else if (this.contentProcess.object.type === 'book') {
      bookTeamMembers = await Team.query()
        .where({
          objectId: this.contentProcess.object.id,
          objectType: 'book',
          role: 'previewer',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')
    }

    const previewerUsers = flatten(
      (bookComponentTeamMembers || [])
        .concat(bookTeamMembers || [])
        .map(
          team =>
            team &&
            team.members &&
            team.members.map(teamMember => teamMember.user),
        ),
    )

    return previewerUsers
  }

  async userIsAuthor(userId) {
    let bookComponentAuthors = []
    let bookAuthors = []

    if (this.contentProcess.object.type === 'bookComponent') {
      bookComponentAuthors = await Team.query()
        .findOne({
          objectId: this.contentProcess.object.id,
          objectType: 'bookComponent',
          role: 'author',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')

      bookAuthors = await Team.query()
        .findOne({
          objectId: this.contentProcess.object.bookId,
          objectType: 'book',
          role: 'author',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')
    } else if (this.contentProcess.object.type === 'book') {
      bookAuthors = await Team.query()
        .findOne({
          objectId: this.contentProcess.object.id,
          objectType: 'book',
          role: 'author',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')
    }

    const editorsUsers = flatten(
      (bookComponentAuthors ? [bookComponentAuthors] : [])
        .concat([bookAuthors])
        .map(
          team =>
            team &&
            team.members &&
            team.members.map(teamMember => teamMember.user),
        ),
    )

    const foundUser = editorsUsers.find(user => user && user.id === userId)

    return !!foundUser
  }

  async userIsPreviewer(userId) {
    let bookComponentPreviewers = []
    let bookPreviewers = []

    if (this.contentProcess.object.type === 'bookComponent') {
      bookComponentPreviewers = await Team.query()
        .findOne({
          objectId: this.contentProcess.object.id,
          objectType: 'bookComponent',
          role: 'previewer',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')

      bookPreviewers = await Team.query()
        .findOne({
          objectId: this.contentProcess.object.bookId,
          objectType: 'book',
          role: 'previewer',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')
    } else if (this.contentProcess.object.type === 'book') {
      bookPreviewers = await Team.query()
        .findOne({
          objectId: this.contentProcess.object.id,
          objectType: 'book',
          role: 'previewer',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')
    }

    const previewerUsers = flatten(
      (bookComponentPreviewers ? [bookComponentPreviewers] : [])
        .concat(bookPreviewers)
        .map(
          team =>
            team &&
            team.members &&
            team.members.map(teamMember => teamMember.user),
        ),
    )

    const foundUser = previewerUsers.find(user => user && user.id === userId)

    return !!foundUser
  }

  async userIsEditor(userId) {
    let bookEditors = []
    let bookComponentEditors = []

    if (this.contentProcess.object.type === 'bookComponent') {
      bookEditors = await Team.query()
        .findOne({
          objectId: this.contentProcess.object.bookId,
          objectType: 'book',
          role: 'editor',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')

      bookComponentEditors = await Team.query()
        .findOne({
          objectId: this.contentProcess.object.id,
          objectType: 'bookComponent',
          role: 'editor',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')
    } else if (this.contentProcess.object.type === 'book') {
      bookEditors = await Team.query()
        .findOne({
          objectId: this.contentProcess.object.id,
          objectType: 'book',
          role: 'editor',
        })
        .eager('[members(onlyEnabledUsers).[user, alias]]')
    }

    const editorsUsers = flatten(
      (bookComponentEditors ? [bookComponentEditors] : [])
        .concat(bookEditors ? [bookEditors] : [])
        .map(
          team =>
            team &&
            team.members &&
            team.members.map(teamMember => teamMember.user),
        ),
    )

    const foundUser = editorsUsers.find(user => user && user.id === userId)

    return !!foundUser
  }

  async validate(userId) {
    this.contentProcess = await this.contentProcess

    if (
      this.userIsPreviewer(userId) &&
      (this.type === 'requestChanges' || this.type === 'approve')
    ) {
      return true
    }

    if (
      (this.userIsAuthor(userId) || this.userIsEditor(userId)) &&
      this.type === 'in-review'
    ) {
      return true
    }

    return false
  }

  async doReview(userId) {
    if (!(await this.validate())) return false
    const previewerUsers = await this.getPreviewers()

    if (this.type === 'in-review') {
      Promise.all(
        previewerUsers.map(async user => {
          if (this.object.status === 'approve') {
            return await Review.query()
              .patch({
                messageId: this.message.id,
                decision: null,
              })
              .findOne({
                objectId: this.message.channel.objectId,
                userId: user.id,
              })
          }

          if (!(await this.userHasReview(user.id))) {
            const reviewCreated = new Review({
              objectId: this.message.channel.objectId,
              messageId: this.message.id,
              userId: user.id,
            })

            return await reviewCreated.save()
          }
        }),
      )
    } else if (this.type === 'requestChanges' || this.type === 'approve') {
      const query = {
        objectId: this.message.channel.objectId,
        userId,
      }

      if (this.type === 'requestChanges') {
        query.decision = null
      }

      await Review.query().findOne(query).patch({
        decision: this.type,
      })
    }
  }

  static async checkResponses(usersReview) {
    let decision = 'in-review'

    decision =
      usersReview.length > 0 &&
      usersReview.every(review => review.decision === 'approve')
        ? 'approve'
        : decision

    decision = usersReview.some(review => review.decision === 'requestChanges')
      ? 'requestChanges'
      : decision

    return {
      decision,
      usersReview: usersReview.map(userReview =>
        pick(userReview, ['id', 'decision', 'userId']),
      ),
    }
  }
}

module.exports = ReviewService
