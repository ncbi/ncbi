/* eslint-disable no-param-reassign */
/* eslint-disable no-return-await */
const { Division, BookComponent } = require('@pubsweet/models')

const OrderService = require('../orderService/orderService')

class DivisionService {
  constructor(data, transaction) {
    this.baseModel = new Division(data)
    this.transaction = transaction
  }

  get model() {
    return this.baseModel
  }

  async save(trx) {
    return await this.baseModel.save(trx)
  }

  // eslint-disable-next-line class-methods-use-this
  async findAndRemoveFromDivision(bookComponent) {
    const divisions = await Division.query(this.transaction).where({
      bookId: this.baseModel.bookId,
    })

    const divisionBookComponents = divisions.map(division => {
      division.bookComponents = division.bookComponents.filter(
        bc => !bookComponent.map(bcv => bcv.id).includes(bc),
      )

      return { id: division.id, bookComponents: division.bookComponents }
    })

    await Promise.all(
      divisionBookComponents.map(async division => {
        if (division.id === this.baseModel.id) {
          this.baseModel.bookComponents = division.bookComponents
        }

        return await Division.query(this.transaction)
          .patch({
            bookComponents: division.bookComponents,
          })
          .findOne({ id: division.id })
      }),
    )

    const bookComponents = await BookComponent.query(this.transaction).where({
      bookId: this.baseModel.bookId,
      parentId: null,
    })

    const bookComponentsUpdate = bookComponents
      .map(bc => {
        const total = bc.bookComponents.length

        bc.bookComponents = bc.bookComponents.filter(
          bcf => !bookComponent.map(bcv => bcv.id).includes(bcf),
        )

        if (total === bc.bookComponents.length) return null

        return { id: bc.id, bookComponents: bc.bookComponents }
      })
      .filter(b => b != null)

    await Promise.all(
      bookComponentsUpdate.map(async bcU => {
        return await BookComponent.query(this.transaction)
          .patch({ bookComponents: bcU.bookComponents })
          .findOne({ id: bcU.id })
      }),
    )
  }

  async addBookComponents(bookComponent) {
    await this.findAndRemoveFromDivision(bookComponent)
    const baseModelComponents = this.baseModel.bookComponents
    baseModelComponents.unshift(bookComponent[0].id)

    await Division.query(this.transaction)
      .where('id', this.baseModel.id)
      .patch({ bookComponents: baseModelComponents })

    await BookComponent.query(this.transaction)
      .patch({ divisionId: this.baseModel.id })
      .whereIn(
        'id',
        bookComponent.map(bcv => bcv.id),
      )

    this.baseModel.bookComponents = baseModelComponents

    await OrderService.model(this.baseModel, {
      transaction: this.transaction,
      exclude: itm => itm.componentType === 'part',
      deep: true,
    })
  }

  static async getDivision(data, transaction) {
    const division = await Division.query(transaction).findOne(data)

    const divisionService = new DivisionService(division, transaction)

    return divisionService
  }
}

module.exports = DivisionService
