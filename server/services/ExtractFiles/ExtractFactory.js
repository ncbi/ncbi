const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const Submitted = require('./Submitted')
const WordConverted = require('./WordConverted')
const PdfConverted = require('./PdfConverted')

class ExtractFactory {
  static create(notification, extractFile) {
    const isXml = notification.topic.includes('xml')
    const isWord = notification.topic.includes('word')
    const isPdf = notification.topic === 'submit_apex_package_receipt'
    const isSubmit = notification.topic === 'submit_package_receipt'

    if (isXml || isWord) {
      return new WordConverted(notification, extractFile)
    }

    if (isPdf) {
      return new PdfConverted(notification, extractFile)
    }

    if (isSubmit) {
      return new Submitted(notification, extractFile)
    }

    logger.info(`kafka No Extraction needed`)
    return false
  }
}

module.exports = ExtractFactory
