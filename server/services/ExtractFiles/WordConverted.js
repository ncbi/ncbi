/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-return-await */
const ExtractFiles = require('./ExtractFiles')

class WordConverted extends ExtractFiles {
  get packageSchema() {
    return ['convertedFile', 'support', 'image', 'supplement']
  }

  async convertedFile(files) {
    const fileNames = Object.keys(files)

    const filename = fileNames.find(
      file =>
        (file.includes('.xml') || file.includes('.bxml')) &&
        !file.includes('eXtyles'),
    )

    const id = files[filename]
    delete files[filename]

    return await this.UpdateCategory(id, 'converted')
  }
}

module.exports = WordConverted
