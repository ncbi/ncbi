/* eslint-disable no-await-in-loop */
/* eslint-disable no-param-reassign */
/* eslint-disable no-return-await */
const ExtractFiles = require('./ExtractFiles')
const FileService = require('../file/fileService')

class Submitted extends ExtractFiles {
  // eslint-disable-next-line class-methods-use-this
  get packageSchema() {
    // Support should be the last category because it gets all other files that didnot make it to a category
    return ['source', 'image', 'supplement', 'pdf', 'cover', 'support']
  }

  // eslint-disable-next-line class-methods-use-this
  async readFileById(id) {
    const content = await FileService.getSourceToBuffer(id, this.transaction)

    return content
      .toString()
      .replace(/(\r\n|\n|\r)/gm, '\n')
      .split('\n')
  }

  async source(files) {
    const fileNames = Object.keys(files)

    // Make sure we dont match on things like __MACOSX/._manifest.txt
    const fname = fileNames.find(
      file => file.match(/^manifest[.]txt$/) !== null,
    )

    const lines = await this.readFileById(files[fname])

    const fileToSave = []

    // eslint-disable-next-line no-restricted-syntax
    for (const line of lines) {
      let category = line.split('\t')[0]
      let filename = line.split('\t')[1]
      if (category) category = category.trim()
      if (filename) filename = filename.trim()

      if (
        category === 'chapter' ||
        category === 'book' ||
        category === 'fm' ||
        category === 'part' ||
        category === 'addendum' ||
        category === 'appendix' ||
        category === 'main_xml'
      ) {
        if (files[filename]) {
          fileToSave.push({
            id: files[filename],
            fileType: category,
            tag: category === 'main_xml' ? 'main_xml' : null,
          })

          delete files[filename]
        }
      }
    }

    // Set main_xml tag if there is no main_xml at manifest
    if (fileToSave.every(f => f.tag === null)) {
      const objIndex = fileToSave.findIndex(f => f.fileType === 'book')

      if (objIndex >= 0) {
        fileToSave[objIndex].tag = 'main_xml'
      }
    }

    return await Promise.all(
      fileToSave.map(async filename => {
        const file = await this.UpdateCategory(filename.id, 'source')
        return { ...file, tag: filename.tag }
      }),
    )
  }

  async cover(files) {
    const fileNames = Object.keys(files)

    // Make sure we dont match on things like __MACOSX/._manifest.txt
    const fname = fileNames.find(
      file => file.match(/^manifest[.]txt$/) !== null,
    )

    const lines = await this.readFileById(files[fname])

    const fileToSave = []

    // eslint-disable-next-line no-restricted-syntax
    for (const line of lines) {
      let category = line.split('\t')[0]
      let filename = line.split('\t')[1]
      if (category) category = category.trim()
      if (filename) filename = filename.trim()

      if (category === 'cover' && files[filename]) {
        fileToSave.push({
          id: files[filename],
          tag: null,
        })

        delete files[filename]
      }
    }

    return await Promise.all(
      fileToSave.map(async filename => {
        const file = await this.UpdateCategory(filename.id, 'cover')
        return { ...file, tag: filename.tag }
      }),
    )
  }

  async pdf(files) {
    const fileNames = Object.keys(files)

    // Make sure we dont match on things like __MACOSX/._manifest.txt
    const fname = fileNames.find(
      file => file.match(/^manifest[.]txt$/) !== null,
    )

    const lines = await this.readFileById(files[fname])

    const fileToSave = []

    // eslint-disable-next-line no-restricted-syntax
    for (const line of lines) {
      let category = line.split('\t')[0]
      let filename = line.split('\t')[1]
      if (category) category = category.trim()
      if (filename) filename = filename.trim()

      if (category === 'chapter_pdf' || category === 'book_pdf') {
        if (files[filename]) {
          fileToSave.push({
            id: files[filename],
            tag: category === 'book_pdf' ? 'book_pdf' : null,
          })

          delete files[filename]
        }
      }
    }

    return await Promise.all(
      fileToSave.map(async filename => {
        const file = await this.UpdateCategory(filename.id, 'pdf')
        return { ...file, tag: filename.tag }
      }),
    )
  }

  async supplement(files) {
    const fileNames = Object.keys(files)

    // Make sure we dont match on things like __MACOSX/._manifest.txt
    const fname = fileNames.find(
      file => file.match(/^manifest[.]txt$/) !== null,
    )

    const lines = await this.readFileById(files[fname])

    const fileToSave = []

    // eslint-disable-next-line no-restricted-syntax
    for (const line of lines) {
      let category = line.split('\t')[0]
      let filename = line.split('\t')[1]
      if (category) category = category.trim()
      if (filename) filename = filename.trim()

      if (category === 'supplement' && files[filename]) {
        fileToSave.push({ id: files[filename] })
        delete files[filename]
      }
    }

    return await Promise.all(
      fileToSave.map(
        async filename => await this.UpdateCategory(filename.id, 'supplement'),
      ),
    )
  }

  async image(files) {
    const fileNames = Object.keys(files)

    // Make sure we dont match on things like __MACOSX/._manifest.txt
    const fname = fileNames.find(
      file => file.match(/^manifest[.]txt$/) !== null,
    )

    const lines = await this.readFileById(files[fname])

    const fileToSave = []

    // eslint-disable-next-line no-restricted-syntax
    for (const line of lines) {
      let category = line.split('\t')[0]
      let filename = line.split('\t')[1]
      if (category) category = category.trim()
      if (filename) filename = filename.trim()

      if (category === 'image') {
        if (files[filename]) {
          fileToSave.push({ id: files[filename] })
          delete files[filename]
        }
      }
    }

    return await Promise.all(
      fileToSave.map(
        async filename => await this.UpdateCategory(filename.id, 'image'),
      ),
    )
  }
}

module.exports = Submitted
