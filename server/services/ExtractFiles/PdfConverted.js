/* eslint-disable no-param-reassign */
/* eslint-disable no-return-await */
const ExtractFiles = require('./ExtractFiles')

class PdfConverted extends ExtractFiles {
  // eslint-disable-next-line class-methods-use-this
  get packageSchema() {
    return ['support', 'image', 'supplement', 'pdf', 'convertedFile', 'cover']
  }

  async convertedFile(files) {
    const fileNames = Object.keys(files)

    const filteredFiles = fileNames.filter(
      file => file.match(/^converted\/*/) !== null,
    )

    return await this.UpdateCategory(files[filteredFiles[0]], 'converted')
  }

  async support(files) {
    const fileNames = Object.keys(files)

    const filteredFiles = fileNames.filter(
      file =>
        file.match(/^support\/*/) !== null &&
        file !== 'support/vendor-meta.xml',
    )

    return await Promise.all(
      filteredFiles.map(async file => {
        return await this.UpdateCategory(files[file], 'support')
      }),
    )
  }

  async cover(files) {
    const fileNames = Object.keys(files)

    const filteredFiles = fileNames.filter(
      file =>
        file.toLowerCase().startsWith('__macosx') !== true &&
        file.match(/^metadata\/*/) !== null &&
        (file.toLowerCase().endsWith('.jpg') ||
          file.toLowerCase().endsWith('.jpeg') ||
          file.toLowerCase().endsWith('.png') ||
          file.toLowerCase().endsWith('.gif') ||
          file.toLowerCase().endsWith('.tif') ||
          file.toLowerCase().endsWith('.tiff')),
    )

    return await Promise.all(
      filteredFiles.map(async file => {
        return await this.UpdateCategory(files[file], 'cover')
      }),
    )
  }

  async image(files) {
    const fileNames = Object.keys(files)

    const filteredFiles = fileNames.filter(
      file => file.match(/^images\/*/) !== null,
    )

    return await Promise.all(
      filteredFiles.map(async file => {
        return await this.UpdateCategory(files[file], 'image')
      }),
    )
  }

  async pdf(files) {
    const fileNames = Object.keys(files)

    const filteredFiles = fileNames.filter(
      file => file.match(/^display-pdfs\/*/) !== null,
    )

    return await Promise.all(
      filteredFiles.map(async file => {
        return await this.UpdateCategory(files[file], 'pdf')
      }),
    )
  }

  async supplement(files) {
    const fileNames = Object.keys(files)

    const filteredFiles = fileNames.filter(
      file => file.match(/^suppl\/*/) !== null,
    )

    return await Promise.all(
      filteredFiles.map(async file => {
        return await this.UpdateCategory(files[file], 'supplement')
      }),
    )
  }
}

module.exports = PdfConverted
