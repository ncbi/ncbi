/* eslint-disable no-await-in-loop */
/* eslint-disable no-param-reassign */
/* eslint-disable no-return-await */
/* eslint-disable no-restricted-syntax */

const Seven = require('node-7z')
const { File } = require('@pubsweet/models')
const path = require('path')
const fs = require('fs')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const FileService = require('../file/fileService')

const logger = getRawLogger()

class ExtractFiles {
  constructor(notification, extractFile) {
    this.files = {}
    this.filesCategory = {}

    this.notification = notification

    this.transaction = null
    this.extractFile = extractFile
  }

  async start() {
    logger.info(`kafka Start extract files`)
    const filename = this.extractFile

    return new Promise((resolve, reject) => {
      this.files[this.notification.id] = {}
      const outputZipFolder = filename.replace(/\.[^/.]+$/, '')

      const myStream = Seven.extractFull(filename, outputZipFolder, {
        $progress: true,
      })

      myStream.on('error', err => reject(err))

      myStream.on('end', async () => {
        try {
          const filenames = this.readDir(outputZipFolder)

          filenames.forEach(file => {
            const fileKey = file.substring(
              `${outputZipFolder}/`.length,
              file.length,
            )

            this.files[this.notification.id][fileKey] = FileService.write({
              filename: path.basename(file),
              stream: fs.createReadStream(file),
              transaction: this.transaction,
            })
          })

          await this.categorizeFiles(this.notification.id)
          await this.cleanExtractedFiles([filename, outputZipFolder])
          resolve(true)
        } catch (e) {
          reject(e)
        }
      })
    })
  }

  readDir(dir) {
    let files = []

    fs.readdirSync(dir).forEach(file => {
      const Absolute = path.join(dir, file)

      if (fs.statSync(Absolute).isDirectory()) {
        files = files.concat(this.readDir(Absolute))
      } else {
        files.push(Absolute)
      }
    })

    return files
  }

  // eslint-disable-next-line class-methods-use-this
  async cleanExtractedFiles(paths) {
    // eslint-disable-next-line no-plusplus
    for (let i = 0, len = paths; i < len; i++) {
      if (fs.existsSync(paths[i])) {
        if (fs.lstatSync(paths[i]).isDirectory()) {
          fs.rmSync(paths[i], { recursive: true, force: true })
        } else if (fs.lstatSync(paths[i]).isFile()) {
          fs.unlinkSync(paths[i])
        }
      }
    }
  }

  async categorizeFiles(id) {
    this.filesCategory[id] = {}

    const files = []

    const tempFiles = {}

    for (const [key, value] of Object.entries(this.files[id])) {
      // eslint-disable-next-line no-await-in-loop
      tempFiles[key] = (await value).id
    }

    // eslint-disable-next-line no-plusplus
    for (let i = 0, len = this.packageSchema.length; i < len; i++) {
      files.push({
        // eslint-disable-next-line no-await-in-loop
        [this.packageSchema[i]]: await this[this.packageSchema[i]](tempFiles),
      })
    }

    files.forEach((f, index) => {
      const keys = Object.keys(f)
      this.filesCategory[id][keys[0]] = files[index][keys[0]]
    })
  }

  async UpdateCategory(id, category) {
    if (!id) {
      logger.error(
        `There is no file in the package that matches this category: ${category} `,
      )

      return null
    }

    return await File.query(this.transaction)
      .patch({
        category,
      })
      .findOne({ id })
      .returning('*')
  }

  async image(files) {
    const fileNames = Object.keys(files)

    // Make sure we dont match on things like __MACOSX/._manifest.txt
    const filenames = fileNames.filter(
      file =>
        file.toLowerCase().startsWith('__macosx') !== true &&
        (file.toLowerCase().endsWith('.jpg') ||
          file.toLowerCase().endsWith('.jpeg') ||
          file.toLowerCase().endsWith('.png') ||
          file.toLowerCase().endsWith('.gif') ||
          file.toLowerCase().endsWith('.tif') ||
          file.toLowerCase().endsWith('.tiff')),
    )

    const fileStream = {}

    filenames.forEach(async file => {
      fileStream[file] = files[file]
      delete files[file]
    })

    return await Promise.all(
      filenames.map(
        async filename =>
          await this.UpdateCategory(fileStream[filename], 'image'),
      ),
    )
  }

  async support(files) {
    const fileNames = Object.keys(files)

    const filenames = fileNames.filter(
      file =>
        file.toLowerCase().startsWith('__macosx') !== true &&
        (file.includes('eXtyles') ||
          file.includes('meta.xml') ||
          file.includes('manifest.txt')),
    )

    const fileStream = {}

    filenames.forEach(file => {
      fileStream[file] = files[file]
      delete files[file]
    })

    return await Promise.all(
      filenames.map(
        async filename =>
          await this.UpdateCategory(fileStream[filename], 'support'),
      ),
    )
  }

  async supplement(files) {
    const filenames = Object.keys(files)

    return await Promise.all(
      filenames.map(
        async filename =>
          await this.UpdateCategory(files[filename], 'supplement'),
      ),
    )
  }

  /* eslint-disable class-methods-use-this */
  getLineBreakChar(string) {
    const indexOfLF = string.indexOf('\n', 1) // No need to check first-character

    if (indexOfLF === -1) {
      if (string.indexOf('\r') !== -1) return '\r'

      return '\n'
    }

    if (string[indexOfLF - 1] === '\r') return '\r\n'

    return '\n'
  }

  async transformFiles(id) {
    let files = []

    if (this.filesCategory[id].convertedFile) {
      files.push({
        ...this.filesCategory[id].convertedFile,
        bookComponentVersionId: null,
        category: 'converted',
      })
    }

    files = files.concat(
      (this.filesCategory[id].source || []).map(source => ({
        ...source,
        bookComponentVersionId: null,
        category: 'source',
      })),
    )

    files = files.concat(
      (this.filesCategory[id].pdf || []).map(pdf => ({
        ...pdf,
        bookComponentVersionId: null,
        category: 'pdf',
      })),
    )

    files = files.concat(
      (this.filesCategory[id].support || []).map(support => ({
        ...support,
        bookComponentVersionId: null,
        category: 'support',
      })),
    )

    files = files.concat(
      (this.filesCategory[id].image || []).map(image => ({
        ...image,
        bookComponentVersionId: null,
        category: 'image',
      })),
    )

    files = files.concat(
      (this.filesCategory[id].cover || []).map(cover => ({
        ...cover,
        bookComponentVersionId: null,
        category: 'cover',
      })),
    )

    files = files.concat(
      (this.filesCategory[id].supplement || []).map(supplement => ({
        ...supplement,
        bookComponentVersionId: null,
        category: 'supplement',
      })),
    )

    return files
  }
}

module.exports = ExtractFiles
