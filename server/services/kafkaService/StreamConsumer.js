/* eslint-disable no-await-in-loop */
/* eslint-disable no-plusplus */
/* eslint-disable class-methods-use-this */

const {
  logger: { getRawLogger },
  // useTransaction,
} = require('@coko/server')

const { ActivityLog } = require('@pubsweet/models')

const PgBossService = require('../pgBossService/pgBossService')

const NcbiParse = require('../handleNcbiMessage/ncbiParse')

// const notify = require('../notify')
let instance = null
const logger = getRawLogger()
class StreamConsumer {
  constructor(kafkaRest) {
    this.consumerInstance = {}
    this.stream = {}
    this.consumerGroup = null

    this.kafkaRest = kafkaRest

    if (!instance) {
      instance = this
    }

    instance.consumerGroup = `console-consumer-${process.env.KAFKA_CONSUMER_GROUP}`

    return instance
  }

  subscribeConsumer(topic) {
    const promise = new Promise((resolve, reject) => {
      this.kafkaRest
        .consumer(`${this.consumerGroup}-${topic}`)
        .join(
          {
            id: `bcms-${topic}`,
            format: 'binary',
            'auto.offset.reset': 'earliest',
            'auto.commit.enable': 'true',
          },
          // eslint-disable-next-line consistent-return
          async (err, consumerInstance) => {
            if (err) {
              logger.error(
                `kafka Failed to create instance in consumer group : ${err}`,
              )

              reject(err)
            }

            if (consumerInstance) {
              this.consumerInstance[topic] = consumerInstance

              consumerInstance
                .subscription({
                  topics: [topic],
                })
                .then(stream => {
                  logger.info(
                    `kafka Connected ${this.consumerGroup} with topic: ${topic}`,
                  )

                  this.stream[topic] = stream
                  resolve(stream)
                })
                .catch(error => reject(error))
            }
          },
        )
        .catch(error => {
          logger.error(`caught error on consumer.join ${error}`)
          reject(error)
        })
    })

    return promise
  }

  clear(args) {
    return new Promise(resolve => {
      let clearCounts = 0

      for (let i = 0; i < args.length; i++) {
        const topic = args && args[i]

        logger.info(`kafka clear stream topics ${topic}`)

        if (!this.consumerInstance || !this.consumerInstance[topic]) {
          logger.info(`kafka: No ${topic} in consumer instance. Skipping.`)
          resolve(false)
        }

        this.stream[topic] = null

        // eslint-disable-next-line no-loop-func
        this.consumerInstance[topic].shutdown(() => {
          clearCounts += 1

          if (clearCounts === args.length) {
            resolve('resolved from callback')
          }
        })
      }

      setTimeout(() => {
        resolve('resolved from timeout')
      }, 10000)
    })
  }

  logShutdown(err) {
    if (err) {
      logger.error(`Error while shutting down: ${err}`)
    } else {
      logger.info(`kafka Shutdown cleanly.`)
    }
  }

  setupStreamToEntity(topic, restart) {
    this.stream[topic].on('data', async msgs => {
      for (let i = 0; i < msgs.length; i++) {
        try {
          const data = (msgs[i].value || '').toString('utf8')
          logger.info(`Incoming msg from ncbi topic: ${topic} data: ${data}`)

          const activitylog = new ActivityLog({
            data,
            action: `incoming_${topic}`,
            tableName: 'ncbi_notification_messages',
          })

          const savedActivity = await activitylog.save()

          const ncbiParse = new NcbiParse(topic)

          const notification = await ncbiParse.parseMessage(data)

          const filesToExtract = ncbiParse.messageFile()

          const boss = await PgBossService.getInstance()

          if (filesToExtract) {
            await boss.send('ftp-queue', {
              isApex: false,
              action: 'download',
              files: [
                {
                  local: filesToExtract.localFile,
                  remote: filesToExtract.remoteFile,
                },
              ],
              pushToQueue: 'extract-queue',
              pushToQueueData: {
                pushToQueue: 'handle-message-queue',
                pushToQueueData: {
                  topic,
                  savedActivityId: savedActivity.id,
                  notification,
                  message: data,
                },
              },
            })
          } else {
            await boss.send('handle-message-queue', {
              topic,
              savedActivityId: savedActivity.id,
              message: data,
            })
          }
        } catch (e) {
          logger.error(e)
        }
      }

      return false
    })

    // eslint-disable-next-line handle-callback-err
    this.stream[topic].on('error', async err => {
      logger.error(`There was an error at the stream ${err}`)
      await restart()
    })
  }
}

module.exports = StreamConsumer
