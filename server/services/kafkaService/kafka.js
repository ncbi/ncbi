const {
  logger: { getRawLogger },
} = require('@coko/server')

const KafkaRest = require('kafka-rest')
const StreamConsumer = require('./StreamConsumer')

const REST_PROXY_KAFKA_URL = process.env.KAFKA_URL
const RESTART_MILLISECONDS = 80000

const logger = getRawLogger()

let restartTimes = 0

const make = async args => {
  let restarting = false

  logger.info(
    `start Connecting to Kafka ${restartTimes} times with args ${JSON.stringify(
      args,
    )}`,
  )

  let streamConsumer = null

  try {
    const kafka = new KafkaRest({ url: REST_PROXY_KAFKA_URL, version: 2 })
    streamConsumer = new StreamConsumer(kafka)
  } catch (e) {
    // eslint-disable-next-line promise/param-names
    await new Promise(r => setTimeout(r, RESTART_MILLISECONDS))
    await make(args)
  }

  const restart = async () => {
    if (!restarting) {
      restartTimes += 1
      restarting = true

      logger.info(
        `kafka Retry to connect to kafka waiting for ${
          RESTART_MILLISECONDS / 1000
        } sec `,
      )

      // eslint-disable-next-line promise/param-names
      await new Promise(r => setTimeout(r, RESTART_MILLISECONDS))

      const returnStream = await streamConsumer.clear(args)

      logger.info(`returned from Clearing stream ${returnStream}`)
      logger.info(`kafka Start retrying to connect after waiting time`)
      await make(args)
    }

    return false
  }

  try {
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < args.length; i++) {
      logger.info(`kafka Subscribe topic ${args[i]}`)

      if (!streamConsumer.stream[args[i]]) {
        // eslint-disable-next-line no-await-in-loop
        try {
          // eslint-disable-next-line no-await-in-loop
          await streamConsumer.subscribeConsumer(args[i])

          logger.info(
            `kafka setup entity to topic ${args[i]}-${streamConsumer.consumerGroup}`,
          )

          streamConsumer.setupStreamToEntity(args[i], restart)
        } catch (error) {
          logger.error(`kafka Please wait more time an error occurred ${error}`)
          // eslint-disable-next-line no-await-in-loop
          await restart()
          // dont keep running this loop after we return from restart!
          // [ since we are already running the loop inside the new restart call ]
          break
        }
      } else {
        logger.info('There is an active stream already')
      }
    }
  } catch (error) {
    logger.error(`Error: ${error} during try catch`)
    await restart()
  }
}

const makeOptions = [
  'convert_word_book_chapter_receipt',
  'ingest_book_chapter_receipt',
  'ingest_book_wholebook_receipt',
  'submit_package_receipt',
  'convert_xml_book_chapter_receipt',
  'convert_xml_book_receipt',
  'submit_apex_package_receipt',
  'cover_receipt',
]

const connect = async () => {
  await make(makeOptions)
}

module.exports = connect
