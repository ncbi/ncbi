const omit = require('lodash/omit')
const moment = require('moment')
const WordPackage = require('./wordPackage')
const VendorMeta = require('../xmlBaseModel/vendorMeta')
const FileService = require('../file/fileService')

const FTP_PDF_INPUT_PATH = '/new/testbcms/'

class ChapterPdfConversion extends WordPackage {
  constructor(options = {}) {
    super(options)

    this.data = {
      ...omit(this.data, ['timeout', 'citation_type', 'target_server']),
      chapter: options.chapter, // chapter ID
    }
  }

  async buildPackage(transaction) {
    const { id } = this.files.find(f => f.name === 'vendor-meta.xml')

    const vendorMeta = new VendorMeta(
      await FileService.getSourceToBuffer(id, transaction),
    )

    this.data[this.id] = vendorMeta.jobId

    const name = `${this.data.domain}.${vendorMeta.bcmsId}.${moment().format(
      'YYYY_MM_DD-hh_mm_ss',
    )}`

    const metadataFile = this.metafileJSON(name)

    const zippedFile = await this.zipFile({
      structureByCategory: true,
      transaction,
    })

    return { metadataFile, name, zippedFile }
  }

  // eslint-disable-next-line class-methods-use-this
  path() {
    return FTP_PDF_INPUT_PATH
  }
}

module.exports = ChapterPdfConversion
