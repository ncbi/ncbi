const ChapterPreview = require('./chapterPreview')
const TocPreview = require('./tocPreview')
const WordPackage = require('./wordPackage')
const ChapterXmlConversion = require('./chapterXmlConversion')
const WholeBookPreview = require('./wholeBookPreview')
const WholeBookXmlConversion = require('./wholeBookXmlConversion')
const ChapterPdfConversion = require('./chapterPdfConversion')
const WholeBookPdfConversion = require('./wholeBookPdfConversion')
const CoverPreview = require('./coverPreview')

class PackageFactory {
  static async create(packageType, fileCollection, options = {}) {
    let packageInstance = {}

    if (packageType === 'wordConversion') {
      packageInstance = new WordPackage(options)
    } else if (packageType === 'publish') {
      packageInstance = options.wholeBook
        ? new WholeBookPreview(options)
        : new ChapterPreview(options)
    } else if (packageType === 'preview') {
      packageInstance = new ChapterPreview(options)
    } else if (packageType === 'coverPreview') {
      packageInstance = new CoverPreview(options)
    } else if (packageType === 'tocPreview') {
      packageInstance = options.isCollection
        ? new WholeBookPreview(options)
        : new TocPreview(options)
    } else if (packageType === 'xmlConversion') {
      packageInstance = new ChapterXmlConversion(options)
    } else if (packageType === 'wholeBookXmlConversion') {
      packageInstance = new WholeBookXmlConversion(options)
    } else if (packageType === 'wholeBookPreview') {
      packageInstance = new WholeBookPreview(options)
    } else if (packageType === 'pdfConversion') {
      packageInstance = new ChapterPdfConversion(options)
    } else if (packageType === 'wholeBookPdfConversion') {
      packageInstance = new WholeBookPdfConversion(options)
    }

    packageInstance.addFiles(await fileCollection)

    return packageInstance
  }
}

module.exports = PackageFactory
