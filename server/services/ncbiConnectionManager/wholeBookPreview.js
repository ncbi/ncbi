const moment = require('moment')
const omit = require('lodash/omit')
const { File } = require('@pubsweet/models')
const Package = require('./package')

const FTP_BOOK_INPUT_PATH = '/ingest/book/'

class WholeBookPreview extends Package {
  constructor(options = {}) {
    super(options)

    this.book = options.book || {}

    const grantsFiltered = (
      ((options.book || {}).metadata || {}).grants || []
    ).map(g => omit(g, ['id', 'apply']))

    this.data = {
      ...this.data,
      release: options.release || false,
      target_database: options.target_database || 'dev',
      thumb: null,
      main_xml: null,
      grants: grantsFiltered || [],
      version_name: this.book?.fundedContentType,
      version_number: this.book?.version,
    }
  }

  // eslint-disable-next-line class-methods-use-this
  get id() {
    return 'package_id'
  }

  // eslint-disable-next-line class-methods-use-this
  path() {
    return FTP_BOOK_INPUT_PATH
  }

  async buildPackage(transaction) {
    try {
      const file = this.files.find(f => f.category === 'converted')

      this.data.main_xml = file.name

      if (this.book.fileCover) {
        const cover = await File.query(transaction).findOne({
          id: this.book.fileCover,
        })

        if (cover) this.files.push(cover)

        this.data.thumb = cover ? cover.name : null
      }

      const name = `${this.data.domain}.${this.data[this.id]}.${moment().format(
        'YYYY_MM_DD-hh_mm_ss',
      )}`

      const metadataFile = this.metafileJSON(name)
      const zippedFile = await this.zipFile({ transaction })

      return { metadataFile, name, zippedFile }
    } catch (e) {
      throw new Error(e)
    }
  }
}

module.exports = WholeBookPreview
