const moment = require('moment')
const Package = require('./package')

const FTP_COVER_PATH = '/covers/'

class CoverPreview extends Package {
  // eslint-disable-next-line class-methods-use-this
  get id() {
    return 'package_id'
  }

  // eslint-disable-next-line class-methods-use-this
  path() {
    return FTP_COVER_PATH
  }

  async buildPackage(transaction) {
    const name = `${this.data[this.id]}.${moment().format(
      'YYYY_MM_DD-hh_mm_ss',
    )}`

    const metadataFile = this.metafileJSON(name)
    const zippedFile = await this.zipFile({ transaction })

    return { metadataFile, name, zippedFile }
  }
}

module.exports = CoverPreview
