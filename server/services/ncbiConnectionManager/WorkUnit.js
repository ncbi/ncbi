/* eslint-disable no-restricted-syntax */
/* eslint-disable no-return-await */
/* eslint-disable no-await-in-loop */
const sleep = ms => {
  return new Promise(resolve => setTimeout(resolve, ms))
}

class WorkUnit {
  constructor(units, options = { maxRetries: 3, delayRetry: 3000 }) {
    this.units = units
    this.options = options
    this.maxRetries = 0
    this.error = null
    this.results = []
    this.currentUnit = null
  }

  async execute() {
    try {
      if (!this.error) {
        this.iteration = await this.iterateJob()
        this.currentUnit = await this.iteration.next()
      }

      while (!this.currentUnit.done) {
        this.maxRetries += 1
        this.results.push(await this.currentUnit.value())
        this.error = null
        this.currentUnit = await this.iteration.next()
      }

      return this.results
    } catch (error) {
      this.error = error

      if (this.options.maxRetries >= this.maxRetries) {
        await sleep(this.options.delayRetry)

        return await this.execute()
      }

      throw Error(
        `Max connection retries reached : ${this.options.maxRetries} times`,
      )
    }
  }

  async *iterateJob() {
    for (const unit of this.units) {
      yield unit
    }
  }
}

module.exports = WorkUnit
