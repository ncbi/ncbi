/* eslint-disable no-unsafe-finally */
/* eslint-disable no-return-await */
// const logger = require('@pubsweet/logger')
const { cloneDeep } = require('lodash')
const WorkUnit = require('./WorkUnit')
const NcbiAdapter = require('../ftp/ncbiAdapter')
const ApexAdapter = require('../ftp/apexAdapter')
const MockTestAdapter = require('../ftp/mockTestAdapter')

class NcbiService {
  constructor(ncbiInterface) {
    this.ncbiInterface = ncbiInterface
  }

  async send(data = null, to = null) {
    const work = new WorkUnit([
      async () => {
        const tempData = cloneDeep(data)

        if (this.ncbiInterface.isClosed) {
          await this.ncbiInterface.connection()
        }

        return await this.ncbiInterface.send(tempData, to)
      },
    ])

    return await work.execute()
  }

  async receive(local, remote) {
    const work = new WorkUnit([
      async () => {
        if (this.ncbiInterface.isClosed) {
          await this.ncbiInterface.connection()
        }

        return await this.ncbiInterface.receive(local, remote)
      },
    ])

    return await work.execute()
  }

  async connection() {
    const work = new WorkUnit([
      async () => {
        return await this.ncbiInterface.connection()
      },
    ])

    return await work.execute()
  }

  async close() {
    return this.ncbiInterface.close()
  }

  static loadFtpAdapter(isApex) {
    let adapter = null

    if (process.env.DISABLE_FTP === 'true') {
      adapter = new MockTestAdapter()
    } else {
      adapter = isApex ? new ApexAdapter() : new NcbiAdapter()
    }

    return new NcbiService(adapter)
  }
}

module.exports = NcbiService
