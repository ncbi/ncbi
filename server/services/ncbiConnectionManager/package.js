/* eslint-disable standard/no-callback-literal */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-return-await */
const fs = require('fs')
const archiver = require('archiver')
const crypto = require('crypto')

const {
  logger: { getRawLogger },
  uuid,
} = require('@coko/server')

const FileService = require('../file/fileService')

const logger = getRawLogger()

const USER = 'coko'

const USER_EMAIL = [
  'jkopanas@gmail.com',
  'ncbi@dionementis.com',
  'christi.1415@gmail.com',
]

class Package {
  constructor(options = {}) {
    this.data = {
      [this.id]: options.id || crypto.randomBytes(3).toString('hex'),
      user_name: USER,
      package: null,
      domain: options.domain || 'cokotestword3',
      recipients: { failure: USER_EMAIL, success: USER_EMAIL },
    }

    this.error = false
    this.objectId = null
  }

  // eslint-disable-next-line class-methods-use-this
  get id() {
    return 'job_id'
  }

  async zipFile(options = {}) {
    const packageName = `/tmp/package-zip-${uuid()}`
    const writeStream = fs.createWriteStream(packageName)
    const archive = archiver('zip', { zlib: { level: 9 } })

    archive.pipe(writeStream)

    // eslint-disable-next-line no-shadow
    archive.on('progress', ({ entries, fs }) => {
      logger.info(`${entries.total} / ${entries.processed}`)
      logger.info(`${fs.totalBytes} ** ${fs.processedBytes}`)
    })

    // eslint-disable-next-line guard-for-in
    for (const f in this.files) {
      const { name, oid, category } = this.files[f]

      logger.info(
        `kafka create Zip File: ${this.files[f].name} on ${packageName}`,
      )

      const filename = options.structureByCategory
        ? `${category}/${name}`
        : name

      const readStream = await FileService.read({
        oid,
        transaction: options.transaction,
      })

      archive.append(readStream, { name: filename })
    }

    await archive.finalize()

    return packageName
  }

  addFiles(files) {
    this.files = files
  }

  metafileJSON(name) {
    const data = {
      ...this.data,
      notification_recipients: this.data.recipients,
      package: `${name}.zip`,
    }

    const packageName = `/tmp/package-json-${uuid()}`

    fs.writeFileSync(packageName, JSON.stringify(data))

    return packageName
  }

  setRecipients(value, state = 'success') {
    if (Array.isArray(value)) {
      this.recipients[state] = this.recipients[state].concat(value)
    }

    this.recipients[state].push(value)
  }
}

module.exports = Package
