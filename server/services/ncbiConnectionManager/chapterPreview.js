/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable padding-line-between-statements */
const moment = require('moment')
const omit = require('lodash/omit')
const Package = require('./package')

const FTP_WORD_INPUT_PATH = '/ingest/chapter/'

class ChapterPreview extends Package {
  constructor(options = {}) {
    super(options)

    const grantsFiltered = (
      ((options.book || {}).metadata || {}).grants || []
    ).map(g => omit(g, ['id', 'apply']))

    const bookComponentsGrantsFiltered = (
      ((options.bookComponent || {}).metadata || {}).grants || []
    ).map(g => omit(g, ['id', 'apply']))

    this.data = {
      ...this.data,
      chapter: null,
      release: options.release || false,
      target_database: options.target_database || 'dev',
      version: options.versionName,
      xml_file: null,
      grants: [...grantsFiltered, ...bookComponentsGrantsFiltered] || [],
    }
  }

  // eslint-disable-next-line class-methods-use-this
  get id() {
    return 'package_id'
  }

  // eslint-disable-next-line class-methods-use-this
  path() {
    return FTP_WORD_INPUT_PATH
  }

  async buildPackage(transaction) {
    try {
      const file = this.files.find(f => f.category === 'converted')

      this.data.xml_file = file.name

      this.data.chapter = file.chapter

      const name = `${this.data.domain}.${file.name.replace(/\.[^/.]+$/, '')}.${
        file.versionName
      }.${this.data[this.id]}.${moment().format('YYYY_MM_DD-hh_mm_ss')}`
      const metadataFile = this.metafileJSON(name)
      const zippedFile = await this.zipFile({ transaction })
      return { metadataFile, name, zippedFile }
    } catch (e) {
      throw new Error(e)
    }
  }
}

module.exports = ChapterPreview
