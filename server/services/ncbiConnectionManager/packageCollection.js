/* eslint-disable no-param-reassign */
/* eslint-disable no-await-in-loop */
const { NcbiNotificationMessage } = require('@pubsweet/models')

const PackageFactory = require('./packageFactory')
const PgBossService = require('../pgBossService/pgBossService')

class PackageCollection {
  static async sendForConversion(
    packageType,
    file,
    options = {},
    jobsParams,
    transaction,
  ) {
    const boss = await PgBossService.getInstance()
    const { objectId, userId = null, pushToQueue = null } = jobsParams

    const packageInstance = await PackageFactory.create(
      packageType,
      await file,
      options,
    )

    const isApex =
      packageType === 'pdfConversion' ||
      packageType === 'wholeBookPdfConversion'

    const {
      metadataFile,
      zippedFile,
      name,
    } = await packageInstance.buildPackage(transaction)

    const savedJobQueue = await NcbiNotificationMessage.query(
      transaction,
    ).insert({
      jobId: packageInstance.data[packageInstance.id],
      packageType,
      options: JSON.stringify(options),
    })

    const destFolder = `${packageInstance.path()}${name}`

    // eslint-disable-next-line no-return-await
    return await boss.send('ftp-queue', {
      isApex,
      action: 'upload',
      pushToQueue,
      pushToQueueData: { objectId, userId },
      objectId,
      notificationId: savedJobQueue.id,
      files: [
        { local: metadataFile, remote: `${destFolder}.json` },
        { local: zippedFile, remote: `${destFolder}.zip` },
      ],
    })
  }
}

module.exports = PackageCollection
