/* eslint-disable standard/no-callback-literal */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-return-await */
const moment = require('moment')
const Package = require('./package')

const TIMEOUT = 720
const FTP_WORD_INPUT_PATH = '/convert/word/in/'

class WordPackage extends Package {
  constructor(options = {}) {
    super(options)

    this.data = {
      ...this.data,
      timeout: options.timeout || TIMEOUT,
      citation_type: options.citation_type || 0,
      target_server: process.env.WORD_TARGET_SERVER,
    }
  }

  async buildPackage(transaction) {
    const file = this.files.find(f => f.category === 'source')

    const name = `${this.data.domain}.${file.name}.${
      this.data[this.id]
    }.${moment().format('YYYY_MM_DD-hh_mm_ss')}`

    const metadataFile = this.metafileJSON(name)

    const zippedFile = await this.zipFile({ transaction })

    return { metadataFile, name, zippedFile }
  }

  // eslint-disable-next-line class-methods-use-this
  path() {
    return FTP_WORD_INPUT_PATH
  }
}

module.exports = WordPackage
