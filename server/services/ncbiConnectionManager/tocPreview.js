/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable padding-line-between-statements */
const moment = require('moment')
const Package = require('./package')

const FTP_WORD_INPUT_PATH = '/ingest/chapter/'

class TocPreview extends Package {
  constructor(options = {}) {
    super(options)

    this.data = {
      ...this.data,
      chapter: 'toc',
      version: 1,
      xml_file: 'TOC.xml',
      target_database: options.target_database || 'dev',
      release: options.release || false,
      thumb: null,
    }
  }

  // eslint-disable-next-line class-methods-use-this
  get id() {
    return 'package_id'
  }

  // eslint-disable-next-line class-methods-use-this
  path() {
    return FTP_WORD_INPUT_PATH
  }

  async buildPackage(transaction) {
    const file = this.files.find(f => f.category === 'cover') || {}

    this.data.thumb = file.name || null

    const name = `${this.data.domain}.toc.1.${
      this.data[this.id]
    }.${moment().format('YYYY_MM_DD-hh_mm_ss')}`

    const metadataFile = this.metafileJSON(name)
    const zippedFile = await this.zipFile({ transaction })

    return { metadataFile, name, zippedFile }
  }
}

module.exports = TocPreview
