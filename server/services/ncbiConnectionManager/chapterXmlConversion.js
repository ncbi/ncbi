const omit = require('lodash/omit')
const WordPackage = require('./wordPackage')

const FTP_XML_INPUT_PATH = '/convert/xml/chapter/in/'

class ChapterXmlConversion extends WordPackage {
  constructor(options = {}) {
    super(options)

    this.data = {
      ...omit(this.data, ['timeout', 'citation_type', 'target_server']),
      chapter: options.chapter, // chapter ID
    }
  }

  // eslint-disable-next-line class-methods-use-this
  path() {
    return FTP_XML_INPUT_PATH
  }
}

module.exports = ChapterXmlConversion
