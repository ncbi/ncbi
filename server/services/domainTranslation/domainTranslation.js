const {
  cloneDeep,
  // get,
  isEmpty,
  isNil,
  set,
} = require('lodash')

// const config = require('config')

const { flattenKeys } = require('../../_helpers')

const BASE_MESSAGE = `Domain translation service:`

// #region HELPERS

/**
 * Takes an object and reverses the keys
 * eg. { tocSubtitle: 'toc-subtitle' } => { 'toc-subtitle': tocSubtitle }
 */

const reverseMapperCreator = original =>
  Object.keys(original).reduce((obj, currentKey) => {
    const currentValue = original[currentKey]
    /* eslint-disable-next-line no-param-reassign */
    obj[currentValue] = currentKey
    return obj
  }, {})

// #endregion HELPERS

const domainToplevelValues = [
  'publisher',
  'journal_qa_status',
  'journal_title',
  'live_in_pmc',
]

const domainPropertiesValues = ['entire_journal_open_access']

const bookSettingsToDomainMapper = {
  title: 'journal_title',
  'metadata.openAccess': 'entire_journal_open_access',
  'settings.alternateVersionsPdf': 'alternate-versions-pdf',
  'settings.alternateVersionsPdfBook': 'alternate-versions-pdf-book',
  'settings.bookLevelAffiliationStyle': 'toc-aff-style',
  'settings.bookLevelLinks': 'book-level-links',
  'settings.bookLevelLinksMarkdown': 'book-level-links-md',
  'settings.chapterLevelAffiliationStyle': 'aff-style',
  'settings.citationSelfUrl': 'citation-self-url',
  'settings.createPdf': 'create-pdf',
  'settings.createWholebookPdf': 'create-wholebook-pdf',
  'settings.createVersionLink': 'create-version-link',
  'settings.displayObjectsLocation': 'display-objects-location',
  'settings.footnotesDecimal': 'relabel-footnotes',
  'settings.indexChaptersInPubmed': 'index-chapter-level',
  'settings.indexInPubmed': 'index-book-in-pubmed',
  'settings.multiplePublishedVersions': 'publisher-version',
  'settings.publisher': 'publisher',
  'settings.publisherUrl': 'journal-url',
  'settings.questionAnswerStyle': 'question-answer-style',
  'settings.referenceListStyle': 'back-reference-list-style',
  'settings.specialPublisherLinkText': 'special-publisher-link-text',
  'settings.specialPublisherLinkUrl': 'special-publisher-link-url',
  'settings.toc.contributors': 'toc-contributors',
  'settings.toc.documentHistory': 'toc-document-history',
  'settings.toc.subtitle': 'toc-subtitle',
  'settings.toc.allTitle': 'toc-all-title',
  'settings.toc.tocMaxDepth': 'toc-max-depth',
  'settings.tocExpansionLevel': 'toc-expansion-level',
  'settings.UKPMC': 'ukpmc-journal',
  'settings.versionLinkText': 'version-link-text',
  'settings.versionLinkUri': 'version-link-uri',
  'settings.xrefAnchorStyle': 'xref-ancor-style',

  // 'settings.qaStatus': 'journal_qa_status',
  // 'settings.releaseStatus': 'live_in_pmc',
}

const collectionSettingsToDomainMapper = {
  'metadata.openAccess': 'entire_journal_open_access',
  'settings.citationSelfUrl': 'citation-self-url',
  'settings.groupBooksBy': 'collection-group',
  'settings.orderBooksBy': 'collection-sort',
  'settings.publisherUrl': 'journal-url',
  'settings.toc.contributors': 'toc-contributors',
  'settings.toc.fullBookCitations': 'toc-pub-info',
  'settings.UKPMC': 'ukpmc-journal',

  // Settings that are not in the model but are built dynamically in this file
  'settings.collectionAutoRebuild': 'collection-auto-rebuild',
}

class DomainTranslation {
  /**
   * Reads a template and returns an object of values that comply with our
   * GraphQL API
   */
  static domainToBookSettings(template) {
    try {
      const data = {}
      const mapper = reverseMapperCreator(bookSettingsToDomainMapper)

      domainPropertiesValues.forEach(key => {
        const mappedKey = mapper[key]
        if (mappedKey) set(data, mappedKey, template.properties[key])
      })

      domainToplevelValues.forEach(key => {
        const mappedKey = mapper[key]
        if (mappedKey) set(data, mappedKey, template[key])
      })

      template.attributes.forEach(attribute => {
        const mappedKey = mapper[attribute.name]

        if (mappedKey) {
          let valueToSet = attribute.value

          if (attribute.datatype === 'boolean') {
            if (attribute.value === 'yes') valueToSet = true
            if (attribute.value === 'no') valueToSet = false
          }

          set(data, mappedKey, valueToSet)
        }
      })

      return data
    } catch (error) {
      error.message = `${BASE_MESSAGE} Domain template to data: ${error}`
      throw error
    }
  }

  // FOR INTERNAL CLASS USE ONLY
  static createDomainUpdateObjectFromData(template, values, mapper) {
    try {
      /**
       * CLONE EXISTING
       */

      const baseTemplate = template
      const newDomainObject = cloneDeep(baseTemplate)
      const flattenedValues = flattenKeys(values)

      /**
       * BUILD VALUES THAT NEED TO BE UPDATED
       */

      // replace attributes object with update (constructed below)
      newDomainObject.attributes = []

      Object.keys(flattenedValues).forEach(key => {
        let value = flattenedValues[key]
        const keyInDomain = mapper[key]

        // disallow sending null or undefined values to API
        if (isNil(value)) return

        // merge subtitle and edition into title value
        if (key === 'title') {
          const { title, subTitle, edition } = values

          // [what this amounts to] "title: subTitle, edition"
          value = `${title}${subTitle || edition ? ': ' : ''}${subTitle || ''}${
            subTitle && edition ? ', ' : ''
          }${edition ? `${edition}` : ''}`
        }

        if (domainToplevelValues.includes(keyInDomain)) {
          newDomainObject[keyInDomain] = value

          if (keyInDomain === 'journal_title') {
            newDomainObject.pubmed_journal_title = value
            newDomainObject.toc_journal_title = value
          }
        }

        if (domainPropertiesValues.includes(keyInDomain)) {
          newDomainObject.properties[keyInDomain] = value
        }

        const attribute = baseTemplate.attributes.find(
          attr => attr.name === keyInDomain,
        )

        if (attribute) {
          const { datatype, name, restrictions } = attribute
          let valueToWrite = value

          if (datatype === 'boolean') {
            if (valueToWrite === true) valueToWrite = 'yes'
            else if (valueToWrite === false) valueToWrite = 'no'
            else throw new Error('Invalid boolean attribute type')
          }

          if (!isEmpty(restrictions)) {
            const optionToWrite = restrictions.find(
              option => option.value === valueToWrite,
            )

            if (!optionToWrite)
              throw new Error(
                `Value for ${valueToWrite} not found in list of restrictions for attribute ${name}`,
              )

            newDomainObject.attributes.push({
              name,
              value: optionToWrite.value,
            })
          } else {
            newDomainObject.attributes.push({
              name,
              value: valueToWrite,
            })
          }
        }
      })

      return newDomainObject
    } catch (error) {
      error.message = `${BASE_MESSAGE} Create domain update object from data: ${error.message}`
      throw error
    }
  }

  /**
   * Reads an object of values as they're coming from our GraphQL API
   * (for book settings) and returns an object that can be sent for
   * a domain update request
   */
  static createBookDomainUpdateObjectFromData(template, values) {
    try {
      const mapper = bookSettingsToDomainMapper
      return this.createDomainUpdateObjectFromData(template, values, mapper)
    } catch (error) {
      error.message = `${BASE_MESSAGE} Create book domain update object from data: ${error.message}`
      throw error
    }
  }

  /**
   * Reads an object of values as they're coming from our GraphQL API
   * (for collection settings) and returns an object that can be sent for
   * a domain update request
   */
  static createCollectionDomainUpdateObjectFromData(template, values) {
    try {
      const mapper = collectionSettingsToDomainMapper
      const data = cloneDeep(values)

      if (values.settings) {
        if (!values.settings.groupBooks) {
          data.settings.groupBooksBy = 'NONE'
        }

        if (values.settings.groupBooksBy === 'customGroupTitles') {
          data.settings.groupBooksBy = 'NONE'
          data.settings.collectionAutoRebuild = false
        } else {
          data.settings.collectionAutoRebuild = true
        }
      }

      return this.createDomainUpdateObjectFromData(template, data, mapper)
    } catch (error) {
      error.message = `${BASE_MESSAGE} Create book domain update object from data: ${error.message}`
      throw error
    }
  }

  /**
   * Creates a patch that edits the book-collection-name attribute in the domain.
   * When updating a collection (ie. setting the value of book-collection-name
   * to the collection's own domain name), the second argument is not needed.
   * When updating a book, pass the book template as the first argument and the
   * domain name of the collection it is being added to as the second argument.
   */
  static createBookCollectionNameUpdateObject(template, collectionDomain) {
    try {
      if (!template.domain) throw new Error('Template must have a domain')
      const newDomainObject = cloneDeep(template)
      const patchValue = collectionDomain || template.domain

      const bookCollectionNamePatch = {
        name: 'book-collection-name',
        value: patchValue,
      }

      newDomainObject.attributes = [bookCollectionNamePatch]
      return newDomainObject
    } catch (error) {
      error.message = `${BASE_MESSAGE} Create book collection name update object: ${error.message}`
      throw error
    }
  }

  // NO USE CASE FOR THIS FOR THE TIME BEING
  /**
   * Gets an attribute name and a value, and returns the value id according
   * to the attribute's restrictions in the domain service.
   *
   * eg. getAttributeValueIdByValue('question-answer-style', 'normal') will return 3
   */

  // static getAttributeValueIdByValue(attributeName, value) {
  //   try {
  //     // it shouldn't make a difference which template we're using
  //     const someTemplate = config.get(
  //       'bookSettings.domainTemplates.chapterProcessed',
  //     )

  //     if (!someTemplate) {
  //       throw new Error('Template not found!')
  //     }

  //     const attribute = someTemplate.attributes.find(
  //       attr => attr.name === attributeName,
  //     )

  //     if (!attribute) {
  //       throw new Error(`Attribute ${attributeName} not found!`)
  //     }

  //     if (!attribute.restrictions || attribute.restrictions.length === 0) {
  //       throw new Error(
  //         `Attribute ${attributeName} does not have a set of predefined acceptable values!`,
  //       )
  //     }

  //     const valueObject = attribute.restrictions.find(
  //       option => option.value === value,
  //     )

  //     if (!valueObject) {
  //       throw new Error(
  //         `No value found for ${value} in attribute ${attributeName}`,
  //       )
  //     }

  //     const valueId = valueObject.id

  //     if (!valueId) {
  //       throw new Error(
  //         `Found value ${valueObject} in attribute ${attributeName} has no "id" property!`,
  //       )
  //     }

  //     return valueId
  //   } catch (error) {
  //     error.message = `${BASE_MESSAGE} getAttributeValueidByValue: Cannot get value id: ${error.message}`
  //     throw error
  //   }
  // }
}

module.exports = DomainTranslation
