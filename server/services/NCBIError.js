class NCBIError {
  constructor(props) {
    this.message = props
    this.name = 'NCBIError'
  }
}

module.exports = NCBIError
