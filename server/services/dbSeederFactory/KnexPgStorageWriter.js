// const mapValues = require('lodash/mapValues')

class KnexPgStorageWriter {
  constructor(knex) {
    this.knex = knex
  }

  insert = async (tableName, data) => {
    /* TODO - Refactor tests to treat "this.knex" as the current transaction.
     * Database changes do not currently rollback between tests because they
     * are not assigned to the current transaction; the fix is to uncomment
     * "this.knex" below BUT this will break existing tests.
     * Refactor tests and then uncomment "this.knex".
     */
    const result = await tableName.query(/* this.knex */).insert(data)

    return {
      ...result,
      ...data,
    }
  }
}

module.exports = KnexPgStorageWriter
