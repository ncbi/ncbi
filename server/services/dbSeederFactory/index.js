const { db } = require('@coko/server')

// eslint-disable-next-line import/no-extraneous-dependencies
const { ref, default: DbSeeder } = require('rs-db-seeder')
const { v4: uuidv4 } = require('uuid')
const _ = require('lodash')

const {
  Book,
  BookComponent,
  Division,
  Errors,
  FileVersion,
  File,
  User,
  Team,
  TeamMember,
  Organisation,
} = require('@pubsweet/models')

const faker = require('faker')

const KnexPgStorageWriter = require('./KnexPgStorageWriter')

const addSeedFactory = () => {
  const storage = new KnexPgStorageWriter(db)
  const seeder = new DbSeeder(storage)

  seeder.addFactory({
    id: 'user',
    tableName: User,
    dataProvider: data => {
      const surname = faker.name.lastName()
      return {
        email: faker.internet.email(),
        username: faker.internet.userName(),
        givenName: `${faker.name.firstName()} ${surname}`,
        surname,
        passwordResetToken: '',
        passwordResetTimestamp: null,
        ...data,
      }
    },
    refs: [],
  })

  seeder.addFactory({
    id: 'team',
    tableName: Team,
    dataProvider: data => data,
    refs: [],
  })

  seeder.addFactory({
    id: 'teamMember',
    tableName: TeamMember,
    dataProvider: data => data,
    refs: [],
  })

  seeder.addFactory({
    id: 'bookComponent',
    tableName: BookComponent,
    dataProvider: data => ({
      title: faker.name.title(),
      ...data,
    }),
    refs: [
      ref('book', 'id', 'bookId'),
      ref('division', 'id', 'divisionId'),
      ref('user', 'id', 'ownerId'),
    ],
  })

  seeder.addFactory({
    id: 'file',
    tableName: File,
    dataProvider: data => {
      return {
        mimeType: 'text/html',
        name: faker.name.title(),
        category: data.category || 'converted',
        oid: 333,
        ...data,
      }
    },
    refs: [],
  })

  seeder.addFactory({
    id: 'fileVersion',
    tableName: FileVersion,
    dataProvider: data => ({
      parentId: null,
      category: 'converted',
      tag: null,
      versionName: '1',
      status: 'new-upload',
      ...data,
    }),
    refs: [ref('user', 'id', 'ownerId'), ref('file', 'id', 'fileId')],
  })

  seeder.addFactory({
    id: 'division',
    tableName: Division,
    dataProvider: data => ({
      label: 'body',
      bookComponents: [],
      ...data,
    }),
    refs: [ref('book', 'id', 'bookId')],
  })

  seeder.addFactory({
    id: 'organisation',
    tableName: Organisation,
    dataProvider: data => ({ name: faker.name.title(), ...data }),
    refs: [],
  })

  seeder.addFactory({
    id: 'error',
    tableName: Errors,
    dataProvider: data => data,
    refs: [],
  })

  seeder.addFactory({
    id: 'book',
    tableName: Book,
    dataProvider: data => ({
      abstract: faker.lorem.text(),
      abstractTitle: faker.name.title(),
      title: faker.name.title(),
      domain: `bcms${faker.random.number()}`,
      domainId: `${faker.random.number()}`,
      ...data,
    }),
    refs: [
      ref('user', 'id', 'ownerId'),
      ref('organisation', 'id', 'organisationId'),
    ],
  })

  seeder.addScenario({
    id: 'addUser',
    // eslint-disable-next-line no-shadow
    insert: async (storage, data) => {
      const { knex } = storage
      knex.raw('BEGIN')

      const passwordHash = await User.hashPassword(data.password)

      const user = await seeder.insert('user', {
        username: data.username,
        passwordHash,
        email: data.email,
        givenName: data.username,
        surname: 'seed',
      })

      if (data.sysAdmin) {
        let sysAdminTeam = await Team.query().findOne({
          global: true,
          role: 'sysAdmin',
        })

        if (!sysAdminTeam) {
          sysAdminTeam = await seeder.insert('team', {
            global: true,
            role: 'sysAdmin',
          })
        }

        await seeder.insert('teamMember', {
          status: 'enabled',
          userId: user.id,
          teamId: sysAdminTeam.id,
        })

        knex.raw('COMMIT')
        return { user, sysAdminTeam }
      }

      knex.raw('COMMIT')
      return { user }
    },
  })

  seeder.addScenario({
    id: 'addTeamMemberToUser',
    // eslint-disable-next-line no-shadow
    insert: async (storage, data) => {
      const { knex } = storage
      knex.raw('BEGIN')

      let { team, user } = data

      const foundUser = await User.query().findOne({
        username: user.username,
      })

      if (!team.id) {
        team = await Team.query().findOne({
          objectId: team.objectId,
          role: team.role,
        })
      }

      await seeder.insert('teamMember', {
        status: 'enabled',
        userId: foundUser.id,
        teamId: team.id,
      })

      knex.raw('COMMIT')

      return true
    },
  })

  seeder.addScenario({
    id: 'addAdmin',
    // eslint-disable-next-line no-shadow
    insert: async (storage, data) => {
      const { knex } = storage
      knex.raw('BEGIN')

      const passwordHash = await User.hashPassword(data.password)

      let user = null

      const adminExists = await User.query().findOne({ username: 'admin' })

      if (adminExists) {
        user = await User.query()
          .patch({
            email: data.email,
            passwordHash,
          })
          .findOne({ username: 'admin' })
          .returning('*')

        knex.raw('COMMIT')
        return { user }
      }

      user = await seeder.insert('user', {
        username: data.username,
        passwordHash,
        email: data.email,
      })

      const sysAdminTeam = await seeder.insert('team', {
        global: true,
        role: 'sysAdmin',
      })

      await seeder.insert('teamMember', {
        status: 'enabled',
        userId: user.id,
        teamId: sysAdminTeam.id,
      })

      knex.raw('COMMIT')
      return { user, sysAdminTeam }
    },
  })

  seeder.addScenario({
    id: 'addBookComponent',
    // eslint-disable-next-line no-shadow
    insert: async (storage, data) => {
      const { knex } = storage
      knex.raw('BEGIN')

      const user = data.user || (await seeder.insert('user', {}))

      const book = await seeder.insert('book', {
        user,
      })

      const division = await seeder.insert('division', { book })

      const bookComponent = await seeder.insert('bookComponent', {
        ...data,
        status: 'preview',
        book,
        user,
        division,
      })

      const fileVersion = await seeder.insert('fileVersion', {
        ...data,
        status: 'preview',
        bookComponentId: bookComponent.id,
        user,
        file: {
          category: 'converted',
        },
      })

      await seeder.insert('fileVersion', {
        ...data,
        status: 'preview',
        category: 'source',
        bookComponentId: bookComponent.id,
        user,
        file: {
          category: 'source',
        },
      })

      knex.raw('COMMIT')
      return { bookComponent, book, user, fileVersion }
    },
  })

  seeder.addScenario({
    id: 'addBookComponentWithCustomStatus',
    // eslint-disable-next-line no-shadow
    insert: async (storage, data) => {
      const { knex } = storage
      knex.raw('BEGIN')

      const user = data.user || (await seeder.insert('user', {}))

      const book = await seeder.insert('book', {
        user,
      })

      const division = await seeder.insert('division', { book })

      const bookComponent = await seeder.insert('bookComponent', {
        ...data,
        status: 'in-review',
        book,
        user,
        division,
      })

      const fileVersion = await seeder.insert('fileVersion', {
        ...data,
        status: 'loading-preview',
        bookComponentId: bookComponent.id,
        user,
        file: {
          category: 'converted',
        },
      })

      await seeder.insert('fileVersion', {
        ...data,
        status: 'loading-preview',
        category: 'source',
        bookComponentId: bookComponent.id,
        user,
        file: {
          category: 'source',
        },
      })

      await seeder.insert('error', {
        objectId: bookComponent.id,
        severity: 'error',
        errorCategory: 'loading-errors',
      })

      knex.raw('COMMIT')
      return { bookComponent, book, user, fileVersion }
    },
  })

  seeder.addScenario({
    id: 'addBookWithCustomStatus',
    // eslint-disable-next-line no-shadow
    insert: async (storage, data) => {
      const { knex } = storage
      knex.raw('BEGIN')

      const user = data.user || (await seeder.insert('user', {}))

      const book = await seeder.insert('book', {
        user,
        status: 'in-review',
      })

      const division = await seeder.insert('division', { book })

      const bookComponent = await seeder.insert('bookComponent', {
        ...data,
        status: 'in-review',
        book,
        user,
        division,
      })

      const fileVersion = await seeder.insert('fileVersion', {
        ...data,
        status: 'loading-preview',
        bookId: book.id,
        user,
        file: {
          category: 'converted',
        },
      })

      await seeder.insert('fileVersion', {
        ...data,
        status: 'loading-preview',
        category: 'source',
        bookId: book.id,
        user,
        file: {
          category: 'source',
        },
      })

      await seeder.insert('error', {
        objectId: book.id,
        severity: 'error',
        errorCategory: 'loading-errors',
      })

      knex.raw('COMMIT')
      return { bookComponent, book, user, fileVersion }
    },
  })

  seeder.addScenario({
    id: 'addBookDetailed',
    // eslint-disable-next-line no-shadow
    insert: async (storage, data) => {
      const { knex } = storage
      knex.raw('BEGIN')

      const user = data.user || (await seeder.insert('user', {}))

      const bookData = data.book || {}

      const book = await seeder.insert('book', {
        // DEFAULTS (may be overwritten by "bookData")
        status: 'in-review',
        // book data specified by the current test
        ...bookData,
        // FIXED VALUES (cannot be overwritten by "bookData")
        user,
      })

      // Prepare bookComponent ids upfront for 'Division.bookComponents'
      const coerceBookComponentIds = bc => {
        const result = _.clone(bc)

        if (!result.id) {
          result.id = uuidv4()
        }

        if (result.bookComponents) {
          result.bookComponents = result.bookComponents.map(
            coerceBookComponentIds,
          )
        } else {
          result.bookComponents = []
        }

        return result
      }

      const addDivision = async label => {
        // Get bookComponents and inject ids as needed
        const bookComponentsData = _.get(
          data,
          `${label}.bookComponents`,
          [],
        ).map(coerceBookComponentIds)

        // Insert current division into the database
        const division = await seeder.insert('division', {
          book,
          label,
          bookComponents: bookComponentsData.map(bc => bc.id),
        })

        // Insert all (nested) bookComponents into the database
        division.bookComponents = await addBookComponents(
          division,
          bookComponentsData,
        )

        return division
      }

      const addBookComponents = async (division, bookComponentsData) => {
        const bookComponents = await Promise.all(
          bookComponentsData.map(async bookComponentData => {
            const bookComponent = await seeder.insert('bookComponent', {
              // DEFAULTS (may be overwritten by "bookComponentData")
              status: 'in-review',
              componentType: bookComponentsData.bookComponents
                ? 'part'
                : 'chapter',
              // book component data specified by the current test
              ...bookComponentData,
              // FIXED VALUES (cannot be overwritten by "bookComponentData")
              bookComponents: bookComponentData.bookComponents.map(bc => bc.id),
              book,
              user,
              division,
            })

            if (bookComponentData.bookComponents) {
              // Insert all (nested) bookComponents into the database
              bookComponent.bookComponents = await addBookComponents(
                division,
                bookComponentData.bookComponents,
              )
            }

            return bookComponent
          }),
        )

        return bookComponents
      }

      let frontMatter = null
      let body = null
      let backMatter = null

      if (data.frontMatter) {
        // Insert Division with the expected array of BookComponents
        frontMatter = await addDivision('frontMatter')
      }

      if (data.body) {
        // Insert Division with the expected array of BookComponents
        body = await addDivision('body')
      }

      if (data.backMatter) {
        // Insert Division with the expected array of BookComponents
        backMatter = await addDivision('backMatter')
      }

      const fvData = data.fileVersion || {}

      const fileVersion = await seeder.insert('fileVersion', {
        status: 'loading-preview',
        ...fvData,
        bookId: book.id,
        user,
        file: {
          category: 'converted',
        },
      })

      await seeder.insert('fileVersion', {
        status: 'loading-preview',
        ...fvData,
        category: 'source',
        bookId: book.id,
        user,
        file: {
          category: 'source',
        },
      })

      await seeder.insert('error', {
        objectId: book.id,
        severity: 'error',
        errorCategory: 'loading-errors',
      })

      knex.raw('COMMIT')
      return { book, frontMatter, body, backMatter, user, fileVersion }
    },
  })

  seeder.addScenario({
    id: 'addBookMultiChapter',
    // eslint-disable-next-line no-shadow
    insert: async (storage, data) => {
      const { knex } = storage
      knex.raw('BEGIN')

      const user = data.user || (await seeder.insert('user', {}))

      const bookData = data.book || {}

      const book = await seeder.insert('book', {
        // DEFAULTS (may be overwritten by "bookData")
        status: 'in-review',
        // book data specified by the current test
        ...bookData,
        // FIXED VALUES (cannot be overwritten by "bookData")
        user,
      })

      // Prepare bookComponent ids upfront for 'Division.bookComponents'
      const bookComponentsData = data.bookComponents.map(bc => {
        if (bc.id) {
          return bc
        }

        const result = _.clone(bc)
        result.id = uuidv4()
        return result
      })

      let parent
      let division

      if (!data.parentType || data.parentType === Division) {
        // Insert Division with the expected array of BookComponents
        division = await seeder.insert('division', {
          book,
          bookComponents: bookComponentsData.map(bc => bc.id),
        })

        parent = division
      } else {
        // Insert Division with a single BookComponent part
        const partId = uuidv4()

        division = await seeder.insert('division', {
          book,
          bookComponents: [partId],
        })

        // Insert BookComponent part with the expected array of BookComponents
        parent = await seeder.insert('bookComponent', {
          id: partId,
          status: 'in-review',
          bookComponents: bookComponentsData.map(bc => bc.id),
          componentType: 'part',
          book,
          user,
          division,
        })
      }

      // Now add all BookComponents to the Division
      const bookComponents = await Promise.all(
        bookComponentsData.map(async bookComponentData => {
          const bookComponent = await seeder.insert('bookComponent', {
            // DEFAULTS (may be overwritten by "bookComponentData")
            status: 'in-review',
            // book component data specified by the current test
            ...bookComponentData,
            // FIXED VALUES (cannot be overwritten by "bookComponentData")
            book,
            user,
            division,
          })

          return bookComponent
        }),
      )

      const fvData = data.fileVersion || {}

      const fileVersion = await seeder.insert('fileVersion', {
        status: 'loading-preview',
        ...fvData,
        bookId: book.id,
        user,
        file: {
          category: 'converted',
        },
      })

      await seeder.insert('fileVersion', {
        status: 'loading-preview',
        ...fvData,
        category: 'source',
        bookId: book.id,
        user,
        file: {
          category: 'source',
        },
      })

      await seeder.insert('error', {
        objectId: book.id,
        severity: 'error',
        errorCategory: 'loading-errors',
      })

      knex.raw('COMMIT')
      return { bookComponents, division, parent, book, user, fileVersion }
    },
  })

  return seeder
}

module.exports = addSeedFactory
