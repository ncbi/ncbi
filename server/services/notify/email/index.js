const {
  bookCreated,
  bookVersionCreated,
  bookTeamUpdated,
  bookComponentPublished,
  bookMetadataUpdated,
  collectionMetadataUpdated,
  changesRequested,
  collectionCreated,
  errors,
  mention,
  mentionVendorIssue,
  requestAccessToOrganisation,
  previewGenerated,
  rejectUsers,
  reviewApproval,
  reviewRequested,
  verifyUsers,
} = require('./types')

const mapper = {
  bookCreated,
  bookVersionCreated,
  bookTeamUpdated,
  bookComponentPublished,
  bookMetadataUpdated,
  collectionMetadataUpdated,
  changesRequested,
  collectionCreated,
  errors,
  mention,
  mentionVendorIssue,
  requestAccessToOrganisation,
  previewGenerated,
  rejectUsers,
  reviewApproval,
  reviewRequested,
  verifyUsers,
}

const email = async (type, context) => {
  if (!mapper[type])
    throw new Error(`${type} is not a valid email notification type`)

  await mapper[type](context)
}

module.exports = email
