const { logger } = require('@coko/server')

const {
  emailListWithoutCurrentUser,
  getObjectEmailsByTeam,
  sendEmail,
} = require('./_helpers')

const reviewRequestedEmail = async context => {
  try {
    const { objectId, objectTitle, userId } = context

    // get previewer emails
    let emails = await getObjectEmailsByTeam(objectId, 'previewer')
    emails = await emailListWithoutCurrentUser(emails, userId)
    if (emails.length === 0) return

    const content = `
      You have been requested to review "${objectTitle}".
    `

    const data = {
      content,
      subject: 'Request for review',
      to: emails.join(','),
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = reviewRequestedEmail
