const { logger } = require('@coko/server')

const { getObjectEmailsByTeam, sendEmail } = require('./_helpers')

const bookComponentPublishedEmail = async context => {
  try {
    const { id, title } = context

    const authorEmails = await getObjectEmailsByTeam(id, 'author')

    const content = `
      A new version of "${title}" has just been published.
    `

    const data = {
      content,
      subject: 'Chapter published',
      to: authorEmails.join(','),
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = bookComponentPublishedEmail
