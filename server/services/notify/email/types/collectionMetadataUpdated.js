const { logger } = require('@coko/server')
const { flatten } = require('lodash')
const Organisation = require('../../../../models/organisation/organisation')
const User = require('../../../../models/user/user')

const { getOrganisationEmailsByTeam, sendEmail } = require('./_helpers')

const collectionMetadataUpdatedEmail = async context => {
  try {
    const { collectionTitle, organisationId, userId, alias } = context

    const emails = []
    emails.push(await getOrganisationEmailsByTeam(organisationId, 'sysAdmin'))
    emails.push(await getOrganisationEmailsByTeam(organisationId, 'orgAdmin'))

    const organisation = await Organisation.query()
      .findById(organisationId)
      .throwIfNotFound()

    const user = await User.query().findById(userId).throwIfNotFound()
    let content = ''

    content = `
      ${user.givenName} ${user.surname} in <i>${organisation.name}</i> has modified metadata in <i>${collectionTitle}</i> (bcms${alias}collect).
      <br/>
      Please review that these changes meet NCBI Bookshelf style guidelines and if necessary reload all documents to apply the changes.
      <br/>
      If you have any questions about this email or your preview, please contact the NCBI Bookshelf at booksauthors@ncbi.nlm.nih.gov.
      <br/>
      Thank you, 
      NCBI Bookshelf
      
    `

    const data = {
      content,
      subject: `${user.givenName} ${user.surname} in ${organisation.name} has modified metadata in ${collectionTitle} (bcms${alias}collect)`,
      to: flatten(emails).join(','),
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = collectionMetadataUpdatedEmail
