const { logger } = require('@coko/server')

const Organisation = require('../../../../models/organisation/organisation')
const User = require('../../../../models/user/user')

const { getOrganisationEmailsByTeam, sendEmail } = require('./_helpers')

const requestAccessToOrganisation = async context => {
  try {
    const { organisationId, userId } = context

    const orgAdminEmails = await getOrganisationEmailsByTeam(
      organisationId,
      'orgAdmin',
    )

    const editorEmails = await getOrganisationEmailsByTeam(
      organisationId,
      'editor',
    )

    const emails = [...orgAdminEmails, ...editorEmails]

    const user = await User.query().findById(userId).throwIfNotFound()

    const org = await Organisation.query()
      .findById(organisationId)
      .throwIfNotFound()

    const content = `
      User ${user.givenName} ${user.surname} has requested to join your organization ${org.name}.
    `

    const data = {
      content,
      subject: 'Request to join organization',
      to: emails.join(','),
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = requestAccessToOrganisation
