const { logger } = require('@coko/server')

const Organisation = require('../../../../models/organisation/organisation')

const {
  emailListWithoutCurrentUser,
  getOrganisationEmailsByTeam,
  sendEmail,
} = require('./_helpers')

const collectionCreatedEmail = async context => {
  try {
    const { collectionTitle, organisationId, userId } = context

    let emails = await getOrganisationEmailsByTeam(organisationId, 'orgAdmin')

    emails = await emailListWithoutCurrentUser(emails, userId)
    if (emails.length === 0) return

    const organisation = await Organisation.query()
      .findById(organisationId)
      .throwIfNotFound()

    const content = `
      A new collection named ${collectionTitle} has been created in your organization ${organisation.name}.
    `

    const data = {
      content,
      subject: 'New collection created',
      to: emails.join(','),
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = collectionCreatedEmail
