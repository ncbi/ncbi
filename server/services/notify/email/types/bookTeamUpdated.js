const capitalize = require('lodash/capitalize')

const { logger } = require('@coko/server')
const { Team } = require('@pubsweet/models')

const User = require('../../../../models/user/user')
const Book = require('../../../../models/book/book')

const { sendEmail } = require('./_helpers')

const verbMapper = {
  add: 'added',
  remove: 'removed',
}

const prepositionMapper = {
  add: 'to',
  remove: 'from',
}

const bookTeamUpdatedEmail = async context => {
  try {
    const { userId, teamId, type } = context
    const verb = verbMapper[type]
    const preposition = prepositionMapper[type]

    const team = await Team.query().findById(teamId).throwIfNotFound()
    const user = await User.query().findById(userId).throwIfNotFound()
    const book = await Book.query().findById(team.objectId).throwIfNotFound()

    const content = `
      You have been ${verb} ${preposition} the ${team.name.toLowerCase()} team of "${
      book.title
    }".
    `

    const subject = `${capitalize(verb)} ${preposition} book team`

    const data = {
      content,
      subject,
      to: user.email,
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = bookTeamUpdatedEmail
