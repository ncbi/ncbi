const { logger } = require('@coko/server')
const Organisation = require('../../../../models/organisation/organisation')
const { sendEmail } = require('./_helpers')

const makeContent = organisationName => {
  return `
    <span>
      Your request for membership in organization ${organisationName} has been rejected.
    </span>
  `
}

const rejectUsersEmail = async context => {
  try {
    const { organisationId, users } = context

    const organisation = await Organisation.query()
      .findById(organisationId)
      .throwIfNotFound()

    users.forEach(user => {
      const { email } = user

      const data = {
        content: makeContent(organisation.name),
        subject: 'Organization membership request rejection',
        to: email,
      }

      sendEmail(data)
    })
  } catch (e) {
    logger.error(e)
  }
}

module.exports = rejectUsersEmail
