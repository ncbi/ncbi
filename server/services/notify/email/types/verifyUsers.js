const { logger } = require('@coko/server')
const Organisation = require('../../../../models/organisation/organisation')
const { sendEmail } = require('./_helpers')

const makeContent = organisationName => {
  return `
    <span>
      Your request for membership in organization ${organisationName} has been accepted.
    </span>
  `
}

const verifyUsersEmail = async context => {
  try {
    const { organisationId, users } = context

    const organisation = await Organisation.query()
      .findById(organisationId)
      .throwIfNotFound()

    users.forEach(user => {
      const { email } = user

      const data = {
        content: makeContent(organisation.name),
        subject: 'Organization membership request acceptance',
        to: email,
      }

      sendEmail(data)
    })
  } catch (e) {
    logger.error(e)
  }
}

module.exports = verifyUsersEmail
