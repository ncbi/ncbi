const capitalize = require('lodash/capitalize')

const { logger } = require('@coko/server')

const {
  getObjectEmailsByTeam,
  getEmailsByUserId,
  sendEmail,
} = require('./_helpers')

const errorsEmail = async context => {
  try {
    const {
      bookComponentId,
      bookComponentTitle,
      errors,
      ownerId,
      type,
    } = context

    const createContent = errs => `
      The following errors occured during the ${type} of ${bookComponentTitle}:
      <ul>
        ${errs.map(err => `<li>${err.message}</li>`)}
      </ul>
    `

    const subject = `${capitalize(type)} errors`

    /* SEND NON-PMC ERRORS */
    const nonPMCErrors = errors.filter(e => e.assignee !== 'PMC')

    if (nonPMCErrors.length > 0) {
      const nonPMCContent = createContent(nonPMCErrors)
      const nonPMCEmails = await getEmailsByUserId(ownerId)

      const nonPMCData = {
        content: nonPMCContent,
        subject,
        to: nonPMCEmails,
      }

      sendEmail(nonPMCData)
    }

    /* SEND PMC ERRORS */

    const PMCErrors = errors.filter(e => e.assignee === 'PMC')

    if (PMCErrors.length > 0) {
      const PMCContent = createContent(PMCErrors)
      const PMCEmails = await getObjectEmailsByTeam(bookComponentId, 'sysAdmin')

      if (type === 'tagging') {
        PMCEmails.concat(
          await getObjectEmailsByTeam(bookComponentId, 'pdf2xmlVendor'),
        )
      }

      if (PMCEmails.length === 0) return

      const PMCData = {
        content: PMCContent,
        subject,
        to: PMCEmails,
      }

      sendEmail(PMCData)
    }
  } catch (e) {
    logger.error(e)
  }
}

module.exports = errorsEmail
