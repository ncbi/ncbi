const { logger } = require('@coko/server')

const {
  emailListWithoutCurrentUser,
  getObjectEmailsByTeam,
  sendEmail,
} = require('./_helpers')

const reviewApprovalEmail = async context => {
  try {
    const { objectId, objectTitle, userId, final } = context

    let emails = await getObjectEmailsByTeam(objectId, 'editor')
    emails = await emailListWithoutCurrentUser(emails, userId)
    if (emails.length === 0) return

    const starterText = final ? 'All previews have now' : 'A preview has'

    const content = `
      ${starterText} been approved for "${objectTitle}".
    `

    const data = {
      content,
      subject: 'Preview approval',
      to: emails.join(','),
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = reviewApprovalEmail
