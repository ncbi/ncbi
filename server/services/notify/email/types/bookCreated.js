const { logger } = require('@coko/server')

const Organisation = require('../../../../models/organisation/organisation')

const { getOrganisationEmailsByTeam, sendEmail } = require('./_helpers')

const bookCreatedEmail = async context => {
  try {
    const { bookTitle, organisationId, userId, alias } = context

    const emails = await getOrganisationEmailsByTeam(organisationId, 'orgAdmin')

    // emails = await emailListWithoutCurrentUser(emails, userId)
    // if (emails.length === 0) return

    const organisation = await Organisation.query()
      .findById(organisationId)
      .throwIfNotFound()

    let content = ''

    if (userId) {
      content = `
      A new book named <i>${bookTitle}</i> with bcms${alias} has been created in your organization <i>${organisation.name}</i>.
      <br/>
      You can search for this book in the BCMS by the BCMS ID and book title.
      <br/>
      If submitting content by FTP account, always use BCMS ID as the 'book-submit-id' in the meta.xml file.
    `
    } else {
      content = `
      A new book named <i>${bookTitle}</i> with bcms${alias} has been created in your organization <i>${organisation.name}</i>.
      <br/>
      You can search for this book in the BCMS by the BCMS ID and book title.
      <br/>
      All future FTP submissions to the book must include the same 'book-submit-id' in the meta.xml file.
    `
    }

    const data = {
      content,
      subject: 'New book created',
      to: emails.join(','),
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = bookCreatedEmail
