const { logger } = require('@coko/server')

const User = require('../../../../models/user/user')

const { sendEmail } = require('./_helpers')

const previewGeneratedEmail = async context => {
  try {
    const { bookComponentTitle, ownerId } = context

    const owner = await User.query().findById(ownerId).throwIfNotFound()

    const content = `
      A preview has been successfully generated for "${bookComponentTitle}".
    `

    const data = {
      content,
      subject: 'Preview generated',
      to: owner.email,
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = previewGeneratedEmail
