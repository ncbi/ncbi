const { logger } = require('@coko/server')
const { flatten } = require('lodash')
const Organisation = require('../../../../models/organisation/organisation')
const User = require('../../../../models/user/user')

const {
  getOrganisationEmailsByTeam,
  getObjectEmailsByTeam,
  sendEmail,
} = require('./_helpers')

const bookMetadataUpdatedEmail = async context => {
  try {
    const { bookTitle, organisationId, userId, alias, bookId } = context

    const emails = []
    emails.push(await getOrganisationEmailsByTeam(organisationId, 'sysAdmin'))
    emails.push(await getOrganisationEmailsByTeam(organisationId, 'orgAdmin'))
    emails.push(await getObjectEmailsByTeam(bookId, 'editor'))

    const organisation = await Organisation.query()
      .findById(organisationId)
      .throwIfNotFound()

    const user = await User.query().findById(userId).throwIfNotFound()
    let content = ''

    content = `
      ${user.givenName} ${user.surname} in <i>${organisation.name}</i> has modified metadata in <i>${bookTitle}</i> (bcms${alias}).
      <br/>
      Please review that these changes meet NCBI Bookshelf expectations for proper citations and if necessary reload all documents to apply the changes.
      <br/>
      If you have any questions about this email or your preview, please contact the NCBI Bookshelf at booksauthors@ncbi.nlm.nih.gov.
      <br/>
      Thank you, 
      NCBI Bookshelf
      
    `

    const data = {
      content,
      subject: `${user.givenName} ${user.surname} in ${organisation.name} has modified metadata in ${bookTitle} (bcms${alias})`,
      to: flatten(emails).join(','),
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = bookMetadataUpdatedEmail
