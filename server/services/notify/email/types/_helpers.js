const config = require('config')
const isEmpty = require('lodash/isEmpty')
const without = require('lodash/without')

const { sendEmail: send } = require('@coko/server')

const User = require('../../../../models/user/user')
const TeamMember = require('../../../../models/team/teamMember')

const from = config.has('mailer.from') && config.get('mailer.from')

const sendEmail = data => {
  const { content, subject, to } = data

  const emailData = {
    from,
    html: `<p>${content}</p>`,
    subject,
    text: content,
    to,
  }

  send(emailData)
}

// Accepts an array of emails as an argument and returns the same array, but
// without the current user. Useful if you don't want the users to trigger
// emails to themselves
const emailListWithoutCurrentUser = async (emails, currentUserId) => {
  const currentUser = await User.query().findById(currentUserId)
  return without(emails, currentUser.email)
}

const getObjectEmailsByTeam = async (objectId, role) => {
  const members = await TeamMember.query()
    .select(['userId', 'email', 'role', 'objectId'])
    .leftJoin('teams', 'team_id', 'teams.id')
    .leftJoin('users', 'user_id', 'users.id')
    .where({
      objectId,
      role,
    })

  if (!members) return []
  return members.map(user => user.email)
}

const getOrganisationEmailsByTeam = async (organisationId, role) => {
  return getObjectEmailsByTeam(organisationId, role)
}

// Can pass either a single id, or an array of multiple ids
const getEmailsByTeamId = async teamIds => {
  let arrayOfTeamIds = teamIds
  if (!Array.isArray(teamIds)) arrayOfTeamIds = [teamIds]

  const users = await User.query()
    .select(['users.id', 'users.email'])
    .leftJoin('team_members', 'team_members.user_id', 'users.id')
    .leftJoin('teams', 'team_members.team_id', 'teams.id')
    .whereIn('teams.id', arrayOfTeamIds)

  if (isEmpty(users)) return []
  return users.map(user => user.email)
}

// Can pass either a single id, or an array of multiple ids
const getEmailsByUserId = async userIds => {
  let arrayOfUserIds = userIds
  if (!Array.isArray(userIds)) arrayOfUserIds = [userIds]

  const users = await User.query()
    .select(['id', 'email'])
    .whereIn('id', arrayOfUserIds)

  if (isEmpty(users)) return []
  return users.map(user => user.email)
}

module.exports = {
  emailListWithoutCurrentUser,
  getEmailsByTeamId,
  getEmailsByUserId,
  getObjectEmailsByTeam,
  getOrganisationEmailsByTeam,
  sendEmail,
}
