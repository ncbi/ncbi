const isEmpty = require('lodash/isEmpty')
const uniq = require('lodash/uniq')

const { logger } = require('@coko/server')

const {
  emailListWithoutCurrentUser,
  getEmailsByTeamId,
  getEmailsByUserId,
  sendEmail,
} = require('./_helpers')

const mentionEmail = async context => {
  try {
    const { title, mentionedIds, userId } = context

    // no one to send an email to
    if (isEmpty(mentionedIds)) return

    // this will work if ids are teams, users or a combination of both
    const teamEmails = await getEmailsByTeamId(mentionedIds)
    const userEmails = await getEmailsByUserId(mentionedIds)

    let emails = uniq([...teamEmails, ...userEmails]) // remove duplicates
    emails = await emailListWithoutCurrentUser(emails, userId)
    if (emails.length === 0) return

    const content = `
      You were mentioned in the Vendor issue of "${title}".
    `

    const data = {
      content,
      subject: 'Chat mention',
      to: emails.join(','),
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = mentionEmail
