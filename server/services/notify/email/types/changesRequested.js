const { logger } = require('@coko/server')

const {
  emailListWithoutCurrentUser,
  getObjectEmailsByTeam,
  sendEmail,
} = require('./_helpers')

const changesRequestedEmail = async context => {
  try {
    const { objectId, objectTitle, userId } = context

    // get author emails
    let emails = await getObjectEmailsByTeam(objectId, 'author')
    emails = await emailListWithoutCurrentUser(emails, userId)
    if (emails.length === 0) return

    const content = `
      Changes have been requested for "${objectTitle}".
    `

    const data = {
      content,
      subject: 'Request for changes',
      to: emails.join(','),
    }

    sendEmail(data)
  } catch (e) {
    logger.error(e)
  }
}

module.exports = changesRequestedEmail
