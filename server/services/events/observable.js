/* eslint-disable global-require */
/* eslint-disable no-plusplus */
/* eslint-disable no-bitwise */
const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

class Observable {
  constructor() {
    this.observers = []
  }

  subscribe(observer) {
    this.observers = this.observers.concat(observer)
  }

  unsubscribe(observer) {
    const index = this.observers.indexOf(observer)

    if (~index) {
      this.observers.splice(index, 1)
    }
  }

  notifyObservers(message) {
    const Model = require('./listeners')

    for (let i = this.observers.length - 1; i >= 0; i--) {
      if (Model[this.observers[i]]) {
        const listener = new Model[this.observers[i]]()
        logger.info(`Start execution of Event ${listener.constructor.name}`)
        listener.handle(message)
      }
    }
  }
}

module.exports = Observable
