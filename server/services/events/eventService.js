/* eslint-disable no-shadow */
const config = require('config')
const Observable = require('./observable')

class EventService {
  constructor(config) {
    this.events = []

    config.forEach(event => {
      const observable = new Observable()
      observable.subscribe(event[Object.keys(event)[0]])

      this.events[Object.keys(event)[0]] = observable
    })
  }

  notify(event, data) {
    try {
      this.events[event].notifyObservers(data)
    } catch (e) {
      throw Error(`Could not find a registered event with that name: ${event}`)
    }
  }
}

module.exports = {
  model: new EventService(config.get('pubsweet-server.events')),
  modelName: 'Events',
}
