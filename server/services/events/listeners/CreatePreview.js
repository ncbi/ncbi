/* eslint-disable no-param-reassign */
/* eslint-disable no-return-await */
/* eslint-disable class-methods-use-this */

const {
  logger: { getRawLogger },
  useTransaction,
} = require('@coko/server')

const { pubsubManager } = require('pubsweet-server')
const ContentProccess = require('../../ContentProccess')

const { getPubsub } = pubsubManager

const CONVERSION_COMPLETED = 'CONVERSION_COMPLETED'
const logger = getRawLogger()
class CreatePreview {
  async handle({ object, notification, hasError }) {
    if (!hasError) {
      useTransaction(async transaction => {
        logger.info(`kafka Start Preview job id: ${notification.data.id}`)

        const { command: Command } = await ContentProccess.findBestContent(
          object,
        )

        try {
          const content = new Command({
            object,
            transaction,
            publishType: 'preview',
          })

          const updateBookComponent = await content.executeCommand(
            'ingestToPMC',
          )

          const pubsub = await getPubsub()

          pubsub.publish(
            `${CONVERSION_COMPLETED}.${updateBookComponent.bookId}`,
            {
              conversionCompleted: {
                id: updateBookComponent.id,
                title: updateBookComponent.title,
                status: updateBookComponent.status,
                updated: updateBookComponent.updated,
                bookId: updateBookComponent.bookId,
                divisionId: updateBookComponent.divisionId,
                bookComponentVersionId:
                  updateBookComponent.bookComponentVersionId,
                componentType: updateBookComponent.componentType,
                metadata: {
                  chapter_number: updateBookComponent.metadata.chapter_number,
                  filename: updateBookComponent.metadata.filename,
                },
              },
            },
          )
        } catch (e) {
          logger.error(`failed to Create Preview for ${object.id}`)
        }
      })
    }
  }
}

module.exports = CreatePreview
