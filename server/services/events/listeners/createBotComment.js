/* eslint-disable no-param-reassign */
/* eslint-disable no-return-await */
/* eslint-disable class-methods-use-this */

const { Channel, Message } = require('@pubsweet/models')

const { pubsubManager } = require('pubsweet-server')

const { getPubsub } = pubsubManager

const MESSAGE_CREATED = 'MESSAGE_CREATED'

const getContentFromStatus = bookComponentVersion => {
  const status = {
    preview: `${bookComponentVersion.metadata.filename} (V${bookComponentVersion.versionName}) preview generated`,
    published: 'Published',
    publishing: 'Publishing',
  }

  return status[bookComponentVersion.status]
}

class CreateBotComment {
  async handle({ user, data }) {
    const pubsub = await getPubsub()

    const channels = await Channel.query().whereIn(
      'objectId',
      data.map(bc => bc.id),
    )

    const bookComponents = data.map(bc => {
      bc.channels = channels.find(
        ch => ch.objectId === bc.id && ch.topic === 'feedback-channel',
      )

      return bc
    })

    await Promise.all(
      bookComponents.map(async bc => {
        if (bc.channels) {
          const savedMessage = await new Message({
            channelId: bc.channels.id,
            content: getContentFromStatus(bc),
            files: [],
            type: 'bot',
            userId: user,
          }).save()

          const message = await Message.query()
            .findById(savedMessage.id)
            .eager('user')

          pubsub.publish(`${MESSAGE_CREATED}.${bc.channels.id}`, message.id)

          return savedMessage
        }

        return bc
      }),
    )

    return true
  }
}

module.exports = CreateBotComment
