/* eslint-disable no-param-reassign */
/* eslint-disable no-return-await */
/* eslint-disable class-methods-use-this */
const {
  logger: { getRawLogger },
  useTransaction,
} = require('@coko/server')

const ContentProccess = require('../../ContentProccess')

const logger = getRawLogger()
class Publish {
  async handle({ object }) {
    const contentProccess = await ContentProccess.findBestContent(object)

    const {
      settings: { approvalOrgAdminEditor, approvalPreviewer },
    } = await contentProccess.getBook()

    if (object.type === 'toc') return

    if (
      !(approvalOrgAdminEditor || approvalPreviewer) &&
      object.status !== 'published'
    ) {
      useTransaction(async transaction => {
        logger.info(`kafka Start Publishing Automatically`)

        const Command = contentProccess.command

        try {
          const chapter = new Command({
            id: object.id,
            userId: object.ownerId,
            transaction,
            manual: false,
          })

          await chapter.executeCommand('publishComponent')
        } catch (e) {
          throw new Error('Automatically Publish failed')
        }
      }).catch(e => {
        logger.error(e)
      })
    }
  }
}

module.exports = Publish
