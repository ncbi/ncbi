/* eslint-disable no-await-in-loop */
/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
/* eslint-disable no-return-await */
/* eslint-disable class-methods-use-this */

const {
  logger: { getRawLogger },
  useTransaction,
} = require('@coko/server')

const File = require('../../../models/file/file')

const PackageCollection = require('../../ncbiConnectionManager/packageCollection')

const logger = getRawLogger()
class CoverLoadToPMC {
  async handle({ domains, cover, bcmsDomains }) {
    if (cover && cover instanceof File) {
      logger.info(`Cover Load to PMC starting with ${domains}`)
      logger.info(`We need to load ${domains.length} covers`)

      for (let i = 0, len = domains.length; i < len; i++) {
        logger.info(
          `Cover Start loading to PMC with id ${domains[i]} and objectId ${bcmsDomains[i]}`,
        )

        await useTransaction(async transaction => {
          await PackageCollection.sendForConversion(
            'coverPreview',
            [cover],
            {
              domain: bcmsDomains[i],
              id: bcmsDomains[i],
            },
            {
              objectId: domains[i],
            },
            transaction,
          )
        })
      }
    } else {
      logger.info(`No cover supplied to CoverLoadToPMC - doing nothing`)
    }
  }
}

module.exports = CoverLoadToPMC
