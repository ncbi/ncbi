/* eslint-disable no-plusplus */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-return-await */
/* eslint-disable class-methods-use-this */

const {
  logger: { getRawLogger },
} = require('@coko/server')

const Book = require('../../../models/book/book')
const Collection = require('../../../models/collection/collection')
const OrderService = require('../../orderService/orderService')

const logger = getRawLogger()
class OrderCollection {
  async handle({ domains }) {
    logger.info(`OrderCollection starting with ${domains}`)

    for (let i = 0, len = domains.length; i < len; i++) {
      const book = await Book.query().findOne({ id: domains[i] })

      if (book && book.collectionId) {
        logger.info(`Order Collection for book: ${book.id}`)

        const collection = await Collection.query().findOne({
          id: book.collectionId,
        })

        await OrderService.model(collection, {
          exclude: itm => itm.settings.isCollectionGroup,
          deep: true,
        })
      }
    }

    return true
  }
}

module.exports = OrderCollection
