/* eslint-disable no-plusplus */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-return-await */
/* eslint-disable class-methods-use-this */

const {
  logger: { getRawLogger },
} = require('@coko/server')

const { XmlFactory } = require('../..')
const Toc = require('../../../models/toc/toc')
const Book = require('../../../models/book/book')
const Collection = require('../../../models/collection/collection')
const FileService = require('../../file/fileService')

const logger = getRawLogger()
class CreateTocXml {
  async handle({ domains }) {
    logger.info(`CreateTocXml starting with ${domains}`)

    for (let i = 0, len = domains.length; i < len; i++) {
      logger.info(`Create Toc for book: ${domains[i]}`)

      const book = await Book.query().findOne({ id: domains[i] })

      const collection = await Collection.query().findOne({
        id: domains[i],
      })

      if (book) {
        await this.runTocBook(book)
      } else if (collection) {
        await this.runTocCollection(collection)
      } else {
        logger.info(`i cannot find the Model to create a toc.`)
      }
    }

    return true
  }

  // Returns True if we should skip because there aren't any published components, or false if we shouldn't skip
  // Given we only return published components from the db, we can check if the array is zero length or not
  // but we need to handle "parts" which have containers of components in attribute "secondLevel"
  recursiveCheckForPublishedComponents(bcArray = []) {
    if (bcArray.length === 0) return true
    return bcArray.map(bc => {
      if (bc.componentType === 'part')
        return this.recursiveCheckForPublishedComponents(bc.secondLevel)
      if (bc.metadata.hideInTOC === true) return true
      return false
    })
  }

  async runTocBook(book) {
    // We just want to load any non-part published book component
    // to check there is at least one, before creating the TOC.
    const divisions = await book.getBookComponentsByDivision({
      statusfilter: ['published'],
      group_chapters_in_parts: book.settings.toc.group_chapters_in_parts,
    })

    // Make sure the book has at least one published component
    const skip = divisions
      .map(div => {
        return this.recursiveCheckForPublishedComponents(div.bookComponents)
      })
      .flat(3)
      .every(s => s === true)

    if (skip === false) {
      logger.info(`Writing Toc for book: ${book.id}`)

      const content = await XmlFactory.createTocXml(book.id)

      const tocComponent = await Toc.query().findOne({ bookId: book.id })

      const file = await FileService.write({
        content,
        filename: 'TOC.xml',
        category: 'converted',
      })

      if (tocComponent) {
        logger.info(
          `File is added: ${file.name} on category converted on Toc ${tocComponent.id}, bcms${tocComponent.alias} by user: ${tocComponent.ownerId}`,
        )

        await tocComponent.addFile(
          { ...file, ownerId: tocComponent.ownerId },
          'converted',
          'loading-preview',
        )
      }
    } else {
      logger.info(`Skipping Toc for book: ${book.id}`)
    }
  }

  async runTocCollection(collection) {
    logger.info(`Writing Toc for collection: ${collection.id}`)

    const content = await XmlFactory.createCollectionXml(collection)

    const tocComponent = await Toc.query().findOne({
      collectionId: collection.id,
    })

    const file = await FileService.write({
      content,
      filename: `${collection.domain}.xml`,
      category: 'converted',
    })

    if (tocComponent) {
      logger.info(
        `File is added: ${file.name} on category converted on Toc ${tocComponent.id}, bcms${tocComponent.alias} by user: ${tocComponent.ownerId}`,
      )

      await tocComponent.addFile(
        { ...file, ownerId: tocComponent.ownerId },
        'converted',
        'loading-preview',
      )
    }
  }
}

module.exports = CreateTocXml
