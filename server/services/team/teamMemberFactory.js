/* eslint-disable no-param-reassign */
/* eslint-disable no-return-await */
const pick = require('lodash/pick')
const isEmpty = require('lodash/isEmpty')

const { Team, TeamMember } = require('@pubsweet/models')

const SearchService = require('../search/searchService')

class TeamMemberFactory {
  /* 
  Factory Create Organisation Services
  Given an ObjectID and Role assigns users to this Role
  */
  static async assign(teamObject, users) {
    const teams = await Team.query().where(
      pick(teamObject, ['objectId', 'objectType']),
    )

    const teamIds = teams.map(team => team.id)
    const foundTeam = teams.find(team => team.role === teamObject.role)

    const join = [
      {
        innerJoin: [
          'teams',
          query => {
            query.on('teams.id', '=', 'team_members.team_id')
          },
        ],
      },
    ]

    const filter = [
      { whereIn: ['team_id', teamIds] },
      { where: ['status', 'enabled'] },
    ]

    const having = {
      query: 'bool_or(role = ?::varchar) IS NOT TRUE',
      value: [foundTeam.role],
    }

    const service = new SearchService(TeamMember, {
      filter,
      groupBy: 'user_id',
      having,
      join,
    })

    const filteredAssignedUsers = (await service.search(['user_id'])).map(
      filtered => filtered.userId,
    )

    const insertIds = isEmpty(users)
      ? filteredAssignedUsers
      : users.filter(userId => filteredAssignedUsers.includes(userId))

    const objectInsert = insertIds.map(userId => ({
      status: 'enabled',
      teamId: foundTeam.id,
      userId,
    }))

    await TeamMember.query().insert(objectInsert)
    return foundTeam
  }
}

module.exports = TeamMemberFactory
