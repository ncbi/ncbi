const { logger } = require('@coko/server')

const { domainRequest } = require('../../../axiosService')
const DomainTranslation = require('../../../domainTranslation/domainTranslation')
const NCBIError = require('../../../NCBIError')

const BASE_MESSAGE = 'Update domain template command:'

class CreateDomainTemplateCommand {
  /* eslint-disable-next-line class-methods-use-this */
  title() {
    return 'Update Template Command'
  }

  constructor({ data, transaction }) {
    this.data = data
    this.transaction = transaction
  }

  async run() {
    try {
      const { domainId, values } = this.data

      /**
       * GET DETAILS OF NEWLY CREATED DOMAIN
       */

      const { data: detailsData } = await domainRequest(
        `domains/${domainId}`,
        'GET',
      )

      if (detailsData.meta.status !== 'ok') {
        logger.error(detailsData.body)
        throw new NCBIError('Get domain details request failed')
      }

      const template = detailsData.body

      /**
       * UPDATE VALUES OF NEWLY CREATED DOMAIN WITH USER VALUES
       */

      const modifiedDomainData = DomainTranslation.createBookDomainUpdateObjectFromData(
        template,
        values,
      )

      const { data: updatedData } = await domainRequest(
        `domains/${domainId}/`,
        'PATCH',
        modifiedDomainData,
      )

      if (updatedData.meta && updatedData.meta.status !== 'ok') {
        logger.error(updatedData.body)
        throw new NCBIError('Update domain request failed')
      }
    } catch (error) {
      error.message = `${BASE_MESSAGE} ${error.message}`
      throw new NCBIError(error.message)
    }
  }
}

module.exports = CreateDomainTemplateCommand
