/* eslint-disable no-plusplus */
const { UserInputError } = require('apollo-server-express')
const config = require('config')

const pick = require('lodash/pick')
const omit = require('lodash/omit')
const merge = require('lodash/merge')

const { Collection, Organisation, Toc } = require('@pubsweet/models')
const { logger } = require('@coko/server')

const { domainRequest } = require('../../../axiosService')
const DomainTranslation = require('../../../domainTranslation/domainTranslation')

const notify = require('../../../notify')

const {
  makeRandomString,
  getNewAlias,
  getAliasWithoutVersion,
} = require('../../../../_helpers')

const NCBIError = require('../../../NCBIError')

class CreateCollectionCommand {
  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Collection Create Command'
  }

  constructor({ transaction, ...data }) {
    const hardcodedValues = {
      clone_from: 'a1aaageneric',
      metadata: {
        sourceType: 'Collection',
      },
    }

    this.data = merge({}, data, hardcodedValues)
    this.transaction = transaction
  }

  async run() {
    const { alias, id, title, organisationId } = await Collection.query(
      this.transaction,
    )
      .insert({
        ...omit(this.data, ['user', 'clone_from']),
        ownerId: this.data.user,
        alias: await getAliasWithoutVersion(),
      })
      .returning('*')

    await Toc.query(this.transaction).insertAndFetch({
      bookId: null,
      collectionId: id,
      status: 'unpublished',
      title: 'Table of contents',
      ownerId: this.data.user,
      alias: await getNewAlias({}),
    })

    const organisation = await Organisation.query(this.transaction).findOne({
      id: this.data.organisationId,
    })

    let domainName = `bcms${alias}collect`
    const useRandomString = config.get('useRandomStringForDomainNames')
    if (useRandomString) domainName = `${makeRandomString(10)}collect`

    const { status, data } = await domainRequest('domains/', 'POST', {
      publisher: organisation.abbreviation,
      domain: domainName,
      ...pick(this.data, ['title', 'clone_from']),
    })

    if (status === 201) {
      const bookCollectionNamePatch = DomainTranslation.createBookCollectionNameUpdateObject(
        data.body,
      )

      const { data: updatedData } = await domainRequest(
        `domains/${data.body.id}/`,
        'PATCH',
        bookCollectionNamePatch,
      )

      if (updatedData.meta && updatedData.meta.status !== 'ok') {
        logger.error(updatedData.body)
        throw new NCBIError('Update domain request failed')
      }

      const collection = await Collection.query(this.transaction)
        .patch({ domainId: `${data.body.id}`, domain: domainName })
        .findOne({ id })
        .returning('*')

      notify('collectionCreated', {
        collectionTitle: title,
        organisationId,
        userId: this.data.user,
      })

      return collection
    }

    if (status === 400 || status === 404) {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(data.body))
      throw new UserInputError('Book invalid User Input', data.body)
    }

    if (status >= 500) {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(data.body))
      throw new NCBIError('Creating ncbi Book Request failed ', data.body)
    }

    throw new NCBIError('Could not create domain')
  }
}

module.exports = CreateCollectionCommand
