const pick = require('lodash/pick')

const { Book, FileVersion, File } = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

class OverwriteCommand {
  constructor({
    object,
    fileId,
    ownerId,
    filename,
    latestBookFile,
    transaction,
  }) {
    // save this for the validation
    this.previousStatus = object.status

    this.data = {
      ...object,
      status: 'new-upload',
      metadata: {
        ...pick(object, ['metadata']).metadata,
        fileId,
        filename,
      },
      ownerId,
    }

    this.latestBookFile = latestBookFile
    this.oldFileId = object.metadata.fileId

    this.transaction = transaction
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Book Overwrite Command'
  }

  async validate() {
    const [sourceFile] = await Book.latestComponentFiles(
      this.data,
      'source',
      this.transaction,
    )

    const [convertedFile] = await Book.latestComponentFiles(
      this.data,
      'converted',
      this.transaction,
    )

    const result = !(
      ((sourceFile && sourceFile.status === 'loading-preview') ||
        (sourceFile && sourceFile.status === 'converting') ||
        (convertedFile && convertedFile.status === 'loading-preview') ||
        (convertedFile && convertedFile.status === 'converting')) &&
      this.previousStatus === 'in-review'
    )

    if (!result)
      throw new Error(
        `Validation Failure for command: ${this.title()}. Cannot overwrite the book as it is either processing or under review.`,
      )

    return result
  }

  async run() {
    const updatedBook = await Book.query(this.transaction).patchAndFetchById(
      this.data.id,
      {
        ...this.data,
        publishedDate: this.data.publishedDate
          ? new Date(this.data.publishedDate).toISOString()
          : null,
      },
    )

    await FileVersion.query(this.transaction)
      .patch({
        created: new Date().toISOString(),
        fileId: updatedBook.metadata.fileId,
        ownerId: this.data.ownerId,
      })
      .findOne({ id: this.latestBookFile[0].id })

    await File.query(this.transaction)
      .delete()
      .findOne({ id: this.latestBookFile[0].fileId })

    const { id, ownerId, metadata, domain, alias } = updatedBook

    const user = ownerId || '"comes from FTP Submission"'

    logger.info(
      `User Id: ${user}  overwrited successfully the file ${metadata.filename} for chapter id : ${id}, bcms${alias}, ${domain} `,
    )

    logger.info(
      `New file id: ${metadata.fileId}, Old file id: ${this.latestBookFile[0].fileId}`,
    )

    return updatedBook
  }
}

module.exports = OverwriteCommand
