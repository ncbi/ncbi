const CreateCommand = require('./createCommand')
const CreateCollectionCommand = require('./createCollectionCommand')
const CreateDomainTemplate = require('./createDomainTemplate')
const GetDetails = require('./getDetails')
const SendTocPMC = require('./sendTocPMC')
const UpdateCollectionCommand = require('./updateCollectionCommand')
const UpdateCommand = require('./updateCommand')
const UpdateDomainTemplate = require('./updateDomainTemplate')
const NewVersion = require('./newVersion')
const UpdateStatus = require('./updateStatus')
const IncreaseFileVersion = require('./increaseFileVersion')
const OverwriteComponent = require('./overwriteComponent')
const PublishComponent = require('./publishComponent')
const IngestToPMC = require('./ingestToPMC')
const SubmitComponent = require('./submitComponent')

module.exports = {
  CreateCommand,
  CreateCollectionCommand,
  CreateDomainTemplate,
  GetDetails,
  SendTocPMC,
  UpdateCollectionCommand,
  UpdateCommand,
  UpdateDomainTemplate,
  NewVersion,
  UpdateStatus,
  IncreaseFileVersion,
  OverwriteComponent,
  PublishComponent,
  IngestToPMC,
  SubmitComponent,
}
