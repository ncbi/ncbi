const { UserInputError } = require('apollo-server-express')
const Models = require('@pubsweet/models')

const { domainRequest } = require('../../../axiosService')
const NCBIError = require('../../../NCBIError')

class UpdateCommand {
  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Domain GetDetails Command'
  }

  constructor(data) {
    this.data = data
    this.transaction = data.transaction
  }

  async run() {
    const model = await Models[this.data.entity]
      .query(this.transaction)
      .findOne({
        id: this.data.id,
      })

    const domainId = this.data.domainId ? this.data.domainId : model.domainId

    const { status, data } = await domainRequest(`domains/${domainId}`, 'GET')

    if (status === 200) {
      return {
        domain: data.body,
        model,
      }
    }

    if (status === 401) {
      throw new NCBIError('Unathorized Access')
    }

    if (status === 404) {
      throw new UserInputError("Domain couldn't be found", data.body)
    }

    if (status === 500) {
      throw new NCBIError('Get ncbi Domain Request failed ', data.body)
    }

    return false
  }
}

module.exports = UpdateCommand
