const { Book, Collection, TeamMember } = require('@pubsweet/models')
const pick = require('lodash/pick')
const cloneDeep = require('lodash/cloneDeep')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const { getNewAlias } = require('../../../../_helpers')
const notify = require('../../../notify')

class NewVersion {
  constructor({ transaction, user, ...data }) {
    this.data = {
      ...data,
    }

    this.user = user
    this.transaction = transaction
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Book Version Create Command'
  }

  // eslint-disable-next-line class-methods-use-this
  async run() {
    const { bookId, workflow, fundedContentType, version } = this.data

    const bookCurrVer = await Book.query(this.transaction).findOne({
      id: bookId,
    })

    const bookNewVerData = {
      ...pick(bookCurrVer, [
        'title',
        'workflow',
        'fundedContentType',
        'organisationId',
        'collectionId',
        'domainId',
        'domain',
        'bookSubmitId',
        'abstract',
        'abstactTitle',
        'subTitle',
        'collectionId',
        'altTitle,',
        'metadata',
        'settings',
        'alias',
      ]),
      version,
      workflow,
      fundedContentType,
      status: 'new-version',
      parentId: bookCurrVer.parentId || bookCurrVer.id,
    }

    const latestBookVersion = await bookCurrVer.getLatestBookVersion(
      this.transaction,
    )

    const bookNewVer = await Book.create({
      ...bookNewVerData,
      status: 'new-version',
      ownerId: this.user,
      transaction: this.transaction,
      alias: await getNewAlias({ from: latestBookVersion.alias }),
    })

    logger.info(
      `New Version of Book with id: ${bookNewVer.id}, ${bookNewVer.alias}, ${bookNewVer.domain} is Created. with Version: ${bookNewVer.versionName}`,
    )

    await this.addNewVersionToCollection(bookNewVer, bookCurrVer)
    // await this.removeOldVersionFromCollection(bookCurrVer) // keeping it here in case we need it in future
    await this.assignCurrentUserAsEditor(bookNewVer)

    return bookNewVer
  }

  // Remove the old book version from the collection it was part of.
  async removeOldVersionFromCollection(oldVersion) {
    if (oldVersion.collectionId) {
      const collection = await Collection.query(this.transaction).findOne({
        id: oldVersion.collectionId,
      })

      const newComponents = cloneDeep(collection.components) || []
      const oldVerIndex = newComponents.findIndex(x => x === oldVersion.id)
      newComponents.splice(oldVerIndex, 1)

      await Collection.query(this.transaction)
        .patch({
          components: newComponents,
        })
        .where('id', collection.id)
    }
  }

  async addNewVersionToCollection(newVersion, previousVersion) {
    if (previousVersion.collectionId) {
      await Collection.addBookToCollection(
        {
          collectionId: previousVersion.collectionId,
          bookId: newVersion.id,
        },
        this.transaction,
      )
    }
  }

  async validate() {
    const {
      bookId,
      fundedContentType: requestedVerName,
      version: requestedVerNumber,
    } = this.data

    const book = await Book.query(this.transaction).findOne({
      id: bookId,
    })

    const requestedVersionExists = await book.getVersion(
      requestedVerName,
      requestedVerNumber,
      this.transaction,
    )

    if (requestedVersionExists) {
      logger.info(
        `Validation Error: A book with the requested version already exist: ${requestedVersionExists.id}`,
      )

      throw new Error(
        `Validation Error: A book with the requested version already exist!`,
      )
    }

    return true
  }

  async assignCurrentUserAsEditor(book) {
    const teamEditor = book.teams.find(team => team.role === 'editor')

    if (this.user) {
      await TeamMember.query(this.transaction).insert({
        status: 'enabled',
        teamId: teamEditor.id,
        userId: this.user,
      })

      notify('bookVersionCreated', {
        bookTitle: book.title,
        organisationId: book.organisationId,
        userId: this.user,
        alias: book.alias,
        versionNumber: book.version,
        versionName: book.fundedContentType,
      })
    }
  }
}

module.exports = NewVersion
