/* eslint-disable no-param-reassign */
const { UserInputError } = require('apollo-server-express')
const omit = require('lodash/omit')
const merge = require('lodash/merge')
const mergeWith = require('lodash/mergeWith')
const cloneDeep = require('lodash/cloneDeep')
const isEmpty = require('lodash/isEmpty')

const { Collection, File } = require('@pubsweet/models')
const db = require('@pubsweet/db-manager/src/db')

const FileService = require('../../../file/fileService')
const DomainTranslation = require('../../../domainTranslation/domainTranslation')
const OrderService = require('../../../orderService/orderService')

const { domainRequest } = require('../../../axiosService')

const notify = require('../../../notify')
const NCBIError = require('../../../NCBIError')

class UpdateCommand {
  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Domain Collection Update Command'
  }

  constructor(data) {
    this.id = data.id

    this.data = omit(data.input, ['fileCover', 'transaction'])
    this.fileCover = data.input && data.input.fileCover
    this.ncbiData = data.ncbiData
    this.transaction = data.transaction
    this.user = data.user
  }

  async run() {
    // eslint-disable-next-line global-require
    const Domain = require('../domain')

    const ncbiDomain = new Domain({
      entity: 'Collection',
      id: this.id,
      transaction: this.transaction,
    })

    const { domain, model } = await ncbiDomain.executeCommand('getDetails')

    const { status, data } = await domainRequest(
      `domains/${model.domainId}/`,
      'PATCH',
      this.update(domain),
    )

    if (status === 204 && !isEmpty(this.data)) {
      this.data = await FileService.updateCover(
        this.fileCover,
        'cover',
        this.data,
        'fileCover',
      )

      const { settings, metadata, ...otherFields } = this.data

      const patchData = {
        ...otherFields,
        settings: merge(cloneDeep(model.settings), this.data.settings),
        metadata: mergeWith(
          {},
          model.metadata,
          this.data.metadata,
          // merge objects, but replace arrays with incoming
          (_, newValue) => (Array.isArray(newValue) ? newValue : undefined),
        ),
      }

      const updatedCollection = await Collection.query(
        this.transaction,
      ).patchAndFetchById(this.id, patchData)

      // These settings need to apply to all books that belong to the collection
      // If you edit which settings do that, make sure to also update the path
      // where a book is added to a collection, as it should inherit the collection's settings
      const settingsThatApplyToAllCollectionBooks = ['approvalOrgAdminEditor']

      await Promise.all(
        settingsThatApplyToAllCollectionBooks.map(async setting => {
          const originalValue = model.settings[setting]
          const newValue = updatedCollection.settings[setting]

          let updateQuery = `
            UPDATE books
            SET settings = jsonb_set(settings, '{${setting}}', '${newValue}'::jsonb)
            WHERE collection_id = '${this.id}'
          `

          if (setting === 'approvalOrgAdminEditor') {
            updateQuery += ` AND settings @> '{ "chapterIndependently": false }'`
          }

          if (originalValue !== newValue) {
            await db.raw(updateQuery)
          }
        }),
      )

      // check and update if needed
      await this.updateComponentsOrder(updatedCollection, model)

      // check and delete if needed
      await this.deleteBookCover(updatedCollection, model)

      const collection = await Collection.query(this.transaction).findOne({
        id: model.id,
      })

      if (collection) {
        // The MetadataForm doesn't set the 'settings' dictionary in the incoming data
        // We don't want to send an email if the user changed "Settings" , only "Metadata" changes should get an email.

        if (settings === undefined) {
          notify('collectionMetadataUpdated', {
            collectionTitle: collection.title,
            organisationId: collection.organisationId,
            userId: this.user,
            alias: collection.alias,
          })
        }
      }

      const cover = collection.fileCover
        ? await File.query(this.transaction).findOne({
            id: collection.fileCover,
          })
        : null

      return { collection, cover }
    }

    if (status === 400) {
      throw new UserInputError('Book invalid User Input', data.body)
    }

    if (status === 401) {
      throw new NCBIError('Unathorized Access')
    }

    if (status === 500) {
      throw new NCBIError('Creating ncbi Book Request failed', data.body)
    }

    throw new NCBIError('Could not update Collection domain')
  }

  update(domain) {
    return DomainTranslation.createCollectionDomainUpdateObjectFromData(
      domain,
      this.data,
    )
  }

  async updateComponentsOrder(restUpdate, col) {
    if (
      col.settings.orderBooksBy &&
      restUpdate.settings.orderBooksBy &&
      restUpdate.settings.orderBooksBy !== col.settings.orderBooksBy
    ) {
      await OrderService.model(col, {
        transaction: this.transaction,
        exclude: itm => itm.settings.isCollectionGroup,
        deep: true,
      })
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async deleteBookCover(restUpdate, book) {
    // remove image from Files if null
    // use relations to delete automatically
    if (restUpdate.fileCover === null && book.fileCover !== null) {
      await File.query(this.transaction).deleteById(book.fileCover)
    }

    if (restUpdate.fileAbstract === null && book.fileAbstract !== null) {
      await File.query(this.transaction).deleteById(book.fileAbstract)
    }
  }
}

module.exports = UpdateCommand
