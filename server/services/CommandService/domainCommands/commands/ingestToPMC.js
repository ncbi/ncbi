/* eslint-disable no-return-await */
/* eslint-disable no-await-in-loop */
const { Book, FileVersion, SourceConvertedFile } = require('@pubsweet/models')

const PackageCollection = require('../../../ncbiConnectionManager/packageCollection')
const NCBIError = require('../../../NCBIError')

class IngestToPMC {
  constructor(data) {
    this.type = data.publishType

    this.object = data.object
    this.transaction = data.transaction

    // Check issue https://gitlab.coko.foundation/ncbi/ncbi/-/issues/435
    // For database selection and release flag
    if (data.publishType === 'preview') {
      this.options = {
        release: false,
        target_database: 'prod',
      }
    } else if (data.publishType === 'preview-published') {
      this.options = {
        release: false,
        target_database: 'preview',
      }
    } else if (data.publishType === 'publish') {
      this.options = {
        release: true,
        target_database: 'prod',
      }
    }

    this.currentUser = data.currentUser || data.object.ownerId
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return `Book Ingest To PMC Command. Book id : ${this.object.id}, bcms${this.object.alias}, ${this.object.domain} from User: ${this.currentUser}`
  }

  async run() {
    const filesValue = await Book.latestComponentFiles(
      this.object,
      null,
      this.transaction,
    )

    const book = await Book.query(this.transaction).findOne({
      id: this.object.id,
    })

    const files = filesValue.map(obj =>
      obj.tag === 'book_pdf'
        ? {
            ...obj,
            name: `${book.domain}.pdf`,
            file: { ...obj.file, name: `${book.domain}.pdf` },
          }
        : obj,
    )

    this.convertedXml = files.find(file => file.category === 'converted')

    if (!this.convertedXml) {
      throw new NCBIError(
        `There is no Converted File for Loading Preview. Book ${this.object.id}`,
      )
    }

    const bookWithAppliedValues = await book.getBookWithAppliedCollectionValues()

    let packageFiles = files
      .filter(
        file => file.category !== 'converted' && file.category !== 'review',
      )
      .concat([this.convertedXml])
      .map(({ file, versionName }) => ({
        ...file,
        versionName,
      }))

    // Get the related book abstract File object

    const fileAbstract = await book.getFileAbstract()

    if (fileAbstract.abstractCover) {
      packageFiles = packageFiles.concat([
        {
          ...fileAbstract.abstractCover,
          versionName: '1',
        },
      ])
    }

    await PackageCollection.sendForConversion(
      'wholeBookPreview',
      packageFiles,
      {
        domain: book.domain,
        book: bookWithAppliedValues,
        ...this.options,
      },
      {
        objectId: this.object.id,
      },
      this.transaction,
    )

    if (this.type === 'preview' || this.type === 'preview-published') {
      const result = await this.updateStatus('loading-preview')
      return result
    }

    return this.bookComponent
  }

  async updateStatus(status) {
    const sourceFiles = await SourceConvertedFile.query(this.transaction).where(
      {
        convertedId: this.convertedXml.id,
      },
    )

    let filesUpdate = (sourceFiles || []).map(f => f.sourceId)

    filesUpdate = filesUpdate.concat([this.convertedXml.id])

    // Update BookComponent
    const book = await Book.query(this.transaction)
      .patch({ status })
      .findOne({
        id: this.object.id,
      })
      .returning('*')

    await FileVersion.query(this.transaction)
      .patch({ status })
      .whereIn('id', filesUpdate)

    if (book.collectionId) {
      const { collection } = await book
        .$query(this.optionstransaction)
        .joinEager('collection')

      await collection.updateCollectionStatus(this.transaction)
    }

    return book
  }
}

module.exports = IngestToPMC
