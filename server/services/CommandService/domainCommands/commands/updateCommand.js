const { UserInputError } = require('apollo-server-express')

const omit = require('lodash/omit')
const merge = require('lodash/merge')
const mergeWith = require('lodash/mergeWith')
const cloneDeep = require('lodash/cloneDeep')
const isEmpty = require('lodash/isEmpty')

const {
  Book,
  Division,
  Collection,
  File,
  Organisation,
} = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const DomainTranslation = require('../../../domainTranslation/domainTranslation')
const { domainRequest } = require('../../../axiosService')
const { model: Events } = require('../../../events/eventService')
const FileService = require('../../../file/fileService')
const notify = require('../../../notify')

const OrderService = require('../../../orderService/orderService')
const NCBIError = require('../../../NCBIError')

class UpdateCommand {
  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Domain Book Update Command'
  }

  constructor(data) {
    this.id = data.id

    this.data = omit(data.input, ['fileCover', 'fileAbstract', 'transaction'])
    this.fileCover = data.input && data.input.fileCover
    this.fileAbstract = data.input && data.input.fileAbstract
    this.transaction = data.transaction
    this.user = data.user
  }

  async run() {
    // eslint-disable-next-line global-require
    const Domain = require('../domain')

    const domain = new Domain({
      entity: 'Book',
      id: this.id,
      transaction: this.transaction,
      domainId: this.data.domainId,
    })

    let getDetails = {}

    try {
      getDetails = await domain.executeCommand('getDetails')
    } catch (e) {
      throw new Error(e)
    }

    const ncbiBook = getDetails.domain
    const book = getDetails.model

    const updateObject = await this.update(ncbiBook)

    const domainId = this.data.domainId ? this.data.domainId : book.domainId

    const { status, data } = await domainRequest(
      `domains/${domainId}/`,
      'PATCH',
      updateObject,
    )

    if (status === 204 && !isEmpty(this.data)) {
      logger.info(
        `Remote Ncbi Domain with domain : ${domainId} was updated successfully with data : ${JSON.stringify(
          updateObject,
        )} from user id: ${this.user}`,
      )

      this.data = await FileService.updateCover(
        this.fileCover,
        'cover',
        this.data,
        'fileCover',
      )

      this.data = await FileService.updateCover(
        this.fileAbstract,
        'abstract',
        this.data,
        'fileAbstract',
      )

      const patchData = {
        ...this.data,
        settings: merge(cloneDeep(book.settings), this.data.settings),
        metadata: mergeWith(
          {},
          book.metadata,
          this.data.metadata,
          // merge objects, but replace arrays with incoming
          (_, newValue) => (Array.isArray(newValue) ? newValue : undefined),
        ),
      }

      if (this.data.publishedDate) {
        patchData.publishedDate = this.data.publishedDate
          ? new Date(this.data.publishedDate).toISOString()
          : null
      }

      const updatedBook = await Book.query(this.transaction).patchAndFetchById(
        this.id,
        patchData,
      )

      logger.info(
        `Book id: ${updatedBook.id}, bcms${
          updatedBook.alias
        } was updated successfully with data : ${JSON.stringify(
          patchData,
        )} from user ${this.user}`,
      )

      // handle book belonging to collection
      // We cannot pass the updatedBook here as there can be a case that
      // updatedBook has the ID of a collection to which it is yet to be added.
      await this.updateBookCollection(book)

      // check and update if needed
      await this.updateComponentsOrder(updatedBook, book)

      // check and delete if needed
      await this.deleteBookCover(updatedBook, book)

      const bookResult = await Book.query(this.transaction).findOne({
        id: book.id,
      })

      if (updatedBook) {
        // The MetadataForm doesn't set the 'settings' dictionary in the incoming data
        // We don't want to send an email if the user changed "Settings" , only "Metadata" changes should get an email.

        if (this.data.settings === undefined) {
          notify('bookMetadataUpdated', {
            bookTitle: updatedBook.title,
            organisationId: updatedBook.organisationId,
            userId: this.user,
            alias: updatedBook.alias,
            bookId: updatedBook.id,
          })
        }

        Events.notify('BookUpdated', {
          domains: [updatedBook.id],
          cover: this.data.fileCover
            ? await File.query(this.transaction).findOne({
                id: this.data.fileCover,
              })
            : null,
          bcmsDomains: [updatedBook.domain],
        })
      }

      // eslint-disable-next-line no-return-await
      return await bookResult.getFile(this.transaction)
    }

    if (status === 204) {
      return true
    }

    if (status === 400) {
      throw new UserInputError('Book invalid User Input', data.body)
    }

    if (status === 401) {
      throw new NCBIError('Unathorized Access')
    }

    if (status === 500) {
      throw new NCBIError('Creating ncbi Book Request failed ', data.body)
    }

    throw new NCBIError('Could not update Book domain')
  }

  async update(ncbiBook) {
    const template = ncbiBook
    const values = cloneDeep(this.data)

    // publisher for the domain is the abbreviation, not our id
    if (values.settings && values.settings.publisher) {
      const publisherId = values.settings.publisher

      const publisherOrg = await Organisation.query(this.transaction).findById(
        publisherId,
      )

      values.settings.publisher = publisherOrg.abbreviation
    }

    const domainUpdateData = DomainTranslation.createBookDomainUpdateObjectFromData(
      template,
      values,
    )

    return domainUpdateData
  }

  async updateComponentsOrder(restUpdate, book) {
    if (
      book.settings.toc &&
      restUpdate.settings.toc &&
      restUpdate.settings.toc.order_chapters_by !==
        book.settings.toc.order_chapters_by
    ) {
      const division = await Division.query(this.transaction).findOne({
        bookId: this.id,
        label: 'body',
      })

      await OrderService.model(division, {
        transaction: this.transaction,
        exclude: itm => itm.componentType === 'part',
        deep: true,
      })
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async deleteBookCover(restUpdate, book) {
    // remove image from Files if null
    // use relations to delete automatically
    if (restUpdate.fileCover === null && book.fileCover !== null) {
      await File.query(this.transaction).deleteById(book.fileCover)
    }

    if (restUpdate.fileAbstract === null && book.fileAbstract !== null) {
      await File.query(this.transaction).deleteById(book.fileAbstract)
    }
  }

  async updateBookCollection(book) {
    const submittedCollectionId = this.data.collectionId
    const existingCollectionId = book.collectionId

    // If no collectionId is submitted, or if the submitted
    // collectionId is same as the existing collectionId, then
    // the book collection needs no update.
    if (
      submittedCollectionId === undefined ||
      submittedCollectionId === existingCollectionId
    ) {
      return
    }

    if (existingCollectionId !== null) {
      await Collection.removeBookFromCollection(
        {
          collectionId: existingCollectionId,
          bookId: this.id,
        },
        this.transaction,
      )
    }

    if (submittedCollectionId !== null) {
      const col = await Collection.addBookToCollection(
        {
          collectionId: submittedCollectionId,
          bookId: this.id,
        },
        this.transaction,
      )

      this.data.fileCover = col.metadata.applyCoverToBooks
        ? col.fileCover
        : this.data.fileCover
    }
  }
}

module.exports = UpdateCommand
