const { FileVersion, Book } = require('@pubsweet/models')

class SubmitComponent {
  constructor(data) {
    this.id = data.id
    this.transaction = data.transaction || null
    this.currentUser = data.currentUser || null
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return `Book Submit Command for book id: ${this.id}`
  }

  async run() {
    const book = await Book.query(this.transaction).findOne({
      id: this.id,
    })

    const status = book.workflow === 'pdf' ? 'tagging' : 'converting'

    await Book.query(this.transaction).patch({ status }).findOne({
      id: this.id,
    })

    const filesConverted = await Book.latestComponentFiles(
      book,
      'source',
      this.transaction,
    )

    await FileVersion.query(this.transaction)
      .patch({ status })
      .whereIn(
        'id',
        filesConverted.map(f => f.id),
      )

    if (book.collectionId) {
      const { collection } = await book
        .$query(this.transaction)
        .joinEager('collection')

      await collection.updateCollectionStatus(this.transaction)
    }
  }
}

module.exports = SubmitComponent
