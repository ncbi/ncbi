const config = require('config')
const snakeCase = require('lodash/snakeCase')
// const faker = require('faker') // For testing only

const { logger } = require('@coko/server')

const { domainRequest } = require('../../../axiosService')
const DomainTranslation = require('../../../domainTranslation/domainTranslation')
const NCBIError = require('../../../NCBIError')

const BASE_MESSAGE = 'Create domain template command:'

class CreateDomainTemplateCommand {
  /* eslint-disable-next-line class-methods-use-this */
  title() {
    return 'Create Template Command'
  }

  constructor({ data, transaction }) {
    this.data = data
    this.transaction = transaction
  }

  async run() {
    try {
      const {
        abbreviation,
        collectionDomainId,
        publisherId,
        templateType,
        values,
      } = this.data

      /**
       * CREATE A NEW DOMAIN
       */

      // 'chapter' or 'whole'
      const domainNameType = snakeCase(templateType).split('_')[0]

      const domainNameFirstPart = collectionDomainId
        ? `col_${collectionDomainId}`
        : `templ_${publisherId}`

      const domainName = `${domainNameFirstPart}_${domainNameType}`

      if (domainName.length > 20) {
        throw new NCBIError(
          `Domain name cannot be longer than 20 characters. Attempted value was ${domainName}.`,
        )
      }

      const cloneFrom = config.get(
        `bookSettings.domainTemplates.${templateType}.domain`,
      )

      const requestBody = {
        publisher: abbreviation,
        domain: domainName,
        title: domainName,
        clone_from: cloneFrom,
      }

      const { data } = await domainRequest('domains/', 'POST', requestBody)

      if (data.meta.status !== 'ok') {
        logger.error(data.body)
        throw new NCBIError('Create domain request failed')
      }

      /**
       * GET DETAILS OF NEWLY CREATED DOMAIN
       */

      const { id } = data.body
      const { data: detailsData } = await domainRequest(`domains/${id}`, 'GET')

      if (detailsData.meta.status !== 'ok') {
        logger.error(detailsData.body)
        throw new NCBIError('Get domain details request failed')
      }

      const template = detailsData.body

      /**
       * UPDATE VALUES OF NEWLY CREATED DOMAIN WITH USER VALUES
       */

      const modifiedDomainData = DomainTranslation.createBookDomainUpdateObjectFromData(
        template,
        values,
      )

      const { data: updatedData } = await domainRequest(
        `domains/${id}/`,
        'PATCH',
        modifiedDomainData,
      )

      if (updatedData.meta && updatedData.meta.status !== 'ok') {
        logger.error(updatedData.body)
        throw new NCBIError('Update domain request failed')
      }

      /**
       * RETURN DOMAIN ID AND NAME FOR STORING IN OUR DB
       */
      return {
        domainName: detailsData.body.domain,
        domainId: String(detailsData.body.id),
      }
    } catch (error) {
      error.message = `${BASE_MESSAGE} ${error.message}`
      throw new NCBIError(error.message)
    }
  }
}

module.exports = CreateDomainTemplateCommand
