/* eslint-disable no-await-in-loop */
const { Book, File, Toc, Collection } = require('@pubsweet/models')
const { useTransaction } = require('@coko/server')

const PackageCollection = require('../../../ncbiConnectionManager/packageCollection')

class SendTocPmcCommand {
  constructor({ toc }) {
    this.toc = toc || null
    this.tocs = []
    this.scheduled = !toc
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Send Toc PMC Command'
  }

  // eslint-disable-next-line class-methods-use-this
  async run() {
    if (this.toc) {
      this.tocs.push(this.toc)
    } else {
      this.tocs = await Toc.query().whereNotNull('book_id')
    }

    await this.sendToc()
  }

  async sendToc() {
    let updatedToc = []
    const { tocs } = this

    // Find TOC XML FILES
    // eslint-disable-next-line no-plusplus
    for (let i = 0, len = tocs.length; i < len; i++) {
      const [convertedFile] = await Toc.latestComponentFiles(
        tocs[i],
        'converted',
      )

      let fileAbstract = null

      if (convertedFile) {
        let tocEntity = {}

        if (tocs[i].bookId) {
          tocEntity = await Book.query().findOne({
            id: tocs[i].bookId,
          })

          // Get the related book abstract File object
          fileAbstract = await tocEntity.getFileAbstract()
        } else if (tocs[i].collectionId) {
          tocEntity = await Collection.query().findOne({
            id: tocs[i].collectionId,
          })
        }

        let files = []

        if (this.scheduled) {
          files = await File.query().whereRaw(
            "id = ? and updated > now() - interval '1 day' or id = ?",
            [convertedFile.fileId, (tocEntity.fileCover || {}).id || null],
          )
        } else {
          files = await File.query().whereIn('id', [
            convertedFile.fileId,
            (tocEntity.fileCover || {}).id || null,
          ])
        }

        if (files.length) {
          // check for adding a abstact graphic
          if (fileAbstract && fileAbstract.abstractCover) {
            files.push(fileAbstract.abstractCover)
          }

          updatedToc = updatedToc.concat({
            isCollection: tocEntity.type === 'collection',
            domain: tocEntity.domain,
            files,
            objectId: tocs[i].id,
          })
        }
      }
    }

    // send for preview the selected files
    useTransaction(async transaction => {
      // eslint-disable-next-line no-plusplus
      for (let i = 0, len = updatedToc.length; i < len; i++) {
        await PackageCollection.sendForConversion(
          'tocPreview',
          updatedToc[i].files,
          {
            isCollection: updatedToc[i].isCollection,
            domain: updatedToc[i].domain,
            release: true,
            target_database: 'prod',
          },
          {
            objectId: updatedToc[i].objectId,
          },
          transaction,
        )
      }
    })
  }
}

module.exports = SendTocPmcCommand
