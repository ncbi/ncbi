/* eslint-disable global-require */
const { FileVersion, SourceConvertedFile, Book } = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

class UpdateStatus {
  constructor(data) {
    this.object = data.object
    this.status = data.status
    this.transaction = data.transaction
    this.currentUser = data.currentUser
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Update Status of Book Command'
  }

  async run() {
    const [convertedFile] = await Book.latestComponentFiles(
      this.object,
      'converted',
      this.transaction,
    )

    let filesUpdate = []
    let sourceFiles = []

    if (convertedFile) {
      sourceFiles = await SourceConvertedFile.query(this.transaction).where({
        convertedId: convertedFile.id,
      })

      filesUpdate = (sourceFiles || []).map(f => f.sourceId)
    } else {
      sourceFiles = await Book.latestComponentFiles(
        this.object,
        'source',
        this.transaction,
      )

      filesUpdate = (sourceFiles || []).map(f => f.id)
    }

    if (convertedFile) {
      filesUpdate = filesUpdate.concat([convertedFile.id])
    }

    if (this.status === 'published') {
      await Book.query(this.transaction)
        .patch({
          status: this.status,
          publishedDate: new Date().toISOString(),
        })
        .findOne({ id: this.object.id })
    } else {
      await Book.query(this.transaction)
        .patch({
          status: this.status,
        })
        .findOne({ id: this.object.id })
    }

    logger.info(
      `Book ${this.object.id}, ${this.object.alias}, ${this.object.domain} has been updated successfully with status: ${this.status} by user id: ${this.currentUser}`,
    )

    await FileVersion.query(this.transaction)
      .patch({ status: this.status })
      .whereIn('id', filesUpdate)

    logger.info(
      `File status for ids: ${filesUpdate.join(',')} is updated to ${
        this.status
      } from user id ${this.currentUser}`,
    )

    const book = await Book.query(this.transaction).findOne({
      id: this.object.id,
    })

    if (book.collectionId) {
      const { collection } = await book
        .$query(this.transaction)
        .joinEager('collection')

      await collection.updateCollectionStatus(this.transaction)
    }

    logger.info(
      `Book ${this.object.id} has been updated successfully with status: ${this.status}`,
    )

    return book
  }
}

module.exports = UpdateStatus
