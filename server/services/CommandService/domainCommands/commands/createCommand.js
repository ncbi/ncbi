/* eslint-disable no-plusplus */

const config = require('config')
const { UserInputError } = require('apollo-server-express')
const cloneDeep = require('lodash/cloneDeep')
const isEmpty = require('lodash/isEmpty')
const merge = require('lodash/merge')
const omit = require('lodash/omit')
const set = require('lodash/set')

const {
  Book,
  TeamMember,
  BookSettingsTemplate,
  Organisation,
  User,
} = require('@pubsweet/models')

const { logger } = require('@coko/server')

const { domainRequest } = require('../../../axiosService')
const DomainTranslation = require('../../../domainTranslation/domainTranslation')
const notify = require('../../../notify')
const { makeRandomString, getNewAlias } = require('../../../../_helpers')
const NCBIError = require('../../../NCBIError')

class CreateCommand {
  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Book Create Command'
  }

  constructor({ transaction, user, ...data }) {
    this.data = {
      ...data,
    }

    this.userId = user
    this.transaction = transaction
  }

  async run() {
    // eslint-disable-next-line global-require
    const Domain = require('../domain')

    // CREATE BOOK
    const {
      settings: settingsAfterCreate,
      metadata: metadataAfterCreate,
      teams,
      alias,
      id,
    } = await Book.create({
      ...omit(this.data, ['collectionId']), // Adding book to collection happens later.
      status: 'new-book',
      ownerId: this.userId,
      transaction: this.transaction,
      alias: await getNewAlias({ from: this.data.alias }),
    })

    logger.info(
      `Book id: ${id}, bcms${alias} is created. From user id: ${this.userId}`,
    )

    // CREATE DOMAIN FOR BOOK
    const organisation = await Organisation.query(this.transaction).findOne({
      id: this.data.organisationId,
    })

    // publisher must be the org you chose in the form
    const publisherId = this.data.settings.publisher
    let publisherOrganization

    if (publisherId !== organisation.id) {
      const publisherOrg = await Organisation.query(this.transaction)
        .findById(publisherId)
        .throwIfNotFound()

      publisherOrganization = publisherOrg
    } else {
      publisherOrganization = organisation
    }

    const submissionType = this.data.settings.chapterIndependently
      ? 'chapterProcessed'
      : 'wholeBook'

    const customCollectionTemplate = await BookSettingsTemplate.query().findOne(
      {
        organizationId: this.data.organisationId,
        collectionId: this.data.collectionId || null,
        templateType: submissionType,
      },
    )

    const customOrganizationTemplate = await BookSettingsTemplate.query().findOne(
      {
        collectionId: null,
        organizationId: this.data.organisationId,
        templateType: submissionType,
      },
    )

    const customTemplate =
      customCollectionTemplate || customOrganizationTemplate

    const cloneFrom = customTemplate
      ? customTemplate.domainName
      : config.get(`bookSettings.domainTemplates.${submissionType}.domain`)

    let domainName = `bcms${alias}`
    const useRandomString = config.get('useRandomStringForDomainNames')
    if (useRandomString) domainName = makeRandomString(10)

    const { status, data } = await domainRequest('domains/', 'POST', {
      publisher: publisherOrganization.abbreviation,
      domain: domainName.substring(
        0,
        domainName.indexOf('.') < 0
          ? domainName.length
          : domainName.indexOf('.'),
      ),
      title: this.data.title,
      clone_from: cloneFrom,
    })

    const { id: domainId, domain } = data.body

    if (status === 201) {
      // GET DOMAIN VALUES
      const { status: domainStatus, data: detailsData } = await domainRequest(
        `domains/${domainId}`,
        'GET',
      )

      if (domainStatus >= 400) {
        logger.error(detailsData.body)
        throw new NCBIError('Get domain details request failed')
      }

      logger.info(
        `Remote Ncbi Domain with domain : ${domainId} was updated successfully with data : ${JSON.stringify(
          detailsData.body,
        )} from user id: ${this.userId}`,
      )

      const domainData = detailsData.body

      /**
       * WHY
       *
       * According to the 'clonechapterprocess' template, the default values
       * for 'createPdf' and 'alternateVersionsPdf' are no and yes respectively.
       * However if a chapter processed book is of the 'word' workflow or if it
       * is of funded content type 'authorManuscript', those settings must be
       * in sync with each other. To accommodate that, on book creation, under
       * the above conditions, we set 'createPdf' manually to true.
       *
       * This unfortunately results in an extra call to the domain service.
       * What we don't do is refetch the data from the domain after the patch,
       * assumming that if the patch had failed to set the right value, it
       * would have thrown an error.
       *
       * By not refetching the data however, there is an edge case that the
       * patch silently failed, and we have set a value on our db that is
       * potentially different than the domain service.
       * Any subsequent patch (eg. by saving the settings) would fix that.
       */

      const extraPatchData = {}
      const isChapterProcessed = submissionType === 'chapterProcessed'
      const isWholeBook = submissionType === 'wholeBook'
      const isWordWorkflow = this.data.workflow === 'word'

      const isAuthorManuscript =
        this.data.fundedContentType === 'authorManuscript'

      if (isChapterProcessed && (isWordWorkflow || isAuthorManuscript))
        set(extraPatchData, 'settings.createPdf', true)

      if (isWholeBook && isAuthorManuscript)
        set(extraPatchData, 'settings.createWholebookPdf', true)

      if (!isEmpty(extraPatchData)) {
        const extraPatchDomainData = DomainTranslation.createBookDomainUpdateObjectFromData(
          domainData,
          extraPatchData,
        )

        const { status: patchStatus, data: updatedData } = await domainRequest(
          `domains/${domainId}/`,
          'PATCH',
          extraPatchDomainData,
        )

        if (patchStatus >= 400) {
          logger.error(updatedData.body)
          throw new NCBIError('Update domain request failed')
        }
      }

      // MERGE DOMAIN VALUES, TEMPLATE VALUES & INCOMING VALUES

      const domainValues = DomainTranslation.domainToBookSettings(domainData)

      const ownDefaultValues = config.get('bookSettings.BCMSBookDefaults')
      let ownValues

      if (customTemplate)
        ownValues = merge({}, ownDefaultValues, customTemplate.values)
      else ownValues = cloneDeep(ownDefaultValues)

      if (isChapterProcessed) {
        ownValues.settings.approvalOrgAdminEditor = true
      }

      const incomingValues = {
        metadata: metadataAfterCreate,
        settings: settingsAfterCreate,
      }

      const valuesToStore = merge(
        {},
        domainValues,
        ownValues,
        incomingValues,
        extraPatchData,
      )

      // UPDATE BOOK SETTINGS ACCORDING TO DOMAIN

      const patchData = {
        domain,
        domainId: String(domainId),
        collectionId: this.data.collectionId,
        ...valuesToStore,
      }

      const domainUpdate = new Domain({
        id,
        domainId,
        input: patchData,
        transaction: this.transaction,
        user: this.userId,
      })

      await domainUpdate.executeCommand('update')

      const book = await Book.query(this.transaction).findOne({ id })

      /* FIND EDITOR TEAM AND ASSIGN CURRENT USER TO THE CREATED BOOK */
      const teamEditor = teams.find(team => team.role === 'editor')

      if (this.userId) {
        const isSysAdmin = await User.hasGlobalRole(this.userId, 'sysAdmin')

        const isOrgAdmin = await User.hasRoleOnObject(
          this.userId,
          'orgAdmin',
          this.data.organisationId,
        )

        if (!(isSysAdmin || isOrgAdmin)) {
          await TeamMember.query(this.transaction).insert({
            status: 'enabled',
            teamId: teamEditor.id,
            userId: this.userId,
          })
        }

        notify('bookCreated', {
          bookTitle: book.title,
          organisationId: book.organisationId,
          userId: this.userId,
          alias,
        })
      }

      return { ...book, teams }
    }

    if (status === 400 || status === 404) {
      logger.error(JSON.stringify(data.body))
      throw new UserInputError('Book invalid User Input', data.body)
    }

    if (status >= 500) {
      logger.error(JSON.stringify(data.body))
      throw new NCBIError('Creating ncbi Book Request failed ', data.body)
    }

    throw new NCBIError('Could not create domain')
  }
}

module.exports = CreateCommand
