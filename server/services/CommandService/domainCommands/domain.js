const { Book, FileVersion } = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const BaseCommand = require('../baseCommand')

const logger = getRawLogger()

const FileService = require('../../file/fileService')

const VendorMeta = require('../../xmlBaseModel/builder/vendorMeta')

const {
  CreateCommand,
  CreateCollectionCommand,
  CreateDomainTemplate,
  GetDetails,
  SendTocPMC,
  UpdateCollectionCommand,
  UpdateCommand,
  UpdateDomainTemplate,
  NewVersion,
  UpdateStatus,
  IncreaseFileVersion,
  OverwriteComponent,
  PublishComponent,
  IngestToPMC,
  SubmitComponent,
} = require('./commands')

const COMMANDS = {
  create: CreateCommand,
  createCollection: CreateCollectionCommand,
  createDomainTemplate: CreateDomainTemplate,
  getDetails: GetDetails,
  sendTocPMC: SendTocPMC,
  updateCollection: UpdateCollectionCommand,
  update: UpdateCommand,
  updateDomainTemplate: UpdateDomainTemplate,
  newVersion: NewVersion,
  updateStatus: UpdateStatus,
  increaseFileVersion: IncreaseFileVersion,
  overwriteComponent: OverwriteComponent,
  publishComponent: PublishComponent,
  ingestToPMC: IngestToPMC,
  submitComponent: SubmitComponent,
}

class Domain extends BaseCommand {
  constructor(props) {
    super(props, COMMANDS)
  }

  static async createVersion(data) {
    const book = await Book.query(data.transaction).findOne({
      id: data.bookId,
    })

    const bookFilesExist = await FileVersion.query(data.transaction)
      .select(['*'])
      .distinctOn('file_versions.parent_id')
      .joinEager('file')
      .where({
        bookId: data.bookId,
        'file_versions.category': data.category,
      })

    const createdBookComponent = { data: {}, action: '' }

    if (bookFilesExist.length > 0) {
      const latestBookFileQuery = FileVersion.query(data.transaction)
        .select(['*'])
        .distinctOn('file_versions.parent_id')
        .joinEager('file')
        .where({
          bookId: data.bookId,
          'file_versions.status': 'new-upload',
          'file_versions.category': data.category,
        })

      // For converted file, we do not query by filename as we just
      // need to fetch the last converted file in "new-upload" status.
      if (data.category !== 'converted') {
        latestBookFileQuery.andWhere(builder => {
          builder.where(
            'name',
            'like',
            `${data.filename.replace(/\.[^/.]+$/, '')}.%`,
          )
        })
      }

      latestBookFileQuery.orderByRaw(
        '"file_versions"."parent_id" , "file_versions"."created" desc',
      )

      const latestBookFile = await latestBookFileQuery

      if (latestBookFile.length > 0) {
        const bookDomain = new Domain({
          latestBookFile,
          object: book,
          fileId: data.fileId,
          filename: data.filename,
          ownerId: data.ownerId,
          transaction: data.transaction,
        })

        createdBookComponent.data = await bookDomain.executeCommand(
          'overwriteComponent',
        )

        createdBookComponent.action = 'overwriteComponent'
      } else {
        const domain = new Domain({
          ...book,
          status: 'new-upload',
          ownerId: data.ownerId,
          currentUser: data.ownerId,
          metadata: {
            ...book.metadata,
            fileId: data.fileId,
            filename: data.filename,
          },
          files: data.files || null,
          transaction: data.transaction,
          category: data.category || 'source',
        })

        createdBookComponent.data = await domain.executeCommand(
          'increaseFileVersion',
        )

        createdBookComponent.action = 'increaseFileVersion'
      }

      return createdBookComponent
    }

    // eslint-disable-next-line no-param-reassign
    data.files = data.files || [
      {
        bookId: data.bookId,
        category: data.category || 'source',
        ownerId: data.ownerId,
        id: data.fileId,
        name: data.filename,
      },
    ]

    if (book.workflow === 'pdf') {
      const vendorMeta = new VendorMeta({
        submissionType: 'book',
        bcmsId: `bcms${book.alias}`,
      })

      const { id, name } = await FileService.write({
        filename: 'vendor-meta.xml',
        category: 'support',
        content: vendorMeta.build(),
      })

      data.files.push({
        bookComponentId: null,
        category: 'support',
        ownerId: data.ownerId,
        id,
        name,
      })
    }

    if (data.files.length > 0) {
      data.files.forEach(async f => {
        logger.info(
          `File is added: ${f.name} on category ${f.category} on Book ${book.id}, bcms${book.alias}`,
        )

        await book.addFile(f, f.category, 'new-upload', data.transaction)
      })
    }

    await book.$query(this.transaction).patch({ status: 'new-upload' })

    createdBookComponent.data = book
    createdBookComponent.action = 'create'
    return createdBookComponent
  }
}

module.exports = Domain
