/* eslint-disable no-shadow */
/* eslint-disable jest/no-disabled-tests */
const { startServer } = require('pubsweet-server')
const { db, useTransaction } = require('@coko/server')
const addSeedFactory = require('../../../dbSeederFactory')
const { Domain } = require('../../..')

describe('Create a Book and increase file version', () => {
  let server
  const seeder = addSeedFactory()

  afterEach(async () => {
    await db.raw('ROLLBACK')
  })

  beforeAll(async () => {
    server = await startServer()
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await db.raw('BEGIN')
  })

  // eslint-disable-next-line jest/expect-expect
  it('fails to increase file version when file is still processing', async () => {
    const user = await seeder.insert('user', {})

    const { book } = await seeder.insert('addBookWithCustomStatus', { user })

    await useTransaction(async transaction => {
      const domain = new Domain({
        ...book,
        ownerId: book.ownerId,
        currentUser: book.ownerId,
        metadata: {
          ...book.metadata,
          fileId: book.metadata.fileId,
          filename: book.metadata.filename,
        },
        files: book.metadata.files || null,
        transaction,
        category: book.metadata.category || 'source',
      })

      expect.assertions(1)

      await expect(
        domain.executeCommand('increaseFileVersion'),
      ).rejects.toThrow(
        `Validation Failure for command: Chapter IncreaseFileVersion Command. Cannot increase the file version as the previous version is either loading-preview or converting.`,
      )
    })
  })
})
