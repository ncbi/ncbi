/* eslint-disable no-shadow */
/* eslint-disable jest/no-disabled-tests */
const { startServer } = require('pubsweet-server')
const { db, useTransaction } = require('@coko/server')
const { Book, FileVersion } = require('@pubsweet/models')
const addSeedFactory = require('../../../dbSeederFactory')
const { Domain } = require('../../..')

describe('Create a Book and overwrite file version', () => {
  let server
  const seeder = addSeedFactory()

  afterEach(async () => {
    await db.raw('ROLLBACK')
  })

  beforeAll(async () => {
    server = await startServer()
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await db.raw('BEGIN')
  })

  // eslint-disable-next-line jest/expect-expect
  it('fails to overwrite file version when file is still processing', async () => {
    const user = await seeder.insert('user', {})

    const { book } = await seeder.insert('addBookWithCustomStatus', { user })

    // eslint-disable-next-line no-console
    const { metadata } = await Book.query().findOne({
      id: book.id,
    })

    await useTransaction(async transaction => {
      const latestBookFile = await FileVersion.query(transaction)
        .select(['*'])
        .distinctOn('file_versions.parent_id')
        .joinEager('file')
        .where({
          bookId: book.id,
          status: 'new-upload',
          'file_versions.category': 'source',
        })
        .andWhere(builder => {
          builder.where(
            'name',
            'like',
            `${metadata.filename.toLowerCase().replace(/\.[^/.]+$/, '')}.%`,
          )
        })
        .orderByRaw(
          '"file_versions"."parent_id" , "file_versions"."created" desc',
        )

      const bookDomain = new Domain({
        latestBookFile,
        object: book,
        fileId: metadata.fileId,
        filename: metadata.filename,
        ownerId: metadata.ownerId,
        transaction,
      })

      expect.assertions(1)

      await expect(
        bookDomain.executeCommand('overwriteComponent'),
      ).rejects.toThrow(
        `Validation Failure for command: Book Overwrite Command. Cannot overwrite the book as it is either processing or under review.`,
      )
    })
  })
})
