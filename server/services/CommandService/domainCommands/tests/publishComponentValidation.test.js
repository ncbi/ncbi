/* eslint-disable no-shadow */
/* eslint-disable jest/no-disabled-tests */
const { startServer } = require('pubsweet-server')
const { db, useTransaction } = require('@coko/server')
const { Book } = require('@pubsweet/models')
const addSeedFactory = require('../../../dbSeederFactory')
const { Domain } = require('../../..')

describe('Publish book and version', () => {
  let server
  const seeder = addSeedFactory()

  afterEach(async () => {
    await db.raw('ROLLBACK')
  })

  beforeAll(async () => {
    server = await startServer()
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await db.raw('BEGIN')
  })

  // eslint-disable-next-line jest/expect-expect
  it('fails to publish due to validation error', async () => {
    const user = await seeder.insert('user', {})

    const { book } = await seeder.insert('addBookWithCustomStatus', { user })

    // eslint-disable-next-line no-console
    const { id, ownerId } = await Book.query().findOne({
      id: book.id,
    })

    await useTransaction(async transaction => {
      const domain = new Domain({
        id,
        userId: ownerId,
        transaction,
        manual: true,
      })

      expect.assertions(1)

      await expect(domain.executeCommand('publishComponent')).rejects.toThrow(
        `Validation Failure for command: Book Publish Command. Error occurred in Loading Error Query.`,
      )
    })
  })
})
