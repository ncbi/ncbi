/* eslint-disable no-shadow */
/* eslint-disable jest/no-disabled-tests */
const { startServer } = require('pubsweet-server')
const { db, useTransaction } = require('@coko/server')
const faker = require('faker')
const addSeedFactory = require('../../../dbSeederFactory')
const { Domain } = require('../../..')

describe('Create a book and version', () => {
  let server
  const seeder = addSeedFactory()

  afterEach(async () => {
    await db.raw('ROLLBACK')
  })

  beforeAll(async () => {
    server = await startServer()
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await db.raw('BEGIN')
  })

  // eslint-disable-next-line jest/expect-expect
  it('fails to create a book with an existing version', async () => {
    const user = await seeder.insert('user', {})

    // Book version is a composite property: fundedContentType & version number
    // We chose a random fundedContentType out of the expected values.
    // The version number by default is '1' when the initial version is created.
    const fundedContentType = faker.random.arrayElement([
      'authorManuscript',
      'prepublicationDraft',
      'publishedPDF',
    ])

    const initialBook = await seeder.insert('book', {
      user,
      fundedContentType,
    })

    const newVersionData = {
      bookId: initialBook.id,
      fundedContentType: initialBook.fundedContentType,
      version: '1',
      workflow: 'pdf',
    }

    await useTransaction(async transaction => {
      const domain = new Domain({ ...newVersionData, user, transaction })

      expect.assertions(1)

      await expect(domain.executeCommand('newVersion')).rejects.toThrow(
        'Validation Error: A book with the requested version already exist!',
      )
    })
  })
})
