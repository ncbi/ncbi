/* eslint-disable no-await-in-loop */
/* eslint-disable no-return-await */
const omit = require('lodash/omit')

const {
  Book,
  BookComponent,
  FileVersion,
  SourceConvertedFile,
  Division,
} = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const DivisionService = require('../../../division/divisionService')

class UpdateCommand {
  constructor(data) {
    this.data = omit(data, ['files', 'transaction'])
    this.files = data.files || []
    this.transaction = data.transaction
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Chapter Update Command'
  }

  async run() {
    const bookComponent = await BookComponent.query(this.transaction)
      .patch({
        abstract: this.data.abstract,
        componentType: this.data.componentType,
        metadata: this.data.metadata,
        status: this.data.status,
        title: this.data.title,
      })
      .findOne({ id: this.data.id })
      .returning('*')

    logger.info(
      `Chapter id: ${bookComponent.id}, ${
        bookComponent.alias
      } has been updated. with data: ${JSON.stringify({
        abstract: this.data.abstract,
        componentType: this.data.componentType,
        metadata: this.data.metadata,
        status: this.data.status,
        title: this.data.title,
      })}`,
    )

    // UPDATE the division of the chapter only if is the latest bookComponent
    if (!bookComponent.parent_id) {
      await this.updateDivision(bookComponent)
    }

    let converted = null

    if (this.files.length > 0) {
      // eslint-disable-next-line no-plusplus
      for (let i = 0, len = this.files.length; i < len; i++) {
        logger.info(
          `File is added: ${this.files[i].name} on category ${this.files[i].category} on Model ${bookComponent.id}, bcms${bookComponent.alias}`,
        )

        const bookComponentFile = await bookComponent.addFile(
          this.files[i],
          this.files[i].category,
          this.data.status,
          this.transaction,
        )

        if (bookComponentFile.category === 'converted') {
          converted = bookComponentFile
        }
      }
    }

    // update source files
    const latestSource = await BookComponent.latestComponentFiles(
      bookComponent,
      'source',
      this.transaction,
    )

    await FileVersion.query(this.transaction)
      .patch({ status: this.data.status })
      .whereIn(
        'id',
        latestSource.map(ls => ls.id),
      )

    if (converted) {
      await SourceConvertedFile.query(this.transaction).insert(
        latestSource.map(source => ({
          sourceId: source.id,
          convertedId: converted.id,
        })),
      )
    }

    const book = await Book.query(this.transaction).findOne({
      id: bookComponent.bookId,
    })

    await book.updateStatus(this.transaction)

    return bookComponent
  }

  // eslint-disable-next-line class-methods-use-this
  async updateDivision(bc) {
    const divisions = await Division.query(this.transaction).where({
      bookId: bc.bookId,
    })

    let division = divisions.find(div =>
      BookComponent.typesPerDivision()[div.label].includes(bc.componentType),
    )

    if (!division) {
      division = divisions.find(div => div.label === 'body')
    }

    if (division.id !== bc.divisionId) {
      const divisionService = new DivisionService(division, this.transaction)
      await divisionService.addBookComponents([bc])
    }
  }
}

module.exports = UpdateCommand
