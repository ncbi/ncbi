const omit = require('lodash/omit')
const pick = require('lodash/pick')
const uuid = require('uuid')

const {
  BookComponent,
  Channel,
  Book,
  Errors,
  NcbiNotificationMessage,
  FileVersion,
  Issue,
} = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const { getNewAlias } = require('../../../../_helpers')

class NewVersionCommand {
  constructor({ bookComponent, ownerId }) {
    this.data = bookComponent
    this.currentOwnerId = ownerId
  }

  async validate() {
    const book = await Book.query().findOne({ id: this.data.bookId })

    if (!book.settings.multiplePublishedVersions)
      throw new Error(
        `Validation Failure for command: ${this.title()}. Could not find a book with the id: ${
          this.data.bookId
        }`,
      )

    return book.settings.multiplePublishedVersions
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Chapter New Version Command'
  }

  async run() {
    const { bookComponentVersionId, versionName, publishedDate } = this.data

    const trx = await BookComponent.startTransaction()

    const bookComponent = new BookComponent({
      ...this.data,
      versionName: (Number.parseInt(this.data.versionName, 10) + 1).toString(),
      status: 'new-version',
      publishedDate: null,
      parentId: null,
      metadata: {
        ...this.data.metadata,
        url: null,
      },
      bookComponentVersionId: uuid.v4(),
      alias: await getNewAlias({ from: this.data.alias }),
    })

    const newVersion = await bookComponent.save(trx)

    const clonedComponent = new BookComponent(
      Object.assign(omit(this.data, ['id', 'alias']), {
        bookComponentVersionId,
        parentId: this.data.id,
        publishedDate: new Date(publishedDate).toISOString(),
        status: 'published',
        versionName,
      }),
    )

    const savedPreviousComponent = await clonedComponent.save(trx)

    await FileVersion.query(trx)
      .patch({ bookComponentId: savedPreviousComponent.id })
      .where('bookComponentId', this.data.id)

    await NcbiNotificationMessage.query(trx)
      .patch({ objectId: savedPreviousComponent.id })
      .andWhere('objectId', this.data.id)

    await Issue.query(trx)
      .patch({ objectId: savedPreviousComponent.id })
      .andWhere('objectId', this.data.id)

    await Errors.query(trx)
      .patch({ objectId: savedPreviousComponent.id })
      .andWhere('objectId', this.data.id)

    await Channel.query(trx).upsertGraphAndFetch(
      {
        objectId: newVersion.id,
        members: [{ user: { id: this.data.ownerId } }],
        topic: 'feedback-channel',
      },
      { relate: true },
    )

    await Channel.query(trx).upsertGraphAndFetch(
      {
        objectId: newVersion.id,
        members: [{ user: { id: this.data.ownerId } }],
        topic: 'error-channel',
      },
      { relate: true },
    )

    // Copy supplementary files to new Version
    const supplementaryFiles = await BookComponent.latestComponentFiles(
      savedPreviousComponent,
      'supplement',
      trx,
    )

    await BookComponent.relatedQuery('files', trx)
      .for(newVersion)
      .insert(
        supplementaryFiles.map(sf =>
          pick(
            {
              ...sf,
              ownerId: this.currentOwnerId,
              copied: true,
              versionName: '1',
            },
            [
              'type',
              'fileId',
              'parentId',
              'category',
              'tag',
              'versionName',
              'ownerId',
              'copied',
            ],
          ),
        ),
      )

    const book = await Book.query(trx).findOne({ id: this.data.bookId })
    await book.updateStatus(trx)

    logger.info(
      `New Version of Chapter with id: ${newVersion.id}, ${newVersion.alias}, ${newVersion.domain} is Created. with Version: ${newVersion.versionName}`,
    )

    await trx.commit()
    return newVersion
  }
}

module.exports = NewVersionCommand
