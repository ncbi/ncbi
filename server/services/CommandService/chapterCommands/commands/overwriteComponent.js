const pick = require('lodash/pick')

const { BookComponent, FileVersion, File } = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

class OverwriteCommand {
  constructor({
    bookComponent,
    fileId,
    ownerId,
    filename,
    latestBookComponentFile,
    transaction,
  }) {
    // save this for the validation
    this.previousStatus = bookComponent.status

    this.data = {
      ...bookComponent,
      status: 'new-upload',
      metadata: {
        ...pick(bookComponent, ['metadata']).metadata,
        fileId,
        filename,
      },
      ownerId,
    }

    this.latestBookComponentFile = latestBookComponentFile

    this.oldFileId = bookComponent.metadata.fileId

    this.transaction = transaction
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Chapter Overwrite Command'
  }

  async validate() {
    const [sourceFile] = await BookComponent.latestComponentFiles(
      this.data,
      'source',
      this.transaction,
    )

    const [convertedFile] = await BookComponent.latestComponentFiles(
      this.data,
      'converted',
      this.transaction,
    )

    const result = !(
      ((sourceFile && sourceFile.status === 'loading-preview') ||
        (sourceFile && sourceFile.status === 'converting') ||
        (convertedFile && convertedFile.status === 'loading-preview') ||
        (convertedFile && convertedFile.status === 'converting')) &&
      this.previousStatus === 'in-review'
    )

    if (!result)
      throw new Error(
        `Validation Failure for command: ${this.title()}. Cannot overwrite the chapter as it is either processing or under review.`,
      )

    return result
  }

  async run() {
    const updatedBookComponent = await BookComponent.query(
      this.transaction,
    ).patchAndFetchById(this.data.id, {
      ...this.data,
      publishedDate: this.data.publishedDate
        ? new Date(this.data.publishedDate).toISOString()
        : null,
    })

    await FileVersion.query(this.transaction)
      .patch({
        created: new Date().toISOString(),
        fileId: updatedBookComponent.metadata.fileId,
        ownerId: this.data.ownerId,
      })
      .findOne({ id: this.latestBookComponentFile[0].id })

    await File.query(this.transaction)
      .delete()
      .findOne({ id: this.latestBookComponentFile[0].fileId })

    const { id, ownerId, metadata, alias } = updatedBookComponent

    const user = ownerId || '"comes from FTP Submission"'

    logger.info(
      `User Id: ${user}  overwrited successfully the file ${metadata.filename} for chapter id : ${id}, bcms${alias}`,
    )

    logger.info(
      `New file id: ${metadata.fileId}, Old file id: ${this.latestBookComponentFile[0].fileId}`,
    )

    return updatedBookComponent
  }
}

module.exports = OverwriteCommand
