/* eslint-disable global-require */
const {
  BookComponent,
  FileVersion,
  SourceConvertedFile,
  Book,
} = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const StatusService = require('../../../statusService/statusService')

const logger = getRawLogger()

class UpdateStatus {
  constructor(data) {
    this.object = data.object
    this.status = data.status
    this.transaction = data.transaction
    this.currentUser = data.currentUser
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Update Status of Book and BookComponent Command'
  }

  async run() {
    const [convertedFile] = await BookComponent.latestComponentFiles(
      this.object,
      'converted',
      this.transaction,
    )

    let filesUpdate = []
    let sourceFiles = []

    if (convertedFile) {
      sourceFiles = await SourceConvertedFile.query(this.transaction).where({
        convertedId: convertedFile.id,
      })

      filesUpdate = (sourceFiles || []).map(f => f.sourceId)
    } else {
      sourceFiles = await BookComponent.latestComponentFiles(
        this.object,
        'source',
        this.transaction,
      )

      filesUpdate = (sourceFiles || []).map(f => f.id)
    }

    if (convertedFile) {
      filesUpdate = filesUpdate.concat([convertedFile.id])
    }

    if (this.status === 'published') {
      const update = {
        status: this.status,
        publishedDate: new Date().toISOString(),
      }

      await BookComponent.query(this.transaction)
        .patch(update)
        .findOne({ id: this.object.id })

      await Book.query(this.transaction)
        .patch(update)
        .findOne({ id: this.object.bookId })
    }

    // Update BookComponent
    const bookComponent = await StatusService.update({
      entity: this.object,
      status: this.status,
      userId: this.currentUser,
      transaction: this.transaction,
      force: true,
    })

    logger.info(
      `BookComponent ${this.object.id}, ${this.object.alias} has been updated successfully with status: ${this.status} by user id: ${this.currentUser}`,
    )

    await FileVersion.query(this.transaction)
      .patch({ status: this.status })
      .whereIn('id', filesUpdate)

    logger.info(
      `File status for ids: ${filesUpdate.join(',')} is updated to ${
        this.status
      } from user id ${this.currentUser}`,
    )

    const book = await Book.query(this.transaction).findOne({
      id: this.object.bookId,
    })

    await book.updateStatus(this.transaction)

    return bookComponent
  }
}

module.exports = UpdateStatus
