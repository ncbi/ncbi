const { BookComponent, FileVersion, Book } = require('@pubsweet/models')
const StatusService = require('../../../statusService/statusService')

class SubmitComponent {
  constructor(data) {
    this.id = data.id
    this.transaction = data.transaction || null
    this.currentUser = data.currentUser || null
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return `Chapter Submit Command for bookComponent id: ${this.id} from user id: ${this.currentUser}`
  }

  async run() {
    const bookComponent = await BookComponent.query(this.transaction).findOne({
      id: this.id,
    })

    const book = await Book.query(this.transaction).findOne({
      id: bookComponent.bookId,
    })

    const status = book.workflow === 'pdf' ? 'tagging' : 'converting'

    await StatusService.update({
      entity: bookComponent,
      status,
      userId: this.currentUser,
      transaction: this.transaction,
      force: true,
    })

    const filesConverted = await BookComponent.latestComponentFiles(
      bookComponent,
      'source',
      this.transaction,
    )

    await FileVersion.query(this.transaction)
      .patch({ status })
      .whereIn(
        'id',
        filesConverted.map(f => f.id),
      )

    await book.updateStatus(this.transaction)
  }
}

module.exports = SubmitComponent
