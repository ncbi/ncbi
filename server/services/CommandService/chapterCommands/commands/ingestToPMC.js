/* eslint-disable no-return-await */
/* eslint-disable no-await-in-loop */
const {
  Book,
  FileVersion,
  BookComponent,
  SourceConvertedFile,
} = require('@pubsweet/models')

const PackageCollection = require('../../../ncbiConnectionManager/packageCollection')

const XmlFactory = require('../../../xmlBaseModel/xmlFactory')

const FileService = require('../../../file/fileService')
const StatusService = require('../../../statusService/statusService')
const NCBIError = require('../../../NCBIError')

class IngestToPMC {
  constructor(data) {
    this.type = data.publishType

    this.object = data.object
    this.transaction = data.transaction

    // Check issue https://gitlab.coko.foundation/ncbi/ncbi/-/issues/435
    // For database selection and release flag
    if (data.publishType === 'preview') {
      this.options = {
        release: false,
        target_database: 'prod',
      }
    } else if (data.publishType === 'preview-published') {
      this.options = {
        release: false,
        target_database: 'preview',
      }
    } else if (data.publishType === 'publish') {
      this.options = {
        release: true,
        target_database: 'prod',
      }
    }

    this.currentUser = data.currentUser || data.object.ownerId
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return `Chapter/Book with id: ${this.object.id}  Ingest To PMC Command by user: ${this.currentUser}`
  }

  async run() {
    const files = await BookComponent.latestComponentFiles(
      this.object,
      null,
      this.transaction,
    )

    this.convertedXml = files.find(file => file.category === 'converted')

    if (!this.convertedXml) {
      throw new NCBIError(
        `There is no Converted File for Loading Preview. BookComponent ${this.object.id}`,
      )
    }

    const book = await Book.query(this.transaction).findOne({
      id: this.object.bookId,
    })

    const bookWithAppliedValues = await book.getBookWithAppliedCollectionValues()

    await this.updateExtractConvertedFile(bookWithAppliedValues)

    let packageFiles = files
      .filter(
        file => file.category !== 'converted' && file.category !== 'review',
      )
      .concat([this.convertedXml])
      .map(({ file, category, versionName }) => ({
        ...file,
        chapter: this.object.metadata.book_component_id,
        versionName,
      }))

    // Get the related book abstract File object

    const fileAbstract = await book.getFileAbstract()

    if (fileAbstract.abstractCover) {
      packageFiles = packageFiles.concat([
        {
          ...fileAbstract.abstractCover,
          versionName: '1',
          chapter: this.object.metadata.book_component_id,
        },
      ])
    }

    await PackageCollection.sendForConversion(
      'preview',
      packageFiles,
      {
        domain: book.domain,
        book: bookWithAppliedValues,
        bookComponent: this.object,
        versionName: this.object.versionName,
        ...this.options,
      },
      {
        objectId: this.object.id,
      },
      this.transaction,
    )

    if (this.type === 'preview' || this.type === 'preview-published') {
      const result = await this.updateStatus('loading-preview')
      return result
    }

    return this.object
  }

  async updateStatus(status) {
    const sourceFiles = await SourceConvertedFile.query(this.transaction).where(
      {
        convertedId: this.convertedXml.id,
      },
    )

    let filesUpdate = (sourceFiles || []).map(f => f.sourceId)

    filesUpdate = filesUpdate.concat([this.convertedXml.id])

    // Update BookComponent
    const bookComponent = await BookComponent.query(this.transaction).findOne({
      id: this.object.id,
    })

    await StatusService.update({
      entity: bookComponent,
      status,
      userId: this.currentUser,
      transaction: this.transaction,
      force: true,
    })

    await FileVersion.query(this.transaction)
      .patch({ status })
      .whereIn('id', filesUpdate)

    const book = await Book.query(this.transaction).findOne({
      id: this.object.bookId,
    })

    await book.updateStatus(this.transaction)

    return await BookComponent.query(this.transaction).findOne({
      id: this.object.id,
    })
  }

  async updateExtractConvertedFile(book) {
    let content = await FileService.getSourceToBuffer(
      this.convertedXml.file.id,
      this.transaction,
    )

    content = await XmlFactory.addMetadataCollectionToBook(book, content)

    this.convertedXml.file = await FileService.update(
      this.convertedXml.file.id,
      { content },
    )
  }
}

module.exports = IngestToPMC
