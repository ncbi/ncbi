const { BookComponent, Book } = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()
const StatusService = require('../../../statusService/statusService')

class IncreaseFileVersionCommand {
  constructor({ transaction, files, category, currentUser, ...data }) {
    this.currentUser = currentUser

    // save this for the validation
    this.previousStatus = data.status

    this.data = {
      ...data,
      publishedDate: data.publishedDate
        ? new Date(data.publishedDate).toISOString()
        : null,
      status: 'new-upload',
      metadata: {
        ...data.metadata,
        url: '',
      },
    }

    this.files = files || [
      {
        bookComponentId: this.data.id,
        category: category || 'source',
        ownerId: data.ownerId,
        id: data.metadata.fileId,
        name: data.metadata.filename,
      },
    ]

    this.transaction = transaction
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Chapter IncreaseFileVersion Command'
  }

  async validate() {
    const [sourceFile] = await BookComponent.latestComponentFiles(
      this.data,
      'source',
      this.transaction,
    )

    const [convertedFile] = await BookComponent.latestComponentFiles(
      this.data,
      'converted',
      this.transaction,
    )

    const result =
      (sourceFile && sourceFile.status === 'loading-preview') ||
      (sourceFile && sourceFile.status === 'converting') ||
      (convertedFile && convertedFile.status === 'loading-preview') ||
      (convertedFile && convertedFile.status === 'converting')

    if (result)
      throw new Error(
        `Validation Failure for command: ${this.title()}. Cannot increase the file version as the previous version is either loading-preview or converting.`,
      )

    return result
  }

  async run() {
    const bookComponent = await BookComponent.query(
      this.transaction,
    ).patchAndFetchById(this.data.id, this.data)

    const { id, alias } = bookComponent

    logger.info(
      `Chapter id: ${id}, bcms${alias} is updated by user : ${
        this.currentUser
      }. Updated Data: ${JSON.stringify(this.data)} `,
    )

    await StatusService.update({
      entity: bookComponent,
      status: this.data.status,
      userId: this.currentUser,
      transaction: this.transaction,
      force: true,
    })

    let fileCover = null

    if (this.files.length > 0) {
      // eslint-disable-next-line no-plusplus
      for (let i = 0, len = this.files.length; i < len; i++) {
        logger.info(
          `File is uploaded: ${this.files[i].name} by user ${this.currentUser} on category ${this.files[i].category} on Model ${id}, bcms${alias}`,
        )

        // eslint-disable-next-line no-await-in-loop
        await bookComponent.addFile(
          this.files[i],
          this.files[i].category,
          'new-upload',
          this.transaction,
        )

        if (this.files[i].category === 'cover') {
          fileCover = this.files[i].id
        }
      }
    }

    if (fileCover) {
      await Book.query(this.transaction)
        .patch({ fileCover })
        .findOne({ id: bookComponent.bookId })
    }

    const book = await Book.query(this.transaction).findOne({
      id: this.data.bookId,
    })

    await book.updateStatus(this.transaction)

    return bookComponent
  }
}

module.exports = IncreaseFileVersionCommand
