const pick = require('lodash/pick')

const {
  BookComponent,
  Channel,
  Division,
  TeamMember,
  Team,
  Book,
  User,
} = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const FileService = require('../../../file/fileService')

const VendorMeta = require('../../../xmlBaseModel/builder/vendorMeta')
const DivisionService = require('../../../division/divisionService')
const { getNewAlias } = require('../../../../_helpers')

class CreateCommand {
  constructor(data) {
    this.data = {
      ...pick(data, ['bookId', 'ownerId', 'componentType']),
      metadata: { ...pick(data, ['fileId', 'filename']) },
      versionName: data.versionName || '1',
      status: 'new-upload',
      title: 'Untitled',
    }

    this.files = data.files || [
      {
        bookComponentId: null,
        category: data.category || 'source',
        ownerId: data.ownerId,
        id: data.fileId,
        name: data.filename,
      },
    ]

    this.transaction = data.transaction
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Chapter Create Command'
  }

  async run() {
    const division = await Division.query(this.transaction).findOne({
      bookId: this.data.bookId,
      label: 'body',
    })

    const createdBookComponent = await BookComponent.query(
      this.transaction,
    ).insertAndFetch({
      ...this.data,
      divisionId: division.id,
      publishedDate: null,
      alias: await getNewAlias({ from: this.data.alias }),
    })

    const { id, alias, ownerId } = createdBookComponent

    logger.info(
      `Chapter id: ${id}, bcms${alias} is created by user : ${ownerId} `,
    )

    await this.updateDivision(createdBookComponent)

    const book = await Book.query(this.transaction).findOne({
      id: this.data.bookId,
    })

    if (book.workflow === 'pdf')
      await this.createVendorMetaFile(createdBookComponent.alias)

    if (this.files.length > 0) {
      logger.info(
        `Files were uploaded: [${this.files.map(f => f.name)}] by user ${
          this.data.ownerId
        } on category ${this.files[0].category} on Model ${
          createdBookComponent.id
        }, bcms${createdBookComponent.alias}`,
      )

      this.files.forEach(async f => {
        await createdBookComponent.addFile(
          { ...f, bookComponentId: createdBookComponent.id },
          f.category,
          'new-upload',
          this.transaction,
        )
      })
    }

    if (this.data.componentType === 'part')
      return { model: createdBookComponent }

    const bookComponentInstance = new BookComponent(createdBookComponent)
    await this.addUserToTeams(bookComponentInstance)

    await this.createChannels(createdBookComponent)

    await book.updateStatus(this.transaction)

    return createdBookComponent
  }

  async createVendorMetaFile(alias) {
    const vendorMeta = new VendorMeta({
      submissionType: 'chapter',
      bcmsId: `bcms${alias}`,
    })

    const { id, name } = await FileService.write({
      filename: 'vendor-meta.xml',
      category: 'support',
      content: vendorMeta.build(),
    })

    this.files.push({
      bookComponentId: null,
      category: 'support',
      ownerId: this.data.ownerId,
      id,
      name,
    })
  }

  // eslint-disable-next-line class-methods-use-this
  async updateDivision(bc) {
    const divisionService = await DivisionService.getDivision(
      {
        bookId: bc.bookId,
        label: 'body',
      },
      this.transaction,
    )

    const addedToDivision = await divisionService.addBookComponents([bc])

    return addedToDivision
  }

  async addUserToTeams(bookComponentInstance) {
    const teams = await bookComponentInstance.createTeams(this.transaction)

    const bookAuthorTeam = await Team.query(this.transaction).findOne({
      objectId: bookComponentInstance.bookId,
      objectType: 'book',
      role: 'author',
    })

    let bookAuthor = null

    if (this.data.ownerId) {
      bookAuthor = await TeamMember.query(this.transaction).findOne({
        teamId: bookAuthorTeam.id,
        userId: this.data.ownerId,
      })
    }

    if (bookAuthor) {
      /* find Author Team and assigning current user to the created BookComponent */
      const team = teams.find(tm => tm.role === 'author')

      const isSysAdmin = await User.hasGlobalRole(this.userId, 'sysAdmin')

      const isOrgAdmin = await User.hasRoleOnObject(
        this.userId,
        'orgAdmin',
        this.data.organisationId,
      )

      const isOrgEditor = await User.hasRoleOnObject(
        this.userId,
        'editor',
        this.data.organisationId,
      )

      if (!(isSysAdmin || isOrgAdmin || isOrgEditor)) {
        await TeamMember.query(this.transaction).insert({
          status: 'enabled',
          teamId: team.id,
          userId: this.data.ownerId,
        })
      }
    }

    return teams
  }

  async createChannels(createdBookComponent) {
    const members = this.data.ownerId
      ? [{ user: { id: this.data.ownerId } }]
      : []

    const feedbackPanel = await Channel.query(
      this.transaction,
    ).upsertGraphAndFetch(
      {
        objectId: createdBookComponent.id,
        members,
        topic: 'feedback-channel',
      },
      { relate: true },
    )

    const errorChannel = await Channel.query(
      this.transaction,
    ).upsertGraphAndFetch(
      {
        objectId: createdBookComponent.id,
        members,
        topic: 'error-channel',
      },
      { relate: true },
    )

    const allChannels = await Promise.all([feedbackPanel, errorChannel])

    return allChannels
  }
}

module.exports = CreateCommand
