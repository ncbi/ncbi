const CreateCommand = require('./createCommand')
const UpdateCommand = require('./updateCommand')
const NewVersion = require('./newVersion')
const IncreaseFileVersion = require('./increaseFileVersion')
const OverwriteComponent = require('./overwriteComponent')
const SubmitComponent = require('./submitComponent')
const PublishComponent = require('./publishComponent')
const IngestToPMC = require('./ingestToPMC')
const UpdateStatus = require('./updateStatus')

module.exports = {
  CreateCommand,
  UpdateCommand,
  NewVersion,
  IncreaseFileVersion,
  OverwriteComponent,
  SubmitComponent,
  PublishComponent,
  IngestToPMC,
  UpdateStatus,
}
