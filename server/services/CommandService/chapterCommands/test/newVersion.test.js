/* eslint-disable no-shadow */
/* eslint-disable jest/no-disabled-tests */
const { startServer } = require('pubsweet-server')
const { db } = require('@coko/server')
const { BookComponent } = require('@pubsweet/models')
const addSeedFactory = require('../../../dbSeederFactory')
const { Chapter } = require('../../..')

describe('Create a chapter and version', () => {
  let server
  const seeder = addSeedFactory()

  afterEach(async () => {
    await db.raw('ROLLBACK')
  })

  beforeAll(async () => {
    server = await startServer()
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await db.raw('BEGIN')
  })

  // eslint-disable-next-line jest/expect-expect
  it('fails to create a chapter with an existing version', async () => {
    const user = await seeder.insert('user', {})

    const { bookComponent } = await seeder.insert('addBookComponent', { user })

    // eslint-disable-next-line no-console
    const { ownerId, bookId } = await BookComponent.query().findOne({
      id: bookComponent.id,
    })

    const chapter = new Chapter({ bookComponent, ownerId })

    expect.assertions(1)

    await expect(chapter.executeCommand('newVersion')).rejects.toThrow(
      `Validation Failure for command: Chapter New Version Command. Could not find a book with the id: ${bookId}`,
    )
  })
})
