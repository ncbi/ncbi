/* eslint-disable no-shadow */
/* eslint-disable jest/no-disabled-tests */
const { startServer } = require('pubsweet-server')
const { db, useTransaction } = require('@coko/server')
const { BookComponent } = require('@pubsweet/models')
const addSeedFactory = require('../../../dbSeederFactory')
const { Chapter } = require('../../..')

describe('Create a chapter and increase file version', () => {
  let server
  const seeder = addSeedFactory()

  afterEach(async () => {
    await db.raw('ROLLBACK')
  })

  beforeAll(async () => {
    server = await startServer()
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await db.raw('BEGIN')
  })

  // eslint-disable-next-line jest/expect-expect
  it('fails to increase file version when file is still processing', async () => {
    const user = await seeder.insert('user', {})

    const {
      bookComponent,
    } = await seeder.insert('addBookComponentWithCustomStatus', { user })

    // eslint-disable-next-line no-console
    const { ownerId, metadata } = await BookComponent.query().findOne({
      id: bookComponent.id,
    })

    await useTransaction(async transaction => {
      const chapter = new Chapter({
        ...bookComponent,
        ownerId,
        currentUser: ownerId,
        metadata: {
          ...bookComponent.metadata,
          fileId: metadata.fileId,
          filename: metadata.filename,
        },
        files: metadata.files || null,
        transaction,
        category: metadata.category || 'source',
      })

      expect.assertions(1)

      await expect(
        chapter.executeCommand('increaseFileVersion'),
      ).rejects.toThrow(
        `Validation Failure for command: Chapter IncreaseFileVersion Command. Cannot increase the file version as the previous version is either loading-preview or converting.`,
      )
    })
  })
})
