/* eslint-disable no-shadow */
/* eslint-disable jest/no-disabled-tests */
const { startServer } = require('pubsweet-server')
const { db, useTransaction } = require('@coko/server')
const { BookComponent, FileVersion } = require('@pubsweet/models')
const addSeedFactory = require('../../../dbSeederFactory')
const { Chapter } = require('../../..')

describe('Create a chapter and increase file version', () => {
  let server
  const seeder = addSeedFactory()

  afterEach(async () => {
    await db.raw('ROLLBACK')
  })

  beforeAll(async () => {
    server = await startServer()
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await db.raw('BEGIN')
  })

  // eslint-disable-next-line jest/expect-expect
  it('fails to overwrite file version when file is still processing', async () => {
    const user = await seeder.insert('user', {})

    const {
      bookComponent,
    } = await seeder.insert('addBookComponentWithCustomStatus', { user })

    // eslint-disable-next-line no-console
    const { ownerId, metadata } = await BookComponent.query().findOne({
      id: bookComponent.id,
    })

    await useTransaction(async transaction => {
      const latestBookComponentFile = await FileVersion.query(transaction)
        .select(['*'])
        .distinctOn('file_versions.parent_id')
        .joinEager('file')
        .where({
          bookComponentId: bookComponent.id,
          status: 'new-upload',
          'file_versions.category': 'source',
        })
        .andWhere(builder => {
          builder.where(
            'name',
            'like',
            `${metadata.filename.toLowerCase().replace(/\.[^/.]+$/, '')}.%`,
          )
        })
        .orderByRaw(
          '"file_versions"."parent_id" , "file_versions"."created" desc',
        )

      const chapter = new Chapter({
        latestBookComponentFile,
        bookComponent,
        fileId: metadata.fileId,
        filename: metadata.filename,
        ownerId,
        transaction,
      })

      expect.assertions(1)

      await expect(
        chapter.executeCommand('overwriteComponent'),
      ).rejects.toThrow(
        `Validation Failure for command: Chapter Overwrite Command. Cannot overwrite the chapter as it is either processing or under review.`,
      )
    })
  })
})
