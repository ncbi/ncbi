/* eslint-disable no-shadow */
/* eslint-disable jest/no-disabled-tests */
const { startServer } = require('pubsweet-server')
const { db, useTransaction } = require('@coko/server')
const { BookComponent } = require('@pubsweet/models')
const addSeedFactory = require('../../../dbSeederFactory')
const { Chapter } = require('../../..')

describe('Publish chapter and version', () => {
  let server
  const seeder = addSeedFactory()

  afterEach(async () => {
    await db.raw('ROLLBACK')
  })

  beforeAll(async () => {
    server = await startServer()
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await db.raw('BEGIN')
  })

  // eslint-disable-next-line jest/expect-expect
  it('fails to publish due to validation error', async () => {
    const user = await seeder.insert('user', {})

    const {
      bookComponent,
    } = await seeder.insert('addBookComponentWithCustomStatus', { user })

    // eslint-disable-next-line no-console
    const { id, ownerId } = await BookComponent.query().findOne({
      id: bookComponent.id,
    })

    await useTransaction(async transaction => {
      const chapter = new Chapter({
        id,
        userId: ownerId,
        transaction,
        manual: true,
      })

      expect.assertions(1)

      await expect(chapter.executeCommand('publishComponent')).rejects.toThrow(
        `Validation Failure for command: Chapter Publish Command. Error occurred in Loading Error Query.`,
      )
    })
  })
})
