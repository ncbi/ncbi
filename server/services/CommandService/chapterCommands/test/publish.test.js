/* eslint-disable no-shadow */
/* eslint-disable jest/no-disabled-tests */
const { startServer } = require('pubsweet-server')

const { db, useTransaction } = require('@coko/server')

const { BookComponent, Errors } = require('@pubsweet/models')

const Chapter = require('../chapter')

const addSeedFactory = require('../../../dbSeederFactory')

describe('Publish Chapter and Book', () => {
  let server
  const seeder = addSeedFactory()

  afterEach(async () => {
    await db.raw('ROLLBACK')
  })

  beforeAll(async () => {
    server = await startServer()
  })

  afterAll(done => {
    server.close(done)
  })

  beforeEach(async () => {
    await db.raw('BEGIN')
  })

  // eslint-disable-next-line jest/expect-expect
  it('fails to publish because of an error at ncbi domain', async () => {
    const { user } = await seeder.insert('addAdmin', {
      password: '123456789',
      username: 'admin',
      email: 'admin@admin.com',
    })

    const { bookComponent } = await seeder.insert('addBookComponent', { user })

    // eslint-disable-next-line no-console
    const { id, ownerId } = await BookComponent.query().findOne({
      id: bookComponent.id,
    })

    await useTransaction(async transaction => {
      const chapter = new Chapter({ id, userId: ownerId, transaction })
      await chapter.executeCommand('publishComponent')
    })

    const afterPublish = await BookComponent.query().findOne({
      id: bookComponent.id,
    })

    const errors = await Errors.query().findOne({
      objectId: afterPublish.id,
    })

    expect(afterPublish.status).toBe('publish-failed')

    expect(errors).toMatchObject({
      noticeTypeName: 'Domain Service',
      severity: 'error',
      assignee: 'PMC',
      errorType: 'system',
      message: 'Error: Running command: Domain Book Update Command Failed',
      errorCategory: 'domain-error',
    })
  })
})
