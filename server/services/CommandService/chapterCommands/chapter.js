const { Book, FileVersion } = require('@pubsweet/models')
const BaseCommand = require('../baseCommand')

const {
  CreateCommand,
  UpdateCommand,
  NewVersion,
  IncreaseFileVersion,
  OverwriteComponent,
  SubmitComponent,
  PublishComponent,
  IngestToPMC,
  UpdateStatus,
} = require('./commands')

const COMMANDS = {
  create: CreateCommand,
  update: UpdateCommand,
  newVersion: NewVersion,
  increaseFileVersion: IncreaseFileVersion,
  overwriteComponent: OverwriteComponent,
  submitComponent: SubmitComponent,
  publishComponent: PublishComponent,
  ingestToPMC: IngestToPMC,
  updateStatus: UpdateStatus,
}

class Chapter extends BaseCommand {
  constructor(props) {
    super(props, COMMANDS)
  }

  static async createVersion(data) {
    let bookComponents = []

    bookComponents = await Book.getBookComponentsByVersion(
      data.bookId,
      data.versionName,
    )

    const bookComponent = (bookComponents || []).find(
      bc =>
        bc.metadata.filename.toLowerCase().replace(/\.[^/.]+$/, '') ===
        data.filename.toLowerCase().replace(/\.[^/.]+$/, ''),
    )

    const createdBookComponent = { data: {}, action: '' }

    if (bookComponent) {
      const latestBookComponentFile = await FileVersion.query(data.transaction)
        .select(['*'])
        .distinctOn('file_versions.parent_id')
        .joinEager('file')
        .where({
          bookComponentId: bookComponent.id,
          'file_versions.status': 'new-upload',
          'file_versions.category': data.category,
        })
        .andWhere(builder => {
          builder.where(
            'name',
            'like',
            `${data.filename.toLowerCase().replace(/\.[^/.]+$/, '')}.%`,
          )
        })
        .orderByRaw(
          '"file_versions"."parent_id" , "file_versions"."created" desc',
        )

      if (latestBookComponentFile.length > 0) {
        const chapter = new Chapter({
          latestBookComponentFile,
          bookComponent,
          fileId: data.fileId,
          filename: data.filename,
          ownerId: data.ownerId,
          transaction: data.transaction,
        })

        createdBookComponent.data = await chapter.executeCommand(
          'overwriteComponent',
        )

        createdBookComponent.action = 'overwriteComponent'
      } else {
        const chapter = new Chapter({
          ...bookComponent,
          ownerId: data.ownerId,
          currentUser: data.ownerId,
          metadata: {
            ...bookComponent.metadata,
            fileId: data.fileId,
            filename: data.filename,
          },
          files: data.files || null,
          transaction: data.transaction,
          category: data.category || 'source',
        })

        createdBookComponent.data = await chapter.executeCommand(
          'increaseFileVersion',
        )

        createdBookComponent.action = 'increaseFileVersion'
      }
    } else {
      const chapter = new Chapter(data)
      createdBookComponent.data = await chapter.executeCommand('create')
      createdBookComponent.action = 'create'
    }

    return createdBookComponent
  }
}

module.exports = Chapter
