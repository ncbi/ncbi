const BaseCommand = require('../baseCommand')

const { PublishComponent, UpdateStatus } = require('./commands')

const COMMANDS = {
  publishComponent: PublishComponent,
  updateStatus: UpdateStatus,
}

class Toc extends BaseCommand {
  constructor(props) {
    super(props, COMMANDS)
  }
}

module.exports = Toc
