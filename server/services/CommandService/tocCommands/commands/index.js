const PublishComponent = require('./publishComponent')
const UpdateStatus = require('./updateStatus')

module.exports = {
  PublishComponent,
  UpdateStatus,
}
