/* eslint-disable global-require */
const { Collection, Book, Errors, Toc } = require('@pubsweet/models')
const { UserInputError } = require('apollo-server-express')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()
const crypto = require('crypto')

const { agreementRequest } = require('../../../axiosService')
const NCBIError = require('../../../NCBIError')

const UNIQUE_ACCEPTABLE_AGGREMENTS = [
  'Works of the U.S. Government',
  'Customized Agreement',
  'NCBI Bookshelf Restricted Access',
  'NCBI Books Online Agreement',
  'Bookshelf Full Participation',
  'NLM LitArch Participation',
  'Additional Title Amendment',
]

const ERROR_MESSAGE = `The BCMS cannot find a valid NLM agreement for this content to permit its release to the public Bookshelf site. Contact an NCBI System Admin to resolve the error and try again.`

const ENABLE_AGREEMENTS = process.env.DEBUG

class PublishComponent {
  constructor(data) {
    this.id = data.id
    this.userId = data.userId
    this.manual = data.manual || true
    this.agreementError = false
    this.bookAgreements = []
    this.collectionAgreements = []
    this.transaction = data.transaction
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'ToC Publish Command'
  }

  async validate() {
    const toc = await Toc.query(this.transaction).findOne({
      id: this.id,
    })

    const canBePublished = await toc.hasPublishedComponents()

    if (!canBePublished) return false

    const { status, id } = toc

    if (status === 'publish-failed') return true

    const loadingErrors = await Errors.query(this.transaction)
      .distinctOn('severity')
      .where({
        objectId: id,
        history: false,
        errorCategory: 'loading-errors',
      })

    let loadingError = ''

    if (this.manual) {
      loadingError =
        loadingErrors.length === 0 ||
        loadingErrors.every(
          error => error.severity === 'warning' || error.severity === 'query',
        )

      logger.info(`Loading Error Query result: ${loadingError}`)
      if (!loadingError)
        throw new Error(
          `Validation Failure for command: ${this.title()}. Error occurred in Loading Error Query.`,
        )

      return loadingError
    }

    loadingError =
      loadingErrors.length === 0 ||
      loadingErrors.every(error => error.severity === 'warning')

    logger.info(`Loading Error Warning result: ${loadingError}`)

    if (!loadingError)
      throw new Error(
        `Validation Failure for command: ${this.title()}. Error occurred in Loading Error Warning.`,
      )

    return loadingError
  }

  async run() {
    const Domain = require('../../domainCommands/domain')
    const TocService = require('../toc')

    let toc = await Toc.query(this.transaction).findOne({
      id: this.id,
    })

    const book = await Book.query(this.transaction).findOne({
      id: toc.bookId,
    })

    const collection = await Collection.query(this.transaction).findOne({
      id: toc.collectionId,
    })

    try {
      if (ENABLE_AGREEMENTS === 'true') {
        await this.hasExactlyOneAgreement(book || collection)
      }
    } catch (error) {
      // eslint-disable-next-line no-return-await
      return await this.publishFailed(toc, {
        category: 'agreementChecks',
        error: error.message,
      })
    }

    const chapterUpdate = new TocService({
      status: 'publishing',
      object: toc,
      transaction: this.transaction,
      currentUser: this.userId,
    })

    toc = await chapterUpdate.executeCommand('updateStatus')

    const ukpmc = await this.ukpmcMatchesPmcBook()
    const openAccess = await this.openAccessMatchesPmcBook()

    if (toc.bookId) {
      book.metadata.openAccess = openAccess || false
      book.settings.UKPMC = ukpmc || false

      const domain = new Domain({
        id: book.id,
        input: book,
        transaction: this.transaction,
        user: this.userId,
      })

      try {
        await domain.executeCommand('update')
      } catch (error) {
        // eslint-disable-next-line no-return-await
        return await this.publishFailed(toc, {
          category: 'domainUpdateFailed',
          error,
        })
      }
    } else if (toc.collectionId) {
      collection.metadata.openAccess = openAccess || false
      collection.settings.UKPMC = ukpmc || false

      const domain = new Domain({
        id: collection.id,
        input: collection,
        transaction: this.transaction,
      })

      try {
        await domain.executeCommand('updateCollection')
      } catch (error) {
        // eslint-disable-next-line no-return-await
        return await this.publishFailed(toc, {
          category: 'domainUpdateFailed',
          error,
        })
      }
    }

    // logger.info(`Start sending Toc xml to be previewed`)
    const domainToc = new Domain({ toc })
    await domainToc.executeCommand('sendTocPMC')

    return toc
  }

  async publishFailed(toc, { error, category }) {
    const TocService = require('../toc')

    const chapterUpdate = new TocService({
      status: 'publish-failed',
      object: toc,
      transaction: this.transaction,
      currentUser: this.userId,
    })

    await chapterUpdate.executeCommand('updateStatus')

    await Errors.query(this.transaction).insert({
      noticeTypeName:
        category === 'agreementChecks'
          ? 'Agreements Check 1'
          : 'Domain Service',
      errorType: 'system',
      errorCategory:
        category === 'agreementChecks' ? 'agreement-check' : 'domain-error',
      assignee: 'PMC',
      severity: 'error',
      message: error.toString(),
      objectId: toc.id,
      jobId: crypto.randomBytes(3).toString('hex'),
    })

    return false
  }

  async hasExactlyOneAgreement({ domain, collectionId }) {
    // Check Book Agreement

    const agreement = await agreementRequest(domain)

    if (agreement.status === 200) {
      this.bookAgreements = agreement.data.results

      const bookMainAgreements = this.bookAgreements.filter(result =>
        UNIQUE_ACCEPTABLE_AGGREMENTS.includes(
          result.agreement.agreement_type.title,
        ),
      )

      if (bookMainAgreements.length > 1) {
        throw new NCBIError(ERROR_MESSAGE)
      }

      let collection = null
      let collectionMainAgreements = []

      if (collectionId) {
        collection = await Collection.query(this.transaction).findOne({
          id: collectionId,
        })
      }

      if (collection) {
        // Check Collection of the Book Agreement
        const {
          data: { results: collectionResult },
        } = await agreementRequest(collection.domain)

        this.collectionAgreements = collectionResult

        collectionMainAgreements = this.collectionAgreements.filter(result =>
          UNIQUE_ACCEPTABLE_AGGREMENTS.includes(
            result.agreement.agreement_type.title,
          ),
        )

        if (collectionMainAgreements.length > 1) {
          throw new NCBIError(ERROR_MESSAGE)
        }
      }

      if (
        bookMainAgreements.length === 0 &&
        collectionMainAgreements.length === 0
      ) {
        throw new NCBIError(ERROR_MESSAGE)
      }

      return true
    }

    if (agreement.status === 401) {
      throw new NCBIError('Unathorized Access')
    }

    if (agreement.status === 404) {
      throw new UserInputError("Agreement couldn't be found")
    }

    if (agreement.status === 500) {
      throw new NCBIError('Get ncbi Agreement Request failed')
    }

    throw new NCBIError('An Error occurred failed')
  }

  async openAccessMatchesPmcBook() {
    let filteredOAagreements = this.bookAgreements.find(
      result => result.agreement.agreement_type.title === 'Access Amendment',
    )

    filteredOAagreements =
      filteredOAagreements ||
      this.bookAgreements.find(
        result => result.agreement.agreement_type.title === 'PMCI Permission',
      )

    filteredOAagreements =
      filteredOAagreements ||
      this.bookAgreements.find(result =>
        UNIQUE_ACCEPTABLE_AGGREMENTS.includes(
          result.agreement.agreement_type.title,
        ),
      )

    let entireJournalOpenAccess = false

    if (filteredOAagreements) {
      entireJournalOpenAccess = !!(
        filteredOAagreements.open_access.toLowerCase() === 'all' ||
        filteredOAagreements.open_access.toLowerCase() === 'yes'
      )
    }

    return entireJournalOpenAccess
  }

  async ukpmcMatchesPmcBook() {
    let filteredAgreements = this.bookAgreements.find(
      result => result.agreement.agreement_type.title === 'PMCI Permission',
    )

    filteredAgreements =
      filteredAgreements ||
      this.bookAgreements.find(
        result => result.agreement.agreement_type.title === 'Access Amendment',
      )

    filteredAgreements =
      filteredAgreements ||
      this.bookAgreements.find(result =>
        UNIQUE_ACCEPTABLE_AGGREMENTS.includes(
          result.agreement.agreement_type.title,
        ),
      )

    return (
      filteredAgreements &&
      filteredAgreements.sites.toLowerCase().includes('ukpmc')
    )
  }
}

module.exports = PublishComponent
