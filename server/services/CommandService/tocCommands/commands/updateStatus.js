/* eslint-disable global-require */
const { FileVersion, SourceConvertedFile, Toc } = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

class UpdateStatus {
  constructor(data) {
    this.object = data.object
    this.status = data.status
    this.transaction = data.transaction
    this.currentUser = data.currentUser
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Update Status of Book/Collection and Toc Command'
  }

  async run() {
    const [convertedFile] = await Toc.latestComponentFiles(
      this.object,
      'converted',
      this.transaction,
    )

    let filesUpdate = []
    let sourceFiles = []

    if (convertedFile) {
      sourceFiles = await SourceConvertedFile.query(this.transaction).where({
        convertedId: convertedFile.id,
      })

      filesUpdate = (sourceFiles || []).map(f => f.sourceId)
    } else {
      sourceFiles = await Toc.latestComponentFiles(
        this.object,
        'source',
        this.transaction,
      )

      filesUpdate = sourceFiles.map(f => f.id)
    }

    if (convertedFile) {
      filesUpdate = filesUpdate.concat([convertedFile.id])
    }

    if (this.status === 'published') {
      await Toc.query(this.transaction)
        .patch({ publishedDate: new Date().toISOString() })
        .findOne({ id: this.object.id })
    }

    const toc = await Toc.query(this.transaction)
      .patch({ status: this.status })
      .findOne({ id: this.object.id })
      .returning('*')

    logger.info(
      `Toc Status ${this.object.id}, with title: ${this.object.title} has been updated successfully with status: ${this.status} by user id: ${this.currentUser}`,
    )

    await FileVersion.query(this.transaction)
      .patch({ status: this.status })
      .whereIn('id', filesUpdate)

    return toc
  }
}

module.exports = UpdateStatus
