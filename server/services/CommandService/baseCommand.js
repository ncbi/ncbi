const { UserInputError } = require('apollo-server-express')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const NCBIError = require('../NCBIError')

const logger = getRawLogger()

class BaseCommand {
  constructor(data, commandPool) {
    this.data = data
    this.commandPool = commandPool
  }

  async executeCommand(command) {
    let commandEntity = null
    let response = {}

    commandEntity = new this.commandPool[command](this.data)

    let validated = false

    try {
      logger.info(`Start execution of ${commandEntity.title()} - ${command}`)

      validated = !commandEntity.validate || (await commandEntity.validate())

      response = await commandEntity.run()
    } catch (e) {
      logger.error(e)
      if (!validated) throw new Error(e.message)

      if (e instanceof UserInputError || e instanceof NCBIError) {
        throw new Error(
          `Running command: ${commandEntity.title()} Failed. Message: `,
          e.message,
        )
      } else {
        throw new Error(`Running command: ${commandEntity.title()} Failed`)
      }
    }

    return response
  }
}

module.exports = BaseCommand
