/* eslint-disable no-return-await */
const { Organisation, Team } = require('@pubsweet/models')
const { UserInputError } = require('apollo-server-express')

const { pick } = require('lodash')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()
const { publisherRequest } = require('../../../axiosService')
const NCBIError = require('../../../NCBIError')

class CreateCommand {
  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Publisher Create Command'
  }

  constructor(data) {
    this.data = data.input
    this.userId = data.userId
  }

  async run() {
    const { status, data } = await publisherRequest(
      'publishers/',
      'POST',
      pick(this.data, ['name', 'abbreviation']),
    )

    if (status === 201) {
      const organisation = await Organisation.create({
        ...this.data,
        publisherId: data.body.id.toString(),
      })

      logger.info(
        `Organization ${organisation.name} is created by user ${this.userId}`,
      )

      if (this.data.teams) {
        await Promise.all(
          this.data.teams.map(async team => {
            const foundTeam = await Team.query().findOne({
              objectId: organisation.id,
              objectType: 'Organisation',
              role: team.role,
            })

            await Team.query()
              .upsertGraphAndFetch(
                {
                  id: foundTeam.id,
                  ...team,
                },
                { eager: 'members.user.teams', relate: true, unrelate: false },
              )
              .eager('members.user.teams')
          }),
        )
      }

      return organisation
    }

    if (status === 400) {
      throw new UserInputError('Organization invalid User Input', data.body)
    }

    if (status === 500) {
      throw new NCBIError(
        'Creating ncbi Organization Request failed ',
        data.body,
      )
    }

    return false
  }
}

module.exports = CreateCommand
