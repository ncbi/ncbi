/* eslint-disable no-return-await */
const { publisherRequest } = require('../../../axiosService')

class GetPublisher {
  constructor(data) {
    this.data = data
  }

  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Publisher Get Publisher Command'
  }

  async run() {
    let publisherId = ''

    if (this.data.id) {
      publisherId = this.data.id
    }

    return await publisherRequest(`publishers/${publisherId}`, 'GET')
  }
}

module.exports = GetPublisher
