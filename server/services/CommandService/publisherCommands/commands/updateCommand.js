/* eslint-disable no-param-reassign */
const { UserInputError } = require('apollo-server-express')

const flatten = require('lodash/flatten')
const omit = require('lodash/omit')

const { Organisation, Team } = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const { publisherRequest } = require('../../../axiosService')
const NCBIError = require('../../../NCBIError')

class UpdateCommand {
  // eslint-disable-next-line class-methods-use-this
  title() {
    return 'Publisher Update Command'
  }

  constructor(data) {
    this.data = data.input
    this.userId = data.userId
  }

  async run() {
    const updatedOrg = await Promise.all(
      // eslint-disable-next-line consistent-return
      this.data.map(async organisation => {
        const { publisherId } = await Organisation.query().findOne({
          id: organisation.id,
        })

        const { data, status } = await publisherRequest(
          `publishers/${publisherId}/`,
          'PATCH',
          {
            name: organisation.name,
          },
        )

        if (status === 204) {
          const patchData = omit(organisation, 'teams')

          const organization = await Organisation.query()
            .patch(patchData)
            .where('id', '=', organisation.id)
            .returning('*')

          logger.info(
            `Organization with id ${organization.id} is created by user ${this.userId}. `,
          )

          if (organisation.teams) {
            await Promise.all(
              organisation.teams.map(async team => {
                const foundTeam = await Team.query().findOne({
                  objectId: organisation.id,
                  objectType: 'Organisation',
                  role: team.role,
                })

                await Team.query()
                  .upsertGraphAndFetch(
                    {
                      ...team,
                      id: foundTeam.id,
                    },
                    {
                      eager: 'members.user.teams',
                      relate: true,
                      unrelate: false,
                    },
                  )
                  .eager('members.user.teams')
              }),
            )
          }

          return organization
        }

        if (status === 400) {
          throw new UserInputError('Organisation invalid User Input', data.body)
        }

        if (status === 500) {
          throw new NCBIError(
            'Organisation ncbi Book Request failed ',
            data.body,
          )
        }
      }),
    )

    return flatten(updatedOrg)
  }
}

module.exports = UpdateCommand
