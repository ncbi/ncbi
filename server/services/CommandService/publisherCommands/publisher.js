const BaseCommand = require('../baseCommand')

const { GetPublisher, CreateCommand, UpdateCommand } = require('./commands')

const COMMANDS = {
  getPublisher: GetPublisher,
  create: CreateCommand,
  update: UpdateCommand,
}

class Publisher extends BaseCommand {
  constructor(props) {
    super(props, COMMANDS)
  }
}

module.exports = Publisher
