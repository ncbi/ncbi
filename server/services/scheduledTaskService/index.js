/* eslint-disable no-await-in-loop */
const {
  cron,
  useTransaction,
  logger: { getRawLogger },
} = require('@coko/server')

const { uniqBy } = require('lodash')

const {
  BookComponent,
  Errors,
  NcbiNotificationMessage,
} = require('@pubsweet/models')

const { Domain, Chapter } = require('..')

const logger = getRawLogger()

class ScheduledTaskService {
  static async sendTocXML(crontab, options) {
    const task = cron.schedule(
      crontab,
      async () => {
        logger.info(`Start sending Toc xml to be previewed`)
        const domain = new Domain({})
        await domain.executeCommand('sendTocPMC')
      },
      options,
    )

    task.start()
  }

  static async forcefailChapters(crontab, options) {
    const task = cron.schedule(
      crontab,
      async () => {
        await useTransaction(async trx => {
          logger.info(
            `Checking for statuses Converting, Loading preview, publishing`,
          )

          const stuckBc = await BookComponent.query(trx)
            .orWhere({
              status: 'converting',
            })
            .orWhere({
              status: 'loading-preview',
            })
            .orWhere({
              status: 'publishing',
            })

          logger.info(`stuck bookComponents: ${stuckBc.length}`)

          logger.info(`unique bookComponents: ${uniqBy(stuckBc, 'id').length}`)

          // eslint-disable-next-line no-plusplus
          for (let i = 0; i < stuckBc.length; i++) {
            const { alias, status, id } = stuckBc[i]

            logger.info(`*** Checking BookComponent ${id} / bcms${alias} ***`)

            const notProcessed = await NcbiNotificationMessage.expiredNotifications(
              id,
              trx,
            )

            if (notProcessed.length) {
              await NcbiNotificationMessage.query(trx)
                .patch({ proccessed: true, timeout: true })
                .whereIn(
                  'id',
                  notProcessed.map(np => np.id),
                )

              await Errors.query(trx).insert(
                notProcessed.map(({ jobId, objectId }) => ({
                  assignee: 'publisher',
                  severity: 'error',
                  message:
                    'The process failed because it took too long. Please try again or contact an NCBI System Admin.',
                  noticeTypeName: 'Failed: Timeout',
                  errorType: 'failed',
                  errorCategory: 'Timeout',
                  objectId,
                  jobId,
                })),
              )
            }

            const notifications = await NcbiNotificationMessage.activeNotifications(
              id,
              trx,
            )

            logger.info(`Expired Notifications: ${notProcessed.length}`)

            logger.info(`Active Notifications: ${notifications.length}`)

            if (notProcessed.length > 0 && notifications.length === 0) {
              const chapterUpdate = new Chapter({
                status: status === 'publishing' ? 'publish-failed' : 'failed',
                object: stuckBc[i],
                transaction: trx,
              })

              await chapterUpdate.executeCommand('updateStatus')
            }
          }
        })
      },
      options,
    )

    task.start()
  }
}

module.exports = ScheduledTaskService
