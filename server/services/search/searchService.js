const { get, isEmpty, isFunction, upperFirst } = require('lodash')
const searchOperators = require('./searchOperators')

class SearchService {
  constructor(Model, options = {}) {
    this.debug = options.debug || false
    this.model = Model
    this.skip = options.skip || 0
    this.take = options.take || -1
    this.converters = options.converters || {}
    this.filter = this.parseOptionsToFilter(options.filter || [])
    this.join = options.join || []
    this.groupBy = options.groupBy || null
    this.sort = options.sort || null
    this.having = options.having || {}
    this.textFullSearch = options.textFullSearch || null
    this.or = options.or

    if (Model.query === undefined) {
      // Assume that model is already a query
      this.query = Model
      this.model = this.query.modelClass()
    } else {
      // Assume that Model is a subclass of BaseModel
      this.query = Model.query()
    }
  }

  get metadata() {
    return {
      skip: this.skip,
      take: this.take,
      total: this.total[0].count,
    }
  }

  /* eslint-disable-next-line class-methods-use-this */
  parseCriterion(criterion) {
    if (isEmpty(criterion.operator) && !Array.isArray(criterion.or)) return null

    if (criterion.or) {
      const orCriteria = this.parseOptionsToFilter(criterion.or)

      const orFn = builder => {
        orCriteria.reduce((accumulator, current, index) => {
          const fn = Object.keys(current)[0]
          const fnArgs = current[fn]

          if (index === 0) return builder[fn](...fnArgs)
          // get the equivalent "orWhereXYZ" function for fn
          return builder[`or${upperFirst(fn)}`](...fnArgs)
        }, builder)
      }

      return { where: orFn }
    }

    // Get the searchOperators function matching criterion.operator.<keys[0]>
    const operator = Object.keys(criterion.operator)[0]
    const operatorFn = get(searchOperators, operator, null)

    if (isFunction(operatorFn)) {
      if (
        criterion.field instanceof String &&
        this.converters[criterion.field]
      ) {
        // eslint-disable-next-line no-param-reassign
        criterion.operator[operator] = this.converters[criterion.field](
          criterion.operator[operator],
        )
      }

      return operatorFn(criterion)
    }

    return criterion
  }

  parseOptionsToFilter(filter) {
    const parsed = filter
      .map(f => this.parseCriterion(f))
      .filter(f => f != null)

    return parsed
  }

  select(select) {
    this.query = this.query.clearSelect()

    if (isEmpty(select)) {
      this.query.select()
    } else {
      this.query.select(select)
    }

    return this
  }

  joinClause() {
    if (this.join.length > 0) {
      const reducer = (accumulator, currentValue) => {
        const fn = Object.keys(currentValue)[0]
        return accumulator[fn].apply(this.query, currentValue[fn])
      }

      this.query = this.join.reduce(reducer, this.query)
    }

    return this
  }

  whereClause() {
    if (this.filter.length > 0) {
      const reducer = (accumulator, currentValue) => {
        const fn = Object.keys(currentValue)[0]
        let whereArgs = currentValue[fn]
        if (isFunction(whereArgs)) whereArgs = [whereArgs]

        return accumulator[fn].apply(this.query, whereArgs)
      }

      this.query = this.filter.reduce(reducer, this.query)
    }

    return this
  }

  limit() {
    if (this.take < 0) return this
    this.query = this.query.limit(this.take)
    return this
  }

  offset() {
    if (this.take < 0) return this
    this.query = this.query.offset(this.skip)
    return this
  }

  groupby() {
    if (!this.groupBy) return this
    this.query = this.query.groupBy(this.groupBy)
    return this
  }

  rawHaving() {
    if (isEmpty(this.having)) return this
    this.query = this.query.havingRaw(this.having.query, this.having.value)
    return this
  }

  order() {
    if (!isEmpty(this.sort)) {
      this.query = this.query.orderBy(this.sort.field[0], this.sort.direction)
    }

    return this
  }

  async search(select = {}) {
    this.joinClause()
    this.whereClause()
    this.total = await this.query.count('*')

    this.select(select).groupby().rawHaving().offset().limit().order()

    return this.debug ? this.query.debug() : this.query
  }
}

module.exports = SearchService
