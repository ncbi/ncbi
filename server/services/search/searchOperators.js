const { replace } = require('lodash')

const parseJsonField = (criterion, operator) => {
  const pieces = criterion.field.split('.')
  const lastPiece = pieces.pop()
  const objForQuery = {}
  objForQuery[lastPiece] = criterion.operator[operator]

  return {
    whereJsonSupersetOf: [pieces.join(':'), objForQuery],
  }
}

/** searchOperators
 * graphql search operators
 * see "common.graphql: OperatorInput"
 */
const searchOperators = {
  /** fullSearch
   * Applies complex free text search using postgresql full search syntax.
   * See: https://www.postgresql.org/docs/current/textsearch-intro.html#TEXTSEARCH-MATCHING
   */
  fullSearch: criterion => {
    if (criterion.operator.fullSearch === '') {
      return null
    }

    return {
      whereRaw: [
        'to_tsvector(??) @@ to_tsquery(?)',
        [criterion.field, `${criterion.operator.fullSearch}`],
      ],
    }
  },
  /** eq
   * Applies "=" condition.
   */
  eq: criterion => {
    if (criterion.operator.eq === '' || criterion.operator.eq === null) {
      return null
    }

    if (criterion.field instanceof String && criterion.field.includes('.')) {
      return parseJsonField(criterion, 'eq')
    }

    return {
      where: [criterion.field, criterion.operator.eq],
    }
  },
  /** noteq
   * Applies "!=" condition.
   */
  noteq: criterion => {
    return {
      whereNot: [criterion.field, criterion.operator.noteq],
    }
  },
  /** is
   * Applies "IS" condition.
   * Eg: title IS NULL
   */
  is: criterion => {
    if (criterion.field instanceof String && criterion.field.includes('.')) {
      return parseJsonField(criterion, 'is')
    }

    return {
      where: [criterion.field, criterion.operator.is],
    }
  },
  /** in
   * Applies "IN" condition.
   * Eg: title IN ('title a', 'title b']
   */
  in: criterion => {
    return {
      whereIn: [criterion.field, criterion.operator.in],
    }
  },
  /** notin
   * Applies "not in" condition.
   * Eg: title NOT IN ['title a', 'title b']
   */
  notin: criterion => {
    return {
      whereNotIn: [criterion.field, criterion.operator.notin],
    }
  },
  /** le
   * Applies "<=" condition.
   */
  le: criterion => {
    return {
      where: [criterion.field, '<=', criterion.operator.le],
    }
  },
  /** lt
   * Applies "<" condition.
   */
  lt: criterion => {
    return {
      where: [criterion.field, '<', criterion.operator.lt],
    }
  },
  /** ge
   * Applies ">=" condition.
   */
  ge: criterion => {
    return {
      where: [criterion.field, '>=', criterion.operator.ge],
    }
  },
  /** gt
   * Applies ">" condition.
   */
  gt: criterion => {
    return {
      where: [criterion.field, '>', criterion.operator.gt],
    }
  },
  /** contains
   * Applies "ILIKE" condition to find any match in a string.
   * Eg: title ILIKE '%partial title%'
   */
  contains: criterion => {
    if (criterion.operator.contains === '') {
      return null
    }

    return {
      where: [criterion.field, `ilike`, `%${criterion.operator.contains}%`],
    }
  },
  /** notContains
   * Applies "NOT ILIKE" condition to block any match in a string.
   * Eg: title NOT ILIKE '%partial title%'
   */
  notContains: criterion => {
    if (criterion.operator.notContains === '') {
      return null
    }

    return {
      whereNot: [
        criterion.field,
        `ilike`,
        `%${criterion.operator.notContains}%`,
      ],
    }
  },
  /** containsAll
   * Applies a full text search to tokens in a whitespace separated string.
   * A match requires that EVERY token is matched at least once within a string.
   * Complex searches are not allowed and any postgresql full search syntax will
   * be treated as whitespace.
   */
  containsAll: criterion => {
    return {
      whereRaw: [
        'to_tsvector(??) @@ to_tsquery(?)',
        // Replace full search syntax with "&"; ie: "and" tokens together
        [
          criterion.field,
          `${replace(
            criterion.operator.containsAll.trim(),
            /([\s&|]|:\*)+/g,
            '&',
          )}`,
        ],
      ],
    }
  },
  /** containsAny
   * Applies a full text search to tokens in a whitespace separated string.
   * A match requires that AT LEAST ONE token is matched at least once within a
   * string.
   * Complex searches are not allowed and any postgresql full search syntax will
   * be treated as whitespace.
   */
  containsAny: criterion => {
    if (criterion.operator.containsAny === '') {
      return null
    }

    return {
      whereRaw: [
        'to_tsvector(??) @@ to_tsquery(?)',
        // Replace full search syntax with "|"; ie: "or" tokens together
        [
          criterion.field,
          `${replace(
            criterion.operator.containsAny.trim(),
            /([\s&|]|:\*)+/g,
            '|',
          )}`,
        ],
      ],
    }
  },
  /** containsNone
   * Applies a full text search to tokens in a whitespace separated string.
   * A match requires that NO tokens are matched within a string.
   * Complex searches are not allowed and any postgresql full search syntax will
   * be treated as whitespace.
   */
  containsNone: criterion => {
    return {
      whereRaw: [
        'to_tsvector(??) @@ to_tsquery(?) IS NOT TRUE',
        // Replace full search syntax with "|"; ie: "or" tokens together
        [
          criterion.field,
          `${replace(
            criterion.operator.containsNone.trim(),
            /([\s&|]|:\*)+/g,
            '|',
          )}`,
        ],
      ],
    }
  },
  /** beginsWith
   * Applies "ILIKE" condition to find match at the start of a string.
   * Eg: title ILIKE 'partial title%'
   */
  beginsWith: criterion => {
    if (criterion.operator.beginsWith === '') {
      return null
    }

    return {
      where: [criterion.field, `ilike`, `${criterion.operator.beginsWith}%`],
    }
  },
  /** endsWith
   * Applies "ILIKE" condition to find match at the end of a string.
   * Eg: title ILIKE '%partial title'
   */
  endsWith: criterion => {
    if (criterion.operator.endsWith === '') {
      return null
    }

    return {
      where: [criterion.field, `ilike`, `%${criterion.operator.endsWith}`],
    }
  },
}

module.exports = searchOperators
