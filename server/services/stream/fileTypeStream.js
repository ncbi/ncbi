const { PassThrough, Transform } = require('stream')
const fileType = require('file-type')

const kResult = Symbol('result')
const kStream = Symbol('stream')

class FileType extends Transform {
  constructor() {
    super()

    this[kStream] = new PassThrough()

    this[kResult] = fileType.fromStream(this[kStream]).then(
      value => {
        this[kStream] = null
        this.emit('file-type', value || null)
        return value || null
      },
      () => {
        this[kStream] = null
        this.emit('file-type', null)
        return null
      },
    )
  }

  fileTypePromise() {
    return this[kResult]
  }

  // eslint-disable-next-line no-underscore-dangle
  _transform(chunk, _, cb) {
    if (this[kStream] != null) {
      this[kStream].write(chunk)
    }

    cb(null, chunk)
  }

  // eslint-disable-next-line no-underscore-dangle
  _flush(cb) {
    if (this[kStream] != null) {
      this[kStream].end(() => cb(null))
    } else {
      cb(null)
    }
  }
}

module.exports = FileType
