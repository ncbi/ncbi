const { Errors, BookComponent } = require('@pubsweet/models')
const decoder = require('html-entities')
const pick = require('lodash/pick')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const Error = require('./error')

const notify = require('../../notify')

const logger = getRawLogger()

class TaggingError extends Error {
  errorCategory = 'tagging-errors'
  async handleError(result, trx) {
    await this.saveErrors(result, trx)

    if (result) {
      notify('errors', {
        bookComponentId: result.id,
        bookComponentTitle: result.title,
        errors: this.notification.notification.data.notices,
        ownerId: result.ownerId,
        type: 'tagging',
      })
    }
  }

  async saveErrors(result, trx) {
    const {
      data: { status, notices, id },
    } = this.notification.notification

    logger.info(`Kafka Start saving Errors for BookComponent id: ${result.id}`)

    if (status === 3 && notices.length === 0) {
      notices.push({
        notice_type_name: 'system',
        error_type: 'Tagging Error',
        assignee: 'PMC',
        severity: 'error',
        message: 'The converted file name must match the source file name.',
      })
    }

    await Errors.query(trx).insert(
      notices.map(notice =>
        Object.assign(pick(notice, ['assignee', 'severity']), {
          message: decoder.decode(notice.message),
          noticeTypeName: notice.notice_type_name,
          errorType: notice.error_type,
          errorCategory: this.errorCategory,
          objectId: result.id,
          jobId: id,
        }),
      ),
    )

    /** We have normalize only the BookComponent Errors
     *  For that we will use ContentProccess to check the type of the object
     *  and if it is bookComponent then we will update the ErrorSummary
     * */
    if (result.type === 'bookComponent') {
      await BookComponent.updateErrorSummary(result.id, trx)
    }

    logger.info(`kafka Errors Saved for BookComponent alias: ${result.alias} `)
  }
}

module.exports = TaggingError
