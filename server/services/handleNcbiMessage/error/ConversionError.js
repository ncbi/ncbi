const notify = require('../../notify')

const Error = require('./error')

class ConversionError extends Error {
  errorCategory = 'conversion-errors'
  async handleError(result, trx) {
    await this.saveErrors(result, trx)

    if (result) {
      notify('errors', {
        bookComponentId: result.id,
        bookComponentTitle: result.title,
        errors: this.notification.data.notices,
        ownerId: result.ownerId,
        type: 'conversion',
      })
    }
  }
}

module.exports = ConversionError
