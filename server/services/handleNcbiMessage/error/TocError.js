const { Errors } = require('@pubsweet/models')
const decoder = require('html-entities')
const pick = require('lodash/pick')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const Error = require('./error')

const notify = require('../../notify')

const logger = getRawLogger()

class PreviewError extends Error {
  errorCategory = 'loading-errors'
  async handleError(result, trx) {
    await this.saveErrors(result, trx)

    if (result) {
      notify('errors', {
        bookComponentId: result.id,
        bookComponentTitle: result.title,
        errors: this.notification.data.notices,
        ownerId: result.ownerId,
        type: 'preview',
      })
    }
  }

  async saveErrors(result, trx) {
    const {
      data: { status, notices, id },
    } = this.notification

    logger.info(`Kafka Start saving Errors for BookComponent id: ${result.id}`)

    if (status === 3 && notices.length === 0) {
      notices.push({
        notice_type_name: 'system',
        error_type: 'system',
        assignee: 'system',
        severity: 'error',
        message:
          'System error: please contact NCBI Bookshelf System Admin by email at booksauthors@ncbi.nlm.nih.gov or by BCMS chat mentioning @Sysadmin.',
      })
    }

    await Errors.query(trx).insert(
      notices.map(notice =>
        Object.assign(pick(notice, ['assignee', 'severity']), {
          message: decoder.decode(notice.message),
          noticeTypeName: notice.notice_type_name,
          errorType: notice.error_type,
          errorCategory: this.errorCategory,
          objectId: result.id,
          jobId: id,
        }),
      ),
    )

    logger.info(`kafka Errors Saved for BookComponent id: ${result.id} `)
  }
}

module.exports = PreviewError
