const { Errors } = require('@pubsweet/models')
const decoder = require('html-entities')
const pick = require('lodash/pick')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const ContentProccess = require('../../ContentProccess')

const logger = getRawLogger()

class Error {
  constructor(notification) {
    this.notification = notification
  }

  async saveErrors(result, trx) {
    const {
      data: { status, notices, id },
    } = this.notification

    logger.info(`Kafka Start saving Errors for BookComponent id: ${result.id}`)

    if (status === 3 && notices.length === 0) {
      notices.push({
        notice_type_name: 'system',
        error_type: 'system',
        assignee: 'system',
        severity: 'error',
        message:
          'System error: please contact NCBI Bookshelf System Admin by email at booksauthors@ncbi.nlm.nih.gov or by BCMS chat mentioning @Sysadmin.',
      })
    }

    await Errors.query(trx).insert(
      notices.map(notice =>
        Object.assign(pick(notice, ['assignee', 'severity']), {
          message: decoder.decode(notice.message),
          noticeTypeName: notice.notice_type_name,
          errorType: notice.error_type,
          errorCategory: this.errorCategory,
          objectId: result.id,
          jobId: id,
        }),
      ),
    )

    /** We have normalize only the BookComponent Errors
     *  For that we will use ContentProccess to check the type of the object
     *  and if it is bookComponent then we will update the ErrorSummary
     * */
    const contentProccess = await ContentProccess.findBestContent(
      result.id,
      trx,
    )

    if (contentProccess.type === 'chapterProccessed') {
      await contentProccess.model.updateErrorSummary(result.id, trx)
    }

    logger.info(`kafka Errors Saved for BookComponent alias: ${result.alias} `)
  }
}

module.exports = Error
