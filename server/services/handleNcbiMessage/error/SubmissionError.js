const Error = require('./error')

class SubmissionError extends Error {
  errorCategory = 'submission-errors'
  async handleError(result, trx) {
    await this.saveErrors(result, trx)
  }
}

module.exports = SubmissionError
