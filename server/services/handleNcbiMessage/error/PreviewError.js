const notify = require('../../notify')

const Error = require('./error')

class PreviewError extends Error {
  errorCategory = 'loading-errors'
  async handleError(result, trx) {
    await this.saveErrors(result, trx)

    if (result) {
      notify('errors', {
        bookComponentId: result.id,
        bookComponentTitle: result.title,
        errors: this.notification.data.notices,
        ownerId: result.ownerId,
        type: 'preview',
      })
    }
  }
}

module.exports = PreviewError
