/* eslint-disable no-return-await */
const { BookComponent } = require('@pubsweet/models')

const set = require('lodash/set')
const bookPartWrapper = require('../xmlBaseModel/bookPartWrapper')
const { Chapter } = require('..')

class ChapterProcessed {
  constructor(workflow, notification) {
    this.type = 'chapter'
    this.notification = notification
    this.workflow = workflow
    this.id = notification.notification.objectId

    this.parser = bookPartWrapper
    this.hasTaggingError = false
  }

  get remoteFolderPath() {
    if (this.workflow.remoteFolderPath) return this.workflow.remoteFolderPath
    return `/convert/${this.workflow.type}/chapter/out`
  }

  setDataStatus(status) {
    set(this.notification, 'notification.data.status', status)
  }

  async isTaggingError(transaction, convertedFileName) {
    const latestBookFile = await BookComponent.latestComponentFiles(
      { id: this.notification.notification.objectId },
      'source',
      transaction,
    )

    this.hasTaggingError = latestBookFile
      .map(
        file =>
          file.file.name.replace(/\.[^/.]+$/, '') ===
          convertedFileName.replace(/\.[^/.]+$/, ''),
      )
      .includes(false)

    return this.hasTaggingError
  }

  async update(parsedData, status, transaction) {
    const {
      id,
      metadata,
      ownerId,
      title,
      componentType,
      abstract,
    } = await BookComponent.query(transaction)
      .findOne({ id: this.id })
      .throwIfNotFound()

    const convertedFileName =
      parsedData == null
        ? ''
        : parsedData.files.find(file => file.category === 'converted').name

    let updatedStatus = status

    const isErrorResult = await this.isTaggingError(
      transaction,
      convertedFileName,
    )

    if (isErrorResult) {
      this.setDataStatus(3) // set error
      updatedStatus = 'tagging-errors'
    }

    const chapter = parsedData
      ? new Chapter({
          transaction,
          abstract: parsedData.metadata.abstract
            ? parsedData.metadata.abstract.text
            : '',
          componentType: parsedData.type,
          files: parsedData.files.map(file => ({ ...file, ownerId })),
          id,
          metadata: {
            ...metadata,
            abstractId: parsedData.metadata.abstract
              ? parsedData.metadata.abstract.id
              : null,
            abstractTitle: parsedData.metadata.abstract
              ? parsedData.metadata.abstract.title
              : null,
            book_component_id: parsedData.id,
            date_created: await parsedData.metadata.date_created,
            date_updated: await parsedData.metadata.date_updated,
            date_revised: await parsedData.metadata.date_revised,
            date_publication: await parsedData.metadata.date_publication,
            date_publication_format: await parsedData.metadata
              .date_publication_format,
            fileId: metadata.fileId,
            filename: metadata.filename,
            section_titles: (
              (
                (parsedData.body || parsedData.namedBookPartBody).section || []
              ).map(sec => sec.title) || []
            ).filter(secTitle => secTitle !== null),
            section_details:
              ((parsedData.body || parsedData.namedBookPartBody).section || [])
                .map(sec => ({
                  id: sec.id,
                  title: sec.title || '',
                  label: sec.label,
                }))
                .filter(sec => sec.id !== null) || [],
            sub_title: parsedData.metadata.sub_title || metadata.sub_title,
            alt_title: parsedData.metadata.alt_title || metadata.alt_title,
            chapter_number:
              parsedData.metadata.chapter_number || metadata.chapter_number,
            /// Use the flatten sets from above
            collaborativeAuthors: metadata.collaborativeAuthors,
            author: metadata.author,
            editor: metadata.editor,
            hideInTOC: parsedData.hideInTOC || metadata.hideInTOC,
          },
          status: updatedStatus,
          title: parsedData.metadata.title,
        })
      : new Chapter({
          transaction,
          id,
          metadata,
          title,
          abstract,
          componentType,
          status,
        })

    const bc = await chapter.executeCommand('update')

    return bc
  }

  hasErrors() {
    return this.notification.hasErrors() || this.hasTaggingError
  }
}

module.exports = ChapterProcessed
