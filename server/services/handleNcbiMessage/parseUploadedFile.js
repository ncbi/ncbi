/* eslint-disable no-return-await */
/* eslint-disable class-methods-use-this */

const { pick, flatten } = require('lodash')

const { Book: BookModel, Collection } = require('@pubsweet/models')

const { pubsubManager } = require('pubsweet-server')

const { getPubsub } = pubsubManager

const CONVERSION_COMPLETED = 'CONVERSION_COMPLETED'

const {
  logger: { getRawLogger },
} = require('@coko/server')

const Chapter = require('../CommandService/chapterCommands/chapter')

const BookPartWrapper = require('../xmlBaseModel/bookPartWrapper')
const Book = require('../xmlBaseModel/book')
const FileService = require('../file/fileService')
const OrderService = require('../orderService/orderService')
const ContentProccess = require('../ContentProccess')
const XmlFactory = require('../xmlBaseModel/xmlFactory')

const logger = getRawLogger()
class ParseUploadedFile {
  async handle({ data: { transaction, category, files, object } }) {
    if (category === 'converted') {
      const pubsub = await getPubsub()
      const content = await FileService.getSourceToBuffer(files.id, transaction)
      let parsedData = ''

      const contentProccess = await ContentProccess.findBestContent(
        object,
        transaction,
      )

      const { metadata } = contentProccess.object

      const book = await contentProccess.getBook(transaction)

      if (contentProccess.type === 'wholeBook') {
        parsedData = await XmlFactory.xmlToModel(
          content.toString(),
          Book,
          {},
          { forceStream: true },
        )

        const openAccessLicense =
          parsedData.metadata.openAccessLicense === 'open-access'

        const pubDate = await parsedData.metadata.pubDate

        await BookModel.query(transaction)
          .patch({
            metadata: {
              ...book.metadata,
              ...pick(parsedData.metadata, [
                'doi',
                'volume',
                'pub_name',
                'pub_loc',
                'licenseType',
                'licenseStatement',
                'copyrightStatement',
                'otherLicenseType',
              ]),
              pubDate: {
                ...pubDate,
                ...pick(parsedData.metadata, ['dateType', 'publicationFormat']),
              },
              openAccessLicense,
            },
            subTitle: parsedData.metadata.sub_title,
            altTitle: parsedData.metadata.alt_title,
            edition: parsedData.metadata.edition,
            title: parsedData.metadata.title,
          })
          .findOne({ id: book.id })

        if (book.collectionId) {
          logger.info(`Order Collection for book: ${book.id}`)

          const collection = await Collection.query(transaction).findOne({
            id: book.collectionId,
          })

          await OrderService.model(collection, {
            transaction,
            exclude: itm => itm.settings.isCollectionGroup,
            deep: true,
          })
        }

        pubsub.publish(`${CONVERSION_COMPLETED}.${book.id}`, {
          id: book.id,
        })
      } else {
        parsedData = await XmlFactory.xmlToModel(
          content.toString(),
          BookPartWrapper,
          {},
          { forceStream: true },
        )

        logger.info(`Parsing Manually Uploaded Converted File`)

        let allAuthors = null
        let allEditors = null
        let allCollab = null

        if (parsedData.metadata.contributors) {
          // We need to combine authors, editors and collaborativeAuthors from across
          // possible multiple contrib-groups into one flat set
          //
          allAuthors =
            flatten(
              parsedData.metadata.contributors.map(cg => {
                return cg.author.map(ath => ({
                  ...ath,
                  affiliation: ath.affiliation ? ath.affiliation : [],
                }))
              }),
            ) || []

          allEditors =
            flatten(
              parsedData.metadata.contributors.map(cg => {
                return cg.editor.map(edt => ({
                  ...edt,
                  affiliation: edt.affiliation ? edt.affiliation : [],
                }))
              }),
            ) || []

          allCollab =
            flatten(
              parsedData.metadata.contributors.map(
                cg => cg.collaborativeAuthors,
              ),
            ) || null
        }

        const chapter = new Chapter({
          transaction,
          abstract: parsedData.metadata.abstract
            ? parsedData.metadata.abstract.text
            : '',
          componentType: parsedData.type,
          files: [],
          id: object.id,
          metadata: {
            ...metadata,
            abstractId: parsedData.metadata.abstract
              ? parsedData.metadata.abstract.id
              : null,
            abstractTitle: parsedData.metadata.abstract
              ? parsedData.metadata.abstract.title
              : null,
            book_component_id: parsedData.id,
            date_created: await parsedData.metadata.date_created,
            date_updated: await parsedData.metadata.date_updated,
            date_revised: await parsedData.metadata.date_revised,
            date_publication: await parsedData.metadata.date_publication,
            date_publication_format: await parsedData.metadata
              .date_publication_format,
            fileId: metadata.fileId,
            filename: metadata.filename,
            section_titles: (
              (
                (parsedData.body || parsedData.namedBookPartBody).section || []
              ).map(sec => sec.title) || []
            ).filter(secTitle => secTitle !== null),
            section_details:
              ((parsedData.body || parsedData.namedBookPartBody).section || [])
                .map(sec => ({
                  id: sec.id,
                  title: sec.title || '',
                  label: sec.label,
                }))
                .filter(sec => sec.id !== null) || [],
            sub_title: parsedData.metadata.sub_title || metadata.sub_title,
            alt_title: parsedData.metadata.alt_title || metadata.alt_title,
            chapter_number:
              parsedData.metadata.chapter_number || metadata.chapter_number,
            /// Use the flatten sets from above
            collaborativeAuthors: allCollab || metadata.collaborativeAuthors,
            author: allAuthors || metadata.author,
            editor: allEditors || metadata.editor,
            hideInTOC: parsedData.hideInTOC || metadata.hideInTOC,
          },
          title: parsedData.metadata.title,
        })

        const UpdatedBookComponent = await chapter.executeCommand('update')

        pubsub.publish(`${CONVERSION_COMPLETED}.${book.id}`, {
          conversionCompleted: UpdatedBookComponent,
        })
      }
    }
  }
}

module.exports = ParseUploadedFile
