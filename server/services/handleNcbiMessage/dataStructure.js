/* eslint-disable no-underscore-dangle */
class DataStructure {
  constructor(data) {
    this._data = data
  }

  get id() {
    return (
      this._data.job_id ||
      this._data.package_id ||
      this._data.session_id ||
      this._data.id
    )
  }

  get status() {
    return parseInt(this._data.status || 0, 10)
  }

  get remoteFiles() {
    return this._data.converted_files || this._data.package
  }

  exportData() {
    return {
      id: this.id,
      status: this.status,
      remoteFiles: this.remoteFiles,
      topic: this._data.topic,
      chapter: this._data.chapter,
      domain: this._data.domain,
      publisher: this._data.publisher,
      url: this._data.url,
      timestamp: this._data.timestamp,
      notices: this._data.notices,
    }
  }
}

module.exports = DataStructure
