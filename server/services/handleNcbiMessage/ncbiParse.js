/* eslint-disable no-underscore-dangle */
const { NcbiNotificationMessage } = require('@pubsweet/models')
const omit = require('lodash/omit')

const {
  logger: { getRawLogger },
  uuid,
} = require('@coko/server')

const DataStructure = require('./dataStructure')

const logger = getRawLogger()

class NcbiParse {
  constructor(topic) {
    this._data = {}
    this.topic = topic
    this.notification = null
  }

  async parseMessage(message) {
    try {
      if (message) {
        this._data = new DataStructure({
          ...JSON.parse(message),
          topic: this.topic,
        })
      }
    } catch (error) {
      logger.info(
        `kafka Couldn't Parsed Message ${JSON.stringify(
          error,
        )} message: ${message}`,
      )

      throw Error(`could not read message ${error}`)
    }

    if (this._data && this._data.status > 0) {
      logger.info(`kafka There was an error during ncbi conversion`)
    }

    this.notification = await this.saveNotification()
    return this.notification
  }

  async saveNotification() {
    const { topic, id } = this.data

    const notification = await NcbiNotificationMessage.query().findOne({
      jobId: id,
      proccessed: false,
    })

    if (notification) {
      // eslint-disable-next-line no-return-await
      return await NcbiNotificationMessage.query()
        .patch({ data: JSON.stringify(this.data), topic })
        .findOne({ jobId: id })
        .returning('*')
    }

    if (
      topic === 'submit_package_receipt' ||
      topic === 'submit_apex_package_receipt'
    ) {
      const incomingMessage = new NcbiNotificationMessage({
        jobId: id,
        data: this.data,
        topic,
      })

      // eslint-disable-next-line no-return-await
      return await incomingMessage.save()
    }

    throw Error(
      `Msg isn't for this app ${JSON.stringify(omit(this.data, ['notices']))}`,
    )
  }

  messageFile() {
    if (!this.data.remoteFiles) {
      return false
    }

    const { notification } = this

    const isChapter =
      notification.topic.includes('_book_chapter_') ||
      (notification.packageType !== 'wholeBookPdfConversion' &&
        notification.topic === 'submit_apex_package_receipt')

    const isWholeBook =
      notification.topic === 'convert_xml_book_receipt' ||
      (notification.packageType === 'wholeBookPdfConversion' &&
        notification.topic === 'submit_apex_package_receipt')

    const isXml = notification.topic.includes('xml')
    const isWord = notification.topic.includes('word')
    const isPdf = notification.topic === 'submit_apex_package_receipt'
    const isSubmit = notification.topic === 'submit_package_receipt'

    const filesToExtract = {
      remoteFile:
        isSubmit || isPdf
          ? `/submit/${notification.data.remoteFiles}`
          : '/convert',
      localFile: `/tmp/package-unzip-${uuid()}.zip`,
    }

    // eslint-disable-next-line no-nested-ternary
    const type = isChapter ? 'chapter' : isWholeBook ? 'book' : null

    if (isXml || isWord) {
      filesToExtract.remoteFile = isXml
        ? `${filesToExtract.remoteFile}/xml/${type}/out/${notification.data.remoteFiles}`
        : `${filesToExtract.remoteFile}/word/out/${notification.data.remoteFiles}`
    }

    return filesToExtract
  }

  hasErrors() {
    if (this._data.status !== 0) return true

    if (this._data && this._data.notices) {
      if (this._data.notices.length === 0) {
        return false
      }
    } else {
      return false
    }

    return (
      (this._data.notices || []).findIndex(
        notice => notice.severity.toLowerCase() === 'error',
      ) > -1
    )
  }

  get data() {
    return this._data.exportData()
  }

  // eslint-disable-next-line class-methods-use-this
  set data(value) {
    this._data = new DataStructure(value)
  }
}

module.exports = NcbiParse
