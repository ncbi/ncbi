/* eslint-disable no-return-await */
/* eslint-disable no-await-in-loop */
const {
  NcbiNotificationMessage,
  Book,
  Organisation,
  File,
  User,
} = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const { Chapter } = require('..')

// const SubmissionError = require('./error/SubmissionError')

const SubmitMeta = require('../xmlBaseModel/submitMeta')

const { PackageCollection, Domain } = require('..')

const FileService = require('../file/fileService')
const { model: Events } = require('../events/eventService')

class Submit {
  remoteFolderPath = '/submit'

  constructor(contentProcessing, notification) {
    this.contentProcessing = { notification: { data: null }, workflow: {} }
    this.notification = notification
    this.extractFiles = {}
    this.files = {}
  }

  setFiles(files) {
    this.files = files
  }

  async handleMessage(transaction) {
    if (!this.files[this.notification.data.id]) {
      throw Error(`i couldn't find necessary files for submission`)
    }

    if (!this.files[this.notification.data.id]['meta.xml']) {
      throw Error(`I could not find a meta.xml file`)
    }

    const submitMeta = new SubmitMeta(
      await FileService.getSourceToBuffer(
        this.files[this.notification.data.id]['meta.xml'].id,
        transaction,
      ),
    )

    this.user = await User.query(transaction).findOne({ username: 'ftp' })

    let book = await this.checkBookBySubmitId(
      submitMeta.bookSubmitId,
      transaction,
    )

    if (!book) {
      if (submitMeta.submissionType === 'book') {
        const domain = new Domain({
          bookSubmitId: submitMeta.bookSubmitId,
          title: submitMeta.bookSubmitId,
          workflow: submitMeta.workflow,
          organisationId: this.organization.id,
          collectionId: null,
          settings: {
            chapterIndependently: false,
            formatPublicationDate: 'YYYY',
            formatChapterDate: null,
            publisher: this.organization.id,
          },
          user: this.user.id || null,
          transaction,
        })

        book = await domain.executeCommand('create')

        book = await Book.query(transaction).findOne({ id: book.id })
      } else {
        throw Error(
          `There is no book id ${submitMeta.domainId} for publisher ${this.notification.data.publisher}`,
        )
      }
    }

    // Checking if this is an object of Model Book
    // we need to check the requests during creation because may in case of 502 gateway
    // doesnt return a book nor throwing an error . Execute Command create book
    if (book.type === 'book') {
      await this.applyCover(book, transaction)
      await this.sentForConversion(book, submitMeta, transaction)
    }

    // const appError = new SubmissionError(this.notification)

    // await appError.handleError(this.object, transaction)
  }

  // eslint-disable-next-line class-methods-use-this
  async sentForConversion(book, submitMeta, transaction) {
    const { source } = this.extractFiles.filesCategory[
      this.notification.data.id
    ]

    let createdBookComponent = ''

    if (book.settings && book.settings.chapterIndependently === false) {
      const { data } = await Domain.createVersion({
        bookId: book.id,
        filename: source.find(s => s.category === 'source').name,
        fileId: source.find(s => s.category === 'source').id,
        files: this.files,
        ownerId: this.user.id || null,
        category: 'source',
        transaction,
      })

      createdBookComponent = data
    } else {
      const { data } = await Chapter.createVersion({
        bookId: book.id,
        componentType: 'chapter',
        filename: source.find(s => s.category === 'source').name,
        fileId: source.find(s => s.category === 'source').id,
        files: this.files,
        ownerId: this.user.id || null,
        category: 'source',
        transaction,
      })

      createdBookComponent = data
    }

    const opt = {
      domain: book.domain,
    }

    let type = ''

    if (submitMeta.submissionType === 'book') {
      type = 'wholeBookXmlConversion'

      const bookWithAppliedValues = await book.getBookWithAppliedCollectionValues()
      const bookCollection = await book.getCollection(transaction)

      opt.collection_id = (bookCollection.collection || {}).domain || null
      opt.collection_title = (bookCollection.collection || {}).title || null
      opt.publisher_name = bookWithAppliedValues.metadata.pub_name
      opt.publisher_loc = bookWithAppliedValues.metadata.pub_loc
      opt.source_type = bookWithAppliedValues.metadata.sourceType
      opt.version_name = bookWithAppliedValues.fundedContentType
      opt.version_number = bookWithAppliedValues.version
    } else if (submitMeta.submissionType === 'chapter') {
      type = 'xmlConversion'
      opt.chapter = submitMeta.chapterId
    }

    await PackageCollection.sendForConversion(
      type,
      source,
      opt,
      {
        objectId: createdBookComponent.id,
        pushToQueue: 'submit-queue',
        userId: this.user.id || null,
      },
      transaction,
    )

    this.bookComponent = createdBookComponent
  }

  async checkBookBySubmitId(bookSubmitId, transaction) {
    const isAlias = /^bcms\d+.\d+$/.test(bookSubmitId.toLowerCase())

    const search = {
      organisationId: this.organization.id,
    }

    if (isAlias) {
      search.alias = bookSubmitId.replace(/bcms/g, '')
    } else {
      search.bookSubmitId = bookSubmitId
    }

    const [book] = await Book.query(transaction).where(search)

    return book
  }

  async applyCover(book, transaction) {
    logger.info(`Saving Cover for Book ${book.id}`)

    const cover = this.files.find(f => f.category === 'cover')

    if (cover) {
      await Book.query(transaction)
        .patch({ fileCover: cover.id })
        .findOne({ id: book.id })

      const fileCover = await File.query(this.transaction).findOne({
        id: cover.id,
      })

      // Send for FTP upload!
      Events.notify('BookUpdated', {
        domains: [book.id],
        cover: fileCover,
        bcmsDomains: [book.domain],
      })
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async validateMessage() {
    if (this.notification.data.publisher) {
      this.organization = await Organisation.query().findOne({
        abbreviation: this.notification.data.publisher,
      })

      return this.organization
    }

    return false
  }

  // eslint-disable-next-line class-methods-use-this
  async triggerEvent() {
    Events.notify(`SubmitEnd`, {})
  }

  async messageProcessed(transaction) {
    return await NcbiNotificationMessage.query(transaction)
      .patch({
        proccessed: true,
      })
      .findOne({
        jobId: this.notification.data.id,
        proccessed: false,
      })
  }
}

module.exports = Submit
