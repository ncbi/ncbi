const { pick } = require('lodash')

const {
  Book: BookModel,
  Collection,
  FileVersion,
  SourceConvertedFile,
} = require('@pubsweet/models')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const logger = getRawLogger()

const bookParser = require('../xmlBaseModel/book')

const OrderService = require('../orderService/orderService')

class WholeBook {
  constructor(workflow, notification) {
    this.notification = notification
    this.type = 'book'
    this.workflow = workflow
    this.id = notification.notification.objectId
    this.parser = bookParser
    this.files = []
  }

  get remoteFolderPath() {
    if (this.workflow.remoteFolderPath) return this.workflow.remoteFolderPath
    return `/convert/${this.workflow.type}/book/out`
  }

  async update(parsedData, status, transaction) {
    let book = await BookModel.query(transaction).findById(this.id)

    let patchData = { status }

    if (parsedData) {
      const pubDate = await parsedData.metadata.pubDate

      const openAccessLicense =
        parsedData.metadata.openAccessLicense === 'open-access'

      patchData = {
        altTitle: parsedData.metadata.alt_title,
        edition: parsedData.metadata.edition,
        subTitle: parsedData.metadata.sub_title,
        title: parsedData.metadata.title,
        status,
        metadata: {
          ...book.metadata,
          ...pick(parsedData.metadata, [
            'doi',
            'volume',
            'pub_name',
            'pub_loc',
            'licenseType',
            'licenseStatement',
            'copyrightStatement',
            'otherLicenseType',
          ]),
          pubDate: {
            ...pubDate,
            ...pick(parsedData.metadata, ['dateType', 'publicationFormat']),
          },
          openAccessLicense,
        },
      }
    }

    book = await book.$query(transaction).patch(patchData).returning('*')

    let converted = null

    if (parsedData) {
      if (parsedData.files.length > 0) {
        // eslint-disable-next-line no-plusplus
        for (let i = 0, len = parsedData.files.length; i < len; i++) {
          logger.info(
            `File is added: ${parsedData.files[i].name} on category ${parsedData.files[i].category} on Book ${book.id}, bcms${book.alias}`,
          )

          // eslint-disable-next-line no-await-in-loop
          const bookFile = await book.addFile(
            parsedData.files[i],
            parsedData.files[i].category,
            status,
            transaction,
          )

          if (bookFile.category === 'converted') {
            converted = bookFile
          }
        }
      }
    }

    // update source files
    const latestSource = await BookModel.latestComponentFiles(
      book,
      'source',
      transaction,
    )

    await FileVersion.query(transaction)
      .patch({ status })
      .whereIn(
        'id',
        latestSource.map(ls => ls.id),
      )

    if (converted) {
      await SourceConvertedFile.query(transaction).insert(
        latestSource.map(source => ({
          sourceId: source.id,
          convertedId: converted.id,
        })),
      )
    }

    if (book.collectionId) {
      const collection = await Collection.query(transaction).findOne({
        id: book.collectionId,
      })

      await OrderService.model(collection, {
        transaction,
        exclude: itm => itm.settings.isCollectionGroup,
        deep: true,
      })
    }

    return book
  }

  hasErrors() {
    return this.notification.hasErrors()
  }
}

module.exports = WholeBook
