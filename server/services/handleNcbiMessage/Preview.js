/* eslint-disable no-this-before-super */
/* eslint-disable constructor-super */
const { merge } = require('lodash')

const {
  Events,
  Toc,
  Book,
  BookComponent,
  Division,
} = require('@pubsweet/models')

const { pubsubManager } = require('pubsweet-server')

const { getPubsub } = pubsubManager

const notify = require('../notify')

const PreviewError = require('./error/PreviewError')
const TocError = require('./error/TocError')

const OrderService = require('../orderService/orderService')

const { TocService, Chapter, Domain } = require('..')

const CONVERSION_COMPLETED = 'CONVERSION_COMPLETED'

class Preview {
  constructor(object, data, service, notification, appError) {
    this.object = object
    this.Service = service
    this.data = data
    this.notification = notification
    this.appError = appError
    this.files = {}
  }

  setFiles() {
    this.files = {}
  }

  async handleMessage(transaction) {
    this.object = await this.object
      .$query(transaction)
      .patch(
        merge(
          {
            metadata: {
              ...this.object.metadata,
              url: this.notification.data.url,
            },
          },
          this.data,
        ),
      )
      .findOne({ id: this.object.id })
      .returning('*')

    const objectService = new this.Service({
      status: this.data.status,
      object: this.object,
      transaction,
    })

    await objectService.executeCommand('updateStatus')

    this.object = await this.object.$query(transaction).findOne({
      id: this.object.id,
    })

    if (this.object.type === 'bookComponent') {
      const division = await Division.query(transaction).findOne({
        bookId: this.object.bookId,
        label: 'body',
      })

      await OrderService.model(division, {
        transaction,
        exclude: itm => itm.componentType === 'part',
        deep: true,
      })
    }

    await this.appError.handleError(this.object, transaction)

    notify('previewGenerated', {
      bookComponentTitle: this.object.title,
      ownerId: this.object.ownerId,
    })

    await this.sendEvent(this.object)
  }

  async triggerEvent() {
    Events.notify(`${this.object.type}PreviewEnd`, { object: this.object })
  }

  // eslint-disable-next-line class-methods-use-this
  async sendEvent({
    id,
    title,
    bookId,
    status,
    updated,
    divisionId,
    collectionId,
    bookComponentVersionId,
    componentType,
    metadata,
  }) {
    const pubsub = await getPubsub()

    pubsub.publish(`${CONVERSION_COMPLETED}.${bookId}`, {
      conversionCompleted: {
        id,
        title,
        status,
        updated,
        bookId,
        divisionId,
        collectionId,
        bookComponentVersionId,
        componentType,
        metadata: {
          chapter_number: metadata.chapter_number,
          filename: metadata.filename,
        },
      },
    })
  }

  static async createChapterInstance(parsedNotification) {
    const { notification } = parsedNotification

    const data = {
      status: parsedNotification.hasErrors() ? 'loading-errors' : 'preview',
    }

    const bookComponent = await BookComponent.query()
      .findOne({
        id: notification.objectId,
      })
      .throwIfNotFound()

    const appError = new PreviewError(parsedNotification)

    return new Preview(
      bookComponent,
      data,
      Chapter,
      parsedNotification,
      appError,
    )
  }

  static async createCoverInstance(parsedNotification) {
    const { notification } = parsedNotification

    const bookComponent = await BookComponent.query()
      .findOne({
        id: notification.objectId,
      })
      .throwIfNotFound()

    const appError = new PreviewError(parsedNotification)

    await appError.handleError(bookComponent)

    return true
  }

  static async createWholeBookInstance(parsedNotification) {
    const { notification } = parsedNotification

    const data = {
      status: parsedNotification.hasErrors() ? 'loading-errors' : 'preview',
    }

    const book = await Book.query()
      .findOne({
        id: notification.objectId,
      })
      .throwIfNotFound()

    const appError = new PreviewError(parsedNotification)

    return new Preview(book, data, Domain, parsedNotification, appError)
  }

  static async createTocInstance(parsedNotification) {
    const { notification } = parsedNotification

    const data = {
      status: parsedNotification.hasErrors() ? 'loading-errors' : 'preview',
    }

    const toc = await Toc.query()
      .findOne({
        id: notification.objectId,
      })
      .throwIfNotFound()

    const appError = new TocError(parsedNotification)

    return new Preview(toc, data, TocService, parsedNotification, appError)
  }
}

module.exports = Preview
