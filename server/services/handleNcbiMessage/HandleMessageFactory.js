/* eslint-disable no-return-await */
const { isString, split } = require('lodash')
const { Book, BookComponent } = require('@pubsweet/models')

const ChapterProcessed = require('./ChapterProcessed')
const WholeBook = require('./WholeBook')
const Word = require('./Word')
const Xml = require('./Xml')
const Pdf = require('./Pdf')
const Conversion = require('./Conversion')
const Preview = require('./Preview')
const Submit = require('./Submit')
const Publish = require('./Publish')

const entity = {
  Word,
  Xml,
  Pdf,
}

const useCase = {
  ChapterProcessed,
  WholeBook,
}

const Action = {
  Conversion,
  Preview,
  Publish,
  Submit,
}

class HandleMessageRouteFactory {
  rules = {
    convert_word_book_chapter_receipt: 'Word.ChapterProcessed.Conversion', // 'BookComponentConversion',
    convert_xml_book_chapter_receipt: 'Xml.ChapterProcessed.Conversion', // 'ChapterXmlConversion',
    convert_xml_book_receipt: 'Xml.WholeBook.Conversion', // 'BookXmlConversion',
    submit_apex_package_receipt: this.checkContentProcessing, // PdfConversion,
    ingest_book_chapter_receipt: this.checkPackage('ChapterProcessed'),
    ingest_book_wholebook_receipt: this.checkPackage('WholeBook'),
    submit_package_receipt: '..Submit',
    cover_receipt: '..Preview:createCoverInstance',
  }

  constructor(parsedNotification) {
    this.parsedNotification = parsedNotification
    this.notification = parsedNotification.notification
    this.packageType = this.notification.packageType
    this.topic = this.notification.topic
    this.workflow = null
  }

  // eslint-disable-next-line class-methods-use-this
  checkPackage(usecase) {
    const method =
      usecase === 'ChapterProcessed'
        ? 'createChapterInstance'
        : 'createWholeBookInstance'

    return data => {
      const isPublish = !!(data.options && data.options.release === true)

      return data.packageType === 'tocPreview'
        ? `TocPreview.${usecase}.${
            isPublish ? 'Publish' : 'Preview'
          }::createTocInstance` // TocPreview
        : `.${usecase}.${isPublish ? 'Publish' : 'Preview'}::${method}` // BookComponentPreview
    }
  }

  // eslint-disable-next-line class-methods-use-this
  checkContentProcessing(data) {
    return data.packageType === 'wholeBookPdfConversion'
      ? 'Pdf.WholeBook.Conversion'
      : 'Pdf.ChapterProcessed.Conversion'
  }

  async instantiateWorkflow(workflow) {
    let modelWorkflow = workflow
    let book = null

    if (this.notification.objectId === null) return null

    if (modelWorkflow === '') {
      book = await Book.query().findOne({ id: this.notification.objectId })

      if (!book) {
        const bookComponent = await BookComponent.query().findOne({
          id: this.notification.objectId,
        })

        book = await Book.query().findOne({ id: bookComponent.bookId })
      }

      modelWorkflow = book.workflow || null
    }

    const WorkFlow = entity[modelWorkflow]

    if (!WorkFlow) return null

    return new WorkFlow()
  }

  async make() {
    const route = isString(this.rules[this.topic])
      ? this.rules[this.topic]
      : this.rules[this.topic](this.notification)

    const [workflow, type, action] = split(route, '.')

    this.workflow = await this.instantiateWorkflow(workflow)

    const caseFlow = useCase[type]
      ? new useCase[type](this.workflow, this.parsedNotification)
      : null

    // let appError = ''

    // if (action === 'Conversion' || action === 'PdfConversion') {
    //   appError = new ConversionError(this.parsedNotification)
    // } else if (action === 'CoverPreview' || action === 'BookComponentPreview') {
    //   appError = new PreviewError(this.parsedNotification)
    // } else if (action === 'FtpSubmitPackage') {
    //   appError = new SubmissionError(this.parsedNotification)
    // } else if (this.notification.packageType === 'tocPreview') {
    //   appError = new TocError(this.parsedNotification)
    // }
    if (action.indexOf('::') > 0) {
      const [ActionClass, StaticMethod] = split(action, '::')
      return await Action[ActionClass][StaticMethod](this.parsedNotification)
    }

    return new Action[action](caseFlow, this.parsedNotification)
  }

  static create(handleMessage) {
    const handleRouteMessage = new HandleMessageRouteFactory(
      handleMessage.notification,
    )

    return handleRouteMessage.make()
  }
}

module.exports = HandleMessageRouteFactory
