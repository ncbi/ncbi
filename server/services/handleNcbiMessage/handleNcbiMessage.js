/* eslint-disable no-return-await */
const { NcbiNotificationMessage } = require('@pubsweet/models')

class HandleNcbiMessage {
  constructor(notification) {
    this.notification = notification
  }

  async validateMessage() {
    return await NcbiNotificationMessage.query().findOne({
      jobId: this.notification.data.id,
      proccessed: false,
      uploaded: true,
    })
  }

  async messageProcessed(transaction) {
    return await NcbiNotificationMessage.query(transaction)
      .patch({
        proccessed: true,
      })
      .findOne({
        jobId: this.notification.data.id,
        proccessed: false,
        uploaded: true,
      })
  }
}

module.exports = HandleNcbiMessage
