/* eslint-disable react/forbid-prop-types */
const {
  logger: { getRawLogger },
} = require('@coko/server')

const { pubsubManager } = require('pubsweet-server')

const { flatten, isEmpty } = require('lodash')
const { Events, Book, File } = require('@pubsweet/models')

const FileService = require('../file/fileService')

const logger = getRawLogger()

const ConversionError = require('./error/ConversionError')
const TaggingError = require('./error/TaggingError')

const XmlFactory = require('../xmlBaseModel/xmlFactory')

const { getPubsub } = pubsubManager

const CONVERSION_COMPLETED = 'CONVERSION_COMPLETED'

class Conversion {
  constructor(contentProcessing, notification) {
    this.contentProcessing = contentProcessing
    this.notification = notification
    this.object = null
    this.files = {}
  }

  setFiles(files) {
    this.files = files
  }

  async extractDatafromConvertedFile(transaction) {
    let parsedData = null

    const { id } = this.files.find(f => f.category === 'converted')

    try {
      const content = await FileService.getSourceToBuffer(id, transaction)

      parsedData = await XmlFactory.xmlToModel(
        content.toString(),
        this.contentProcessing.parser,
        {},
        { forceStream: true },
      )

      logger.info(`kafka Parsed File successfully - file id: ${id}`)
    } catch (e) {
      logger.error(`HandleNcbiMessage: ${e} , on file id: ${id}`)
      parsedData = null
    }

    return parsedData
  }

  calculateStatus() {
    const status =
      this.contentProcessing.workflow.type === 'pdf' ? 'tagging' : 'converting'

    return this.contentProcessing.hasErrors() ? 'conversion-errors' : status
  }

  async applyCover(transaction) {
    const id = this.object.type === 'book' ? this.object.id : this.object.bookId
    logger.info(`Saving Cover for Book ${id}`)
    const cover = this.files.find(f => f.category === 'cover')

    if (cover) {
      const book = await Book.query(transaction)
        .patch({ fileCover: cover.id })
        .findOne({ id })
        .returning('*')

      const fileCover = await File.query(transaction).findOne({
        id: cover.id,
      })

      return {
        domains: [book.id],
        cover: fileCover,
        bcmsDomains: [book.domain],
      }
    }

    return {}
  }

  async handleMessage(transaction) {
    let parsedData = null

    if (!isEmpty(this.files)) {
      parsedData = await this.extractDatafromConvertedFile(transaction)

      if (parsedData) parsedData.files = this.files
    }

    const status = this.calculateStatus()

    if (parsedData && parsedData.metadata.contributors) {
      // We need to combine authors, editors and collaborativeAuthors from across
      // possible multiple contrib-groups into one flat set
      //

      parsedData.metadata.author =
        flatten(
          parsedData.metadata.contributors.map(cg => {
            return cg.author.map(ath => ({
              ...ath,
              affiliation: ath.affiliation ? ath.affiliation : [],
            }))
          }),
        ) || []

      parsedData.metadata.editor =
        flatten(
          parsedData.metadata.contributors.map(cg => {
            return cg.editor.map(edt => ({
              ...edt,
              affiliation: edt.affiliation ? edt.affiliation : [],
            }))
          }),
        ) || []

      parsedData.metadata.collaborativeAuthors =
        flatten(
          parsedData.metadata.contributors.map(cg => cg.collaborativeAuthors),
        ) || null
    }

    this.object = await this.contentProcessing.update(
      parsedData,
      status,
      transaction,
    )

    this.eventData = {
      object: this.object,
      notification: this.contentProcessing.notification,
      hasError: this.contentProcessing.hasErrors(),
    }

    if (this.object.status === 'tagging-errors') {
      const taggingError = new TaggingError(this.contentProcessing.notification)
      await taggingError.handleError(this.object, transaction)
      return
    }

    await this.sendEvent(this.object)

    const appError = new ConversionError(this.contentProcessing.notification)

    await appError.handleError(this.object, transaction)

    if (this.contentProcessing.workflow.type === 'pdf') {
      this.eventData = {
        ...this.eventData,
        ...(await this.applyCover(transaction)),
      }
    }
  }

  async triggerEvent() {
    Events.notify(
      `${this.contentProcessing.workflow.type}ConversionEnd`,
      this.eventData,
    )
  }

  // eslint-disable-next-line class-methods-use-this
  async sendEvent({
    id,
    title,
    bookId,
    status,
    updated,
    divisionId,
    bookComponentVersionId,
    componentType,
    metadata,
  }) {
    const pubsub = await getPubsub()

    pubsub.publish(`${CONVERSION_COMPLETED}.${bookId}`, {
      conversionCompleted: {
        id,
        title,
        status,
        updated,
        bookId,
        divisionId,
        bookComponentVersionId,
        componentType,
        metadata: {
          chapter_number: metadata.chapter_number,
          filename: metadata.filename,
        },
      },
    })
  }
}

module.exports = Conversion
