/* eslint-disable no-param-reassign */
/* eslint-disable no-return-await */
const { File } = require('@pubsweet/models')
const concat = require('concat-stream')
const { Readable } = require('stream')
const cloneDeep = require('lodash/cloneDeep')
const { useTransaction } = require('@coko/server')
const BlobManager = require('./blobManager')

class FileService {
  static async streamToDatabase({ stream, filename }, filepublishName = null) {
    return await useTransaction(async transaction => {
      const blobManager = new BlobManager({ stream, transaction })

      return await blobManager.write(filename, filepublishName)
    })
  }

  static async updateCover(file, type, data, dataId) {
    let cover = null
    let returnData = data

    if (file) {
      cover = await FileService.writeFileGraphic(file, type)

      returnData = cloneDeep({
        ...data,
        [dataId]: cover.id || null,
      })
    } else if (file === null) {
      returnData = cloneDeep({
        ...data,
        [dataId]: null,
      })
    }

    return returnData
  }

  static async writeFileGraphic(file, type) {
    const { filename, createReadStream: stream } = await file
    let cover = null

    if (stream) {
      cover = await FileService.write({
        filename,
        stream: stream(),
        category: type,
      })
    } else {
      cover = { id: file }
    }

    return cover
  }

  static async write(
    { stream, filename, category = null, content },
    filepublishName,
    transaction,
  ) {
    if (content && !stream) {
      stream = new Readable()

      // eslint-disable-next-line no-underscore-dangle
      stream._read = () => {}

      stream.push(Buffer.from(content, 'utf8'))
      stream.push(null)
    }

    const blobwrite = async trx => {
      const blobManager = new BlobManager({ stream, transaction: trx })

      const modelData = await blobManager.write(filename, filepublishName)

      return await File.query(trx)
        .insert({
          category,
          mimeType: modelData.mimeType,
          name: filename,
          oid: modelData.oid,
        })
        .returning('*')
    }

    if (transaction) {
      return await blobwrite(transaction)
    }

    // eslint-disable-next-line no-shadow
    return await useTransaction(async transaction => {
      return await blobwrite(transaction)
    })
  }

  static async update(id, data, filepublishName) {
    let { stream, filename, category, content } = data

    if (content && !stream) {
      stream = new Readable()

      // eslint-disable-next-line no-underscore-dangle
      stream._read = () => {}

      stream.push(Buffer.from(content, 'utf8'))
      stream.push(null)
    }

    return await useTransaction(async transaction => {
      const blobManager = new BlobManager({ stream, transaction })

      const modelData = await blobManager.write(filename, filepublishName)

      const updateData = {
        mimeType: modelData.mimeType,
        name: filename,
        oid: modelData.oid,
      }

      if (category) {
        updateData.category = category
      }

      return await File.query(transaction)
        .patch(updateData)
        .findOne({ id })
        .returning('*')
    })
  }

  static async getSourceToBuffer(identifier, trx) {
    const getFile = async transaction => {
      const search = /^\d+$/.test(identifier)
        ? { oid: identifier }
        : { id: identifier }

      const fileModel = await File.query(transaction).findOne(search)

      // eslint-disable-next-line no-async-promise-executor
      return await new Promise(async resolve => {
        const blobManager = new BlobManager({ transaction })
        const readStream = await blobManager.read(fileModel.oid)

        readStream.pipe(concat(async sourceBuffer => resolve(sourceBuffer)))
      })
    }

    if (trx) {
      return await getFile(trx)
    }

    return await useTransaction(getFile)
  }

  static async read({ stream, oid, transaction, end }) {
    if (transaction) {
      const blobManager = new BlobManager({ stream, transaction, end })
      return blobManager.read(oid)
    }

    throw Error('Please provide a transaction.')
  }
}

module.exports = FileService
