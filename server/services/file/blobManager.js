/* eslint-disable no-return-await */

const {
  getPubsub,
  asyncIterators,
} = require('pubsweet-server/src/graphql/pubsub')

const { LargeObjectManager } = require('pg-large-object')

const {
  logger: { getRawLogger },
} = require('@coko/server')

const FileType = require('../stream/fileTypeStream')

const { ON_UPLOAD_PROGRESS } = asyncIterators

const logger = getRawLogger()

class BlobManager {
  constructor({ stream, transaction, end }) {
    this.stream = stream
    this.transaction = transaction
    this.mimeType = 'text/plain'
    this.bufferSize = 16384
    this.end = end

    this.setupFileTypeDetector()
  }

  async getConnectionFromTransaction() {
    this.connection = await this.transaction.client.acquireConnection()
    this.manager = new LargeObjectManager({ pg: this.connection })
  }

  read(oid) {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve, reject) => {
      await this.getConnectionFromTransaction()

      this.manager.openAndReadableStream(
        oid,
        this.bufferSize,
        async (err, size, blobStream) => {
          if (err) {
            logger.error(`error creating stream: ${err}`)
            reject(err)
            return
          }

          if (this.end) blobStream.on('end', this.end)

          blobStream.on('error', error => {
            logger.error(`blob error: ${error}`)
          })

          resolve(blobStream)

          if (this.stream) {
            blobStream.pipe(this.stream)
          }
        },
      )
    })
  }

  async write(file, publishName) {
    await this.getConnectionFromTransaction()

    const pubsub = await getPubsub()

    pubsub.publish(`${ON_UPLOAD_PROGRESS}.${file}.${publishName}`, {
      uploadConvertFileProgress: {
        file,
        percentage: 1,
        status: 'uploading',
        total: 1,
      },
    })

    const [oid, streamFile] = await this.manager.createAndWritableStreamAsync(
      this.bufferSize,
    )

    // The server has generated an oid
    logger.info(`Creating a large object with the oid: ${oid}`)
    this.stream.pipe(this.fileTypeDetector).resume()

    this.stream.pipe(streamFile)

    return new Promise(resolve => {
      streamFile.on('finish', () => {
        pubsub.publish(`${ON_UPLOAD_PROGRESS}.${file}.${publishName}`, {
          uploadConvertFileProgress: {
            file,
            percentage: 100,
            status: 'uploaded',
            total: 1,
          },
        })

        resolve({
          oid,
          streamFile: this.stream,
          filename: file,
          mimeType: this.mimeType,
        })
      })
    })
  }

  setupFileTypeDetector() {
    this.fileTypeDetector = new FileType()

    this.fileTypeDetector.on('file-type', fileType => {
      if (fileType === null) {
        logger.info(`The mime type of the file could not be determined`)
      } else {
        this.mimeType = fileType.mime
        logger.info(`"${fileType.mime}" mime type`)
      }
    })
  }
}

module.exports = BlobManager
