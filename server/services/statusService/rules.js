/*
Here list all checks to automatically perform the ServiceStatus
 Object Structure ( key: [rules ([by], [customRule], status:String )])

 1. (key) this key of the object is the current status
 2. rules array of rule
 3. (by) function that checks teams if the user is member
 4. (customRule) custom function  to check against if the status is valid 
 5. (status) the new status of the entity

*/

const bookComponent = {
  'new-upload': [
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsEditor',
        'userIsAuthor',
        'userIsFtp',
      ],
      status: 'converting',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsEditor',
        'userIsAuthor',
        'userIsFtp',
      ],
      status: 'tagging',
    },

    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsEditor',
        'userIsAuthor',
        'userIsFtp',
      ],
      status: 'tagging-errors',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsEditor',
        'userIsAuthor',
        'userIsFtp',
      ],
      status: 'loading-preview',
    },
  ],
  preview: [
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'in-review',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'published',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'publishing',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'loading-preview',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'new-upload',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'publish-failed',
    },
  ],
  'in-review': [
    {
      by: ['isOrgAdmin', 'isSysAdmin', 'userIsPreviewer', 'userIsFtp'],
      customRule: ['allReviewsApproved'],
      status: 'approve',
    },
    {
      by: ['isOrgAdmin', 'isSysAdmin', 'userIsEditor', 'userIsFtp'],
      status: 'published',
    },
  ],
  approve: [
    {
      by: ['isOrgAdmin', 'isSysAdmin', 'userIsEditor', 'userIsFtp'],
      status: 'published',
    },
    {
      by: ['isOrgAdmin', 'isSysAdmin', 'userIsEditor', 'userIsFtp'],
      status: 'in-review',
    },
  ],
  published: [
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsEditor',
        'userIsAuthor',
        'userIsFtp',
      ],
      status: 'publishing',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsEditor',
        'userIsAuthor',
        'userIsFtp',
      ],
      status: 'new-version',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'new-upload',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'loading-preview',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'publish-failed',
    },
  ],
}

const fileVersion = {
  'new-upload': [
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsEditor',
        'userIsAuthor',
        'userIsFtp',
      ],
      status: 'converting',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsEditor',
        'userIsAuthor',
        'userIsFtp',
      ],
      status: 'tagging',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsEditor',
        'userIsAuthor',
        'userIsFtp',
      ],
      status: 'loading-preview',
    },
  ],
  approve: [
    {
      by: ['isOrgAdmin', 'isSysAdmin', 'userIsEditor', 'userIsFtp'],
      status: 'published',
    },
    {
      by: ['isOrgAdmin', 'isSysAdmin', 'userIsEditor', 'userIsFtp'],
      status: 'in-review',
    },
  ],
  published: [
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsEditor',
        'userIsAuthor',
        'userIsFtp',
      ],
      status: 'publishing',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsEditor',
        'userIsAuthor',
        'userIsFtp',
      ],
      status: 'new-version',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'new-upload',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'loading-preview',
    },
    {
      by: [
        'isOrgAdmin',
        'isSysAdmin',
        'userIsAuthor',
        'userIsEditor',
        'userIsFtp',
      ],
      status: 'publish-failed',
    },
  ],
  // 'new-upload': [
  //   {
  //     by: ['isSysAdmin', 'userIsAuthor', 'userIsEditor'],
  //     status: 'converting',
  //   },
  //   {
  //     by: ['isSysAdmin', 'userIsAuthor', 'userIsEditor'],
  //     status: 'loading-preview',
  //   },
  //   {
  //     by: ['isSysAdmin', 'userIsAuthor', 'userIsEditor'],
  //     status: 'submission-errors',
  //   },
  // ],
  // 'loading-preview': [
  //   {
  //     by: ['isSysAdmin', 'userIsAuthor', 'userIsEditor'],
  //     status: 'converting',
  //   },
  //   {
  //     by: ['isSysAdmin', 'userIsAuthor', 'userIsEditor'],
  //     status: 'loading-preview',
  //   },
  //   {
  //     by: ['isSysAdmin', 'userIsAuthor', 'userIsEditor'],
  //     status: 'submission-errors',
  //   },
  // ],
}

const book = {}

module.exports = {
  bookComponent,
  fileVersion,
  book,
}
