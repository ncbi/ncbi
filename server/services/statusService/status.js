/* eslint-disable no-return-await */

const rules = require('./rules')

const BookStatus = require('./bookStatus')
const BookComponentStatus = require('./bookComponentStatus')
const BookComponentFileStatus = require('./bookComponentFileStatus')
const UserCheck = require('./userCheck')

const EntityStatus = {
  book: BookStatus,
  bookComponent: BookComponentStatus,
  fileVersion: BookComponentFileStatus,
}

class Status {
  constructor(entity, status, transaction) {
    this.entity = entity
    this.newStatus = status
    this.ownerId = entity.ownerId
    this.transaction = transaction

    this.validation = new EntityStatus[this.entity.type](entity, transaction)

    this.userCheck = new UserCheck(this.ownerId, transaction)
  }

  async update(force) {
    if (await this.validate()) {
      if (this.entity.status === this.newStatus && force === false)
        return this.entity

      return await this.entity
        .$query(this.transaction)
        .context({ ignoreUpdate: true })
        .patch({ status: this.newStatus })
        .returning('*')
    }

    return false
  }

  async validate() {
    const { status } = this.entity

    if (!rules[this.entity.type][status]) return true

    const checks = (rules[this.entity.type][status] || []).find(
      validNext => validNext.status === this.newStatus,
    )

    if (!checks) return false

    const validateUsers = (
      await Promise.all(
        (checks || {}).by.length > 0
          ? checks.by.map(async user => await this.userCheck[user]())
          : [true],
      )
    ).some(entity => entity === true)

    const validateCustomRules = (
      await Promise.all(
        ((checks || {}).customRule || []).map(
          async rule => await this.validation[rule](),
        ),
      )
    ).every(entity => entity === true)

    return validateUsers && validateCustomRules
  }
}

module.exports = Status
