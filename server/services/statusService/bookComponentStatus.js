const ReviewService = require('../review/review')
const { model: Review } = require('../../models/review')

class BookComponentStatus {
  constructor(entity, transaction) {
    this.entity = entity
    this.transaction = transaction
  }

  async allReviewsApproved() {
    const review = await Review.query(this.transaction).where({
      objectId: this.entity.id,
    })

    const { decision } = await ReviewService.checkResponses(review)

    return decision === 'approve'
  }
}

module.exports = BookComponentStatus
