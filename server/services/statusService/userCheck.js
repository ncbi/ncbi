const { Team, TeamMember, Book, User } = require('@pubsweet/models')
const flatten = require('lodash/flatten')

class UserCheck {
  constructor(ownerId, transaction) {
    this.bookId = 0
    this.bookComponentId = 0
    this.ownerId = ownerId
    this.transaction = transaction
  }

  async isSysAdmin() {
    if (this.ownerId === null) return true

    const sysAdminTeam = await Team.query(this.transaction).findOne({
      role: 'sysAdmin',
      global: true,
    })

    const sysAdminMember = await TeamMember.query(this.transaction).findOne({
      userId: this.ownerId,
      teamId: sysAdminTeam.id,
    })

    return !!sysAdminMember
  }

  async userIsFtp() {
    const { username } = await User.query(this.transaction).findOne({
      id: this.ownerId,
    })

    return username === 'ftp'
  }

  async isOrgAdmin() {
    if (this.ownerId === null) return true

    const { organisationId } = await Book.query(this.transaction).findOne({
      id: this.bookId,
    })

    const orgTeam = await Team.query(this.transaction).findOne({
      role: 'orgAdmin',
      objectId: organisationId,
    })

    if (!orgTeam) return false

    const orgAdminMember = await TeamMember.query(this.transaction).findOne({
      userId: this.ownerId,
      teamId: orgTeam.id,
    })

    return !!orgAdminMember
  }

  async userIsEditor() {
    if (this.ownerId === null) return true

    const bookEditors = await Team.query(this.transaction)
      .findOne({
        objectId: this.bookId,
        objectType: 'book',
        role: 'editor',
      })
      .eager('[members(onlyEnabledUsers).[user, alias]]')

    const bookComponentEditors = await Team.query(this.transaction)
      .findOne({
        objectId: this.bookComponentId,
        objectType: 'bookComponent',
        role: 'editor',
      })
      .eager('[members(onlyEnabledUsers).[user, alias]]')

    const editorsUsers = flatten(
      (bookComponentEditors ? [bookComponentEditors] : [])
        .concat(bookEditors ? [bookEditors] : [])
        .map(team => team.members.map(teamMember => teamMember.user)),
    )

    const foundUser = editorsUsers.find(user => user.id === this.ownerId)

    return !!foundUser
  }

  async userIsPreviewer() {
    if (this.ownerId === null) return true

    const bookComponentPreviewers = await Team.query(this.transaction)
      .findOne({
        objectId: this.bookComponentId,
        objectType: 'bookComponent',
        role: 'previewer',
      })
      .eager('[members(onlyEnabledUsers).[user, alias]]')

    const bookPreviewers = await Team.query(this.transaction)
      .findOne({
        objectId: this.bookId,
        objectType: 'book',
        role: 'previewer',
      })
      .eager('[members(onlyEnabledUsers).[user, alias]]')

    const previewerUsers = flatten(
      (bookComponentPreviewers ? [bookComponentPreviewers] : [])
        .concat(bookPreviewers ? [bookPreviewers] : [])
        .map(team => team.members.map(teamMember => teamMember.user)),
    )

    const foundUser = previewerUsers.find(user => user.id === this.ownerId)
    return !!foundUser
  }

  async userIsAuthor() {
    if (this.ownerId === null) return true

    const bookComponentAthors = await Team.query(this.transaction)
      .findOne({
        objectId: this.bookComponentId,
        objectType: 'bookComponent',
        role: 'author',
      })
      .eager('[members(onlyEnabledUsers).[user, alias]]')

    const bookAthors = await Team.query(this.transaction)
      .findOne({
        objectId: this.bookId,
        objectType: 'book',
        role: 'author',
      })
      .eager('[members(onlyEnabledUsers).[user, alias]]')

    const editorsUsers = flatten(
      (bookComponentAthors ? [bookComponentAthors] : [])
        .concat(bookAthors ? [bookAthors] : [])
        .map(team => team.members.map(teamMember => teamMember.user)),
    )

    const foundUser = editorsUsers.find(user => user.id === this.ownerId)
    return !!foundUser
  }
}

module.exports = UserCheck
