/* eslint-disable global-require */
/* eslint-disable no-return-await */
/* eslint-disable no-param-reassign */
class StatusService {
  static async findModelFromEntity(model, transaction) {
    const { model: BookComponent } = require('../../models/bookComponent')

    let book = 0
    let bookComponent = 0

    if (model.type === 'bookComponent') {
      book = model.bookId || model.book_id
      bookComponent = model.id
    } else if (model.type === 'fileVersion') {
      const bc = await BookComponent.query(transaction).findOne({
        id: model.bookComponentId || model.book_component_id,
      })

      book = bc.bookId || bc.book_id
      bookComponent = model.bookComponentId || model.book_component_id
    }

    return { book, bookComponent }
  }

  static validateArgs({ entity, status, userId }) {
    if (userId) {
      entity.ownerId = userId
    }

    if (!entity.status)
      throw new Error(`Cannot Validate without current status`)

    if (!status) throw new Error(`Cannot Validate without  new status`)

    return entity
  }

  /*
   *
   * Force update when is needed
   *
   */
  static async update({ entity, status, userId, transaction, force = false }) {
    const Status = require('./status')
    const model = StatusService.validateArgs({ entity, status, userId })
    const statusInstance = new Status(model, status, transaction)

    const modelEntity = await StatusService.findModelFromEntity(
      entity,
      transaction,
    )

    statusInstance.userCheck.bookId = modelEntity.book
    statusInstance.userCheck.bookComponentId = modelEntity.bookComponent

    return await statusInstance.update(force)
  }

  static async updateEntity(model, item, trx) {
    const updateStatus = await StatusService.validate({
      entity: model,
      status: item.status,
      userId: item.ownerId || model.ownerId,
      trx,
    })

    return updateStatus ? item.status : model.status
  }

  static async validate({ entity, status, userId, trx }) {
    const Status = require('./status')
    const model = StatusService.validateArgs({ entity, status, userId })

    const statusInstance = new Status(model, status, trx)

    const modelEntity = await StatusService.findModelFromEntity(entity, trx)

    statusInstance.userCheck.bookId = modelEntity.book
    statusInstance.userCheck.bookComponentId = modelEntity.bookComponent

    return await statusInstance.validate()
  }
}

module.exports = StatusService
