const { model: BookComponent } = require('../../models/bookComponent')

class BookStatus {
  constructor(entity, transaction) {
    this.entity = entity
    this.transaction = transaction
  }

  async calculateStatus() {
    let status = null

    const bookComponents = await BookComponent.query(this.transaction).where({
      bookId: this.entity.id,
      parentId: null,
    })

    const noPartComponents = bookComponents.filter(
      x => x.componentType !== 'part' && x.componentType !== 'toc',
    )

    const sameStatus = noPartComponents.every(
      bc => bc.status === noPartComponents[0].status,
    )

    if (this.entity.settings.chapterIndependently) {
      status = sameStatus ? noPartComponents[0].status : 'in-production'
    } else if (this.entity.status !== noPartComponents[0].status) {
      status = noPartComponents[0].status
    }

    return status
  }
}

module.exports = BookStatus
