/* eslint-disable class-methods-use-this */
const XmlBuilder = require('./xmlBuilder')

class Date extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'date'
  }

  get attributes() {
    return {
      'date-type': this.data.attributes.type,
    }
  }

  content() {
    const date = this.data.date.split('-')
    return `<day>${date[2]}</day><month>${date[1]}</month><year>${date[0]}</year>`
  }
}

module.exports = Date
