/* eslint-disable class-methods-use-this */
const XmlBuilder = require('./xmlBuilder')

class PubDate extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'pub-date'
  }

  get attributes() {
    return {
      'date-type': this.data.pubDate.dateType,
      'publication-format': this.data.pubDate.publicationFormat,
    }
  }

  content() {
    const { day, year, month } = this.data.pubDate.date

    let content = ''
    // Don't emit a day tag on a range - we only have months and years
    if (day && this.data.pubDate.dateType !== 'pubr')
      content = `${content}<day>${day}</day>`
    if (month) content = `${content}<month>${month}</month>`
    if (year) content = `${content}<year>${year}</year>`

    return content
  }
}

module.exports = PubDate
