/* eslint-disable class-methods-use-this */
const {
  logger: { getRawLogger },
} = require('@coko/server')

const XmlBuilder = require('./xmlBuilder')
const { replaceSpecialChars } = require('../../../_helpers')

const logger = getRawLogger()

class Publisher extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'publisher'
  }

  content() {
    const name =
      this.data.pub_name !== ''
        ? `<publisher-name>${replaceSpecialChars(
            this.data.pub_name,
          )}</publisher-name>`
        : ''

    const loc =
      this.data.pub_loc !== ''
        ? `<publisher-loc>${replaceSpecialChars(
            this.data.pub_loc,
          )}</publisher-loc>`
        : ''

    logger.info(
      `build xml publisher loc/name: ${this.data.pub_name}, ${this.data.pub_loc} : result ${name}${loc} `,
    )

    return `${name}${loc}`
  }
}

module.exports = Publisher
