/* eslint-disable class-methods-use-this */
const XmlBuilder = require('./xmlBuilder')

class Affiliation extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'aff'
  }

  get attributes() {
    return {
      id: `${this.data.attributes.id.toLowerCase().replace(/[^A-Z0-9]/gi, '')}`,
    }
  }

  content() {
    let affiliation = ''

    if (this.data.affiliation) {
      affiliation = `<institution>${this.data.affiliation}</institution>`
    }

    return `${affiliation}`
  }
}

module.exports = Affiliation
