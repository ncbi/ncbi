/* eslint-disable class-methods-use-this */
const XmlBuilder = require('./xmlBuilder')

const Date = require('./date')

class PubHistory extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'pub-history'
  }

  get children() {
    const children = []

    // This is the book component's pub history
    // as inside "book-part-meta"
    // (not inside "book-meta" as controlled by BookMeta)

    if (this.data.date && this.data.date.created) {
      children.push(
        new Date(
          {
            attributes: { type: 'created' },
            date: this.data.date.created,
          },
          this.context,
        ),
      )
    }

    if (this.data.date && this.data.date.updated) {
      children.push(
        new Date(
          {
            attributes: { type: 'updated' },
            date: this.data.date.updated,
          },
          this.context,
        ),
      )
    }

    if (this.data.date && this.data.date.revised) {
      children.push(
        new Date(
          {
            attributes: { type: 'revised' },
            date: this.data.date.revised,
          },
          this.context,
        ),
      )
    }

    return children
  }

  static pubHistoryData(metadata, context) {
    let pubHistory = null

    if (
      metadata &&
      PubHistory.hasValidData(
        metadata.date_created,
        metadata.date_updated,
        metadata.date_revised,
      )
    ) {
      pubHistory = new PubHistory({
        date: {
          created: metadata.date_created,
          updated: metadata.date_updated,
          revised: metadata.date_revised,
        },
      })
    }

    return pubHistory
  }

  static hasValidData(dateCreated, dateUpdated, dateRevised) {
    let exist = false

    if (
      (dateCreated && dateCreated !== null && dateCreated !== '') ||
      (dateRevised && dateRevised !== null && dateRevised !== '') ||
      (dateUpdated && dateUpdated !== null && dateUpdated !== '')
    ) {
      exist = true
    }

    return exist
  }
}

module.exports = PubHistory
