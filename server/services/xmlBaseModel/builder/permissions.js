/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
const {
  logger: { getRawLogger },
} = require('@coko/server')

const { transformHTMLToXML } = require('./transform')
const XmlBuilder = require('./xmlBuilder')
const { replaceSpecialChars } = require('../../../_helpers')

const logger = getRawLogger()

class Permissions extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'permissions'
  }

  content() {
    const statement =
      this.data.metadata.copyrightStatement !== '' &&
      this.data.metadata.copyrightStatement !== null
        ? `<copyright-statement>${replaceSpecialChars(
            transformHTMLToXML(this.data.metadata.copyrightStatement),
          )}</copyright-statement>`
        : ''

    let license = ''

    if (this.context.isCollection !== true) {
      //
      // Book part wrapper level permissions XML

      const isOpenAccess = this.data.metadata.openAccessLicense === true
      const type = isOpenAccess ? 'license-type="open-access"' : ''

      let xlink = ''

      if (
        isOpenAccess &&
        this.data.metadata.licenseType !== 'public-domain' &&
        this.data.metadata.licenseType !== 'other'
      ) {
        xlink = `xlink:href="https://creativecommons.org/licenses/${(
          this.data.metadata.licenseType || ''
        ).toLowerCase()}/4.0/"`
      } else if (
        isOpenAccess &&
        this.data.metadata.licenseType === 'public-domain'
      ) {
        xlink = `xlink:href="https://creativecommons.org/publicdomain/zero/1.0/"`
      } else if (isOpenAccess && this.data.metadata.licenseType === 'other') {
        xlink = `xlink:href="${this.data.metadata.licenseUrl}"`
      }

      // Books use licenseStatement
      //
      const licenseP = this.data.metadata.licenseStatement
        ? `<license-p>${replaceSpecialChars(
            transformHTMLToXML(this.data.metadata.licenseStatement),
          )}</license-p>`
        : `<license-p>The license is${
            isOpenAccess ? '' : ' not'
          } open access</license-p>`

      const emitLicense =
        this.data.metadata.licenseType !== '' &&
        this.data.metadata.licenseType !== null &&
        !(
          this.data.metadata.licenseType === 'other' &&
          (this.data.metadata.licenseStatement === '' ||
            this.data.metadata.licenseStatement === null)
        )

      license = emitLicense
        ? `<license ${type} xmlns:xlink="http://www.w3.org/1999/xlink" ${xlink}>${licenseP}</license>`
        : ''
    } else {
      //
      // Collections XML

      const isOpenAccess = this.data.metadata.openAccessLicense === true
      const type = isOpenAccess ? 'license-type="open-access"' : ''

      let xlink = ''

      if (
        isOpenAccess &&
        this.data.metadata.licenseType !== 'public-domain' &&
        this.data.metadata.licenseType !== 'other'
      ) {
        xlink = `xlink:href="https://creativecommons.org/licenses/${(
          this.data.metadata.licenseType || ''
        ).toLowerCase()}/4.0/"`
      } else if (
        isOpenAccess &&
        this.data.metadata.licenseType === 'public-domain'
      ) {
        xlink = `xlink:href="https://creativecommons.org/publicdomain/zero/1.0/"`
      } else if (isOpenAccess && this.data.metadata.licenseType === 'other') {
        xlink = ``
      }

      // Collections use licenseStatement
      //
      const licenseP = this.data.metadata.licenseStatement
        ? `<p>${replaceSpecialChars(
            transformHTMLToXML(this.data.metadata.licenseStatement),
          )}</p>`
        : `<p>The license is${isOpenAccess ? '' : ' not'} open access</p>`

      const emitLicense =
        this.data.metadata.licenseType !== '' &&
        this.data.metadata.licenseType !== null &&
        !(
          this.data.metadata.licenseType === 'other' &&
          (this.data.metadata.licenseStatement === '' ||
            this.data.metadata.licenseStatement === null)
        )

      license = emitLicense
        ? `<license ${type} xmlns:xlink="http://www.w3.org/1999/xlink" ${xlink}>${licenseP}</license>`
        : ''
    }

    logger.info(`build permissions : ${this.data.metadata.copyrightStatement}`)

    return `${statement}${license}`
  }
}

module.exports = Permissions
