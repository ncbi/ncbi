/* eslint-disable no-nested-ternary */
/* eslint-disable class-methods-use-this */
const { flatten, map, groupBy } = require('lodash')
const XmlBuilder = require('./xmlBuilder')
const TocEntry = require('./tocEntry')
const TitleGroup = require('./titleGroup')
const ContribGroup = require('./contribGroup')
const { transformHTMLToXML } = require('./transform')

const ContentType = {
  // https://jats.nlm.nih.gov/extensions/bits/tag-library/2.0/element/book-back.html
  backMatter: 'book-back',
  body: 'book-body',
  frontMatter: 'front-matter',
}

const alphaindexPI = '<?alpha-index-location?>'

class TocDiv extends XmlBuilder {
  get mainTag() {
    // class TocDiv implements the model of a container of chapters or parts, i.e Divisions of Front Matter, Body, Back Matter
    // class TocEntry simply writes out XML for a chapter.
    // Parts are a container in our model of chapters or parts, but NCBI want them as 'toc-entry' XML tag
    return this.data.partBased === false ? 'toc-div' : 'toc-entry'
  }

  get isFragment() {
    return true
  }

  get attributes() {
    let attrs = {}

    this.data.attributes.forEach(attr => {
      if (attr['content-type']) {
        attrs['content-type'] = ContentType[attr['content-type']]
      } else {
        attrs = Object.assign(attrs, attr)
      }
    })

    return attrs
  }

  get children() {
    const filteredBookComponents = this.data.tocEntries.filter(
      bc => bc.metadata.hideInTOC !== true,
    )

    if (
      this.data.group === 'alpha-index' &&
      this.data.settings.toc.group_chapters_in_parts === false
    ) {
      const alphaindexEntries = []

      // Sort by first letter - this is AZ ordering on Books with no parts
      this.groupEntriesByFirstLetter(filteredBookComponents).map(entryInner =>
        alphaindexEntries.push(
          new TocDiv(
            {
              // Only A-Z ordering attributes on toc-div tag for books sorted by 'title' (A-Z ordering) and not grouped into parts
              // Note, we call without { group: 'alpha-index' } so we go into code flow below when this TocDiv runs
              attributes: [{ 'specific-use': 'alpha-index' }],
              title: entryInner.index,
              tocEntries: entryInner.data,
              settings: this.data.settings,
              partBased: false,
            },
            this.context,
          ),
        ),
      )

      const titleGroup = []

      if (this.data.title) {
        titleGroup.push(
          new TitleGroup({ title: this.data.title }, this.context),
        )
      }

      return flatten(
        [titleGroup, this.data.contributors].concat(alphaindexEntries),
      )
    }

    // All other types of sorting, except 'alpha-index' sorting on Books not organised in parts
    //

    const entries = (filteredBookComponents || []).map(toc => {
      if (toc.componentType === 'part') {
        const contribgroup = ContribGroup.contribGroupData(
          toc.metadata,
          this.context,
        )

        if (toc.secondLevel.length > 0) {
          return new TocDiv(
            {
              attributes: [],
              title: toc.title,
              tocEntries: toc.secondLevel,
              contributors: contribgroup,
              settings: this.data.settings,
              partBased: true,
            },
            this.context,
          )
        }

        return null
      }

      // Not a part , but a chapter
      const tocEntry = new TocEntry(
        {
          toc,
          settings: this.data.settings,
          tocEntries: null,
        },
        this.context,
      )

      return tocEntry
    })

    const titleGroup = []

    if (this.data.title) {
      if (this.data.partBased === false) {
        titleGroup.push(
          new TitleGroup({ title: this.data.title }, this.context),
        )
      } else {
        titleGroup.push(`<title>${transformHTMLToXML(this.data.title)}</title>`)
      }
    }

    return flatten([titleGroup, this.data.contributors].concat(entries))
  }

  contentHook(content) {
    // Only A-Z ordering PIs for books sorted by 'title' (A-Z ordering) and not grouped into parts
    if (
      this.data.group === 'alpha-index' &&
      this.data.emitPI === true &&
      this.data.settings.toc.group_chapters_in_parts === false
    ) {
      return `${alphaindexPI}${content.join('')}`
    }

    return content.join('')
  }

  groupEntriesByFirstLetter(data) {
    return map(
      groupBy(data, bc =>
        bc.title
          .replace(/<[^>]*>?/gm, '')
          .toUpperCase()
          .charAt(0),
      ),
      (value, key) => ({
        data: value,
        index: key,
      }),
    ).sort((a, b) => (a.index < b.index ? -1 : a.index > b.index ? 1 : 0))
  }
}

module.exports = TocDiv
