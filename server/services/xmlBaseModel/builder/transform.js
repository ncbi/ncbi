/* eslint-disable no-param-reassign */
const cheerio = require('cheerio')
const decoder = require('html-entities')

const transformHTMLToXML = html => {
  if (html === null || html === undefined) return ''
  const abstractHtmlObject = cheerio.load(html, null, false)

  abstractHtmlObject('strong').each((index, element) => {
    element.tagName = 'bold'
  })

  abstractHtmlObject('em').each((index, element) => {
    element.tagName = 'italic'
  })

  abstractHtmlObject('li').each((index, element) => {
    element.tagName = 'list-item'
  })

  abstractHtmlObject('ol').attr('list-type', 'order')

  abstractHtmlObject('ol').each((index, element) => {
    element.tagName = 'list'
  })

  abstractHtmlObject('ul').attr('list-type', 'bullet')

  abstractHtmlObject('ul').each((index, element) => {
    element.tagName = 'list'
  })

  // Wrap list in a p tag
  abstractHtmlObject('list').wrap('<p></p>')

  abstractHtmlObject('a').removeAttr('target')
  abstractHtmlObject('a').removeAttr('rel')

  // xmlns:xlink="http://www.w3.org/1999/xlink" ext-link-type="uri"
  abstractHtmlObject('a').attr('xmlns:xlink', 'http://www.w3.org/1999/xlink')

  abstractHtmlObject('a').attr('ext-link-type', 'uri')

  abstractHtmlObject('a').each((index, element) => {
    element.tagName = 'ext-link'
    const link = element.attribs.href
    element.attribs['xlink:href'] = link
  })

  abstractHtmlObject('ext-link').removeAttr('href')

  abstractHtmlObject('p').removeAttr('class')

  abstractHtmlObject('p')
    // eslint-disable-next-line func-names
    .filter(function (index, element) {
      return abstractHtmlObject(this).contents().length === 0
    })
    .remove()

  return decoder.decode(abstractHtmlObject.html())
}

module.exports = { transformHTMLToXML }
