/* eslint-disable class-methods-use-this */
const cheerio = require('cheerio')
const XmlBuilder = require('./xmlBuilder')
const ContribGroup = require('./contribGroup')
const BookTitleGroup = require('./bookTitleGroup')
const PubDate = require('./pubDate')
const Publisher = require('./publisher')
const Abstract = require('./abstract')
const Permissions = require('./permissions')
const { transformHTMLToXML } = require('./transform')

const {
  replaceSpecialChars,
  isStringNotEmptyOrNull,
  getAllProcessingInstructions,
} = require('../../../_helpers')

class BookMeta extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'book-meta'
  }

  get children() {
    let contribGroup = ''
    let bookTitleGroup = ''
    let pubDateStart = ''
    let pubDateEnd = ''
    let publisher = ''
    let abstract = ''
    let permissions = ''
    let edition = ''
    let volume = ''
    let isbn = ''
    let issn = ''
    let pubHistory = ''
    let customMetaWrap = ''

    if (
      this.data.metadata &&
      this.data.metadata.author &&
      ContribGroup.hasValidData(
        this.data.metadata.author,
        this.data.metadata.editor,
        this.data.metadata.collaborativeAuthors,
      )
    ) {
      contribGroup = new ContribGroup(
        {
          author: this.data.metadata.author,
          editor: this.data.metadata.editor,
          collaborativeAuthors: this.data.metadata.collaborativeAuthors,
        },
        this.context,
      )
    }

    if (this.data.title) {
      bookTitleGroup = new BookTitleGroup(this.data, this.context)
    }

    if (this.data.metadata) {
      if (
        this.data.metadata.pubDate &&
        this.data.metadata.pubDate.dateType === 'pubr'
      ) {
        pubDateStart = new PubDate(
          {
            pubDate: {
              ...this.data.metadata.pubDate,
              date: {
                ...this.data.metadata.pubDate.date,
                year: this.data.metadata.pubDate.dateRange.startYear,
                month: this.data.metadata.pubDate.dateRange.startMonth,
              },
            },
          },
          this.context,
        )

        // Actually, we can have a range where only the start year is defined

        if (this.data.metadata.pubDate.dateRange.endYear) {
          pubDateEnd = new PubDate(
            {
              pubDate: {
                ...this.data.metadata.pubDate,
                date: {
                  ...this.data.metadata.pubDate.date,
                  year: this.data.metadata.pubDate.dateRange.endYear,
                  month: this.data.metadata.pubDate.dateRange.startMonth,
                },
              },
            },
            this.context,
          )
        }
      } else if (
        this.data.metadata.pubDate &&
        this.data.metadata.pubDate.dateType === 'pub'
      ) {
        pubDateStart = new PubDate(
          {
            pubDate: this.data.metadata.pubDate,
          },
          this.context,
        )
      }
    }

    if (
      this.data.metadata &&
      (this.data.metadata.pub_loc !== '' || this.data.metadata.pub_name !== '')
    ) {
      publisher = new Publisher(this.data.metadata, this.context)
    }

    if (
      (this.data.metadata &&
        this.data.metadata.licenseStatement &&
        this.data.metadata.licenseType) ||
      this.data.metadata.copyrightStatement
    ) {
      permissions = new Permissions(this.data, this.context)
    }

    if (
      this.data &&
      (this.data.abstract || this.data.abstractTitle) &&
      transformHTMLToXML(this.data.abstract) !== ''
    ) {
      abstract = new Abstract(this.data, this.context)
    }

    if (this.data && isStringNotEmptyOrNull(this.data.metadata.edition)) {
      edition = `<edition>${replaceSpecialChars(this.data.edition)}</edition>`
    }

    if (
      this.data.metadata &&
      isStringNotEmptyOrNull(this.data.metadata.volume)
    ) {
      volume = `<book-volume-number>${replaceSpecialChars(
        this.data.metadata.volume,
      )}</book-volume-number>`
    }

    if (
      this.data.metadata &&
      this.data.metadata.issn &&
      this.data.metadata.issn !== ''
    ) {
      issn = `<issn>${replaceSpecialChars(this.data.metadata.issn)}</issn>`
    }

    if (this.data.metadata && isStringNotEmptyOrNull(this.data.metadata.isbn)) {
      isbn = `<isbn>${replaceSpecialChars(this.data.metadata.isbn)}</isbn>`
    }

    // This is the book's metadata record of publication history
    // Goes inside "book-meta"

    if (
      this.data.metadata.dateCreated !== null &&
      this.data.metadata.dateUpdated !== null
    ) {
      const dateCreatedDay =
        this.data.metadata.dateCreated.day !== null &&
        this.data.metadata.dateCreated.day !== ''
          ? `<day>${this.data.metadata.dateCreated.day}</day>`
          : ''

      const dateCreatedMonth =
        this.data.metadata.dateCreated.month !== null &&
        this.data.metadata.dateCreated.month !== ''
          ? `<month>${this.data.metadata.dateCreated.month}</month>`
          : ''

      const dateCreatedYear =
        this.data.metadata.dateCreated.year !== null &&
        this.data.metadata.dateCreated.year !== ''
          ? `<year>${this.data.metadata.dateCreated.year}</year>`
          : ''

      const dateCreated =
        dateCreatedDay !== '' ||
        dateCreatedMonth !== '' ||
        dateCreatedYear !== ''
          ? `<date date-type="created">${dateCreatedDay}${dateCreatedMonth}${dateCreatedYear}</date>`
          : ''

      // Update Date
      const dateUpdatedDay =
        this.data.metadata.dateUpdated.day !== null &&
        this.data.metadata.dateUpdated.day !== ''
          ? `<day>${this.data.metadata.dateUpdated.day}</day>`
          : ''

      const dateUpdatedMonth =
        this.data.metadata.dateUpdated.month !== null &&
        this.data.metadata.dateUpdated.month !== ''
          ? `<month>${this.data.metadata.dateUpdated.month}</month>`
          : ''

      const dateUpdatedYear =
        this.data.metadata.dateUpdated.year !== null &&
        this.data.metadata.dateUpdated.year !== ''
          ? `<year>${this.data.metadata.dateUpdated.year}</year>`
          : ''

      const dateUpdated =
        dateUpdatedDay !== '' ||
        dateUpdatedMonth !== '' ||
        dateUpdatedYear !== ''
          ? `<date date-type="updated">${dateUpdatedDay}${dateUpdatedMonth}${dateUpdatedYear}</date>`
          : ''

      pubHistory =
        dateUpdated !== '' || dateCreated !== ''
          ? `<pub-history>${dateCreated}${dateUpdated}</pub-history>`
          : ''
    }

    if (this.data.metadata.sourceType !== '') {
      // custom-meta-wrap
      customMetaWrap = `<custom-meta-group><custom-meta><meta-name>books-source-type</meta-name><meta-value>${this.data.metadata.sourceType}</meta-value></custom-meta></custom-meta-group>`
    }

    // Author notes
    let notesAuthorContent = ''

    if (this.data.metadata.notes) {
      notesAuthorContent = this.data.metadata.notes.map(note => {
        const noteTitle = note.title
          ? `<label>${replaceSpecialChars(note.title)}</label>`
          : ''

        const noteDescription = note.description
          ? replaceSpecialChars(transformHTMLToXML(note.description))
          : ''

        switch (note.type) {
          case 'author-note':
            return `<author-notes><fn>${noteTitle}${noteDescription}</fn></author-notes>`
          default:
            return ''
        }
      })
    }

    // Non-author notes
    // Notes

    let notesContent = ''

    if (this.data.metadata.notes) {
      let mixedCitationId = 0

      notesContent = this.data.metadata.notes.map(note => {
        const noteTitle = note.title
          ? `<title>${replaceSpecialChars(note.title)}</title>`
          : ''

        // cant use <title> inside <mixed-citation>
        const noteTitlePlain = note.title
          ? `${replaceSpecialChars(note.title)}`
          : ''

        const noteDescription = note.description
          ? replaceSpecialChars(transformHTMLToXML(note.description))
          : ''

        const noteDescriptionHTMLObject = cheerio.load(
          note.description,
          null,
          false,
        )

        let noteDescriptionStripped = ''
        let mixedCitationText = []

        if (
          note.type === 'suggestedCitation' ||
          note.type === 'geneticCounseling' ||
          note.type === 'resources'
        ) {
          let contentTypeValue = ''

          switch (note.type) {
            case 'geneticCounseling':
              contentTypeValue = 'genetic_counseling'
              break
            case 'resources':
              contentTypeValue = 'resources'
              break
            default:
              contentTypeValue = ''
          }

          // We need to massage all P tags for certain types of note : we cannot have P tags contained within another P tag.
          noteDescriptionHTMLObject('p').attr('content-type', contentTypeValue)

          noteDescriptionStripped = replaceSpecialChars(
            transformHTMLToXML(noteDescriptionHTMLObject.html()),
          )

          // For mixed citation, there are no P tags possible inside <mixed-citation>, just styled text
          noteDescriptionHTMLObject('p').each((index, element) => {
            mixedCitationText.push(noteDescriptionHTMLObject(element).html())
          })

          mixedCitationText = mixedCitationText.join(' ')
        }

        switch (note.type) {
          case 'disclaimer':
            return `<notes notes-type='disclaimer'>${noteTitle}${replaceSpecialChars(
              noteDescription,
            )}</notes>`
          case 'generic':
            return `<notes>${noteTitle}${replaceSpecialChars(
              noteDescription,
            )}</notes>`
          case 'courtesy-note':
            return `<notes notes-type='courtesy-note'>${noteTitle}${replaceSpecialChars(
              noteDescription,
            )}</notes>`
          case 'suggestedCitation':
            // eslint-disable-next-line no-plusplus
            mixedCitationId++
            return `<notes notes-type='suggested-citation'><p>
              <mixed-citation publication-type="other" id="mcid-${mixedCitationId}">
              ${noteTitlePlain} ${transformHTMLToXML(
              mixedCitationText,
            )}</mixed-citation></p></notes>`
          case 'geneticCounseling':
            // eslint-disable-next-line no-case-declarations
            const gcTitle = noteTitlePlain
              ? `<p content-type="genetic_counseling">${noteTitlePlain}</p>`
              : ''

            return `<notes notes-type='ednotes'>${gcTitle}${noteDescriptionStripped}</notes>`
          case 'resources':
            // eslint-disable-next-line no-case-declarations
            const resTitle = noteTitlePlain
              ? `<p content-type="resources">${noteTitlePlain}</p>`
              : ''

            return `<notes notes-type='ednotes'>${resTitle}${noteDescriptionStripped}</notes>`
          case 'manuscriptFinalVersionNote':
            return `<notes>${noteTitle}${noteDescription}</notes>`
          default:
            return ''
        }
      })
    }

    return [
      bookTitleGroup,
      contribGroup,
      `${notesAuthorContent.join('')}`,
      pubDateStart,
      pubDateEnd,
      volume,
      issn,
      isbn,
      publisher,
      edition,
      pubHistory,
      permissions,
      abstract,
      customMetaWrap,
      `${notesContent.join('')}`,
    ]
  }

  contentHook(content) {
    const selectChaptersPI =
      // eslint-disable-next-line no-nested-ternary
      this.processingInstructions &&
      this.processingInstructions.includes('select_chapters')
        ? '<?select_chapters?>'
        : // eslint-disable-next-line no-nested-ternary
        !this.data.collection || this.data.collection === null
        ? ''
        : // eslint-disable-next-line no-nested-ternary
        this.data.settings.chapterIndependently &&
          this.data.collection.collectionType === 'funded'
        ? '<?select_chapters?>'
        : ''

    const authorManuscriptPI =
      // eslint-disable-next-line no-nested-ternary
      this.processingInstructions &&
      this.processingInstructions.includes('book-chapter-manuscript')
        ? '<?book-chapter-manuscript?>'
        : // eslint-disable-next-line no-nested-ternary
        !this.data.collection || this.data.collection === null
        ? ''
        : // eslint-disable-next-line no-nested-ternary
        this.data.settings.chapterIndependently &&
          this.data.collection.collectionType === 'funded' &&
          this.data.workflow === 'pdf'
        ? '<?book-chapter-manuscript?>'
        : ''

    const pmcid = `<book-id book-id-type="pmcid">${this.data.domain}</book-id>`

    const doi = this.data.metadata.doi
      ? `<book-id book-id-type="doi">${replaceSpecialChars(
          this.data.metadata.doi,
        )}</book-id>`
      : ''

    return `${selectChaptersPI}${authorManuscriptPI}${pmcid}${doi}${content.join(
      '',
    )}`
  }

  setProcessingInstructions(content) {
    this.processingInstructions = getAllProcessingInstructions(content)
  }
}

module.exports = BookMeta
