/* eslint-disable class-methods-use-this */
const { isArray } = require('lodash')
const XmlBuilder = require('./xmlBuilder')
const { transformHTMLToXML } = require('./transform')
const { replaceSpecialChars } = require('../../../_helpers')

class BookTitleGroup extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'book-title-group'
  }

  content() {
    const titleXML = this.data.title
      ? replaceSpecialChars(transformHTMLToXML(this.data.title))
      : 'Untitled'

    const titleXMLTag = `<book-title>${titleXML}</book-title>`

    const subtitleArray = isArray(this.data.subTitle)
      ? this.data.subTitle
      : [this.data.subTitle]

    const subtitleXMLTag = subtitleArray
      .map(subtitle =>
        subtitle
          ? `<subtitle>${replaceSpecialChars(
              transformHTMLToXML(subtitle),
            )}</subtitle>`
          : '',
      )
      .join('')

    const altTitleXML = this.data.altTitle
      ? replaceSpecialChars(transformHTMLToXML(this.data.altTitle))
      : ''

    const altTitleXMLTag = altTitleXML
      ? `<alt-title>${altTitleXML}</alt-title>`
      : ''

    return `${titleXMLTag}${subtitleXMLTag}${altTitleXMLTag}`
  }
}

module.exports = BookTitleGroup
