/* eslint-disable class-methods-use-this */
const crypto = require('crypto')
const XmlBuilder = require('./xmlBuilder')

class VendorMeta extends XmlBuilder {
  get isFragment() {
    return true
  }

  get doctype() {
    return `book-submit SYSTEM "books-submit-vendor.dtd"`
  }

  get attributes() {
    return {
      'bcms-id': this.data.bcmsId,
      'job-id': crypto.randomBytes(3).toString('hex'),
      'submission-type': this.data.submissionType,
    }
  }

  get mainTag() {
    return 'book-submit'
  }

  build() {
    let content = this.root.end({ allowEmptyTags: true })

    content = `<!DOCTYPE ${this.doctype}>${content}`

    return content
  }
}

module.exports = VendorMeta
