const pubHistory = require('./pubHistory')
const date = require('./date')
const titleGroup = require('./titleGroup')
const contribGroup = require('./contribGroup')
const contrib = require('./contrib')
const bookMeta = require('./bookMeta')
const vendorMeta = require('./vendorMeta')

module.exports = {
  bookMeta,
  vendorMeta,
  contrib,
  contribGroup,
  date,
  pubHistory,
  titleGroup,
}
