/* eslint-disable class-methods-use-this */
const cheerio = require('cheerio')
const XmlBuilder = require('./xmlBuilder')
const PubDateCollections = require('./pubDateCollections')
const Publisher = require('./publisher')
const Abstract = require('./abstract')
const Permissions = require('./permissions')
const ContribGroup = require('./contribGroup')
const { transformHTMLToXML } = require('./transform')

// This class is for Collection meta XML inside COLLECTION top-level XML

class CollectionsMeta extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'collection-meta'
  }

  get children() {
    const collectionId = `<collection-id>${this.data.domain}</collection-id>`

    // WORKAROUND until we figure out if title can be styled.
    //
    const abstractHtmlObject = cheerio.load(this.data.title, null, false)
    const collectionName = `<collection-name>${abstractHtmlObject.text()}</collection-name>`

    const publisher = new Publisher(this.data.metadata, this.context)

    const issn = this.data.metadata.issn
      ? `<?issn ${this.data.metadata.issn}?>`
      : ''

    const eissn = this.data.metadata.eissn
      ? `<?eissn ${this.data.metadata.eissn}?>`
      : ''

    // pubDate
    let pubDateStart = ''
    let pubDateEnd = ''

    if (
      this.data.metadata.pubDate &&
      this.data.metadata.pubDate.dateType === 'pubr'
    ) {
      pubDateStart = new PubDateCollections(
        {
          pubDate: {
            ...this.data.metadata.pubDate,
            date: {
              ...this.data.metadata.pubDate.date,
              year: this.data.metadata.pubDate.dateRange.startYear,
              month: this.data.metadata.pubDate.dateRange.startMonth,
            },
          },
        },
        this.context,
      )

      // Actually, we can have a range where only the start year is defined

      if (this.data.metadata.pubDate.dateRange.endYear) {
        pubDateEnd = new PubDateCollections(
          {
            pubDate: {
              ...this.data.metadata.pubDate,
              date: {
                ...this.data.metadata.pubDate.date,
                year: this.data.metadata.pubDate.dateRange.endYear,
                month: this.data.metadata.pubDate.dateRange.endMonth,
              },
            },
          },
          this.context,
        )
      }
    }

    // Abstract
    //
    let abstract = ''

    if (
      (this.data.abstract || this.data.abstractTitle) &&
      transformHTMLToXML(this.data.abstract) !== ''
    ) {
      abstract = new Abstract(this.data, this.context)
    }

    // Authors & Editors
    //
    let contribGroup = ''

    if (
      this.data.metadata &&
      ContribGroup.hasValidData(
        [],
        this.data.metadata.editor,
        this.data.metadata.author,
      )
    ) {
      contribGroup = new ContribGroup(
        {
          author: [],
          editor: this.data.metadata.editor,
          collaborativeAuthors: this.data.metadata.author,
        },
        this.context,
      )
    }

    // Author notes
    let notesAuthorContent = ''

    if (this.data.metadata.notes) {
      notesAuthorContent = this.data.metadata.notes.map(note => {
        const noteTitle = note.title ? `<label>${note.title}</label>` : ''

        const noteDescription = note.description
          ? transformHTMLToXML(note.description)
          : ''

        switch (note.type) {
          case 'author-note':
            return `<author-notes><fn>${noteTitle}${noteDescription}</fn></author-notes>`
          default:
            return ''
        }
      })
    }

    // Non-author notes
    // Notes

    let notesContent = ''

    if (this.data.metadata.notes) {
      notesContent = this.data.metadata.notes.map(note => {
        const noteTitle = note.title ? `<title>${note.title}</title>` : ''

        const noteDescription = note.description
          ? transformHTMLToXML(note.description)
          : ''

        switch (note.type) {
          case 'disclaimer':
            return `<notes notes-type='disclaimer'>${noteTitle}${noteDescription}</notes>`
          case 'generic':
            return `<notes>${noteTitle}${noteDescription}</notes>`
          case 'courtesy-note':
            return `<notes notes-type='courtesy-note'>${noteTitle}${noteDescription}</notes>`
          default:
            return ''
        }
      })
    }

    // permissions
    let permissions = ''

    if (
      (this.data.metadata.licenseStatement && this.data.metadata.licenseType) ||
      this.data.metadata.copyrightStatement
    ) {
      permissions = new Permissions(this.data, {
        ...this.context,
        isCollection: true,
      })
    }

    // custom-meta-wrap
    const customMetaWrap =
      '<custom-meta-wrap><custom-meta><meta-name>books-source-type</meta-name><meta-value>Collection</meta-value></custom-meta></custom-meta-wrap>'

    return [
      `${collectionId}`,
      `${collectionName}`,
      contribGroup,
      `${notesAuthorContent.join('')}`,
      publisher,
      `${issn}`,
      `${eissn}`,
      pubDateStart,
      pubDateEnd,
      permissions,
      abstract,
      `${customMetaWrap}`,
      `${notesContent.join('')}`,
      '<collection-list/>',
    ]
  }
}

module.exports = CollectionsMeta
