/* eslint-disable class-methods-use-this */
const {
  logger: { getRawLogger },
} = require('@coko/server')

const { transformHTMLToXML } = require('./transform')
const XmlBuilder = require('./xmlBuilder')
const { replaceSpecialChars } = require('../../../_helpers')

const logger = getRawLogger()

class Abstract extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'abstract'
  }

  content() {
    const title = this.data.abstractTitle
      ? `<title>${replaceSpecialChars(this.data.abstractTitle)}</title>`
      : ''

    const content = this.data.abstract
      ? replaceSpecialChars(transformHTMLToXML(this.data.abstract))
      : ''

    // Graphic
    let graphicTag = ''
    const useGraphic = this.data.fileAbstract !== null

    if (useGraphic) {
      if (this.data.abstractGraphic) {
        const fileName = this.data.abstractGraphic.name

        graphicTag = `<graphic xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="${fileName}"></graphic>`
      }
    }

    const abstractTag = `${title}${graphicTag}${content}`

    logger.info(`build abstract : ${abstractTag}  `)

    return abstractTag
  }
}

module.exports = Abstract
