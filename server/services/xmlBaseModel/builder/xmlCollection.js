/* eslint-disable class-methods-use-this */
const XmlBuilder = require('./xmlBuilder')
const CollectionsMeta = require('./collectionsMeta')

class XmlCollection extends XmlBuilder {
  get mainTag() {
    return 'collection'
  }

  get attributes() {
    return {
      'dtd-version': '2.3',
      id: 'collection',
      'xmlns:mml': 'http://www.w3.org/1998/Math/MathML',
      // 'xmlns:xlink': 'http://www.w3.org/1999/xlink',
    }
  }

  get children() {
    const collectionMeta = new CollectionsMeta(this.data, this.context)

    return [collectionMeta]
  }
}

module.exports = XmlCollection
