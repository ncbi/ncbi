/* eslint-disable class-methods-use-this */
const XmlBuilder = require('./xmlBuilder')
const ContribGroup = require('./contribGroup')
const { replaceSpecialChars, formatDate } = require('../../../_helpers')

class TocEntry extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'toc-entry'
  }

  get children() {
    // We need to fudge up 'collaborativeAuthors' array to shove
    // the data into a key 'givenName' , to match the data structure
    // at the book level metadata.
    // c.f ContribGroup, and contrib
    if (this.data.toc.metadata.collaborativeAuthors) {
      this.data.toc.metadata.collaborativeAuthors = this.data.toc.metadata.collaborativeAuthors.map(
        cauthor => {
          return { givenName: cauthor }
        },
      )
    }

    const contribgroup = ContribGroup.contribGroupData(
      this.data.toc.metadata,
      this.context,
    )

    return [contribgroup]
  }

  contentHook(content) {
    let label = ''
    let navpointer = ''
    let altTitle = ''
    let subTitle = ''
    let chapterLevelDates = ''
    let abstractTocEntry = ''
    let nestedSections = ''

    if (
      this.data.settings.toc.order_chapters_by === 'number' &&
      this.data.toc.metadata.chapter_number
    ) {
      label = `<label>${this.data.toc.metadata.chapter_number}</label>`
    }

    // We use the versionName of the Book Component not the File !
    if (this.data.toc.convertedFile && this.data.toc.convertedFile.length > 0) {
      navpointer = `<nav-pointer>
      <related-object document-id="${
        this.data.toc.metadata.book_component_id || ''
      }" document-type="${this.data.toc.componentType || ''}">
        <?xml_file ${this.data.toc.convertedFile[0].name || ''}?>
        <?version ${this.data.toc.versionName}?>
      </related-object>
      </nav-pointer>`
    }

    if (this.data.toc.metadata.alt_title) {
      altTitle = `<subtitle content-type="alt-title">${replaceSpecialChars(
        this.data.toc.metadata.alt_title,
      )}</subtitle>`
    }

    if (this.data.toc.metadata.sub_title) {
      subTitle = `<subtitle>${replaceSpecialChars(
        this.data.toc.metadata.sub_title,
      )}</subtitle>`
    }

    let datePublicationFormatToken = 'ppub'

    switch (this.data.toc.metadata.date_publication_format) {
      case 'print':
        datePublicationFormatToken = 'ppub'
        break
      case 'electronic':
        datePublicationFormatToken = 'epub'
        break
      default:
        break
    }

    // Here we want to make sure months and days are leading zero formatted
    // if they are single digits
    chapterLevelDates = `${
      this.data.toc.metadata.date_created
        ? `<?date-created ${
            formatDate(this.data.toc.metadata.date_created) || ''
          }?>`
        : ''
    } ${
      this.data.toc.metadata.date_updated
        ? `<?date-updated ${
            formatDate(this.data.toc.metadata.date_updated) || ''
          }?>`
        : ''
    }   ${
      this.data.toc.metadata.date_revised
        ? `<?date-revised ${
            formatDate(this.data.toc.metadata.date_revised) || ''
          }?>`
        : ''
    }
        ${
          this.data.toc.metadata.date_publication
            ? `<?${datePublicationFormatToken} ${
                formatDate(this.data.toc.metadata.date_publication) || ''
              }?>`
            : ''
        }
    
    `

    if (this.data.toc.metadata.abstractTitle) {
      abstractTocEntry = `<toc-entry><title>${replaceSpecialChars(
        (this.data.toc.metadata.abstractTitle || '').replace(/<[^>]+>/g, ''),
      )}</title>
          <nav-pointer>
          <related-object document-id="${
            this.data.toc.metadata.book_component_id || ''
          }" document-type="chapter" object-id="${
        this.data.toc.metadata.abstractId
      }" object-type="sec"/>
          </nav-pointer>
          </toc-entry>`
    }

    nestedSections = (this.data.toc.metadata.section_details || []).map(
      (sec, index) => {
        let secLabel = ''

        if (sec.label) {
          secLabel = `<label>${sec.label}</label>`
        }

        return `<toc-entry>
        ${secLabel}
        <title>${replaceSpecialChars(
          (sec.title || 'Untitled').replace(/<[^>]+>/g, ''),
        )}</title>
        <nav-pointer>
        <related-object document-id="${
          this.data.toc.metadata.book_component_id || ''
        }" document-type="chapter" object-id="${sec.id}" object-type="sec"/>
        </nav-pointer>
        </toc-entry>`
      },
    )

    return `
    ${label}
    <title>${replaceSpecialChars(this.data.toc.title)}</title>
    ${altTitle}
    ${subTitle}
    ${chapterLevelDates}
    ${content.join('')}
    ${navpointer}
    ${abstractTocEntry}
    ${nestedSections.join('')}
    `
  }
}

module.exports = TocEntry
