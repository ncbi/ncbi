/* eslint-disable class-methods-use-this */
const XmlBuilder = require('./xmlBuilder')
const { transformHTMLToXML } = require('./transform')

class TitleGroup extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'toc-title-group'
  }

  content() {
    return `<title>${transformHTMLToXML(this.data.title)}</title>`
  }
}

module.exports = TitleGroup
