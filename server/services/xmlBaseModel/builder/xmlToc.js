/* eslint-disable class-methods-use-this */
const XmlBuilder = require('./xmlBuilder')
const CollectionMeta = require('./collectionMeta')
const BookMeta = require('./bookMeta')
const TocDiv = require('./TocDiv')

class XmlToc extends XmlBuilder {
  get mainTag() {
    return 'book-part-wrapper'
  }

  get attributes() {
    return {
      'dtd-version': '2.0',
      id: 'toc',
      'xmlns:ali': 'http://www.niso.org/schemas/ali/1.0/',
      'xmlns:mml': 'http://www.w3.org/1998/Math/MathML',
      'xmlns:xi': 'http://www.w3.org/2001/XInclude',
      'xmlns:xlink': 'http://www.w3.org/1999/xlink',
    }
  }

  get children() {
    const collectionMeta = []

    if (this.data.settings.chapterIndependently === true) {
      if (this.data.collection) {
        // Both types of collections have the 'ncbi-books-collection' type of Collection Meta XML
        //
        collectionMeta.push(
          new CollectionMeta(this.data.collection, this.context),
        )

        // We need to add a 'book-series' type XML
        // Don't add it if the publisher book series title isn't set
        if (
          this.data.collection.collectionType === 'bookSeries' &&
          !!this.data.collection.metadata.publisherBookSeriesTitle
        ) {
          collectionMeta.push(
            new CollectionMeta(this.data.collection, {
              ...this.context,
              bookSeriesAdditionalNode: true,
            }),
          )
        }
      }
    }

    const bookMeta = new BookMeta(this.data, this.context)

    const tocDiv = this.data.divisions
      .map(div => {
        if (div.bookComponents && div.bookComponents.length === 0) return ''

        const filteredBookComponents = div.bookComponents.filter(
          bc => bc.metadata.hideInTOC !== true,
        )

        if (filteredBookComponents.length === 0) return ''

        if (
          div.label === 'body' &&
          this.data.settings.toc.order_chapters_by === 'title'
        ) {
          const tD = new TocDiv(
            {
              attributes: [{ 'content-type': div.label }],
              group: 'alpha-index',
              emitPI: true,
              tocEntries: filteredBookComponents,
              settings: this.data.settings,
              partBased: false,
            },
            this.context,
          )

          return tD
        }

        const tD = new TocDiv(
          {
            attributes: [{ 'content-type': div.label }],
            tocEntries: filteredBookComponents,
            settings: this.data.settings,
            group_chapters_in_parts: this.data.settings.toc
              .group_chapters_in_parts,
            partBased: false,
          },
          this.context,
        )

        return tD
      })
      .filter(x => x) // Remove empty elements!

    if (this.data.collection) {
      return [...collectionMeta, bookMeta, '<toc>', ...tocDiv, '</toc>']
    }

    return [bookMeta, '<toc>', ...tocDiv, '</toc>']
  }
}

module.exports = XmlToc
