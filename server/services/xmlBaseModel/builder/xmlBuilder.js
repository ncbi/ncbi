const { create, fragment } = require('xmlbuilder2')
const decoder = require('html-entities')

class XmlBuilder {
  constructor(data, context) {
    this.context = context
    this.data = data

    const parse = (parent, contents) => parent.txt(contents)

    if (this.isFragment === true) {
      this.root = fragment({
        encoding: 'UTF-8',
        parser: { parse },
      }).ele(this.mainTag)

      if (this.attributes) {
        this.root = this.root.att(this.attributes)
      }
    } else {
      this.root = create({ parser: { parse } })
        .ele(this.mainTag)
        .att(this.attributes)
        .dtd({
          pubID: this.context.pubID,
          sysID: this.context.sysID,
        })
    }
  }

  build() {
    let content = false

    if (this.content) {
      content = this.content()
    } else {
      content = this.children.map(
        child => (child && child.build && child.build()) || child,
      )

      if (this.contentHook) {
        content = this.contentHook(content)
      } else {
        content = content.join('')
      }
    }

    content = this.root.ele(content).end()
    content = decoder.decode(content)

    if (this.doctype) {
      content = `<!DOCTYPE ${this.doctype}>${content}`
    }

    return content
  }
}

module.exports = XmlBuilder
