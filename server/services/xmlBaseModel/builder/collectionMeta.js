/* eslint-disable class-methods-use-this */
const cheerio = require('cheerio')
const XmlBuilder = require('./xmlBuilder')
const { transformHTMLToXML } = require('./transform')

// This class is for Collection meta XML inside BOOK PART WRAPPER top level XML
// *not* inside COLLECTION top-level XML

class CollectionMeta extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'collection-meta'
  }

  get attributes() {
    return {
      'collection-type':
        this.data.collectionType === 'bookSeries' &&
        this.context.bookSeriesAdditionalNode === true
          ? 'book-series'
          : 'ncbi-books-collection',
    }
  }

  content() {
    let collectionId = ''

    if (this.context.bookSeriesAdditionalNode !== true) {
      collectionId = `<collection-id collection-id-type='pmcid'>${this.data.domain}</collection-id>`
    }

    const issn = this.data.metadata.issn
      ? `<?issn ${this.data.metadata.issn}?>`
      : ''

    const eissn = this.data.metadata.eissn
      ? `<?eissn ${this.data.metadata.eissn}?>`
      : ''

    let { title } = this.data

    if (
      this.data.collectionType === 'bookSeries' &&
      this.context.bookSeriesAdditionalNode
    ) {
      title = this.data.metadata.publisherBookSeriesTitle
    }

    if (title !== null) {
      const abstractHtmlObject = cheerio.load(title, null, false)
      // remove any P tags wrapped around the content
      const titlePstripped = abstractHtmlObject('p').html()

      if (titlePstripped !== null) {
        title = titlePstripped
      }
    }

    const collectionTitle = `<title-group><title>${transformHTMLToXML(
      title,
    )}</title></title-group>`

    return `${collectionId}${collectionTitle}${issn}${eissn}`
  }
}

module.exports = CollectionMeta
