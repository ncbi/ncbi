/* eslint-disable class-methods-use-this */
const XmlBuilder = require('./xmlBuilder')

class PubDateCollections extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'pub-date'
  }

  get attributes() {
    let prefix = 'p'

    switch (this.data.pubDate.publicationFormat) {
      case 'electronic':
        prefix = 'e'
        break
      case 'print':
        prefix = 'p'
        break
      default:
        break
    }

    return {
      'pub-type': `${prefix}${this.data.pubDate.dateType}`,
    }
  }

  content() {
    const { day, year, month } = this.data.pubDate.date

    let content = ''
    if (day) content = `${content}<day>${day}</day>`
    if (month) content = `${content}<month>${month}</month>`
    if (year) content = `${content}<year>${year}</year>`

    return content
  }
}

module.exports = PubDateCollections
