/* eslint-disable class-methods-use-this */
const { isArray } = require('lodash')
const XmlBuilder = require('./xmlBuilder')
const Contrib = require('./contrib')
const Affiliation = require('./affiliation')

class ContribGroup extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'contrib-group'
  }

  get children() {
    let authors = []
    let editors = []
    let collabAuthors = []

    let affiliations = []
    let affiliationsList = []

    if (this.data.author) {
      this.data.author.forEach(author => {
        if (author.affiliation && isArray(author.affiliation)) {
          author.affiliation.forEach(affiliation => {
            affiliations = [...affiliations, affiliation]
          })
        }
      })
    }

    if (this.data.editor) {
      this.data.editor.forEach(editor => {
        if (editor.affiliation && isArray(editor.affiliation)) {
          editor.affiliation.forEach(affiliation => {
            affiliations = [...affiliations, affiliation]
          })
        }
      })
    }

    if (affiliations.length) {
      affiliationsList = Array.from(new Set(affiliations)).map(
        (affiliation, index) =>
          new Affiliation(
            {
              attributes: { id: affiliation },
              affiliation,
            },
            this.context,
          ),
      )
    }

    if (this.data.author) {
      authors = this.data.author
        .filter(el => {
          return el.surname && el.givenName
        })
        .map(
          author =>
            new Contrib(
              {
                attributes: { type: 'author' },
                user: author,
              },
              this.context,
            ),
        )
    }

    if (this.data.editor) {
      editors = this.data.editor
        .filter(el => {
          return el.surname && el.givenName
        })
        .map(
          editor =>
            new Contrib(
              {
                attributes: { type: 'editor' },
                user: editor,
              },
              this.context,
            ),
        )
    }

    // This is book level metadata collaborative author spec
    // i.e with key 'givenName'
    if (this.data.collaborativeAuthors) {
      collabAuthors = this.data.collaborativeAuthors
        .filter(el => {
          return el.givenName
        })
        .map(
          collabAuthor =>
            new Contrib(
              {
                attributes: { type: 'author' },
                collaborativeAuthor: true,
                user: collabAuthor,
              },
              this.context,
            ),
        )
    }

    return [...authors, ...editors, ...collabAuthors, ...affiliationsList]
  }

  static contribGroupData(metadata, context) {
    let contribgroup = null

    if (
      metadata &&
      metadata.author &&
      ContribGroup.hasValidData(
        metadata.author,
        metadata.editor,
        metadata.collaborativeAuthors,
      )
    ) {
      contribgroup = new ContribGroup(
        {
          editor: metadata.editor,
          author: metadata.author,
          collaborativeAuthors: metadata.collaborativeAuthors,
        },
        context,
      )
    }

    return contribgroup
  }

  static hasValidData(authors, editors, collabAuthors) {
    let exist = false

    authors.forEach(author => {
      exist =
        Object.keys(author).some(prop => author[prop] !== '') && exist === false
          ? true
          : exist
    })

    editors.forEach(ed => {
      exist =
        Object.keys(ed).some(prop => ed[prop] !== '') && exist === false
          ? true
          : exist
    })

    exist = collabAuthors.length > 0 && exist === false ? true : exist

    return exist
  }
}

module.exports = ContribGroup
