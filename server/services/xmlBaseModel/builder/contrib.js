/* eslint-disable class-methods-use-this */
const XmlBuilder = require('./xmlBuilder')

class Contrib extends XmlBuilder {
  get isFragment() {
    return true
  }

  get mainTag() {
    return 'contrib'
  }

  get attributes() {
    return {
      'contrib-type': this.data.attributes.type,
    }
  }

  content() {
    let surname = ''
    let givenName = ''
    let degrees = ''
    let affiliation = ''
    let email = ''
    let nameFlag = false
    let nameTag = ''
    let role = ''
    let suffix = ''

    if (this.data.collaborativeAuthor !== true) {
      if (this.data.user.surname) {
        surname = `<surname>${this.data.user.surname}</surname>`
        nameFlag = true
      }

      if (this.data.user.givenName) {
        givenName = `<given-names>${this.data.user.givenName}</given-names>`
        nameFlag = true
      }

      if (this.data.user.suffix) {
        suffix = `<suffix>${this.data.user.suffix}</suffix>`
        nameFlag = true
      }

      if (nameFlag) {
        nameTag = `<name>${surname}${givenName}${suffix}</name>`
      }

      if (this.data.user.degrees) {
        degrees = `<degrees>${this.data.user.degrees}</degrees>`
      }

      if (this.data.user.affiliation) {
        this.data.user.affiliation.map(
          value =>
            (affiliation += `<xref ref-type="aff" rid="${value
              .toLowerCase()
              .replace(/[^A-Z0-9]/gi, '')}"/>`),
        )
      }

      if (this.data.user.email) {
        email = `<email>${this.data.user.email}</email>`
      }

      if (this.data.user.role) {
        role = `<role>${this.data.user.role}</role>`
      }

      return `${nameTag}${degrees}${affiliation}${email}${role}`
    }

    return `<collab>${this.data.user.givenName}</collab>`
  }
}

module.exports = Contrib
