/* eslint-disable class-methods-use-this */
const XmlBase = require('./xmlBase')

class Section extends XmlBase {
  get OverwriteSchema() {
    return true
  }

  validate(value) {
    if (
      value.name === 'sec' &&
      (value.attributes['sec-type'] === 'figs-and-tables' ||
        value.attributes['sec-type'] === 'link-group')
    ) {
      return false
    }

    return true
  }

  schema() {
    return {
      id: {
        path: {
          location: `sec:id`,
          inRoot: true,
        },
      },
      label: {
        path: {
          location: `label`,
          onlyInChildren: true,
        },
      },
      p: {
        path: {
          location: `p`,
          onlyInChildren: true,
        },
      },
      title: {
        path: {
          location: `title`,
          onlyInChildren: true,
        },
      },
    }
  }

  title(value) {
    return this.getContent(value, 'title')
  }

  p(value) {
    return this.getContent(value, 'p')
  }

  label(value) {
    return this.getContent(value, 'label')
  }
}

module.exports = Section
