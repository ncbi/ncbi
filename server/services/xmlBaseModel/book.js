/* eslint-disable class-methods-use-this */
const XmlBase = require('./xmlBase')
const MetadataBook = require('./metadataBook')
const Body = require('./body')
const XmlFactory = require('./xmlFactory')

class Book extends XmlBase {
  schema() {
    return {
      id: {
        path: {
          location: [`book:id`],
        },
      },
      body: {
        path: {
          location: `body`,
        },
        defaultValue: {},
      },
      metadata: {
        path: {
          location: [`book-meta`, `article-meta`],
        },
        defaultValue: {},
      },
      namedBookPartBody: {
        path: {
          location: `named-book-part-body`,
        },
        defaultValue: {},
      },
    }
  }

  metadata(value) {
    if (!value) return {}
    return new MetadataBook(`<book-meta>${value}</book-meta>`)
  }

  body(value) {
    if (!value) return null

    return XmlFactory.xmlToModel(value, Body, {}, { forceStream: true })
  }

  namedBookPartBody(value) {
    if (!value) return null
    return XmlFactory.xmlToModel(value, Body, {}, { forceStream: true })
  }
}

module.exports = Book
