/* eslint-disable class-methods-use-this */
const XmlBase = require('./xmlBase')
const BookPart = require('./bookPart')
const Body = require('./body')

const XmlFactory = require('./xmlFactory')

class BookPartWrapper extends XmlBase {
  schema() {
    return {
      id: {
        path: {
          location: [`book-part-wrapper:id`, `book-part:id`, `book-app:id`],
        },
      },
      type: {
        path: {
          location: [
            `book-part-wrapper:content-type`,
            `book-part:book-part-type`,
            `book-app:book-part-type`,
          ],
        },
        defaultValue: null,
      },
      body: {
        path: {
          location: `body`,
        },
        defaultValue: {},
      },
      metadata: {
        path: {
          location: [
            'book-app',
            'book-app-group',
            'book-part',
            'dedication',
            'foreword',
            'front-matter-part',
            'preface',
          ],
          onlyInChildren: true,
        },
        defaultValue: {},
      },
      namedBookPartBody: {
        path: {
          location: `named-book-part-body`,
        },
        defaultValue: {},
      },
      hideInTOC: {
        path: {
          location: [`hideTOC`],
        },
        defaultValue: false,
      },
    }
  }

  metadata(value) {
    if (!value) return {}
    const bookPart = new BookPart(value)
    return bookPart.metadata
  }

  body(value) {
    if (!value) return {}

    const typeFlag = !!(
      this.model.type === 'section' || this.model.type === 'chapter'
    )

    return XmlFactory.xmlToModel(
      value,
      Body,
      {
        parseSectionChildren: typeFlag,
      },
      { forceStream: true },
    )
  }

  namedBookPartBody(value) {
    if (!value) return {}

    return XmlFactory.xmlToModel(value, Body, {}, { forceStream: true })
  }

  hideInTOC(value) {
    return value.includes('hideTOC')
  }
}

module.exports = BookPartWrapper
