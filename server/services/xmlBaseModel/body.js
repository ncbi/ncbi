/* eslint-disable no-return-await */
/* eslint-disable array-callback-return */
/* eslint-disable handle-callback-err */
/* eslint-disable camelcase */
/* eslint-disable class-methods-use-this */
const XmlBase = require('./xmlBase')
const Sec = require('./section')
const XmlFactory = require('./xmlFactory')

class Body extends XmlBase {
  get OverwriteSchema() {
    return true
  }

  schema() {
    return {
      content: {
        path: {
          location: `*`,
        },
      },
      section: {
        path: {
          location: `sec`,
          onlyInChildren: true,
        },
        hasMultiple: true,
      },
    }
  }

  section(value) {
    if (this.options.parseSectionChildren === true) {
      return Promise.all(
        value.map(v => {
          return XmlFactory.xmlToModel(v, Sec, {}, { forceStream: true })
        }),
      )
    }

    return []
  }
}

module.exports = Body
