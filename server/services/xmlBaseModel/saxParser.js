/* eslint-disable no-param-reassign */
/* eslint-disable handle-callback-err */
const isArray = require('lodash/isArray')
const isFunction = require('lodash/isFunction')
const forEach = require('lodash/forEach')
const sax = require('sax')

const decoder = require('html-entities')

class SaxParser {
  constructor(xmlString, model) {
    this.xmlString = xmlString
    this.fields = []
    this.fieldPos = {}
    this.model = model
    this.levels = {}

    this.init()
  }

  init() {
    this.schema = this.model.OverwriteSchema
      ? this.model.schema()
      : Object.assign(this.model.rootSchema(), this.model.schema() || {})

    this.fields = Object.keys(this.schema)

    this.model.model = {}

    this.fields.forEach(f => {
      this.model.model[f] =
        this.schema[f].defaultValue === undefined
          ? null
          : this.schema[f].defaultValue
    })

    this.sax = sax.parser(false, { position: true, lowercase: true })
    this.sax.ondoctype = this.onDocType
    this.sax.onopentag = this.onOpenTag
    this.sax.onclosetag = this.onCloseTag
    this.sax.onprocessinginstruction = this.onProcessInstruction

    this.promise = new Promise(resolve => {
      this.sax.onend = this.onEnd(resolve)

      this.sax.write(this.xmlString).close()
    })
  }

  get currentLevel() {
    let maxLevel = 0

    forEach(this.levels, (value, key) => {
      const level = this.levels[key].map(tag => tag.level)

      const ml = Math.max(...level)

      if (ml > maxLevel) {
        maxLevel = ml
      }
    })

    return maxLevel
  }

  isChildren() {
    return this.currentLevel <= 2
  }

  isRoot() {
    return this.currentLevel <= 1
  }

  isClosest(nd) {
    if (!this.fieldPos[nd]) return true
    return this.fieldPos[nd].every(field => field.level <= this.currentLevel)
  }

  isSameLevel(nd) {
    if (!this.fieldPos[nd]) return false
    return this.fieldPos[nd].every(field => field.level === this.currentLevel)
  }

  // eslint-disable-next-line class-methods-use-this
  checkNode(n, nd) {
    // const pass = false
    const [node, attribute] = nd.split(':')

    if (n.name === node && attribute === undefined) {
      return true
    }

    if (n.name === node && n.attributes[attribute]) {
      return true
    }

    return false
  }

  onDocType = n => {
    // Here we can add to the parser custom entities based on the doctype
    // like this this.sax.ENTITIES['iamcustomentity'] = '';
  }

  onProcessInstruction = n => {
    this.fields.forEach(field => {
      const {
        hasMultiple,
        path: { location },
      } = this.schema[field]

      const nodes = isArray(location) ? location : [location]

      nodes.forEach(nd => {
        if (nd === n.name) {
          const start = this.sax.startTagPosition - 1
          const end = this.sax.position

          this.fieldPos[nd] = [
            {
              start,
              end,
              value: this.xmlString.substring(start, end),
              key: field,
              level: this.currentLevel,
              hasMultiple: hasMultiple || false,
            },
          ]
        }
      })
    })
  }

  onOpenTag = n => {
    // opened a tag.  node has "name" and "attributes"

    if (this.levels[n.name] && this.levels[n.name].length > 0) {
      this.levels[n.name].push({ level: this.currentLevel + 1 })
    } else {
      this.levels[n.name] = [{ level: this.currentLevel + 1 }]
    }

    this.fields.forEach(field => {
      const {
        hasMultiple,
        path: { location, onlyInChildren = false, inRoot = false },
      } = this.schema[field]

      const nodes = isArray(location) ? location : [location]

      nodes.forEach(nd => {
        const [, attribute] = nd.split(':')

        if (this.checkNode(n, nd) && this.isClosest()) {
          if (onlyInChildren === true && inRoot === true)
            throw new Error(
              'You cannot have both onlyInChildren AND InRoot active',
            )

          let proccess =
            onlyInChildren === true && inRoot === false
              ? !!this.isChildren()
              : true

          proccess =
            onlyInChildren === false && inRoot === true
              ? !!this.isRoot()
              : proccess

          if (proccess) {
            const valid = isFunction(this.model.validate)
              ? this.model.validate(n)
              : true

            if (valid) {
              const value =
                attribute && n.attributes[attribute]
                  ? n.attributes[attribute]
                  : null

              // if (attribute === 'id' && n.name === 'sec') {
              //   console.log({ value })
              // }

              if (this.isSameLevel(nd)) {
                this.fieldPos[nd].push({
                  start: this.sax.startTagPosition - 1,
                  end: null,
                  value,
                  key: field,
                  level: this.currentLevel,
                  hasMultiple: hasMultiple || false,
                })
              } else {
                this.fieldPos[nd] = [
                  {
                    start: this.sax.startTagPosition - 1,
                    end: null,
                    value,
                    key: field,
                    level: this.currentLevel,
                    hasMultiple: hasMultiple || false,
                  },
                ]
              }
            }
          }
        }
      })
    })
  }

  onCloseTag = n => {
    // opened a tag.  node has "name" and "attributes"
    this.fields.forEach(field => {
      const {
        path: { location },
      } = this.schema[field]

      const nodes = isArray(location) ? location : [location]

      nodes.forEach(nd => {
        const [node] = nd.split(':')

        if (node === n && this.fieldPos[nd]) {
          this.fieldPos[nd] = this.fieldPos[nd].map(tag => {
            if (tag.level === this.currentLevel) {
              if (tag.end === null) {
                tag.end = this.sax.position

                tag.value = tag.value
                  ? tag.value
                  : this.xmlString.substring(tag.start, tag.end)
              }
            }

            return tag
          })
        }
      })
    })

    this.levels[n] = this.levels[n].filter(
      tag => tag.level !== this.currentLevel,
    )
  }

  onEnd = resolve => {
    return () => {
      const f = Object.keys(this.fieldPos)
      const fields = {}

      // eslint-disable-next-line no-plusplus
      for (let i = 0; i < f.length; i++) {
        if (this.fieldPos[f[i]]) {
          if (this.fieldPos[f[i]].length > 0) {
            const { key, hasMultiple, value } = this.fieldPos[f[i]][0]

            fields[key] = hasMultiple
              ? this.fieldPos[f[i]].map(field => {
                  return field.value
                })
              : value
          }
        }
      }

      resolve(fields)
    }
  }

  async getData() {
    const parserResponse = await this.promise

    const entries = Object.entries(parserResponse)

    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < entries.length; i++) {
      const [key, value] = entries[i]

      // eslint-disable-next-line no-await-in-loop
      await this.loadModel(key, value)
    }

    return Promise.resolve(this.model.model)
  }

  async loadModel(field, value) {
    let modelVal

    if (isFunction(this.model[field])) {
      modelVal = await this.model[field](value)
    } else if (value) {
      // Decode strings back from html-entities escaping
      // if they are leaf nodes, and not being passed into a method
      modelVal = decoder.decode(value)
    } else {
      modelVal = null
    }

    this.model.model[field] = modelVal
  }
}

module.exports = SaxParser
