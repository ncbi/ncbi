/* eslint-disable class-methods-use-this */
const XmlBase = require('./xmlBase')
const Metadata = require('./metadata')
const Body = require('./body')

const XmlFactory = require('./xmlFactory')

class BookPart extends XmlBase {
  schema() {
    return {
      body: {
        path: {
          location: `body`,
        },
        defaultValue: {},
      },
      metadata: {
        path: {
          location: `book-part-meta`,
        },
        defaultValue: {},
      },
    }
  }

  metadata(value) {
    if (!value) return {}
    return new Metadata(`<book-part-meta>${value}</book-part-meta>`)
  }

  body(value) {
    if (!value) return {}

    const typeFlag = !!(
      this.model.type === 'section' || this.model.type === 'chapter'
    )

    return XmlFactory.xmlToModel(
      value,
      Body,
      {
        parseSectionChildren: typeFlag,
      },
      { forceStream: true },
    )
  }

  namedBookPartBody(value) {
    if (!value) return {}

    return XmlFactory.xmlToModel(value, Body, {}, { forceStream: true })
  }

  hideInTOC(value) {
    return value.includes('hideTOC')
  }
}

module.exports = BookPart
