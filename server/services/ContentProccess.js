const { Book, BookComponent } = require('@pubsweet/models')

class ContentProccess {
  constructor(object) {
    this.object = object
    this.command = null
    this.model = null
    this.type = null
  }

  setObject(object) {
    if (!this.object) {
      this.object = object
    }
  }

  setModel(model) {
    this.model = model
  }

  setCommands(command) {
    this.command = command
  }

  async getBook(trx) {
    let book = null

    if (this.object.type === 'bookComponent') {
      book = await Book.query(trx).findOne({ id: this.object.bookId })
    } else {
      book = this.object
    }

    return book
  }

  static asWholeBook(book) {
    // eslint-disable-next-line global-require
    const { Domain } = require('./index')

    const contentProccess = new ContentProccess(book)

    contentProccess.setCommands(Domain)
    contentProccess.setModel(Book)
    contentProccess.type = 'wholeBook'
    return contentProccess
  }

  static asChapterProccessed(bookComponent) {
    // eslint-disable-next-line global-require
    const { Chapter } = require('./index')
    const contentProccess = new ContentProccess(bookComponent)

    contentProccess.setCommands(Chapter)
    contentProccess.setModel(BookComponent)
    contentProccess.type = 'chapterProccessed'
    return contentProccess
  }

  static async findBestContent(object, transaction) {
    // eslint-disable-next-line global-require
    let contentProccess = new ContentProccess()
    const regexExp = /^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/gi

    if (regexExp.test(object)) {
      let obj = await Book.query(transaction).findOne({ id: object })

      if (!obj) {
        obj = await BookComponent.query(transaction).findOne({ id: object })
        contentProccess = ContentProccess.asChapterProccessed(obj)
      } else if (obj.workflow === 'word') {
        const bookComponent = await BookComponent.query(transaction).findOne({
          bookId: obj.id,
        })

        contentProccess = ContentProccess.asChapterProccessed(bookComponent)
      } else {
        contentProccess = ContentProccess.asWholeBook(obj)
      }
    } else if (object.type === 'bookComponent') {
      contentProccess = ContentProccess.asChapterProccessed(object)
    } else if (object.type === 'book') {
      contentProccess = ContentProccess.asWholeBook(object)
    }

    return contentProccess
  }
}

module.exports = ContentProccess
