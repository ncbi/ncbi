const OrderModel = require('./orderModel')

class OrderService {
  // This updates model and saves the field that has been assigned automatically
  // if you need to return just the order version of the model without to be saved
  // it needs some changes
  static async model(
    model,
    { transaction, exclude, deep = false, predicatedModel = null },
  ) {
    const orderModel = new OrderModel(model, {
      transaction,
      exclude,
      predicatedModel,
    })

    const sortedModel = await orderModel.orderBy()

    if (deep) {
      await Promise.all(
        sortedModel.map(async m => {
          await OrderService.model(m, {
            transaction,
            exclude,
            deep,
            predicatedModel: orderModel.predicatedModel,
          })
        }),
      )
    }
  }
}

module.exports = OrderService
