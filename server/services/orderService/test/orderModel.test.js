/* eslint-disable no-shadow */
/* eslint-disable jest/no-disabled-tests */
// const { startServer } = require('pubsweet-server')

const { db } = require('@coko/server')

const { Book, BookComponent, Division } = require('@pubsweet/models')

const OrderModel = require('../orderModel')

const addSeedFactory = require('../../dbSeederFactory')

describe.each([Division, BookComponent])(
  'Order empty BookComponent array',
  parentType => {
    const seeder = addSeedFactory()

    afterEach(async () => {
      await db.raw('ROLLBACK')
    })

    beforeEach(async () => {
      await db.raw('BEGIN')
    })

    it('confirms that sorting empty array throws no error', async () => {
      let parentRecord
      const user = await seeder.insert('user', {})
      const book = await seeder.insert('book', { user, status: 'in-review' })
      const division = await seeder.insert('division', { book })

      if (parentType === Division) {
        parentRecord = await Division.query(db).findOne({ id: division.id })
      } else {
        const bookComponent = await seeder.insert('bookComponent', {
          book,
          division,
          user,
        })

        parentRecord = await BookComponent.query(db).findOne({
          id: bookComponent.id,
        })
      }

      const orderModel = new OrderModel(parentRecord, { transaction: db })
      const sortedComponents = await orderModel.orderBy()
      expect(sortedComponents).toEqual([])
    })
  },
)

describe.each([Division, BookComponent])(
  'Order populated BookComponent array',
  parentType => {
    const seeder = addSeedFactory()
    let bookComponents
    let division
    let parentRecord

    afterEach(async () => {
      bookComponents = null
      division = null
      parentRecord = null
      await db.raw('ROLLBACK')
    })

    beforeEach(async () => {
      await db.raw('BEGIN')

      const { user } = await seeder.insert('addAdmin', {
        password: '123456789',
        username: 'admin',
        email: 'admin@admin.com',
      })

      const obj = await seeder.insert('addBookMultiChapter', {
        user,
        book: {
          settings: {
            toc: { order_chapters_by: 'number' },
          },
        },
        bookComponents: [
          {
            title: 'Things and stuff',
            metadata: {
              chapter_number: 'Chapter five',
              date_publication: '2022-10-15',
              date_updated: '2023-3-1',
              date_created: '2022-10-1',
            },
          },
          {
            title: 'Just some title text',
            metadata: {
              chapter_number: 'Chapter 4',
              date_publication: '2023-8-10',
            },
          },
          { title: 'Untitled', metadata: { chapter_number: 'Chapter 2' } },
          {
            title: 'Yet another title',
            metadata: {
              chapter_number: 'Chapter 3',
              date_publication: null,
              date_updated: '2023-1-1',
              date_created: '2022-10-1',
            },
          },
          {
            title: 'A title',
            metadata: {
              chapter_number: 'Chapter 1',
              date_publication: null,
              date_updated: null,
              date_created: '2023-8-1',
            },
          },
          {
            title: 'Appendix A',
            metadata: {
              chapter_number: 'Appendix A',
              date_publication: null,
              date_updated: null,
              date_created: null,
            },
          },
        ],
        // the type for the parent of "bookComponents"
        parentType,
      })

      bookComponents = obj.bookComponents
      division = obj.division
      parentRecord = await parentType.query(db).findOne({ id: obj.parent.id })
    })

    it('confirms that test data is initially unsorted', async () => {
      // Confirm that book components are unsorted
      expect(parentRecord.bookComponents).toEqual(
        bookComponents.map(bc => bc.id),
      )

      expect(bookComponents.map(bc => bc.metadata.chapter_number)).toEqual([
        'Chapter five',
        'Chapter 4',
        'Chapter 2',
        'Chapter 3',
        'Chapter 1',
        'Appendix A',
      ])
    })

    // eslint-disable-next-line jest/expect-expect
    it('sorts BookComponents in ascending numerical order', async () => {
      // Sort the bookComponents using OrderModel
      const orderModel = new OrderModel(parentRecord, { transaction: db })
      const sortedComponents = await orderModel.orderBy()

      expect(sortedComponents.map(bc => bc.id)).toEqual([
        bookComponents[0].id, // Chapter five
        bookComponents[5].id, // Appendix A
        bookComponents[4].id, // Chapter 1
        bookComponents[2].id, // Chapter 2
        bookComponents[3].id, // Chapter 3
        bookComponents[1].id, // Chapter 4
      ])

      // Check that the sorting has been saved to the database
      parentRecord = await parentType.query(db).findOne({ id: parentRecord.id })

      expect(parentRecord.bookComponents).toEqual(
        sortedComponents.map(bc => bc.id),
      )
    })

    // eslint-disable-next-line jest/expect-expect
    it('sorts BookComponents in descending numerical order', async () => {
      await Book.query(db)
        .findById(division.bookId)
        .patch({ settings: { toc: { order_chapters_by: 'number_desc' } } })

      // Sort the bookComponents using OrderModel
      const orderModel = new OrderModel(parentRecord, { transaction: db })
      const sortedComponents = await orderModel.orderBy()

      expect(sortedComponents.map(bc => bc.id)).toEqual([
        bookComponents[0].id, // Chapter five
        bookComponents[5].id, // Appendix A
        bookComponents[1].id, // Chapter 4
        bookComponents[3].id, // Chapter 3
        bookComponents[2].id, // Chapter 2
        bookComponents[4].id, // Chapter 1
      ])

      // Check that the sorting has been saved to the database
      parentRecord = await parentType.query(db).findOne({ id: parentRecord.id })

      expect(parentRecord.bookComponents).toEqual(
        sortedComponents.map(bc => bc.id),
      )
    })

    // eslint-disable-next-line jest/expect-expect
    it('sorts BookComponents in descending date order', async () => {
      await Book.query(db)
        .findById(division.bookId)
        .patch({ settings: { toc: { order_chapters_by: 'date_desc' } } })

      // Sort the bookComponents using OrderModel
      const orderModel = new OrderModel(parentRecord, { transaction: db })
      const sortedComponents = await orderModel.orderBy()

      expect(sortedComponents.map(bc => bc.id)).toEqual([
        bookComponents[2].id, // null ('Chapter 2')
        bookComponents[5].id, // null ('Appendix A')
        bookComponents[1].id, // 2023-8-10 ('Chapter 4')
        bookComponents[4].id, // 2023-8-1 ('Chapter 1')
        bookComponents[3].id, // 2023-1-1 ('Chapter 3')
        bookComponents[0].id, // 2022-10-15 ('Chapter five')
      ])

      // Check that the sorting has been saved to the database
      parentRecord = await parentType.query(db).findOne({ id: parentRecord.id })

      expect(parentRecord.bookComponents).toEqual(
        sortedComponents.map(bc => bc.id),
      )
    })

    // eslint-disable-next-line jest/expect-expect
    it('sorts BookComponents in ascending title order', async () => {
      await Book.query(db)
        .findById(division.bookId)
        .patch({ settings: { toc: { order_chapters_by: 'title' } } })

      // Sort the bookComponents using OrderModel
      const orderModel = new OrderModel(parentRecord, { transaction: db })
      const sortedComponents = await orderModel.orderBy()

      expect(sortedComponents.map(bc => bc.id)).toEqual([
        bookComponents[2].id, // null ('Untitled')
        bookComponents[4].id, // 'A title'
        bookComponents[5].id, // 'Appendix A'
        bookComponents[1].id, // 'Just some title text'
        bookComponents[0].id, // 'Things and stuff'
        bookComponents[3].id, // 'Yet another title'
      ])

      // Check that the sorting has been saved to the database
      parentRecord = await parentType.query(db).findOne({ id: parentRecord.id })

      expect(parentRecord.bookComponents).toEqual(
        sortedComponents.map(bc => bc.id),
      )
    })
  },
)
