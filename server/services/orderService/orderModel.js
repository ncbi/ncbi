/* eslint-disable no-await-in-loop */
const _ = require('lodash')

/**
 * Configure a model's parent and child models and columns. This information is
 * required by OrderModel so that it can sort model rows.
 * @typedef {Object} OrderModel~orderMapping
 * @property {orderMapping~field} field
 *   Maps selected rows to their child model(s) and row(s).
 * @property {orderMapping~predicate} predicate
 *   Maps selected rows to their parent model and row.
 */

/**
 * @typedef {Object} orderMapping~field
 * @property {String} idColumn:
 *   The name of a selected row's primary key column.
 * @property {String} column:
 *   The name of the column containing a selected row's child array.
 *   Array items are "field.foreignColumn" values pointing to rows of the
 *   child model.
 * @property {String} foreignColumn:
 *   The name of the child column mapping "column" to a row's children.
 * @property {String|Array} modelClass:
 *   The child model(s) which map to the children of a selected row.
 */

/**
 * @typedef {Object} orderMapping~predicate
 * @property {String} idColumn:
 *   The name of the primary key column of the parent model.
 * @property {String} foreignColumn:
 *   The column name which maps a selected row to it's parent row.
 * @property {String} modelClass:
 *   The parent model of each selected row
 * @property {function} field:
 *   A callback which accepts a parent row as its only argument and returns
 *   the selected row as a sortable object which can be passed to "OrderModel".
 *   @returns {orderMapping~sortable}
 */

/**
 * @typedef {orderMapping~sortable}
 * @property {String|Function} field
 *   The name of the "order by column" or a callback function which accepts the
 *   row being sorted and returns the name of the best available sort field for
 *   that row.
 * @property {('Number'|'Date'|'String')} type
 *   The type the "field".
 * @property {('asc'|'desc')} order
 *   Sort in ascending or descending order of "value".
 * @property {Function} value
 *   A callback which coerces the value of "field" to the value to be used for
 *   sorting.
 */

/**
 * Configure the behaviour and context for the "OrderModel" instance.
 * @typedef {Object} OrderModel~options
 * @property (Object) transaction
 *   A database transaction object.
 * @property exclude
 * @property {Boolean} deep
 *   Whether or not to perform a deep sort.
 * @property {BaseModel} predicatedModel
 *   Optionally override OrderModel~orderMappings.predicate.modelClass.
 */

/**
 * Implements common sorting logic for models which have the static
 * "orderMappings" property/getter.
 */
class OrderModel {
  /**
   * @param {BaseModel} entity
   *   The model to be sorted.
   * @param {OrderModel~orderMapping} entity.constructor.orderMappings
   *   Defines parent-child mappings for sorting "entity" rows
   * @param {OrderModel~options} options
   *   An object containing configuration and context for this instance.
   */
  constructor(entity, options) {
    this.options = options
    this.entity = entity
    this.predicatedModel = options.predicatedModel || null
    this.transaction = options.transaction

    // add Check if Model doesnt have orderMapping
    this.orderMappings = this.entity.constructor.orderMappings || null

    this.fieldArray =
      this.options.field ||
      ((this.orderMappings || {}).field || {}).column ||
      null
  }

  async getItemsToCompare() {
    if (Array.isArray(this.orderMappings.field.modelClass)) {
      const arrayItems = await Promise.all(
        this.orderMappings.field.modelClass.map(
          async modelClass =>
            // eslint-disable-next-line no-return-await
            await modelClass
              .query(this.transaction)
              .whereIn(
                this.orderMappings.field.foreignColumn,
                this.entity[this.fieldArray],
              ),
        ),
      )

      return _.flattenDeep(arrayItems)
    }

    // eslint-disable-next-line no-return-await
    return await this.orderMappings.field.modelClass
      .query(this.transaction)
      .whereIn(
        this.orderMappings.field.foreignColumn,
        this.entity[this.fieldArray],
      )
  }

  async predicateField() {
    this.predicatedModel =
      this.predicatedModel ||
      (await this.orderMappings.predicate.modelClass
        .query(this.transaction)
        .findOne({
          [this.orderMappings.predicate.idColumn]: this.entity[
            this.orderMappings.predicate.foreignColumn
          ],
        }))

    const predicate = this.orderMappings.predicate.field(this.predicatedModel)

    const field = predicate.field === '' ? 'created' : predicate.field

    const type = predicate.field === '' ? 'Date' : predicate.type
    return {
      field,
      type,
      value: predicate.value || null,
      order: predicate.order || 'desc',
    }
  }

  keepExcludePositions = item => {
    const mapper = {}

    item.forEach(itm => {
      if (this.options.exclude(itm)) mapper[itm.id] = item.indexOf(itm)
    })

    return mapper
  }

  // sort by whatever - if same, sort by reverse chronological
  sortByPredicateThenCreated = (
    data,
    { field, type, value, order = 'desc' },
  ) => {
    // If sort(a, b) returns a value > than 0, sort b before a.
    // If sort(a, b) returns a value < than 0, sort a before b.
    return data.sort((a, b) => {
      let alpha
      let bhta
      let diff = 0

      if (_.isFunction(field)) {
        alpha = field(a)
        bhta = field(b)
      } else {
        alpha = _.get(a, field)
        bhta = _.get(b, field)
      }

      if (value) {
        alpha = value(alpha)
        bhta = value(bhta)
      }

      // Bring all "unsortable" rows to the top of the array
      if (!alpha) {
        diff = bhta ? -1 : 0
      } else if (!bhta) {
        diff = alpha ? 1 : 0
      } else {
        // Both "a" and "b" are sortable
        if (type === 'Number') {
          diff = alpha - bhta
        } else if (type === 'Date') {
          // diff = alpha.localeCompare(bhta)

          const diffA = new Date(alpha).getTime()

          const diffB = new Date(bhta).getTime()

          diff = diffA - diffB
        } else if (type === 'String') {
          alpha = alpha.replace(/<[^>]*>?/gm, '').toUpperCase()

          bhta = bhta.replace(/<[^>]*>?/gm, '').toUpperCase()

          diff = alpha.localeCompare(bhta)
        }

        // Reverse sort order for "desc"
        if (diff && order === 'desc') {
          diff *= -1
        }
      }

      return diff
    })
  }

  async sortOrOrder(data, predicate) {
    let listWithoutExcluded = data

    if (this.options.exclude) {
      listWithoutExcluded = _.reject(data, this.options.exclude)
    }

    const sortedList = this.sortByPredicateThenCreated(
      listWithoutExcluded,
      predicate,
    )

    if (this.options.exclude) {
      const excludeItemPositions = this.keepExcludePositions(data)

      if (_.keys(excludeItemPositions).length > 0) {
        // eslint-disable-next-line no-plusplus
        for (let i = 0; i < _.keys(excludeItemPositions).length; i++) {
          const itemPosition =
            excludeItemPositions[_.keys(excludeItemPositions)[i]]

          const item = data.find(d => d.id === _.keys(excludeItemPositions)[i])

          sortedList.splice(itemPosition, 0, item)
        }
      }
    }

    return sortedList
  }

  async orderBy() {
    if (!this.orderMappings) return []
    const predicate = await this.predicateField()

    let items = await this.getItemsToCompare()

    const sorderdBC = this.entity[this.fieldArray]

    items.sort((x, y) => {
      const xkey = sorderdBC.indexOf(x.id)
      const ykey = sorderdBC.indexOf(y.id)
      return xkey - ykey
    })

    if (predicate.field !== 'manual')
      items = await this.sortOrOrder(items, predicate)

    const updatedField = items.map(
      itm => itm[this.orderMappings.field.foreignColumn],
    )

    if (_.isEqual(this.entity[this.fieldArray], updatedField)) return []

    // eslint-disable-next-line no-return-await
    await this.entity.constructor
      .query(this.transaction)
      .patch({
        [this.fieldArray]: updatedField,
      })
      .findOne({
        [this.orderMappings.field.idColumn]: this.entity[
          this.orderMappings.field.idColumn
        ],
      })

    return items
  }
}

module.exports = OrderModel
