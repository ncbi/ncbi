/* eslint-disable no-useless-escape */
/* eslint-disable no-param-reassign */
import { useDropzone } from 'react-dropzone'
import { useEffect, useReducer, createRef } from 'react'
import { useSubscription } from '@apollo/client'
import query from '../ui/Pages/Bookmanager/graphql'

const reducer = (state, action) => {
  switch (action.type) {
    case 'remove': {
      return {
        files: [
          ...state.files.filter(file => file.name !== action.payload.name),
        ],
      }
    }

    case 'removeConverted': {
      return {
        files: [
          ...(state?.files || []).filter(file => file.status !== 'converted'),
        ],
      }
    }

    case 'add': {
      const stateFiles = state.files || []
      const validatedFiles = action.payload.validationFn(action.payload.files)

      return {
        files: [...stateFiles, ...validatedFiles],
      }
    }

    case 'replace':
      return {
        files: [
          ...state?.files.map(file => {
            if (file.name === action.payload.name) {
              file = Object.assign(
                action.payload.files[0],
                { errors: [], progress: 0, status: 'readyToUpload' },
                { refItem: createRef(file.name) },
              )

              file = action.payload.validationFn([file])
            }

            return file
          }),
        ],
      }

    case 'update': {
      return {
        files: [
          ...(state?.files || []).map(file => {
            if (action.payload.name) {
              if (file.name === action.payload.name) {
                file = Object.assign(file, action.payload.update)
              }
            } else {
              file = Object.assign(file, action.payload.update)
            }

            return file
          }),
        ],
      }
    }

    case 'reset':
      return { files: [] }
    default:
      return state
  }
}

export default ({
  validationFn = files => {
    return files
  },
}) => {
  const { acceptedFiles, getRootProps, getInputProps, open } = useDropzone()
  const { data } = useSubscription(query.UPLOAD_PROGRESS)

  const [state, dispatch] = useReducer(reducer, [])

  useEffect(() => {
    const filesArray = []

    acceptedFiles.forEach(file => {
      filesArray.push(
        Object.assign(
          file,
          { errors: [], progress: 0, status: 'readyToUpload' },
          { refItem: createRef(file.name) },
        ),
      )
    })

    if (filesArray.length > 0) {
      if (!getInputProps()?.ref.current.multiple) {
        dispatch({
          type: 'reset',
        })
      }

      // we don't allow adding same file multiple times
      const uniquefilesArray = filesArray.filter(
        x => (state?.files || []).findIndex(y => y.name === x.name) < 0,
      )

      dispatch({
        payload: {
          files: uniquefilesArray,
          // eslint-disable-next-line no-shadow
          validationFn: filesArray => validationFn(filesArray),
        },
        type: 'add',
      })
    }
  }, [acceptedFiles])

  useEffect(() => {
    if (data && data.uploadConvertFileProgress) {
      dispatch({
        payload: {
          name: data.uploadConvertFileProgress.file,
          update: {
            progress: data.uploadConvertFileProgress.percentage,
            status: data.uploadConvertFileProgress.status,
          },
        },
        type: 'update',
      })
    }
  }, [data])

  const currentFile =
    state?.files?.find(file => {
      if (!data) return false
      return file.name === data.uploadConvertFileProgress.file
    }) || {}

  return {
    currentFile,
    dispatch,
    fileList: state.files || [],
    getInputProps,
    getRootProps,
    isValid: state?.files?.every(file => file.status !== 'inValid'),
    open,
  }
}
