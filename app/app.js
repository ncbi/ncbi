import { merge } from 'lodash'
import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import { hot } from 'react-hot-loader'
import { createBrowserHistory } from 'history'

import { Root } from '@pubsweet/client'

import theme from './theme'
import routes from './routes'

const history = createBrowserHistory()
const rootEl = document.getElementById('root')

const makeApolloConfig = config => {
  // Common logic for merging graphql properties which h
  const mergeConflicts = (existing, incoming, { mergeObjects }) => {
    return mergeObjects(existing, incoming)
  }

  merge(config.cache.policies.typePolicies, {
    Book: {
      fields: {
        // NB: docs suggest { merge: true } but this throws an error
        metadata: { merge: mergeConflicts },
        settings: { merge: mergeConflicts },
      },
    },
    BookComponent: { fields: { metadata: { merge: mergeConflicts } } },
    BookSettingsTemplate: { fields: { values: { merge: mergeConflicts } } },
    Collection: { fields: { metadata: { merge: mergeConflicts } } },
    Organisation: { fields: { settings: { merge: mergeConflicts } } },
    Toc: { fields: { metadata: { merge: mergeConflicts } } },
  })

  return config
}

ReactDOM.render(
  <Root
    history={history}
    makeApolloConfig={makeApolloConfig}
    routes={routes}
    theme={theme}
  />,
  rootEl,
)

export default hot(module)(Root)
