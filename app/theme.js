import { merge } from 'lodash'
import '@fontsource/source-sans-pro'

import { theme as cokoTheme } from '@coko/client'

import {
  Accordion,
  Action,
  Link,
  // Menu,
  Steps,
  Tab,
  TabContainer,
  Tabs,
  TextArea,
  TextField,
  WaxLinkContainer,
  WaxLinkInput,
  WaxLinkCreate,
  WaxLinkCancel,
} from './themeElements'

const edits = {
  fontSizeBaseVerySmall: '12px',
  fontInterface: 'Source Sans Pro',
  lineHeightBase: '24px',

  borderRadius: '2px',

  menuItemHeight: '32px',

  buttonBorderColor: '#ffffff',
  bottomBorderColor: '#C4C4C4',

  navColor: '#eeeeee',

  colorBackgroundHue: '#F7F7F7',
  colorApprove: '#578000',
  colorBorder: '#aeb0b5',
  colorBlue: '#0071BC',
  colorDisabled: '#BDBDBD',
  colorErrorLight: '#B40422',
  colorInnerBorder: '#D9D9DA',
  colorPrimary: '#205493',
  colorPrimaryLighter: '#EAF1FA',
  colorPrimaryDark: '#212121',
  colorPrimaryDarker: '#205493',
  colorRedPrimary: '#E31C3D',
  colorSecondary: '#5792D8',
  colorSuccess: '#2E8540',
  colorText: '#212121',
  colorTextPlaceholder: '#E7E7E7',
  colorSubMenu: '#828282',
  // colors for statuses
  colorRevise: '#FDB81E',
  colorReview: '#5792D8',
  colorError: '#E31C3D',
  colorWarning: '#FDB81E',
  colorArchive: '#BEBEBE',
  // -end- colors for statuses

  cssOverrides: {
    ui: {
      Accordion,
      Action,
      Link,
      // Menu,
      Steps,
      Tab,
      TabContainer,
      Tabs,
      TextArea,
      TextField,
    },
    Wax: {
      LinkContainer: WaxLinkContainer,
      LinkInput: WaxLinkInput,
      LinkCreate: WaxLinkCreate,
      LinkCancel: WaxLinkCancel,
    },
  },

  Modal: {
    defaultStyles: {
      overlay: {
        backgroundColor: 'rgba(0,0,0,0.8)',
      },
    },
  },
}

const editedTheme = merge(cokoTheme, edits)

export default editedTheme
