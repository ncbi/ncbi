import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import { PageLayout } from '@coko/client'

import App from './components/App'
import PrivateRoute from './components/PrivateRoute'

import {
  Signup,
  SignUpNcbi,
  Login,
  LoginNcbi,
  SysAdminPage,
  JobsPanel,
} from '../ui/Pages'

import {
  BookComponentManager,
  BookManager,
  CollectionManager,
  CreateBook,
  CreateCollection,
  Navigation,
  Organization,
  Organizations,
  Toc,
} from './pages'

const routes = (
  <App>
    <PageLayout navComponent={Navigation} padPages={false}>
      <Switch>
        <Route component={Signup} exact path="/signup" />
        <Route component={SignUpNcbi} exact path="/signUpNcbi" />
        <Route component={LoginNcbi} path="/loginNcbi" />
        <Route component={Login} path="/login" />

        <PrivateRoute
          component={() => <Redirect to="/organizations" />}
          exact
          path="/"
        />

        <PrivateRoute component={Organizations} exact path="/organizations" />
        <PrivateRoute component={SysAdminPage} exact path="/dashboard" />
        <PrivateRoute component={JobsPanel} exact path="/jobs" />

        <PrivateRoute
          component={Organization}
          exact
          path="/organizations/:organizationId"
        />

        <PrivateRoute
          component={CreateBook}
          exact
          path="/organizations/:organizationId/createBook"
        />

        <PrivateRoute
          component={CreateCollection}
          exact
          path="/organizations/:organizationId/createCollection"
        />

        <PrivateRoute
          component={BookManager}
          exact
          path="/organizations/:organizationId/bookmanager/:bookId"
        />

        <PrivateRoute
          component={BookComponentManager}
          exact
          path="/organizations/:organizationId/bookmanager/:bookId/:bookComponentId"
        />
        <PrivateRoute
          component={Toc}
          exact
          path="/organizations/:organizationId/bookmanager/:bookId/toc/:id"
        />

        <PrivateRoute
          component={CollectionManager}
          exact
          path="/organizations/:organizationId/collectionmanager/:collectionId"
        />

        <PrivateRoute
          component={Toc}
          exact
          path="/organizations/:organizationId/collectionmanager/:collectionId/toc/:id"
        />

        <PrivateRoute
          component={BookManager}
          exact
          path="/organizations/:organizationId/collectionmanager/:collectionId/bookmanager/:bookId/"
        />

        <Route
          component={({ location }) => (
            <div>
              <h3>
                No match for <code>{location.pathname}</code>
              </h3>
            </div>
          )}
        />
      </Switch>
    </PageLayout>
  </App>
)

export default routes
