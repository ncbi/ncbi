import { gql } from '@apollo/client'

const CURRENT_USER = gql`
  {
    currentUser {
      id
      username
      givenName
      surname
      auth {
        isSysAdmin
        isPDF2XMLVendor

        sysAdminForOrganisation
        orgAdminForOrganisation
        editorForOrganisation
        awardeeForOrganisation
        userForOrganisation

        editorForBook
        authorForBook
        previewerForBook

        authorForBookComponent
        editorForBookComponent
        previewerForBookComponent
      }
      organisations {
        id
        name
        settings {
          type {
            publisher
            funder
          }
        }
        teams {
          id
          role
          name
          members {
            id
            status
            user {
              id
            }
          }
        }
      }
    }
  }
`

/* eslint-disable-next-line import/prefer-default-export */
export { CURRENT_USER }
