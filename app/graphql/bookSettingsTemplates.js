import { gql } from '@apollo/client'

const BOOK_SETTINGS_TEMPLATE_FIELDS = gql`
  fragment BookSettingsTemplateFields on BookSettingsTemplate {
    id
    isCustom
    templateType

    domainId
    domainName

    values {
      metadata {
        openAccess
      }

      settings {
        approvalOrgAdminEditor
        approvalPreviewer
        bookLevelAffiliationStyle
        bookLevelLinks
        bookLevelLinksMarkdown
        chapterLevelAffiliationStyle
        citationSelfUrl
        citationType
        createPdf
        createVersionLink
        createWholebookPdf
        displayObjectsLocation
        footnotesDecimal
        indexChaptersInPubmed
        indexInPubmed
        multiplePublishedVersions
        publisherUrl
        questionAnswerStyle
        referenceListStyle
        specialPublisherLinkText
        specialPublisherLinkUrl
        toc {
          add_body_to_parts
          allTitle
          contributors
          documentHistory
          group_chapters_in_parts
          order_chapters_by
          subtitle
          tocMaxDepth
        }
        tocExpansionLevel
        UKPMC
        versionLinkText
        versionLinkUri
        xrefAnchorStyle
      }

      # qaStatus
      # releaseStatus
    }
  }
`

const CREATE_BOOK_SETTINGS_TEMPLATE = gql`
  ${BOOK_SETTINGS_TEMPLATE_FIELDS}

  mutation CreateBookSettingsTemplate(
    $input: CreateBookSettingsTemplateInput!
  ) {
    createBookSettingsTemplate(input: $input) {
      ...BookSettingsTemplateFields
    }
  }
`

const UPDATE_BOOK_SETTINGS_TEMPLATE = gql`
  ${BOOK_SETTINGS_TEMPLATE_FIELDS}

  mutation UpdateBookSettingsTemplate(
    $input: UpdateBookSettingsTemplateInput!
  ) {
    updateBookSettingsTemplate(input: $input) {
      ...BookSettingsTemplateFields
    }
  }
`

export {
  BOOK_SETTINGS_TEMPLATE_FIELDS,
  CREATE_BOOK_SETTINGS_TEMPLATE,
  UPDATE_BOOK_SETTINGS_TEMPLATE,
}
