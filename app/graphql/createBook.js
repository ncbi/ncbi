import { gql } from '@apollo/client'

const GET_ORGANIZATION = gql`
  query GetOrganisation($id: ID!) {
    getOrganisation(id: $id) {
      id
      name
      collections {
        id
        title
      }
      publisherOptions {
        id
        name
      }
      settings {
        pdf
        word
        xml
        collections {
          funded
        }
        type {
          publisher
          funder
        }
      }
    }
  }
`

const CREATE_BOOK = gql`
  mutation CreateBook($input: CreateBookInput!) {
    createBook(input: $input) {
      id
      collection {
        id
        collectionType
      }
      organisation {
        id
        name
      }
      settings {
        approvalOrgAdminEditor
        chapterIndependently
        citationType
        multiplePublishedVersions
        toc {
          add_body_to_parts
          contributors
          documentHistory
          group_chapters_in_parts
          order_chapters_by
          tocMaxDepth
        }
      }
      status
      title
      updated
      workflow
      version
    }
  }
`

const UPDATE_BOOK = gql`
  mutation($id: ID!, $input: UpdateInput) {
    updateBook(id: $id, input: $input) {
      id
      workflow
      settings {
        chapterIndependently
      }
      bookComponents {
        id
        bookComponentVersionId
      }
    }
  }
`

const REFETCH_COLLECTION_ON_BOOK_CREATION = gql`
  query RefetchCollectionOnBookCreation($id: ID!) {
    getCollection(id: $id) {
      id
      books {
        id
      }
    }
  }
`

const GET_FUNDED_COLLECTIONS_OF_OTHER_ORGANIZATIONS = gql`
  query GetFundedCollectionsOfOtherOrganizations($organisationId: ID!) {
    getFundedCollectionsOfOtherOrganisations(organisationId: $organisationId) {
      id
      collectionType
      title
      organisation {
        id
        name
      }
    }
  }
`

export {
  CREATE_BOOK,
  GET_ORGANIZATION,
  GET_FUNDED_COLLECTIONS_OF_OTHER_ORGANIZATIONS,
  REFETCH_COLLECTION_ON_BOOK_CREATION,
  UPDATE_BOOK,
}
