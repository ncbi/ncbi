import { gql } from '@apollo/client'

import {
  BOOK_SETTINGS_TEMPLATE_FIELDS,
  CREATE_BOOK_SETTINGS_TEMPLATE,
  UPDATE_BOOK_SETTINGS_TEMPLATE,
} from './bookSettingsTemplates'

const COLLECTION_FIELDS = gql`
  fragment CollectionFields on Collection {
    abstract
    abstractTitle
    collectionType
    components
    customGroups {
      books {
        workflow
        bookComponents {
          id
          bookComponentVersionId
          status
          sourceFiles {
            id
            created
            updated
            name
            source
            mimeType
            versionName
            status
            bcfId
            parentId
            copied
            tag
            owner {
              id
              username
              givenName
              surname
            }
          }
          convertedFile {
            id
            created
            updated
            name
            source
            mimeType
            versionName
            status
            bcfId
            parentId
            copied
            tag
            owner {
              id
              username
              givenName
              surname
            }
          }

          metadata {
            collaborativeAuthors
            date_publication
            chapter_number
            book_component_id
            language
            alt_title
            sub_title
            date_created
            date_updated
            date_revised
            section_titles
            filename
            url
          }
        }
        edition
        id
        publishedDate
        metadata {
          volume
          pubDate {
            date {
              day
              month
              year
            }
            dateRange {
              startYear
              startMonth
              endYear
              endMonth
            }
            dateType
            publicationFormat
          }
        }
        organisation {
          name
          id
        }
        settings {
          chapterIndependently
        }
        status
        sourceFiles {
          id
          created
          updated
          name
          source
          mimeType
          versionName
          status
          bcfId
          parentId
          copied
          tag
          owner {
            id
            username
            givenName
            surname
          }
        }
        convertedFile {
          id
          created
          updated
          name
          source
          mimeType
          versionName
          status
          bcfId
          parentId
          copied
          tag
          owner {
            id
            username
            givenName
            surname
          }
        }
        title
        subTitle
        fundedContentType
        version
        updated
        workflow
      }
      collectionType
      components
      id
      settings {
        isCollectionGroup
      }
      title
    }
    domain
    fileCover {
      id
      name
    }
    id
    metadata {
      applyCoverToBooks
      applyPermissionsToBooks
      applyPublisherToBooks
      author {
        givenName
      }
      chapterProcessedSourceType
      copyrightHolder
      copyrightStatement
      copyrightYear
      editor {
        givenName
        surname
        role
        degrees
      }
      grants {
        id
        apply
        country
        institution_acronym
        institution_code
        institution_name
        number
      }
      issn
      eissn
      licenseStatement
      licenseType
      nlmId
      notes {
        applyNotePdf
        description
        title
        type
      }
      openAccess
      openAccessLicense
      otherLicenseType
      pub_loc
      pub_name

      pubDate {
        dateRange {
          startYear
          startMonth
          endYear
          endMonth
        }
        dateType
        publicationFormat
      }
      publisherBookSeriesTitle
      sourceType
      wholeBookSourceType
    }
    toc {
      id
      status
    }
    organisation {
      name
      id
      settings {
        collections {
          bookSeries
          funded
        }
      }
    }
    settings {
      approvalOrgAdminEditor
      citationSelfUrl
      groupBooks
      groupBooksBy
      isCollectionGroup
      orderBooksBy
      publisherUrl
      toc {
        contributors
        fullBookCitations
        publisher
      }
      UKPMC
    }
    title
    updated
    workflow
    books {
      id
      title
      subTitle
      fundedContentType
      version
      organisation {
        name
        id
      }
      publishedDate
      edition
      status
      workflow
      updated
      settings {
        chapterIndependently
      }
      bookComponents {
        id
        bookComponentVersionId
        status
        sourceFiles {
          id
          created
          updated
          name
          source
          mimeType
          versionName
          status
          bcfId
          parentId
          copied
          tag
          owner {
            id
            username
            givenName
            surname
          }
        }
        convertedFile {
          id
          created
          updated
          name
          source
          mimeType
          versionName
          status
          bcfId
          parentId
          copied
          tag
          owner {
            id
            username
            givenName
            surname
          }
        }

        metadata {
          collaborativeAuthors
          date_publication
          chapter_number
          book_component_id
          language
          alt_title
          sub_title
          date_created
          date_updated
          date_revised
          section_titles
          filename
          url
        }
        issues {
          id
          status
          title
        }
      }
      sourceFiles {
        id
        created
        updated
        name
        source
        mimeType
        versionName
        status
        bcfId
        parentId
        copied
        tag
        owner {
          id
          username
          givenName
          surname
        }
      }
      convertedFile {
        id
        created
        updated
        name
        source
        mimeType
        versionName
        status
        bcfId
        parentId
        copied
        tag
        owner {
          id
          username
          givenName
          surname
        }
      }
      metadata {
        url
        volume
        pubDate {
          dateType
          publicationFormat
          date {
            day
            month
            year
          }
          dateRange {
            startYear
            startMonth
            endYear
            endMonth
          }
        }
      }
    }
  }
`

const GET_COLLECTION = gql`
  ${COLLECTION_FIELDS}

  query getCollection($id: ID!) {
    getCollection(id: $id) {
      ...CollectionFields
    }
  }
`

const UPDATE_COLLECTION = gql`
  ${COLLECTION_FIELDS}

  mutation($id: ID!, $input: UpdateCollectionInput) {
    updateCollection(id: $id, input: $input) {
      ...CollectionFields
    }
  }
`

const UPDATE_CUSTOM_GROUP_ORDER = gql`
  ${COLLECTION_FIELDS}

  mutation(
    $parentCollectionId: ID!
    $collectionId: ID!
    $ids: [ID]
    $index: Int
  ) {
    updateCollectionOrder(
      parentCollectionId: $parentCollectionId
      collectionId: $collectionId
      ids: $ids
      index: $index
    ) {
      ...CollectionFields
    }
  }
`

const COLLECTION_TEMPLATES = gql`
  ${BOOK_SETTINGS_TEMPLATE_FIELDS}

  query CollectionTemplates($organizationId: ID!, $collectionId: ID!) {
    bookSettingsTemplates(
      organizationId: $organizationId
      collectionId: $collectionId
    ) {
      ...BookSettingsTemplateFields
    }
  }
`

const SUBMIT_BOOK_COMPONENTS = gql`
  mutation submitBookComponents($bookComponents: [ID]) {
    submitBookComponents(bookComponents: $bookComponents)
  }
`

const BOOK_COMPONENT_PUBLISH = gql`
  mutation publishBookComponents($bookComponent: [ID!]) {
    publishBookComponents(bookComponent: $bookComponent) {
      id
      status
    }
  }
`

const RECONVERT_FILE = gql`
  mutation reconvertBookComponent($ids: [ID!]!) {
    reconvertBookComponent(ids: $ids) {
      id
      status
    }
  }
`

export {
  GET_COLLECTION,
  COLLECTION_TEMPLATES,
  CREATE_BOOK_SETTINGS_TEMPLATE,
  UPDATE_BOOK_SETTINGS_TEMPLATE,
  UPDATE_COLLECTION,
  UPDATE_CUSTOM_GROUP_ORDER,
  SUBMIT_BOOK_COMPONENTS,
  BOOK_COMPONENT_PUBLISH,
  RECONVERT_FILE,
}
