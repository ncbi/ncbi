import { gql } from '@apollo/client'

const toc = `
      id
      updated
      versionName
      status
      publishedDate
      canBePublished
      tocFiles {
        id
        status
        id
        created
        updated
      }
      alias
      metadata {
        url
      }
      errors {
        id
        noticeTypeName
        severity
        assignee
      }
      collection {
        id
        status
        books {
          id
          status
        }
      }
      book {
        id
        bookComponents {
          id
          status
        }
      }
`

const tocBookComponent = `
id
bookId
componentType
title
updated
created
publishedDate
status
alias
metadata {
  url
}
publishedVersions {
  id
  publishedDate
  status
  versionName
}
errors {
  id
  noticeTypeName
  severity
  assignee
  message
  objectId
}
convertedFile {
  id
  status
  id
  created
  updated
  name
}
`

const GET_TOC_BOOK_COMPONENT = gql`
  query getBookComponent($id: ID!) {
    getBookComponent(id: $id) {
   ${tocBookComponent}
    }
  }
`

const GET_TOC = gql`
  query getToc($id: ID!) {
    getToc(id: $id) {
      ${toc}
    }
  }
`

const GET_BOOK_COMPONENT_STATUSES = gql`
  query getBookStatuses($id: ID!, $divisionLabels: [String!]) {
    getBook(id: $id) {
      id
      bookComponents(divisionLabels: $divisionLabels) {
        id
        status
      }
    }
  }
`

const BOOK_COMPONENT_PUBLISH = gql`
  mutation publishBookComponents($bookComponent: [ID!]) {
    publishBookComponents(bookComponent: $bookComponent) {
      id
      status
    }
  }
`

const TOC_PUBLISH = gql`
mutation publishTocs(
    $toc: [ID!]
  ) {
    publishTocs(toc: $toc) {
      ${toc}
    }
  }
`

const GET_BOOK = gql`
  query getBook($id: ID!, $divisionLabels: [String!]) {
    getBook(id: $id) {
      id
      title
      abstract
      abstractTitle
      subTitle
      altTitle
      edition
      workflow
      domain
      alias
      status
      bookSubmitId
      publishedDate
      collection {
        id
      }
      organisation {
        id
        teams {
          id
          role
          members {
            id
            user {
              username
              givenName
              surname
              id
              # status
            }
            status
          }
          object {
            objectId
            objectType
          }
          name
        }
      }
      bookComponents(divisionLabels: $divisionLabels) {
        id
        bookId
        bookComponentVersionId
        title
        componentType
        updated
        created
        publishedDate
        abstract
        divisionId
        versionName
        status
        errors {
          id
          noticeTypeName
          severity
          assignee
          message
          objectId
        }

        metadata {
          url
        }
      }

      teams {
        id
        role
        members {
          id
          user {
            username
            givenName
            surname
            id
            #   status
          }
          status
        }
        object {
          objectId
          objectType
        }
        name
      }
    }
  }
`

const GET_BOOK_COMPONENT_ERRORS = gql`
  query getErrors($objectId: ID, $showHistory: Boolean) {
    getErrors(objectId: $objectId, showHistory: $showHistory) {
      ... on JobReportError {
        id
        jobId
        sessionId
        url
        status
        updated
        errors {
          id
          noticeTypeName
          severity
          assignee
          message
          objectId
        }
      }
      ... on BcmsReportError {
        id
        jobId
        updated
        errors {
          id
          noticeTypeName
          severity
          assignee
          message
          objectId
        }
      }
    }
  }
`

const CONVERT_SUBSCRIPTION = gql`
  subscription conversionCompleted($bookId: ID) {
    conversionCompleted(bookId: $bookId) {
      id
      title
      updated
      status
      bookId
      divisionId
      bookComponentVersionId
      componentType
      metadata {
        chapter_number
        filename
      }
    }
  }
`

export {
  GET_TOC_BOOK_COMPONENT,
  GET_BOOK,
  BOOK_COMPONENT_PUBLISH,
  GET_BOOK_COMPONENT_ERRORS,
  CONVERT_SUBSCRIPTION,
  GET_TOC,
  TOC_PUBLISH,
  GET_BOOK_COMPONENT_STATUSES,
}
