import { gql } from '@apollo/client'

const GET_ORGANIZATION = gql`
  query GetOrganisation($id: ID!) {
    getOrganisation(id: $id) {
      id
      settings {
        collections {
          funded
          bookSeries
        }
      }
    }
  }
`

const CREATE_COLLECTION = gql`
  mutation($input: CreateCollectionInput) {
    createCollection(input: $input) {
      id
      title
      metadata {
        openAccess
      }
      settings {
        approvalOrgAdminEditor
        citationSelfUrl
        groupBooks
        groupBooksBy
        orderBooksBy
        publisherUrl
        toc {
          contributors
          fullBookCitations
        }
        UKPMC
      }
    }
  }
`

const UPDATE_COLLECTION = gql`
  mutation($id: ID!, $input: UpdateCollectionInput) {
    updateCollection(id: $id, input: $input) {
      id
      title
      metadata {
        openAccess
      }
      settings {
        approvalOrgAdminEditor
        citationSelfUrl
        groupBooks
        groupBooksBy
        orderBooksBy
        publisherUrl
        toc {
          contributors
          fullBookCitations
        }
        UKPMC
      }
    }
  }
`

export { GET_ORGANIZATION, CREATE_COLLECTION, UPDATE_COLLECTION }
