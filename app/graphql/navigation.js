import { gql } from '@apollo/client'

const NAVIGATION_ORGANIZATION = gql`
  query NavigationOrganization($id: ID!) {
    getOrganisation(id: $id) {
      id
      name
    }
  }
`

const NAVIGATION_BOOK = gql`
  query NavigationBook($id: ID!) {
    getBook(id: $id) {
      id
      title
      settings {
        chapterIndependently
      }
    }
  }
`

const NAVIGATION_COLLECTION = gql`
  query NavigationCollection($id: ID!) {
    getCollection(id: $id) {
      id
      title
    }
  }
`

export { NAVIGATION_ORGANIZATION, NAVIGATION_BOOK, NAVIGATION_COLLECTION }
