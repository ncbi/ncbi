import { gql } from '@apollo/client'

const NCBI_URLS = gql`
  mutation($cookie: String!) {
    getNcbiUrls(cookie: $cookie) {
      homePageURL
      registerURL
      signInURL
      signOutURL
      errors
    }
  }
`

/* eslint-disable-next-line import/prefer-default-export */
export { NCBI_URLS }
