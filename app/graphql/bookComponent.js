import { gql } from '@apollo/client'

const bookComponentCore = `
  id
  bookId
  bookComponentVersionId
  title
  componentType
  updated
  created
  publishedDate
  abstract
  divisionId
  versionName
  status
  issues {
    id
    status
    title
  }
  errors {
    id
    noticeTypeName
    severity
    assignee
    message
    objectId
  }
  teams {
    id
    role
    members {
      id
      user {
        username
        givenName
        surname
        id
      }
      status
    }
    object {
      objectId
      objectType
    }
    name
  }
  reviewsResponse {
    id
    messageId
    bookComponentVersionId
    userId
    decision
  }
  owner {
    id
    username
    givenName
    surname
  }
`

const CONTRIBUTOR_FIELDS = gql`
  fragment editorAuthor on MetadataBookUser {
    affiliation
    degrees
    email
    givenName
    role
    suffix
    surname
  }
`

const PUBLISHED_VERSION_FIELDS = gql`
  fragment publishedVersions on BookComponent {
    id
    bookId
    bookComponentVersionId
    metadata {
      filename
    }
    convertedFile {
      id
      status
      id
      created
      updated
      versionName
    }
    publishedDate
    status
    versionName
  }
`

const COMPONENT_FILE = gql`
  fragment bookComponentFile on FileVersion {
    id
    created
    updated
    name
    source
    mimeType
    versionName
    status
    bcfId
    parentId
    copied
    tag
    owner {
      id
      username
      givenName
      surname
    }
  }
`

const BOOK_COMPONENT_FIELDS = gql`
  fragment bookComponent on BookComponent {
    ${bookComponentCore}
    alias
    metadata {
      alt_title
      author {
        affiliation
        degrees
        email
        givenName
        role
        suffix
        surname
      }
      editor {
        affiliation
        degrees
        email
        givenName
        role
        suffix
        surname
      }
      collaborativeAuthors
      date_publication
      chapter_number
      book_component_id
      date_created
      date_revised
      date_updated
      filename
      grants {
        id
        country
        institution_acronym
        institution_code
        institution_name
        number
      }
      language
      section_titles
      sub_title
      url
    }
    channels {
      id
      topic
    }
    reviewsFiles {
      id
      name
      created
      owner {
        id
        username
        givenName
        surname
      }
    }
  }
`

const BOOK_COMPONENT_USERS = gql`
  fragment bookComponentUsers on BookComponent {
    ${bookComponentCore}
    metadata {
      alt_title
      author {
        affiliation
        degrees
        email
        givenName
        role
        suffix
        surname
      }
      chapter_number
      book_component_id
      language
      sub_title
      date_created
      date_revised
      date_updated
      date_publication
      section_titles
      filename
      url
      hideInTOC
    }
    sourceFiles {
      id
      status
      id
      created
      updated
    }
    convertedFile {
      id
      status
      id
      created
      updated
    }
  }
`

const GET_BOOK_USERS = gql`
  ${CONTRIBUTOR_FIELDS}
  ${COMPONENT_FILE}
  ${BOOK_COMPONENT_USERS}

  query getBook($id: ID!, $divisionLabels: [String!], $skip: Int, $take: Int) {
    getBook(id: $id) {
      id
      abstract
      abstractTitle
      alias
      altTitle
      bookSubmitId
      domain
      edition
      publishedDate
      updated
      status
      subTitle
      title
      workflow
      version
      fundedContentType
      supplementaryFiles {
        ...bookComponentFile
      }
      convertedFile {
        ...bookComponentFile
      }
      supportFiles {
        ...bookComponentFile
      }
      images {
        ...bookComponentFile
      }
      pdf {
        ...bookComponentFile
      }
      sourceFiles {
        ...bookComponentFile
        relatedBookComponentFile
      }
      channels {
        id
        topic
      }
      reviewsFiles {
        id
        name
        created
        owner {
          id
          username
          givenName
          surname
        }
      }
      reviewsResponse {
        id
        messageId
        bookComponentVersionId
        userId
        decision
      }
      errors {
        id
        severity
        assignee
      }
      collection {
        id
        collectionType
        fileCover {
          id
          name
        }
        metadata {
          applyCoverToBooks
          applyPermissionsToBooks
          applyPublisherToBooks
          chapterProcessedSourceType
          copyrightStatement
          grants {
            id
            apply
            country
            institution_acronym
            institution_code
            institution_name
            number
          }
          licenseStatement
          licenseType
          licenseUrl
          notes {
            applyNotePdf
            description
            title
            type
          }
          openAccessLicense
          pub_loc
          pub_name
          wholeBookSourceType
        }
      }
      bookComponents(divisionLabels: $divisionLabels) {
        ...bookComponentUsers
      }

      fileCover {
        id
        name
      }
      fileAbstract {
        id
        name
      }
      organisation {
        id
        settings {
          type {
            publisher
            funder
          }
        }
        users {
          results {
            id
            username
          }
        }
        publisherOptions {
          id
          name
        }
        collections {
          id
          title
        }
        teams {
          id
          role
          members {
            id
            user {
              username
              givenName
              surname
              id
            }
            status
          }
          object {
            objectId
            objectType
          }
          name
        }
      }
      parts {
        id
        bookId
        bookComponentVersionId
        title
        updated
        created
        publishedDate
        abstract
        divisionId
      }
      divisions(divisionLabels: $divisionLabels) {
        id
        bookComponents(skip: $skip, take: $take) {
          metadata {
            total
            skip
            take
          }
          results {
            ...bookComponentUsers
          }
        }
        fullBookComponents
        label
      }
      settings {
        alternateVersionsPdf
        alternateVersionsPdfBook
        approvalOrgAdminEditor
        approvalPreviewer
        bookLevelAffiliationStyle
        bookLevelLinks
        bookLevelLinksMarkdown
        chapterIndependently
        chapterLevelAffiliationStyle
        citationSelfUrl
        citationType
        createPdf
        createVersionLink
        createWholebookPdf
        displayObjectsLocation
        footnotesDecimal
        formatChapterDate
        formatPublicationDate
        indexChaptersInPubmed
        indexInPubmed
        multiplePublishedVersions
        publisher {
          id
          name
        }
        publisherUrl
        questionAnswerStyle
        referenceListStyle
        specialPublisherLinkText
        specialPublisherLinkUrl
        toc {
          add_body_to_parts
          allTitle
          contributors
          documentHistory
          group_chapters_in_parts
          order_chapters_by
          subtitle
          tocMaxDepth
        }
        tocExpansionLevel
        UKPMC
        versionLinkText
        versionLinkUri
        xrefAnchorStyle

        # publisher
        # qaStatus
        # releaseStatus
        # supportMultiplePublishedVersions
      }
      teams {
        id
        role
        members {
          id
          user {
            username
            givenName
            surname
            id
          }
          status
        }
        object {
          objectId
          objectType
        }
        name
      }
      metadata {
        author {
          ...editorAuthor
        }
        collaborativeAuthors {
          givenName
        }
        copyrightStatement
        doi
        filename
        editor {
          ...editorAuthor
        }
        grants {
          id
          apply
          country
          institution_acronym
          institution_code
          institution_name
          number
        }
        licenseStatement
        licenseType
        licenseUrl
        openAccessLicense
        notes {
          description
          title
          type
        }
        openAccess
        dateCreated {
          day
          month
          year
        }
        dateUpdated {
          day
          month
          year
        }
        pubDate {
          dateType
          publicationFormat
          date {
            day
            month
            year
          }
          dateRange {
            startMonth
            startYear
            endMonth
            endYear
          }
        }
        processingInstructions
        pub_loc
        pub_name
        sourceType
        volume
        url
      }
      issues {
        id
        status
        title
      }
    }
  }
`

const GET_BOOK_USERS_OPTIMIZED = gql`
  ${CONTRIBUTOR_FIELDS}
  ${COMPONENT_FILE}

  query getBook($id: ID!, $divisionLabels: [String!]) {
    getBook(id: $id) {
      id
      abstract
      abstractTitle
      alias
      altTitle
      bookSubmitId
      domain
      edition
      publishedDate
      updated
      status
      subTitle
      title
      workflow
      version
      fundedContentType
      sourceFiles {
        ...bookComponentFile
        relatedBookComponentFile
      }
      reviewsResponse {
        id
        messageId
        bookComponentVersionId
        userId
        decision
      }
      errors {
        id
        severity
        assignee
      }
      collection {
        id
        collectionType
        fileCover {
          id
          name
        }
        metadata {
          applyCoverToBooks
          applyPermissionsToBooks
          applyPublisherToBooks
          chapterProcessedSourceType
          copyrightStatement
          grants {
            id
            apply
            country
            institution_acronym
            institution_code
            institution_name
            number
          }
          licenseStatement
          licenseType
          licenseUrl
          notes {
            applyNotePdf
            description
            title
            type
          }
          openAccessLicense
          pub_loc
          pub_name
          wholeBookSourceType
        }
      }
      fileCover {
        id
        name
      }
      fileAbstract {
        id
        name
      }
      organisation {
        id
        settings {
          type {
            publisher
            funder
          }
        }
        users {
          results {
            id
            username
          }
        }
        publisherOptions {
          id
          name
        }
        collections {
          id
          title
        }
        teams {
          id
          role
          members {
            id
            user {
              username
              givenName
              surname
              id
            }
            status
          }
          object {
            objectId
            objectType
          }
          name
        }
      }
      parts {
        id
        bookId
        bookComponentVersionId
        bookComponents {
          results {
            id
          }
        }
        title
        updated
        created
        publishedDate
        abstract
        divisionId
      }
      divisions(divisionLabels: $divisionLabels) {
        id
        fullBookComponents
        label
        partsNested
      }
      settings {
        alternateVersionsPdf
        alternateVersionsPdfBook
        approvalOrgAdminEditor
        approvalPreviewer
        bookLevelAffiliationStyle
        bookLevelLinks
        bookLevelLinksMarkdown
        chapterIndependently
        chapterLevelAffiliationStyle
        citationSelfUrl
        citationType
        createPdf
        createVersionLink
        createWholebookPdf
        displayObjectsLocation
        footnotesDecimal
        formatChapterDate
        formatPublicationDate
        indexChaptersInPubmed
        indexInPubmed
        multiplePublishedVersions
        publisher {
          id
          name
        }
        publisherUrl
        questionAnswerStyle
        referenceListStyle
        specialPublisherLinkText
        specialPublisherLinkUrl
        toc {
          add_body_to_parts
          allTitle
          contributors
          documentHistory
          group_chapters_in_parts
          order_chapters_by
          subtitle
          tocMaxDepth
        }
        tocExpansionLevel
        UKPMC
        versionLinkText
        versionLinkUri
        xrefAnchorStyle

        # publisher
        # qaStatus
        # releaseStatus
        # supportMultiplePublishedVersions
      }
      teams {
        id
        role
        members {
          id
          user {
            username
            givenName
            surname
            id
          }
          status
        }
        object {
          objectId
          objectType
        }
        name
      }
      metadata {
        author {
          ...editorAuthor
        }
        collaborativeAuthors {
          givenName
        }
        copyrightStatement
        doi
        filename
        editor {
          ...editorAuthor
        }
        grants {
          id
          apply
          country
          institution_acronym
          institution_code
          institution_name
          number
        }
        licenseStatement
        licenseType
        licenseUrl
        openAccessLicense
        notes {
          description
          title
          type
        }
        openAccess
        dateCreated {
          day
          month
          year
        }
        dateUpdated {
          day
          month
          year
        }
        pubDate {
          dateType
          publicationFormat
          date {
            day
            month
            year
          }
          dateRange {
            startMonth
            startYear
            endMonth
            endYear
          }
        }
        processingInstructions
        pub_loc
        pub_name
        sourceType
        volume
        url
      }
    }
  }
`

const GET_BOOK_VERSIONS = gql`
  query getBookVersions($bookId: ID!) {
    getBookVersions(bookId: $bookId) {
      id
      abstract
      abstractTitle
      alias
      altTitle
      bookSubmitId
      domain
      edition
      publishedDate
      updated
      status
      subTitle
      title
      workflow
      version
      fundedContentType

      teams {
        id
        role
        members {
          id
          user {
            username
            givenName
            surname
            id
          }
          status
        }
        object {
          objectId
          objectType
        }
        name
      }
    }
  }
`

const SUBMIT_BOOK_COMPONENTS = gql`
  ${CONTRIBUTOR_FIELDS}
  ${PUBLISHED_VERSION_FIELDS}
  mutation submitBookComponents($bookComponents: [ID]) {
    submitBookComponents(bookComponents: $bookComponents)
  }
`

const UPLOAD_CONVERT_FILE = gql`
  ${CONTRIBUTOR_FIELDS}
  ${PUBLISHED_VERSION_FIELDS}
  mutation uploadConvertFiles(
    $files: [FileUploadInput!]
    $id: ID!
    $versionName: Int
    $category: String!
  ) {
    uploadConvertFiles(
      files: $files
      id: $id
      versionName: $versionName
      category: $category
    ) {
      id
      title
      bookComponents {
        id
        bookId
        bookComponentVersionId
        title
        componentType
        updated
        created
        publishedDate
        abstract
        divisionId
        versionName
        status
        teams {
          id
          role
          members {
            id
            user {
              username
              givenName
              surname
              id
            }
            status
          }
          object {
            objectId
            objectType
          }
          name
        }
        reviewsResponse {
          id
          messageId
          bookComponentVersionId
          userId
          decision
        }
        publishedVersions {
          ...publishedVersions
        }
        metadata {
          author {
            ...editorAuthor
          }
          editor {
            ...editorAuthor
          }
          collaborativeAuthors
          date_publication
          book_component_id
          language
          alt_title
          sub_title
          date_created
          date_updated
          date_revised
          section_titles
          filename
        }
        owner {
          id
          username
          givenName
          surname
        }
      }
      divisions {
        id
        label
        fullBookComponents
      }
    }
  }
`

const GET_BOOK_COMPONENTS = gql`
  ${BOOK_COMPONENT_FIELDS}
  query getBookComponents($bookId: ID!) {
    getBookComponents(bookId: $bookId) {
      ...bookComponent
    }
  }
`

const GET_NEXT_PREVIOUS_BOOK_COMPONENT = gql`
  query getNextPrevious($id: ID!, $partId: ID!) {
    getNextPrevious(id: $id, partId: $partId) {
      id
      partId
    }
  }
`

const GET_BOOK_COMPONENT = gql`
  ${PUBLISHED_VERSION_FIELDS}
  ${COMPONENT_FILE}
  ${BOOK_COMPONENT_FIELDS}
  query getBookComponent($id: ID!) {
    getBookComponent(id: $id) {
      ...bookComponent
      publishedVersions {
        ...publishedVersions
      }
      isDuplicate
      supplementaryFiles {
        ...bookComponentFile
      }
      convertedFile {
        ...bookComponentFile
      }
      supportFiles {
        ...bookComponentFile
      }
      images {
        ...bookComponentFile
      }
      pdf {
        ...bookComponentFile
      }
      sourceFiles {
        ...bookComponentFile
        relatedBookComponentFile
      }
    }
  }
`

const GET_BOOK_COMPONENT_USERS = gql`
  ${BOOK_COMPONENT_USERS}
  query getBookComponent($id: ID!, $skip: Int, $take: Int) {
    getBookComponent(id: $id) {
      ...bookComponentUsers
      bookComponents(skip: $skip, take: $take) {
        metadata {
          total
          skip
          take
        }
        results {
          ...bookComponentUsers
        }
      }
    }
  }
`

const UPLOAD_PROGRESS = gql`
  subscription uploadConvertFileProgress {
    uploadConvertFileProgress {
      file
      percentage
      total
      status
    }
  }
`

const UPDATE_BOOK_COMPONENT = gql`
  ${BOOK_COMPONENT_FIELDS}
  mutation updateBookComponent($id: ID!, $input: UpdateBookComponentInput) {
    updateBookComponent(id: $id, input: $input) {
      ...bookComponent
    }
  }
`

const UPDATE_BOOK_COMPONENTS = gql`
  ${BOOK_COMPONENT_FIELDS}
  mutation updateBookComponents($bookComponents: [UpdateBookComponentInput]) {
    updateBookComponents(bookComponents: $bookComponents) {
      ...bookComponent
    }
  }
`

const UPLOAD_FILE = gql`
  ${COMPONENT_FILE}
  mutation($files: [FileUploadInput!], $id: ID!, $category: String) {
    uploadFile(files: $files, id: $id, category: $category) {
      ...bookComponentFile
    }
  }
`

const DELETE_FILE = gql`
  mutation deleteFile($versionName: String, $parentId: ID!) {
    deleteFile(versionName: $versionName, parentId: $parentId)
  }
`

const BOOK_COMPONENT_PUBLISH = gql`
  ${BOOK_COMPONENT_FIELDS}
  mutation publishBookComponents($bookComponent: [ID!]) {
    publishBookComponents(bookComponent: $bookComponent) {
      id
      status
    }
  }
`

const NEW_BOOK_COMPONENT_VERSION = gql`
  ${BOOK_COMPONENT_FIELDS}
  mutation createNewPublishedVersion($id: ID!) {
    createNewPublishedVersion(id: $id) {
      ...bookComponent
    }
  }
`

const NEW_PART = gql`
  ${CONTRIBUTOR_FIELDS}
  ${PUBLISHED_VERSION_FIELDS}
  mutation addBookComponent($id: ID!, $input: BookPart) {
    addBookComponent(id: $id, input: $input) {
      id
      bookId
      bookComponentVersionId
      componentType
      title
      updated
      created
      publishedDate
      abstract
      divisionId
      versionName
      status
      teams {
        id
        role
        members {
          id
          user {
            username
            givenName
            surname
            id
          }
          status
        }
        object {
          objectId
          objectType
        }
        name
      }
      metadata {
        author {
          ...editorAuthor
        }
        editor {
          ...editorAuthor
        }
        collaborativeAuthors
        date_publication
        book_component_id
        language
        alt_title
        sub_title
        date_created
        date_updated
        date_revised
        section_titles
        filename
      }
      channels {
        id
        topic
      }
      reviewsResponse {
        id
        messageId
        bookComponentVersionId
        userId
        decision
      }
      publishedVersions {
        ...publishedVersions
      }
    }
  }
`

const NEW_MESSAGE = gql`
  mutation(
    $content: String
    $type: String
    $channelId: String
    $files: [FileUploadInput!]
  ) {
    createMessage(
      content: $content
      type: $type
      channelId: $channelId
      files: $files
    ) {
      content
      user {
        id
      }
      id
      created
      updated
      files {
        id
        name
      }
      type
    }
  }
`

const FEEDBACK_PANEL_MESSAGES = gql`
  query messages($channelId: ID, $first: Int, $after: String, $before: String) {
    messages(
      channelId: $channelId
      first: $first
      after: $after
      before: $before
    ) {
      edges {
        content
        user {
          id
          username
        }
        id
        created
        updated
        files {
          id
          name
        }
        type
      }
      pageInfo {
        startCursor
        hasPreviousPage
        hasNextPage
      }
    }
  }
`

const MESSAGES_SUBSCRIPTION = gql`
  subscription messageCreated($channelId: ID) {
    messageCreated(channelId: $channelId) {
      id
      created
      updated
      content
      type
      files {
        id
        name
      }
      user {
        id
        username
      }
    }
  }
`

const GET_BOOK_COMPONENT_ERRORS = gql`
  query getErrors($objectId: ID, $showHistory: Boolean) {
    getErrors(objectId: $objectId, showHistory: $showHistory) {
      ... on JobReportError {
        id
        jobId
        sessionId
        url
        status
        updated
        errors {
          id
          noticeTypeName
          severity
          assignee
          message
          objectId
        }
      }
      ... on BcmsReportError {
        id
        jobId
        updated
        errors {
          id
          noticeTypeName
          severity
          assignee
          message
          objectId
        }
      }
    }
  }
`

const CONVERT_SUBSCRIPTION = gql`
  subscription conversionCompleted($bookId: ID!) {
    conversionCompleted(bookId: $bookId) {
      id
      title
      updated
      status
      bookId
      divisionId
      bookComponentVersionId
      componentType
      metadata {
        chapter_number
        filename
      }
    }
  }
`

const OBJECT_UPDATE_SUBSCRIPTION = gql`
  subscription objectUpdated($id: ID) {
    objectUpdated(id: $id) {
      id
    }
  }
`

const RECONVERT_FILE = gql`
  ${BOOK_COMPONENT_FIELDS}
  mutation reconvertBookComponent($ids: [ID!]!) {
    reconvertBookComponent(ids: $ids) {
      bookComponent
    }
  }
`

const GET_VENDOR_ISSUESDT = gql`
  query getIssuesDatatable($input: SearchModelInput) {
    getIssuesDatatable(input: $input) {
      metadata {
        total
        skip
        take
      }
      results {
        id
        title
        content
        bookComponentId
        user {
          username
          givenName
          surname
          id
        }
        status
        created
        updated
        files {
          id
          name
        }
      }
    }
  }
`

const NEW_ISSUE = gql`
  mutation createIssue(
    $title: String
    $content: String
    $bookComponentId: String
    $files: [FileUploadInput!]
  ) {
    createIssue(
      content: $content
      title: $title
      bookComponentId: $bookComponentId
      files: $files
    ) {
      id
      title
      content
      user {
        username
        givenName
        surname
        id
      }
      created
      updated
      files {
        id
        name
      }
    }
  }
`

const NEW_COMMENT = gql`
  mutation(
    $type: String
    $content: String
    $issueId: String
    $files: [FileUploadInput!]
  ) {
    createComment(
      content: $content
      type: $type
      issueId: $issueId
      files: $files
    ) {
      id
      content
      user {
        username
        givenName
        surname
        id
      }
      type
      created
      updated
      files {
        id
        name
      }
    }
  }
`

const GET_COMMENTS = gql`
  query getComments($issueId: ID!) {
    getComments(issueId: $issueId) {
      id
      content
      issueId
      user {
        username
        givenName
        surname
        id
      }
      created
      updated
      files {
        id
        name
      }
      type
    }
  }
`

const UPDATE_ISSUE = gql`
  mutation updateIssue($id: [ID], $status: String) {
    updateIssue(id: $id, status: $status) {
      id
      status
    }
  }
`

const UPDATETAGFILE = gql`
  mutation updateTagFile($id: ID!, $tag: String!) {
    updateTagFile(id: $id, tag: $tag) {
      id
    }
  }
`

const COMMENTS_SUBSCRIPTION = gql`
  subscription commentCreated($issueId: ID) {
    commentCreated(issueId: $issueId) {
      id
      created
      updated
      content
      type
      files {
        id
        name
      }
      user {
        username
        givenName
        surname
        id
      }
    }
  }
`

const MOVE_ABOVE = gql`
  mutation MoveAbove($input: MoveRelativeInput!) {
    moveAbove(input: $input) {
      id
    }
  }
`

const MOVE_BELOW = gql`
  mutation MoveBelow($input: MoveRelativeInput!) {
    moveBelow(input: $input) {
      id
    }
  }
`

const MOVE_INSIDE = gql`
  mutation MoveInside($input: MoveComponentInput!) {
    moveInsidePart(input: $input) {
      id
    }
  }
`

const REPEAT_BOOK_COMPONENT = gql`
  mutation repeatBookComponent($partIds: [ID], $componentId: ID, $bookId: ID) {
    repeatBookComponent(
      componentId: $componentId
      partIds: $partIds
      bookId: $bookId
    ) {
      id
    }
  }
`

export {
  UPDATE_BOOK_COMPONENT,
  UPDATE_BOOK_COMPONENTS,
  UPDATETAGFILE,
  GET_BOOK_COMPONENT_ERRORS,
  GET_BOOK_USERS,
  GET_BOOK_COMPONENTS,
  GET_BOOK_COMPONENT,
  GET_BOOK_COMPONENT_USERS,
  UPLOAD_CONVERT_FILE,
  SUBMIT_BOOK_COMPONENTS,
  UPLOAD_PROGRESS,
  UPLOAD_FILE,
  DELETE_FILE,
  BOOK_COMPONENT_PUBLISH,
  RECONVERT_FILE,
  NEW_BOOK_COMPONENT_VERSION,
  NEW_MESSAGE,
  FEEDBACK_PANEL_MESSAGES,
  MESSAGES_SUBSCRIPTION,
  CONVERT_SUBSCRIPTION,
  OBJECT_UPDATE_SUBSCRIPTION,
  NEW_PART,
  NEW_ISSUE,
  NEW_COMMENT,
  GET_VENDOR_ISSUESDT,
  GET_COMMENTS,
  UPDATE_ISSUE,
  COMMENTS_SUBSCRIPTION,
  GET_BOOK_VERSIONS,
  GET_NEXT_PREVIOUS_BOOK_COMPONENT,
  GET_BOOK_USERS_OPTIMIZED,
  MOVE_ABOVE,
  MOVE_BELOW,
  MOVE_INSIDE,
  REPEAT_BOOK_COMPONENT,
}
