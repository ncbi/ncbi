import { gql } from '@apollo/client'

import {
  BOOK_SETTINGS_TEMPLATE_FIELDS,
  CREATE_BOOK_SETTINGS_TEMPLATE,
  UPDATE_BOOK_SETTINGS_TEMPLATE,
} from './bookSettingsTemplates'

const GET_ORGANIZATION = gql`
  query GetOrganisation($id: ID!) {
    getOrganisation(id: $id) {
      id
      name
      agreement
      abbreviation
      publisherId
      collections {
        id
        title
      }
      settings {
        type {
          publisher
          funder
        }
        collections {
          funded
          bookSeries
        }
        xml
        pdf
        word
      }
      teams {
        id
        role
        name
        members {
          status
          user {
            id
            username
          }
        }
      }
    }
  }
`

const ORGANIZATION_USERS = gql`
  query OrganisationUsers($organisationId: ID!, $searchValue: String) {
    searchOrganisationUsers(
      organisationId: $organisationId
      searchValue: $searchValue
    ) {
      metadata {
        total
        skip
        take
      }
      results {
        id
        username
        email
        givenName
        surname
        teams {
          id
          role
          name
          members {
            id
            user {
              id
            }
            status
            awardeeAffiliation
            awardNumber
          }
        }
      }
    }
  }
`

const ORGANIZATION_TEMPLATES = gql`
  ${BOOK_SETTINGS_TEMPLATE_FIELDS}

  query GetOrganisationTemplates($organizationId: ID!) {
    bookSettingsTemplates(organizationId: $organizationId) {
      ...BookSettingsTemplateFields
    }
  }
`

const UPDATE_USER = gql`
  mutation UpdateUser($id: ID, $input: NcbiUserUpdateInput) {
    updateNcbiUser(id: $id, input: $input) {
      id
      username
      email
      givenName
      surname
      # verificationAwardNumber
    }
  }
`

const VERIFY_USER = gql`
  mutation VerifyUser($id: [ID!]!, $organisationId: ID!) {
    verifyUsers(id: $id, organisationId: $organisationId) {
      id
      username
      email
      givenName
      surname
      # verificationAwardNumber
    }
  }
`

const REJECT_USER = gql`
  mutation RejectUser($id: [ID!]!, $organisationId: ID!) {
    rejectUsers(id: $id, organisationId: $organisationId) {
      id
      username
      email
      givenName
      surname
      # verificationAwardNumber
    }
  }
`

const CHECKALL_ORGANIZATION_USERS = gql`
  mutation CheckAllOrganizationUsers(
    $input: CheckAllOrganizationUser
    $action: Actions!
  ) {
    checkAllOrganizationUsers(input: $input, action: $action)
  }
`

const UPDATE_MEMBER_TEAMS = gql`
  mutation UpdateMemberTeams($id: ID!, $members: [TeamMemberInput]) {
    assignMembersToTeam(id: $id, members: $members) {
      name
      members {
        id
        user {
          username
          givenName
          surname
          id
        }
        status
      }
    }
  }
`

/* eslint-disable-next-line import/prefer-default-export */
export {
  CHECKALL_ORGANIZATION_USERS,
  CREATE_BOOK_SETTINGS_TEMPLATE,
  GET_ORGANIZATION,
  ORGANIZATION_TEMPLATES,
  ORGANIZATION_USERS,
  REJECT_USER,
  UPDATE_MEMBER_TEAMS,
  UPDATE_USER,
  UPDATE_BOOK_SETTINGS_TEMPLATE,
  VERIFY_USER,
}
