import { css } from 'styled-components'

const Link = css`
  min-width: 0;
  text-transform: none;
`

export default Link
