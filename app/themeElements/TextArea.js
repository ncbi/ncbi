import { css } from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

const TextArea = css`
  margin-bottom: calc(${th('gridUnit')} * 2);

  > label {
    color: ${th('colorText')};
    font-size: 12pt;
    margin-bottom: calc(${th('gridUnit')} / 2);
    text-align: start;
  }
`

export default TextArea
