import { css } from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

const readOnly = css`
  background: ${th('colorBackgroundHue')};
  cursor: not-allowed;
`

const TextField = css`
  border-width: 0px;
  line-height: ${th('lineHeightBase')};
  margin-bottom: calc(${th('gridUnit')} * 2);
  padding: 0;
  width: ${props => (props.width ? props.width : '100%')};

  input {
    border-radius: ${th('borderRadius')};
    border-style: solid;
    border-width: 1px;
    color: ${th('colorText')};
    height: 34px;

    ${props => props.readOnly && readOnly};
  }

  input :focus {
    border: 1px solid ${th('colorBorder')};
    box-shadow: 0 0 1px ${th('colorPrimary')};
    outline: none;
  }

  > label {
    color: ${th('colorText')};
    font-size: 12pt;
    margin-bottom: calc(${th('gridUnit')} / 2);
    text-align: start;
  }
`

export default TextField
