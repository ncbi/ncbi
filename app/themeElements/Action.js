import { css } from 'styled-components'

import { th, grid } from '@pubsweet/ui-toolkit'

const Action = css`
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBackground')};
  border-radius: ${th('borderRadius')};
  color: ${th('colorTextReverse')};
  font-weight: normal;
  line-height: ${grid(3)};
  padding: 3px;
  text-align: center;
  text-decoration: none;
  width: 100%;

  /* stylelint-disable no-descending-specificity */
  :hover,
  :focus,
  :active {
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBackground')};
    color: ${th('colorBackground')};
    text-decoration: none;
  }
`

export default Action
