import { css } from 'styled-components'

import { grid, th } from '@pubsweet/ui-toolkit'

export const WaxLinkContainer = css`
  border-radius: 0;
  box-shadow: rgba(0, 0, 0, 0.5) 0px 4px 8px 0px,
    rgba(0, 0, 0, 0.5) 0px 0px 1px 0px;
  padding: ${grid(0.5)} ${grid(1)};
`

export const WaxLinkInput = css`
  font-size: ${th('fontSizeBaseSmall')};
`

const linkButton = css`
  border: 1px solid ${th('colorPrimary')};
  font-size: ${th('fontSizeBaseSmall')};
  padding: ${grid(1)} ${grid(2)};
`

export const WaxLinkCreate = css`
  margin-right: ${grid(0.5)};
  ${linkButton}
`

export const WaxLinkCancel = css`
  margin: 0;
  ${linkButton}
`
