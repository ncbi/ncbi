import { css } from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

const activeTab = css`
  background: #fff;
  border: 1px solid ${th('colorPrimary')};
  border-bottom: none;
  font-weight: bold;
`

export const Tabs = css`
  border-bottom: 1px solid ${th('colorPrimary')};
`

export const TabContainer = css`
  border: 5px solid grey;
  border-left: none;
`

export const Tab = css`
  background: #eee;
  border: none;
  border-left: none;
  color: ${th('colorText')};
  cursor: pointer;
  margin: 5px;
  transition: all 0.1s ease-in;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${props => props.active && activeTab}
`
