import { css } from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

const borderSize = '2px'

const Bullet = css`
  background-color: ${th('colorPrimary')};
  border-radius: 50%;
  height: calc(100% - ${borderSize} * 2);
  width: calc(100% - ${borderSize} * 2);
`

const Root = css`
  align-items: center;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 0;
  min-width: unset;
  padding-bottom: 35px;
`

const Separator = css`
  background-color: ${props =>
    props.isPast ? th('colorPrimary') : th('colorFurniture')};
  transition: background-color 0.5s ease-in-out;
`

const Step = css`
  align-items: center;
  background-color: ${props =>
    props.isPast ? th('colorPrimary') : th('colorBackground')};
  border: ${borderSize} solid
    ${props => {
      if (props.isPast) return th('colorPrimary')
      if (props.isCurrent) return th('colorText')
      return th('colorFurniture')
    }};
  border-radius: 50%;
  display: flex;
  height: 16px;
  position: relative;
  width: 16px;
`

const StepTitle = css`
  color: ${props => {
    if (props.isPast) return th('colorPrimary')
    if (props.isCurrent) return th('colorText')
    return th('colorFurniture')
  }};
  font-size: ${th('fontSizeBase')};
  font-weight: ${props => (props.isCurrent ? 'bold' : 'normal')};
`

const Success = css`
  display: none;
`

const ThemedSteps = { Bullet, Root, Separator, Step, StepTitle, Success }
export default ThemedSteps
