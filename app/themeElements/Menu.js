import { css } from 'styled-components'

const Options = css`
  bottom: 45px;
  left: 0;
  position: absolute;
  right: 0;
`

export default {
  Options,
}
