import { css } from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

const Accordion = css`
  > div > span {
    color: ${th('colorPrimaryDarker')};
    font-style: normal;
    font-weight: 500;
    text-transform: capitalize;
  }
`

export default Accordion
