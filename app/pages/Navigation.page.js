import React, { useContext, useState } from 'react'
import { useApolloClient, useMutation, useQuery } from '@apollo/client'
import { useLocation, matchPath } from 'react-router-dom'

import { NCBI_URLS } from '../graphql/ncbiUrls'
import {
  NAVIGATION_ORGANIZATION,
  NAVIGATION_BOOK,
  NAVIGATION_COLLECTION,
} from '../graphql/navigation'

import CurrentUserContext from '../userContext'
import { getNcbiCookie } from '../cookie'
import { AppBar } from '../../ui/components'
import AccessModal from '../../ui/Pages/Dashboard/Organization/AccessModal'

const getUrlMatch = ({ currentPath }) => {
  const bookManagerMatch = matchPath(currentPath, {
    path: '/organizations/:organizationId/bookmanager/:bookId',
    exact: true,
  })

  const tocMatch = matchPath(currentPath, {
    path:
      '/organizations/:organizationId/bookmanager/:bookId/toc/:bookComponentId',
    exact: true,
  })

  const tocCollectionMatch = matchPath(currentPath, {
    path:
      '/organizations/:organizationId/collectionmanager/:collectionId/toc/:id',
    exact: true,
  })

  const collectionManagerMatch = matchPath(currentPath, {
    path: '/organizations/:organizationId/collectionmanager/:collectionId',
    exact: true,
  })

  const bookComponentMatch = matchPath(currentPath, {
    path: '/organizations/:organizationId/bookmanager/:bookId/:bookComponentId',
    exact: true,
  })

  const collectionBookComponentMatch = matchPath(currentPath, {
    path: '/organizations/:organizationId/collectionmanager/:collectionId',
    exact: true,
  })

  const isOneMatch =
    !!bookManagerMatch ||
    !!tocMatch ||
    !!tocCollectionMatch ||
    !!collectionManagerMatch ||
    !!bookComponentMatch ||
    !!collectionBookComponentMatch

  let path = ''

  const matches = [
    bookManagerMatch,
    tocMatch,
    tocCollectionMatch,
    collectionManagerMatch,
    bookComponentMatch,
    collectionBookComponentMatch,
  ]

  matches.forEach(item => {
    if (item) {
      path = item.path
    }
  })

  return {
    bookManagerMatch,
    tocMatch,
    tocCollectionMatch,
    collectionManagerMatch,
    bookComponentMatch,
    collectionBookComponentMatch,
    isBookComponent: !!bookComponentMatch,
    isOneMatch,
    path,
  }
}

const Navigation = () => {
  const location = useLocation()
  const { pathname } = location
  const { currentUser } = useContext(CurrentUserContext)

  const [modalIsOpen, setModalIsOpen] = useState(false)

  const localHistory =
    JSON.parse(sessionStorage.getItem('history') || '[]') || []

  if (localHistory?.length === 0 || localHistory[0] !== pathname) {
    const currentMatches = getUrlMatch({ currentPath: pathname })

    const lastItemMatches = getUrlMatch({
      currentPath: localHistory[0],
    })

    if (currentMatches.path !== lastItemMatches.path) {
      if (
        !(currentMatches.isBookComponent && lastItemMatches.isBookComponent)
      ) {
        localHistory.unshift(pathname)
        localHistory.length >= 20 && localHistory.pop()
        sessionStorage.setItem(`history`, JSON.stringify(localHistory))
      }
    }
  }

  /**
   * QUERIES
   */
  const currentPath = localHistory[0]

  const organizationMatch = matchPath(window.location.pathname, {
    path: '/organizations/:organizationId',
  })

  const generalBookMatch = matchPath(window.location.pathname, {
    path: '/organizations/:organizationId/bookmanager/:bookId',
  })

  const {
    bookManagerMatch,
    tocMatch,
    tocCollectionMatch,
    collectionManagerMatch,
    bookComponentMatch,
    partMatch,
  } = getUrlMatch({ currentPath })

  const { collectionBookComponentMatch } = getUrlMatch({
    currentPath: localHistory[1],
  })

  const isBookManager = !!bookManagerMatch
  const isBookComponentManager = !!bookComponentMatch || !!partMatch
  const isTocManager = !!tocMatch
  const isTocCollectionManager = !!tocCollectionMatch
  const isCollectionManager = !!collectionManagerMatch

  const organizationId =
    organizationMatch && organizationMatch.params.organizationId

  const bookId =
    (generalBookMatch && generalBookMatch.params.bookId) ||
    (tocMatch && tocMatch.params.bookId)

  const { data: organizationData } = useQuery(NAVIGATION_ORGANIZATION, {
    skip: !organizationId,
    variables: {
      id: organizationId,
    },
  })

  const { data: bookData } = useQuery(NAVIGATION_BOOK, {
    skip: !bookId,
    variables: {
      id: bookId,
    },
  })

  const collectionId =
    (collectionBookComponentMatch &&
      collectionBookComponentMatch.params.collectionId) ||
    (tocCollectionMatch && tocCollectionMatch.params.collectionId)

  const { data: collectionData } = useQuery(NAVIGATION_COLLECTION, {
    skip: !collectionId,
    variables: {
      id: collectionId,
    },
  })

  let showOrganization = false
  let showBackToOrganization = false
  let organizationName = null

  let organizationUrl = null
  let showBackToBook = false
  let bookName = null
  let bookUrl = null

  let showBackToCollection = false
  let collectionName = null
  let collectionUrl = null

  if (organizationData?.getOrganisation) {
    organizationName = organizationData.getOrganisation.name
    organizationUrl = `/organizations/${organizationId}`

    if (
      (isTocCollectionManager && collectionBookComponentMatch) ||
      (isBookManager && collectionBookComponentMatch)
    ) {
      showBackToCollection = true
      collectionName = collectionData?.getCollection.title
      // eslint-disable-next-line prefer-destructuring
      collectionUrl = localHistory[1]
    } else if (isTocManager || isBookComponentManager) {
      showBackToBook = true
      bookName = bookData?.getBook.title
      bookUrl = `/organizations/${organizationId}/bookmanager/${bookId}`
    } else if (isBookManager || isCollectionManager) {
      showBackToOrganization = true
    }
  }

  if (
    !(showBackToBook || showBackToOrganization || showBackToCollection) &&
    organizationData
  ) {
    showOrganization = true
  }
  /**
   * LOGOUT
   */

  const client = useApolloClient()
  const [getLogoutUrls] = useMutation(NCBI_URLS)

  const logoutUser = async () => {
    localStorage.removeItem('token')
    client.cache.reset()

    const cookie = getNcbiCookie()

    if (cookie) {
      const {
        data: {
          getNcbiUrls: { signOutURL, errors },
        },
      } = await getLogoutUrls({ variables: { cookie } })

      if (errors) {
        window.location.reload()
        return
      }

      window.location.href = `${signOutURL}?back_url=${window.location.href}`
    } else {
      window.location.reload()
    }
  }

  /**
   * AUTH
   */

  const isSysAdmin = currentUser?.auth.isSysAdmin || false
  const isPDF2XMLVendor = currentUser?.auth.isPDF2XMLVendor || false
  const canViewOrganizationAccess = !(isSysAdmin || isPDF2XMLVendor)

  /**
   * UI
   */

  let displayName

  if (currentUser) {
    const { username, givenName, surname } = currentUser
    const fullName = givenName && surname && `${givenName} ${surname}`
    displayName = fullName || username
  }

  if (!currentUser) return null

  return (
    <>
      <AppBar
        bookName={bookName}
        bookUrl={bookUrl}
        canViewOrganizationAccess={canViewOrganizationAccess}
        collectionName={collectionName}
        collectionUrl={collectionUrl}
        displayName={displayName}
        isSysAdmin={isSysAdmin}
        loginLink="/login"
        onClickOrganizationAccess={() => setModalIsOpen(true)}
        onLogoutClick={() => logoutUser(client)}
        organizationId={organizationId}
        organizationName={organizationName}
        organizationUrl={organizationUrl}
        showBackToBook={showBackToBook}
        showBackToCollection={showBackToCollection}
        showBackToOrganization={showBackToOrganization}
        showOrganization={showOrganization}
      />

      <AccessModal
        currentUser={currentUser}
        hideModal={() => setModalIsOpen(false)}
        modalIsOpen={modalIsOpen}
      />
    </>
  )
}

Navigation.propTypes = {}

Navigation.defaultProps = {}

export default Navigation
