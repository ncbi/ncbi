import React, { useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { useQuery, useMutation } from '@apollo/client'

import CreateCollectionUI from '../../ui/Pages/CreateCollection/CreateCollection'
import {
  GET_ORGANIZATION,
  CREATE_COLLECTION,
  UPDATE_COLLECTION,
} from '../graphql/createCollection'
import {
  collectionSettingsToCollectionAPI,
  collectionMetadataToCollectionAPI,
} from './_helpers/collectionSettingsToApiConverter'
import { Notification } from '../../ui/components'

const CreateCollection = props => {
  const { organizationId } = useParams()
  const history = useHistory()

  const [currentStep, setCurrentStep] = useState(0)
  const [stepTwoInitialValues, setStepTwoInitialValues] = useState(null)
  const [collectionId, setCollectionId] = useState(null)

  const { data, loading } = useQuery(GET_ORGANIZATION, {
    variables: { id: organizationId },
  })

  const [createCollection, { loading: loadingCreateCollection }] = useMutation(
    CREATE_COLLECTION,
  )

  const createCollectionFn = async formData => {
    const {
      title,
      collectionType,
      dateOfPublication,
      publisherLocation,
      publisherName,
      publicationDateType,
      wholeBookSourceType,
      chapterProcessedSourceType,
    } = formData

    const metadataToConvert = {
      title,
      dateOfPublication,
      publisherLocation,
      publisherName,
      publicationDateType,
      wholeBookSourceType,
      chapterProcessedSourceType,
    }

    const convertedMetadata = await collectionMetadataToCollectionAPI(
      metadataToConvert,
    )

    const input = {
      organisationId: organizationId,
      collectionType,
      ...convertedMetadata,
    }

    return createCollection({ variables: { input } })
      .then(res => {
        const responseData = res.data.createCollection
        const { id, settings } = responseData
        /* eslint-disable-next-line camelcase */
        const { groupBooks, groupBooksBy, orderBooksBy, toc } = settings

        setStepTwoInitialValues({
          groupBooksBy,
          groupBooksOnTOC: groupBooks,
          orderBooksBy,
          requireApprovalByOrgAdminOrEditor: true,
          tocContributors: toc.contributors,
          tocFullBookCitations: toc.fullBookCitations,
        })

        setCollectionId(id)
        setCurrentStep(1)
      })
      .catch(e => {
        console.error(e)

        Notification({
          message: `Could not create collection: ${e.message}`,
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const [updateCollection, { loading: loadingUpdateCollection }] = useMutation(
    UPDATE_COLLECTION,
    {
      variables: {
        id: collectionId,
      },
    },
  )

  const updateCollectionFn = formData => {
    const transformed = collectionSettingsToCollectionAPI(formData)
    const input = transformed
    input.settings.isCollectionGroup = false

    return updateCollection({
      variables: { input },
    })
      .then(() => {
        history.push(
          `/organizations/${organizationId}/collectionmanager/${collectionId}`,
        )
      })
      .catch(e => {
        console.error(e)

        Notification({
          message: `Could not update collection: ${e.message}`,
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const organization = data?.getOrganisation

  const bookSeriesCollectionsEnabled =
    organization?.settings.collections.bookSeries

  const fundedCollectionsEnabled = organization?.settings.collections.funded

  return (
    <CreateCollectionUI
      bookSeriesCollectionsEnabled={bookSeriesCollectionsEnabled}
      currentStep={currentStep}
      fundedCollectionsEnabled={fundedCollectionsEnabled}
      isStepOneSaving={loadingCreateCollection}
      isStepTwoSaving={loadingUpdateCollection}
      loading={loading}
      onStepOneSubmit={createCollectionFn}
      onStepTwoSubmit={updateCollectionFn}
      stepTwoInitialValues={stepTwoInitialValues}
    />
  )
}

export default CreateCollection
