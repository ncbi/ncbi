/* eslint-disable react/prop-types */
import React, { useEffect, useMemo } from 'react'
import { useQuery, useMutation, useLazyQuery } from '@apollo/client'
import { useHistory, useParams, useLocation } from 'react-router-dom'
import query from '../../ui/Pages/Bookmanager/graphql'

import BookComponentPage from '../../ui/Pages/Bookmanager/BookComponentPage'
import { Spinner, Notification } from '../../ui/components'
// import { getNavigationComponents } from './_helpers/bookComponentNavigation'
import {
  GET_BOOK_COMPONENT,
  NEW_BOOK_COMPONENT_VERSION,
  // GET_BOOK_USERS,
  CONVERT_SUBSCRIPTION,
  UPDATE_BOOK_COMPONENT,
  GET_NEXT_PREVIOUS_BOOK_COMPONENT,
  OBJECT_UPDATE_SUBSCRIPTION,
  GET_BOOK_USERS_OPTIMIZED,
  MOVE_ABOVE,
  MOVE_BELOW,
  MOVE_INSIDE,
  REPEAT_BOOK_COMPONENT,
} from '../graphql/bookComponent'

import { GET_ORGANIZATION } from '../graphql/createBook'
import {
  handleComponentReorder,
  handleComponentRepeat,
  moveSearch,
} from '../../ui/Pages/Bookmanager/commonPageHelpers'

import {
  componentAPIToComponentMetadata,
  componentMetadataToComponentAPI,
} from './_helpers/componentSettingsToApiConverter'
import Granthub from './_helpers/Granthub'
import useGetCollections from './_helpers/useGetCollectionsList'

const PAGE = 'bookComponent'

const BookComponentManager = () => {
  const { bookId, bookComponentId, organizationId } = useParams()
  const { hash: partId } = useLocation()
  const history = useHistory()

  const {
    data: bookComponentData,
    loading: loadingBookComponent,
    subscribeToMore: subscribeToMoreBookComponent,
    refetch: refetchBookComponent,
  } = useQuery(GET_BOOK_COMPONENT, {
    variables: { id: bookComponentId },
  })

  const {
    data: nextPreviousData,
    // loading: loadingNextPreviousBookComponent,
  } = useQuery(GET_NEXT_PREVIOUS_BOOK_COMPONENT, {
    variables: { id: bookComponentId, partId: partId.substring(1) },
  })

  const {
    loading: loadingBook,
    data: bookData,
    subscribeToMore,
    refetch,
  } = useQuery(GET_BOOK_USERS_OPTIMIZED, {
    variables: {
      id: bookId,
      input: {
        skip: 0,
        sort: { direction: 'asc', field: ['username'] },
        take: 10,
      },
    },
  })

  const differentOrganizations =
    !!bookData?.getBook.organisation.id &&
    organizationId !== bookData?.getBook.organisation.id

  const { data: organizationData, loading: loadingOrg1 } = useQuery(
    GET_ORGANIZATION,
    {
      variables: { id: organizationId },
    },
  )

  const { data: bookOrganization, loading: loadingOrg2 } = useQuery(
    GET_ORGANIZATION,
    {
      variables: { id: bookData?.getBook.organisation.id },
      skip: !differentOrganizations,
    },
  )

  const organization = organizationData?.getOrganisation

  const collectionOptions = useGetCollections({
    organization,
    bookOrganization: bookOrganization?.getOrganisation,
    differentOrganizations,
  })

  const [getMultipleBookComponentsById] = useLazyQuery(
    query.GET_MULTIPLE_BOOK_COMPONENT_CHILDREN,
    {
      fetchPolicy: 'network-only',
    },
  )

  const bodyId = bookData?.getBook.divisions.find(d => d.label === 'body')?.id
  if (bookData && !bodyId)
    console.error('Book component page: Body division id not found!')

  const mutationOptions = {
    refetchQueries: [
      {
        query: GET_BOOK_COMPONENT,
        variables: { id: bookComponentId },
      },
      {
        query: GET_NEXT_PREVIOUS_BOOK_COMPONENT,
        variables: { id: bookComponentId, partId: partId.substring(1) },
      },
      {
        query: GET_BOOK_USERS_OPTIMIZED,
        variables: { id: bookId },
      },
      {
        query: query.GET_DIVISION,
        variables: { id: bodyId, take: 100, skip: 0 },
      },
    ],
  }

  const [moveAbove] = useMutation(MOVE_ABOVE, mutationOptions)

  const [moveBelow] = useMutation(MOVE_BELOW, mutationOptions)

  const [moveInside] = useMutation(MOVE_INSIDE, mutationOptions)

  const [repeatBookComponent] = useMutation(
    REPEAT_BOOK_COMPONENT,
    mutationOptions,
  )

  const [newBookCompVersion, { loading: loadingNewVersion }] = useMutation(
    NEW_BOOK_COMPONENT_VERSION,
  )

  const [updateBookComponent, { loading: updatingBookComponent }] = useMutation(
    UPDATE_BOOK_COMPONENT,
    {
      variables: {
        id: bookComponentId,
      },
    },
  )

  // const [updateBook, { loading: loadingUpdateBook }] = useMutation(
  //   query.UPDATE_BOOK,
  //   mutationOptions,
  // )

  // const [updateBookTeam, { loading: loadingUpdateBookTeams }] = useMutation(
  //   query.UPDATE_BOOK_TEAM,
  // )

  // const handleUpdateBook = input => {
  //   return updateBook({
  //     variables: {
  //       id: bookId,
  //       input,
  //     },
  //   }).catch(err => {
  //     console.error(err)

  //     Notification({
  //       message: `Could not update - ${err.message}`,
  //       type: 'danger',
  //       title: 'Error',
  //     })
  //   })
  // }

  // const handleUpdateBookTeams = teams => {
  //   const members = teams.map(team => ({
  //     id: team.id,
  //     team: {
  //       members: team.members.map(tm => ({
  //         status: tm.status,
  //         user: { id: tm.user.id },
  //       })),
  //     },
  //   }))

  //   return updateBookTeam({
  //     awaitRefetchQueries: true,
  //     refetchQueries: [
  //       {
  //         query: query.GET_BOOK,
  //         variables: { id: bookId },
  //       },
  //     ],
  //     variables: {
  //       input: members,
  //     },
  //   })
  // }

  const handleSaveMetadata = async submittedValues => {
    const transformed = await componentMetadataToComponentAPI(
      submittedValues,
      PAGE,
    )

    updateBookComponent({
      variables: {
        input: transformed,
      },
    })
      .then(() => {
        Notification({
          message: 'Metadata successfully updated',
          type: 'success',
          title: 'Success',
        })
      })
      .catch(e => {
        console.error(e)

        Notification({
          message: 'Could not update metadata',
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const [searchMoveComponents] = useLazyQuery(query.SEARCH_BOOK_COMPONENTS, {
    fetchPolicy: 'no-cache',
  })

  const handleMoveSearch = options => {
    return moveSearch({
      ...options,
      bookId,
      searchFn: searchMoveComponents,
    })
  }

  const handleReorder = (
    dragId,
    dropId,
    actionType,
    parentPartId = null,
    parentIdsToRefresh = [],
  ) => {
    return handleComponentReorder(
      dragId,
      dropId,
      actionType,
      parentPartId,
      parentIdsToRefresh,
      moveAbove,
      moveInside,
      moveBelow,
      getMultipleBookComponentsById,
    )
  }

  const handleRepeat = (componentId, partIds, parentIdsToRefresh) => {
    return handleComponentRepeat(
      bookId,
      componentId,
      partIds,
      parentIdsToRefresh,
      repeatBookComponent,
      getMultipleBookComponentsById,
    )
  }

  useEffect(() => {
    subscribeToMore({
      document: CONVERT_SUBSCRIPTION,
      variables: { bookId },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev
        return null
      },
    })

    subscribeToMoreBookComponent({
      document: OBJECT_UPDATE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev

        refetchBookComponent()
        return null
      },
      variables: { id: bookComponentData?.getBookComponent?.id },
    })

    subscribeToMore({
      document: OBJECT_UPDATE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev

        refetch()
        return null
      },
      variables: { id: bookData?.getBook?.id },
    })
  })

  /* DATA WRANGLING */
  const book = bookData?.getBook

  // const body = book?.divisions.find(div => div.label === 'body')
  // const bodyDivisionId = body?.id

  // const flatOrderedBookComponents = getNavigationComponents({
  //   book,
  //   divisionId: bodyDivisionId,
  //   bookComponentVersionId: bookComponentId,
  //   partId, // optional
  // })

  const isBookOrderedManually =
    book?.settings?.toc?.order_chapters_by === 'manual' || false

  // const { prevComp, nextComp } = flatOrderedBookComponents
  const bookComponent = bookComponentData?.getBookComponent

  const isMultiChapterBook = book?.settings.chapterIndependently
  const workflow = book?.workflow
  // const isWord = workflow === 'word'

  const division = book?.divisions.find(d => d.id === bookComponent?.divisionId)

  const partsTree = useMemo(() => {
    return division && division.partsNested
      ? JSON.parse(division.partsNested)
      : undefined
  }, [division?.partsNested])

  const body = book?.divisions.find(d => d.label === 'body')
  const frontMatter = book?.divisions.find(d => d.label === 'frontMatter')
  const backMatter = book?.divisions.find(d => d.label === 'backMatter')

  const divisionsMap = {
    body,
    frontMatter,
    backMatter,
  }

  const filename = bookComponent?.metadata?.filename
  const isPart = bookComponent?.componentType === 'part'

  const isComponentInDivisionTopLevel = division?.fullBookComponents.includes(
    bookComponent.id,
  )

  let iconName = 'book'

  if (isMultiChapterBook) {
    iconName = 'bookWithChapters'
  }

  const bookUrl = `/organizations/${organizationId}/bookmanager/${bookId}`

  const backPressed = () => {
    history.push(bookUrl)
  }

  const createNewBookcomponentVersion = async () => {
    const {
      data: { createNewPublishedVersion },
    } = await newBookCompVersion({
      variables: {
        id: bookComponent?.id,
      },
    }).catch(res => {
      console.error(res)

      Notification({
        message: 'Could not create new version',
        type: 'danger',
        title: 'Error',
      })
    })

    window.location.assign(
      `/organizations/${organizationId}/bookmanager/${bookId}/${createNewPublishedVersion?.bookComponentVersionId}${partId}`,
    )
  }

  const onPageClick = page => {
    const { bookComponentVersionId } = bookComponent?.publishedVersions[
      page.currentPage - 1
    ]

    history.push(
      `/organizations/${organizationId}/bookmanager/${bookId}/${bookComponentVersionId}${partId}`,
    )
  }

  const [bookComponentVersionsCount, activeBookComponentVersion] = [
    bookComponent?.publishedVersions.length || 1,
    bookComponent?.publishedVersions.findIndex(
      x => x.bookComponentVersionId === bookComponentId,
    ) + 1,
  ]

  const bookComponentMetadata =
    bookComponent &&
    book &&
    componentAPIToComponentMetadata(bookComponent, book)

  // TODO: this should be done in backend
  if (bookComponentMetadata) {
    if (!bookComponentMetadata.dateCreated) {
      bookComponentMetadata.dateCreated = { day: '', month: '', year: '' }
    } else {
      const dts = bookComponentMetadata.dateCreated.split('-')

      bookComponentMetadata.dateCreated = {
        day: dts[2] || '',
        month: dts[1] || '',
        year: dts[0] || '',
      }
    }

    if (!bookComponentMetadata.dateUpdated) {
      bookComponentMetadata.dateUpdated = { day: '', month: '', year: '' }
    } else {
      const dts = bookComponentMetadata.dateUpdated.split('-')

      bookComponentMetadata.dateUpdated = {
        day: dts[2] || '',
        month: dts[1] || '',
        year: dts[0] || '',
      }
    }

    if (!bookComponentMetadata.dateRevised) {
      bookComponentMetadata.dateRevised = { day: '', month: '', year: '' }
    } else {
      const dts = bookComponentMetadata.dateRevised.split('-')

      bookComponentMetadata.dateRevised = {
        day: dts[2] || '',
        month: dts[1] || '',
        year: dts[0] || '',
      }
    }

    if (!bookComponentMetadata.dateOfPublication) {
      bookComponentMetadata.dateOfPublication = { day: '', month: '', year: '' }
    } else {
      const dts = bookComponentMetadata.dateOfPublication.split('-')

      bookComponentMetadata.dateOfPublication = {
        day: dts[2] || '',
        month: dts[1] || '',
        year: dts[0] || '',
      }
    }

    if (!bookComponentMetadata.authors) bookComponentMetadata.authors = []
    if (!bookComponentMetadata.editors) bookComponentMetadata.editors = []
    if (!bookComponentMetadata.collaborativeAuthors)
      bookComponentMetadata.collaborativeAuthors = []
  }

  const searchGranthubFn = searchValue => Granthub.search(searchValue, PAGE)

  if (loadingBook || loadingBookComponent || loadingOrg1 || loadingOrg2)
    return <Spinner pageLoader />

  return (
    <BookComponentPage
      activeBookComponentVersion={activeBookComponentVersion}
      backPressed={backPressed}
      book={book}
      bookComponent={bookComponent}
      bookComponentMetadata={bookComponentMetadata}
      bookComponentVersionsCount={bookComponentVersionsCount}
      collectionOptions={collectionOptions}
      createNewBookcomponentVersion={createNewBookcomponentVersion}
      divisionsMap={divisionsMap}
      filename={filename}
      handleSaveMetadata={handleSaveMetadata}
      iconName={iconName}
      isBookOrderedManually={isBookOrderedManually}
      isComponentInDivisionTopLevel={isComponentInDivisionTopLevel}
      isMultiChapterBook={isMultiChapterBook}
      isPart={isPart}
      isSaving={updatingBookComponent}
      loadingBook={loadingBook}
      loadingBookComponent={loadingBookComponent}
      loadingNewVersion={loadingNewVersion}
      // loadingUpdateBook={loadingUpdateBook}
      // loadingUpdateBookTeams={loadingUpdateBookTeams}
      nextComponent={nextPreviousData?.getNextPrevious[1]}
      onMoveSearch={handleMoveSearch}
      onPageClick={onPageClick}
      onReorder={handleReorder}
      // onUpdateBook={handleUpdateBook}
      // onUpdateBookTeams={handleUpdateBookTeams}
      onRepeat={handleRepeat}
      partsTree={partsTree}
      prevComponent={nextPreviousData?.getNextPrevious[0]}
      searchGranthub={searchGranthubFn}
      workflow={workflow}
    />
  )
}

export default BookComponentManager
