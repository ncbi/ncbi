import React, { useContext, useEffect, useState } from 'react'
import { useQuery, useMutation } from '@apollo/client'
import { useHistory, useParams } from 'react-router-dom'

import CurrentUserContext from '../userContext'
import CreateBookUI from '../../ui/Pages/CreateBook/CreateBook'
import { ConfirmCreateBookStepOne } from '../../ui/modals'
import { Notification } from '../../ui/components'
import {
  CREATE_BOOK,
  GET_ORGANIZATION,
  GET_FUNDED_COLLECTIONS_OF_OTHER_ORGANIZATIONS,
  UPDATE_BOOK,
  REFETCH_COLLECTION_ON_BOOK_CREATION,
} from '../graphql/createBook'
import { CURRENT_USER } from '../graphql'
import { bookSettingsToBookAPI, bookAPIToBookSettings } from './_helpers'

const CreateBook = props => {
  const history = useHistory()
  const { organizationId } = useParams()
  const { currentUser } = useContext(CurrentUserContext)

  const [currentStep, setCurrentStep] = useState(0)
  const [workflow, setWorkflow] = useState(null)
  const [bookType, setBookType] = useState(null)
  const [bookId, setBookId] = useState(null)
  const [stepTwoInitialValues, setStepTwoInitialValues] = useState(null)
  const [showConfimationModal, setShowConfirmationModal] = useState(false)
  const [createBookValues, setCreateBookValues] = useState(null)
  const [inCollection, setInCollection] = useState(false)
  const [inFundedCollection, setInFundedCollection] = useState(false)
  const [collectionId, setCollectionId] = useState(null)

  // QUERIES

  const { loading, data, refetch } = useQuery(GET_ORGANIZATION, {
    variables: { id: organizationId },
  })

  useEffect(() => {
    refetch()
  }, [])

  const organization = data?.getOrganisation

  const {
    data: fundedCollectionsData,
    loading: fundedCollectionsLoading,
  } = useQuery(GET_FUNDED_COLLECTIONS_OF_OTHER_ORGANIZATIONS, {
    variables: { organisationId: organizationId },
    skip: !organization?.settings.type.publisher,
  })

  const fundedCollections =
    fundedCollectionsData?.getFundedCollectionsOfOtherOrganisations

  // MUTATIONS

  const [createBook, { loading: isSavingCreate }] = useMutation(CREATE_BOOK, {
    refetchQueries: [{ query: CURRENT_USER }],
  })

  const createBookFn = values => {
    const transformed = {
      title: values.title,
      workflow: values.conversionWorkflow,
      organisationId: organization.id,
      collectionId: values.collection,
      fundedContentType: values.fundedContentType,
      version: values.version.toString(),
      settings: {
        chapterIndependently: values.submissionType === 'individualChapters',
        publisher: values.publisher,
      },
    }

    return createBook(
      {
        variables: {
          input: transformed,
        },
      },
      {
        refetchQueries: [
          {
            query: REFETCH_COLLECTION_ON_BOOK_CREATION,
            variables: { id: transformed.collectionId },
            skip: !transformed.collectionId,
          },
        ],
      },
    )
      .then(createBookResponse => {
        const responseData = createBookResponse.data.createBook

        const {
          id,
          collection,
          settings,
          workflow: responseWorkflow,
        } = responseData

        const responseBookType = settings.chapterIndependently
          ? 'chapterProcessed'
          : 'wholeBook'

        const isInCollection = !!collection
        const isInFundedCollection = collection?.collectionType === 'funded'

        setBookId(id)
        setBookType(responseBookType)
        setWorkflow(responseWorkflow)
        setCurrentStep(1)
        setStepTwoInitialValues(bookAPIToBookSettings(responseData))
        setInCollection(isInCollection)
        setInFundedCollection(isInFundedCollection)
        setCollectionId(collection?.id)
      })
      .catch(err => {
        console.error(err)

        Notification({
          message: `Could not create book: ${err.message}`,
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const [updateBook, { loading: isSavingUpdate }] = useMutation(UPDATE_BOOK)

  const updateBookSettingsFn = values => {
    const input = bookSettingsToBookAPI(values)
    input.collectionId = collectionId

    updateBook({
      variables: {
        id: bookId,
        input,
      },
    })
      .then(response => {
        const responseData = response.data.updateBook

        const {
          settings,
          bookComponents,
          // workflow: responseWorkflow,
        } = responseData

        if (!settings.chapterIndependently && bookComponents.length > 0) {
          const bookComponentId = bookComponents[0].bookComponentVersionId
          history.push(`bookmanager/${bookId}/${bookComponentId}`)
        } else {
          history.push(`bookmanager/${bookId}`)
        }
      })
      .catch(err => {
        console.error(err)

        Notification({
          message: `Could not update book: ${err.message}`,
          type: 'danger',
          title: 'Error',
        })
      })
  }

  // AUTH

  const {
    editorForOrganisation,
    orgAdminForOrganisation,
    isSysAdmin,
  } = currentUser.auth

  const isOrgAdmin = orgAdminForOrganisation.includes(organization?.id)
  const isEditor = editorForOrganisation.includes(organization?.id)

  const canViewPage = isSysAdmin || isOrgAdmin || isEditor

  if (!canViewPage)
    return 'You do not have sufficient permissions to view this page'

  // UI

  let acceptedWorkflows,
    showFundedContentType,
    publisherOptions,
    collectionOptions

  if (organization) {
    acceptedWorkflows = ['pdf', 'word', 'xml'].filter(
      wf => organization.settings[wf],
    )

    showFundedContentType = organization.settings.collections.funded

    publisherOptions = organization.publisherOptions.map(publisher => ({
      value: publisher.id,
      label: publisher.name,
    }))

    collectionOptions = organization.collections.map(collection => ({
      value: collection.id,
      label: collection.title,
      organization: organization.name,
      isOwnCollection: true,
    }))

    if (fundedCollections) {
      const transformedFundedCollections = fundedCollections
        .filter(
          x =>
            x.collectionType === 'funded' || x.collectionType === 'bookSeries',
        )
        .map(collection => ({
          value: collection.id,
          label: collection.title,
          organization: collection.organisation.name,
          isOwnCollection: false,
          tag: collection.organisation.name,
        }))

      collectionOptions = collectionOptions.concat(transformedFundedCollections)
    }
  }

  const isFunderOrganization = organization?.settings.type.funder
  const isPublisherOrganization = organization?.settings.type.publisher

  const handleClickNext = values => {
    setShowConfirmationModal(true)
    setCreateBookValues({ ...values, version: values.version.toString() })
  }

  const handleClickConfirm = () => {
    createBookFn(createBookValues)

    setShowConfirmationModal(false)
    setCreateBookValues(null)
  }

  const handleClickCancelModal = () => {
    setShowConfirmationModal(false)
  }

  return (
    <>
      <CreateBookUI
        acceptedWorkflows={acceptedWorkflows}
        bookType={bookType}
        collectionOptions={collectionOptions}
        createBook={handleClickNext}
        currentStep={currentStep}
        inCollection={inCollection}
        inFundedCollection={inFundedCollection}
        isEditor={isEditor}
        isFunderOrganization={isFunderOrganization}
        isOrgAdmin={isOrgAdmin}
        isPublisherOrganization={isPublisherOrganization}
        isSaving={isSavingCreate || isSavingUpdate}
        isSysAdmin={isSysAdmin}
        loading={loading || fundedCollectionsLoading}
        organizationId={organizationId}
        publisherOptions={publisherOptions}
        showFundedContentType={showFundedContentType}
        stepTwoInitialValues={stepTwoInitialValues}
        updateBookSettings={updateBookSettingsFn}
        workflow={workflow}
      />

      <ConfirmCreateBookStepOne
        isOpen={showConfimationModal}
        onClickCancel={handleClickCancelModal}
        onClickSuccess={handleClickConfirm}
      />
    </>
  )
}

CreateBook.propTypes = {}

CreateBook.defaultProps = {}

export default CreateBook
