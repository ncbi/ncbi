import React, { useContext } from 'react'
import { useHistory, useParams, Redirect } from 'react-router-dom'
import { useQuery, useLazyQuery, useMutation } from '@apollo/client'
import { cloneDeep, findIndex } from 'lodash'

import {
  CHECKALL_ORGANIZATION_USERS,
  GET_ORGANIZATION,
  ORGANIZATION_TEMPLATES,
  ORGANIZATION_USERS,
  REJECT_USER,
  UPDATE_MEMBER_TEAMS,
  UPDATE_USER,
  VERIFY_USER,
  CREATE_BOOK_SETTINGS_TEMPLATE,
  UPDATE_BOOK_SETTINGS_TEMPLATE,
} from '../graphql/organization'

import { Notification } from '../../ui/components'
import { Organization as OrganizationUI } from '../../ui/Pages'
import {
  bookSettingsToBookAPI,
  transformTemplatesToUIData,
  getAccessedOrganizations,
} from './_helpers'
import ConstantsContext from '../constantsContext'
import { getDisplayName } from '../../ui/common/utils'
import PermissionsGate from '../components/PermissionsGate'
import useGetCurrentUser from '../useGetCurrentUser'

const Organization = () => {
  const history = useHistory()
  const { organizationId } = useParams()
  const { s } = useContext(ConstantsContext)
  const currentUser = useGetCurrentUser()
  const accesedOrgs = getAccessedOrganizations(currentUser)

  // redirect if is not System admin or PDF2XMLVendor AND has not access specifically on that organization
  if (
    !(currentUser?.auth.isSysAdmin || currentUser?.auth.isPDF2XMLVendor) &&
    accesedOrgs.findIndex(x => x.id === organizationId) === -1
  ) {
    return <Redirect to="/organizations" />
  }

  // #region QUERIES

  const { loading, data } = useQuery(GET_ORGANIZATION, {
    variables: { id: organizationId },
  })

  const organization = data?.getOrganisation

  const { data: templatesData, loading: templatesLoading } = useQuery(
    ORGANIZATION_TEMPLATES,
    {
      variables: { organizationId },
    },
  )

  const [
    getOrganizationUsers,
    { loading: loadingUsers, data: organizationUsersData, called: usersCalled },
  ] = useLazyQuery(ORGANIZATION_USERS, {
    variables: {
      organisationId: organizationId,
      searchValue: '',
    },
  })

  const users = organizationUsersData?.searchOrganisationUsers

  if (!usersCalled) getOrganizationUsers()

  const searchUsers = searchValue => {
    getOrganizationUsers({
      variables: { searchValue },
    })
  }
  // #endregion QUERIES

  // #region MUTATIONS

  const templateSuccess = {
    type: 'success',
    title: 'Success',
    message: 'Template successfully saved',
  }

  const templateError = error => ({
    message: `Could not save template: ${error.message}`,
    type: 'danger',
    title: 'Error',
  })

  const [createTemplate, { loading: isSavingCreateBookSettings }] = useMutation(
    CREATE_BOOK_SETTINGS_TEMPLATE,
    {
      update: (cache, { data: createTemplateResponse }) => {
        const responseData = createTemplateResponse.createBookSettingsTemplate
        const { templateType } = responseData

        const queryParams = {
          query: ORGANIZATION_TEMPLATES,
          variables: { organizationId },
        }

        const currentTemplateData = cache.readQuery({ ...queryParams })
          ?.bookSettingsTemplates

        // replace default template in our list with newly created one
        const newData = currentTemplateData.filter(
          template =>
            template.id !== `${organizationId}-default-${templateType}`,
        )

        newData.push(responseData)

        cache.writeQuery({
          ...queryParams,
          data: {
            bookSettingsTemplates: newData,
          },
        })
      },
    },
  )

  const createTemplateFn = (type, values) => {
    return createTemplate({
      variables: {
        input: {
          organizationId,
          type,
          values: bookSettingsToBookAPI(values),
        },
      },
    })
      .then(() => {
        Notification(templateSuccess)
      })
      .catch(error => {
        console.error(error)
        Notification(templateError(error))
      })
  }

  const [updateTemplate, { loading: isSavingUpdateBookSettings }] = useMutation(
    UPDATE_BOOK_SETTINGS_TEMPLATE,
  )

  const updateTemplateFn = (templateId, values) => {
    return updateTemplate({
      variables: {
        input: {
          templateId,
          values: bookSettingsToBookAPI(values),
        },
      },
    })
      .then(() => {
        Notification(templateSuccess)
      })
      .catch(error => {
        console.error(error)
        Notification(templateError(error))
      })
  }

  const [updateUserFn] = useMutation(UPDATE_USER, {
    refetchQueries: [
      {
        query: ORGANIZATION_USERS,
        variables: {
          organisationId: organizationId,
          searchValue: '',
        },
      },
      { query: GET_ORGANIZATION, variables: { id: organizationId } },
    ],
  })

  const updateUser = (userId, formData) => {
    const { givenName, surname, username, email } = formData
    const { teams } = organization
    const inputTeams = []

    const roles = ['orgAdmin', 'editor']

    const hasRole = role => {
      const team = teams.find(t => t.role === role)
      let hasIt = false

      if (team) {
        const memberIds = team.members.map(m => m.user.id)
        hasIt = memberIds.includes(userId)
      }

      return hasIt
    }

    roles.forEach(role => {
      const isCurrentlyRole = hasRole(role)
      const hasRoleIncoming = formData[role]
      const wasAddedAsRole = !isCurrentlyRole && hasRoleIncoming
      const wasRemovedAsRole = isCurrentlyRole && !hasRoleIncoming
      const shouldTouchRoleTeam = wasAddedAsRole || wasRemovedAsRole

      if (shouldTouchRoleTeam) {
        const roleTeam = teams.find(t => t.role === role)
        const roleTeamForInput = cloneDeep(roleTeam)

        if (wasAddedAsRole) {
          roleTeamForInput.members.push({
            status: 'enabled',
            user: { id: userId },
          })
        }

        if (wasRemovedAsRole) {
          roleTeamForInput.members = roleTeamForInput.members.filter(
            m => m.user.id !== userId,
          )
        }

        inputTeams.push(roleTeamForInput)
      }
    })

    const plainUserTeam = teams.find(t => t.role === 'user')

    const currentStatus = plainUserTeam.members.find(m => m.user.id === userId)
      .status

    const newStatus = formData.enabled ? 'enabled' : 'disabled'

    if (newStatus !== currentStatus) {
      const plainUserTeamForInput = cloneDeep(plainUserTeam)

      const memberIndex = findIndex(
        plainUserTeamForInput.members,
        m => m.user.id === userId,
      )

      plainUserTeamForInput.members[memberIndex].status = newStatus
      inputTeams.push(plainUserTeamForInput)
    }

    const cleanedUpInputTeams = inputTeams.map(t => {
      const cleanTeam = {
        id: t.id,
        role: t.role,
      }

      cleanTeam.members = t.members.map(m => {
        const cleanMember = {
          user: {
            id: m.user.id,
          },
        }

        if (m.id) cleanMember.id = m.id
        if (m.status) cleanMember.status = m.status

        return cleanMember
      })

      return cleanTeam
    })

    const updateUserData = {
      variables: {
        id: userId,
        input: {
          givenName,
          surname,
          username,
          email,
          teams: cleanedUpInputTeams,
        },
      },
    }

    return updateUserFn(updateUserData)
      .then(() => {
        const message = s('notifications.succesUpdateUser').replace(
          '$name',
          givenName,
        )

        Notification({ message })
      })
      .catch(res => {
        console.error(res)

        Notification({
          message: 'Could not update',
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const [verifyUserFn] = useMutation(VERIFY_USER, {
    refetchQueries: [
      {
        query: ORGANIZATION_USERS,
        variables: {
          organisationId: organizationId,
          searchValue: '',
        },
      },
    ],
  })

  const verifyUser = userId => {
    return verifyUserFn({
      variables: {
        id: userId,
        organisationId: organizationId,
      },
    })
      .then(() => {
        let message

        if (Array.isArray(userId)) {
          message = 'Selected users have been successfully verified'
        } else {
          const userVerified = users.results.find(u => u.id === userId)
          const displayName = getDisplayName(userVerified)

          message = s('notifications.successVerifyUser').replace(
            '$name',
            displayName,
          )
        }

        Notification({ message })
      })
      .catch(res => {
        console.error(res)

        Notification({
          message: 'Could not update',
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const [rejectUserFn] = useMutation(REJECT_USER, {
    refetchQueries: [
      {
        query: ORGANIZATION_USERS,
        variables: {
          organisationId: organizationId,
          searchValue: '',
        },
      },
    ],
  })

  const rejectUser = userId => {
    return rejectUserFn({
      variables: {
        id: userId,
        organisationId: organizationId,
      },
    })
      .then(() => {
        let message

        if (Array.isArray(userId)) {
          message = 'Selected users have been successfully rejected'
        } else {
          const userRejected = users.results.find(u => u.id === userId)
          const displayName = getDisplayName(userRejected)

          message = s('notifications.successRejectUser').replace(
            '$name',
            displayName,
          )
        }

        Notification({ message })
      })
      .catch(res => {
        console.error(res)

        Notification({
          message: 'Could not update',
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const [checkAllOrganizationUsersFn] = useMutation(
    CHECKALL_ORGANIZATION_USERS,
    {
      refetchQueries: [
        {
          query: ORGANIZATION_USERS,
          variables: {
            organisationId: organizationId,
            searchValue: '',
          },
        },
      ],
    },
  )

  const checkAllOrganizationUsers = (action, role, onlyEnabled) => {
    const inputData = { organizationId }
    if (role) inputData.role = role

    return checkAllOrganizationUsersFn({
      variables: {
        action,
        input: inputData,
      },
    })
      .then(() => {
        if (!onlyEnabled) {
          Notification({
            message: s('notifications.assignRolesWarning'),
            type: 'warning',
          })
        }
      })
      .catch(res => {
        console.error(res)

        Notification({
          message: 'Could not update',
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const [updateMemberTeamsFn] = useMutation(UPDATE_MEMBER_TEAMS, {
    refetchQueries: [
      {
        query: ORGANIZATION_USERS,
        variables: {
          organisationId: organizationId,
          searchValue: '',
        },
      },
    ],
  })

  const updateMemberTeams = (
    teamId,
    members,
    role,
    onlyEnabled,
    userName = 'Selected users',
  ) => {
    return updateMemberTeamsFn({
      variables: {
        id: teamId,
        members,
        role,
      },
    })
      .then(() => {
        if (!onlyEnabled) {
          Notification({
            message: s('notifications.assignRolesWarning'),
            type: 'warning',
          })
        }
      })
      .catch(res => {
        console.error(res)

        Notification({
          message: `${userName} already has the role assigned`,
          type: 'danger',
          title: 'Error',
        })
      })
  }

  // #endregion MUTATIONS

  // #region AUTH

  const {
    orgAdminForOrganisation,
    editorForOrganisation,
    isPDF2XMLVendor,
    isSysAdmin,
  } = currentUser.auth

  const isOrgAdmin = orgAdminForOrganisation.includes(organizationId)
  const isEditor = editorForOrganisation.includes(organizationId)

  const canCreateBookCollection = PermissionsGate({
    scopes: ['create-book'],
    getPermission: true,
  })

  const canCreateCollection = PermissionsGate({
    scopes: ['create-collection'],
    getPermission: true,
  })

  const canViewSettingsTab = PermissionsGate({
    scopes: ['view-organization-tab'],
    getPermission: true,
  })

  const canViewMetadataTab = PermissionsGate({
    scopes: ['view-metadata-content-tab'],
    getPermission: true,
  })

  const canViewUsersTab = PermissionsGate({
    scopes: ['view-user-tab'],
    getPermission: true,
  })

  const canViewBooksCollectionsTab = true

  const canViewBookSettingsTemplatesTab = PermissionsGate({
    scopes: ['view-book-settings-template-tab'],
    getPermission: true,
  })

  // #endregion AUTH

  // #region LOCALSTORAGE

  // Handle saving last visited tab in localstorage
  const savedTabKey = localStorage.getItem(`${organizationId}-tab-key`)

  const handleChangeTab = key => {
    localStorage.setItem(`${organizationId}-tab-key`, key)
  }

  // #endregion LOCALSTORAGE

  // #region UI

  const handleCreateBook = () => {
    history.push(`/organizations/${organizationId}/createBook`)
  }

  const handleCreateCollection = () => {
    history.push(`/organizations/${organizationId}/createCollection`)
  }

  if (!loading && !organization) {
    console.error('No data')
    return null
  }

  let showCreateNewBook = false
  let showCreateNewCollection = false
  let templates
  let isPublisherOrganization = false

  if (!loading && organization) {
    const { pdf, word, xml, collections } = organization.settings

    isPublisherOrganization = organization.settings.type.publisher

    showCreateNewBook = (pdf || word || xml) && canCreateBookCollection

    showCreateNewCollection =
      (collections.funded || collections.bookSeries) && canCreateCollection
  }

  if (!templatesLoading && templatesData) {
    templates = transformTemplatesToUIData(templatesData.bookSettingsTemplates)
  }

  const usersTransformed = users?.results.map(user => {
    const orgUserTeam = organization?.teams.find(t => t.role === 'user')
    const member = orgUserTeam?.members.find(m => m.user.id === user.id)
    const status = member?.status

    const roleDontShowList = ['Org User', 'awardee']

    const roles = organization?.teams
      .filter(t => t.members.find(m => m.user.id === user.id))
      .map(t => t.name)
      .filter(role => !roleDontShowList.includes(role))

    return {
      ...user,
      roles,
      status,
    }
  })

  const usersObject = {
    metadata: users?.metadata,
    results: usersTransformed,
  }

  return (
    <OrganizationUI
      canCreateBookCollection={canCreateBookCollection}
      canViewBooksCollectionsTab={canViewBooksCollectionsTab}
      canViewBookSettingsTemplatesTab={canViewBookSettingsTemplatesTab}
      canViewMetadataTab={canViewMetadataTab}
      canViewSettingsTab={canViewSettingsTab}
      canViewUsersTab={canViewUsersTab}
      checkAllOrganizationUsers={checkAllOrganizationUsers}
      createTemplate={createTemplateFn}
      isEditor={isEditor}
      isOrgAdmin={isOrgAdmin}
      isPDF2XMLVendor={isPDF2XMLVendor}
      isPublisherOrganization={isPublisherOrganization}
      isSavingBookSettings={
        isSavingCreateBookSettings || isSavingUpdateBookSettings
      }
      isSysAdmin={isSysAdmin}
      loading={loading}
      loadingTemplates={templatesLoading}
      loadingUsers={!usersCalled || loadingUsers || loading}
      onChangeTab={handleChangeTab}
      onClickCreateBook={handleCreateBook}
      onClickCreateCollection={handleCreateCollection}
      organization={organization}
      rejectUser={rejectUser}
      savedTabKey={savedTabKey}
      searchUsers={searchUsers}
      showCreateNewBook={showCreateNewBook}
      showCreateNewCollection={showCreateNewCollection}
      templates={templates}
      updateMemberTeams={updateMemberTeams}
      updateTemplate={updateTemplateFn}
      updateUser={updateUser}
      users={usersObject}
      verifyUser={verifyUser}
    />
  )

  // #endregion UI
}

Organization.propTypes = {}

Organization.defaultProps = {}

export default Organization
