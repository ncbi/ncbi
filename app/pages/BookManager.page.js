/* eslint-disable react/prop-types */
import React, { useEffect, useState, useContext, useMemo } from 'react'
import {
  useQuery,
  useLazyQuery,
  useMutation,
  useApolloClient,
} from '@apollo/client'

import query from '../../ui/Pages/Bookmanager/graphql'
import { Notification, Spinner } from '../../ui/components'
import BookChapterProcessed from '../../ui/Pages/Bookmanager/Book/BookChapterProcessed'
import WholeBookPage from '../../ui/Pages/Bookmanager/BookComponent/asWholeBook/BookComponent.page'
import useGetCollections from './_helpers/useGetCollectionsList'
import {
  transformData,
  formatDateTime,
  gatherKeys,
  flatten,
} from '../../ui/Pages/Bookmanager/pageHelpers'
import {
  handleComponentReorder,
  handleComponentRepeat,
  moveSearch,
} from '../../ui/Pages/Bookmanager/commonPageHelpers'
import CurrentUserContext from '../userContext'

const {
  GET_BOOK,
  GET_DIVISION,
  GET_ORGANIZATION,
  SEARCH_BOOK_COMPONENTS,
  GET_BOOK_COMPONENT_CHILDREN,
  MOVE_ABOVE,
  MOVE_BELOW,
  MOVE_INSIDE,
  SUBMIT_BOOK_COMPONENTS,
  BOOK_COMPONENT_PUBLISH,
  RECONVERT_FILE,
  REPEAT_BOOK_COMPONENT,
  CONVERT_SUBSCRIPTION,
  OBJECT_UPDATE_SUBSCRIPTION,
  NEW_PART,
  UPDATE_BOOK,
  UPLOAD_CONVERT_FILE_OPTIMIZED,
  GET_MULTIPLE_BOOK_COMPONENT_CHILDREN,
  UPDATE_BOOK_TEAM,
} = query

const PAGE_SIZE = 100
const DEFAULT_TAB = 'body'

const allowedStatusesToMove = [
  'loading-preview',
  'loading-errors',
  'preview',
  'conversion-errors',
  'publish-failed',
  'published',
]

const BookManagerPage = ({
  history,
  match: {
    params: { bookId, organizationId },
  },
}) => {
  const client = useApolloClient()
  const { currentUser } = useContext(CurrentUserContext)

  const { auth } = currentUser

  const {
    isSysAdmin,
    isPDF2XMLVendor,
    editorForBook,
    orgAdminForOrganisation,
  } = auth

  const isOrgAdmin = orgAdminForOrganisation.includes(organizationId)
  const isBookEditor = editorForBook.includes(bookId)

  // #region state
  const [activeTab, setActiveTab] = useState(DEFAULT_TAB)
  const [skip, setSkip] = useState(0)
  const [totalRecords, setTotalRecords] = useState(0)

  const [currentPages, setCurrentPages] = useState({
    frontMatter: 1,
    body: 1,
    backMatter: 1,
    search: 1,
  })

  const [searchQueryParams, setSearchQueryParams] = useState({
    filters: [],
    search: '',
    sort: { field: ['updated'], direction: 'desc' },
  })

  const [appliedSearchQueryParams, setAppliedSearchQueryParams] = useState()
  const [searchCriteria, setSearchCriteria] = useState([])
  const [showingSearch, setShowingSearch] = useState(false)
  const [searchBookComponentsData, setSearchBookComponentsData] = useState(null)
  const [changingSearchPage, setChangingSearchPage] = useState(false)
  // #endregion state

  // #region queries
  const { data, loading, subscribeToMore } = useQuery(GET_BOOK, {
    variables: {
      id: bookId,
    },
  })

  const book = data?.getBook

  const currentDivision = book?.divisions.find(d => d.label === activeTab)
  const currentDivisionId = currentDivision?.id

  const currentDivisionNestedParts = useMemo(() => {
    return currentDivision && currentDivision.partsNested
      ? JSON.parse(currentDivision.partsNested)
      : undefined
  }, [currentDivision?.partsNested])

  const {
    data: divisionData,
    loading: loadingDivision,
    fetchMore: fetchMoreDivision,
    refetch,
  } = useQuery(GET_DIVISION, {
    variables: {
      id: currentDivisionId,
      take: PAGE_SIZE,
      skip,
    },
    skip: !currentDivisionId,
    onCompleted: res => {
      setTotalRecords(
        parseInt(res.getDivision.bookComponents.metadata.total, 10),
      )

      return res
    },
  })

  const { data: organizationData } = useQuery(GET_ORGANIZATION, {
    variables: { id: organizationId },
  })

  const bookOrganizationId = book?.organisation.id
  const differentOrganizations = organizationId !== bookOrganizationId

  // If the book org is not the org you're in, fetch the book org
  const { data: bookOrganization } = useQuery(GET_ORGANIZATION, {
    variables: { id: bookOrganizationId },
    skip: !book || !differentOrganizations,
  })

  const organization = organizationData?.getOrganisation

  const collectionOptions = useGetCollections({
    organization,
    bookOrganization: bookOrganization?.getOrganisation,
    differentOrganizations,
  })

  const [
    searchBookComponents,
    { loading: loadingSearchResults, fetchMore: searchMore },
  ] = useLazyQuery(SEARCH_BOOK_COMPONENTS, {
    variables: {
      input: searchQueryParams,
    },
    onCompleted: searchData => {
      setShowingSearch(true)
      setCurrentPages({ ...currentPages, search: 1 })
      setSearchBookComponentsData(searchData)
    },
    fetchPolicy: 'no-cache',
  })

  const [searchMoveComponents] = useLazyQuery(SEARCH_BOOK_COMPONENTS, {
    fetchPolicy: 'no-cache',
  })

  const [getBookComponentChildren] = useLazyQuery(GET_BOOK_COMPONENT_CHILDREN, {
    fetchPolicy: 'network-only',
  })

  const [getMultipleBookComponentsById] = useLazyQuery(
    GET_MULTIPLE_BOOK_COMPONENT_CHILDREN,
    {
      fetchPolicy: 'network-only',
    },
  )
  // #endregion queries

  // #region mutations
  const mutationOptions = {
    refetchQueries: [
      {
        query: GET_DIVISION,
        variables: {
          id: currentDivisionId,
          skip,
          take: PAGE_SIZE,
        },
      },
    ],
  }

  const [moveAbove] = useMutation(MOVE_ABOVE, mutationOptions)

  const [moveBelow] = useMutation(MOVE_BELOW, mutationOptions)

  const [moveInside] = useMutation(MOVE_INSIDE, mutationOptions)

  const [repeatBookComponent] = useMutation(
    REPEAT_BOOK_COMPONENT,
    mutationOptions,
  )

  const [submitComponentsFn, { loading: isSubmitting }] = useMutation(
    SUBMIT_BOOK_COMPONENTS,
  )

  const [publishBookComponent, { loading: publishing }] = useMutation(
    BOOK_COMPONENT_PUBLISH,
    mutationOptions,
  )

  const [reconvertBookComponent, { loading: reconvertingBc }] = useMutation(
    RECONVERT_FILE,
    mutationOptions,
  )

  const [addPart, { loading: loadingAddPart }] = useMutation(
    NEW_PART,
    mutationOptions,
  )

  // TO DO -- break update settings & update metadata into separate mutations
  // they need different return values and different refetches (metadata does not need a refetch at all)
  const [updateBook, { loading: loadingUpdateBook }] = useMutation(
    UPDATE_BOOK,
    mutationOptions,
  )

  const [updateBookTeam, { loading: loadingUpdateBookTeams }] = useMutation(
    UPDATE_BOOK_TEAM,
  )

  const [uploadBookComponent] = useMutation(
    UPLOAD_CONVERT_FILE_OPTIMIZED,
    mutationOptions,
  )
  // #endregion mutations

  // #region effect
  useEffect(() => {
    subscribeToMore({
      document: CONVERT_SUBSCRIPTION,
      variables: { bookId },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev
        return null
      },
    })

    subscribeToMore({
      document: OBJECT_UPDATE_SUBSCRIPTION,
      variables: { id: bookId },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev

        refetch() // refetch division

        client.cache.evict({
          fieldName: SEARCH_BOOK_COMPONENTS.definitions[0].name.value,
        })

        return null
      },
    })
  })

  useEffect(() => {
    setCurrentPages({ ...currentPages, [activeTab]: skip / PAGE_SIZE + 1 })
  }, [skip])

  useEffect(() => {
    setTotalRecords(
      parseInt(
        showingSearch
          ? searchBookComponentsData.searchBookComponents.metadata.total
          : divisionData?.getDivision.bookComponents.metadata.total,
        10,
      ),
    )
  }, [divisionData, searchBookComponentsData])

  useEffect(() => {
    if (!showingSearch) {
      setSearchBookComponentsData(null)
    }
  }, [showingSearch])
  // #endregion effect

  // #region handlers
  const handleTabChange = async selectedTab => {
    setActiveTab(selectedTab)

    const divisionId = divisions.find(d => d.label === selectedTab).id
    const skipComps = PAGE_SIZE * (currentPages[selectedTab] - 1)

    const { data: fetchMoreData } = await fetchMoreDivision({
      variables: {
        id: divisionId,
        skip: skipComps,
        take: PAGE_SIZE,
      },
    })

    setSkip(skipComps)

    setTotalRecords(
      parseInt(fetchMoreData.getDivision.bookComponents.metadata.total, 10),
    )
  }

  const handlePageChange = async page => {
    const { currentPage } = page
    const skipComps = PAGE_SIZE * (currentPage - 1)

    if (showingSearch) {
      setChangingSearchPage(true)

      const { data: searchMoreData } = await searchMore({
        variables: {
          input: {
            skip: skipComps,
            take: PAGE_SIZE,
            search: {
              criteria: searchCriteria,
            },
          },
        },
      })

      setCurrentPages({ ...currentPages, search: currentPage })

      setAppliedSearchQueryParams({
        ...appliedSearchQueryParams,
        skip: skipComps,
      })

      setSearchBookComponentsData(searchMoreData)
      setChangingSearchPage(false)
    } else {
      setCurrentPages({ ...currentPages, [activeTab]: currentPage })

      const divisionId = divisions.find(d => d.label === activeTab).id

      const { data: fetchMoreData } = await fetchMoreDivision({
        variables: {
          id: divisionId,
          skip: PAGE_SIZE * (currentPage - 1),
          take: PAGE_SIZE,
        },
      })

      setSkip(skipComps)

      setTotalRecords(
        parseInt(fetchMoreData.getDivision.bookComponents.metadata.total, 10),
      )
    }
  }

  const handleAfterSearchAction = async () => {
    setChangingSearchPage(true)

    const { data: searchMoreData } = await searchMore({
      variables: {
        input: {
          skip: 0,
          take: PAGE_SIZE,
          search: {
            criteria: searchCriteria,
          },
        },
      },
    })

    setCurrentPages({ ...currentPages, search: 1 })

    setAppliedSearchQueryParams({
      ...appliedSearchQueryParams,
      skip: 0,
    })

    setSearchBookComponentsData(searchMoreData)
    setChangingSearchPage(false)
  }

  const handleReorder = (
    dragId,
    dropId,
    actionType,
    parentPartId = null,
    parentIdsToRefresh = [],
  ) => {
    return handleComponentReorder(
      dragId,
      dropId,
      actionType,
      parentPartId,
      parentIdsToRefresh,
      moveAbove,
      moveInside,
      moveBelow,
      getMultipleBookComponentsById,
    )
  }

  const handleMoveSearch = options => {
    return moveSearch({
      ...options,
      bookId,
      searchFn: searchMoveComponents,
    })
  }

  const handleSubmit = ids => {
    return submitComponentsFn({
      variables: {
        bookComponents: ids,
      },
    }).catch(e => {
      Notification({
        title: 'Error happened while submitting',
        message: e.message,
        type: 'danger',
      })
    })
  }

  const handlePublish = ids => {
    return publishBookComponent({
      variables: {
        bookComponent: ids,
      },
    }).catch(e => {
      Notification({
        type: 'danger',
        message: e.message || "Can't publish",
        title: 'Error',
      })
    })
  }

  const handleReconvert = ids => {
    return reconvertBookComponent({
      variables: {
        ids,
      },
    })
  }

  const handleRepeat = (componentId, partIds, parentIdsToRefresh) => {
    return handleComponentRepeat(
      bookId,
      componentId,
      partIds,
      parentIdsToRefresh,
      repeatBookComponent,
      getMultipleBookComponentsById,
    )
  }

  const handleSearch = () => {
    const criteria = [
      {
        field: 'bookId',
        operator: { eq: bookId },
      },
    ]

    searchQueryParams.filters.forEach(filter => {
      if (filter.name === 'division') {
        criteria.push({
          field: 'divisionId',
          operator: { eq: filter.value.value },
        })
      } else if (filter.name === 'status') {
        criteria.push({
          field: 'status',
          operator: {
            in: filter.value.map(obj => obj.value),
          },
        })
      } else if (
        filter.value &&
        (filter.name === 'lastUpdated' || filter.name === 'lastPublished')
      ) {
        const [startDate, endDate] = filter.value

        const fieldToQuery =
          filter.name === 'lastUpdated' ? 'updated' : 'publishedDate'

        if (startDate !== null) {
          const formattedStartDate = formatDateTime(startDate, 'start')

          criteria.push({
            field: fieldToQuery,
            operator: { ge: formattedStartDate },
          })
        }

        if (endDate !== null) {
          const formattedEndDate = formatDateTime(endDate, 'end')

          criteria.push({
            field: fieldToQuery,
            operator: { le: formattedEndDate },
          })
        }
      }
    })

    if (searchQueryParams.search) {
      criteria.push({
        field: 'search',
        operator: { containsAny: searchQueryParams.search },
      })
    }

    setSearchCriteria(criteria)
    setAppliedSearchQueryParams(searchQueryParams)

    return searchBookComponents({
      variables: {
        input: {
          skip: 0,
          take: PAGE_SIZE,
          search: {
            criteria,
          },
        },
      },
    })
  }

  const handleLoadNestedData = async node => {
    return getBookComponentChildren({
      variables: {
        id: node.id,
      },
    })
  }

  const handleAddPart = formData => {
    return addPart({
      variables: {
        id: bookId,
        input: formData,
      },
    })
      .then(() => {
        setSkip(0)
      })
      .catch(res => {
        console.error(res)

        Notification({
          message: 'Could not add part',
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const handleUpdateBook = input => {
    return updateBook({
      variables: {
        id: bookId,
        input,
      },
    }).catch(err => {
      console.error(err)

      Notification({
        message: `Could not update - ${err.message}`,
        type: 'danger',
        title: 'Error',
      })
    })
  }

  const handleUpload = fileList => {
    const files = fileList
      .filter(f => f.status !== 'uploaded')
      .map(file => ({ data: file }))

    return uploadBookComponent({
      variables: {
        files,
        id: bookId,
        category: 'source',
      },
    })
  }

  const handleUpdateBookTeams = teams => {
    const members = teams.map(team => ({
      id: team.id,
      team: {
        members: team.members.map(tm => ({
          status: tm.status,
          user: { id: tm.user.id },
        })),
      },
    }))

    return updateBookTeam({
      awaitRefetchQueries: true,
      refetchQueries: [
        {
          query: GET_BOOK,
          variables: { id: bookId },
        },
      ],
      variables: {
        input: members,
      },
    })
  }
  // #endregion handlers

  if (loading) return <Spinner label="Loading book..." pageLoader />

  // #region data wrangling
  const title = book?.title
  const settings = book?.settings
  const divisions = book?.divisions
  const metadata = book?.metadata
  const collection = book?.collection
  const workflow = book?.workflow
  const chapterIndependently = settings?.chapterIndependently
  const isOrderedManually = settings?.toc?.order_chapters_by === 'manual'
  const groupChaptersInParts = settings?.toc?.group_chapters_in_parts
  const tocComponent = book?.toc

  const body = book?.divisions.find(d => d.label === 'body')
  const frontMatter = book?.divisions.find(d => d.label === 'frontMatter')
  const backMatter = book?.divisions.find(d => d.label === 'backMatter')

  const divisionsMap = {
    body,
    frontMatter,
    backMatter,
  }

  const bodyDivisionId = body.id

  const chapterProcessedSourceType = collection
    ? !!collection?.metadata?.chapterProcessedSourceType
    : !!metadata?.sourceType

  const showPartContent = settings?.toc.add_body_to_parts

  const canDragAndDrop =
    (isSysAdmin || isOrgAdmin || isBookEditor) &&
    (activeTab === 'frontMatter' ||
      activeTab === 'backMatter' ||
      isOrderedManually ||
      groupChaptersInParts) // having parts means that dnd needs to be on, since parts are always draggable

  const canSelect = isSysAdmin || isOrgAdmin || isBookEditor || isPDF2XMLVendor
  const canAddPart = activeTab === 'body' && groupChaptersInParts

  const division = divisionData?.getDivision
  const divisionBookComponents = division?.bookComponents.results

  const currentDivisionBookComponentIds = currentDivision.fullBookComponents

  const treeData = transformData(
    divisionBookComponents,
    0,
    null,
    book,
    organizationId,
    client,
    false,
  )

  // used for select all functionality
  const treeDataKeyList = gatherKeys(treeData)
  const treeDataFlat = flatten(treeData)

  const search = searchBookComponentsData?.searchBookComponents
  const searchResults = search?.results
  const searchMetadata = search?.metadata

  const searchData = transformData(
    searchResults,
    0,
    null,
    book,
    organizationId,
    client,
    true,
  )

  const searchResultsKeyList = gatherKeys(searchData)
  const searchResultsFlat = flatten(searchData)
  // #endregion data wrangling

  if (chapterIndependently)
    return (
      <BookChapterProcessed
        activeTab={activeTab}
        allowedStatusesToMove={allowedStatusesToMove}
        appliedSearchQueryParams={appliedSearchQueryParams}
        bodyDivisionId={bodyDivisionId}
        canAddPart={canAddPart}
        canDragAndDrop={canDragAndDrop}
        canSelect={canSelect}
        chapterIndependently={chapterIndependently}
        chapterProcessedSourceType={chapterProcessedSourceType}
        collectionOptions={collectionOptions}
        currentDivisionBookComponentIds={currentDivisionBookComponentIds}
        currentPages={currentPages}
        data={data}
        divisions={divisions}
        divisionsMap={divisionsMap}
        isSubmitting={isSubmitting}
        loadingAddPart={loadingAddPart}
        loadingDivision={
          loadingDivision || !treeData || !treeDataFlat || !treeDataKeyList
        }
        loadingSearchResults={
          loadingSearchResults ||
          changingSearchPage ||
          !searchMetadata ||
          !searchResults
        }
        loadingUpdateBook={loadingUpdateBook}
        loadingUpdateBookTeams={loadingUpdateBookTeams}
        onAddPart={handleAddPart}
        onAfterSearchAction={handleAfterSearchAction}
        onLoadNestedData={handleLoadNestedData}
        onMoveSearch={handleMoveSearch}
        onPageChange={handlePageChange}
        onPublish={handlePublish}
        onReconvert={handleReconvert}
        onReorder={handleReorder}
        onRepeat={handleRepeat}
        onSearch={handleSearch}
        onSubmit={handleSubmit}
        onTabChange={handleTabChange}
        onUpdateBook={handleUpdateBook}
        onUpdateBookTeams={handleUpdateBookTeams}
        onUpload={handleUpload}
        pageSize={PAGE_SIZE}
        partsTree={currentDivisionNestedParts}
        publishing={publishing}
        reconvertingBc={reconvertingBc}
        searchMetadata={searchMetadata}
        searchQueryParams={searchQueryParams}
        searchResults={searchData}
        searchResultsFlat={searchResultsFlat}
        searchResultsKeyList={searchResultsKeyList}
        setAppliedSearchQueryParams={setAppliedSearchQueryParams}
        setSearchQueryParams={setSearchQueryParams}
        setShowingSearch={setShowingSearch}
        showPartContent={showPartContent}
        title={title}
        tocComponent={tocComponent}
        totalRecords={totalRecords}
        treeData={treeData}
        treeDataFlat={treeDataFlat}
        treeDataKeyList={treeDataKeyList}
        workflow={workflow}
      />
    )

  if (!chapterIndependently) {
    return (
      <WholeBookPage
        history={history}
        loadingUpdateBook={loadingUpdateBook}
        loadingUpdateBookTeams={loadingUpdateBookTeams}
        match={{ params: { bookId, organizationId } }}
        onUpdateBook={handleUpdateBook}
        onUpdateBookTeams={handleUpdateBookTeams}
      />
    )
  }

  return <div>unknown workflow</div>
}

export default BookManagerPage
