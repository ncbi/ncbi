import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useQuery, useMutation } from '@apollo/client'
import moment from 'moment'
import config from 'config'
import { TOCPage as TocUI } from '../../ui/Pages'
import Notification from '../../ui/components/Notification'

import {
  GET_TOC,
  GET_BOOK_COMPONENT_ERRORS,
  TOC_PUBLISH,
  CONVERT_SUBSCRIPTION,
} from '../graphql/toc'
// import CurrentUserContext from '../userContext'

const PUBLISHING = 'publishing'
const UNPUBLISHED = 'unpublished'
const PUBLISHFAILED = 'publish-failed'

const getMessage = (status, isCollection) => {
  if (status === PUBLISHING)
    return 'Live view will display when TOC loads successfully'
  if (status === UNPUBLISHED)
    return isCollection
      ? 'Not available. Publish to generate live view.'
      : 'To create a table of contents page for the book, chapters must be published. This page is updated with the published table of contents page on Bookshelf only once overnight when new chapters are published.'

  if (status === PUBLISHFAILED) {
    return 'Not available.  Resolve errors to generate the live view.'
  }

  return null
}

const Toc = () => {
  const { collectionId, bookId, id } = useParams()

  //    Queries
  const { loading, data, subscribeToMore } = useQuery(GET_TOC, {
    variables: { id },
  })

  const objectId = data?.getToc?.id

  const {
    loading: loadingErrors,
    data: errors,
    refetch: refetchErrors,
  } = useQuery(GET_BOOK_COMPONENT_ERRORS, {
    skip: !data?.getToc.id,
    variables: {
      objectId,
      showHistory: true,
    },
  })

  useEffect(() => {
    bookId &&
      subscribeToMore({
        document: CONVERT_SUBSCRIPTION,
        updateQuery: (prev, { subscriptionData }) => {
          refetchErrors()
          if (!subscriptionData.data) return prev
          return null
        },
        variables: { bookId },
      })
  })

  // Mutations
  const [publishTocs, { loading: isUpdatingToc }] = useMutation(TOC_PUBLISH, {
    refetchQueries: [
      {
        query: GET_BOOK_COMPONENT_ERRORS,
        variables: {
          objectId,
          showHistory: true,
        },
      },
    ],
  })

  const previewLink = data?.getToc.metadata?.url
  const isCollection = !!collectionId

  const disablePublishToc = !data?.getToc?.canBePublished

  const status = data?.getToc.status
  const errorData = errors?.getErrors || []

  const lastPublishedDate =
    data?.getToc.publishedDate &&
    moment
      .utc(data?.getToc.publishedDate)
      .utcOffset(moment().utcOffset())
      .format('L LT')

  // const { orgAdminForOrganisation, isSysAdmin } = currentUser.auth

  // const isOrgAdmin = orgAdminForOrganisation.includes(organizationId)

  // const canViewTabs = isSysAdmin || isOrgAdmin

  const handlePublishToc = () =>
    publishTocs({
      variables: {
        toc: [data?.getToc.id],
      },
    }).catch(e => {
      Notification({
        type: 'danger',
        message: e.message || "Can't republish TOC",
        title: 'An error happened!',
      })
    })

  const handleDownloadToc = () => {
    const serverUrl = config['pubsweet-client'].baseUrl

    const link = bookId
      ? `${serverUrl}/downloadTocXml?id=${bookId}`
      : `${serverUrl}/downloadCollectionXml?id=${collectionId}`

    window.location.href = link
  }

  const disableDownloadToc = (data?.getToc.tocFiles || []).length === 0

  return (
    <TocUI
      disableDownloadToc={disableDownloadToc}
      disablePublishToc={disablePublishToc}
      errorData={errorData}
      handleDownloadToc={handleDownloadToc}
      handleRepublishToc={handlePublishToc}
      isCollection={isCollection}
      isUpdatingToc={isUpdatingToc}
      lastPublishedDate={lastPublishedDate}
      loading={loading || loadingErrors}
      noPreviewMessage={getMessage(status, !!collectionId)}
      previewLink={previewLink}
      status={status}
    />
  )
}

export default Toc
