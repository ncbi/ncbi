import cloneDeep from 'lodash/cloneDeep'
import get from 'lodash/get'
import set from 'lodash/set'
import isNil from 'lodash/isNil'

import { reverseMapperCreator } from './conversionHelpers'
import Granthub from './Granthub'
import { createImageUrl } from './imageUrl'

const nestedObjectList = ['collection', 'publisher']

const metadataMapper = {
  abstract: 'abstract',
  abstractGraphic: 'fileAbstract',
  alternativeTitle: 'altTitle',
  authors: 'metadata.author',
  objectId: 'alias',
  bookSourceType: 'metadata.sourceType',
  bookSubmitId: 'bookSubmitId',
  collaborativeAuthors: 'metadata.collaborativeAuthors',
  copyrightStatement: 'metadata.copyrightStatement',
  cover: 'fileCover',
  dateCreated: 'metadata.dateCreated',
  dateOfPublication: 'metadata.pubDate.dateRange',
  datePublicationFormat: 'metadata.pubDate.publicationFormat',
  dateType: 'metadata.pubDate.dateType',
  dateUpdated: 'metadata.dateUpdated',
  doi: 'metadata.doi',
  domain: 'domain',
  edition: 'edition',
  editors: 'metadata.editor',
  funding: 'metadata.grants',
  isbn: 'metadata.isbn',
  issn: 'metadata.issn',
  licenseStatement: 'metadata.licenseStatement',
  licenseType: 'metadata.licenseType',
  licenseUrl: 'metadata.licenseUrl',
  notes: 'metadata.notes',
  openAccessLicense: 'metadata.openAccessLicense',
  publisherLocation: 'metadata.pub_loc',
  publisherName: 'metadata.pub_name',
  subtitle: 'subTitle',
  title: 'title',
  volume: 'metadata.volume',

  // language
  // nlmId
}

const settingsMapper = {
  addBodyToParts: 'settings.toc.add_body_to_parts',
  bookLevelAffiliationStyle: 'settings.bookLevelAffiliationStyle',
  bookLevelLinks: 'settings.bookLevelLinks',
  bookLevelLinksMarkdown: 'settings.bookLevelLinksMarkdown',
  chapterLevelAffiliationStyle: 'settings.chapterLevelAffiliationStyle',
  citationSelfUrl: 'settings.citationSelfUrl',
  citationType: 'settings.citationType',
  collection: 'collection',
  contentType: 'fundedContentType',
  conversionWorkflow: 'workflow',
  createBookLevelPdf: 'settings.createWholebookPdf',
  createChapterLevelPdf: 'settings.createPdf',
  createVersionLink: 'settings.createVersionLink',
  displayBookLevelPdf: 'settings.alternateVersionsPdfBook',
  displayChapterLevelPdf: 'settings.alternateVersionsPdf',
  displayHeadingLevel: 'settings.toc.tocMaxDepth',
  displayObjectsLocation: 'settings.displayObjectsLocation',
  footnotesDecimal: 'settings.footnotesDecimal',
  groupChaptersInParts: 'settings.toc.group_chapters_in_parts',
  indexChaptersInPubmed: 'settings.indexChaptersInPubmed',
  indexInPubmed: 'settings.indexInPubmed',
  orderChaptersBy: 'settings.toc.order_chapters_by',
  publisher: 'settings.publisher',
  publisherUrl: 'settings.publisherUrl',
  questionAnswerStyle: 'settings.questionAnswerStyle',
  referenceListStyle: 'settings.referenceListStyle',
  requireApprovalByOrgAdminOrEditor: 'settings.approvalOrgAdminEditor',
  requireApprovalByPreviewers: 'settings.approvalPreviewer',
  specialPublisherLinkText: 'settings.specialPublisherLinkText',
  specialPublisherLinkUrl: 'settings.specialPublisherLinkUrl',
  supportMultiplePublishedVersions: 'settings.multiplePublishedVersions',
  tocAltTitle: 'settings.toc.allTitle',
  tocChapterLevelDates: 'settings.toc.documentHistory',
  tocContributors: 'settings.toc.contributors',
  tocExpansionLevel: 'settings.tocExpansionLevel',
  tocSubtitle: 'settings.toc.subtitle',
  UKPMC: 'settings.UKPMC',
  versionLinkText: 'settings.versionLinkText',
  versionLinkUri: 'settings.versionLinkUri',
  xrefAnchorStyle: 'settings.xrefAnchorStyle',
  openAccessStatus: 'metadata.openAccess',
}
// qaStatus: null,
// releaseStatus: false,

const converter = (data, mapper, reverse) => {
  const values = cloneDeep(data)
  const output = {}

  Object.keys(reverse ? mapper : values).forEach(key => {
    const mappedKey = mapper[key]

    if (mappedKey) {
      const value = get(values, key)
      set(output, mappedKey, value)
    }
  })

  return output
}

/**
 * As it exists in the book settings form => As it is valid in the API
 */
export const bookSettingsToBookAPI = values => {
  const input = {}

  Object.keys(values).forEach(key => {
    let mappedKey = settingsMapper[key]
    if (key === 'collection') mappedKey = 'collectionId'

    if (mappedKey) {
      const value = values[key]
      set(input, mappedKey, value)
    }
  })

  return input
}

/**
 * As it is coming in from the API => As it is valid in the book settings form
 */
export const bookAPIToBookSettings = values => {
  const thisMapper = reverseMapperCreator(settingsMapper)

  const formValues = {}

  Object.keys(thisMapper).forEach(key => {
    const value = get(values, key)

    const mappedKey = thisMapper[key]

    // Some fields will return an object
    // eg. publisher = { id: '1', ...otherProperties }
    // Capture the id and pass it down
    if (nestedObjectList.includes(mappedKey) && value && value.id) {
      formValues[mappedKey] = value.id
    } else {
      formValues[mappedKey] = value
    }
  })

  const { chapterIndependently } = values.settings

  if (!isNil(chapterIndependently)) {
    if (chapterIndependently) {
      formValues.submissionType = 'chapterProcessed'
    } else {
      formValues.submissionType = 'wholeBook'
    }
  }

  return formValues
}

/**
 * As it exists in the book metadata form => As it is valid in the API
 */
export const bookMetadataToBookAPI = async (values, page) => {
  const modifiedValues = { ...values }
  const mapper = { ...metadataMapper }

  if (modifiedValues.cover) delete modifiedValues.cover.url

  if (modifiedValues.notes) {
    modifiedValues.notes = modifiedValues.notes.filter(
      note => !note.fromCollection,
    )
  }

  if (modifiedValues.publicationDateType) {
    const [
      dateType,
      datePublicationFormat,
    ] = modifiedValues.publicationDateType.split('-')

    modifiedValues.dateType = dateType
    modifiedValues.datePublicationFormat = datePublicationFormat
    if (dateType === 'pub') mapper.dateOfPublication = 'metadata.pubDate.date'
    delete modifiedValues.publicationDateType
  }

  if (modifiedValues.funding) {
    modifiedValues.funding = modifiedValues.funding.filter(
      grant => !grant.appliedFromParent,
    )

    modifiedValues.funding = await Granthub.getAwardsForAPI(
      modifiedValues.funding,
      page,
    )
  }

  return converter(modifiedValues, mapper)
}

/**
 * As it exists in the API => As it is valid in the book metadata form
 */
export const bookAPIToBookMetadata = values => {
  const data = cloneDeep(values)
  const isChapterProcessed = data.settings.chapterIndependently
  const isWholeBook = !isChapterProcessed
  const bookNotes = data.metadata.notes || []

  const appliedCollectionNotes =
    (data.workflow === 'pdf' &&
      data.collection?.metadata.notes
        .filter(note => note.applyNotePdf)
        .map(note => ({
          type: note.type,
          description: note.description,
          title: note.title,
          fromCollection: true,
        }))) ||
    []

  data.metadata.notes = [...bookNotes, ...appliedCollectionNotes]

  const reverseMapper = reverseMapperCreator(metadataMapper)

  if (data.metadata.pubDate.dateType === 'pub') {
    reverseMapper['metadata.pubDate.date'] = 'dateOfPublication'
    delete reverseMapper['metadata.pubDate.dateRange']
  }

  const transformed = converter(data, reverseMapper, true)

  const applyCoverToBooks = data.collection?.metadata.applyCoverToBooks

  if (applyCoverToBooks) {
    transformed.cover = data.collection.fileCover
  }

  const applyPublisher = data.collection?.metadata.applyPublisherToBooks

  if (applyPublisher && isChapterProcessed) {
    transformed.publisherLocation = data.collection.metadata.pub_loc
    transformed.publisherName = data.collection.metadata.pub_name
  }

  const applyPermissions = data.collection?.metadata.applyPermissionsToBooks

  if (applyPermissions && data.workflow === 'pdf') {
    transformed.copyrightStatement = data.collection.metadata.copyrightStatement
    transformed.licenseStatement = data.collection.metadata.licenseStatement
    transformed.licenseType = data.collection.metadata.licenseType
    transformed.licenseUrl = data.collection.metadata.licenseUrl
    transformed.openAccessLicense = data.collection.metadata.openAccessLicense
  }

  if (data.collection) {
    if (isChapterProcessed)
      transformed.bookSourceType =
        data.collection.metadata.chapterProcessedSourceType

    if (isWholeBook)
      transformed.bookSourceType = data.collection.metadata.wholeBookSourceType
  }

  if (!transformed.dateType || !transformed.datePublicationFormat) {
    transformed.publicationDateType = null
  } else {
    transformed.publicationDateType = `${transformed.dateType}-${transformed.datePublicationFormat}`
  }

  if (isWholeBook && data.collection?.metadata.grants?.length > 0) {
    const appliedGrants = data.collection.metadata.grants
      .filter(g => g.apply)
      .map(g => ({
        ...g,
        appliedFromParent: true,
      }))

    transformed.funding = [...appliedGrants, ...transformed.funding]
  }

  delete transformed.dateType
  delete transformed.datePublicationFormat

  if (transformed.cover) {
    transformed.cover.url = createImageUrl(transformed.cover.id, 'cover')
  }

  if (transformed.abstractGraphic) {
    transformed.abstractGraphic.url = createImageUrl(
      transformed.abstractGraphic.id,
      'abstract',
    )
  }

  return transformed
}
