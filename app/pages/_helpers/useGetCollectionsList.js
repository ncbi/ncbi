import uniqBy from 'lodash/uniqBy'
import { useQuery } from '@apollo/client'
import { GET_FUNDED_COLLECTIONS_OF_OTHER_ORGANIZATIONS } from './bookComponentQuery'

const useGetCollections = ({
  organization: urlOrganization,
  bookOrganization,
  differentOrganizations,
}) => {
  const currentOrgId = bookOrganization
    ? bookOrganization?.id
    : urlOrganization?.id

  const currentOrgType = bookOrganization
    ? bookOrganization?.settings?.type?.publisher
    : urlOrganization?.settings?.type?.publisher

  const { data: fundedCollectionsData } = useQuery(
    GET_FUNDED_COLLECTIONS_OF_OTHER_ORGANIZATIONS,
    {
      variables: {
        organisationId: currentOrgId,
      },
      skip: !currentOrgType,
    },
  )

  const fundedCollections =
    fundedCollectionsData?.getFundedCollectionsOfOtherOrganisations

  let collectionOptions = []

  if (urlOrganization) {
    collectionOptions = urlOrganization?.collections.map(collection => ({
      value: collection.id,
      label: collection.title,
      organization: urlOrganization?.name,
      isOwnCollection: !differentOrganizations,
      tag: urlOrganization?.name,
    }))

    if (fundedCollections) {
      const transformedFundedCollections = fundedCollections
        .filter(
          x =>
            x.collectionType === 'funded' || x.collectionType === 'bookSeries',
        )
        .map(collection => ({
          value: collection.id,
          label: collection.title,
          organization: collection.organisation.name,
          isOwnCollection: false,
          tag: collection.organisation.name,
        }))

      collectionOptions = collectionOptions.concat(transformedFundedCollections)
    }

    if (bookOrganization) {
      const collection2Options = bookOrganization.collections.map(
        collection => ({
          value: collection.id,
          label: collection.title,
          organization: bookOrganization?.name,
          isOwnCollection: false,
          tag: bookOrganization?.getOrganisation?.name,
        }),
      )

      collectionOptions = collectionOptions.concat(collection2Options)
    }
  }

  collectionOptions = uniqBy(collectionOptions, e => e.value)

  return collectionOptions
}

export default useGetCollections
