const GRANTHUB_API = 'https://api.ncbi.nlm.nih.gov/granthub'

const getStorageLocation = page => `granthubSearch-${page}`

const search = (searchValue, page) => {
  const searchUrl = `${GRANTHUB_API}/collapsed/?award.number=${searchValue}`

  return fetch(searchUrl)
    .then(res => {
      return res.json()
    })
    .then(result => {
      const searchData = []

      const prevValue =
        JSON.parse(localStorage.getItem(getStorageLocation(page))) || '[]'

      result.body.forEach(item => {
        item.awards.forEach(award => {
          const value = {
            number: award.number,
            institution_acronym: award.funding_institution.acronym,
            institution_code: award.funding_institution.code,
            institution_name: award.funding_institution.name,
          }

          searchData.push(value)
        })
      })

      const duplicateValue = [...searchData, ...prevValue]

      const uniqueValue = [
        ...new Map(duplicateValue.map(item => [item.number, item])).values(),
      ]

      localStorage.setItem(
        getStorageLocation(page),
        JSON.stringify(uniqueValue),
      )

      const searchResults = searchData.map(i => i.number)
      return searchResults
    })
    .catch(e => console.error(e))
}

/**
 * Takes awards as they exist in the form, and attaches the country field, as
 * well as the institution fields saved when searching
 */
const getAwardsForAPI = async (awards, page) => {
  if (!Array.isArray(awards)) return []
  const useApply = page === 'collectionManager' || page === 'bookManager'

  const storedResults = JSON.parse(
    localStorage.getItem(getStorageLocation(page)),
  )

  const modifiedAwards = await Promise.all(
    awards.map(award => {
      // has all fields populated from a previous save
      if (award.country) {
        if (useApply) return award

        const { apply, ...otherFields } = award
        return { ...otherFields }
      }

      const awardToSave = storedResults.find(a => a.number === award.number)

      awardToSave.id = award.id // BCMS uuid, not the award number

      if (useApply) {
        awardToSave.apply = award.apply
      }

      const url = `${GRANTHUB_API}/fundinginstitution/?funding_institution.code=${awardToSave.institution_code}`

      return fetch(url)
        .then(res => res.json())
        .then(result => {
          if (!result.body.length > 0)
            throw new Error('Granthub API: No results for institution!')

          awardToSave.country = result.body[0].country
          return awardToSave
        })
        .catch(e => console.error(e))
    }),
  )

  return modifiedAwards
}

export default { search, getAwardsForAPI }
