// eslint-disable-next-line import/prefer-default-export
export const getAccessedOrganizations = currentUser => {
  const accessedORgs = currentUser.organisations.filter(x => {
    const userRole = x.teams.find(y => y.role === 'user')

    const member = userRole?.members.find(mem => mem.user.id === currentUser.id)

    return member.status === 'enabled'
  })

  return accessedORgs
}
