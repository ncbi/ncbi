import get from 'lodash/get'
import set from 'lodash/set'

import { reverseMapperCreator } from './conversionHelpers'
import Granthub from './Granthub'

const metadataMapper = {
  altTitle: 'metadata.alt_title',
  authors: 'metadata.author',
  objectId: 'alias',
  chapterId: 'metadata.book_component_id',
  collaborativeAuthors: 'metadata.collaborativeAuthors',
  dateCreated: 'metadata.date_created',
  dateOfPublication: 'metadata.date_publication',
  dateRevised: 'metadata.date_revised',
  dateUpdated: 'metadata.date_updated',
  editors: 'metadata.editor',
  funding: 'metadata.grants',
  label: 'metadata.chapter_number',
  language: 'metadata.language',
  subTitle: 'metadata.sub_title',
  title: 'title',
}

const settingsMapper = {
  openAccess: 'metadata.openAccess',
}

const converter = (values, mapper, reverse) => {
  const output = {}

  Object.keys(reverse ? mapper : values).forEach(key => {
    const mappedKey = mapper[key]

    if (mappedKey) {
      const value = get(values, key)
      set(output, mappedKey, value)
    }
  })

  return output
}

/**
 * As it exists in the component settings form => As it is valid in the API
 */
const componentSettingsToComponentAPI = values => {
  return converter(values, settingsMapper)
}

/**
 * As it exists in the API => As it is valid in the component settings form
 */
const componentAPIToComponentSettings = values => {
  const reverseMapper = reverseMapperCreator(settingsMapper)
  return converter(values, reverseMapper, true)
}

/**
 * As it exists in the component metadata form => As it is valid in the API
 */
const componentMetadataToComponentAPI = async (values, page) => {
  const modifiedValues = { ...values }

  if (modifiedValues.publicationDateType) {
    const [
      dateType,
      datePublicationFormat,
    ] = modifiedValues.publicationDateType.split('-')

    modifiedValues.dateType = dateType
    modifiedValues.datePublicationFormat = datePublicationFormat
    delete modifiedValues.publicationDateType
  }

  if (modifiedValues.funding) {
    modifiedValues.funding = modifiedValues.funding.filter(
      grant => !grant.appliedFromParent,
    )

    modifiedValues.funding = await Granthub.getAwardsForAPI(
      modifiedValues.funding,
      page,
    )
  }

  return converter(modifiedValues, metadataMapper)
}

/**
 * As it exists in the API => As it is valid in the component metadata form
 */
const componentAPIToComponentMetadata = (values, book) => {
  const reverseMapper = reverseMapperCreator(metadataMapper)
  const transformed = converter(values, reverseMapper, true)

  if (!transformed.dateType || !transformed.datePublicationFormat) {
    transformed.publicationDateType = null
  } else {
    transformed.publicationDateType = `${transformed.dateType}-${transformed.datePublicationFormat}`
  }

  delete transformed.dateType
  delete transformed.datePublicationFormat

  if (!transformed.funding) transformed.funding = []

  const appliedGrants =
    book.metadata.grants
      ?.filter(g => g.apply)
      .map(g => ({
        ...g,
        appliedFromParent: true,
      })) || []

  transformed.funding = [...appliedGrants, ...transformed.funding]

  return transformed
}

export {
  componentSettingsToComponentAPI,
  componentAPIToComponentSettings,
  componentMetadataToComponentAPI,
  componentAPIToComponentMetadata,
}
