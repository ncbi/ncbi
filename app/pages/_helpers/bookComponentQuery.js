import { gql } from '@apollo/client'

const GET_BOOK_COMPONENT = gql`
  query getBookComponent($id: ID!) {
    getBookComponent(id: $id) {
      id
    }
  }
`

const GET_BOOK = gql`
  query getBook($id: ID!, $divisionLabels: [String!]) {
    getBook(id: $id) {
      id
      bookComponents(divisionLabels: $divisionLabels) {
        id
      }
    }
  }
`

const GET_FUNDED_COLLECTIONS_OF_OTHER_ORGANIZATIONS = gql`
  query GetFundedCollectionsOfOtherOrganizations($organisationId: ID!) {
    getFundedCollectionsOfOtherOrganisations(organisationId: $organisationId) {
      id
      collectionType
      title
      organisation {
        id
        name
      }
    }
  }
`

export {
  GET_BOOK,
  GET_BOOK_COMPONENT,
  GET_FUNDED_COLLECTIONS_OF_OTHER_ORGANIZATIONS,
}
