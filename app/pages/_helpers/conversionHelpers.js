const reverseMapperCreator = original =>
  Object.keys(original).reduce((obj, currentKey) => {
    const currentValue = original[currentKey]
    /* eslint-disable-next-line no-param-reassign */
    obj[currentValue] = currentKey
    return obj
  }, {})

/* eslint-disable-next-line import/prefer-default-export */
export { reverseMapperCreator }
