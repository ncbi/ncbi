/* eslint-disable import/prefer-default-export */

import { serverUrl } from '../../utilsConfig'

export const createImageUrl = (id, category) =>
  `${serverUrl}/getImage?id=${id}&category=${category}`
