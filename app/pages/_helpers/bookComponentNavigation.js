import { cloneDeep, forEach, sortBy } from 'lodash'

export const bcListFlatDivison = (data, divisionId) => {
  const dt = cloneDeep(
    data?.bookComponents.filter(x => x.divisionId === divisionId) || [],
  )

  const division = data?.divisions.find(x => x.id === divisionId)

  const sortedDivisionBc = division?.bookComponents
    .map(bcID => dt.find(x => x.id === bcID))
    .filter(x => x.divisionId === divisionId)

  const returnArr = []

  forEach(sortedDivisionBc, item => {
    returnArr.push(item.id)

    // division book compoennts
    if (item.componentType === 'part') {
      if (item.bookComponents.length) {
        // part level 1  book compoennts
        const constainigBc = item.bookComponents.map(bcID =>
          dt.find(x => x.id === bcID),
        )

        forEach(constainigBc, item2 => {
          returnArr.push(item2.id)

          if (item2.componentType === 'part') {
            if (item2.bookComponents.length) {
              // part level 2  book compoennts
              returnArr.push(...item2.bookComponents)
            }
          }
        })
      }
    }
  })

  return returnArr
}

export const bookComponentsFlatList = ({ book, divisionId }) => {
  const returnArr = []
  const division = book?.divisions.find(x => x.id === divisionId)

  const sortedDivisionBC = division?.bookComponents
    .map(bcID => book?.bookComponents.find(x => x.id === bcID))
    .filter(x => x.divisionId === divisionId)

  forEach(sortedDivisionBC, item => {
    returnArr.push({ parentId: divisionId, ...item })

    // division book components
    if (item.componentType === 'part' && item.bookComponents.length > 0) {
      // part level 1  book components
      const partBookComponents = book.bookComponents.filter(x =>
        item.bookComponents.includes(x.id),
      )

      const sortedPartBookComponents = sortBy(partBookComponents, i => {
        return item.bookComponents.indexOf(i.id)
      })

      forEach(sortedPartBookComponents, item2 => {
        returnArr.push({ parentId: item.id, ...item2 })

        if (item2.componentType === 'part' && item2.bookComponents.length > 0) {
          const nestedPartsL2 = book.bookComponents
            .filter(x => item2.bookComponents.includes(x.id))
            .map(x => ({
              parentId: item2.id,
              ...x,
            }))

          const sortedPartBookComponentsL2 = sortBy(nestedPartsL2, i => {
            return item2.bookComponents.indexOf(i.id)
          })

          // part level 2  book compoennts
          returnArr.push(...sortedPartBookComponentsL2)
        }
      })
      // }
    }
  })

  return returnArr
}

export const getNavigationComponents = ({
  book,
  divisionId,
  bookComponentVersionId,
  partId,
}) => {
  const flatOrderedBookComponents = cloneDeep(
    bookComponentsFlatList({
      book,
      divisionId,
    }),
  )

  const itemsReturn = flatOrderedBookComponents.map(x => ({
    id: x.id,
    title: x.title,
    bookComponentVersionId: x.bookComponentVersionId,
    partId: x.parentId,
    metadata: {
      filename: x.metadata?.filename,
    },
  }))

  let nextComp = null
  let prevComp = null

  const index = !partId
    ? itemsReturn?.findIndex(
        x => x.bookComponentVersionId === bookComponentVersionId,
      )
    : itemsReturn?.findIndex(
        x =>
          x.bookComponentVersionId === bookComponentVersionId &&
          x.partId === partId,
      )

  const currentComp = itemsReturn[index]

  nextComp = index + 1 < itemsReturn.length ? itemsReturn[index + 1] : null

  prevComp = index - 1 > -1 ? itemsReturn[index - 1] : null

  return { prevComp, currentComp, nextComp }
}
