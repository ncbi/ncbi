import get from 'lodash/get'
import set from 'lodash/set'
import cloneDeep from 'lodash/cloneDeep'

import { reverseMapperCreator } from './conversionHelpers'
import Granthub from './Granthub'
import { createImageUrl } from './imageUrl'

const metadataMapper = {
  abstract: 'abstract',
  applyCoverToBooks: 'metadata.applyCoverToBooks',
  applyPermissions: 'metadata.applyPermissionsToBooks',
  applyPublisherToBooks: 'metadata.applyPublisherToBooks',
  chapterProcessedSourceType: 'metadata.chapterProcessedSourceType',
  collaborativeAuthors: 'metadata.author',
  copyrightStatement: 'metadata.copyrightStatement',
  cover: 'fileCover',
  dateOfPublication: 'metadata.pubDate.dateRange',
  datePublicationFormat: 'metadata.pubDate.publicationFormat',
  dateType: 'metadata.pubDate.dateType',
  domain: 'domain',
  funding: 'metadata.grants',
  issn: 'metadata.issn',
  eissn: 'metadata.eissn',
  licenseStatement: 'metadata.licenseStatement',
  licenseType: 'metadata.licenseType',
  licenseUrl: 'metadata.licenseUrl',
  notes: 'metadata.notes',
  openAccessLicense: 'metadata.openAccessLicense',
  publisherBookSeriesTitle: 'metadata.publisherBookSeriesTitle',
  publisherLocation: 'metadata.pub_loc',
  publisherName: 'metadata.pub_name',
  seriesEditors: 'metadata.editor',
  sourceType: 'metadata.sourceType',
  title: 'title',
  wholeBookSourceType: 'metadata.wholeBookSourceType',
}

const settingsMapper = {
  citationSelfUrl: 'settings.citationSelfUrl',
  groupBooksBy: 'settings.groupBooksBy',
  groupBooksOnTOC: 'settings.groupBooks',
  openAccess: 'metadata.openAccess',
  orderBooksBy: 'settings.orderBooksBy',
  publisherUrl: 'settings.publisherUrl',
  requireApprovalByOrgAdminOrEditor: 'settings.approvalOrgAdminEditor',
  tocContributors: 'settings.toc.contributors',
  tocFullBookCitations: 'settings.toc.fullBookCitations',
  UKPMC: 'settings.UKPMC',
}

const converter = (data, mapper, reverse) => {
  const values = cloneDeep(data)
  const output = {}

  Object.keys(reverse ? mapper : values).forEach(key => {
    const mappedKey = mapper[key]

    if (mappedKey) {
      const value = get(values, key)
      set(output, mappedKey, value)
    }
  })

  return output
}

/**
 * As it exists in the collection settings form => As it is valid in the API
 */
const collectionSettingsToCollectionAPI = values => {
  return converter(values, settingsMapper)
}

/**
 * As it exists in the API => As it is valid in the collection settings form
 */
const collectionAPIToCollectionSettings = values => {
  const reverseMapper = reverseMapperCreator(settingsMapper)
  return converter(values, reverseMapper, true)
}

/**
 * As it exists in the collection metadata form => As it is valid in the API
 */
const collectionMetadataToCollectionAPI = async (values, page) => {
  const modifiedValues = { ...values }

  if (modifiedValues.cover) delete modifiedValues.cover.url

  if (modifiedValues.publicationDateType) {
    const [
      dateType,
      datePublicationFormat,
    ] = modifiedValues.publicationDateType.split('-')

    modifiedValues.dateType = dateType
    modifiedValues.datePublicationFormat = datePublicationFormat
    delete modifiedValues.publicationDateType
  }

  if (modifiedValues.funding) {
    modifiedValues.funding = await Granthub.getAwardsForAPI(
      modifiedValues.funding,
      page,
    )
  }

  return converter(modifiedValues, metadataMapper)
}

/**
 * As it exists in the API => As it is valid in the collection metadata form
 */
const collectionAPIToCollectionMetadata = values => {
  const reverseMapper = reverseMapperCreator(metadataMapper)
  const transformed = converter(values, reverseMapper, true)

  if (!transformed.dateType || !transformed.datePublicationFormat) {
    transformed.publicationDateType = null
  } else {
    transformed.publicationDateType = `${transformed.dateType}-${transformed.datePublicationFormat}`
  }

  delete transformed.dateType
  delete transformed.datePublicationFormat

  if (transformed.cover) {
    transformed.cover.url = createImageUrl(transformed.cover.id, 'cover')
  }

  return transformed
}

export {
  collectionSettingsToCollectionAPI,
  collectionAPIToCollectionSettings,
  collectionMetadataToCollectionAPI,
  collectionAPIToCollectionMetadata,
}
