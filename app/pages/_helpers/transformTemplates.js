/* eslint-disable import/prefer-default-export */

import { bookAPIToBookSettings } from './bookSettingsToApiConverter'

export const transformTemplatesToUIData = templates => {
  if (!templates) return null
  const bookSettingsTemplates = {}

  templates.forEach(template => {
    const {
      values,
      id,
      isCustom,
      templateType,
      domainId,
      domainName,
    } = template

    bookSettingsTemplates[templateType] = {
      values: bookAPIToBookSettings(values),
      id,
      isCustom,
      templateType,
      domainId,
      domainName,
    }
  })

  return bookSettingsTemplates
}
