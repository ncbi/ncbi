import React, { useState, useContext } from 'react'
import { useQuery, useMutation } from '@apollo/client'
import { useParams } from 'react-router-dom'
import cloneDeep from 'lodash/cloneDeep'
import omit from 'lodash/omit'

import {
  GET_COLLECTION,
  COLLECTION_TEMPLATES,
  CREATE_BOOK_SETTINGS_TEMPLATE,
  UPDATE_BOOK_SETTINGS_TEMPLATE,
  UPDATE_COLLECTION,
  UPDATE_CUSTOM_GROUP_ORDER,
  SUBMIT_BOOK_COMPONENTS,
  BOOK_COMPONENT_PUBLISH,
  RECONVERT_FILE,
} from '../graphql/collectionManager'

import CollectionManagerUI from '../../ui/Pages/Collectionmanager/CollectionManagerPage'
import {
  transformTemplatesToUIData,
  bookSettingsToBookAPI,
  Granthub,
} from './_helpers'
import UserContext from '../userContext'
import {
  collectionAPIToCollectionSettings,
  collectionSettingsToCollectionAPI,
  collectionAPIToCollectionMetadata,
  collectionMetadataToCollectionAPI,
} from './_helpers/collectionSettingsToApiConverter'
import { Notification, Information } from '../../ui/components'
import PermissionsGate from '../components/PermissionsGate'

import {
  disableReloadPreview,
  disabledSubmit,
  disablePublish,
  areFilesTaged,
} from '../../ui/common/statusActions'

import { canPublishMultiple } from '../userRights'

const PAGE = 'collectionManager'

const CollectionManager = () => {
  const [
    collectionSourceTypeModal,
    setCollectionSourceTypeModalIsOpen,
  ] = useState(false)

  const { currentUser } = useContext(UserContext)
  const { collectionId, organizationId } = useParams()

  const templatesQueryVariables = {
    organizationId,
    collectionId,
  }

  // #region QUERIES
  const { data, loading } = useQuery(GET_COLLECTION, {
    variables: {
      id: collectionId,
    },
  })

  const {
    data: templatesData,
    loading: templatesLoading,
  } = useQuery(COLLECTION_TEMPLATES, { variables: templatesQueryVariables })
  // #endregion QUERIES

  // #region MUTATIONS
  const [updateCollection, { loading: isUpdatingCollection }] = useMutation(
    UPDATE_COLLECTION,
  )

  const updateCollectionFn = values => {
    return updateCollection({
      variables: {
        id: collectionId,
        input: values,
      },
    })
  }

  const saveCollectionSettings = values => {
    const transformedValues = collectionSettingsToCollectionAPI(values)
    return updateCollectionFn(transformedValues)
      .then(() => {
        Notification({
          message: 'Successfully updated settings',
          type: 'success',
          title: 'Success',
        })
      })
      .catch(res => {
        console.error(res)

        Notification({
          message: 'Could not update',
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const saveCollectionMetadata = async values => {
    let submittedValues = cloneDeep(values)

    if (
      values.cover?.id &&
      values.cover.id === data?.getCollection.fileCover?.id
    ) {
      submittedValues = omit(submittedValues, 'cover')
    }

    const transformedValues = await collectionMetadataToCollectionAPI(
      submittedValues,
      PAGE,
    )

    return updateCollectionFn(transformedValues)
      .then(() => {
        Notification({
          message: 'Successfully updated metadata',
          type: 'success',
          title: 'Success',
        })
      })
      .catch(res => {
        console.error(res)

        Notification({
          message: 'Could not update',
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const [updateCollectionOrder] = useMutation(UPDATE_CUSTOM_GROUP_ORDER)

  const updateCollectionOrderFn = (itemId, itemIndex) => {
    const finalPostObject = {
      parentCollectionId: collection.id,
      collectionId: collection.id,
      ids: [itemId],
      index: itemIndex,
    }

    return updateCollectionOrder({
      variables: finalPostObject,
    }).catch(res => {
      console.error(res)

      Notification({
        message: 'Could not update',
        type: 'danger',
        title: 'Error',
      })
    })
  }

  const templateSuccess = {
    type: 'success',
    title: 'Success',
    message: "Collection members' template successfully saved",
  }

  const templateError = error => ({
    message: `Could not save collection members' template: ${error.message}`,
    type: 'danger',
    title: 'Error',
  })

  const [createTemplate, { loading: isSavingCreateBookSettings }] = useMutation(
    CREATE_BOOK_SETTINGS_TEMPLATE,
    {
      update: (cache, { data: createTemplateResponse }) => {
        const responseData = createTemplateResponse.createBookSettingsTemplate
        const { templateType } = responseData

        const queryParams = {
          query: COLLECTION_TEMPLATES,
          variables: templatesQueryVariables,
        }

        const currentTemplateData = cache.readQuery({ ...queryParams })
          ?.bookSettingsTemplates

        // replace default template in our list with newly created one
        const newData = currentTemplateData.filter(
          template => template.id !== `${collectionId}-default-${templateType}`,
        )

        newData.push(responseData)

        cache.writeQuery({
          ...queryParams,
          data: {
            bookSettingsTemplates: newData,
          },
        })
      },
    },
  )

  const createTemplateFn = (type, values) => {
    return createTemplate({
      variables: {
        input: {
          collectionId,
          organizationId,
          type,
          values: bookSettingsToBookAPI(values),
        },
      },
    })
      .then(() => {
        Notification(templateSuccess)
      })
      .catch(error => {
        console.error(error)
        Notification(templateError(error))
      })
  }

  const [updateTemplate, { loading: isSavingUpdateBookSettings }] = useMutation(
    UPDATE_BOOK_SETTINGS_TEMPLATE,
  )

  const updateTemplateFn = (templateId, values) => {
    return updateTemplate({
      variables: {
        input: {
          templateId,
          values: bookSettingsToBookAPI(values),
        },
      },
    })
      .then(() => {
        Notification(templateSuccess)
      })
      .catch(error => {
        console.error(error)
        Notification(templateError(error))
      })
  }

  const [submitComponentsFn, { loading: isSubmitting }] = useMutation(
    SUBMIT_BOOK_COMPONENTS,
  )

  const [publishBookComponent, { loading: isPublishing }] = useMutation(
    BOOK_COMPONENT_PUBLISH,
  )

  const [reconvertBookComponent, { loading: isReconverting }] = useMutation(
    RECONVERT_FILE,
  )

  const submitComponents = selectedRows => {
    // eslint-disable-next-line no-extra-boolean-cast
    if (
      collectionMetadata?.sourceType &&
      collectionMetadata?.wholeBookSourceType &&
      collectionMetadata?.chapterProcessedSourceType
    ) {
      setCollectionSourceTypeModalIsOpen(false)

      submitComponentsFn({
        variables: {
          bookComponents: selectedRows.map(selected => selected.id),
        },
        refetchQueries: [
          { query: GET_COLLECTION, variables: { id: collection.id } },
        ],
      }).catch(e => {
        Notification({
          title: 'Error happened while submiting',
          message: e.message,
          type: 'danger',
        })
      })
    } else {
      setCollectionSourceTypeModalIsOpen(true)
    }
  }

  const publishBookcompFn = selectedRows => {
    return publishBookComponent({
      refetchQueries: [
        { query: GET_COLLECTION, variables: { id: collection.id } },
      ],
      variables: {
        bookComponent: selectedRows
          // .filter(sel => sel.checked)
          .map(item => item.id),
      },
    })
      .then(() => {
        // setConfimIsOpen(false)
        // setReviewConfirmIsOpen(false)
        // setSelectedRows([])
      })
      .catch(e => {
        Notification({
          type: 'danger',
          message: e.message || "Can't publish",
          title: 'Error',
        })
      })
  }

  const handleReloadPreview = selectedRows => {
    // eslint-disable-next-line no-extra-boolean-cast
    if (
      collectionMetadata?.sourceType &&
      collectionMetadata?.wholeBookSourceType &&
      collectionMetadata?.chapterProcessedSourceType
    ) {
      setCollectionSourceTypeModalIsOpen(false)

      reconvertBookComponent({
        refetchQueries: [
          { query: GET_COLLECTION, variables: { id: collectionId } },
        ],
        variables: {
          ids: selectedRows.map(selected => selected.id),
        },
      })
    } else {
      setCollectionSourceTypeModalIsOpen(true)
    }
  }

  // #endregion MUTATIONS

  // #region PERMISSIONS
  const {
    orgAdminForOrganisation,
    editorForOrganisation,
    isSysAdmin,
  } = currentUser.auth

  const isOrgAdmin = orgAdminForOrganisation.includes(organizationId)
  const isEditor = editorForOrganisation.includes(organizationId)

  // #endregion PERMISSIONS

  // #region UI
  const collection = data?.getCollection

  const collectionSettings =
    collection && collectionAPIToCollectionSettings(collection)

  const collectionMetadata =
    collection && collectionAPIToCollectionMetadata(collection)

  const templates = transformTemplatesToUIData(
    templatesData?.bookSettingsTemplates,
  )

  const searchGranthubFn = searchValue => Granthub.search(searchValue, PAGE)

  if (!isSysAdmin && !isOrgAdmin) {
    return 'You do not have sufficient permissions to access this page'
  }

  const canPublish = selectedRows =>
    selectedRows.every(book =>
      canPublishMultiple({
        currentUser,
        organizationId,
        bookId: book.id,
      }),
    )

  const disablePublishBtn = selectedRows => {
    if (isPublishing || selectedRows.length === 0) return true

    return (
      selectedRows.filter(x => {
        return disablePublish({
          url: x.metadata.url,
          status: x.status,
          sourceFiles: x.sourceFiles,
          convertedFile: x.convertedFile,
        })
      }).length > 0
    )
  }

  const disableReloadPreviewBtn = selectedRows =>
    isReconverting ||
    isSubmitting ||
    (selectedRows.length > 0
      ? selectedRows.filter(item => !disableReloadPreview(item)).length !==
        selectedRows.length
      : true)

  const disableSubmitBtn = selectedRows => {
    return (
      isSubmitting ||
      isReconverting ||
      (selectedRows.length > 0
        ? selectedRows.filter(item => {
            return (
              areFilesTaged({
                workflow: item.workflow,
                chapterIndependently: item?.chapterIndependently,
                sourceFiles: item.sourceFiles,
              }) && !disabledSubmit(item)
            )
          }).length !== selectedRows.length
        : true)
    )
  }

  const actionsLoader = {
    isSubmitting,
    isReconverting,
    isPublishing,
  }

  return (
    <PermissionsGate scopes={['view-collection']}>
      <CollectionManagerUI
        actionsLoader={actionsLoader}
        canPublish={canPublish}
        collection={collection}
        collectionMetadata={collectionMetadata}
        collectionSettings={collectionSettings}
        createTemplate={createTemplateFn}
        disablePublish={disablePublishBtn}
        disableReloadPreview={disableReloadPreviewBtn}
        disableSubmit={disableSubmitBtn}
        isEditor={isEditor}
        isOrgAdmin={isOrgAdmin}
        isSavingTemplate={
          isSavingCreateBookSettings || isSavingUpdateBookSettings
        }
        isSysAdmin={isSysAdmin}
        isUpdatingCollection={isUpdatingCollection}
        loading={loading}
        publishBooks={publishBookcompFn}
        reloadPreviewBooks={handleReloadPreview}
        saveCollectionSettings={saveCollectionSettings}
        searchGranthub={searchGranthubFn}
        submitBooks={submitComponents}
        templates={templates}
        templatesLoading={templatesLoading}
        updateCollectionMetadata={saveCollectionMetadata}
        updateCollectionOrder={updateCollectionOrderFn}
        updateTemplate={updateTemplateFn}
      />
      <Information
        hideModal={() => setCollectionSourceTypeModalIsOpen(false)}
        isOpen={collectionSourceTypeModal}
        title="Complete the required 'Book Source type' field in the Book Metadata, and then come back to this action."
        type="warning"
      />
    </PermissionsGate>
  )
  // #endregion UI
}

CollectionManager.propTypes = {}

CollectionManager.defaultProps = {}

export default CollectionManager
