import React, { useContext } from 'react'
import { Redirect } from 'react-router-dom'

import CurrentUserContext from '../userContext'

import { OrganizationsPage as OrganizationsUI } from '../../ui/Pages'
import { getAccessedOrganizations } from './_helpers'

const Organizations = () => {
  const { currentUser } = useContext(CurrentUserContext)
  const organizationsList = getAccessedOrganizations(currentUser)

  if (currentUser?.auth.isSysAdmin || currentUser?.auth.isPDF2XMLVendor) {
    return <Redirect to="/dashboard" />
  }

  if (organizationsList.length === 1) {
    return <Redirect to={`/organizations/${organizationsList[0].id}`} />
  }

  return <OrganizationsUI organizationsList={organizationsList} />
}

Organizations.propTypes = {}

Organizations.defaultProps = {}

export default Organizations
