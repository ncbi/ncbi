import { useContext } from 'react'
import ConstantsContext from './constantsContext'

const UseConstant = prop => {
  const { s } = useContext(ConstantsContext)
  return s(prop)
}

export default UseConstant
