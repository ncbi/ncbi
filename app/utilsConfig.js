/* eslint-disable no-param-reassign */

import config from 'config'

export const serverUrl = config['pubsweet-client'].baseUrl
  ? config['pubsweet-client'].baseUrl
  : window.location.origin

export const onClickFileDownload = fileId => {
  const fileLink = `${serverUrl}/downloadFileVersion?id=${fileId}&category=comment`
  window.location.href = fileLink
}

export const getDownloadLink = (fileId, category) => {
  return `${serverUrl}/downloadFileVersion?id=${fileId}&category=${category}`
}
