const oneOf = (...arg) => {
  let cond = false

  arg.forEach(element => (cond = cond || element))

  return cond
}

export const hasOneModifierRole = ({
  currentUser,
  organizationId,
  bookId,
  bookComponentId,
}) => {
  const {
    isSysAdmin,
    orgAdminForOrganisation,
    editorForBook,
    authorForBook,
    editorForBookComponent,
    authorForBookComponent,
  } = currentUser.auth

  const isOrgAdmin = orgAdminForOrganisation.includes(organizationId)
  const isEditorForBook = editorForBook.includes(bookId)
  const isAuthorForBook = authorForBook.includes(bookId)

  const isEditorForBookComponent =
    bookComponentId && editorForBookComponent.includes(bookComponentId)

  const isAuthorForBookComponent =
    bookComponentId && authorForBookComponent.includes(bookComponentId)

  return oneOf(
    isSysAdmin,
    isOrgAdmin,
    isEditorForBook,
    isAuthorForBook,
    isEditorForBookComponent,
    isAuthorForBookComponent,
  )
}

export const canPublishMultiple = ({
  currentUser,
  organizationId,
  bookId,
  bookComponentIds,
}) => {
  const {
    isSysAdmin,
    orgAdminForOrganisation,
    editorForBook,
  } = currentUser.auth

  const isOrgAdmin = orgAdminForOrganisation.includes(organizationId)
  const isEditorForBook = editorForBook.includes(bookId)

  return oneOf(isSysAdmin, isOrgAdmin, isEditorForBook)
}

export const canPublish = ({
  currentUser,
  organizationId,
  bookId,
  bookComponentId,
}) => {
  const {
    isSysAdmin,
    orgAdminForOrganisation,
    editorForBook,
    editorForBookComponent,
  } = currentUser.auth

  const isOrgAdmin = orgAdminForOrganisation.includes(organizationId)
  const isEditorForBook = editorForBook.includes(bookId)

  const isEditorForBookComponent = editorForBookComponent.includes(
    bookComponentId,
  )

  return oneOf(
    isSysAdmin,
    isOrgAdmin,
    isEditorForBook,
    isEditorForBookComponent,
  )
}

export const canUploadBulk = ({ currentUser, organizationId, bookId }) => {
  const {
    orgAdminForOrganisation,
    editorForBook,
    authorForBook,
    isSysAdmin,
  } = currentUser.auth

  const isOrgAdmin = orgAdminForOrganisation.includes(organizationId)
  const isEditorForBook = editorForBook.includes(bookId)
  const isAuthorForBook = authorForBook.includes(bookId)

  return oneOf(isSysAdmin, isOrgAdmin, isEditorForBook, isAuthorForBook)
}

export const canUploadFilesTab = ({
  currentUser,
  organizationId,
  bookId,
  bookComponentId,
}) => {
  return hasOneModifierRole({
    currentUser,
    organizationId,
    bookId,
    bookComponentId,
  })
}

export const isOnlyPreviewer = ({
  currentUser,
  organizationId,
  bookId,
  bookComponentId,
}) => {
  const hasOneofModifierRoles = hasOneModifierRole({
    currentUser,
    organizationId,
    bookId,
    bookComponentId,
  })

  const { previewerForBook, previewerForBookComponent } = currentUser.auth

  const isPreviewer =
    previewerForBook.includes(bookId) ||
    previewerForBookComponent.includes(bookComponentId)

  return !hasOneofModifierRoles && isPreviewer
}

export const canModifyBook = ({ currentUser, organizationId, bookId }) => {
  const hasOneofModifierRoles = hasOneModifierRole({
    currentUser,
    organizationId,
    bookId,
  })

  const { isPDF2XMLVendor } = currentUser.auth

  return hasOneofModifierRoles || isPDF2XMLVendor
}

export const canViewBook = ({ currentUser, organizationId, bookId }) => {
  const hasOneofModifierRoles = hasOneModifierRole({
    currentUser,
    organizationId,
    bookId,
  })

  const { previewerForBook, isPDF2XMLVendor } = currentUser.auth

  const isPreviewer = previewerForBook.includes(bookId)

  return hasOneofModifierRoles || isPreviewer || isPDF2XMLVendor
}
