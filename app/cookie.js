export function getCookieValue(cookieName) {
  const cookie = document.cookie
    .split(';')
    .map(valuePair => valuePair.split('=').map(c => c.trim()))
    .find(([name, _]) => name === cookieName)

  return cookie === undefined ? undefined : cookie[1]
}

export function getNcbiCookie() {
  return getCookieValue('WebCubbyUser')
}
