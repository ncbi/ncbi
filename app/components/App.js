import React, { useState } from 'react'
import { get } from 'lodash'
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'

import { ModalProvider } from '@pubsweet/ui/src/molecules/modal'
import constants from '../../config/constants'

import { CurrentUserProvider } from '../userContext'
import { ConstantsProvider } from '../constantsContext'

const App = ({ children }) => {
  const [currentUser, setCurrentUser] = useState(null)

  return (
    <ModalProvider>
      <ReactNotification />
      <CurrentUserProvider value={{ currentUser, setCurrentUser }}>
        <ConstantsProvider value={{ s: str => get(constants, str) }}>
          {children}
        </ConstantsProvider>
      </CurrentUserProvider>
    </ModalProvider>
  )
}

export default App
