import React, { useContext, useEffect } from 'react'
import { Redirect, Route, useLocation, useParams } from 'react-router-dom'
import { useQuery } from '@apollo/client'

import { CURRENT_USER } from '../graphql'
import CurrentUserContext from '../userContext'
import { Spinner } from '../../ui/components'

const PrivateRoute = props => {
  const { organizationId } = useParams()
  const location = useLocation()

  const { currentUser, setCurrentUser } = useContext(CurrentUserContext)
  const token = localStorage.getItem('token')
  const { data, error } = useQuery(CURRENT_USER, { skip: !token })

  // update context when data arrives
  useEffect(() => {
    if (data && data.currentUser) {
      setCurrentUser(data.currentUser)
    }
  }, [data])

  const loginUrl =
    process.env.ENABLE_DEVELOPMENT_LOGIN === 'true'
      ? `/login?next=${location.pathname}`
      : `/loginNcbi?next=${location.pathname}`

  const RedirectToLogin = <Redirect to={loginUrl} />

  if (!token) return RedirectToLogin

  if (error) {
    console.error(error)
    return RedirectToLogin
  }

  if (!currentUser) return <Spinner label="Logging you in" pageLoader />

  const { isSysAdmin, isPDF2XMLVendor } = currentUser.auth

  const hasOrganizationAccess =
    isSysAdmin ||
    isPDF2XMLVendor ||
    !!currentUser.organisations.find(org => org.id === organizationId)

  if (organizationId && !hasOrganizationAccess) {
    return <Redirect to="/organizations" />
  }

  return <Route {...props} />
}

export default PrivateRoute
