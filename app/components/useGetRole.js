import { useContext } from 'react'
import { useQuery } from '@apollo/client'
import UserContext from '../userContext'
import {
  GET_BOOK_COMPONENT,
  GET_BOOK,
} from '../pages/_helpers/bookComponentQuery'

const useGetRole = ({ organizationId, bookId, bookComponentVersionId }) => {
  const roles = []

  const { currentUser } = useContext(UserContext)
  currentUser.auth.isSysAdmin && roles.push('sysAdmin')
  currentUser.auth.isPDF2XMLVendor && roles.push('PDF2XMLVendor')

  currentUser.auth.orgAdminForOrganisation.includes(organizationId) &&
    roles.push('orgAdminForOrganisation')

  // roles for organization
  currentUser.auth.userForOrganisation.includes(organizationId) &&
    roles.push('userForOrganisation')

  currentUser.auth.editorForOrganisation?.includes(organizationId) &&
    roles.push('editorForOrganisation')

  currentUser.auth.authorForOrganisation?.includes(organizationId) &&
    roles.push('authorForOrganisation')

  currentUser.auth.previewerForOrganisation?.includes(organizationId) &&
    roles.push('previewerForOrganisation')

  // roles for book
  currentUser.auth.editorForBook.includes(bookId) && roles.push('editorForBook')

  currentUser.auth.authorForBook?.includes(bookId) &&
    roles.push('authorForBook')

  currentUser.auth.previewerForBook.includes(bookId) &&
    roles.push('previewerForBook')

  const { data: bookData } = useQuery(GET_BOOK, {
    skip: !bookId,
    variables: { id: bookId },
  })

  // Check if user has roles in any of book components
  if (bookData?.getBook) {
    const bookComps = bookData?.getBook.bookComponents.map(x => x.id)

    bookComps.forEach(item => {
      currentUser.auth.editorForBookComponent.includes(item) &&
        roles.push('editorForSomeBookComponent')

      currentUser.auth.authorForBookComponent?.includes(item) &&
        roles.push('authorForSomeBookComponent')

      currentUser.auth.previewerForBookComponent.includes(item) &&
        roles.push('previewerForSomeBookComponent')
    })
  }

  // if we are inside book component page

  const { data: bookComponentData } = useQuery(GET_BOOK_COMPONENT, {
    skip: !bookComponentVersionId,
    variables: { id: bookComponentVersionId },
  })

  const bookComponentId = bookComponentData?.getBookComponent.id

  currentUser.auth.editorForBookComponent.includes(bookComponentId) &&
    roles.push('editorForBookComponent')

  currentUser.auth.authorForBookComponent?.includes(bookComponentId) &&
    roles.push('authorForBookComponent')

  currentUser.auth.previewerForBookComponent.includes(bookComponentId) &&
    roles.push('previewerForBookComponent')

  return { roles }
}

export default useGetRole
