/* eslint-disable react/jsx-no-useless-fragment */
/* eslint-disable react/prop-types */
import React, { cloneElement } from 'react'
import { useParams } from 'react-router-dom'
import { PERMISSIONS } from './permissionMaps'
import useGetRole from './useGetRole'

const hasPermission = ({ permissions, scopes }) => {
  const scopesMap = {}

  scopes.forEach(scope => {
    scopesMap[scope] = true
  })

  return permissions.some(permission => scopesMap[permission])
}

export default function PermissionsGate({
  children,
  errorProps = null,
  scopes = [],
  showPermissionInfo,
  getPermission = false,
  externalBookId,
}) {
  if (scopes.length === 0) return <>{children}</>

  const {
    bookId: parameterBookId,
    bookComponentId,
    organizationId,
  } = useParams()

  const bookId = externalBookId || parameterBookId

  const renderPermissionMessage = (
    <div>You don&apos;t have sufficient permissions</div>
  )

  const { roles } = useGetRole({
    organizationId,
    bookId,
    bookComponentVersionId: bookComponentId,
  })

  const permissions = []

  if (roles.includes('sysAdmin')) {
    return getPermission ? true : <>{children}</>
  }

  roles.length === 0 && roles.push('no-role')

  roles.forEach(
    role => PERMISSIONS[role] && permissions.push(...PERMISSIONS[role]),
  )

  const permissionGranted = hasPermission({ permissions, scopes })

  // console.info(
  //   scopes, // .filter(x => x === 'view-book-errors-comment'),
  //   roles,
  //   permissionGranted,
  // )

  if (getPermission) return permissionGranted

  if (!permissionGranted && showPermissionInfo) return renderPermissionMessage

  if (!permissionGranted && errorProps)
    return cloneElement(children, { ...errorProps })

  if (!permissionGranted) return null
  return <>{children}</>
}
