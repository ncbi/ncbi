export const ROLES = {
  sysAdmin: 'sysAdmin',
  PDF2XMLVendor: 'PDF2XMLVendor',

  // organization roles
  orgAdminForOrganisation: 'orgAdminForOrganisation',
  authorForOrganisation: 'authorForOrganisation',
  previewerForOrganisation: 'previewerForOrganisation',
  editorForOrganisation: 'editorForOrganisation',

  awardeeForOrganisation: 'awardeeForOrganisation',
  userForOrganisation: 'userForOrganisation',

  // book roles
  authorForBook: 'authorForBook',
  editorForBook: 'editorForBook',
  previewerForBook: 'previewerForBook',

  // book component roles
  authorForBookComponent: 'authorForBookComponent',
  editorForBookComponent: 'editorForBookComponent',
  previewerForBookComponent: 'previewerForBookComponent',
  editorForSomeBookComponent: 'editorForSomeBookComponent', // editor for one or more components in the book

  // No roles
  noRole: 'no-role',
}
export const SCOPES = {
  // Dashboard
  canViewDashboard: 'view-dashboard',
  canViewOrganizationTab: 'view-organization-tab',
  canViewOrganizationAccessTab: 'view-organization-access-tab',
  canViewMetadataContentTab: 'view-metadata-content-tab',
  canEditOrganizationSettings: 'edit-organization',
  canViewOrganizationsTab: 'view-organizations-tab',
  canViewBookSettingsTemplateTab: 'view-book-settings-template-tab',
  canCreateOrganization: 'create-organization',
  canCreateCollection: 'create-collection',
  canViewUserTab: 'view-user-tab',
  canAcceptRejectUser: 'accept-reject-user',
  canGiveRoleSysAdmin: 'edit-role-sysadmin',
  canGiveRolePDF2XML: 'edit-role-PDF2XML',
  canGiveRoleOrgAdmin: 'edit-role-orgAdmin',
  canGiveRoleEditor: 'edit-role-editor',
  canGiveRoleAuthor: 'edit-role-author',
  canGiveRolePreviewer: 'edit-role-previewer',
  canInviteOrgAdmin: 'invite-orgAdmin',
  canApproveUser: 'approve-user',
  editOrganizationSettingModal: 'edit-organization-setting-modal',
  canViewOrganizationSettingTab: 'view-organization-setting-tab',
  canViewBookTemplate: 'view-book-template',
  canEnableDisableUser: 'enable-disable-user',
  canEditUserModal: 'edit-user-modal',

  editRoleSysAdmin: 'edit-sysadmin',
  editRolePDF2XML: 'edit-PDF2XML',
  editRoleOrgAdmin: 'edit-orgAdmin',
  editRoleEditor: 'edit-editor',

  canCreateBook: 'create-book',
  canPublishBook: 'publish-book',
  canDownloadBook: 'download-bok',
  canArchiveBook: 'archive-book',
  canDeleteBook: 'delete-book',
  enableUpload: 'enable-upload',

  canArchiveCollection: 'archive-collection',
  canDeleteCollection: 'delete-collection',
  canViewBookTeamTab: 'view-book-team-tab',
  // Collection Manager
  canViewCollection: 'view-collection',
  canViewCollectionTeam: 'view-collection-team', // (not developed yet)
  canViewCollectionSetting: 'view-collection-setting',
  canViewCollectionMetaData: 'view-collection-metadata',
  canSubmitWholeBooks: 'submit-whole-books',
  canReloadPreviewWholeBooks: 'reload-preview-whole-books',
  canPublishWholeBooks: 'publish-whole-books',
  canDownloadFromCollection: 'download-from-collection', // (not developed yet)
  canArchiveFromCollection: 'archive-from-collection', // (not developed yet)
  canDeleteFromCollection: 'delete-from-collection', // (not developed yet)
  canMoveBooksToGroups: 'move-books-to-groups',
  canManualOrderCollection: 'order-by-drag-drop-collection',
  canViewCollectionToc: 'view-collection-toc',
  canPublishCollectionToc: 'publish-collection-toc',
  canGranthubMetadatamodal: 'granthub-metadata-modal',
  canProcessingInstructions: 'processing-instructions',
  canEditCollectionMetaData: 'edit-collection-metadata',
  canEditCollectionSetting: 'edit-collection-setting',

  // Book Manager
  canViewBook: 'view-book',
  canViewWholeBook: 'view-whole-book',
  canEditBook: 'edit-book',
  canEditBookMetadata: 'edit-book-metadata',
  canEditBookSettings: 'edit-book-settings',
  canEditBookTeam: 'edit-book-team',
  canEditCollection: 'edit-collection',
  canBulkUploadChapters: 'upload-bulk-chapters',
  canViewBookTeamModal: 'view-book-team',
  canViewBookSettingModal: 'view-book-settings',
  canViewBookMetaDataModal: 'view-book-metadata',
  canViewBookToc: 'view-book-toc',
  canSubmitBook: 'submit-book',
  canReloadPreviewBook: 'book-reload-preview',
  canPublishTocBook: 'publish-book-toc',
  canMoveBookTo: 'move-book-to',
  // canAddPartBook: 'add-part', its same as can edit book
  canManualOrderBook: 'order-by-drag-drop',
  canViewBookFileTab: 'view-book-file-tab',
  canAddPartBook: 'add-part',
  canRepeatBookTo: 'repeat-book-to',

  // Book Component
  canViewBookComponent: 'view-book-component',
  canEditBookComponent: 'edit-book-component',

  canViewBookNewVersion: 'view-book-new-version',
  canViewBookAllVersions: 'view-book-all-versions',

  canViewNewVersion: 'view-book-component-new-version',
  canViewAllVersions: 'view-book-component-all-versions',

  canViewBookComponentRequestReview: 'view-book-component-request-review',
  canViewBookComponentRevise: 'view-book-component-revise',
  canViewBookComponentApprove: 'view-book-component-approve',

  canViewReviewCommentPanel: 'view-review-comment-panel',
  canPublishBookComponent: 'publish-book-component',
  canViewBookComponentPublish: 'view-book-component-publish',
  canViewBookComponentTeamTab: 'view-book-component-team-tab',
  canViewBookComponentAddMember: 'view-book-component-add-member',
  canViewBookComponentRemoveFromTeam: 'remove-from-team-book-component',
  canEditBookComponentTeam: 'edit-team-book-component',
  canViewBookComponentFileTab: 'view-book-component-file-tab',
  canViewBookComponentSubmit: 'view-book-component-submit',
  canViewBookComponentReloadPreview: 'view-book-component-reload-preview',
  canViewBookComponentUploadSourceFileVersion: 'upload-source-file',
  canViewBookComponentUploadConvertedFileVersion: 'upload-converted-file',
  canViewBookComponentDownloadFileVersion: 'download-file',
  canBookComponentDisplayPdfUploadFiles: 'pdf-upload-files-bookShelfDisplayPdf',
  canBookComponentXmlDisplayPdfUploadFiles:
    'xml-upload-files-bookShelfDisplayPdf',
  canBookComponentPdfDeleteFiles: 'pdf-delete-files',
  canViewBookComponentImage: 'view-image-files',
  canUploadBookComponentImage: 'upload-image-files',
  canDeleteImageFiles: 'delete-image-files',
  canViewBookComponentDownload: 'view-download-files',
  canViewBookComponentSupport: 'view-support-files',
  canUploadBookComponentSupport: 'upload-support-files',
  canDeleteBookComponentSupport: 'delete-support-files',
  canViewBookComponentReview: 'view-review-files',
  canViewBookComponentReviewDownload: 'view-review-download',
  canViewBookComponentMetadataTab: 'view-book-component-metadata-tab',
  canEditBookComponentMetadata: 'edit-book-component-metadata',
  canViewBookComponentErrorsTab: 'view-book-component-errors-tab',
  canViewBookComponentComment: 'view-book-component-errors-comment',
  canViewBookErrorsComment: 'view-book-errors-comment',
  canViewVendorTab: 'view-vendor-tab',
  canOpenIssue: 'create-issue',
  canCloseIssue: 'close-issue',
  canCommentOnIssue: 'comment-on-issue',
  canUploadSupplementaryFiles: 'upload-supplementary-files',
  canDeleteSupplementaryFiles: 'delete-supplementary-files',
}

export const PERMISSIONS = {
  [ROLES.orgAdminForOrganisation]: [
    SCOPES.canArchiveBook,
    SCOPES.canArchiveCollection,
    SCOPES.canDownloadBook,
    SCOPES.canPublishBook,
    SCOPES.canApproveUser,
    SCOPES.canCreateBook,
    SCOPES.canViewUserTab,
    SCOPES.canAcceptRejectUser,
    SCOPES.canGiveRoleOrgAdmin,
    SCOPES.canGiveRoleEditor,
    SCOPES.canGiveRoleAuthor,
    SCOPES.canGiveRolePreviewer,
    SCOPES.canInviteOrgAdmin,
    SCOPES.canViewCollection,
    SCOPES.canEditCollectionSetting,
    SCOPES.canEditCollectionMetaData,
    SCOPES.canViewOrganizationSettingTab,
    SCOPES.canViewOrganizationAccessTab,
    SCOPES.canViewOrganizationTab, // if member of more than one
    SCOPES.canEnableDisableUser,
    SCOPES.editRoleOrgAdmin,
    SCOPES.editRoleEditor,

    // Collection Manager

    SCOPES.canViewTeamModal,
    SCOPES.canViewSettingModal,
    SCOPES.canViewMetaDataModal,
    SCOPES.canSubmitWholeBooks,
    SCOPES.canReloadPreviewWholeBooks,
    SCOPES.canPublishWholeBooks,
    SCOPES.canDownloadFromCollection,
    SCOPES.canArchiveFromCollection,
    SCOPES.canMoveBooksToGroups,
    SCOPES.canManualOrderCollection,
    SCOPES.canViewCollectionToc,
    SCOPES.canViewBookNewVersion,
    // Book Manager
    SCOPES.canViewBook,
    SCOPES.canViewWholeBook,
    SCOPES.canBulkUploadChapters,
    SCOPES.canEditBookMetadata,
    SCOPES.canEditBookSettings,
    SCOPES.canViewBookTeamModal,
    SCOPES.canEditBookTeam,
    SCOPES.canViewBookSettingModal,
    SCOPES.canViewBookMetaDataModal,
    SCOPES.canViewBookToc,
    SCOPES.canSubmitBook,
    SCOPES.canReloadPreviewBook,
    SCOPES.canPublishBook,
    SCOPES.canPublishTocBook,
    SCOPES.canDownloadBook,
    SCOPES.canArchiveBook,
    SCOPES.canBookMoveTo,
    SCOPES.canBookAddPart,
    SCOPES.canManualOrderBook,
    SCOPES.canPublishBookComponent,
    SCOPES.canAddPartBook,
    SCOPES.canMoveBookTo,
    SCOPES.canRepeatBookTo,
    SCOPES.canEditBook,
    SCOPES.canViewBook,

    // Book Component
    SCOPES.canViewNewVersion,
    SCOPES.canViewAllVersions,
    SCOPES.canViewReviewCommentPanel,
    SCOPES.canPublishBookComponent,
    SCOPES.canViewBookComponentPublish,
    SCOPES.canPublishBookComponentPublish,
    SCOPES.canViewBookComponentTeamTab,
    SCOPES.canViewBookTeamTab,
    SCOPES.canViewBookComponentAddMember,
    SCOPES.canViewBookComponentRemoveFromTeam,
    SCOPES.canEditBookComponentTeam,
    SCOPES.canViewBookComponentFileTab,
    SCOPES.canViewBookFileTab,
    SCOPES.canViewBookComponentSubmit,
    SCOPES.canViewBookComponentReloadPreview,
    SCOPES.canViewBookComponentUploadSourceFileVersion,
    SCOPES.canViewBookComponentDownloadFileVersion,
    SCOPES.canBookComponentDisplayPdfUploadFiles,
    SCOPES.canBookComponentXmlDisplayPdfUploadFiles,
    SCOPES.canBookComponentPdfDeleteFiles,
    SCOPES.canViewBookComponentImage,
    SCOPES.canViewBookComponentDownload,
    SCOPES.canViewBookComponentSupport,
    SCOPES.canEditBookComponentMetadata,
    SCOPES.canViewBookComponentReview,
    SCOPES.canViewBookComponentReviewDownload,
    SCOPES.canViewBookComponentMetadataTab,
    SCOPES.canViewBookComponentErrorsTab,
    SCOPES.canViewBookComponent,
    SCOPES.canViewBookComponentComment,
    SCOPES.canViewBookErrorsComment,
    SCOPES.canViewBookComponentVendorTab,
    SCOPES.canViewBookComponentOpenIssue,
    SCOPES.canViewBookComponentCloseIssue,
    SCOPES.canViewBookComponentCommentOnIssue,
    SCOPES.canUploadSupplementaryFiles,
    SCOPES.canUploadBookComponentImage,
    SCOPES.canUploadBookComponentSupport,
    SCOPES.canViewVendorTab,
    SCOPES.canOpenIssue,
    SCOPES.canCloseIssue,
    SCOPES.canCommentOnIssue,
  ],

  [ROLES.editorForOrganisation]: [
    SCOPES.canViewDashboard,
    SCOPES.canCreateBook,
    SCOPES.canArchiveBook,
    SCOPES.canGiveRoleAuthor,
    SCOPES.canGiveRolePreviewer,
    SCOPES.canViewUserTab,
    SCOPES.canViewOrganizationTab, // if member of more than one
    SCOPES.canViewBook,
    SCOPES.canViewOrganizationSettingTab,

    // Book Manager
    SCOPES.canViewBookTeamModal,
    SCOPES.canViewBookSettingModal,
    SCOPES.canViewBookMetaDataModal,
    SCOPES.canViewBookToc,
    SCOPES.canDownloadBook,
    SCOPES.canArchiveBook,

    // Book component
    SCOPES.canViewBookComponent,
    SCOPES.canViewBookComponentMetadataTab,
    SCOPES.canViewOrganizationAccessTab,
    SCOPES.canAcceptRejectUser,
    // Collection Manager
  ],

  [ROLES.editorForBook]: [
    SCOPES.canViewBook,
    SCOPES.canViewOrganizationTab,
    SCOPES.canViewBookTeamModal,
    SCOPES.canViewBookSettingModal,
    SCOPES.canViewBookMetaDataModal,
    SCOPES.canViewBookToc,
    SCOPES.canDownloadBook,
    SCOPES.canEditBook,
    SCOPES.canViewBookFileTab,
    SCOPES.canEditBookMetadata,
    SCOPES.canEditBookSettings,
    SCOPES.canEditBookTeam,
    SCOPES.canViewAllVersions,
    SCOPES.canViewUserTab,
    SCOPES.canViewOrganizationAccessTab,
    SCOPES.canViewWholeBook,
    // Book Component
    SCOPES.canViewBookComponent,
    SCOPES.canEditBookComponent,

    SCOPES.canViewBookNewVersion,
    SCOPES.canViewBookAllVersions,

    SCOPES.canViewNewVersion,
    SCOPES.canViewAllVersions,

    SCOPES.canViewBookComponentRequestReview,
    SCOPES.canViewBookComponentRevise,
    SCOPES.canViewBookComponentApprove,
    SCOPES.canPublishBook,
    SCOPES.canViewReviewCommentPanel,
    SCOPES.canPublishBookComponent,
    SCOPES.canViewBookComponentTeamTab,
    SCOPES.canViewBookTeamTab,
    SCOPES.canViewBookComponentAddMember,
    SCOPES.canViewBookComponentRemoveFromTeam,
    SCOPES.canEditBookComponentTeam,
    SCOPES.canViewBookComponentFileTab,
    SCOPES.canViewBookComponentSubmit,
    SCOPES.canViewBookComponentReloadPreview,
    SCOPES.canViewBookComponentUploadSourceFileVersion,
    // SCOPES.canGranthubMetadatamodal,
    SCOPES.canViewBookComponentDownloadFileVersion,
    SCOPES.canBookComponentDisplayPdfUploadFiles,
    SCOPES.canBookComponentXmlDisplayPdfUploadFiles,

    SCOPES.canBookComponentPdfDeleteFiles,
    SCOPES.canViewBookComponentImage,
    SCOPES.canUploadBookComponentImage,
    SCOPES.canViewBookComponentDownload,
    SCOPES.canViewBookComponentSupport,
    SCOPES.canUploadBookComponentSupport,
    SCOPES.canViewBookComponentReview,
    SCOPES.canViewBookComponentReviewDownload,
    SCOPES.canViewBookComponentMetadataTab,
    SCOPES.canEditBookComponentMetadata,
    SCOPES.canViewBookComponentErrorsTab,
    SCOPES.canViewBookComponentComment,
    SCOPES.canViewBookErrorsComment,
    SCOPES.canCommentOnIssue,
    SCOPES.canUploadSupplementaryFiles,
    SCOPES.canViewVendorTab,
    SCOPES.canOpenIssue,
    SCOPES.canCloseIssue,
    SCOPES.canCommentOnIssue,
    SCOPES.canMoveBookTo,
    SCOPES.canRepeatBookTo,
    SCOPES.canAddPartBook,
    SCOPES.canReloadPreviewBook,
    SCOPES.canPublishBookComponent,
    SCOPES.canSubmitBook,
    SCOPES.canViewBookComponentSubmit,
    SCOPES.canBulkUploadChapters,
    SCOPES.canPublishTocBook,
    SCOPES.canManualOrderBook,
    // SCOPES.canGranthubMetadatamodal,
  ],

  [ROLES.editorForBookComponent]: [
    SCOPES.canViewBook,
    SCOPES.canViewBookTeamModal,
    SCOPES.canViewBookSettingModal,
    SCOPES.canViewBookMetaDataModal,
    SCOPES.canViewBookToc,
    SCOPES.canViewWholeBook,
    // Book Component
    SCOPES.canViewBookComponent,
    SCOPES.canEditBookComponent,

    SCOPES.canViewBookNewVersion,
    SCOPES.canViewBookAllVersions,

    SCOPES.canViewNewVersion,
    SCOPES.canViewAllVersions,

    SCOPES.canViewBookComponentRequestReview,
    SCOPES.canViewBookComponentRevise,
    SCOPES.canViewBookComponentApprove,

    SCOPES.canViewReviewCommentPanel,
    SCOPES.canPublishBookComponent,
    SCOPES.canViewBookComponentTeamTab,

    SCOPES.canViewBookComponentAddMember,
    SCOPES.canViewBookComponentRemoveFromTeam,
    SCOPES.canEditBookComponentTeam,
    SCOPES.canViewBookComponentFileTab,
    SCOPES.canViewBookComponentSubmit,
    SCOPES.canViewBookComponentReloadPreview,
    SCOPES.canViewBookComponentUploadSourceFileVersion,

    SCOPES.canViewBookComponentDownloadFileVersion,
    SCOPES.canBookComponentDisplayPdfUploadFiles,
    SCOPES.canBookComponentXmlDisplayPdfUploadFiles,

    SCOPES.canBookComponentPdfDeleteFiles,
    SCOPES.canViewBookComponentImage,
    SCOPES.canUploadBookComponentImage,
    SCOPES.canViewBookComponentComment,
    SCOPES.canViewBookComponentDownload,
    SCOPES.canViewBookComponentSupport,
    SCOPES.canUploadBookComponentSupport,
    SCOPES.canViewBookComponentReview,
    SCOPES.canViewBookComponentReviewDownload,
    SCOPES.canViewBookComponentMetadataTab,
    SCOPES.canEditBookComponentMetadata,
    SCOPES.canViewBookComponentErrorsTab,
    SCOPES.canViewBookErrorsComment,

    SCOPES.canCommentOnIssue,
    SCOPES.canUploadSupplementaryFiles,
    SCOPES.canViewVendorTab,
    SCOPES.canOpenIssue,
    SCOPES.canCloseIssue,
    SCOPES.canCommentOnIssue,
    // Book Manager

    SCOPES.canBulkUploadChapters,
    SCOPES.canViewBookComponentSubmit,
  ],

  [ROLES.editorForSomeBookComponent]: [
    SCOPES.canViewBook,
    SCOPES.canViewBookTeamModal,
    SCOPES.canViewBookSettingModal,
    SCOPES.canViewBookMetaDataModal,
    SCOPES.canViewBookToc,
    // Book Component
    SCOPES.canViewBookComponent,
  ],

  [ROLES.authorForOrganisation]: [
    // Dashboard
    SCOPES.canDownloadBook, // if member of book team
    SCOPES.canViewOrganizationAccessTab,

    SCOPES.canViewOrganizationsTab, // if member of more than one

    // Book Manager
    SCOPES.canBulkUploadChapters,
    SCOPES.canSubmitBook,
    SCOPES.canReloadPreviewBook,
    SCOPES.canDownloadBook,
    SCOPES.canViewBookSettingModal, // View only
    SCOPES.canViewBookMetaDataModal, // View only
    SCOPES.canViewBookToc, // View only

    // Book Component
    SCOPES.canViewBookComponentRequestReview,
  ],

  [ROLES.authorForBook]: [
    // Dashboard
    SCOPES.canDownloadBook, // if member of book team
    SCOPES.canViewOrganizationsTab, // if member of more than one

    // Book Manager
    SCOPES.canViewWholeBook,
    // SCOPES.canBulkUploadChapters,
    // SCOPES.canSubmitBook,
    // SCOPES.canReloadPreviewBook,
    SCOPES.canDownloadBook,
    SCOPES.canViewBookSettingModal, // View only
    SCOPES.canViewBookMetaDataModal, // View only
    SCOPES.canViewBookToc, // View only
    SCOPES.canViewNewVersion,
    SCOPES.canViewAllVersions,
    SCOPES.canViewBookComponent,
    // Book Component
    SCOPES.canViewBookComponentUploadSourceFileVersion,
    SCOPES.canViewBookComponentRequestReview,
    SCOPES.canBookComponentXmlDisplayPdfUploadFiles,
    SCOPES.canUploadBookComponentImage,
    SCOPES.canViewBookComponentComment,
    SCOPES.canViewBookErrorsComment,
  ],

  [ROLES.authorForBookComponent]: [
    // Dashboard
    SCOPES.canDownloadBook, // if member of book team

    SCOPES.canViewOrganizationsTab, // if member of more than one

    // Book Manager

    SCOPES.canDownloadBook,
    SCOPES.canViewBookSettingModal, // View only
    SCOPES.canViewBookMetaDataModal, // View only
    SCOPES.canViewBookToc, // View only

    // Book Component
    SCOPES.canViewBookComponentRequestReview,
    SCOPES.canViewNewVersion,
    SCOPES.canViewAllVersions,
    SCOPES.canViewBookComponentUploadSourceFileVersion,
    SCOPES.canBookComponentXmlDisplayPdfUploadFiles,
    SCOPES.canUploadBookComponentImage,
  ],

  [ROLES.previewerForOrganisation]: [
    // Dashboard
    SCOPES.canViewOrganizationAccessTab,
    SCOPES.canViewOrganizationsTab, // if member of more than one

    // Book Component
    SCOPES.canViewBookComponentFileTab,
    SCOPES.canViewBookFileTab,
    SCOPES.canViewBookComponentReview,
    SCOPES.canViewBookComponentDownloadFileVersion, // only current Version
    SCOPES.canUploadSupplementaryFiles,
    SCOPES.canViewBookComponentRevise,
    SCOPES.canViewBookComponentApprove,
    SCOPES.canViewBookToc, // View only
  ],

  [ROLES.previewerForBook]: [
    // Dashboard

    SCOPES.canViewOrganizationsTab, // if member of more than one
    SCOPES.canViewWholeBook,
    // Book Component
    SCOPES.canViewBookComponentFileTab,
    SCOPES.canViewBookFileTab,
    SCOPES.canViewBookComponentReview,
    SCOPES.canViewBookComponentDownloadFileVersion, // only current Version
    SCOPES.canUploadSupplementaryFiles,
    SCOPES.canViewBookComponentRevise,
    SCOPES.canViewBookComponentApprove,
    SCOPES.canViewBookToc, // View only
  ],

  [ROLES.previewerForBookComponent]: [
    // Dashboard

    SCOPES.canViewOrganizationsTab, // if member of more than one

    // Book Component
    SCOPES.canViewBookComponentFileTab,
    SCOPES.canViewBookFileTab,
    SCOPES.canViewBookComponentReview,
    SCOPES.canViewBookComponentDownloadFileVersion, // only current Version
    SCOPES.canUploadSupplementaryFiles,
    SCOPES.canViewBookComponentRevise,
    SCOPES.canViewBookComponentApprove,
    SCOPES.canViewBookToc, // View only
  ],
  [ROLES.editorForSomeBookComponent]: [
    SCOPES.canViewBook,
    SCOPES.canViewBookTeamModal,
    SCOPES.canViewBookSettingModal,
    SCOPES.canViewBookMetaDataModal,
    SCOPES.canViewBookToc,
    // Book Component
    SCOPES.canViewBookComponent,
  ],

  [ROLES.PDF2XMLVendor]: [
    // Dashboard
    SCOPES.canDownloadBook,
    SCOPES.canViewOrganizationTab,
    // SCOPES.canViewOrganizationsTab,

    // Book Manager
    SCOPES.canViewWholeBook,
    SCOPES.canViewBook,
    SCOPES.canViewBookSettingModal, // View only
    SCOPES.canViewBookMetaDataModal, // View only
    SCOPES.canReloadPreviewBook,
    SCOPES.canDownloadBook,
    SCOPES.canViewBook,

    // Book Component
    SCOPES.canViewBookComponent,
    SCOPES.canViewBookComponentImage,
    SCOPES.canViewBookComponentDownload,
    SCOPES.canViewBookComponentSupport,

    SCOPES.canViewBookComponentReview,
    SCOPES.canViewBookComponentReviewDownload,
    SCOPES.canViewBookComponentMetadataTab, // View only
    SCOPES.canViewBookComponentErrorsTab,

    SCOPES.canViewBookComponentComment,
    SCOPES.canViewBookErrorsComment,
    SCOPES.canViewBookComponentVendorTab,
    SCOPES.canViewBookComponentOpenIssue,
    SCOPES.canViewBookComponentCloseIssue,
    SCOPES.canViewBookComponentCommentOnIssue,
    SCOPES.canBookComponentDisplayPdfUploadFiles,

    SCOPES.canViewBookComponentDownloadFileVersion,
    SCOPES.canViewBookComponentFileTab,
    SCOPES.canViewBookFileTab,
    SCOPES.canViewBookComponentReloadPreview,
    SCOPES.canViewReviewCommentPanel,
    SCOPES.canUploadSupplementaryFiles,
    SCOPES.canViewBookComponentUploadConvertedFileVersion,
    SCOPES.canUploadBookComponentImage,
    SCOPES.canViewVendorTab,
    SCOPES.canOpenIssue,
    SCOPES.canCloseIssue,
    SCOPES.canCommentOnIssue,
  ],

  [ROLES.noRole]: [SCOPES.canViewOrganizationAccessTab],
}
