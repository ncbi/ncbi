import React from 'react'
import { get } from 'lodash'
import constants from '../config/constants'

const ConstantsContext = React.createContext({
  constants,
  s: str => get(constants, str),
})

const { Provider, Consumer } = ConstantsContext

export { Consumer as ConstantsConsumer, Provider as ConstantsProvider }

export default ConstantsContext
