import { useLazyQuery } from '@apollo/client'

export default query => {
  const [getData, { data, variables, loading }] = useLazyQuery(query, {
    fetchPolicy: 'no-cache',
    notifyOnNetworkStatusChange: true,
  })

  return [getData, { data, variables }, loading]
}
