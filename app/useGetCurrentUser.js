import { useContext } from 'react'
import UserContext from './userContext'

const useGetCurrentUser = () => {
  const { currentUser } = useContext(UserContext)
  return currentUser
}

export default useGetCurrentUser
