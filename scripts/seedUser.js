/*eslint-disable */

const { logger } = require('@coko/server')
const users = require('../cypress/support/credentials')

const addSeedFactory = require('../server/services/dbSeederFactory')

;(async () => {
  try {
    const seeder = await addSeedFactory()

    const operationType = process.argv[2]
    const user = process.argv[3]

    if (operationType === 'create') {
      const data = {
        ...users[user],
        sysAdmin: true,
      }
      return await seeder.insert('addUser', data)
    } else if (operationType === 'addToTeams') {
      const username = process.argv[3]
      const objectId = process.argv[4]
      const role = process.argv[5] || 'editor'

      return await seeder.insert('addTeamMemberToUser', {
        user: { username },
        team: { objectId, role },
      })
    } else {
      throw new Error('Invalid Operation Type')
    }
  } catch (err) {
    logger.error(err)
    return false
  }
})()
