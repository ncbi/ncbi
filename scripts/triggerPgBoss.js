const PgBoss = require('pg-boss')

const run = async () => {
  const connectionString = `postgres://${process.env.POSTGRES_USER}:${process.env.POSTGRES_PASSWORD}@${process.env.POSTGRES_HOST}:${process.env.POSTGRES_PORT}/${process.env.POSTGRES_DB}`
  const boss = new PgBoss(connectionString)
  boss.on('error', error => console.error(error))

  await boss.start()

  await boss.send('extract-queue', {
    pushToQueueData: {
      topic: 'ffdfdfd',
      savedActivityId: null,
      notification: {
        id: '6dec7d0c-26d5-4ef9-b437-21b439b0f40f',
        type: 'ncbiNotificationMessage',
        created: '2021-06-23 12:46:17.875+03',
        updated: '2021-06-29 11:40:35.169+03',
        data:
          '{"id": "10728412", "topic": "submit_package_receipt", "status": 0, "chapter": "b978-3-030-31011-0_13", "notices": [], "publisher": "CADTH", "timestamp": "2021-06-23 05:46:14", "remoteFiles": "CADTH/978_ftp.zip"}',
        topic: 'submit_package_receipt',
        job_id: '10728412',
        proccessed: false,
        uploaded: false,
        package_type: null,
        options: null,
        retry: null,
        timeout: null,
        object_id: null,
      },
      message: 'data',
    },
    files: [
      {
        local: '/tmp/package-unzip-fb13d25d-2417-4758-912f-2f0d05fbce5e.zip',
        remote: `test.json`,
      },
    ],
  })
}

run()
