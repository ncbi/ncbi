const argv = require('minimist')(process.argv.slice(2))
const CreateTocXml = require('../server/services/events/listeners/CreateTocXml.js')

const id = argv.bookComponent || process.env.KAFKA_URL

// eslint-disable-next-line padding-line-between-statements
;(async () => {
  const createTocXml = new CreateTocXml()

  await createTocXml.handle({
    domains: [id],
  })
})()
