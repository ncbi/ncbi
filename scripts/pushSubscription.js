const argv = require('minimist')(process.argv.slice(2))

const { pubsubManager } = require('pubsweet-server')

const ContentProccess = require('../server/services/ContentProccess')

const { getPubsub } = pubsubManager

const OBJECT_UPDATED = 'OBJECT_UPDATED'
/* eslint-disable class-methods-use-this */

const id = argv.bookComponent

// eslint-disable-next-line padding-line-between-statements
;(async () => {
  const pubsub = await getPubsub()

  const contentProccess = await ContentProccess.findBestContent(id)

  const book = await contentProccess.getBook()

  pubsub.publish(`${OBJECT_UPDATED}.${book.id}`, {
    objectUpdated: {
      ...book,
    },
  })

  process.exit(0)
})()
