const argv = require('minimist')(process.argv.slice(2))
const fs = require('fs')
const FileService = require('../server/services/file/fileService')

const id = argv.file

// eslint-disable-next-line padding-line-between-statements
;(async () => {
  const content = await FileService.getSourceToBuffer(id)
  fs.writeFileSync('./downloadedFile.xml', content)
})()
