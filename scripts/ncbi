#!/bin/bash
#set -x
shopt -s globasciiranges

current_dir=$(pwd)
root_dir="$( cd "$( dirname "${BASH_SOURCE[0]}/" )" >/dev/null 2>&1 && pwd )"
export_variable_path="export PATH=$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
dir="node_modules"
package="NCBI script"
app_pid="./app.pid"
bold="\033[1m"
reset="\033[0m"
red="\033[31m"
green="\033[32m"

startApp () {
  if [ ! -f "$app_pid" ]; then
    if [ -d "${current_dir}/${dir}" ]; then
    
      # run migration scripts
      npx wait-for-it "${POSTGRES_HOST}:${POSTGRES_PORT}" --strict --timeout="${WAIT_POSTGRES_TIMEOUT}" -- pubsweet migrate 
    
      # run seed scripts
      if [ -d "./scripts/seeds" ]; then
        node ./scripts/seeds/index.js
      fi

      # run pubsweet app
      if [ "$NODE_ENV" == "production" ]; then
        array=("npx pubsweet server &")
        runApp "${array[@]}"
      else
        array=("env NODE_OPTIONS=--openssl-legacy-provider npx pubsweet start:client" "npx pubsweet start:server")
        runApp "${array[@]}"
      fi
    else
      BoldLog "You have not installed dependencies. Please run script with --install"
    fi
  else
    BoldLog "app is already running"
  fi
}

pushBookNotification () {
  node ./scripts/pushSubscription.js --bookComponent ff534ba5-50a4-450a-8dce-c2224c7aa16f
}

loadJobMessage () {
  node ./server/services/kafkaService/messageProducer.js --message e0f5567c-1fbf-4b02-b348-017c35362b28
}

downloadfile() {
  node ./scripts/downloadfile.js --file 86c4cad2-8e22-485f-bef0-aa81d4fa4ab9
}

createPreview () {
  node ./scripts/triggerCreatePreview.js --bookComponent dbc929bb-7d42-4d90-9a88-7249a9bd91bb
}

createPublish () {
  node ./scripts/triggerPublish.js --bookComponent dc1981f5-c295-4437-94d8-446a6be690c9
}

createPgBossQueue () {
  node ./scripts/triggerPgBoss.js
}

createTocXml () {
  node ./scripts/triggerCreateTocXml.js --bookComponent d59e6bdc-6fd8-4eb3-90f0-a53cb233328d
}

loadJobString () {
  node ./server/services/kafkaService/consoleProducer.js --topic submit_package_receipt
  node ./server/services/kafkaService/consoleProducer.js --topic convert_word_book_chapter_receipt
  node ./server/services/kafkaService/consoleProducer.js --topic ingest_book_chapter_receipt
  node ./server/services/kafkaService/consoleProducer.js --topic ingest_book_wholebook_receipt
  node ./server/services/kafkaService/consoleProducer.js --topic convert_xml_book_chapter_receipt
  node ./server/services/kafkaService/consoleProducer.js --topic convert_xml_book_receipt
}

restartApp () {
  stopApp
  startApp
}

findPid () {
  cpids=`pgrep -P $1|xargs`
    for cpid in $cpids;
    do
      local KILL_LIST+=" $cpid";
      findPid $cpid
    done
    echo $KILL_LIST
}

stopApp () { 
  if [ -f "$app_pid" ]; then
    while IFS= read -r line
    do 
    pids=$(findPid $line)
    [ "$pids" ] && kill $pids
    done < "$app_pid"
    rm -rf "$app_pid"
    BoldLog "app stopped"
  else
    BoldLog "app is already stopped"
  fi
}

configureDatabase () {
  export PGPASSWORD=$POSTGRES_PASSWORD && psql -U "${POSTGRES_USER}" -h "${POSTGRES_HOST}" "${POSTGRES_DB}" -c "\q"
  if [ "$?" != 0 ]; then
    BoldLog "Could not find user. Trying to create user, password, database using super user"
    unset PGPASSWORD

    cat << EOF | psql -U postgres -h "${POSTGRES_HOST}"
      SELECT 'CREATE USER $POSTGRES_USER WITH ENCRYPTED PASSWORD ''$POSTGRES_PASSWORD'';' WHERE NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname='$POSTGRES_USER')\gexec
      GRANT $POSTGRES_USER  TO postgres;
      SELECT 'CREATE DATABASE $POSTGRES_DB OWNER=$POSTGRES_USER TEMPLATE=template1' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = '$POSTGRES_DB')\gexec
      GRANT ALL PRIVILEGES ON DATABASE $POSTGRES_DB TO $POSTGRES_USER;
EOF
  else 
    unset PGPASSWORD
    BoldLog "Database and user already exists. No need to configure database for app"
  fi
}


runApp () {
  arr=("$@")
  for cmd in "${arr[@]}"; do {
    BoldLog "app $cmd started"
    $cmd & pid=$!
    echo "$pid" >> app.pid
    PID_LIST+=" $pid";
  } done

  trap "rm -rf $app_pid" SIGINT SIGTERM SIGKILL 
  if [ "$NODE_ENV" != "production" ]; then
    echo "Parallel processes have started";
    wait $PID_LIST
  fi

}


export_enviromental_variables () {
while IFS='' read -r line
do
  line=`echo $line | sed -e 's/^[[:space:]]*//'`
  if [[ ${line:0:1} != '#' ]]  && [[ $line != '' ]]; then 
    export "$line"; 
  fi
done < "$enviroment_file"
}

install_yarn () {
  if ! yarn_loc="$(type -p yarn)" || [[ -z $yarn_loc ]];
  then
    BoldLog "Could not find yarn executable. Trying to install yarn version"
    curl --compressed -o- -L https://yarnpkg.com/install.sh | bash -s -- --version "${YARN_VERSION}"
    $export_variable_path
    BoldLog "Yarn was successfully installed. here: $(which yarn) with version $(yarn --version)"
  fi
}

install_required_packages () {
  install_yarn
  yarn cache clean
  [[ -z "${http_proxy}" ]] && yarn install || env -u http_proxy yarn install
  # [[ -z "${http_proxy}" ]] && lerna exec --parallel yarn link || lerna exec --parallel env -u http_proxy yarn install
  [[ -z "${http_proxy}" ]] || yarn install
}

function BoldLog() {
  printf "$green[ NCBI output:$reset $bold$1$reset $green]$reset\n"
}


################ Start NCBI script #################
cd "$root_dir/../"

if [ -f "./.env" ]; then
  enviroment_file='.env'
else
  enviroment_file=".env.example"
fi

export_enviromental_variables

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "$package - attempt to capture frames"
      echo " "
      echo "$package [options] application [arguments]"
      echo " "
      echo "options:"
      echo "-h, --help                show brief help"
      echo "-i, --install             install & start app"
      echo "-iy, --install-yarn       install yarn for the project"
      echo "-s, --start               start the application"
      echo "-r, --restart             restart the application"
      echo "--stop                    stop application"
      exit 0
      ;;
    -iy|--install-yarn)
      if test $# -gt 0; then
        install_yarn
      fi
      shift
      ;;
    -i|-in|--install)
      if test $# -gt 0; then
        install_required_packages
        npx wait-for-it "${WAIT_POSTGRES_PORT}" --timeout=1 
        if [ "$?" != 0 ]; then
          BoldLog "Database service is not running."
        fi
        configureDatabase
        startApp
      fi
      shift
      ;;
    -s|--start)
      if test $# -gt 0; then
        startApp
      fi
      shift
      ;;
    --create-preview)
      if test $# -gt 0; then
        createPreview
      fi
      shift
      ;;
    --create-publish)
      if test $# -gt 0; then
        createPublish
      fi
      shift
      ;;
    --create-pg-boss)
      if test $# -gt 0; then
        createPgBossQueue
      fi
      shift
      ;;
    --create-toc-xml)
      if test $# -gt 0; then
        createTocXml
      fi
      shift
      ;;
    --download|-d)
      if test $# -gt 0; then
        downloadfile
      fi
      shift
      ;;
    -lm|--load-data)
      if test $# -gt 0; then
        loadJobMessage
      fi
      shift
      ;;
    -push)
      if test $# -gt 0; then
        pushBookNotification
      fi
      shift
      ;;
    -ls|--load-string)
      if test $# -gt 0; then
        loadJobString
      fi
      shift
      ;;
    -r|--restart)
      if test $# -gt 0;
      then
        restartApp
      fi
      shift
      ;;
    --stop)
      if test $# -gt 0;
      then
        stopApp
      fi
      shift
      ;;
    -st|--cypress)
      if test $# -gt 0;
      then
        array=("npx pubsweet start:server" "npx pubsweet start:client" "npx cypress open")
        runApp "${array[@]}"
      fi
      shift
      ;;
    *)
      break
      ;;
  esac
done
