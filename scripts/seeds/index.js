/*eslint-disable */
const normalizedPath = require('path').join(__dirname)

require('fs')
  .readdirSync(normalizedPath)
  .forEach(async file => {
    if (file === 'index.js') return
    const seed = require(`./${file}`)
    await seed()
  })
