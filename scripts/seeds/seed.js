const { logger } = require('@coko/server')
const { Team, TeamMember, User } = require('@pubsweet/models')
const { transaction } = require('objection')

/**
 * GLOBAL TEAMS
 */

const globalTeams = {
  sysAdmin: {
    role: 'sysAdmin',
    name: 'SysAdmin',
  },
  pdf2xmlvendor: {
    role: 'pdf2xmlVendor',
    name: 'PDF2XMLVendor',
  },
}

const createTeam = async teamData => {
  const { role, name } = teamData

  logger.info(`>>> Checking if ${role} team exists...`)

  const team = await Team.query().findOne({
    global: true,
    role,
  })

  if (team) {
    logger.info(`>>> ${role} team found`)
    return
  }

  logger.info(`>>> ${role} team not found. Creating...`)

  try {
    await Team.query().insert({
      global: true,
      name,
      role,
    })

    logger.info(`>>> ${role} team successfully created.`)
  } catch (e) {
    logger.error(e)
    process.kill(process.pid)
  }
}

const seedGlobalTeams = async () => {
  console.log('') /* eslint-disable-line no-console */
  logger.info('### CREATING GLOBAL TEAMS ###')

  await createTeam(globalTeams.sysAdmin)
  await createTeam(globalTeams.pdf2xmlvendor)
}

/**
 * ADMIN USER
 */

const createAdmin = async userData => {
  logger.info('Creating user', userData.username)

  const allUsers = await User.query()
  const usersExist = allUsers.length > 0

  if (!usersExist) {
    try {
      return transaction(User.knex(), async trx => {
        const user = new User(userData)
        await user.save()
        logger.info(`Successfully added user: ${user.username}`)

        const sysAdminTeam = await Team.query(trx).findOne({
          global: true,
          role: 'sysAdmin',
        })

        await TeamMember.query(trx).insert({
          userId: user.id,
          teamId: sysAdminTeam.id,
        })

        logger.info(
          `Successfully added user ${user.username} to the SysAdmin team`,
        )

        return user
      })
    } catch (e) {
      throw new Error(e)
    }
  }

  return false
}

const seedAdmin = async () => {
  const {
    ADMIN_USERNAME: username,
    ADMIN_PASSWORD: password,
    ADMIN_EMAIL: email,
  } = process.env

  if (!password) throw new Error('No password provided!')
  const passwordHash = await User.hashPassword(password)

  createAdmin({ username, passwordHash, email })
}

const seedFtpUser = async () => {
  const { ADMIN_PASSWORD: password } = process.env

  if (!password) throw new Error('No password provided!')
  const passwordHash = await User.hashPassword(password)

  const userData = {
    username: 'ftp',
    passwordHash,
    email: 'ftp@ftp.com',
    givenName: 'system [FTP]',
    surname: '',
  }

  logger.info('Creating user', 'FTP')

  const allUsers = await User.query().findOne({ username: 'ftp' })

  if (!allUsers) {
    try {
      return transaction(User.knex(), async trx => {
        const user = new User(userData)
        await user.save()

        logger.info(`Successfully added user: ${user.username}`)
      })
    } catch (e) {
      throw new Error(e)
    }
  }

  return false
}

const run = async () => {
  await seedGlobalTeams()
  await seedAdmin()
  await seedFtpUser()
}

module.exports = run
