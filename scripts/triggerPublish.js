const argv = require('minimist')(process.argv.slice(2))
const { Book } = require('@pubsweet/models')
const Publish = require('../server/services/events/listeners/Publish')

const id = argv.bookComponent || process.env.KAFKA_URL

// eslint-disable-next-line padding-line-between-statements
;(async () => {
  const object = await Book.query().findOne({ id }).throwIfNotFound()

  const createPublish = new Publish()
  await createPublish.handle({ object })
})()
