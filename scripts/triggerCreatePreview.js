const argv = require('minimist')(process.argv.slice(2))
const { BookComponent } = require('@pubsweet/models')
const CreatePreview = require('../server/services/events/listeners/CreatePreview')

const id = argv.bookComponent || process.env.KAFKA_URL

// eslint-disable-next-line padding-line-between-statements
;(async () => {
  const bookComponent = await BookComponent.query()
    .findOne({ id })
    .throwIfNotFound()

  const createPreview = new CreatePreview()

  await createPreview.handle({
    bookComponent,
    error: false,
    notification: { data: { id: 0 } },
  })
})()
