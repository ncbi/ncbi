/*eslint-disable */

const {
  Organisation,
  Team,
  User,
  Book,
  TeamMember,
} = require('@pubsweet/models')

;(async () => {
  const user = await User.findByField('username', 'admin').then(
    users => users[0],
  )
  // const orgName = process.argv[2] ? process.argv[2] : 'newTestOrg1326'
  const orgData = {
    name: '',
    abbreviation: '',
    teams: [
      {
        role: 'sysAdmin',
        members: [
          {
            user: { id: user.id },
            status: 'enabled',
          },
        ],
      },
    ],
    settings: {
      collections: { funded: true, bookSeries: false },
      word: true,
      xml: true,
      pdf: true,
      type: { publisher: true, funder: false },
    },
  }

  const bookData = {
    title: 'chapBook1354',
    workflow: 'word',
    organisationId: '',
    collectionId: null,
    settings: {
      chapterIndependently: true,
      // publisher: null,
      publisher: '',
    },
  }
  const settingUpdate = {
    toc: {
      allTitle: false,
      subtitle: true,
      tocMaxDepth: '4',
      contributors: true,
      documentHistory: true,
      add_body_to_parts: false,
      order_chapters_by: 'manual',
      group_chapters_in_parts: false,
    },
    UKPMC: false,
    createPdf: true,
    publisher: '',
    citationType: 'numberedParentheses',
    publisherUrl: '',
    indexInPubmed: false,
    bookLevelLinks: false,
    versionLinkUri: '',
    citationSelfUrl: '',
    versionLinkText: '',
    xrefAnchorStyle: 'default',
    footnotesDecimal: false,
    approvalPreviewer: false,
    createVersionLink: false,
    tocExpansionLevel: '4',
    createWholebookPdf: false,
    referenceListStyle: 'decimal',
    questionAnswerStyle: 'normal',
    alternateVersionsPdf: true,
    chapterIndependently: true,
    indexChaptersInPubmed: false,
    approvalOrgAdminEditor: true,
    bookLevelLinksMarkdown: '',
    displayObjectsLocation: 'at-xref',
    specialPublisherLinkUrl: '',
    alternateVersionsPdfBook: false,
    specialPublisherLinkText: '',
    bookLevelAffiliationStyle: 'at-end',
    multiplePublishedVersions: false,
    chapterLevelAffiliationStyle: 'at-end',
  }
  const metadataUpdate = {
    doi: '',
    isbn: '',
    issn: '',
    nlmId: '',
    notes: [],
    author: [],
    editor: [],
    grants: [],
    volume: '',
    pubDate: {
      date: { day: null, year: null, month: null },
      dateType: null,
      dateRange: {
        endYear: null,
        endMonth: null,
        startYear: null,
        startMonth: null,
      },
      publicationFormat: null,
    },
    pub_loc: '',
    language: 'en',
    pub_name: '',
    openAccess: false,
    sourceType: '',
    dateCreated: { day: null, year: null, month: null },
    dateUpdated: { day: null, year: null, month: null },
    licenseType: '',
    copyrightYear: '',
    copyrightHolder: '',
    licenseStatement: '',
    otherLicenseType: '',
    openAccessLicense: false,
    copyrightStatement: '',
    collaborativeAuthors: [],
  }

  const createOrg = async orgName => {
    try {
      if (!orgName) orgName = 'newTestOrg1113'
      const org = await Organisation.create({
        ...orgData,
        name: orgName,
        abbreviation: orgName,
        publisherId: null,
      })

      if (orgData.teams && org) {
        await Promise.all(
          orgData.teams.map(async team => {
            const foundTeam = await Team.query().findOne({
              objectId: org.id,
              objectType: 'Organisation',
              role: team.role,
            })
            await Team.query()
              .upsertGraphAndFetch(
                {
                  id: foundTeam.id,
                  ...team,
                },
                { eager: 'members.user.teams', relate: true, unrelate: false },
              )
              .eager('members.user.teams')
          }),
        )
      }
      return org
    } catch (err) {
      console.log(err)
    }
  }

  const createBook = async (orgId, bookName) => {
    try {
      if (!bookName) bookName = 'newBook11112'
      bookData['settings']['publisher'] = orgId
      bookData['title'] = bookName
      bookData['organisationId'] = orgId
      const book = await Book.create({
        ...bookData,
        status: 'new-book',
        ownerId: user.id,
        // transaction: this.transaction,
      })
      const updateBook = await Book.query().patchAndFetchById(book.id, {
        ...bookData,
        settings: { ...settingUpdate, publisher: orgId },
        metadata: metadataUpdate,
      })
      return updateBook
    } catch (err) {
      console.log(err)
    }
  }

  if (process.argv[2] && process.argv[3]) {
    const org = await createOrg(process.argv[2])
    const book = await createBook(org.id, process.argv[3])
    console.log(
      `organisation ${org.name} and book ${book.title} created successfully`,
    )
  } else {
    const org = await createOrg(process.argv[2])
    console.log(`organisation ${org.name} created`)
  }
})()
