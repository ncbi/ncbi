/* eslint-disable no-param-reassign */

import React from 'react'
import uniqBy from 'lodash/uniqBy'
import forEach from 'lodash/forEach'
import moment from 'moment'
import { Link } from '@pubsweet/ui'

const [WORD, PDF, XML, FUNDER] = ['word', 'pdf', 'xml', 'funder']

export const getDisplayName = ({ givenName, surname, username }) => {
  return givenName || surname ? `${givenName || ''} ${surname || ''}` : username
}

export const bookContentTypes = {
  authorManuscript: 'Author Manuscript',
  publishedPDF: 'Published PDF',
  prepublicationDraft: 'Prepublication Draft',
  finalFullText: 'Final Full Text',
}

export const getVersionTitle = ({ fundedContentType, version }) =>
  `${bookContentTypes[fundedContentType] || ''}  V${version}`

// order array based on another array
export function mapOrder(array, order, key) {
  array.sort((a, b) => {
    const A = a[key]
    const B = b[key]

    if (order.indexOf(A) > order.indexOf(B)) {
      return 1
    }

    return -1
  })

  return array
}

export const getBookComponentUsers = (teams = []) => {
  let usersList = []

  forEach(teams, item => {
    item &&
      usersList.push(
        ...item.members.map(itm => ({
          ...itm,
          role: item?.role,
          roleId: item?.id,
          roleName: item?.name,
        })),
      )
  })

  usersList = usersList?.map(item => {
    const displayName =
      item.user.givenName || item.user.surname
        ? `${item.user.givenName || ''} ${item.user.surname || ''}`
        : item.user.username

    return {
      display: displayName,
      id: item.user.id,
      role: item.role,
      roleId: item.roleId,
      roleName: item.roleName,
    }
  })

  return uniqBy(usersList, 'id')
}

export const getAllUsersForIssues = book => {
  const admins =
    book &&
    book.organisation?.teams?.filter(
      item =>
        item.role === 'sysAdmin' ||
        item.role === 'orgAdmin' ||
        item.role === 'pdf2XmlVendor',
    )

  const adminsRoles = admins.map(x => ({ id: x.id, display: `${x.name}s` }))

  const members = []

  forEach(admins, item => {
    members.push(...item.members.map(x => x.user))
  })

  return [...adminsRoles, ...members]
}

export const getAllUsers = (book, bookComponent) => {
  const bookEditors = book && book?.teams?.find(item => item.role === 'editor')

  const bookPreviewer =
    book && book?.teams?.find(item => item.role === 'previewer')

  const bookOrgAdmins =
    book &&
    book.organisation?.teams?.find(item => item.role === 'adminOrganisation')

  const bookComponentUsers =
    (bookComponent &&
      bookComponent.teams &&
      getBookComponentUsers(bookComponent.teams)) ||
    []

  const bookEditorUsers =
    (bookEditors && getBookComponentUsers([bookEditors])) || []

  const bookPreviewerUsers =
    (bookEditors && getBookComponentUsers([bookPreviewer])) || []

  const bookOrgAdminUsers =
    (bookOrgAdmins && getBookComponentUsers([bookOrgAdmins])) || []

  return [
    ...bookComponentUsers,
    ...bookPreviewerUsers,
    ...bookEditorUsers,
    ...bookOrgAdminUsers,
  ]
}

export const getAllRoles = (book, bookComponent) => {
  const bookCompRoles =
    (bookComponent.teams &&
      bookComponent?.teams?.map(({ id, role, name }) => ({
        display: `${name}s`,
        id,
        name,
        role,
      }))) ||
    []

  const bookEditor =
    book?.teams
      ?.filter(x => x.role === 'editor')
      ?.map(({ id, role, name }) => ({
        display: `${name}s`,
        id,
        name,
        role,
      })) || []

  const bookPreviewer =
    book?.teams
      ?.filter(x => x.role === 'previewer')
      ?.map(({ id, role, name }) => ({
        display: `${name}s`,
        id,
        name,
        role,
      })) || []

  const orgAdmin =
    book?.organisation?.teams
      .filter(x => x.role === 'adminOrganisation')
      ?.map(({ id, role, name }) => ({
        display: `${name}s`,
        id,
        name,
        role,
      })) || []

  return uniqBy(
    [...bookCompRoles, ...bookEditor, ...bookPreviewer, ...orgAdmin],
    'id',
  )
}

export const getReviewData = ({ selectedRows, bookTeams }) => {
  const userPreviewers =
    bookTeams
      .find(item => item.role === 'previewer')
      ?.members.filter(user => user.status === 'enabled') || []

  const bookPreviewers = userPreviewers?.map(item => {
    return {
      ...item.user,
    }
  })

  const reviewData = selectedRows.map(component => {
    if (component.status === 'preview') {
      return {
        title: component.title,
        approved: 'No review requested',
        revise: '',
        pending: '',
      }
    }

    const componentTeam = component.teams
      .find(item => item.role === 'previewer')
      ?.members.filter(user => user.status === 'enabled')

    const componentPreviewers = componentTeam.map(item => {
      return {
        ...item.user,
      }
    })

    const allPreviewers = [...componentPreviewers, ...bookPreviewers]

    const approved = component.reviewsResponse.filter(
      item => item.decision === 'approve',
    )

    const revised = component.reviewsResponse.filter(
      item => item.decision === 'requestChanges',
    )

    const pending = component.reviewsResponse.filter(
      item => item.decision === null,
    )

    const approvedUsers = approved.map(item => ({
      name: getDisplayName(allPreviewers.find(x => x.id === item.userId)) || '',
    }))

    const pendingUsers = pending.map(item => ({
      name: getDisplayName(allPreviewers.find(x => x.id === item.userId)) || '',
    }))

    const revisedUsers = revised.map(item => ({
      name: getDisplayName(allPreviewers.find(x => x.id === item.userId)) || '',
    }))

    return {
      title: component.title,
      approved: approvedUsers.map(x => x.name).join(', '),
      revise: revisedUsers.map(x => x.name).join(', '),
      pending: pendingUsers.map(x => x.name).join(', '),
    }
  })

  return reviewData
}

export const getBookComponentReviewResponse = ({ bookTeams, component }) => {
  // start response
  const bookReview = component.reviewsResponse
  const userRole = component.teams.find(item => item.role === 'previewer')
  const bookUserRole = bookTeams.find(item => item.role === 'previewer')

  const bookCompPreviewers = userRole?.members.filter(
    user => user.status === 'enabled',
  )

  const bookPreviewers = bookUserRole?.members.filter(
    user => user.status === 'enabled',
  )

  const previewers = [
    ...(bookCompPreviewers || []).map(item => {
      return {
        id: item.user.id,
        name: getDisplayName(item.user),
      }
    }),
    ...(bookPreviewers || []).map(item => {
      return {
        id: item.user.id,
        name: getDisplayName(item.user),
      }
    }),
  ]

  const approved = bookReview.filter(item => item.decision === 'approve')
  const revised = bookReview.filter(item => item.decision === 'requestChanges')
  const pending = bookReview.filter(item => item.decision === null)

  const approvedUsers = approved.map(item => ({
    name: (previewers.find(x => x.id === item.userId) || {}).name || '',
  }))

  const revisedUsers = revised.map(item => ({
    name: (previewers.find(x => x.id === item.userId) || {}).name || '',
  }))

  const pendingUsers = pending.map(item => ({
    name: (previewers.find(x => x.id === item.userId) || {}).name || '',
  }))

  return { approvedUsers, pendingUsers, revisedUsers }
}

export const openWindowWithPost = (url, data) => {
  const form = document.createElement('form')
  form.target = '_blank'
  form.method = 'POST'
  form.action = url
  form.style.display = 'none'

  data.forEach(element => {
    const input = document.createElement('input')
    input.type = 'hidden'
    input.name = element.key
    input.value = element.value
    form.appendChild(input)
  })

  document.body.appendChild(form)
  form.submit()
  document.body.removeChild(form)
}

export const checkFileNames = (file1, file2) =>
  file2 &&
  file1 &&
  file2.replace(/\.[^/.]+$/, '') !== file1.replace(/\.[^/.]+$/, '')

// for XML and PDF files only
/* eslint consistent-return: "off" */
const validExtensions = (name, file) => {
  if (
    `${name}.docx` === file.path ||
    `${name}.doc` === file.path ||
    `${name}.pdf` === file.path ||
    `${name}.xlsx` === file.path ||
    `${name}.xml` === file.path
  )
    return true
}

// its for PDF And word files
export const validateFilesBulkupload = (
  files,
  bookComponents = [],
  workflow,
) => {
  files.map(file => {
    const filename = file.name.replace(/\.[^/.]+$/, '')
    const regex = new RegExp('^[A-Za-z0-9-_\\.]+$')
    const regexnumeral = /^\d/
    const whitespaceregex = /\s/
    const fileExtension = file.name.split('.').pop()
    const lastIndex = file.name.lastIndexOf('.')
    const name = file.name.slice(0, lastIndex)

    if (workflow === 'pdf' && !validExtensions(name, file)) {
      file.errors.push(
        'File Extension should be a .docx, .doc, .pdf , .xlsx, .xml',
      )
    }

    if (
      workflow === 'word' &&
      !(`docx` === fileExtension || `doc` === fileExtension)
    ) {
      file.errors.push('File Extension should be a .docx or .doc')
    }

    if (whitespaceregex.test(filename)) {
      file.errors.push('Filenames must not contain spaces')
      file.status = 'inValid'
      return file
    }

    if (!regex.test(filename)) {
      file.errors.push(
        'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
      )
    }

    if (workflow !== 'pdf' && regexnumeral.test(filename)) {
      // Return true
      file.errors.push('Filenames must not begin with a numeral')
    }

    const notAllowedToUpload = bookComponents.find(x => x.filename === filename)

    if (notAllowedToUpload) {
      const message =
        notAllowedToUpload.status === 'failed' ||
        notAllowedToUpload.status === 'publish-failed'
          ? 'Current file version has not been processed. Please retry.'
          : 'Duplicate upload. File is currently processing.'

      file.errors.push(message)
    }

    if (file.errors.length > 0) {
      file.status = 'inValid'
    }

    return file
  })

  return files
}

export const validateXMLFilesBulkupload = (
  files,
  bookComponents = [],
  workflow,
) => {
  files.map(file => {
    const filename = file.name.replace(/\.[^/.]+$/, '')
    const regex = new RegExp('^[A-Za-z0-9-_\\.]+$')
    const whitespaceregex = /\s/
    const lastIndex = file.name.lastIndexOf('.')
    const name = file.name.slice(0, lastIndex)

    if (workflow === 'xml' && !validExtensions(name, file)) {
      file.errors.push(
        'File Extension should be a .docx, .doc, .pdf , .xlsx, .xml',
      )
    }

    if (whitespaceregex.test(filename)) {
      file.errors.push('Filenames must not contain spaces')
      file.status = 'inValid'
      return file
    }

    if (!regex.test(filename)) {
      file.errors.push(
        'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
      )
    }

    const notAllowedToUpload = bookComponents.find(x => x.filename === filename)

    if (notAllowedToUpload) {
      const message =
        notAllowedToUpload.status === 'failed' ||
        notAllowedToUpload.status === 'publish-failed'
          ? 'Current file version has not been processed. Please retry.'
          : 'Duplicate upload. File is currently processing.'

      file.errors.push(message)
    }

    if (file.errors.length > 0) {
      file.status = 'inValid'
    }

    return file
  })

  return files
}

export const validateFileName = (
  files,
  fileNameCompare,
  conversionWorkflow,
) => {
  files.map(file => {
    const fileExtension = file.name.toLowerCase()
    const filename = file.name.replace(/\.[^/.]+$/, '')

    if (
      conversionWorkflow === 'word' &&
      !(
        fileExtension.split('.')[1] === 'doc' ||
        fileExtension.split('.')[1] === 'docx'
      )
    ) {
      file.errors.push(`File extension should be a .docx or .doc`)
    }

    if (checkFileNames(filename, fileNameCompare)) {
      file.errors.push(
        `Filename does not match previous version (${fileNameCompare})`,
      )
    }

    if (file.errors.length > 0) {
      file.status = 'inValid'
    }

    return file
  })

  return files
}

export const validateFileNameConverted = ({
  uploadedFiles,
  fileNameCompare,
  conversionWorkflow,
  chapterIndependently,
}) => {
  uploadedFiles.map(file => {
    const filename = file.name.toLowerCase() || ''
    const extension = filename.split('.').pop()

    if (conversionWorkflow === 'xml' || conversionWorkflow === 'pdf') {
      // no source file
      if (
        (fileNameCompare === null || fileNameCompare.length === 0) &&
        !(extension === 'xml' || extension === 'bxml')
      ) {
        file.errors.push(`File extension should be .xml or .bxml`)
        file.status = 'inValid'
        return file
      }

      if (
        fileNameCompare !== null &&
        !(extension === 'xml' || extension === 'bxml')
      ) {
        file.errors.push(`File extension should be .xml or .bxml`)
        file.status = 'inValid'
        return file
      }

      if (
        fileNameCompare !== null &&
        chapterIndependently &&
        !fileNameCompare.includes(filename)
      ) {
        file.errors.push(
          `Filename should be ${fileNameCompare[0]} or ${fileNameCompare[1]}`,
        )
      }
    }

    if (conversionWorkflow === 'word') {
      // no source file
      if (fileNameCompare === null || fileNameCompare.length === 0) {
        !(
          filename === 'toc.xml' ||
          filename === 'toc.bxml' ||
          filename === 'TOC.xml' ||
          filename === 'TOC.bxml'
        ) && file.errors.push(`File should be toc.xml or toc.bxml`)
      } else if (!fileNameCompare.includes(filename.toLowerCase())) {
        file.errors.push(
          `Filename should be ${fileNameCompare[0]} or ${fileNameCompare[1]}`,
        )
      }
    }

    if (file.errors.length > 0) {
      file.status = 'inValid'
    }

    return file
  })

  return uploadedFiles
}

export const validateWholeBookSourceFiles = (files, workflow) => {
  files.map(file => {
    const filename = file.name.replace(/\.[^/.]+$/, '')
    const regex = new RegExp('^[A-Za-z0-9-_\\.]+$')
    const whitespaceregex = /\s/
    const lastIndex = file.name.lastIndexOf('.')
    const name = file.name.slice(0, lastIndex)

    if (whitespaceregex.test(filename)) {
      file.errors.push('Filenames must not contain spaces')
      file.status = 'inValid'
      return file
    }

    if (!regex.test(filename)) {
      file.errors.push(
        'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
      )
    }

    if (
      workflow === 'word' &&
      !(`${name}.docx` === file.path || `${name}.doc` === file.path)
    ) {
      file.errors.push('File extension should be a .docx or .doc')
    }

    if (workflow === 'word' && filename !== 'toc') {
      file.errors.push('Filename should be toc.doc or toc.docx')
    }

    if (workflow === 'pdf' && !validExtensions(name, file)) {
      file.errors.push(
        'File extension should be a .docx, .doc, .pdf , .xlsx, .xml',
      )
    }

    if (workflow === 'xml' && !validExtensions(name, file)) {
      file.errors.push(
        'File extension should be a .docx, .doc, .pdf , .xlsx, .xml',
      )
    }

    const size = file.size / (1024 * 1024 * 1024)

    if (file.size === 0) {
      file.errors.push('You are trying to upload an empty file')
    }

    if (size > 2) {
      file.errors.push(
        `${'Files should be less than 2GB. This file size is: '}${size.toFixed(
          2,
        )}GB`,
      )
    }

    return file
  })

  return files
}

export const validateSupportFile = files => {
  files.map(file => {
    // const parts = file.name.split('.')
    // const fileExtention = parts[parts.length - 1]

    const filename = file.name.replace(/\.[^/.]+$/, '')
    const regex = new RegExp('^[A-Za-z0-9-_\\.]+$')
    const whitespaceregex = /\s/

    // const ACCEPTED_TYPES =
    //   '.doc,.docx,application/msword,.pdf,.ppt,.txt,.xml,.csv,.xls ,.mov,.avi,.mpg,.mpeg,.mp4,.mkv,.flv,.wmv,.zip'

    // const validextentions = ACCEPTED_TYPES.split(',')

    // if (!validextentions.includes(`.${fileExtention}`)) {
    //   file.errors.push('Not a valid file type')
    // }

    if (whitespaceregex.test(filename)) {
      file.errors.push('Filenames must not contain spaces')
      file.status = 'inValid'
      return file
    }

    if (!regex.test(filename)) {
      file.errors.push(
        'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
      )
    }

    return file
  })

  return files
}

export const validateImagesFile = files => {
  files.map(file => {
    // const parts = file.name.split('.')
    // const fileExtention = parts[parts.length - 1]

    const filename = file.name.replace(/\.[^/.]+$/, '')
    const regex = new RegExp('^[A-Za-z0-9-_\\.]+$')
    const whitespaceregex = /\s/

    // const ACCEPTED_TYPES =
    //   '.doc,.docx,application/msword,.pdf,.ppt,.txt,.xml,.csv,.xls ,.mov,.avi,.mpg,.mpeg,.mp4,.mkv,.flv,.wmv,.zip'

    // const validextentions = ACCEPTED_TYPES.split(',')

    // if (!validextentions.includes(`.${fileExtention}`)) {
    //   file.errors.push('Not a valid file type')
    // }

    if (whitespaceregex.test(filename)) {
      file.errors.push('Filenames must not contain spaces')
      file.status = 'inValid'
      return file
    }

    if (!regex.test(filename)) {
      file.errors.push(
        'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
      )
    }

    return file
  })

  return files
}

export const validateSupplementaryFile = files => {
  files.map(file => {
    const filename = file.name.replace(/\.[^/.]+$/, '')
    const regex = new RegExp('^[A-Za-z0-9-_\\.]+$')
    const whitespaceregex = /\s/

    if (file.size / (1024 * 1024 * 1024) >= 1) {
      file.errors.push('The file size should not be bigger than 1GB')
      file.status = 'inValid'
      return file
    }

    if (whitespaceregex.test(filename)) {
      file.errors.push('Filenames must not contain spaces')
      file.status = 'inValid'
      return file
    }

    if (!regex.test(filename)) {
      file.errors.push(
        'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
      )
    }

    return file
  })

  return files
}
// export const validateSingleFile = (files, )

export const getPdfDowloadSettings = ({ book, organizationType }) => {
  let createDisplayPDFdownload = {
    show: false,
    eachChapter: false,
    wholeBook: false,
  }

  let displayPDFdownload = {
    show: false,
    eachChapter: false,
    wholeBook: false,
  }

  if (organizationType !== FUNDER) {
    switch (book.workflow) {
      case WORD:
        createDisplayPDFdownload = {
          show: true,
          eachChapter: true,
          wholeBook: true,
        }

        break
      case PDF:
      case XML:
        displayPDFdownload = {
          show: true,
          eachChapter: book.settings?.chapterIndependently || false,
          wholeBook: true,
        }

        break
      default:
      // code block
    }
  } else if (book.workflow === PDF) {
    if (book.settings?.author_manuscript) {
      createDisplayPDFdownload = {
        show: true,
        eachChapter: book.settings?.chapterIndependently,
        wholeBook: true,
      }
    } else {
      displayPDFdownload = {
        show: true,
        eachChapter: book.settings?.chapterIndependently,
        wholeBook: true,
      }
    }
  }

  return { createDisplayPDFdownload, displayPDFdownload }
}

export const getLongDate = dt =>
  moment.utc(dt).utcOffset(moment().utcOffset()).format('MM/DD/YYYY hh:mm A')

export const getFromNowDate = dt =>
  moment.utc(dt).utcOffset(moment().utcOffset()).fromNow()

export const WordBulkUploadText = () => (
  <div>
    Bulk upload only supports uploading one or more Word documents using the
    Bookshelf Word template and styled according to the specifications provided
    in the{' '}
    <Link
      target="_blank"
      to="https://preview.ncbi.nlm.nih.gov/books/NBK310885/"
    >
      NCBI Bookshelf Author Guide
    </Link>
    . <br />
    Supplementary files must be uploaded directly to the associated chapter.{' '}
    <br />
    Document filenames: <br />
    <ul>
      <li>
        should only contain letters, numbers, hyphens, dashes, periods, and
        underscores
      </li>

      <li>must not begin with a numeral </li>

      <li>must not contain spaces</li>
    </ul>
    Make sure each document has a unique filename. Duplicate filenames within
    one book will cause problems in the publishing process. <br />
    <div style={{ color: '#a56a00' }}>
      WARNING! Do not modify the Word document filename for an existing record
      when you are updating it.
    </div>
    Please note that it can take up to 20 to 30 minutes before a preview is
    available. Any errors in preparing the preview will be reported before a
    preview may be generated.
  </div>
)

export const PDFBulkUploadText = () => (
  <div>
    Bulk upload only supports uploading one or more PDF documents . <br />
    Supplementary files must be uploaded directly to the associated chapter.{' '}
    <br />
    Document filenames: <br />
    <ul>
      <li>
        should only contain letters, numbers, hyphens, dashes, periods, and
        underscores
      </li>

      <li>must not contain spaces</li>
    </ul>
    Make sure each document has a unique filename. Duplicate filenames within
    one book will cause problems in the publishing process. <br />
    <div style={{ color: '#a56a00' }}>
      WARNING! Do not modify the PDF document filename for an existing record
      when you are updating it.
    </div>
  </div>
)

export const XMLBulkUploadText = () => (
  <div>
    Bulk upload only supports uploading one or more XML documents. <br />
    Images and supplementary files must be uploaded directly to the associated
    chapter.
    <br />
    Document filenames: <br />
    <ul>
      <li>
        should only contain letters, numbers, hyphens, dashes, periods, and
        underscores
      </li>

      <li>must not contain spaces</li>
    </ul>
    Make sure each document has a unique filename. Duplicate filenames within
    one book will cause problems in the publishing process. <br />
    <div style={{ color: '#a56a00' }}>
      WARNING! Do not modify the XML document filename for an existing record
      when you are updating it.
    </div>
    <div>
      Please note that it can take up to 20 to 30 minutes before a preview is
      available. Any errors in preparing the preview will be reported before a
      preview may be generated.
    </div>
  </div>
)

export const validateDate = ({ dateType, date, dateRange }) => {
  let isValidDate = false

  if (date || dateRange) {
    if (dateType === 'pub') {
      const { day, month, year } = date

      if (!day && !month && year) {
        isValidDate = moment(`${year}`, 'YYYY', true).isValid()
      } else if (!day && month && year) {
        isValidDate = moment(`${month}/${year}`, 'MM/YYYY', true).isValid()
      } else if (day && month && year) {
        isValidDate = moment(
          `${day}/${month}/${year}`,
          'DD/MM/YYYY',
          true,
        ).isValid()
      }

      if (!year) {
        return `Date is required`
      }

      if (year && !isValidDate) {
        return `Not a valid date`
      }
    } else {
      const { startYear, endYear } = dateRange

      if (startYear && endYear) {
        isValidDate =
          moment(startYear, 'YYYY', true).isValid() &&
          moment(endYear, 'YYYY', true).isValid() &&
          startYear <= endYear
      } else {
        isValidDate = moment(startYear, 'YYYY', true).isValid()
      }

      if (!startYear) {
        return `Date is required`
      }

      if (startYear && !isValidDate) {
        return `Not a valid date range`
      }
    }
  }

  return null
}

function searchTree(element, id) {
  const array = []

  if (element.bookComponents?.includes(id)) {
    array.push(element.id)
  }

  if (element.parts?.length) {
    let result = []

    forEach(element.parts, (part, i) => {
      result = searchTree(element.parts[i], id)
      array.push(...result)
    })
  }

  return array
}

export const getSelectedParts = ({ bookComponentId, parts, bodyId }) => {
  const temp = []

  forEach(parts, part => {
    temp.push(...searchTree(part, bookComponentId))
  })

  return temp.length > 0 ? temp : [bodyId]
}
