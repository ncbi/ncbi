import moment from 'moment'

export function ValidDateCheck(val) {
  const { month, year, day } = val

  const date = [
    { value: month, pattern: 'MM' },
    { value: day, pattern: 'DD' },
    { value: year, pattern: 'YYYY' },
  ]

  const results = []
  const datePattern = []
  let inValid = false

  date.forEach(element => {
    if (element.value) {
      results.push(element.value)
      datePattern.push(element.pattern)
    }
  })

  inValid = moment(results.join('-'), datePattern.join('-'), true).isValid()

  return results.length > 0 ? inValid : !inValid
}

export function DayValidation(val) {
  const { year, month, day } = val
  let isValid = false

  if (!year && !month && day) {
    isValid = moment(year, 'YYYY', true).isValid()
  } else {
    isValid = true
  }

  return isValid
}

export function DayYearValidation(val) {
  const { month, year, day } = val
  let isValid = false

  if (day && year && !month) {
    isValid =
      moment(month, 'MM', true).isValid() &&
      moment(year, 'YYYY', true).isValid()
  } else {
    isValid = true
  }

  return isValid
}

export function MonthValidation(val) {
  const { month, year, day } = val
  let isValid = false

  if (!day && !year && month) {
    isValid =
      moment(month, 'MM', true).isValid() &&
      moment(year, 'YYYY', true).isValid()
  } else if (day && month) {
    isValid = moment(year, 'YYYY', true).isValid()
  } else {
    isValid = true
  }

  return isValid
}

export function DateOfPublication(val, parent) {
  const { publicationDateType } = parent
  if (!publicationDateType) return false

  const type = publicationDateType.split('-')[0]
  const isDate = type === 'pub'
  const isDateRange = type === 'pubr'

  let isValid = false

  // validate rage is either first year only (can include month), years range, or month-year range
  if (isDateRange) {
    const { startMonth, startYear, endMonth, endYear } = val

    if (endYear || endMonth || startMonth || startYear) {
      isValid =
        moment(startYear, 'YYYY', true).isValid() ||
        moment(endMonth, 'MM', true).isValid() ||
        moment(endYear, 'YYYY', true).isValid() ||
        moment(startMonth, 'MM', true).isValid()
    }
  }

  if (isDate) {
    const { year, month, day } = val

    isValid =
      moment(year, 'YYYY', true).isValid() ||
      moment(month, 'MM', true).isValid() ||
      moment(day, 'DD', true).isValid()
  }

  return isValid
}

export function PublicationMonthYearValidation(val, parent) {
  const { publicationDateType } = parent
  if (!publicationDateType) return false

  const { day, month, year } = val
  const type = publicationDateType.split('-')[0]
  const isDate = type === 'pub'
  let isValid = false

  if (isDate) {
    if (!day && !year && month) {
      isValid =
        moment(month, 'MM', true).isValid() &&
        moment(year, 'YYYY', true).isValid()
    } else if (day && month) {
      isValid = moment(year, 'YYYY', true).isValid()
    } else {
      isValid = true
    }
  } else {
    isValid = true
  }

  return isValid
}

export function PublicationStartRangeValidation(val, parent) {
  const { publicationDateType } = parent
  if (!publicationDateType) return false

  const { startMonth, startYear, endMonth, endYear } = val

  const type = publicationDateType.split('-')[0]
  const isDateRange = type === 'pubr'
  let isValid = false

  if (isDateRange) {
    if (!startMonth && !startYear && !endMonth && endYear) {
      isValid =
        moment(endYear, 'YYYY', true).isValid() &&
        moment(startYear, 'YYYY', true).isValid()
    } else if (!startMonth && !startYear && endMonth && endYear) {
      isValid =
        moment(startYear, 'YYYY', true).isValid() &&
        moment(startMonth, 'MM', true).isValid()
    } else {
      isValid = true
    }
  } else {
    isValid = true
  }

  return isValid
}

export function PublicationYearValidation(val, parent) {
  const { publicationDateType } = parent
  if (!publicationDateType) return false

  const { startMonth, startYear, endMonth, endYear } = val

  const type = publicationDateType.split('-')[0]
  const isDateRange = type === 'pubr'
  let isValid = false

  if (isDateRange) {
    if ((startMonth || endMonth) && !startYear && !endYear) {
      isValid =
        moment(endYear, 'YYYY', true).isValid() &&
        moment(startYear, 'YYYY', true).isValid()
    } else {
      isValid = true
    }
  } else {
    isValid = true
  }

  return isValid
}

export function YearRequiredValidation(val, parent) {
  const { publicationDateType } = parent
  if (!publicationDateType) return false

  const { startMonth, startYear, endMonth, endYear } = val

  const type = publicationDateType.split('-')[0]
  const isDateRange = type === 'pubr'
  let isValid = false

  if (isDateRange) {
    if (endMonth && startYear && !startMonth && !endYear) {
      isValid =
        moment(startMonth, 'MM', true).isValid() &&
        moment(endYear, 'YYYY', true).isValid()
    } else if (!endMonth && !startYear && startMonth && endYear) {
      isValid =
        moment(endMonth, 'MM', true).isValid() &&
        moment(startYear, 'YYYY', true).isValid()
    } else if (endMonth && !startYear && startMonth && endYear) {
      isValid = moment(startYear, 'YYYY', true).isValid()
    } else if (endMonth && startYear && startMonth && !endYear) {
      isValid = moment(endYear, 'YYYY', true).isValid()
    } else {
      isValid = true
    }
  } else {
    isValid = true
  }

  return isValid
}

export function StartMonthRequiredValidation(val, parent) {
  const { publicationDateType } = parent
  if (!publicationDateType) return false

  const { startMonth, startYear, endMonth, endYear } = val

  const type = publicationDateType.split('-')[0]
  const isDateRange = type === 'pubr'
  let isValid = false

  if (isDateRange) {
    if (endMonth && endYear && startYear && !startMonth) {
      isValid = moment(startMonth, 'MM', true).isValid()
    } else {
      isValid = true
    }
  } else {
    isValid = true
  }

  return isValid
}

export function EndMonthRequiredValidation(val, parent) {
  const { publicationDateType } = parent
  if (!publicationDateType) return false

  const { startMonth, startYear, endMonth, endYear } = val

  const type = publicationDateType.split('-')[0]
  const isDateRange = type === 'pubr'
  let isValid = false

  if (isDateRange) {
    if (!endMonth && endYear && startYear && startMonth) {
      isValid = moment(endMonth, 'MM', true).isValid()
    } else {
      isValid = true
    }
  } else {
    isValid = true
  }

  return isValid
}

export function publicationDateRequiredValidation(val, isDateRange) {
  let inValid = false

  if (isDateRange) {
    const { startMonth, startYear, endMonth, endYear } = val

    const startDate = [
      { value: startMonth, pattern: 'MM' },
      { value: startYear, pattern: 'YYYY' },
    ]

    const endDate = [
      { value: endMonth, pattern: 'MM' },
      { value: endYear, pattern: 'YYYY' },
    ]

    const startResults = []
    const startPattern = []

    const endResults = []
    const endPattern = []

    startDate.forEach(element => {
      if (element.value !== null && element.value !== undefined) {
        startResults.push(element.value)
        startPattern.push(element.pattern)
      }
    })

    endDate.forEach(element => {
      if (element.value !== null && element.value !== undefined) {
        endResults.push(element.value)
        endPattern.push(element.pattern)
      }
    })

    if (startMonth || endMonth) {
      return (
        moment(startYear, 'YYYY', true).isValid &&
        moment(endYear, 'YYYY', true).isValid() &&
        moment(endMonth, 'MM', true).isValid() &&
        moment(startMonth, 'MM', true).isValid()
      )
    }

    if (startResults.length > 0) {
      if (endResults.length > 0) {
        return (
          moment(
            startResults.join('-'),
            startPattern.join('-'),
            true,
          ).isValid() &&
          moment(endResults.join('-'), endPattern.join('-'), true).isValid()
        )
      }

      return moment(
        startResults.join('-'),
        startPattern.join('-'),
        true,
      ).isValid()
    }

    return moment(endResults.join('-'), endPattern.join('-'), true).isValid()
  }

  const { day, month, year } = val

  const date = [
    { value: month, pattern: 'MM' },
    { value: day, pattern: 'DD' },
    { value: year, pattern: 'YYYY' },
  ]

  const results = []
  const datePattern = []

  date.forEach(element => {
    if (element.value !== null && element.value !== undefined) {
      results.push(element.value)
      datePattern.push(element.pattern)
    }
  })

  inValid = moment(results.join('-'), datePattern.join('-'), true).isValid()

  return results.length > 0 ? inValid : !inValid
}

export function isEmptyPublicationDate(val, isDateRange) {
  if (isDateRange) {
    const { endMonth, endYear, startMonth, startYear } = val

    if (!!startMonth || !!startYear || !!endMonth || !!endYear) {
      return false
    }

    return 'Date of publication is required'
  }

  const { day, month, year } = val

  if (!!day || !!month || !!year) return false
  return 'Date of publication is required'
}

export const dateValidations = value => {
  const { day, month, year } = value
  const [hasDay, hasMonth, hasYear] = [!!day, !!month, !!year]

  // null/null/YEAR
  if (!hasMonth && !hasDay && hasYear) {
    if (!ValidDateCheck({ year })) return 'Not a valid year'
    return false
  }

  // MM/null/YEAR	-	Yes
  if (hasMonth && !hasDay && hasYear) {
    if (!ValidDateCheck({ month, year })) return 'Not a valid date'
    return false
  }

  // MM/DD/YEAR	-	Yes
  if (hasMonth && hasDay && hasYear) {
    if (!ValidDateCheck({ day, month, year })) return "This date doesn't exist"
    return false
  }

  // null/DD/null
  if (!hasMonth && hasDay && !hasYear)
    return 'If day is provided, month and year must be provided too'
  // null/DD/YEAR
  if (!hasMonth && hasDay && hasYear)
    return 'If day is provided, month must be provided too'
  // MM/null/null
  if (!hasMonth && !hasDay && !hasYear)
    return 'If month is provided, year must be provided too'
  // MM/DD/null
  if (hasMonth && hasDay && !hasYear) return 'Year of publication is required'

  return false
}

export const dateRangeValidations = value => {
  const { startDay, startMonth, startYear, endDay, endMonth, endYear } = value

  const [hasStartDay, hasStartMonth, hasStartYear] = [
    !!startDay,
    !!startMonth,
    !!startYear,
  ]

  const [hasEndDay, hasEndMonth, hasEndYear] = [!!endDay, !!endMonth, !!endYear]

  // null/null/YEAR-null/null/null	-	Yes
  if (
    !hasStartDay &&
    !hasStartMonth &&
    hasStartYear &&
    !hasEndDay &&
    !hasEndMonth &&
    !hasEndYear
  ) {
    if (!ValidDateCheck({ year: startYear })) return 'Not a valid year'
    return false
  }

  // MM/null/YEAR-null/null/null	-	Yes
  if (
    !hasStartDay &&
    hasStartMonth &&
    hasStartYear &&
    !hasEndDay &&
    !hasEndMonth &&
    !hasEndYear
  ) {
    if (!ValidDateCheck({ month: startMonth, year: startYear }))
      return 'Not a valid date'
    return false
  }

  // MM/DD/YEAR-null/null/null	-	Yes
  if (
    hasStartDay &&
    hasStartMonth &&
    hasStartYear &&
    !hasEndDay &&
    !hasEndMonth &&
    !hasEndYear
  ) {
    if (!ValidDateCheck({ day: startDay, month: startMonth, year: startYear }))
      return "This date doesn't exist"
    return false
  }

  // null/null/YEAR-MM/null/YEAR
  if (!hasStartMonth && hasStartYear && hasEndMonth && hasEndYear) {
    return 'If a closing month for the range is provided, a start month must be provided too'
  }

  // null/null/YEAR-MM/null/null
  if (!hasStartMonth && hasStartYear && hasEndMonth && !hasEndYear) {
    return 'If month is provided, year must be provided too'
  }

  // null/null/null-MM/null/null
  if (!hasStartMonth && !hasStartYear && hasEndMonth && !hasEndYear) {
    return 'Year of publication is required'
  }

  // MM/null/null-null/null/YEAR
  if (hasStartMonth && !hasStartYear && !hasEndMonth && hasEndYear) {
    return 'If month is provided, year must be provided too'
  }

  // MM/null/null-MM/null/null
  if (hasStartMonth && !hasStartYear && hasEndMonth && !hasEndYear) {
    return 'Year of publication is required'
  }

  // MM/null/null-MM/null/YEAR
  if (hasStartMonth && !hasStartYear && hasEndMonth && hasEndYear) {
    return 'If month is provided, year must be provided too'
  }

  // MM/null/YYYY-MM/null/null
  if (hasStartMonth && hasStartYear && hasEndMonth && !hasEndYear) {
    return 'If month is provided, year must be provided too'
  }

  // MM/null/YEAR-null/null/YEAR	 	No
  if (hasStartMonth && hasStartYear && !hasEndMonth && hasEndYear) {
    return 'If a start month for the range is provided, a closing month must be provided too'
  }

  if (!hasStartYear) {
    return 'The year the publication range starts is required'
  }

  if (
    !hasStartDay &&
    hasStartMonth &&
    hasStartYear &&
    !hasStartDay &&
    hasEndMonth &&
    hasEndYear
  ) {
    if (!ValidDateCheck({ month: startMonth, year: startYear })) {
      return `Start date does not exist`
    }

    if (!ValidDateCheck({ month: endMonth, year: endYear })) {
      return `End date does not exist`
    }

    if (
      moment(`${startYear}-${startMonth}`).isAfter(`${endYear}-${endMonth}`)
    ) {
      return `Start date should be earlier than end date`
    }

    return false
  }

  if (
    hasStartDay &&
    hasStartMonth &&
    hasStartYear &&
    hasStartDay &&
    hasEndMonth &&
    hasEndYear
  ) {
    if (
      !ValidDateCheck({ day: startDay, month: startMonth, year: startYear })
    ) {
      return `Start date does not exist`
    }

    if (!ValidDateCheck({ day: endDay, month: endMonth, year: endYear })) {
      return `End date does not exist`
    }

    if (
      moment(`${startYear}-${startMonth}-${startDay}`).isAfter(
        `${endYear}-${endMonth}-${endDay}`,
      )
    ) {
      return `Start date should be earlier than end date`
    }

    return false
  }

  if (
    !hasStartDay &&
    !hasStartMonth &&
    hasStartYear &&
    !hasStartDay &&
    !hasEndMonth &&
    hasEndYear
  ) {
    if (!ValidDateCheck({ year: startYear })) return 'Not a valid start year'
    if (!ValidDateCheck({ year: endYear })) return 'Not a valid end year'

    if (startYear > endYear)
      return 'End date should be larger or equal with start date '

    return false
  }

  if (!hasStartMonth && hasStartYear && !hasEndMonth && hasEndYear) return false

  if (hasStartMonth && hasStartYear && hasEndMonth && hasEndYear) return false

  return 'Not a valid date range'
}

export const validatePublicationDate = (
  value,
  { parent, createError, path },
) => {
  const { publicationDateType } = parent

  const isDateRange = publicationDateType
    ? publicationDateType?.split('-')[0] === 'pubr'
    : true

  const emptyPublicationDate = isEmptyPublicationDate(value, isDateRange)

  if (emptyPublicationDate) {
    return createError({
      path,
      message: emptyPublicationDate,
    })
  }

  const singleDateValidations = !isDateRange && dateValidations(value)

  if (singleDateValidations) {
    return createError({
      path,
      message: singleDateValidations,
    })
  }

  const rangeDateValidations = isDateRange && dateRangeValidations(value)

  if (rangeDateValidations) {
    return createError({
      path,
      message: rangeDateValidations,
    })
  }

  return true
}
