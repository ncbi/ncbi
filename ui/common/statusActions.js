import { isEmpty } from 'lodash'
import { getLatestVersionFiles } from '../components/FilesSections/common'

export const inReviewConverting = ({ status, sourceFiles, convertedFile }) => {
  if (status === 'in-review') {
    return (
      [('converting', 'loading-preview')].includes(sourceFiles[0].status) ||
      [('converting', 'loading-preview')].includes(convertedFile[0].status)
    )
  }

  return false
}

export const disableReloadPreview = ({
  status,
  sourceFiles = [],
  convertedFile = [],
}) => {
  if (['preview', 'approved'].includes(status)) {
    return false
  }

  if (
    status === 'converting' ||
    (status === 'failed' && convertedFile.length === 0)
  )
    return true

  if (convertedFile.length === 0) {
    return true
  }

  if (
    convertedFile.length > 0 &&
    ['new-upload', 'failed', 'loading-errors'].includes(convertedFile[0].status)
  ) {
    return false
  }

  if (sourceFiles.length > 0 && sourceFiles[0].status === 'new-upload') {
    return true
  }

  if (inReviewConverting({ status, sourceFiles, convertedFile })) return true

  return [
    'submision-errors',
    'conversion-errors',
    'error',
    'converting',
    'loading-preview',
    'failed',
    'tagging',
    'publishing',
    'published',
    'new-book',
    'publish-failed',
  ].includes(status)
}

export const disabledSubmit = ({ status, sourceFiles = [] }) => {
  if (sourceFiles.length === 0) {
    return true
  }

  if (
    status === 'tagging' ||
    (status === 'new-upload' && sourceFiles[0].status === 'new-upload') ||
    status === 'failed'
  ) {
    return false
  }

  if (status === 'in-review') {
    return sourceFiles[0].status !== 'new-upload'
  }

  return true
}

export const disablePublish = ({
  url,
  status,
  sourceFiles = [],
  convertedFile = [],
}) => {
  // if we have preview link
  if (!isEmpty(url)) {
    return (
      ![
        'published',
        'in-review',
        'preview',
        'approved',
        'publish-failed',
      ].includes(status) ||
      inReviewConverting({ status, sourceFiles, convertedFile })
    )
  }

  return true
}

export const disableUploadSource = ({
  status,
  sourceFiles = [],
  convertedFile = [],
}) =>
  [
    'error',
    'converting',
    'failed',
    'loading-preview',
    'publishing',
    'publish-failed',
  ].includes(status) ||
  inReviewConverting({ status, sourceFiles, convertedFile })

export const disableUploadConverted = ({
  status,
  sourceFiles = [],
  convertedFile = [],
}) =>
  [
    'error',
    'submission-errors',
    'converting',
    'tagging',
    'failed',
    'loading-preview',
    'publishing',
    'publish-failed',
  ].includes(status) ||
  inReviewConverting({ status, sourceFiles, convertedFile })

export const disableUploadSupplementary = ({
  status,
  sourceFiles = [],
  convertedFile = [],
}) =>
  [
    'converting',
    'loading-preview',
    'failed',
    'publishing',
    'published',
    'publish-failed',
  ].includes(status) ||
  inReviewConverting({ status, sourceFiles, convertedFile })

export const disableUploadImages = disableUploadSupplementary

export const disableUploadSupport = ({ status, sourceFiles, convertedFile }) =>
  [
    'converting',
    'loading-preview',
    'loading-errors',
    'failed',
    'preview',
    'publishing',
    'published',
    'publish-failed',
  ].includes(status) ||
  inReviewConverting({ status, sourceFiles, convertedFile })

export const disableUploadPDFs = ({
  status,
  sourceFiles = [],
  convertedFile = [],
}) =>
  [
    'converting',
    'tagging',
    'loading-preview',
    'failed',
    'publishing',
    'published',
    'publish-failed',
  ].includes(status) ||
  inReviewConverting({ status, sourceFiles, convertedFile })

export const areFilesTaged = ({
  workflow,
  chapterIndependently,
  sourceFiles = [],
}) => {
  if (workflow === 'xml' && !chapterIndependently) {
    const tagedSource =
      getLatestVersionFiles(sourceFiles).filter(fl => fl.tag === 'main_xml')
        .length > 0

    // const tagedPdf =
    //   pdf.length > 0
    //     ? getLatestVersionFiles(pdf).filter(fl => fl.tag === 'book_pdf')
    //         .length > 0
    //     : true

    return tagedSource
  }

  return true
}
