import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Formik, Form } from 'formik'
import * as yup from 'yup'

import { grid } from '@pubsweet/ui-toolkit'

import ConstantsContext from '../../../app/constantsContext'
import {
  DateInputComponent as DateInput,
  EditorComponent as Editor,
  SelectComponent as Select,
  TextFieldComponent as TextField,
} from '../../components/FormElements/FormikElements'
import { Button } from '../../components/common'
import FormSection from '../../components/common/FormSection'
import { validatePublicationDate } from '../../common/metadataDateValidation'

// #region styled
const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 800px;

  > form > div:not(:last-child) {
    margin-bottom: ${grid(2)};
  }
`

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: end;
`
// #endregion styled

const initialValues = {
  chapterProcessedSourceType: null,
  collectionType: '',
  dateOfPublication: { startYear: null, endYear: null },
  publicationDateType: null,
  publisherLocation: '',
  publisherName: '',
  title: '',
  wholeBookSourceType: null,
}

const validations = yup.object().shape({
  collectionType: yup.string().required('Collection type is required'),
  title: yup.string().required('Title is required'),
  publisherLocation: yup.string().required('Publisher location is required'),
  publisherName: yup.string().required('Publisher name is required'),
  publicationDateType: yup
    .string()
    .nullable()
    .required('Publication date type is required'),
  dateOfPublication: yup
    .object()
    .shape({
      startYear: yup.string().nullable(),
      endYear: yup.string().nullable(),
    })
    .test('validator-custom', (value, { parent, createError, path }) => {
      const customValidation = validatePublicationDate(value, {
        parent,
        createError,
        path,
      })

      if (customValidation) return customValidation
      return true
    }),
  chapterProcessedSourceType: yup
    .string()
    .nullable()
    .required('Chapter processed source type is required'),
  wholeBookSourceType: yup
    .string()
    .nullable()
    .required('Whole book source type is required'),
})

const StepOne = props => {
  const {
    className,
    bookSeriesCollectionsEnabled,
    fundedCollectionsEnabled,
    onSubmit,
  } = props

  const { s } = useContext(ConstantsContext)

  const collectionTypeOptions = [
    {
      label: 'Funded Collection',
      value: 'funded',
      isDisabled: !fundedCollectionsEnabled,
    },
    {
      label: 'Book Series Collection',
      value: 'bookSeries',
      isDisabled: !bookSeriesCollectionsEnabled,
    },
  ]

  const publicationDateTypeOptions = [
    { label: 'Print publication range', value: 'pubr-print' },
    {
      label: 'Electronic publication range',
      value: 'pubr-electronic',
    },
  ]

  const sourceTypeOptions = [
    { label: 'Report', value: 'Report' },
    { label: 'Book', value: 'Book' },
    { label: 'Database', value: 'Database' },
    { label: 'Documentation', value: 'Documentation' },
  ]

  const handleSubmit = v => onSubmit(v)

  return (
    <Wrapper className={className}>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const { values } = formProps

          const {
            collectionType,
            dateOfPublication,
            publisherLocation,
            publisherName,
            title,
          } = values

          return (
            <Form noValidate>
              <Select
                data-test-id="collection-type"
                field={{ name: 'collectionType', value: collectionType }}
                form={formProps}
                label={s('form.collectionType')}
                menuPortalTarget={document.body}
                options={collectionTypeOptions}
                required
              />

              <Editor
                editorType="inline"
                field={{ name: 'title', value: title }}
                form={formProps}
                label={s('form.title')}
                name="title"
                required
              />

              <FormSection label={s('form.publisher')}>
                <TextField
                  field={{ name: 'publisherName', value: publisherName }}
                  form={formProps}
                  label={s('form.publisherName')}
                  name="publisherName"
                  required
                />

                <TextField
                  field={{
                    name: 'publisherLocation',
                    value: publisherLocation,
                  }}
                  form={formProps}
                  label={s('form.publisherLocation')}
                  name="publisherLocation"
                  required
                />
              </FormSection>

              <FormSection label={s('form.publisherDate')}>
                <Select
                  data-test-id="pubdate-type-select"
                  field={{
                    name: 'publicationDateType',
                    value: values.publicationDateType,
                  }}
                  form={formProps}
                  label={s('form.type')}
                  options={publicationDateTypeOptions}
                  required
                />

                <DateInput
                  field={{
                    name: 'dateOfPublication',
                    value: dateOfPublication,
                  }}
                  form={formProps}
                  isDateRange
                  label={s('form.dateOfPublication')}
                  name="dateOfPublication"
                  required
                />
              </FormSection>

              <FormSection label={s('form.NCBICustomMeta')}>
                <Select
                  data-test-id="wholeBookSourceType-select"
                  field={{
                    name: 'wholeBookSourceType',
                    value: values.wholeBookSourceType,
                  }}
                  form={formProps}
                  label={s('form.wholeBookSourceType')}
                  options={sourceTypeOptions}
                  required
                />

                <Select
                  data-test-id="chapterProcessedSourceType-select"
                  field={{
                    name: 'chapterProcessedSourceType',
                    value: values.chapterProcessedSourceType,
                  }}
                  form={formProps}
                  label={s('form.chapterProcessedSourceType')}
                  options={sourceTypeOptions}
                  required
                />
              </FormSection>

              <ButtonWrapper>
                <Button data-test-id="next-step" status="primary" type="submit">
                  Next
                </Button>
              </ButtonWrapper>
            </Form>
          )
        }}
      </Formik>
    </Wrapper>
  )
}

StepOne.propTypes = {
  bookSeriesCollectionsEnabled: PropTypes.bool.isRequired,
  fundedCollectionsEnabled: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
}

StepOne.defaultProps = {}

export default StepOne
