import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Steps } from '@pubsweet/ui'

import { Spinner } from '../../components/common'
import StepOne from './StepOne'
import CollectionSettings from '../Collectionmanager/Form/CollectionSettingsForm'

// #region styled
const Wrapper = styled.div`
  height: 100%;
  padding: 30px 20px 50px 20px;
`

const StepsWrapper = styled.div`
  margin: 0 auto 30px auto;
  max-width: 600px;
`

const Main = styled.div`
  height: calc(100% - 131px);
`
// #endregion styled

const CreateCollection = props => {
  const {
    className,
    currentStep,
    loading,

    bookSeriesCollectionsEnabled,
    fundedCollectionsEnabled,
    onStepOneSubmit,
    isStepOneSaving,

    stepTwoInitialValues,
    onStepTwoSubmit,
    isStepTwoSaving,
  } = props

  if (loading) return <Spinner pageLoader />

  const isSaving = isStepOneSaving || isStepTwoSaving

  return (
    <Wrapper className={className}>
      <StepsWrapper>
        <Steps currentStep={currentStep}>
          <Steps.Step title="Create collection" />
          <Steps.Step title="Collection settings" />
        </Steps>
      </StepsWrapper>

      <Main>
        {isStepOneSaving && (
          <Spinner label="Creating collection..." pageLoader />
        )}

        {isStepTwoSaving && (
          <Spinner label="Updating collection settings..." pageLoader />
        )}

        {!isSaving && currentStep === 0 && (
          <StepOne
            bookSeriesCollectionsEnabled={bookSeriesCollectionsEnabled}
            fundedCollectionsEnabled={fundedCollectionsEnabled}
            onSubmit={onStepOneSubmit}
          />
        )}

        {!isSaving && currentStep === 1 && stepTwoInitialValues && (
          <CollectionSettings
            initialFormValues={stepTwoInitialValues}
            isSaving={isStepTwoSaving}
            onSubmit={onStepTwoSubmit}
            showSubmitButton
          />
        )}
      </Main>
    </Wrapper>
  )
}

CreateCollection.propTypes = {
  currentStep: PropTypes.oneOf([0, 1]).isRequired,

  bookSeriesCollectionsEnabled: PropTypes.bool.isRequired,
  fundedCollectionsEnabled: PropTypes.bool.isRequired,

  onStepOneSubmit: PropTypes.func.isRequired,
  onStepTwoSubmit: PropTypes.func.isRequired,

  loading: PropTypes.bool,
  isStepOneSaving: PropTypes.bool,
  isStepTwoSaving: PropTypes.bool,

  /* eslint-disable-next-line react/forbid-prop-types */
  stepTwoInitialValues: PropTypes.object,
}

CreateCollection.defaultProps = {
  loading: false,
  isStepOneSaving: false,
  isStepTwoSaving: false,
  stepTwoInitialValues: null,
}

export default CreateCollection
