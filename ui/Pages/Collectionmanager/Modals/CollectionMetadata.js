import React, { useState, useRef, useContext } from 'react'
import PropTypes from 'prop-types'

import {
  Button,
  ButtonGroup,
  DragAndDrop,
  FixedLoader,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from '../../../components'
import MetadataForm from '../../../components/MetadataForm'
import ConstantsContext from '../../../../app/constantsContext'
import DisplayInlineEditor from '../../../components/wax/DisplayInlneEditor'
import PermissionsGate from '../../../../app/components/PermissionsGate'

const CollectionMetadata = props => {
  const {
    data,
    loading,
    isBookSeriesCollection,
    isFundedCollection,
    isSaving,
    searchGranthub,
    update,
  } = props

  const [modalIsOpen, setIsOpen] = useState(false)
  const { s } = useContext(ConstantsContext)
  const formRef = useRef(null)

  const handleSubmit = async () => {
    formRef.current.handleSubmit()
  }

  const toggleModal = () => setIsOpen(!modalIsOpen)

  return (
    <>
      <Button
        data-test-id="collectionMetadata"
        onClick={toggleModal}
        outlined
        status="primary"
      >
        {s('form.metadata')}
      </Button>

      <Modal fullSize isOpen={modalIsOpen} onClose={toggleModal}>
        <ModalHeader hideModal={toggleModal}>
          Metadata:&nbsp;
          <DisplayInlineEditor content={data.title} />
        </ModalHeader>

        <ModalBody noPadding>
          <DragAndDrop>
            <PermissionsGate scopes={['edit-collection-metadata']}>
              <MetadataForm
                initialFormValues={data}
                innerRef={formRef}
                isBookSeriesCollection={isBookSeriesCollection}
                isFundedCollection={isFundedCollection}
                isSaving={isSaving}
                onSubmit={update}
                searchGranthub={searchGranthub}
                variant="collection"
              />
            </PermissionsGate>
          </DragAndDrop>
        </ModalBody>

        <ModalFooter>
          <ButtonGroup>
            <PermissionsGate scopes={['edit-collection-metadata']}>
              <Button
                data-test-id="save-collection-metadata"
                disabled={isSaving}
                loading={isSaving}
                onClick={handleSubmit}
                outline
                status="primary"
              >
                {loading ? s('form.saving') : s('form.save')}
              </Button>
            </PermissionsGate>

            <Button
              data-test-id="cancel-collection-metadata"
              onClick={() => setIsOpen(false)}
              outlined
              size="small"
              status="primary"
            >
              {loading ? s('form.close') : s('form.cancel')}
            </Button>
          </ButtonGroup>
        </ModalFooter>
      </Modal>

      {loading && <FixedLoader title={s('form.savingCollectionMeta')} />}
    </>
  )
}

CollectionMetadata.propTypes = {
  searchGranthub: PropTypes.func.isRequired,
  update: PropTypes.func.isRequired,

  /* eslint-disable-next-line react/forbid-prop-types */
  data: PropTypes.object,
  loading: PropTypes.bool,
  isBookSeriesCollection: PropTypes.bool,
  isFundedCollection: PropTypes.bool,
  isSaving: PropTypes.bool,
}

CollectionMetadata.defaultProps = {
  data: {},
  isBookSeriesCollection: false,
  isFundedCollection: false,
  loading: false,
  isSaving: false,
}

export default CollectionMetadata
