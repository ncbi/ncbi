/* eslint-disable array-callback-return */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */

import React, { useState } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import Select from 'react-select'
import { useMutation } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  FixedLoader,
} from '../../../components'

import query from '../graphql'

const Root = styled.div`
  font-family: ${th('fontInterface')};
`

const BookComponents = styled.div`
  display: block;
  font-family: ${th('fontInterface')};
  margin-left: 30px;
`

export default ({
  books,
  collectionId,
  setSelectedRows,
  setSelectAll,
  groups,
  disabled,
}) => {
  const [modalIsOpen, setIsOpen] = useState(false)
  const [selectedOption, setSelectedOption] = useState()

  const [updateCollectionOrder, { loading }] = useMutation(
    query.UPDATE_CUSTOM_GROUP_ORDER,
  )

  const optionsList = groups.map(item => ({
    label: item.title,
    value: item.id,
  }))

  return (
    <>
      <Button
        data-test-id="move-chapters-to"
        disabled={disabled}
        icon="corner-left-up"
        onClick={e => {
          setIsOpen(true)
        }}
        outlined
        status="primary"
      >
        {loading ? 'Moving...' : 'Move'}
      </Button>
      <Root>
        <Modal isOpen={modalIsOpen}>
          <ModalHeader hideModal={() => setIsOpen(false)}>
            Move books to
          </ModalHeader>
          <ModalBody>
            {books.map(item => (
              <BookComponents key={item.title}>
                {(item.title || '').replace(/<[^>]+>/g, '')}
              </BookComponents>
            ))}

            <br />
            <div>Search or select group</div>
            <Select
              data-test-id="group-select"
              menuPortalTarget={document.body}
              name="group-select"
              onChange={selected => {
                if (selected) {
                  setSelectedOption(selected.value)
                } else {
                  setSelectedOption(null)
                }
              }}
              options={optionsList}
            />
          </ModalBody>
          <ModalFooter>
            <Button
              data-test-id="save-book-metadata"
              disabled={!selectedOption}
              isClearable
              onClick={async () => {
                if (books.length) {
                  const finalPostObject = {
                    parentCollectionId: collectionId,
                    collectionId: selectedOption,
                    ids: books.map(x => x.id),
                  }

                  updateCollectionOrder({
                    refetchQueries: [getOperationName(query.GET_COLLECTION)],
                    variables: finalPostObject,
                  }).catch(res => {
                    console.error(res)

                    Notification({
                      message: 'Could not update',
                      type: 'danger',
                      title: 'Error',
                    })
                  })

                  setSelectedRows([])
                  setSelectAll(false)
                  setIsOpen(false)
                }
              }}
              status="primary"
            >
              {loading ? 'Saving...' : 'Save'}
            </Button>

            <Button
              data-test-id="cancel-book-metadata"
              onClick={() => setIsOpen(false)}
              outlined
              status="primary"
            >
              {loading ? 'Close' : 'Cancel'}
            </Button>
          </ModalFooter>
        </Modal>
      </Root>
      {loading && <FixedLoader title="Updating order..." />}
    </>
  )
}
