/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */

import React, { useState, useRef } from 'react'
import { useMutation } from '@apollo/client'
import styled from 'styled-components'
import { grid, th, darken } from '@pubsweet/ui-toolkit'
import { getOperationName } from '@apollo/client/utilities'
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Notification,
  FixedLoader,
} from '../../../components'
import UIDisplayInlineEditor from '../../../components/wax/DisplayInlneEditor'
// eslint-disable-next-line import/no-cycle
import { AddGroupForm } from '../index'
import query from '../graphql'

const Root = styled.div`
  font-family: ${th('fontInterface')};
`

const DisplayInlineEditor = styled(UIDisplayInlineEditor)`
  margin-left: ${grid(1)};
`

export default ({ collection, values = {}, modalIsOpen, setIsOpen }) => {
  const modalRef = useRef(null)

  const [createCustomGroup, { loading }] = useMutation(
    query.CREATE_CUSTOM_GROUP,
  )

  const [updateCustomGroup, { loading: loadingEdit }] = useMutation(
    query.UPDATE_CUSTOM_GROUP,
  )

  const hideModal = () => setIsOpen(false)
  return (
    <Root>
      <Modal isOpen={modalIsOpen} onClose={hideModal}>
        <ModalHeader hideModal={hideModal}>
          {values.title ? (
            'Modify Group'
          ) : (
            <>
              Add new Group <DisplayInlineEditor content={collection.title} />
            </>
          )}
        </ModalHeader>
        <ModalBody>
          <AddGroupForm formikFormRef={modalRef} values={values} />
        </ModalBody>
        <ModalFooter>
          <Button
            data-test-id="save-book-metadata"
            onClick={async () => {
              const formData = await modalRef.current.triggerSave()

              if (formData) {
                // .log(addGroup)

                if (values.id) {
                  const input = {
                    ...formData,
                  }

                  await updateCustomGroup({
                    refetchQueries: [getOperationName(query.GET_COLLECTION)],
                    variables: {
                      id: values.id,
                      input,
                    },
                  })
                    .then(() => {
                      setIsOpen(false)

                      Notification({
                        type: 'success',
                        message: 'Custom group was updated',
                      })
                    })
                    .catch(res => {
                      Notification({
                        type: 'danger',
                        message: `Custom group  could not be saved${res.message}`,
                      })

                      console.error(res)
                    })
                } else {
                  createCustomGroup({
                    refetchQueries: [getOperationName(query.GET_COLLECTION)],
                    variables: {
                      id: collection.id,
                      input: {
                        ...formData,
                        organisationId: collection.organisation.id,
                      },
                    },
                  })
                    .then(() => {
                      setIsOpen(false)

                      Notification({
                        type: 'success',
                        message: 'Custom group was created',
                      })
                    })
                    .catch(res => {
                      Notification({
                        type: 'danger',
                        message: `Custom group  could not be created${res.message}`,
                      })

                      console.error(res)
                    })
                }

                setIsOpen(false)
                Object.assign(collection.settings, formData.settings)
              }
            }}
            status="primary"
          >
            Save
          </Button>
          <Button
            data-test-id="cancel-book-metadata"
            onClick={hideModal}
            outlined
            status="primary"
          >
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
      {loadingEdit ||
        (loading && <FixedLoader title="Saving custom group..." />)}
    </Root>
  )
}
