/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */

import React, { useState, useRef } from 'react'
// import { useMutation } from '@apollo/client'
import styled from 'styled-components'
import { Action as ActionButton } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
} from '../../../components'
// eslint-disable-next-line import/no-cycle
import { CollectionTeamForm } from '../index'

const Root = styled.div`
  font-family: ${th('fontInterface')};
`

const FlexSpace = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  width: 100%;
`

export default ({ book, values = {} }) => {
  const [modalIsOpen, setIsOpen] = useState(false)
  const modalRef = useRef(null)

  return (
    <>
      <ActionButton
        color={th('colorTextReverse')}
        data-test-id="collectionTeam"
        icon="plus"
        onClick={e => {
          setIsOpen(true)
        }}
        outlined
        size="small"
      >
        Collection team
      </ActionButton>

      <Root>
        <Modal isOpen={modalIsOpen}>
          <ModalHeader hideModal={() => setIsOpen(false)}>
            Collection team
          </ModalHeader>
          <ModalBody>
            <CollectionTeamForm formikFormRef={modalRef} values={values} />
          </ModalBody>
          <ModalFooter>
            <FlexSpace>
              <div>
                <Button
                  data-test-id="save-book-metadata"
                  onClick={async () => {
                    const formData = await modalRef.current.triggerSave()

                    if (formData) {
                      if (values.id) {
                        // const input = {
                        //   id: values.id,
                        //   title: formData.name,
                        //   abstract: formData.content,
                        //   metadata: {
                        //     author: formData.metadata.editor,
                        //   },
                        // }
                        // await updateBookComponent({
                        //   variables: {
                        //     id: values.id,
                        //     input,
                        //   },
                        // })
                      }
                    }
                  }}
                  status="primary"
                >
                  Save
                </Button>
                <Button
                  data-test-id="cancel-book-metadata"
                  onClick={() => setIsOpen(false)}
                  outlined
                  status="primary"
                >
                  Cancel
                </Button>
              </div>

              <Button
                data-test-id="cancel-book-metadata"
                icon="trash-2"
                iconPosition="end"
                onClick={() => setIsOpen(false)}
                outlined
                status="danger"
              >
                Remove from team
              </Button>
            </FlexSpace>
          </ModalFooter>
        </Modal>
      </Root>
    </>
  )
}
