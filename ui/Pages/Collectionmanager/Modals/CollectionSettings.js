import React, { useState, useRef } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import {
  Modal as UIModal,
  ModalBody as UIModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Tabs as UITabs,
} from '../../../components'
// eslint-disable-next-line import/no-cycle
import { CollectionSettingsForm } from '../index'
import BookSettingsTemplateChooser from '../../Dashboard/BookSettingsTemplateChooser'
import DisplayInlineEditor from '../../../components/wax/DisplayInlneEditor'
import PermissionsGate from '../../../../app/components/PermissionsGate'

const Modal = styled(UIModal)`
  height: 810px;
  max-height: 90vh;
  min-height: unset;
`

const ModalBody = styled(UIModalBody)`
  height: 100%;
  margin: 0;
  padding: 0;
`

const Tabs = styled(UITabs)``

const CollectionSettings = props => {
  const {
    createTemplate,
    collectionTitle,
    isBookSeriesCollection,
    isEditor,
    isOrgAdmin,
    isSavingSettings,
    isSavingTemplate,
    isSysAdmin,
    saveCollectionSettings,
    settingsValues,
    templates,
    templatesLoading,
    updateTemplate,
  } = props

  const [modalIsOpen, setIsOpen] = useState(false)

  const [currentTab, setCurrentTab] = useState('settings')

  // store chosen template so that it doesn't disappear if you switch tabs
  const [chosenBookSettingsTemplate, setChosenBookSettingsTemplate] = useState(
    null,
  )

  const settingsFormRef = useRef(null)
  const templatesFormRef = useRef(null)

  const toggleModal = () => setIsOpen(!modalIsOpen)

  const handleChooseBookSettingsTemplate = choice => {
    setChosenBookSettingsTemplate(choice)
  }

  const settingsForm = (
    <PermissionsGate scopes={['edit-collection-setting']}>
      <CollectionSettingsForm
        initialFormValues={settingsValues}
        innerRef={settingsFormRef}
        isSaving={isSavingSettings}
        isSysAdmin={isSysAdmin}
        onSubmit={saveCollectionSettings}
        showSubmitButton={false}
      />
    </PermissionsGate>
  )

  const handleClickSave = () => {
    if (currentTab === 'settings') {
      settingsFormRef.current.handleSubmit()
    } else if (currentTab === 'templates') {
      templatesFormRef.current.handleSubmit()
    }
  }

  let body = null

  if (isBookSeriesCollection) {
    const tabSections = [
      {
        label: 'Collection settings',
        key: 'settings',
        content: settingsForm,
      },
      {
        label: "Collection members' settings templates",
        key: 'templates',
        content: (
          <BookSettingsTemplateChooser
            chapterProcessedTemplate={templates?.chapterProcessed}
            chosenTemplate={chosenBookSettingsTemplate}
            createTemplate={createTemplate}
            innerRef={templatesFormRef}
            isEditor={isEditor}
            isOrgAdmin={isOrgAdmin}
            isSaving={isSavingTemplate}
            isSysAdmin={isSysAdmin}
            loading={templatesLoading}
            onChooseTemplate={handleChooseBookSettingsTemplate}
            showSubmitButton={false}
            updateTemplate={updateTemplate}
            wholeBookTemplate={templates?.wholeBook}
          />
        ),
      },
    ]

    body = (
      <Tabs
        activeKey="settings"
        onChangeTab={k => setCurrentTab(k)}
        tabItems={tabSections}
      />
    )
  } else {
    body = settingsForm
  }

  return (
    <>
      <Button
        data-test-id="collectionSettings"
        onClick={toggleModal}
        outlined
        status="primary"
      >
        Settings
      </Button>

      <Modal isOpen={modalIsOpen} onClose={toggleModal}>
        <ModalHeader hideModal={toggleModal}>
          Settings: <DisplayInlineEditor content={collectionTitle} />
        </ModalHeader>

        <ModalBody>{body}</ModalBody>

        <ModalFooter>
          <Button
            data-test-id="cancel-collection-settings"
            onClick={toggleModal}
            outlined
            status="primary"
          >
            Cancel
          </Button>
          <PermissionsGate scopes={['edit-collection-setting']}>
            <Button
              data-test-id="save-collection-settings"
              disabled={isSavingSettings || isSavingTemplate}
              loading={isSavingSettings || isSavingTemplate}
              onClick={handleClickSave}
              status="primary"
            >
              Save
            </Button>
          </PermissionsGate>
        </ModalFooter>
      </Modal>
    </>
  )
}

CollectionSettings.propTypes = {
  collectionTitle: PropTypes.string.isRequired,

  saveCollectionSettings: PropTypes.func.isRequired,
  createTemplate: PropTypes.func.isRequired,
  updateTemplate: PropTypes.func.isRequired,

  isEditor: PropTypes.bool.isRequired,
  isOrgAdmin: PropTypes.bool.isRequired,
  isSysAdmin: PropTypes.bool.isRequired,

  isBookSeriesCollection: PropTypes.bool,
  isSavingSettings: PropTypes.bool,
  isSavingTemplate: PropTypes.bool,
  templatesLoading: PropTypes.bool,

  /* eslint-disable react/forbid-prop-types */
  settingsValues: PropTypes.object,
  templates: PropTypes.object,
  /* eslint-enable react/forbid-prop-types */
}

CollectionSettings.defaultProps = {
  isBookSeriesCollection: false,
  templatesLoading: false,
  isSavingSettings: false,
  isSavingTemplate: false,
  settingsValues: {},
  templates: {},
}

export default CollectionSettings
