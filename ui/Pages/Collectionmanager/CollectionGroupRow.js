/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/require-default-props */
/* eslint-disable react/prop-types */
import React, { forwardRef, useImperativeHandle, useRef } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import * as icons from 'react-feather'

import { Checkbox, Icon } from '../../components'

// #region styled-components
const BookComponentStyled = styled.div`
  background-color: ${props =>
    props.checked ? th('colorTextPlaceholder') : 'none'};
  border-bottom: 1px solid ${th('colorTextPlaceholder')};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 50px;
  opacity: ${props => props.opacity};
  padding-left: ${props => (props.marginChildren ? props.marginChildren : '0')};

  :hover {
    cursor: pointer;
  }
`

const Wrapper = styled.div`
  opacity: ${props => (props.hide ? 0 : 1)};
`

const Row = styled.div`
  display: flex;
  margin-left: ${props => (props.secondData ? '80px' : '0')};
  text-align: left;
`

const Column = styled.div`
  align-self: center;
  display: flex;
  flex-basis: 0;
  flex-grow: ${props => props.flexgrow};
  justify-content: flex-start;
  margin: 0px;
`

const Title = styled.div`
  color: ${props =>
    props.collapsed ? th('colorText') : th('colorPrimaryDarker')};
  font-weight: 600;
`

const MoveIconContainer = styled.span`
  cursor: grab;
  display: inline-flex;
  padding: calc(${th('gridUnit')} / 2);

  svg {
    height: calc(${props => props.size} * ${th('gridUnit')});
    stroke: ${props => props.theme.colorText};
    width: calc(${props => props.size} * ${th('gridUnit')});
  }
`

// #endregion

const MoveIcon = forwardRef(
  ({ children, color, size = 3, testId, theme }, ref) => {
    const icon = icons.Move

    return (
      <MoveIconContainer data-test-id={testId} ref={ref} role="img" size={size}>
        {icon ? icon({}) : ''}
      </MoveIconContainer>
    )
  },
)

const RowElement = React.forwardRef(
  (
    {
      book,
      check,
      unCheck,
      selectedRows,
      children,
      collapsed,
      setCollapsed,
      disableCheck,
      isDragging,
      isOver,
      isOverCurrent,
      connectDragPreview,
      connectDragSource,
      connectDropTarget,
      canDrag,
      props,
    },
    ref,
  ) => {
    const { id, title } = book
    const elementRef = useRef(null)
    const dragHandlerRef = useRef(null)

    connectDragSource(dragHandlerRef)
    connectDragPreview(elementRef)
    connectDropTarget(elementRef)

    useImperativeHandle(ref, () => ({
      getNode: () => elementRef.current,
    }))

    return (
      <Wrapper>
        <BookComponentStyled
          checked={selectedRows.length && selectedRows.find(x => x.id === id)}
        >
          <Row>
            <Checkbox
              checked={
                selectedRows.length && selectedRows.find(x => x.id === book.id)
              }
              disabled={disableCheck}
              name="checkbox"
              onClick={isChecked => {
                if (isChecked) {
                  check(book)
                } else {
                  unCheck(book)
                }
              }}
            />
            <Icon color="colorText" onClick={() => setCollapsed()}>
              {collapsed ? 'chevron-right' : 'chevron-down'}
            </Icon>

            <Column>
              <MoveIcon ref={dragHandlerRef} testId={`${title}-dnd`} />
            </Column>
            <Column data-test-id="book-title" flexgrow={1}>
              <Title collapsed={collapsed}> {title}</Title>
            </Column>
          </Row>
        </BookComponentStyled>
        {!collapsed ? children : null}
      </Wrapper>
    )
  },
)

export default RowElement
