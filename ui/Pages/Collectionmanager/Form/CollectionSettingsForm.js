/* eslint-disable react/no-danger */
/* stylelint-disable no-descending-specificity */

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Formik, Form } from 'formik'
import { merge } from 'lodash'
import * as yup from 'yup'

import { grid } from '@pubsweet/ui-toolkit'

import { Button } from '../../../components/common'
import {
  SelectComponent as Select,
  TextFieldComponent as UITextField,
  ToggleComponent as UIToggle,
} from '../../../components/FormElements/FormikElements'
import FormSection from '../../../components/common/FormSection'
import ConstantsContext from '../../../../app/constantsContext'

// #region styled
const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 800px;
  padding-bottom: 50px;
  padding-top: 20px;

  > form > div:not(:last-child) {
    margin-bottom: ${grid(2)};
  }
`

const Toggle = styled(UIToggle)`
  max-width: 400px;
`

const ToggleGroup = styled.div`
  > div:first-child {
    font-weight: bold;
    margin-bottom: ${grid(1)};
  }
`

const TextField = styled(UITextField)`
  margin-bottom: 0;
`

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`
// #endregion styled

const defaultInitialValues = {
  citationSelfUrl: null,
  groupBooksBy: null,
  groupBooksOnTOC: false,
  openAccess: false,
  orderBooksBy: null,
  publisherUrl: null,
  requireApprovalByOrgAdminOrEditor: false,
  tocContributors: false,
  tocFullBookCitations: false,
  UKPMC: false,
}

const regMatch = /^((http|https):\/\/)/
const doiRegMatch = /^((http|https):\/\/)|^doi$/

const validations = yup.object().shape({
  orderBooksBy: yup
    .string()
    .nullable()
    .required('Order books by is a required field'),
  publisherUrl: yup
    .string()
    .nullable()
    .matches(regMatch, 'URL should start with https:// or http://'),
  citationSelfUrl: yup
    .string()
    .nullable()
    .matches(
      doiRegMatch,
      'Value should be doi or start with https:// and http://',
    ),
})

const CollectionSettingsForm = props => {
  const {
    className,
    initialFormValues,
    innerRef,
    isSaving,
    isSysAdmin,
    onSubmit,
    showSubmitButton,
  } = props

  const { s } = useContext(ConstantsContext)
  const getDescription = key => s(`fieldDescriptions.collectionForm.${key}`)

  const groupBooksByOptions = [
    { label: s('form.year'), value: 'year' },
    { label: s('form.volume'), value: 'volume' },
    { label: s('form.currentArchived'), value: 'status' },
    { label: s('form.yearCurrentArchived'), value: 'year-status' },
    { label: s('form.customGroupTitles'), value: 'customGroupTitles' },
  ]

  const orderBooksByOptions = [
    {
      label: (
        <span
          dangerouslySetInnerHTML={{
            __html: `${s('form.title')} ${s('emDash')} A${s('enDash')}Z`,
          }}
        />
      ),
      value: 'title',
    },
    {
      label: (
        <span
          dangerouslySetInnerHTML={{
            __html: `${s('form.volume')} ${s('emDash')} ${s('descending')}`,
          }}
        />
      ),
      value: 'volume',
    },
    {
      label: (
        <span
          dangerouslySetInnerHTML={{
            __html: `${s('form.publicationDate')} ${s('emDash')} ${s(
              'descending',
            )}`,
          }}
        />
      ),
      value: 'date',
    },
  ]

  const initialValues = merge({}, defaultInitialValues, initialFormValues)
  const readOnly = isSaving

  return (
    <Wrapper className={className}>
      <Formik
        enableReinitialize
        initialValues={initialValues}
        innerRef={innerRef}
        onSubmit={onSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const { setFieldValue, values } = formProps

          const {
            citationSelfUrl,
            groupBooksBy,
            groupBooksOnTOC,
            openAccess,
            orderBooksBy,
            publisherUrl,
            requireApprovalByOrgAdminOrEditor,
            tocContributors,
            tocFullBookCitations,
            UKPMC,
          } = values

          const handleGroupBooksOnTOCChange = v => {
            if (!v) {
              setFieldValue('groupBooksBy', null)
            }
          }

          return (
            <Form>
              <FormSection label="Publishing settings">
                <ToggleGroup>
                  <div>Require approval before publishing</div>

                  <div>
                    <Toggle
                      data-test-id="org-admin-or-editor-lock"
                      disabled={readOnly}
                      field={{
                        name: 'requireApprovalByOrgAdminOrEditor',
                        value: requireApprovalByOrgAdminOrEditor,
                      }}
                      form={formProps}
                      label="Org Admin or Editor"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                      note="Approval by Org Admin/Editor applies to all ‘wholebooks’ submitted to the collection."
                    />
                  </div>
                </ToggleGroup>

                <Toggle
                  data-test-id="open-access-status-lock"
                  description={getDescription('openAccess')}
                  disabled={readOnly || !isSysAdmin}
                  field={{
                    name: 'openAccess',
                    value: openAccess,
                  }}
                  form={formProps}
                  label="Open Access Status"
                  locked
                  lockPosition="left"
                />

                <Toggle
                  data-test-id="ukpmc-lock"
                  description={getDescription('UKPMC')}
                  disabled={readOnly || !isSysAdmin}
                  field={{
                    name: 'UKPMC',
                    value: UKPMC,
                  }}
                  form={formProps}
                  label="UKPMC"
                  locked
                  lockPosition="left"
                />
              </FormSection>

              <FormSection label="Landing Page: Links">
                <TextField
                  description={getDescription('publisherUrl')}
                  disabled={readOnly}
                  field={{ name: 'publisherUrl', value: publisherUrl }}
                  form={formProps}
                  label="Publisher URL"
                />

                <TextField
                  description={getDescription('citationSelfUrl')}
                  disabled={readOnly}
                  field={{ name: 'citationSelfUrl', value: citationSelfUrl }}
                  form={formProps}
                  label="Self-citation URL"
                />
              </FormSection>

              <FormSection label="Table of Contents settings">
                <Toggle
                  description={getDescription('groupBooksOnTOC')}
                  disabled={readOnly}
                  field={{
                    name: 'groupBooksOnTOC',
                    value: groupBooksOnTOC,
                  }}
                  form={formProps}
                  label="Group books on TOC"
                  onClick={handleGroupBooksOnTOCChange}
                />

                {groupBooksOnTOC && (
                  <Select
                    data-test-id="group-books-by"
                    description={getDescription('groupBooksBy')}
                    disabled={readOnly}
                    field={{
                      name: 'groupBooksBy',
                      value: groupBooksBy,
                    }}
                    form={formProps}
                    label="Group books by"
                    options={groupBooksByOptions}
                  />
                )}

                <Select
                  data-test-id="order-books-by"
                  description={getDescription('orderBooksBy')}
                  disabled={readOnly}
                  field={{
                    name: 'orderBooksBy',
                    value: orderBooksBy,
                  }}
                  form={formProps}
                  label="Order books by"
                  options={orderBooksByOptions}
                  required
                />

                <ToggleGroup>
                  <div>Display book metadata on TOC</div>

                  <div>
                    <Toggle
                      description={getDescription('tocContributors')}
                      disabled={readOnly}
                      field={{
                        name: 'tocContributors',
                        value: tocContributors,
                      }}
                      form={formProps}
                      label="Contributors"
                      labelPosition="left"
                      lockPosition="left"
                    />

                    <Toggle
                      description={getDescription('tocFullBookCitations')}
                      disabled={readOnly}
                      field={{
                        name: 'tocFullBookCitations',
                        value: tocFullBookCitations,
                      }}
                      form={formProps}
                      label="Full Book Citations"
                      labelPosition="left"
                      lockPosition="left"
                    />
                  </div>
                </ToggleGroup>
              </FormSection>

              {showSubmitButton && (
                <ButtonWrapper>
                  <Button
                    data-test-id="save-collection-settings"
                    disabled={readOnly}
                    status="primary"
                    type="submit"
                  >
                    Submit
                  </Button>
                </ButtonWrapper>
              )}
            </Form>
          )
        }}
      </Formik>
    </Wrapper>
  )
}

CollectionSettingsForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,

  /* eslint-disable-next-line react/forbid-prop-types */
  initialFormValues: PropTypes.object,

  isSaving: PropTypes.bool,
  isSysAdmin: PropTypes.bool,
  showSubmitButton: PropTypes.bool,
}

CollectionSettingsForm.defaultProps = {
  initialFormValues: {},
  isSaving: false,
  isSysAdmin: false,
  showSubmitButton: false,
}

export default CollectionSettingsForm
