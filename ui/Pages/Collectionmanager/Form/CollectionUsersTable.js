/* eslint-disable no-param-reassign */
/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import React from 'react'
import styled from 'styled-components'
// import { Field, getIn } from 'formik'
// import { ErrorText } from '@pubsweet/ui'
// import { th } from '@pubsweet/ui-toolkit'
import { TableLocalData } from '../../../components/Datatable'
import { Button } from '../../../components'

// #region Styles
const Root = styled.div`
  display: flex;
`
// #endregion

const UsersTable = ({
  data,
  checkboxColumn,
  setSelectedRows,
  selectedRows,
  setFieldValue,
}) => {
  const columns = [
    {
      dataKey: 'username',
      disableSort: true,
      label: '',
      dynamicHeight: true,
    },
    {
      cellRenderer: () => ({ ...params }) => (
        <Button
          icon="trash-2"
          onClick={() => {
            // const teamMembers =
            //   data?.map(tmb =>
            //     tmb.teamId.map(tId => ({
            //       ...tmb,
            //       teamId: tId,
            //     })),
            //   ) || []
            // teamMembers && teamMembers.splice(params.rowIndex, 1)
            // setFieldValue(`teamMembers`, [...flatten(teamMembers)])
          }}
          {...params}
          outlined
          status="danger"
        />
      ),
      dataKey: 'teamId',
      disableSort: true,
      label: '',
      width: 200,
    },
  ]

  return (
    <Root>
      <TableLocalData
        columns={columns}
        dataTable={[{ username: 'danjela', id: 1 }]}
        key="userstableCollection"
        noDataMessage="No users found"
        // onColumnClick={props => {
        //   if (props.dataKey === 'id' || props.dataKey === 'teamId')
        //     props.event.stopPropagation()
        // }}
        // onRowClick={() => {}}
        // selectedRows={selectedRows}
        // setSelectedRows={setSelectedRows}
      />
    </Root>
  )
}

export default UsersTable
