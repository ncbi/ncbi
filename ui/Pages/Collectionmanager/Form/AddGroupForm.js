/* eslint-disable react/prop-types */
import React, { useImperativeHandle } from 'react'
import { withFormik, Form, Field } from 'formik'
import { isEmpty } from 'lodash'
import { TextFieldComponent, Column, ColumnWraper } from '../../../components'

const Basic = ({
  setValidationAction,
  submitForm,
  handleSubmit,
  setFieldValue,
  errors,
  editForm,
  editMode,
  values,
  validateForm,
  showContent,
  ...props
}) => {
  // eslint-disable-next-line react/destructuring-assignment
  useImperativeHandle(props.formikFormRef, () => ({
    triggerSave: async () => {
      submitForm()
      // eslint-disable-next-line no-unreachable
      const isValid = await validateForm()
      return isEmpty(isValid) ? { title: values.title.trim() } : false
    },
  }))

  return (
    <Form onSubmit={handleSubmit}>
      <ColumnWraper>
        <Column>
          <Field
            component={TextFieldComponent}
            label="Title of group"
            name="title"
          />
        </Column>{' '}
      </ColumnWraper>
    </Form>
  )
}

const GroupForm = withFormik({
  handleSubmit: ({ setSubmitting }) => {
    setSubmitting && setSubmitting(false)
  },
  mapPropsToValues: ({ values }) => ({
    title: values.title || '',
  }),
  validate: values => {
    const errors = {}
    if (!(values.title || '').trim()) errors.title = 'Title is required'
    return errors
  },
})(Basic)

export default GroupForm
