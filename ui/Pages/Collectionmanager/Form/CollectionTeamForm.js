/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable react/prop-types */
import React, { useImperativeHandle } from 'react'
import { withFormik, Form, Field, ErrorMessage } from 'formik'
import { ErrorText } from '@pubsweet/ui'
import { isEmpty } from 'lodash'
import {
  SelectComponent,
  ColumnWraper,
  Column,
  Button,
} from '../../../components'
import { CollectionUsersTable } from './index'
// import RolesInput from './RolesInput'
// import UsersTable from './UsersTable'

const Basic = ({
  setValidationAction,
  submitForm,
  handleSubmit,
  setFieldValue,
  errors,
  editForm,
  editMode,
  values,
  validateForm,
  tableProps,
  ...props
}) => {
  // eslint-disable-next-line react/destructuring-assignment
  useImperativeHandle(props.formikFormRef, () => ({
    triggerSave: async () => {
      submitForm()
      const isValid = await validateForm()

      if (isEmpty(isValid)) {
        return values
      }

      return false
    },
    unassign: async selected => {
      setFieldValue(`teamMembers`, [
        ...values.teamMembers.filter(tm => !selected.includes(tm.id)),
      ])
    },
  }))

  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <>
          <ColumnWraper style={{ alignItems: 'end' }}>
            <Column grow={0}>
              Editors
              <Field
                component={SelectComponent}
                mappings={data => {
                  if (data && data.getBook) {
                    return data.getBook.organisation.users.results.map(
                      ({ id, username }) => ({
                        value: id,
                        label: username,
                      }),
                    )
                  }

                  return []
                }}
                onChange={selected => {
                  setFieldValue(`added.id`, selected.value)
                  setFieldValue(`added.userId`, selected.value)
                  setFieldValue(`added.username`, selected.label)
                }}
                options={[]}
                placeholder="Username"
              />
              <ErrorMessage component={ErrorText} name="organisationId" />
            </Column>
            <Column>
              <Button
                data-test-id="add-member-collection"
                icon="add"
                onClick={() => {
                  const found = values.teamMembers.find(
                    team =>
                      team.userId === values.added.userId &&
                      values.added.teamId.includes(team.teamId),
                  )

                  if (values.added.userId && values.added.teamId && !found) {
                    values.added.teamId.forEach((input, index) => {
                      setFieldValue(
                        `teamMembers.[${values.teamMembers.length + index}]`,
                        {
                          ...values.added,
                          teamId: input,
                        },
                      )
                    })

                    setFieldValue(`added`, {
                      id: null,
                      userId: null,
                      teamId: [],
                      username: null,
                    })
                  }
                }}
                status="primary"
              >
                Add Member
              </Button>
            </Column>
          </ColumnWraper>

          <CollectionUsersTable
            data={[]}
            errors={errors}
            setFieldValue={setFieldValue}
            {...tableProps}
          />
        </>
      </Form>
    </div>
  )
}

const CollectionTeamForm = withFormik({
  handleSubmit: (values, { setSubmitting }) => {
    setSubmitting(false)
  },
  mapPropsToValues: ({ values, teamMembers }) => ({
    teamMembers: (values || {}).teamMembers || teamMembers,
    added: {
      id: null,
      userId: null,
      teamId: [],
      username: null,
    },
  }),

  validate: values => {
    const errors = {}
    return errors
  },
})(Basic)

export default CollectionTeamForm
