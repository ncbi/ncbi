/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { cloneDeep, unionBy } from 'lodash'
import { useHistory } from 'react-router-dom'

import { grid } from '@pubsweet/ui-toolkit'

import DivisionContainer from './DivisionContainer'
import {
  ActionBar,
  BookHeader,
  Button,
  ButtonGroup,
  Checkbox,
  DragAndDrop,
  Spinner,
  Status,
  FixedLoader,
} from '../../components'
import { FilterButton } from '../Filters'
import CollectionMetadata from './Modals/CollectionMetadata'
import {
  CollectionRow,
  MoveBookToGroup,
  CollectionSettings,
  AddGroup,
} from './index'

import PermissionsGate from '../../../app/components/PermissionsGate'

// #region styled
const Root = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow-y: auto;
  padding: 0 20px;
`

const RootPage = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const CollectionBar = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  padding: ${grid(1)};
`

const SelectAll = styled.div`
  font-weight: bold;
  margin: ${grid(0.5)} ${grid(1)} 0 ${grid(1)};
`
// #endregion styled

const CollectionManagerPage = props => {
  const {
    canPublish,
    collection = {},
    collectionMetadata,
    collectionSettings,
    createTemplate,
    isEditor,
    isOrgAdmin,
    isSavingTemplate,
    isSysAdmin,
    isUpdatingCollection,
    loading,
    actionsLoader,
    saveCollectionSettings,
    searchGranthub,
    templates,
    templatesLoading,
    updateCollectionMetadata,
    updateCollectionOrder,
    updateTemplate,
    submitBooks,
    publishBooks,
    reloadPreviewBooks,
    disablePublish,
    disableReloadPreview,
    disableSubmit,
  } = props

  const history = useHistory()

  const [selectedRows, setSelectedRows] = useState([])
  const [collapseAll, setcollapseAll] = useState(false)
  const [selectAll, setSelectAll] = useState(false)
  const [collapsed, setCollapsed] = useState([])

  const [modalPartIsOpen, setPartIsOpen] = useState(false)
  const [modalValues, setModalValues] = useState({})

  const tocComponent = collection?.toc

  // #region  Tree and hande data change

  useEffect(() => {
    if (collapseAll) {
      setCollapsed(
        cloneDeep(
          // eslint-disable-next-line array-callback-return
          (collection.customGroups || []).map(itm => {
            return itm.id
          }),
        ),
      )
    } else {
      setCollapsed([])
    }
  }, [collapseAll])

  useEffect(() => {
    if (selectAll) {
      setSelectedRows(
        cloneDeep(
          collection.books.filter(itm => itm.componentType !== 'group'),
        ),
      )
    } else {
      setSelectedRows([])
    }
  }, [selectAll])

  if (loading) return <Spinner pageLoader />

  const hasGroups = collection.settings.groupBooks

  const hasCustomGroups =
    collection.settings.groupBooks &&
    collection.settings.groupBooksBy === 'customGroupTitles'

  const setCollapsedGroup = customcollectionId => {
    const indx = collapsed.findIndex(x => x === customcollectionId)

    if (indx < 0) {
      const arr = cloneDeep(collapsed)
      arr.push(customcollectionId)
      setCollapsed(arr)
    } else {
      const arr = cloneDeep(collapsed)
      arr.splice(indx, 1)
      setCollapsed(arr)
    }
  }

  const sortComponents = (itemsArray = [], sortingArr = []) => {
    itemsArray.sort((a, b) => {
      return sortingArr.indexOf(a.id) - sortingArr.indexOf(b.id)
    })

    return itemsArray
  }

  const getTreeList = (list = [], components = []) => {
    const listItems = sortComponents(list, components)
    return listItems.map(x => {
      const thisBooks = cloneDeep(x.books)
      const thisComponents = cloneDeep(x.components)

      if (x.settings.isCollectionGroup) {
        return {
          ...x,
          children: sortComponents(thisBooks, thisComponents),
        }
      }

      return x
    })
  }

  const merge = unionBy(collection.books, collection.customGroups, 'id')

  const rootComponents =
    merge && merge.filter(x => collection.components.includes(x.id))

  const treeList = getTreeList(rootComponents, collection.components)
  const isBookSeriesCollection = collection.collectionType === 'bookSeries'
  const isFundedCollection = collection.collectionType === 'funded'

  const handleClickExpandCollapse = e => {
    e.stopPropagation()
    setcollapseAll(!collapseAll)
  }

  const handleClickAddGroup = e => {
    e.stopPropagation()
    setPartIsOpen(true)
    setModalValues({})
  }

  const handleClickSelectAll = () => setSelectAll(!selectAll)

  const handleClickRow = item => {
    const { id } = item

    if (item.settings.isCollectionGroup) {
      setPartIsOpen(true)
      setModalValues(item)
    } else {
      history.push(`/organizations/${item.organisation.id}/bookmanager/${id}`)
    }
  }

  const updateOrder = item => {
    updateCollectionOrder(item.row.id, item.index)
  }

  const hasPubDate = pubDate => {
    const { date, dateRange } = pubDate

    if (date.year) return true
    if (dateRange.startYear) return true
    return false
  }

  const showTOCLabel = row => {
    const orderSetting = collection.settings.orderBooksBy
    if (orderSetting === 'title' && !row.title) return true
    if (orderSetting === 'volume' && !row.metadata.volume) return true
    if (orderSetting === 'date' && !hasPubDate(row.metadata.pubDate))
      return true

    return false
  }

  const areChapterProcessedBooks = () => {
    return selectedRows.some(item => item.settings.chapterIndependently)
  }

  const getWholeBookComponent = areChapterProcessedBooks()
    ? []
    : selectedRows.map(x => ({
        workflow: x.workflow,
        chapterIndependently: false,

        ...(x.bookComponents.length > 0 ? x.bookComponents[0] : x),
      }))

  return (
    <RootPage>
      <BookHeader
        childrenRight={
          <ButtonGroup>
            <CollectionMetadata
              data={collectionMetadata}
              isBookSeriesCollection={isBookSeriesCollection}
              isFundedCollection={isFundedCollection}
              isSaving={isUpdatingCollection}
              searchGranthub={searchGranthub}
              update={updateCollectionMetadata}
            />

            <CollectionSettings
              collectionTitle={collection.title}
              createTemplate={createTemplate}
              isBookSeriesCollection={isBookSeriesCollection}
              isEditor={isEditor}
              isOrgAdmin={isOrgAdmin}
              isSavingSettings={isUpdatingCollection}
              isSavingTemplate={isSavingTemplate}
              isSysAdmin={isSysAdmin}
              saveCollectionSettings={saveCollectionSettings}
              settingsValues={collectionSettings}
              templates={templates}
              templatesLoading={templatesLoading}
              updateTemplate={updateTemplate}
            />
            {/* <CollectionTeam key="collection_team" book={data} values={data} /> */}
            <PermissionsGate scopes={['view-collection-toc']}>
              <Button
                data-test-id="book-toc"
                icon={<Status hideText status={tocComponent?.status} />}
                iconPosition="end"
                onClick={() => {
                  const bookUrl = `/organizations/${collection.organisation.id}/collectionmanager/${collection?.id}/toc/${tocComponent?.id}`
                  history.push(bookUrl)
                }}
                outlined
                separator
                status="primary"
              >
                TOC
              </Button>
            </PermissionsGate>
            <FilterButton key="nav_2" />
          </ButtonGroup>
        }
        title={collection.title}
        type="collection"
      />

      <AddGroup
        collection={collection}
        modalIsOpen={modalPartIsOpen}
        setIsOpen={setPartIsOpen}
        values={modalValues}
      />

      <Root>
        <CollectionBar>
          <SelectAll>
            <Checkbox
              checked={selectAll}
              label="Select all"
              name="selectAll"
              onClick={handleClickSelectAll}
              value={selectAll}
            />
          </SelectAll>

          {hasCustomGroups && (
            <Button
              data-test-id="addGroup"
              icon="plus"
              onClick={handleClickAddGroup}
              primary
              size="small"
            >
              Add Group
            </Button>
          )}

          {hasGroups && (
            <Button
              color="colorPrimaryDarker"
              onClick={handleClickExpandCollapse}
              outlined
              status="primary"
            >
              {collapseAll ? 'Expand all' : 'Collapse all'}
            </Button>
          )}
        </CollectionBar>

        {treeList.length === 0 && <center>No books added yet</center>}
        <PermissionsGate scopes={['order-by-drag-drop-collection']}>
          <DivisionContainer
            allComponents={merge}
            canDrag={false}
            collapsed={collapsed}
            divisionId={collection.id}
            dragComponent={() => {}}
            id={collection.id}
            key={`division${collection.id}`}
            onRowClick={handleClickRow}
            orderSetting={collectionSettings.orderBooksBy}
            rootComponents={treeList || []}
            rowComponent={CollectionRow}
            selectedRows={selectedRows}
            setCollapsedPart={setCollapsedGroup}
            setSelectedRows={setSelectedRows}
            showTOCLabel={showTOCLabel}
            testId={collection.title}
            updateOrder={updateOrder}
          />
        </PermissionsGate>
      </Root>

      <ActionBar
        leftComponent={
          <ButtonGroup>
            <PermissionsGate scopes={['publish-whole-books']}>
              <Button
                data-test-id="book-comp-publish"
                disabled={
                  areChapterProcessedBooks() ||
                  disablePublish(getWholeBookComponent) ||
                  !canPublish(selectedRows)
                }
                loading={actionsLoader.isPublishing}
                onClick={() => {
                  publishBooks(getWholeBookComponent)
                  setSelectedRows([])

                  // oneOfBooks.settings.approvalPreviewer
                  //   ? setReviewConfirmIsOpen(true)
                  //   : setConfimIsOpen(true)
                }}
                publishBooks
                status="primary"
              >
                Publish
              </Button>
            </PermissionsGate>
            <PermissionsGate scopes={['reload-preview-whole-books']}>
              <Button
                data-test-id="reload-preview-books"
                disabled={
                  areChapterProcessedBooks() ||
                  disableReloadPreview(getWholeBookComponent)
                }
                key="reload-preview-books"
                loading={actionsLoader.isReconverting}
                onClick={() => {
                  reloadPreviewBooks(selectedRows)
                  setSelectedRows([])
                }}
                primary
              >
                Reload preview
              </Button>
            </PermissionsGate>
            <PermissionsGate scopes={['submit-whole-books']}>
              <Button
                data-test-id="submit-books"
                disabled={
                  areChapterProcessedBooks() ||
                  disableSubmit(getWholeBookComponent)
                }
                key="submit-book-component"
                loading={actionsLoader.isSubmitting}
                onClick={() => {
                  submitBooks(getWholeBookComponent)
                  setSelectedRows([])
                }}
                primary
              >
                Submit
              </Button>
            </PermissionsGate>
            <PermissionsGate scopes={['move-books-to-groups']}>
              <MoveBookToGroup
                books={selectedRows}
                collectionId={collection.id}
                disabled={!hasCustomGroups || selectedRows.length === 0}
                groups={sortComponents(
                  cloneDeep(collection.customGroups),
                  collection.components,
                )}
                key="3-update-books-status"
                setSelectAll={setSelectAll}
                setSelectedRows={setSelectedRows}
              />
            </PermissionsGate>
          </ButtonGroup>
        }
      />
      {actionsLoader.isSubmitting && (
        <FixedLoader title="Submitting Books..." />
      )}
      {actionsLoader.isReconverting && (
        <FixedLoader title="Reloading preview of Books..." />
      )}
      {actionsLoader.isPublishing && (
        <FixedLoader title="Publishing Books..." />
      )}
    </RootPage>
  )
}

export default props => (
  <DragAndDrop>
    <CollectionManagerPage {...props} />
  </DragAndDrop>
)
