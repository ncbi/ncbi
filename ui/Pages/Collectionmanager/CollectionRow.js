/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { forwardRef, useRef } from 'react'
import styled from 'styled-components'
import moment from 'moment'
import { cloneDeep, isEmpty } from 'lodash'
import { useDrag, useDrop } from 'react-dnd'
import { th, grid } from '@pubsweet/ui-toolkit'
import * as icons from 'react-feather'
import { Status, Checkbox } from '../../components'
import { Icon, SvgIcon, TOCLabel } from '../../components/common'
import DisplayInlineEditor from '../../components/wax/DisplayInlneEditor'

// #region  Styles
const Wrapper = styled.div`
  opacity: ${props => (props.hide ? 0 : 1)};
`

const BookComponentStyled = styled.div`
  background-color: ${props =>
    props.checked ? th('colorTextPlaceholder') : 'none'};
  border-bottom: 1px solid ${th('colorTextPlaceholder')};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  opacity: ${props => props.opacity};
  padding: 8px;
  padding-left: ${props => (props.marginChildren ? props.marginChildren : '0')};

  :hover {
    cursor: pointer;
  }
`

const Row = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  margin-left: 8px;
  text-align: left;
`

const BookRow = styled.div`
  display: flex;
  flex-direction: column;
`

const FirstRow = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: row wrap;
  justify-content: start;
  margin-left: 8px;
  text-align: left;
  width: 100%;
`

const SecondRow = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  padding-left: 50px;
  text-align: left;
  width: 100%;

  div: {
    flex: 0 1 auto;
  }
`

const Flex = styled.div`
  align-items: center;
  display: flex;
`

const Volume = styled.div`
  align-content: flex-start;
  display: flex;
  height: 100%;
  justify-content: center;
  min-width: 55px;
  padding-left: ${grid(1)};
  padding-right: ${grid(1)};
`

const Column = styled.div`
  align-items: center;
  display: flex;
`

const CenterSpan = styled.span`
  align-items: center;
  display: flex;
  margin: 0 8px;
`

const Label = styled.span`
  align-items: center;
  background: ${props =>
    props.reverseColor ? th('colorText') : th('colorBackground')};
  border-radius: 16px;
  color: ${props =>
    props.reverseColor ? th('colorTextReverse') : th('colorText')};
  display: flex;
  font-size: ${th('fontSizeBaseSmall')};
  font-weight: 600;
  height: min-content;
  justify-content: center;
  padding: 0 4px;
`

const ColumnTitle = styled.div`
  align-items: center;
  display: flex;
  flex: 1 1;
  flex-direction: column;
  font-weight: ${props => (props.type === 'customGroup' ? 700 : 400)};
  margin: 0px;
  text-align: left;
`

const GroupTitle = styled(ColumnTitle)`
  flex-direction: row;
  margin-left: ${grid(1)};
`

const Subtitles = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 14px;
`

const VersionColumn = styled.div`
  align-items: end;
  display: flex;
  justify-content: start;
  min-width: 140px;
`

const Organisation = styled.div`
  align-items: end;
  color: #000000;
  display: flex;
  flex: 0 0 260px;
  font-size: 12px;
  justify-content: end;
  line-height: 12px;
  margin-right: 10px;
`

const Version = styled.div`
  align-items: center;
  background-color: #f2f2f2;
  border-radius: 3px;
  color: #000000;
  display: table;
  font-size: 12px;
  font-weight: bold;
  letter-spacing: 0.5px;
`

const MoveIconContainer = styled.span`
  cursor: grab;
  display: inline-flex;
  padding: ${grid(0.5)};

  svg {
    height: calc(${props => props.size} * ${th('gridUnit')});
    stroke: ${props => props.color || props.theme.colorText};
    width: calc(${props => props.size} * ${th('gridUnit')});
  }
`

const ColumnDates = styled.div`
  align-content: end;
  align-items: end;
  display: flex;
  flex: 0 0 260px;
  flex-flow: row wrap;
  justify-content: flex-start;
`

const ColumnStatus = styled(ColumnDates)`
  align-content: end;
  align-items: end;
  display: flex;
  flex: 0 0 300px;
  flex-flow: row wrap;
  justify-content: space-between;
`

const ColumnLabel = styled.div`
  align-content: end;
  align-items: center;
  display: flex;
  flex: 0 0 70px;
  justify-content: flex-start;
`

const Title = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: start;
  margin-left: ${grid(1)};
  width: 100%;
`

const MainTitle = styled.div`
  font-weight: 600;
`
// #endregion

const MoveIcon = forwardRef(
  ({ children, color, size = 3, theme, ...props }, ref) => {
    const icon = icons.Move

    return (
      <MoveIconContainer
        color={th('colorText')}
        ref={ref}
        role="img"
        size={size}
      >
        {icon ? icon({}) : ''}
      </MoveIconContainer>
    )
  },
)

// eslint-disable-next-line no-unused-vars
const publishedDate = row => {
  // eslint-disable-next-line no-shadow
  const date = moment
    .utc(row.publishedDate)
    .utcOffset(moment().utcOffset())
    .format('L LT')

  return row.publishedDate ? `${date} ` : '-'
}

const RowElement = ({
  row,
  index,
  selectedRows = [],
  check,
  unCheck,
  onRowClick,
  showTOCLabel,
  orderSetting,
  children,
  collapsed,
  setCollapsed,
  disableCheck,
  updateOrder,
  isOver,
  canDrag,
}) => {
  const elementRef = useRef(null)
  const dragHandlerRef = useRef(null)
  const targetRow = row
  const targetIndex = index

  let isChecked = !!(
    selectedRows.length && selectedRows.find(x => x.id === row.id)
  )

  const [{ isDragging }, connectDrag, preview] = useDrag({
    type: 'BOOK_COMPONENT',
    item: { id: row.id, row },
    collect(props, monitor, component) {
      // console.log(index)
      return {
        index: props.index,
        listId: props.listId,
        row: cloneDeep(props.row),
        sorceParentType: component?.getNode().parentNode?.dataset?.type,
        sourceType: component?.getNode().dataset?.type,
      }
    },
    canDrag() {
      return canDrag
    },
  })

  const [, connectDrop] = useDrop({
    accept: 'BOOK_COMPONENT',

    drop(props, monitor) {
      if (!monitor.didDrop()) {
        updateOrder(props, targetIndex)
      }
    },
    async hover(props, monitor) {
      try {
        const hoverIndex = targetIndex

        if (props.row.id !== targetRow.id) {
          // item.row.partId = sourceChanged.row.parentid
          // Note: we're mutating the monitor item here!
          // Generally it's better to avoid mutations,
          // but it's good here for the sake of performance
          // to avoid expensive index searches.
          monitor.index = hoverIndex // sourceChanged.index
          // monitor.getItem().index = hoverIndex // sourceChanged.index
        }

        return
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e)
      }
    },
  })

  connectDrag(dragHandlerRef)
  connectDrop(preview(elementRef))

  const { isCollectionGroup } = row.settings
  const showRowDetails = !isCollectionGroup
  const groupType = isCollectionGroup ? 'customGroup' : 'collection'

  const getPubDate = pubDate => {
    const { date, dateRange, dateType } = pubDate
    let dateOfPublication = '-'

    if (dateType === 'pub') {
      dateOfPublication = `${date.month ? date.month : ''}${
        date.month ? '/' : ''
      }${date.day ? date.day : ''}${date.day ? '/' : ''}${date.year}`
    } else {
      const startMonth = !isEmpty(dateRange.startMonth)
        ? dateRange.startMonth
        : ''

      const startYear = !isEmpty(dateRange.startYear) ? dateRange.startYear : ''

      const endMonth = !isEmpty(dateRange.endMonth) ? dateRange.endMonth : ''

      const endYear = !isEmpty(dateRange.endYear) ? dateRange.endYear : ''
      if (startMonth || startYear || endMonth || endYear)
        dateOfPublication = `${startMonth || null}${startMonth && '/'}
       ${startYear}${startYear && endYear && '-'}${endMonth || ''}${
          endMonth && '/'
        }${endYear}`
    }

    return dateOfPublication
  }

  const removeSpaces = str => {
    if (!str) str = ''
    return str.replace(/\s/g, '')
  }

  const hasOpenIssues =
    row?.bookComponents?.length > 0 &&
    row?.bookComponents[0]?.issues?.length > 0 &&
    row?.bookComponents[0]?.issues.filter(issue => issue.status === 'open')
      .length > 0

  const contentType = [
    {
      key: 'publishedPDF',
      value: 'Published PDF',
    },
    {
      key: 'authorManuscript',
      value: 'Author Manuscript',
    },
    {
      key: 'prepublicationDraft',
      value: 'Prepublication Draft',
    },
    {
      key: 'finalFullText',
      value: 'Final full-Text',
    },
  ]

  const getContentType = value => {
    return contentType.find(props => props.key === value)
  }

  return (
    <Wrapper
      data-type={groupType}
      hide={isDragging}
      index={index}
      ref={elementRef}
    >
      <BookComponentStyled
        checked={isChecked}
        // marginChildren={marginChildren}
      >
        {isCollectionGroup && (
          <Row
            style={{
              background: isOver && isCollectionGroup ? '#efeaea8a' : 'none',
            }}
          >
            <Flex>
              {isCollectionGroup && (
                <Icon
                  color={th('colorText')}
                  data-test-id="collapse-icon"
                  onClick={() => setCollapsed()}
                >
                  {collapsed ? 'chevron-right' : 'chevron-down'}
                </Icon>
              )}

              {canDrag && !showRowDetails && <MoveIcon ref={dragHandlerRef} />}
            </Flex>
            <GroupTitle>
              <DisplayInlineEditor
                color={
                  isCollectionGroup && !collapsed ? 'colorPrimary' : 'colorText'
                }
                content={row.title}
              />
            </GroupTitle>
          </Row>
        )}

        {!isCollectionGroup && (
          <BookRow>
            <FirstRow>
              <Column>
                <CenterSpan>
                  <Checkbox
                    checked={isChecked}
                    data-test-id={`checkbox-${removeSpaces(row.title)}`}
                    disabled={disableCheck}
                    name="checkbox"
                    onClick={e => {
                      if (isChecked) {
                        unCheck(row)
                      } else {
                        check(row)
                      }

                      isChecked = !e.target.checked
                    }}
                  />
                </CenterSpan>{' '}
                <Flex>
                  <SvgIcon
                    name={
                      row.chapterIndependently ||
                      row.settings.chapterIndependently
                        ? 'bookWithChapters'
                        : 'book'
                    }
                  />
                </Flex>
              </Column>

              <ColumnTitle
                data-test-id="chapter-title"
                onClick={() => onRowClick(row)}
                type="collection"
              >
                <Title>
                  <>
                    {orderSetting === 'volume' && (
                      <Volume>{row.metadata.volume}</Volume>
                    )}
                    <MainTitle>
                      <DisplayInlineEditor content={row.title} />
                    </MainTitle>
                  </>
                  <Subtitles>
                    {(row.subTitle || []).map(sub => (
                      <div
                        // eslint-disable-next-line react/no-danger
                        dangerouslySetInnerHTML={{ __html: sub }}
                        key={sub}
                      />
                    ))}
                  </Subtitles>
                </Title>
              </ColumnTitle>
            </FirstRow>
            <SecondRow>
              <VersionColumn>
                <Version>
                  {row.fundedContentType &&
                    `${getContentType(row.fundedContentType).value} V${
                      row.version
                    }`}
                </Version>
              </VersionColumn>
              <Organisation> {row.organisation?.name}</Organisation>

              {orderSetting === 'date' && (
                <ColumnDates onClick={() => onRowClick(row)}>
                  <Label>Publication Date:</Label>
                  <span> {getPubDate(row.metadata.pubDate)}</span>
                </ColumnDates>
              )}

              <ColumnDates onClick={() => onRowClick(row)}>
                <Label>Last Updated:</Label>
                <span> {moment(row.updated).fromNow()}</span>
              </ColumnDates>
              <ColumnDates onClick={() => onRowClick(row)}>
                <Label>Last Published:</Label>
                <span> {publishedDate(row)}</span>
              </ColumnDates>
              <ColumnStatus onClick={() => onRowClick(row)}>
                <Status status={row.status} />
                {hasOpenIssues ? <Status status="vendor-issues" /> : ''}
              </ColumnStatus>
              <ColumnLabel>
                <TOCLabel missing={showTOCLabel(row)} />
              </ColumnLabel>
            </SecondRow>
          </BookRow>
        )}
      </BookComponentStyled>
      {!collapsed ? children : null}
    </Wrapper>
  )
}

export default RowElement
