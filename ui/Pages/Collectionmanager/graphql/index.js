import { gql } from '@apollo/client'

const GET_COLLECTION = gql`
  query getCollection($id: ID!) {
    getCollection(id: $id) {
      id
      collectionType
      title
      domain
      workflow
      updated
      abstract
      abstractTitle
      fileCover {
        id
        name
      }
      organisation {
        name
        id
        settings {
          collections {
            bookSeries
            funded
          }
        }
      }
      settings {
        groupBooks
        groupBooksBy
        orderBooksBy
        isCollectionGroup
        toc {
          contributors
          publisher
        }
      }
      metadata {
        pub_loc
        pub_name
        copyrightStatement
        copyrightYear
        copyrightHolder
        openAccess
        licenseType
        otherLicenseType
        licenseStatement
        nlmId
        author {
          givenName
        }
        editor {
          givenName
          surname
          role
          degrees
        }
        notes {
          type
          title
          description
        }
      }
      components
      customGroups {
        id
        title
        collectionType
        components
        settings {
          isCollectionGroup
        }
        books {
          id
          title
          subTitle
          fundedContentType
          organisation {
            name
            id
          }
          edition
          status
          workflow
          updated
          bookComponents {
            id
            bookComponentVersionId
          }
          settings {
            chapterIndependently
          }
          metadata {
            pubDate {
              dateType
              publicationFormat
              date {
                day
                month
                year
              }
              dateRange {
                startYear
                startMonth
                endYear
                endMonth
              }
            }
          }
        }
      }
      books {
        id
        title
        subTitle
        fundedContentType
        organisation {
          name
          id
        }
        edition
        status
        workflow
        updated
        settings {
          chapterIndependently
        }
        bookComponents {
          id
          bookComponentVersionId
        }
        metadata {
          pubDate {
            dateType
            publicationFormat
            date {
              day
              month
              year
            }
            dateRange {
              startYear
              startMonth
              endYear
              endMonth
            }
          }
        }
      }
    }
  }
`

const CREATE_CUSTOM_GROUP = gql`
  mutation($id: ID!, $input: CreateCollectionInput) {
    createCustomGroup(id: $id, input: $input) {
      title
    }
  }
`

const UPDATE_CUSTOM_GROUP = gql`
  mutation($id: ID!, $input: UpdateCollectionInput) {
    updateCustomGroup(id: $id, input: $input) {
      title
    }
  }
`

const UPDATE_CUSTOM_GROUP_ORDER = gql`
  mutation(
    $parentCollectionId: ID!
    $collectionId: ID!
    $ids: [ID]
    $index: Int
  ) {
    updateCollectionOrder(
      parentCollectionId: $parentCollectionId
      collectionId: $collectionId
      ids: $ids
      index: $index
    ) {
      id
      title
      domain
      workflow
      updated
      abstract
      abstractTitle
      fileCover {
        id
        name
      }
      organisation {
        name
        id
      }
      settings {
        groupBooks
        groupBooksBy
        orderBooksBy
        isCollectionGroup
        toc {
          contributors
          publisher
        }
      }
      metadata {
        pub_loc
        pub_name
        copyrightStatement
        copyrightYear
        copyrightHolder
        openAccess
        licenseType
        otherLicenseType
        licenseStatement
        nlmId
        author {
          givenName
        }
        editor {
          givenName
          surname
          role
          degrees
        }
        notes {
          type
          title
          description
        }
      }
      components
      customGroups {
        id
        title
        collectionType
        settings {
          isCollectionGroup
        }
        books {
          id
          title
          subTitle
          organisation {
            name
            id
          }
          bookComponents {
            id
            bookComponentVersionId
          }
          edition
          status
          workflow
          updated
          settings {
            chapterIndependently
          }

          metadata {
            pubDate {
              dateType
              publicationFormat
              date {
                day
                month
                year
              }
              dateRange {
                startYear
                startMonth
                endYear
                endMonth
              }
            }
          }
        }
      }
      books {
        id
        title
        subTitle
        organisation {
          name
          id
        }
        edition
        status
        workflow
        updated
        fundedContentType
        settings {
          chapterIndependently
        }
        bookComponents {
          id
          bookComponentVersionId
        }
        metadata {
          pubDate {
            dateType
            publicationFormat
            date {
              day
              month
              year
            }
            dateRange {
              startYear
              startMonth
              endYear
              endMonth
            }
          }
        }
      }
    }
  }
`

export default {
  GET_COLLECTION,
  CREATE_CUSTOM_GROUP,
  UPDATE_CUSTOM_GROUP,
  UPDATE_CUSTOM_GROUP_ORDER,
}
