/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react'
import BookComponentRow from '../Bookmanager/Book/BookComponentRow'

import Row from './CollectionRow'

const Container = props => {
  const {
    rootComponents,
    book,
    setPartIsOpen,
    setPartModalValues,
    testId,
    canDragItemsInDivision,
    pushCard,
    collapsed,
    divisionName,
    dragComponent,
    divisionId,
    moveCard,
    onRowClick,
    pushOneLevelUp,
    removeCard,
    reorderPartComponent,
    selectedRows,
    setCardChild,
    setCollapsedPart,
    setSelectedRows,
    updateOrder,

    allowedStatusesToMove,
    orderSetting,
    showTOCLabel,
  } = props

  return (
    <div data-test-id={testId}>
      {rootComponents.map(
        (bookComponent, i) =>
          bookComponent.title && (
            <BookComponentRow
              allowedStatusesToMove={allowedStatusesToMove}
              book={book}
              canDragItemsInDivision={canDragItemsInDivision}
              changeSection={pushCard}
              collapsedParts={collapsed}
              divisionName={divisionName}
              dragComponent={dragComponent}
              index={i}
              key={`BookComponentRow${bookComponent.id}`}
              listId={divisionId}
              moveCard={moveCard}
              onRowClick={onRowClick}
              orderSetting={orderSetting}
              pushOneLevelUp={pushOneLevelUp}
              removeCard={removeCard}
              reorderPartComponent={reorderPartComponent}
              row={bookComponent}
              rowComponent={Row}
              selectedRows={selectedRows}
              setCardChild={setCardChild}
              setCollapsed={setCollapsedPart}
              setPartIsOpen={setPartIsOpen}
              setPartModalValues={setPartModalValues}
              setSelectedRows={setSelectedRows}
              showTOCLabel={showTOCLabel}
              updateOrder={updateOrder}
            />
          ),
      )}
    </div>
  )
}

export default Container
