import React from 'react'
import styled from 'styled-components'

import Header from '../../components/LayoutElement/PageHeader'
import Footer from '../../components/LayoutElement/PageFooter'
import SignupContainer from './SignUpContainer'

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  font-family: ${props => props.theme.fontInterface};
  font-weight: 400;
  height: 100%;
`

const StyledSignupContainer = styled(SignupContainer)`
  flex-grow: 1;
`

const Signup = () => (
  <PageWrapper>
    <Header />
    <StyledSignupContainer />
    <Footer />
  </PageWrapper>
)

export default Signup
