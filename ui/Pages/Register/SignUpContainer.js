import React from 'react'
import { useLazyQuery, useMutation } from '@apollo/client'

import query from './graphql'
import Signup from './SignUpForm'

const SignupContainer = props => {
  const { className } = props
  const [registerUser] = useMutation(query.REGISTER_REQUEST_USER)
  const [checkUser] = useLazyQuery(query.CHECK_USER)

  const handleSubmit = values => {
    const { givenName, surname, username, email, password } = values

    const data = {
      variables: {
        input: {
          givenName,
          surname,
          username,
          email,
          password,
        },
      },
    }

    return registerUser(data).catch(e => {
      console.error(e)
    })
  }

  const checkUsernameAvailability = username => {
    return checkUser({
      variables: {
        search: username,
      },
    })
  }

  return (
    <Signup
      checkUsernameAvailability={checkUsernameAvailability}
      className={className}
      onSubmit={handleSubmit}
      registerUser={registerUser}
    />
  )
}

export default SignupContainer
