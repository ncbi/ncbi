import { gql } from '@apollo/client'

const REGISTER_REQUEST_USER = gql`
  mutation($input: CokoUserCreateInput) {
    createCokoUser(input: $input) {
      id
      username
      email
      givenName
      surname
    }
  }
`

const CHECK_USER = gql`
  query($search: String!) {
    checkUser(search: $search) {
      id
      username
    }
  }
`

export default {
  CHECK_USER,
  REGISTER_REQUEST_USER,
}
