/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { Formik, Form, Field } from 'formik'
import * as yup from 'yup'

import { Link, ErrorText } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { FormWrapper, SuccessText } from '../../components/FormElements'
import { TextFieldComponent, Button } from '../../components'

// #region styled
const FormNotes = styled.div`
  color: ${th('colorText')};
  display: flex;
  margin-bottom: 0;
  margin-top: 12px;
`

const RegisterNcbi = styled.div`
  text-align: left;

  > a {
    color: ${th('colorPrimary')};
  }
`

const SubmitButton = styled(Button)`
  width: 100%;
`

const Login = styled.div`
  flex-grow: 1;
  text-align: right;

  > a {
    color: ${th('colorPrimary')};
  }
`

const InlineInputs = styled.div`
  display: flex;
  flex-direction: row;

  > div {
    width: 50%;
  }
`
// #endregion styled

const initialValues = {
  givenName: '',
  surname: '',
  username: '',
  email: '',
  password: '',
  confirmPassword: '',
}

const SignupForm = props => {
  const { className, checkUsernameAvailability, onSubmit } = props

  const validations = yup.object().shape({
    givenName: yup.string().required('Given name is required'),
    surname: yup.string().required('Surname is required'),
    username: yup
      .string()
      .required('Username is required')
      .test(
        'username-available',
        'Username not available, please try another one',
        value => {
          if (!value) return false
          return checkUsernameAvailability(value).then(res => {
            return !res.data.checkUser
          })
        },
      ),
    email: yup
      .string()
      .email('Must be a valid email')
      .required('Email is required'),
    password: yup
      .string()
      .required('Password is required')
      .min(8, 'Password must be at least 8 characters')
      .matches(/^(?=.*[0-9])/, 'Password must contain a number')
      .matches(/^(?=.*[A-Z])/, 'Password must contain an uppercase letter')
      .matches(/^(?=.*[a-z])/, 'Password must contain a lowercase letter'),
    confirmPassword: yup
      .string()
      .required('Confirm your password')
      .test('password-confirm', 'Passwords must match', (value, { parent }) => {
        return value === parent.password
      }),
  })

  const handleSubmit = (values, formikBag) => {
    const { setStatus, resetForm, setSubmitting } = formikBag

    onSubmit(values)
      .then(() => {
        resetForm()
        setStatus({ success: 'User has been created successfully!' })
      })
      .catch(message => {
        setStatus({ error: message })
        setSubmitting(false)
      })
  }

  return (
    <FormWrapper className={className}>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const { isValid, status } = formProps

          return (
            <Form noValidate>
              <InlineInputs>
                <Field
                  component={TextFieldComponent}
                  label="Given name"
                  name="givenName"
                />

                <Field
                  component={TextFieldComponent}
                  label="Surname"
                  name="surname"
                />
              </InlineInputs>

              <Field
                component={TextFieldComponent}
                label="Username"
                name="username"
              />

              <Field
                component={TextFieldComponent}
                label="Email"
                name="email"
              />

              <Field
                component={TextFieldComponent}
                label="Password"
                name="password"
                type="password"
              />

              <Field
                component={TextFieldComponent}
                label="Confirm Password"
                name="confirmPassword"
                type="password"
              />

              {status && status.success && (
                <SuccessText>{status.success}</SuccessText>
              )}

              {status && status.error && <ErrorText>{status.error}</ErrorText>}

              <SubmitButton disabled={!isValid} status="primary" type="submit">
                Register
              </SubmitButton>

              <FormNotes>
                <RegisterNcbi>
                  <Link to="/loginNcbi">Register with myNCBI</Link>
                </RegisterNcbi>

                <Login>
                  Already have an account ? <Link to="/login">Log in</Link>
                </Login>
              </FormNotes>
            </Form>
          )
        }}
      </Formik>
    </FormWrapper>
  )
}

export default SignupForm
