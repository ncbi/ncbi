import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import { useMutation } from '@apollo/client'
import config from 'config'
import styled from 'styled-components'
import { LOGIN_NCBI_USER } from './graphql'
import { getNcbiCookie } from '../../../app/cookie'
import { Spinner } from '../../components'

const Centered = styled.div`
  left: 50%;
  position: absolute;
  top: 30%;
  transform: translateX(-50%) translateY(-50%);
`

const LoginNcbi = () => {
  const [loginUser] = useMutation(LOGIN_NCBI_USER)

  const [isUserRegistered, setIsUserRegistered] = useState(null)

  useEffect(() => {
    async function getToken() {
      const cookie = getNcbiCookie()
      const { myNcbi } = config['pubsweet-client']
      const myNcbiLogin = `${myNcbi}${window.location.href}`

      if (!cookie) {
        window.location.href = myNcbiLogin
      } else {
        const {
          data: {
            loginNcbiUser: { cookieStatus, token },
          },
        } = await loginUser({ variables: { cookie } })

        switch (cookieStatus) {
          case 'loggedIn':
            localStorage.setItem('token', token)
            setIsUserRegistered(true)
            break
          case 'loggedOut':
            window.location.href = myNcbiLogin
            break
          case 'newUser':
            setIsUserRegistered(false)
            break
          default:
        }
      }
    }

    getToken()
  }, [])

  const urlParams = new URLSearchParams(window.location.search)
  const redirectPath = urlParams.get('next') || '/'

  return (
    <Centered>
      <Spinner size="small" text="Redirecting..." />
      {isUserRegistered !== null &&
        (isUserRegistered ? (
          <Redirect to={redirectPath} />
        ) : (
          <Redirect to={`/signUpNcbi${window.location.search}`} />
        ))}
    </Centered>
  )
}

export default LoginNcbi
