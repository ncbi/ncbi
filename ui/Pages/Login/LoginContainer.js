import { compose, withState, withHandlers } from 'recompose'
import { withFormik } from 'formik'

import { graphql } from '@apollo/client/react/hoc'
import config from 'config'
import { LOGIN_USER } from './graphql'
import Login from './LoginForm'

const getNextUrl = () => {
  const url = new URL(window.location.href)

  const redirectLink = config['pubsweet-client']['login-redirect']
  return `${url.searchParams.get('next') || redirectLink}`
}

const localStorage = window.localStorage || undefined

const handleSubmit = (values, { props, setSubmitting, setStatus, setErrors }) =>
  props
    .loginUser({ variables: { input: values } })
    .then(({ data, errors }) => {
      if (!errors) {
        localStorage.setItem('token', data.loginUser.token)

        setTimeout(() => {
          props.onLoggedIn(getNextUrl())
        }, 100)
      }
    })
    .catch(e => {
      if (e.graphQLErrors && e.graphQLErrors.length > 0) {
        setSubmitting(false)
        setErrors(e.graphQLErrors[0].message)
        setStatus({ error: e.graphQLErrors[0].message })
      }
    })

const LoginFormik = withFormik({
  displayName: 'login',
  handleSubmit,
  mapPropsToValues: props => ({
    username: '',
    password: '',
    // TODO: remember: false,
  }),
  validate: values => {
    const errors = {}

    if (!values.username) {
      errors.username = 'Username is required'
    } else if (values.username.length > 15) {
      errors.username = 'Username is not valid'
    }

    if (!values.password) {
      errors.password = 'Password is required'
    }

    return errors
  },
})(Login)

export default compose(
  graphql(LOGIN_USER, {
    name: 'loginUser',
  }),
  withState('redirectLink', 'loggedIn', null),
  withHandlers({
    onLoggedIn: ({ loggedIn }) => returnUrl => loggedIn(() => returnUrl),
  }),
)(LoginFormik)
