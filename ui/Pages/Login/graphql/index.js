import { gql } from '@apollo/client'

export const LOGIN_USER = gql`
  mutation($input: LoginUserInput) {
    loginUser(input: $input) {
      token
    }
  }
`

export const LOGIN_NCBI_USER = gql`
  mutation($cookie: String!) {
    loginNcbiUser(cookie: $cookie) {
      token
      cookieStatus
    }
  }
`
