import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import Header from '../../components/LayoutElement/PageHeader'
import LoginContainer from './LoginContainer'
import Footer from '../../components/LayoutElement/PageFooter'

const PageWrapper = styled.div`
  font-family: ${th('fontInterface')};
  font-weight: 400;
`

const Login = () => (
  <PageWrapper>
    <Header />
    <LoginContainer />
    <Footer />
  </PageWrapper>
)

export default Login
