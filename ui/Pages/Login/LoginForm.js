/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import { Redirect } from 'react-router-dom'
import styled from 'styled-components'
import { th, grid } from '@pubsweet/ui-toolkit'
import { ErrorText, Link } from '@pubsweet/ui'
import { Field } from 'formik'

import { FormWrapper } from '../../components/FormElements'

import { TextFieldComponent, Checkbox, Button } from '../../components'

const RemeberMeWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: ${grid(2)};

  span,
  a {
    color: ${th('colorText')};
    font-size: 12pt;
    font-style: normal;
    text-decoration: none;
  }

  > a {
    flex-grow: 1;
    text-align: end;
  }
`

const RemeberMeCheckbox = styled(Checkbox)`
  flex-grow: 1;
`

const SubmitButton = styled(Button)`
  height: ${grid('5')};
  width: 100%;
`

const NoAccountNote = styled.p`
  color: ${th('colorText')};
  margin-bottom: 0;
`

const RememberMeInput = ({ checked, onClick, field }) => (
  <RemeberMeCheckbox
    checked={checked}
    label="Remember me"
    name={field.name}
    onClick={onClick}
  />
)

const LoginForm = ({
  handleSubmit,
  errors,
  touched,
  isValid,
  status,
  redirectLink,
}) => {
  const [rememberMe, setRememberMe] = useState(false)
  return redirectLink ? (
    <Redirect to={redirectLink} />
  ) : (
    <FormWrapper>
      <form onSubmit={handleSubmit}>
        <Field
          autoFocus
          component={TextFieldComponent}
          label="Username"
          name="username"
        />
        <Field
          component={TextFieldComponent}
          label="Password"
          name="password"
          type="password"
        />
        <RemeberMeWrapper>
          <Field
            checked={rememberMe}
            component={RememberMeInput}
            name="remember"
            onClick={() => {
              setRememberMe(!rememberMe)
            }}
          />
          <Link to="/">Forgot password ?</Link>
        </RemeberMeWrapper>
        {status && status.error && <ErrorText>{status.error}</ErrorText>}
        <SubmitButton
          disabled={!isValid}
          onClick={handleSubmit}
          status="primary"
          type="submit"
        >
          Log in
        </SubmitButton>
      </form>
      <NoAccountNote>
        No account yet? <Link to="/signup">Register</Link>
      </NoAccountNote>
    </FormWrapper>
  )
}

export default LoginForm
