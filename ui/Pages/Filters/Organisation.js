/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable react/prop-types */
import React, { useEffect } from 'react'
import styled from 'styled-components'
import { useLazyQuery } from '@apollo/client'

import query from '../Dashboard/graphql'
import Select from '../../components/common/Select'

const OrgSelection = styled.div`
  display: flex;
  flex-direction: column;
`

const StyledSelect = styled(Select)`
  min-width: 200px;
`

const OrganizationFilter = ({ organisationId, changeFilterValue, isMulti }) => {
  const [getOrganisations, { data }] = useLazyQuery(query.GET_ORGANISATIONS)

  useEffect(() => {
    getOrganisations({
      variables: {
        input: {
          skip: 0,
          take: 10,
        },
      },
    })
  }, [])

  const orgOptions =
    data &&
    data.getOrganisations &&
    data.getOrganisations.results.map(({ id, name }) => ({
      value: id,
      label: name,
    }))

  const selectedItem =
    data &&
    data.getOrganisations &&
    orgOptions.filter(option => option.value === organisationId)

  return (
    <OrgSelection>
      <StyledSelect
        className="basic-single"
        classNamePrefix="select"
        data-test-id="selectOrganization"
        defaultOptions
        multiple={isMulti}
        name="search"
        onChange={selected => {
          if (selected) {
            return changeFilterValue(selected.value)
          }

          return null
        }}
        onInputChange={value =>
          getOrganisations({
            variables: {
              input: {
                skip: 0,
                take: -1,
                search: {
                  criteria: [{ field: 'name', operator: { contains: value } }],
                },
              },
            },
          })
        }
        options={orgOptions}
        placeholder="Type your organization"
        value={selectedItem}
      />
    </OrgSelection>
  )
}

export default OrganizationFilter
