import React from 'react'
import { Button } from '../../components'

const FilterButton = () => <Button icon="filter" status="primary" />

export default FilterButton
