import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { th, grid } from '@pubsweet/ui-toolkit'
import Preview from './Tabs/PreviewTab'
import Errors from './Tabs/ErrorsTab'
import Status from '../../components/Status'
import Tabs from '../../components/common/Tabs'
import Spinner from '../../components/common/Spinner'
import PermissionsGate from '../../../app/components/PermissionsGate'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const TabsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  > div:last-child {
    flex-grow: 1;
  }
`

const PageWrapper = styled.div`
  display: flex;
  flex-direction: row;
  height: 100%;
`

const PageMenu = styled.div`
  align-items: center;
  background: ${th('colorSubMenu')};
  color: ${th('colorBackground')};
  display: flex;
  height: 50px;
  justify-content: center;
  max-width: 100%;
  padding: ${grid(0.25)} ${grid(1)};
`

const RightComponent = styled.div`
  align-items: center;
  display: flex;
  margin-left: auto;
`

const TabContainer = styled.div`
  height: 100%;
  width: 100%;
`

const TocPage = props => {
  const {
    status,
    previewLink,
    noPreviewMessage,
    disablePublishToc,
    disableDownloadToc,
    isUpdatingToc,
    isCollection,
    handleRepublishToc,
    handleDownloadToc,
    errorData,
    loading,
    lastPublishedDate,
  } = props

  const previewTab = {
    label: 'Live',
    key: '#preview',
    content: (
      <TabContainer>
        <PermissionsGate
          errorProps={{ hideTocButton: true }}
          scopes={[
            isCollection ? 'publish-collection-toc' : 'publish-book-toc',
          ]}
        >
          <Preview
            disableDownloadToc={disableDownloadToc}
            disablePublishToc={disablePublishToc}
            handleDownloadToc={handleDownloadToc}
            handleRepublishToc={handleRepublishToc}
            isCollection={isCollection}
            isUpdatingToc={isUpdatingToc}
            noPreviewMessage={noPreviewMessage}
            previewLink={previewLink}
            tocStatus={status}
          />
        </PermissionsGate>
      </TabContainer>
    ),
  }

  const errorsTab = {
    label: 'Errors',
    key: '#errors',
    content: (
      <TabContainer>
        <Errors errorData={errorData} isUpdating={isUpdatingToc} />
      </TabContainer>
    ),
  }

  if (loading) return <Spinner pageLoader />
  return (
    <Wrapper>
      <PageMenu>Table of contents</PageMenu>
      <PageWrapper>
        <TabsWrapper>
          <PermissionsGate scopes={['view-book-toc']} showPermissionInfo>
            <Tabs
              activeKey={previewTab.key}
              rightComponent={
                <RightComponent>
                  {lastPublishedDate && (
                    <>Last published: {lastPublishedDate}</>
                  )}
                  <Status status={status} />
                </RightComponent>
              }
              tabItems={[previewTab, errorsTab]}
            />
          </PermissionsGate>
        </TabsWrapper>
      </PageWrapper>
    </Wrapper>
  )
}

const errorItemShape = {
  noticeTypeName: PropTypes.string,
  severity: PropTypes.string,
  assignee: PropTypes.string,
  message: PropTypes.string,
}

const errorDataShape = {
  jobId: PropTypes.string,
  sessionId: PropTypes.string,
  status: PropTypes.string,
  url: PropTypes.string,
  updated: PropTypes.string,
  errors: PropTypes.arrayOf(PropTypes.shape(errorItemShape)),
}

TocPage.propTypes = {
  status: PropTypes.oneOf([
    'unpublished',
    'publish-failed',
    'published',
    'publishing',
  ]).isRequired,
  previewLink: PropTypes.string,
  noPreviewMessage: PropTypes.string,
  disablePublishToc: PropTypes.bool.isRequired,
  disableDownloadToc: PropTypes.bool.isRequired,
  isUpdatingToc: PropTypes.bool.isRequired,
  handleRepublishToc: PropTypes.func.isRequired,
  handleDownloadToc: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  lastPublishedDate: PropTypes.string,
  errorData: PropTypes.arrayOf(PropTypes.shape(errorDataShape)),
  isCollection: PropTypes.bool,
}

TocPage.defaultProps = {
  previewLink: null,
  noPreviewMessage: null,
  isCollection: false,
  lastPublishedDate: null,
  errorData: [],
}

export default TocPage
