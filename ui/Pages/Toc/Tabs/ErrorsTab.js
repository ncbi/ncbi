import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment'
import 'react-datepicker/dist/react-datepicker.css'

import { th, grid } from '@pubsweet/ui-toolkit'

import Button from '../../../components/common/Button'
import Status from '../../../components/Status'
import Spinner from '../../../components/common/Spinner'
import { TableLocalData } from '../../../components/Datatable'
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from '../../../components/Modal'

const ErrorsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  padding: ${grid(1)};
  width: 100%;
`

const TextWrapper = styled.div`
  background: #eee;
  margin-bottom: ${grid(1)};
  padding: ${grid(1)};
`

const ShowMore = styled.div`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')} ${th('colorPrimary')};
  display: flex;
  margin: ${grid(1.25)} 0 ${grid(2.5)};
  position: relative;
  width: 100%;
`

const HistoryButton = styled(Button)`
  background: ${th('colorBackground')};
  bottom: ${grid(-2)};
  opacity: 1;
  padding: 0 ${grid(1.25)};
  position: absolute;

  &:hover,
  &:active,
  &:focus {
    background: ${th('colorBackground')};
    opacity: 1;
  }
`

const ErrorHeader = styled.div`
  font-weight: bold;
  margin-top: ${th('gridUnit')};
`

const Severities = styled.ul`
  list-style: disc;
  margin: 0;
`

const noDataMessage = status =>
  status === 3
    ? 'System error: please contact NCBI Bookshelf System Admin by email at booksauthors@ncbi.nlm.nih.gov or by BCMS chat mentioning @Sysadmin.'
    : 'No errors available'

const Errors = ({ errorData, isUpdating }) => {
  const [modalIsOpen, setIsOpen] = useState(false)
  const [modalContent, setModalContent] = useState()
  const [showHistoryErrors, setShowHistoryErrors] = useState(false)
  const tables = []

  const columns = [
    {
      dataKey: 'noticeTypeName',
      disableSort: false,
      label: 'Name',
      width: 550,
    },
    {
      cellRenderer: () => () => <>-</>,
      disableSort: true,
      label: 'Category',
      width: 550,
    },
    {
      // eslint-disable-next-line react/prop-types
      cellRenderer: () => ({ cellData }) => (
        // eslint-disable-next-line react/prop-types
        <Status status={cellData.toLowerCase()} />
      ),
      dataKey: 'severity',
      disableSort: true,
      label: 'Severity',
      width: 550,
    },

    {
      dataKey: 'assignee',
      cellRenderer: () => ({ cellData }) =>
        // eslint-disable-next-line react/prop-types
        cellData.charAt(0).toUpperCase() + cellData.slice(1),
      disableSort: true,
      label: 'Assignee',
      width: 450,
    },
    {
      // eslint-disable-next-line react/prop-types
      cellRenderer: () => ({ cellData }) => (
        <div
          onClick={() => showError(cellData)}
          onKeyPress={() => showError(cellData)}
          role="textbox"
          tabIndex="0"
        >
          {cellData}
        </div>
      ),
      dynamicHeight: true,
      dataKey: 'message',
      disableSort: true,
      label: 'Message',
    },
  ]

  const filteredErrors = showHistoryErrors
    ? errorData
    : errorData.filter(x => x.isCurrent)

  filteredErrors.forEach(errorGroup => {
    const { jobId, sessionId, status, url, updated, errors } = errorGroup

    tables.push(
      <>
        <strong>
          {`Errors reported for Job ID: ${jobId} `}
          {sessionId && (
            <a href={url} rel="noreferrer" target="_blank">
              ({sessionId})
            </a>
          )}
          {` ${moment(updated).format('YYYY-MM-DD hh:mm:ss')}`}
        </strong>

        <TableLocalData
          columns={columns}
          dataTable={errors || []}
          headerHeight="50"
          key={`book-component-errors-${jobId}`}
          noDataMessage={noDataMessage(status)}
        />
      </>,
    )
  })

  function showError(cellData) {
    setModalContent(cellData)
    setIsOpen(true)
  }

  return (
    <>
      <ErrorsWrapper>
        <TextWrapper>
          Bookshelf errors are automatically reported to NCBI to resolve. View
          the{' '}
          <a href="https://preview.ncbi.nlm.nih.gov/books/NBK310885">
            NCBI Bookshelf Author Guide
          </a>{' '}
          for guidance on resolving submitter errors or select ‘Show help’
          below.
          <ErrorHeader>Error Severity</ErrorHeader>
          <Severities>
            <li>Error: must be fixed to successfully process submissions.</li>

            <li>Warning: should be reviewed for potential tagging issues.</li>

            <li>Query: should be addressed to ensure quality assurance.</li>
          </Severities>
        </TextWrapper>

        {errorData.length === 0 && <div>There are no errors</div>}

        {isUpdating && <Spinner pageLoader />}

        {!isUpdating && tables}

        {!isUpdating && errorData.length > 0 && (
          <ShowMore>
            <HistoryButton
              onClick={() => {
                setShowHistoryErrors(!showHistoryErrors)
              }}
              outlined
              status="primary"
            >
              {`${showHistoryErrors ? 'hide history' : 'show history'}`}
            </HistoryButton>
          </ShowMore>
        )}
      </ErrorsWrapper>

      <Modal
        isOpen={modalIsOpen}
        onClose={() => setIsOpen(false)}
        width="800px"
      >
        <ModalHeader hideModal={() => setIsOpen(false)} />
        <ModalBody>{modalContent}</ModalBody>
        <ModalFooter>
          <Button
            data-test-id="close-error-modal"
            onClick={() => setIsOpen(false)}
            outlined
            status="primary"
          >
            Close
          </Button>
        </ModalFooter>
      </Modal>
    </>
  )
}

const errorItemShape = {
  noticeTypeName: PropTypes.string,
  severity: PropTypes.string,
  assignee: PropTypes.string,
  message: PropTypes.string,
}

const errorDataShape = {
  jobId: PropTypes.string,
  sessionId: PropTypes.string,
  status: PropTypes.string,
  url: PropTypes.string,
  updated: PropTypes.string,
  errors: PropTypes.arrayOf(PropTypes.shape(errorItemShape)),
}

Errors.propTypes = {
  isUpdating: PropTypes.bool.isRequired,
  errorData: PropTypes.arrayOf(PropTypes.shape(errorDataShape)),
}

Errors.defaultProps = {
  errorData: [],
}

export default Errors
