import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { ActionBar, ButtonGroup, Button } from '../../../components/common'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const PreviewWrapper = styled.div`
  flex-grow: 1;
`

const NoPreviewNote = styled.div`
  align-content: center;
  align-items: center;
  align-self: center;
  display: flex;
  justify-content: center;
  padding: 8px;
`

const Preview = ({
  previewLink,
  noPreviewMessage,
  disablePublishToc,
  disableDownloadToc,
  hideTocButton,
  isCollection,
  isUpdatingToc,
  handleRepublishToc,
  handleDownloadToc,
  tocStatus,
}) => {
  const previewContent =
    previewLink && !noPreviewMessage ? (
      <iframe
        frameBorder="0"
        marginHeight="0"
        marginWidth="0"
        src={previewLink}
        style={{ height: '100%' }}
        title="prev"
        width="100%"
      />
    ) : (
      <NoPreviewNote>{noPreviewMessage}</NoPreviewNote>
    )

  const getBtnText = () => {
    return tocStatus === 'unpublished' ? 'Publish' : 'Update published version'
  }

  return (
    <Wrapper>
      <PreviewWrapper>{previewContent}</PreviewWrapper>
      <ActionBar
        leftComponent={
          <ButtonGroup>
            {!hideTocButton && (
              <Button
                data-test-id="book-comp-publish"
                disabled={
                  disablePublishToc ||
                  isUpdatingToc ||
                  tocStatus === 'publishing'
                }
                loading={isUpdatingToc}
                onClick={handleRepublishToc}
                status="primary"
              >
                {getBtnText()}
              </Button>
            )}
          </ButtonGroup>
        }
        rightComponent={
          <Button
            data-test-id="download-book-toc"
            disabled={disableDownloadToc}
            icon="download"
            onClick={handleDownloadToc}
            outlined
            status="primary"
          >
            Download
          </Button>
        }
      />
    </Wrapper>
  )
}

Preview.propTypes = {
  previewLink: PropTypes.string,
  noPreviewMessage: PropTypes.string,
  disablePublishToc: PropTypes.bool.isRequired,
  disableDownloadToc: PropTypes.bool.isRequired,
  hideTocButton: PropTypes.bool,
  isCollection: PropTypes.bool,
  isUpdatingToc: PropTypes.bool.isRequired,
  tocStatus: PropTypes.string.isRequired,
  handleRepublishToc: PropTypes.func.isRequired,
  handleDownloadToc: PropTypes.func.isRequired,
}

Preview.defaultProps = {
  isCollection: false,
  previewLink: null,
  noPreviewMessage: null,
  hideTocButton: false,
}

export default Preview
