/* eslint-disable react/prop-types */
import React from 'react'
import styled from 'styled-components'
import { useParams, useHistory } from 'react-router-dom'
import { grid, th } from '@pubsweet/ui-toolkit'

import {
  BookHeader,
  DragAndDrop,
  FixedLoader,
  Dropdown,
  Status,
  ButtonGroup,
} from '../../components'
import {
  NewBookVersion,
  BookMetadata,
  BookSettings,
  BookTeams,
} from './Book/Modals'

import WholeBookTabs from './BookComponent/asWholeBook/WholeBookTabs'
import {
  LastPublishedDate,
  StatusView,
  LastUpdatedDate,
} from './bookManagerFunctions'
import { getVersionTitle } from '../../common/utils'
import PermissionsGate from '../../../app/components/PermissionsGate'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const InlineWrapper = styled.div`
  display: flex;
`

const Root = styled.div`
  display: flex;
  flex-grow: 1;
  height: calc(100% - 58px);
`

const RightComponent = styled.div`
  align-items: center;
  display: flex;
  margin-left: auto;
`

const Separator = styled.div`
  border-right: 2px solid #eee;
  height: 35px;
  margin: 0 ${grid(1)};
`

const VersionItem = styled.div`
  background-color: ${props =>
    props.active ? th('buttonBorderColor') : th('colorTextPlaceholder')};
  color: ${th('colorPrimary')};
  display: inline;
  font-weight: ${props => (props.active ? '900' : '400')};
  height: 100%;
  padding: ${grid(0.5)};
  width: 100%;

  &:hover {
    background-color: ${th('colorSecondary')};
  }

  &[disabled] {
    background-color: #eee;
    color: ${th('colorDisabled')};
    cursor: not-allowed;
  }
`

const WholeBookPage = props => {
  const {
    book,
    handleSaveMetadata,
    iconName,
    isSaving,
    loadingBook,
    loadingNewVersion,
    searchGranthub,
    workflow,
    collectionOptions,
    bookVersions = [],
    onUpdateBook,
    onUpdateBookTeams,
    loadingUpdateBook,
    loadingUpdateBookTeams,
  } = props

  const isWholeBook = !book?.settings.chapterIndependently
  const { organizationId } = useParams()

  if (loadingBook) return <FixedLoader title="Loading data..." />

  if (!book) return null
  const history = useHistory()

  const bookVLength = (bookVersions?.length || 1) - 1

  const isOldBookVersion =
    bookVersions && bookVersions[bookVLength]?.id !== book.id

  const isWord = book.workflow === 'word'
  return (
    <Wrapper>
      {isWholeBook && (
        <BookHeader
          childrenRight={
            !isWord ? (
              <InlineWrapper>
                {isOldBookVersion && <Status status="old-book-version" />}
                {!isOldBookVersion && <NewBookVersion book={book} />}
              </InlineWrapper>
            ) : (
              <ButtonGroup>
                <PermissionsGate scopes={['view-book-metadata']}>
                  <BookMetadata
                    book={book}
                    loadingUpdateBook={loadingUpdateBook}
                    onUpdateBook={onUpdateBook}
                  />
                </PermissionsGate>
                <PermissionsGate scopes={['view-book-settings']}>
                  <BookSettings
                    book={book}
                    collectionOptions={collectionOptions}
                    loadingUpdateBook={loadingUpdateBook}
                    onUpdateBook={onUpdateBook}
                  />
                </PermissionsGate>
                <PermissionsGate scopes={['view-book-team']}>
                  <BookTeams
                    book={book}
                    loadingUpdateBookTeams={loadingUpdateBookTeams}
                    onUpdateBookTeams={onUpdateBookTeams}
                  />
                </PermissionsGate>
              </ButtonGroup>
            )
          }
          title={book?.title}
          type={iconName}
          workflow={workflow}
        />
      )}

      <Root>
        {loadingNewVersion && <FixedLoader title="Creating new version..." />}
        <PermissionsGate
          scopes={[
            book?.settings.chapterIndependently
              ? 'view-book-component'
              : 'view-book',
          ]}
        >
          <WholeBookTabs
            book={book}
            bookMetadata={book?.metadata}
            collectionOptions={collectionOptions}
            handleSaveMetadata={handleSaveMetadata}
            isLatestVersion={!isOldBookVersion}
            isSaving={isSaving}
            organizationId={organizationId}
            searchGranthub={searchGranthub}
            tabsRightComponent={
              <RightComponent>
                <LastUpdatedDate bookComponent={book} />
                <LastPublishedDate bookComponent={book} />
                <StatusView book={book} />
                {!isWord && (
                  <>
                    <Separator />
                    <Dropdown
                      itemsList={bookVersions.map((item, index) => {
                        const hasPermission = PermissionsGate({
                          getPermission: true,
                          scopes: ['view-whole-book'],
                          externalBookId: item.id,
                        })

                        return {
                          title: (
                            <VersionItem
                              active={item.id === book.id}
                              disabled={!hasPermission}
                            >
                              {getVersionTitle(item)}{' '}
                              {index === bookVersions.length - 1 && (
                                <small> (latest)</small>
                              )}
                            </VersionItem>
                          ),
                          // This generates a warning because value is expected
                          // to be a number BUT it is a uuid
                          value: item.id,
                          onClick: () => {
                            hasPermission &&
                              history.push(
                                `/organizations/${organizationId}/bookmanager/${item.id}`,
                              )
                          },
                        }
                      })}
                      outlined
                    >
                      {getVersionTitle(book)}
                    </Dropdown>
                  </>
                )}
              </RightComponent>
            }
          />
        </PermissionsGate>
      </Root>
    </Wrapper>
  )
}

export default props => (
  <DragAndDrop>
    <WholeBookPage {...props} />
  </DragAndDrop>
)
