/* eslint-disable camelcase */

import get from 'lodash/get'
import moment from 'moment-timezone'

import query from './graphql'

const { GET_BOOK_COMPONENT_CHILDREN } = query

const formatDateTime = (date, type = 'start') => {
  if (!date || !(date instanceof Date)) {
    throw new Error('Invalid date provided')
  }

  const momentDate = moment(date)

  if (type === 'start') {
    // Set the time to the start of the day (00:00:00.000)
    momentDate.startOf('day')
  } else {
    // Set the time to the end of the day (23:59:59.999)
    momentDate.endOf('day')
  }

  const customISOString = momentDate.format(`YYYY-MM-DDTHH:mm:ss.SSSZ`)

  return customISOString
}

/**
 * Flatten & gather keys are used for select all functionality
 * When selecting all, if there are disabled rows, you should select the rest
 * of them and set select all to true (as you've selected all that you can)
 * This is why disabled rows are filtered in these functions
 */

// Takes nested tree data and returns them in a flat array
const flatten = data => {
  if (!data) return undefined

  return data.reduce((accumulated, current) => {
    if (current.disabled) return accumulated // see note above

    let newItems = [current]

    if (current.children && current.children.length > 0) {
      newItems = [...newItems, ...flatten(current.children)]
    }

    return [...accumulated, ...newItems]
  }, [])
}

// Takes nested tree data and returns a flat array of their keys
const gatherKeys = data => {
  if (!data) return undefined

  return data.reduce((accumulated, current) => {
    if (current.disabled) return accumulated // see note above

    // if nested, the key will be parentId.id
    const currentKeyPath = current.key.split('.')
    const currentKey = currentKeyPath[currentKeyPath.length - 1]
    let newKeys = [currentKey]

    if (current.children && current.children.length > 0) {
      newKeys = [...newKeys, ...gatherKeys(current.children)]
    }

    return [...accumulated, ...newKeys]
  }, [])
}

// Checks if the two lists contain the same ids, irrespective of order
const isAllSelected = (allKeys, selectedKeys) => {
  if (!Array.isArray(allKeys) || !Array.isArray(selectedKeys)) return false

  // covers case where there are no book components
  if (allKeys.length === 0 && selectedKeys.length === 0) return false

  const allKeysCopy = [...allKeys]
  const selectedKeysCopy = [...selectedKeys]
  return allKeysCopy.sort().toString() === selectedKeysCopy.sort().toString()
}

const transformData = (
  data,
  level,
  parentId = null,
  book,
  organizationId,
  client,
  flat,
) => {
  if (!data || !Array.isArray(data)) return null
  if (data.length === 0) return []

  return data.map(c => {
    const {
      assigneeErrors,
      bookComponentVersionId,
      componentType,
      convertedFile,
      division,
      hasWarnings,
      id,
      isDuplicate,
      issues,
      metadata,
      publishedDate,
      sourceFiles,
      status,
      title,
      updated,
      versionName,
    } = c

    const { filename, hideInTOC, chapter_number, url, sub_title } = metadata

    let children = []

    if (!flat) {
      // if children have been requested, they will be in the cache
      // they will not be in the get division query cache, as they are not in the request schema
      const cachedValue = client.readQuery({
        query: GET_BOOK_COMPONENT_CHILDREN,
        variables: { id },
      })

      children = cachedValue?.getBookComponentById?.bookComponents.results || []
    }

    // #region show toc
    let showTOCLabel = false

    const tocStatusShow = [
      'conversion-errors',
      'loading-preview',
      'loading-errors',
      'tagging-errors',
      'preview',
      'published',
    ].includes(c.status)

    if (division.label === 'body' && tocStatusShow) {
      const orderChaptersBy = book?.settings?.toc?.order_chapters_by

      if (orderChaptersBy === 'title') {
        showTOCLabel = get(c, 'title') === 'Untitled'
      } else if (orderChaptersBy === 'date_desc') {
        showTOCLabel = !(
          get(c, 'metadata.date_publication', null) ||
          get(c, 'metadata.date_updated', null) ||
          get(c, 'metadata.date_created', null)
        )
      } else if (orderChaptersBy !== 'manual' && c.componentType !== 'part') {
        // To get here, the order_chpaters_by === 'number'
        // so we need to check the existence of the chapter_number
        // in metadata AND that chapter_number endswith a number
        showTOCLabel = get(c, 'metadata.chapter_number')
        showTOCLabel = !showTOCLabel || !showTOCLabel.replace(/\D/g, '')
      }
    }
    // #endregion show toc

    // #region review data
    let reviewData = {}

    if (c.status === 'in-review') {
      if (c.reviewsResponse) {
        const approved = c.reviewsResponse.filter(
          item => item.decision === 'approve',
        ).length

        const revise = c.reviewsResponse.filter(
          item => item.decision === 'requestChanges',
        ).length

        reviewData = { approved, revise }
      }
    }
    // #endregion review data

    const showAssigneeErrors =
      [
        'error',
        'submission-errors',
        'conversion-errors',
        'loading-errors',
      ].includes(c.status) && (c.assigneeErrors || []).length > 0

    let transformedChildren = []
    if (children.length > 0)
      transformedChildren = transformData(
        children,
        level + 1,
        id,
        book,
        organizationId,
        client,
      )

    const href = `/organizations/${organizationId}/bookmanager/${book.id}/${c.bookComponentVersionId}#${c.parentId}`

    const isPart = componentType === 'part'

    return {
      assigneeErrors,
      bookComponentVersionId,
      chapterNumber: chapter_number,
      children: transformedChildren,
      convertedFileStatus: convertedFile.length > 0 && convertedFile[0].status,
      disabled: status === 'converting',
      divisionId: division.id,
      sourceFileStatus: sourceFiles.length > 0 && sourceFiles[0].status,
      filename,
      hasSourceFile: sourceFiles.length > 0,
      hasConvertedFile: convertedFile.length > 0,
      hasIssues: issues && issues.length > 0,
      hasWarnings,
      href,
      id,
      isApproved: !!reviewData.approved,
      // isDropTarget,
      isDuplicate,
      isHiddenInTOC: hideInTOC,
      isLeaf: flat ? true : !isPart,
      isMissingFromTOC: showTOCLabel,
      isPart,
      isUnderRevision: !!reviewData.revise,
      key: `${parentId ? `${parentId}.` : ''}${id}`,
      lastPublished: publishedDate,
      lastUpdated: updated,
      level,
      parentId,
      previewLink: url,
      showAssigneeErrors,
      status,
      subTitle: sub_title,
      title,
      version: Number(versionName),
    }
  })
}

export { formatDateTime, flatten, gatherKeys, isAllSelected, transformData }
