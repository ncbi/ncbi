import { forEach, uniq } from 'lodash'

const moveSearch = ({
  searchTerm,
  divisionId,
  autoSorted,
  selectedRows,
  bookId,
  searchFn,
}) => {
  const criteria = [
    {
      field: 'bookId',
      operator: {
        eq: bookId,
      },
    },
    {
      field: 'divisionId',
      operator: {
        eq: divisionId,
      },
    },
    {
      or: [
        {
          field: 'title',
          operator: {
            containsAny: searchTerm,
          },
        },
        {
          field: 'title',
          operator: {
            contains: searchTerm,
          },
        },
      ],
    },
    {
      field: 'id',
      operator: {
        notin: selectedRows,
      },
    },
  ]

  if (autoSorted) {
    criteria.push({
      field: 'componentType',
      operator: {
        eq: 'part',
      },
    })
  }

  return searchFn({
    variables: {
      input: {
        search: {
          criteria,
        },
      },
    },
  })
}

const parseMoveSearchData = (data, divisions) => {
  const parsedData = []

  data.searchBookComponents.results.forEach(bc => {
    const { id, title, componentType, isDuplicate, parts, division } = bc
    const belongsToParts = parts.length > 0

    const divisionComponentIds = divisions?.find(d => d.id === division.id)
      ?.fullBookComponents

    if (!divisionComponentIds) {
      console.error(
        `Parse move data: Cannot find component ids for division id ${division.id}!`,
      )

      return
    }

    const isInDivision = divisionComponentIds.includes(bc.id)

    const itemCore = {
      id,
      title,
      componentType,
      isDuplicate,
    }

    if (isInDivision) {
      parsedData.push({
        ...itemCore,
        key: id,
        parentId: null,
        parentTitle: null,
        parentIsBody: true,
      })
    }

    if (belongsToParts) {
      bc.parts.map(p => {
        return parsedData.push({
          ...itemCore,
          key: `${p.id}.${id}`,
          parentId: p.id,
          parentTitle: p.title,
          parentIsBody: false,
        })
      })
    }
  })

  return parsedData
}

const handleComponentReorder = async (
  dragId,
  dropId,
  actionType,
  partId,
  parentIdsToRefresh,
  moveAbove,
  moveInside,
  moveBelow,
  refetchBookComponentChildren,
) => {
  // console.log(`Move ${dragId} ${actionType} ${dropId}`)

  const messages = []
  const baseMessage = 'Handle reorder:'
  const actionTypeValues = ['above', 'below', 'inside']

  if (!dragId) messages.push(`${baseMessage} Missing drag id`)
  if (!dropId) messages.push(`${baseMessage} Missing drop id`)

  if (!actionType || !actionTypeValues.includes(actionType)) {
    messages.push(`${baseMessage} Invalid action type "${actionType}"`)
  }

  if (!Array.isArray(parentIdsToRefresh))
    messages.push(`${baseMessage} Parent ids to refresh needs to be an array`)

  if (!moveAbove)
    messages.push(`${baseMessage} Missing move above function definition`)

  if (!moveInside)
    messages.push(`${baseMessage} Missing move inside function definition`)

  if (!moveBelow)
    messages.push(`${baseMessage} Missing move below function definition`)

  if (!refetchBookComponentChildren)
    messages.push(
      `${baseMessage} Missing refetch book component children definition`,
    )

  if (messages.length > 0) {
    messages.forEach(message => console.error(message))
    return null
  }

  const args = {
    variables: {
      input: {
        componentIds: Array.isArray(dragId) ? dragId : [dragId],
        targetId: dropId,
        ...(actionType !== 'inside' && { partId }),
      },
    },
  }

  let mutationToRun

  if (actionType === 'above') mutationToRun = moveAbove
  if (actionType === 'inside') mutationToRun = moveInside
  if (actionType === 'below') mutationToRun = moveBelow

  return mutationToRun(args).then(() => {
    let idsToRefetch = parentIdsToRefresh
    if (actionType === 'inside') idsToRefetch.push(dropId)
    idsToRefetch = uniq(parentIdsToRefresh).filter(i => i)

    refetchBookComponentChildren({
      variables: {
        ids: idsToRefetch,
      },
    })
  })
}

const handleComponentRepeat = async (
  bookId,
  componentId,
  partIds,
  parentIdsToRefresh,
  repeatBookComponent,
  refetchBookComponentChildren,
) => {
  const messages = []
  const baseMessage = 'Handle repeat:'

  if (!bookId) messages.push(`${baseMessage} Missing book id`)
  if (!componentId) messages.push(`${baseMessage} Missing component id`)

  if (!Array.isArray(parentIdsToRefresh))
    messages.push(`${baseMessage} Parent ids to refresh needs to be an array`)

  if (!refetchBookComponentChildren)
    messages.push(
      `${baseMessage} Missing refetch book component children definition`,
    )

  if (messages.length > 0) {
    messages.forEach(message => console.error(message))
    return null
  }

  const args = {
    variables: {
      bookId,
      componentId,
      partIds,
    },
  }

  return repeatBookComponent(args)
    .then(() => {
      const idsToRefetch = uniq(parentIdsToRefresh).filter(i => i)

      refetchBookComponentChildren({
        variables: {
          ids: idsToRefetch,
        },
      })
    })
    .catch(error => {
      console.error(`onRepeat failed: ${error}`)
      throw error
    })
}

const searchTree = (element, id) => {
  const array = []

  if (element.bookComponents?.includes(id)) {
    array.push(element.id)
  }

  if (element.children?.length) {
    let result = []

    forEach(element.children, (part, i) => {
      result = searchTree(element.children[i], id)
      array.push(...result)
    })
  }

  return array
}

const parentPartsForRepeatedComponent = (bookComponentId, parts) => {
  const temp = []

  forEach(parts, part => {
    temp.push(...searchTree(part, bookComponentId))
  })

  return temp
}

export {
  moveSearch,
  parseMoveSearchData,
  handleComponentReorder,
  handleComponentRepeat,
  parentPartsForRepeatedComponent,
}
