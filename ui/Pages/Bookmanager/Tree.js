/* eslint-disable react/prop-types */

import React, { useRef, useEffect, useState } from 'react'
// import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Tree as AntTree } from 'antd'
import { DownOutlined, DragOutlined } from '@ant-design/icons'

import { grid, th } from '@pubsweet/ui-toolkit'

import ChapterRow from './ChapterRow'

/**
 * TO DO:
 * if you drag to the bottom of the scrollable area, it doesn't scroll
 * there is a delay of the drop line when dragging quickly
 * row on hover shows html tooltip -- needs PR in ant / rc-tree
 */

/**
 * TERMINOLOGY:
 * A leaf is a chapter in this case, the end of a tree branch
 *
 * Drop position explained here: https://github.com/ant-design/ant-design/issues/14244#issuecomment-743170015
 * If it is 0, it means it is will go inside the drop target, not above or below it
 */

/**
 * DRAG AND DROP MAX LEVEL TEST CASES
 * cannot drop part with depth 1 onto leaf with level 3
 * cannot drop part with depth 2 onto leaf with depth 2
 * cannot drop part with depth 1 onto part with depth 2
 * cannot drop part with depth 2 onto part with depth 1
 */

// #region styled
const Wrapper = styled.div`
  height: 100%;
  overflow-y: auto;
`

const StyledTree = styled(AntTree)`
  a:hover {
    color: inherit;
  }

  .ant-tree-treenode {
    border-bottom: 1px solid ${th('colorBorder')};
    padding: ${grid(0.5)} 0;

    /* &.dragging {
      background-color: pink;
      cursor: not-allowed;
    } */
  }

  .ant-tree-node-selected {
    /* stylelint-disable-next-line declaration-no-important */
    background-color: ${th('colorBackground')} !important;

    &:hover {
      /* stylelint-disable-next-line declaration-no-important */
      background-color: ${th('colorBackgroundHue')} !important;
    }
  }

  .ant-tree-switcher {
    align-items: center;
    display: flex;
    justify-content: center;
    padding: 0 ${grid(3)};
  }

  .ant-tree-switcher-icon {
    /* stylelint-disable-next-line declaration-no-important */
    font-size: ${th('fontSizeBase')} !important;
  }

  /* .ant-tree-drop-indicator {
    border: 2px solid ${th('colorPrimary')} !important;

    &:after {
      display: none;
    }
  } */

  /* .ant-tree-indent {
    background: papayawhip;
  } */

  .ant-tree-indent-unit {
    /* background: cornflowerblue; */
    /* height: 3px; */
    /* margin-left: 45px; */
  }

  .ant-tree-draggable-icon {
    align-self: center;
    color: ${th('colorText')};
    /* stylelint-disable-next-line declaration-no-important */
    opacity: 1 !important;
  }

  /* .ant-tree-switcher-leaf-line {
    &:before {
      left: 0;
    }

    &:after {
      display: none;
    }
  }

  .ant-tree-indent-unit:before {
    left: 23px;
  } */
`
// #endregion styled

const findLevelsOfNestingInObject = obj => {
  let count = -1

  const inspectChildren = o => {
    if (o.level > count) count += 1
    if (!o.children) return

    o.children.forEach(child => {
      inspectChildren(child)
    })
  }

  inspectChildren(obj)
  return count
}

const Tree = props => {
  const {
    canDrag,
    canSelect,
    className,
    checkedKeys,
    data,
    maxNestingLevel,
    onCheck,
    onDrop,
    onLoadNestedData,
    statusesThatAllowRowMove,
  } = props

  const [height, setHeight] = useState(null)
  const [dropTarget, setDropTarget] = useState(null)
  // const [isDragItemDisabled, setIsDragItemDisabled] = useState(false)
  const ref = useRef(null)

  useEffect(() => {
    const getHeight = () => {
      setHeight(ref.current.clientHeight - 2)
    }

    getHeight()
    window.addEventListener('resize', getHeight)

    return () => window.removeEventListener('resize', getHeight)
  }, [])

  let draggable = false

  if (canDrag) {
    draggable = {
      icon: <DragOutlined />,
      // even if dnd is generally on, individual rows can have dnd disabled
      // nodeDraggable: node => {
      //   if (node.isPart) return true
      //   if (statusesThatAllowRowMove.includes(node.status)) return true
      //   return false
      // },
    }
  }

  const allowDrop = ({ dragNode, dropNode, dropPosition }) => {
    const willNest = dropPosition === 0

    if (
      !dragNode.isPart &&
      !statusesThatAllowRowMove.includes(dragNode.status)
    ) {
      return false
    }

    // Don't allow nesting if the target is not a part
    if (!dropNode.isPart && willNest) return false

    // Don't allow moving parts beyond their max nesting level
    if (dragNode.isPart && maxNestingLevel) {
      const levelsWithinDraggedPart = findLevelsOfNestingInObject(dragNode)

      const maxLeafLevel = maxNestingLevel
      const maxPartLevel = maxNestingLevel - 1

      // -- leave for testing --
      // console.log('-')
      // console.log('dropnode level', dropNode.level)
      // console.log('levels within', levelsWithinDraggedPart)
      // console.log('sum', dropNode.level + levelsWithinDraggedPart)
      // console.log('-')

      // Don't allow dropping in parallel to max leaf
      if (!willNest && dropNode.level + levelsWithinDraggedPart > maxLeafLevel)
        return false

      // Don't allow dropping inside max part
      if (willNest && dropNode.level + levelsWithinDraggedPart > maxPartLevel)
        return false
    }

    return true
  }

  const handleDrop = info => {
    const { dragNode, node: dropNode } = info

    const dragId = dragNode.id
    const dropId = dropNode.id

    const dropPos = info.node.pos.split('-')
    const dropPosition = info.dropPosition - Number(dropPos[dropPos.length - 1])

    let actionType

    if (dropPosition === -1) actionType = 'above'
    if (dropPosition === 0) actionType = 'inside'
    if (dropPosition === 1) actionType = 'below'

    const parentIds = [dragNode.parentId, dropNode.parentId]

    return onDrop(dragId, dropId, actionType, null, parentIds)
  }

  const handleDragStart = info => {
    // if (!statusesThatAllowRowMove.includes(info.node.status)) {
    //   setIsDragItemDisabled(true)
    // }
  }

  const handleDragOver = info => {
    if (info.node.id !== dropTarget) setDropTarget(info.node.id)
  }

  const handleDragEnd = () => {
    setDropTarget(null)
    // setIsDragItemDisabled(false)
  }

  const handleCheck = (ids, info) => {
    onCheck(info.checkedNodes)
  }

  return (
    <Wrapper className={className} ref={ref}>
      <StyledTree
        allowDrop={allowDrop}
        blockNode
        checkable={canSelect}
        checkedKeys={checkedKeys}
        draggable={draggable}
        height={height}
        loadData={onLoadNestedData}
        motion={{}} // disable animations for improved performance
        onCheck={handleCheck}
        onDragEnd={handleDragEnd}
        onDragOver={handleDragOver}
        onDragStart={handleDragStart}
        onDrop={handleDrop}
        // showLine
        switcherIcon={<DownOutlined />}
        titleRender={rowProps => {
          return (
            <ChapterRow
              {...rowProps}
              isDropTarget={rowProps.id === dropTarget}
            />
          )
        }}
        treeData={data}
      />
    </Wrapper>
  )
}

Tree.propTypes = {}

Tree.defaultProps = {}

export default Tree
