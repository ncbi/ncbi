/* eslint-disable react/prop-types */
import React, { useContext } from 'react'
import styled from 'styled-components'
import { useParams } from 'react-router-dom'
import { Icon, Link } from '@pubsweet/ui'
import { th, grid } from '@pubsweet/ui-toolkit'
import CurrentUserContext from '../../../app/userContext'

import {
  BookHeader,
  Button,
  ButtonGroup,
  DragAndDrop,
  FixedLoader,
  Pagination,
} from '../../components'
import { TOCLabel } from '../../components/common'
import { BookMetadata, BookSettings, BookTeams } from './Book/Modals'
import { BookComponentTabs } from './BookComponent'
import BookComponentPart from './BookComponent/BookPartComponent'
import {
  LastPublishedDate,
  StatusView,
  LastUpdatedDate,
} from './bookManagerFunctions'
import { canModifyBook } from '../../../app/userRights'
import PermissionsGate from '../../../app/components/PermissionsGate'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const Root = styled.div`
  display: flex;
  flex-grow: 1;
  height: calc(100% - 54px);
`

const PageMenu = styled.div`
  align-items: center;
  background: #eeeeee;
  display: flex;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  height: 50px;
  justify-content: space-between;
  max-width: 100%;
  padding: 2px 8px;
`

const Part = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: ${props => (props.grow ? 1 : 0)};
  justify-content: ${props => (props.grow ? 'center' : '')};
  margin-left: ${props =>
    props.margin ? `${props.margin}px` : th('gridUnit')};
  margin-right: ${props =>
    props.margin ? `${props.margin}px` : th('gridUnit')};
  overflow: ${props => (props.grow ? 'hidden' : 'visible')};
  text-align: ${props => props.align};
  text-overflow: ellipsis;
  white-space: ${props => (props.grow ? 'nowrap' : '')};
`

const Center = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
  height: 100%;
`

const LinkNav = styled(Link)`
  align-items: center;
  color: ${th('colorText')};
  cursor: pointer;
  display: flex;
  text-decoration: none;
`

const Right = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
  height: 100%;
  justify-content: flex-end;
`

const RightComponent = styled.div`
  align-items: center;
  display: flex;
  margin-left: auto;
`

const Separator = styled.div`
  border-right: 2px solid #eee;
  height: 35px;
  margin: 0 ${grid(1)};
`

const BookComponentPage = props => {
  const {
    activeBookComponentVersion,
    backPressed,
    book,
    bookComponent,
    bookComponentMetadata,
    bookComponentVersionsCount,
    collectionOptions,
    createNewBookcomponentVersion,
    divisionsMap,
    filename,
    handleSaveMetadata,
    iconName,
    isBookOrderedManually,
    isComponentInDivisionTopLevel,
    isMultiChapterBook,
    isPart,
    isSaving,
    loadingBook,
    loadingBookComponent,
    loadingNewVersion,
    // loadingUpdateBook,
    // loadingUpdateBookTeams,
    nextComponent,
    onMoveSearch,
    onPageClick,
    onReorder,
    // onUpdateBook,
    // onUpdateBookTeams,
    onRepeat,
    partsTree,
    prevComponent,
    searchGranthub,
    workflow,
  } = props

  const { bookId, organizationId } = useParams()

  if (loadingBook || loadingBookComponent)
    return <FixedLoader title="Loading data..." />
  const { currentUser } = useContext(CurrentUserContext)

  const hasOneModifierRoles = canModifyBook({
    currentUser,
    organizationId,
    bookId: book.id,
  })

  const hasEditRight = hasOneModifierRoles || currentUser?.auth.isPDF2XMLVendor

  return (
    <Wrapper>
      {/* TO DO: this will never be true? you can only get to this page from a chapter processed book */}
      {!book?.settings.chapterIndependently && (
        <BookHeader
          childrenRight={
            hasEditRight && (
              <ButtonGroup>
                <PermissionsGate scopes={['view-book-metadata']}>
                  <BookMetadata
                    book={book}
                    // loadingUpdateBook={loadingUpdateBook}
                    // onUpdateBook={onUpdateBook}
                  />
                </PermissionsGate>
                <PermissionsGate scopes={['view-book-settings']}>
                  <BookSettings
                    book={book}
                    collectionOptions={collectionOptions}
                    // loadingUpdateBook={loadingUpdateBook}
                    // onUpdateBook={onUpdateBook}
                  />
                </PermissionsGate>
                <PermissionsGate scopes={['view-book-team']}>
                  <BookTeams
                    book={book}
                    // loadingUpdateBookTeams={loadingUpdateBookTeams}
                    // onUpdateBookTeams={onUpdateBookTeams}
                  />
                </PermissionsGate>
              </ButtonGroup>
            )
          }
          title={book.title}
          type={iconName}
          workflow={workflow}
        />
      )}

      {isMultiChapterBook && (
        <PermissionsGate
          scopes={[
            book.settings.chapterIndependently
              ? 'view-book-component'
              : 'view-book',
          ]}
        >
          <PageMenu>
            <Part align="left">
              {prevComponent ? (
                <Center>
                  <LinkNav
                    data-test-id="previous-book-component"
                    to={`/organizations/${organizationId}/bookmanager/${bookId}/${prevComponent.id}#${prevComponent.partId}`}
                  >
                    <Icon>chevron-left</Icon>

                    <span>Previous</span>
                  </LinkNav>
                </Center>
              ) : (
                ''
              )}
            </Part>

            <Part
              align="center"
              grow={1}
              margin={20}
              title={
                bookComponent &&
                (bookComponent.title || '').replace(/<[^>]+>/g, '')
              }
            >
              <div>
                {bookComponent &&
                  (bookComponent.title || '').replace(/<[^>]+>/g, '')}
              </div>

              <div>{filename && `File name: ${filename}`}</div>
            </Part>

            <Part align="right">
              {nextComponent ? (
                <Right>
                  <LinkNav
                    data-test-id="next-book-component"
                    to={`/organizations/${organizationId}/bookmanager/${bookId}/${nextComponent.id}#${nextComponent.partId}`}
                  >
                    <span>Next </span>

                    <Icon>chevron-right</Icon>
                  </LinkNav>
                </Right>
              ) : (
                ''
              )}
            </Part>
          </PageMenu>
        </PermissionsGate>
      )}

      <Root>
        {loadingNewVersion && <FixedLoader title="Creating new version..." />}

        {isPart ? (
          <PermissionsGate
            scopes={[
              book.settings.chapterIndependently
                ? 'view-book-component'
                : 'view-book',
            ]}
            showPermissionInfo
          >
            <BookComponentPart
              addBodyToParts={book.settings.toc.add_body_to_parts}
              backPressed={backPressed}
              bookComponent={bookComponent}
            />
          </PermissionsGate>
        ) : (
          <PermissionsGate
            scopes={[
              book.settings.chapterIndependently
                ? 'view-book-component'
                : 'view-book',
            ]}
            showPermissionInfo
          >
            <BookComponentTabs
              book={book}
              bookComponent={bookComponent}
              bookComponentMetadata={bookComponentMetadata}
              divisionsMap={divisionsMap}
              handleSaveMetadata={handleSaveMetadata}
              isBookOrderedManually={isBookOrderedManually}
              isComponentInDivisionTopLevel={isComponentInDivisionTopLevel}
              isSaving={isSaving}
              onMoveSearch={onMoveSearch}
              onReorder={onReorder}
              onRepeat={onRepeat}
              organizationId={organizationId}
              partsTree={partsTree}
              searchGranthub={searchGranthub}
              tabsRightComponent={
                <RightComponent>
                  <LastUpdatedDate bookComponent={bookComponent} />
                  <LastPublishedDate bookComponent={bookComponent} />
                  <StatusView book={book} bookComponent={bookComponent} />
                  {bookComponent.isDuplicate && <TOCLabel duplicate />}
                  {book?.settings?.multiplePublishedVersions && (
                    <>
                      <Separator />
                      <PermissionsGate
                        scopes={[
                          book.settings.chapterIndependently
                            ? 'view-book-component-new-version'
                            : 'view-book-new-version',
                        ]}
                      >
                        <Button
                          disabled={
                            loadingNewVersion ||
                            activeBookComponentVersion !==
                              bookComponentVersionsCount ||
                            (bookComponent &&
                              bookComponent.status !== 'published')
                          }
                          onClick={createNewBookcomponentVersion}
                          outlined
                          status="primary"
                        >
                          New Published Version
                        </Button>
                      </PermissionsGate>
                      <PermissionsGate
                        scopes={[
                          book.settings.chapterIndependently
                            ? 'view-book-component-all-versions'
                            : 'view-book-all-versions',
                        ]}
                      >
                        <Pagination
                          currentPage={activeBookComponentVersion}
                          navigationIcons
                          onPageChanged={onPageClick}
                          pageLimit={1}
                          renderPage={page => `V${page}`}
                          showFirstPage
                          totalRecords={bookComponentVersionsCount}
                          versionStyle
                        />
                      </PermissionsGate>
                    </>
                  )}
                </RightComponent>
              }
            />
          </PermissionsGate>
        )}
      </Root>
    </Wrapper>
  )
}

export default props => (
  <DragAndDrop>
    <BookComponentPage {...props} />
  </DragAndDrop>
)
