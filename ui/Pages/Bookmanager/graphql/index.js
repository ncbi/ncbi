import { gql } from '@apollo/client'

const editorAuthor = `
  affiliation
  degrees
  email
  givenName
  role
  suffix
  surname
`

const publishedVersions = `
  id
  bookId
  bookComponentVersionId
  metadata {
    filename
  }
  convertedFile {
    id
    status
    id
    created
    updated
    versionName
  }
  publishedDate
  status
  versionName
`

const bookComponentFile = `
  id
  created
  updated
  name
  source
  mimeType
  versionName
  status
  bcfId
  parentId
  copied
  tag
  owner {
    id
    username
    givenName
    surname
  }
`

const bookComponentRow = `
  id
  assigneeErrors
  bookComponentVersionId
  componentType
  convertedFile {
    id
    status
  }
  division {
    id
    label
  }
  hasWarnings
  isDuplicate
  issues {
    id
  }
  metadata {
    filename
    hideInTOC
    chapter_number
    url
  }
  publishedDate
  status
  sourceFiles {
    id
    status
  }
  title
  updated
  versionName
`

const bookComponentCore = `
  id
  abstract
  bookComponentVersionId
  bookId
  componentType
  created
  division {
    id
    label
    bookComponents {
      results {
        id
        title
      }
    }
  }
  hasWarnings
  isDuplicate
  issues {
    id
  }
  metadata {
    alt_title
    author {
      ${editorAuthor}
    }
    book_component_id
    chapter_number
    date_created
    date_publication
    date_updated
    filename
    hideInTOC
    language
    section_titles
    sub_title
    url
  }
  owner {
    id
    username
    givenName
    surname
  }
  publishedDate
  reviewsResponse {
    id
    bookComponentVersionId
    decision
    messageId
    userId
  }
  status
  teams {
    id
    members {
      id
      status
      user {
        id
        givenName
        surname
        username
      }
    }
    name
    object {
      objectId
      objectType
    }
    role
  }
  title
  updated
  versionName
`

const bookComponent = `
  ${bookComponentCore}
  alias
  channels {
    id
    topic
  }
  reviewsFiles {
    id
    name
    created
    owner {
      id
      username
      givenName
      surname
    }
  }
  errors {
      id
      noticeTypeName
      severity
      assignee
      message
      objectId
  }
`

const BOOK_COMPONENT_BOOK = gql`
  fragment bookComponentBook on BookComponent {
    ${bookComponentCore}
    assigneeErrors
    hasWarnings
    issues {
      id
      status
      title
    }
    sourceFiles {
      id
      status
      id
      created
      updated
    }
    convertedFile {
      id
      status
      id
      created
      updated
    }
  }
`

const BOOK_COMPONENT_USER = gql`
  fragment bookComponentUser on BookComponent {
    ${bookComponentCore}
    issues {
      id
      status
      title
    }
    sourceFiles {
      id
      status
      id
      created
      updated
    }
    convertedFile {
      id
      status
      id
      created
      updated
    }
    errors {
      id
      noticeTypeName
      severity
      assignee
      message
      objectId
    }
    publishedVersions {
      ${publishedVersions}
    }
  }
`

const COMPONENT_FILE = gql`
  fragment bookComponentFile on FileVersion {
    id
    created
    updated
    name
    source
    mimeType
    versionName
    status
    bcfId
    parentId
    copied
    tag
    owner {
      id
      username
      givenName
      surname
    }
  }
`

const GET_BOOK_USERS = gql`
  ${COMPONENT_FILE}
  ${BOOK_COMPONENT_USER}
  query getBook($id: ID!, $divisionLabels: [String!], $skip: Int, $take: Int) {
    getBook(id: $id) {
      id
      abstract
      abstractTitle
      alias
      altTitle
      bookSubmitId
      domain
      edition
      publishedDate
      status
      subTitle
      title
      workflow
      fundedContentType
      version
      supplementaryFiles {
        ...bookComponentFile
      }
      convertedFile {
        ...bookComponentFile
      }
      supportFiles {
        ...bookComponentFile
      }
      images {
        ...bookComponentFile
      }
      pdf {
        ...bookComponentFile
      }
      sourceFiles {
        ...bookComponentFile
        relatedBookComponentFile
      }
      channels {
        id
        topic
      }
      reviewsFiles {
        id
        name
        created
        owner {
          id
          username
          givenName
          surname
        }
      }
      reviewsResponse {
        id
        messageId
        bookComponentVersionId
        userId
        decision
      }
      collection {
        id
        collectionType
        fileCover {
          id
          name
        }
        metadata {
          applyCoverToBooks
          applyPermissionsToBooks
          applyPublisherToBooks
          chapterProcessedSourceType
          copyrightStatement
          grants {
            id
            apply
            country
            institution_acronym
            institution_code
            institution_name
            number
          }
          licenseStatement
          licenseType
          licenseUrl
          notes {
            applyNotePdf
            description
            title
            type
          }
          openAccessLicense
          pub_loc
          pub_name
          wholeBookSourceType
        }
      }
      fileCover {
        id
        name
      }
      fileAbstract {
        id
        name
      }
      organisation {
        id
        settings {
          type {
            publisher
            funder
          }
        }
        users {
          results {
            id
            username
          }
        }
        publisherOptions {
          id
          name
        }
        collections {
          id
          title
        }
        teams {
          id
          role
          members {
            id
            user {
              username
              givenName
              surname
              id
            }
            status
          }
          object {
            objectId
            objectType
          }
          name
        }
      }
      bookComponents(divisionLabels: $divisionLabels) {
        ...bookComponentUser
      }
      toc {
        id
        updated
        versionName
        status
        publishedDate
        tocFiles {
          id
          status
          id
          created
          updated
        }
        alias
        errors {
          id
          noticeTypeName
          severity
          assignee
          message
        }
      }
      parts {
        id
        bookId
        bookComponentVersionId
        title
        updated
        created
        publishedDate
        abstract
        # divisionId
      }
      divisions(divisionLabels: $divisionLabels) {
        id
        bookComponents(skip: $skip, take: $take) {
          metadata {
            total
            skip
            take
          }
          results {
            ...bookComponentUser
          }
        }
        fullBookComponents
        label
      }
      settings {
        alternateVersionsPdf
        alternateVersionsPdfBook
        approvalOrgAdminEditor
        approvalPreviewer
        bookLevelAffiliationStyle
        bookLevelLinks
        bookLevelLinksMarkdown
        chapterIndependently
        chapterLevelAffiliationStyle
        citationSelfUrl
        citationType
        createPdf
        createVersionLink
        createWholebookPdf
        displayObjectsLocation
        footnotesDecimal
        formatChapterDate
        formatPublicationDate
        indexChaptersInPubmed
        indexInPubmed
        multiplePublishedVersions
        publisher {
          id
          name
        }
        publisherUrl
        questionAnswerStyle
        referenceListStyle
        specialPublisherLinkText
        specialPublisherLinkUrl
        toc {
          add_body_to_parts
          allTitle
          contributors
          documentHistory
          group_chapters_in_parts
          order_chapters_by
          subtitle
          tocMaxDepth
        }
        tocExpansionLevel
        UKPMC
        versionLinkText
        versionLinkUri
        xrefAnchorStyle
        # publisher
        # qaStatus
        # releaseStatus
        # supportMultiplePublishedVersions
      }
      teams {
        id
        role
        members {
          id
          user {
            username
            givenName
            surname
            id
          }
          status
        }
        object {
          objectId
          objectType
        }
        name
      }
      metadata {
        author {
          ${editorAuthor}
        }
        collaborativeAuthors {
          givenName
        }
        copyrightStatement
        dateCreated {
          day
          month
          year
        }
        dateUpdated {
          day
          month
          year
        }
        doi
        editor {
          ${editorAuthor}
        }
        grants {
          id
          apply
          country
          institution_acronym
          institution_code
          institution_name
          number
        }
        isbn
        issn
        licenseStatement
        licenseType
        licenseUrl
        notes {
          description
          title
          type
        }
        openAccess
        openAccessLicense
        pubDate {
          dateType
          publicationFormat
          date {
            day
            month
            year
          }
          dateRange {
            startMonth
            startYear
            endMonth
            endYear
          }
        }
        pub_loc
        pub_name
        sourceType
        volume
        url
      }
      issues {
        id
        status
        title
      }
    }
  }
`

// ${BOOK_COMPONENT_BOOK}
const GET_BOOK = gql`
  ${COMPONENT_FILE}
  
  query getBook($id: ID!) {
    getBook(id: $id) {
      id
      abstract
      abstractTitle
      alias
      altTitle
      bookSubmitId
      domain
      edition
      publishedDate
      status
      subTitle
      title
      workflow
      fundedContentType
      version
      convertedFile {
        ...bookComponentFile
      }
      sourceFiles {
        ...bookComponentFile
        relatedBookComponentFile
      }
      reviewsFiles {
        id
        name
        created
        owner {
          id
          username
          givenName
          surname
        }
      }
      reviewsResponse {
        id
        messageId
        bookComponentVersionId
        userId
        decision
      }
      collection {
        id
        collectionType
        fileCover {
          id
          name
        }
        metadata {
          applyCoverToBooks
          applyPermissionsToBooks
          applyPublisherToBooks
          chapterProcessedSourceType
          copyrightStatement
          grants {
            id
            apply
            country
            institution_acronym
            institution_code
            institution_name
            number
          }
          licenseStatement
          licenseType
          notes {
            applyNotePdf
            description
            title
            type
          }
          openAccessLicense
          pub_loc
          pub_name
          wholeBookSourceType
        }
      }
      fileCover {
        id
        name
      }
      fileAbstract {
        id
        name
      }
      organisation {
        id
        settings
        {
          type{
            publisher
            funder
          }
        }
        users {
          results {
            id
            username
          }
        }
        publisherOptions {
          id
          name
        }
        collections {
          id
          title
        }
        teams {
          id
          role
          members {
            id
            user {
              username
              givenName
              surname
              id
            }
            status
          }
          object {
            objectId
            objectType
          }
          name
        }
      }
      toc {
        id
        updated
        versionName
        status
        publishedDate
        tocFiles {
          id
          status
          id
          created
          updated
        }
        alias
        errors {
          id
          noticeTypeName
          severity
          assignee
          message
        }
      }
      parts {
        id
        bookComponents {
          results {
            id
          }
        }
        title
      #   updated
      #   created
      #   publishedDate
      #   abstract
      #   divisionId
      }
      divisions {
        id
        label
        fullBookComponents
		partsNested
      }
      settings {
        alternateVersionsPdf
        alternateVersionsPdfBook
        approvalOrgAdminEditor
        approvalPreviewer
        bookLevelAffiliationStyle
        bookLevelLinks
        bookLevelLinksMarkdown
        chapterIndependently
        chapterLevelAffiliationStyle
        citationSelfUrl
        citationType
        createPdf
        createVersionLink
        createWholebookPdf
        displayObjectsLocation
        footnotesDecimal
        formatChapterDate
        formatPublicationDate
        indexChaptersInPubmed
        indexInPubmed
        multiplePublishedVersions
        publisher {
          id
          name
        }
        publisherUrl
        questionAnswerStyle
        referenceListStyle
        specialPublisherLinkText
        specialPublisherLinkUrl
        toc {
          add_body_to_parts
          allTitle
          contributors
          documentHistory
          group_chapters_in_parts
          order_chapters_by
          subtitle
          tocMaxDepth
        }
        tocExpansionLevel
        UKPMC
        versionLinkText
        versionLinkUri
        xrefAnchorStyle
        # publisher
        # qaStatus
        # releaseStatus
        # supportMultiplePublishedVersions
      }
      teams {
        id
        role
        members {
          id
          user {
            username
            givenName
            surname
            id
          }
          status
        }
        object {
          objectId
          objectType
        }
        name
      }
      metadata {
        author {
          ${editorAuthor}
        }
        collaborativeAuthors {
          givenName
        }
        copyrightStatement
        dateCreated {
          day
          month
          year
        }
        dateUpdated {
          day
          month
          year
        }
        doi
        editor {
          ${editorAuthor}
        }
        grants {
          id
          apply
          country
          institution_acronym
          institution_code
          institution_name
          number
        }
        isbn
        issn
        licenseStatement
        licenseType
        licenseUrl
        notes {
          description
          title
          type
        }
        openAccess
        openAccessLicense
        pubDate {
          dateType
          publicationFormat
          date {
            day
            month
            year
          }
          dateRange {
            startMonth
            startYear
            endMonth
            endYear
          }
        }
        pub_loc
        pub_name
        sourceType
        volume
        url
      }
      issues {
        id
        status
        title
      }
    }
  }
`

const GET_DIVISION = gql`
  query GetDivision($id: ID!, $skip: Int!, $take: Int!) {
    getDivision(id: $id) {
      id
      label
      bookComponents(skip: $skip, take: $take) {
        metadata {
          total
        }
        results {
          ${bookComponentRow}
        }
      }
      partsNested
    }
  }
`

const GET_BOOK_COMPONENT_CHILDREN = gql`
  query GetBookComponentChildren($id: ID!) {
    getBookComponentById(id: $id) {
      id
      bookComponents {
        results {
          ${bookComponentRow}
        }
      }
    }
  }
`

const GET_MULTIPLE_BOOK_COMPONENT_CHILDREN = gql`
  query GetMultipleBookComponentChildren($ids: [ID!]!) {
    getBookComponentsById(ids: $ids) {
      id
      bookComponents {
        results {
          ${bookComponentRow}
        }
      }
    }
  }
`

const GET_BOOK_COMPONENT_PUBLISHED = gql`
query getBookComponent($id: ID!, $skip: Int, $take: Int) {
  getBookComponent(id: $id) {
    ${bookComponent}
    bookComponents(skip: $skip, take: $take) {
      metadata {
        total
        skip
        take
      }
      results {
        ${bookComponent}
      }
    }
    publishedVersions {
      ${publishedVersions}
    }
  }
}
`

const UPDATE_BOOK = gql`
  mutation($id: ID!, $input: UpdateInput) {
    updateBook(id: $id, input: $input) {
      id
      abstract
      alias
      altTitle
      bookSubmitId
      domain
      edition
      publishedDate
      subTitle
      title
      fileAbstract {
        id
        name
      }
      fileCover {
        id
        name
      }
      metadata {
        author {
          ${editorAuthor}
        }
        collaborativeAuthors {
          ${editorAuthor}
        }
        copyrightStatement
        dateCreated {
          day
          month
          year
        }
        dateUpdated {
          day
          month
          year
        }
        doi
        editor {
          ${editorAuthor}
        }
        grants {
          id
          apply
          country
          institution_acronym
          institution_code
          institution_name
          number
        }
        isbn
        issn
        licenseStatement
        licenseType
        licenseUrl
        openAccess
        openAccessLicense
        pubDate {
          dateType
          publicationFormat
          date {
            day
            month
            year
          }
          dateRange {
            startMonth
            startYear
            endMonth
            endYear
          }
        }
        pub_loc
        pub_name
        sourceType
        volume
      }
      settings {
        alternateVersionsPdf
        alternateVersionsPdfBook
        approvalOrgAdminEditor
        approvalPreviewer
        bookLevelAffiliationStyle
        bookLevelLinks
        bookLevelLinksMarkdown
        chapterIndependently
        chapterLevelAffiliationStyle
        citationSelfUrl
        citationType
        createPdf
        createVersionLink
        createWholebookPdf
        displayObjectsLocation
        footnotesDecimal
        formatChapterDate
        formatPublicationDate
        indexChaptersInPubmed
        indexInPubmed
        multiplePublishedVersions
        publisher {
          id
          name
        }
        publisherUrl
        questionAnswerStyle
        referenceListStyle
        specialPublisherLinkText
        specialPublisherLinkUrl
        toc {
          add_body_to_parts
          allTitle
          contributors
          documentHistory
          group_chapters_in_parts
          order_chapters_by
          subtitle
          tocMaxDepth
        }
        tocExpansionLevel
        UKPMC
        versionLinkText
        versionLinkUri
        xrefAnchorStyle
        # publisher
        # qaStatus
        # releaseStatus
        # supportMultiplePublishedVersions
      }
    }
  }
`

const UPDATE_COLLECTION = gql`
  mutation($id: ID!, $input: UpdateCollectionInput) {
    updateCollection(id: $id, input: $input) {
      id
      title
      abstract
      abstractTitle
      domain
      collectionType
      settings {
        approvalOrgAdminEditor
        approvalPreviewer
        groupBooksBy
        isCollectionGroup
        orderBooksBy
        toc {
          contributors
          publisher
        }
      }

      metadata {
        pub_date
        pub_loc
        pub_name
      }
    }
  }
`

const DELETE_BOOKS = gql`
  mutation($id: [ID]) {
    deleteBook(id: $id)
  }
`

const CHECKALL_BOOK = gql`
  mutation($action: Actions!) {
    checkAllBook(action: $action)
  }
`

const UPDATE_BOOK_TEAM = gql`
  mutation updateBookTeamMembers($input: [BookTeamMemberInput]) {
    updateBookTeamMembers(input: $input) {
      id
      members {
        id
        user {
          id
        }
      }
      role
    }
  }
`

const SUBMIT_BOOK_COMPONENTS = gql`
  mutation submitBookComponents($bookComponents: [ID]) {
    submitBookComponents(bookComponents: $bookComponents)
  }
`

const UPLOAD_CONVERT_FILE = gql`
  mutation uploadConvertFiles($files: [FileUploadInput!], $id: ID! $versionName: Int, $category: String!) {
    uploadConvertFiles(files: $files, id: $id, versionName: $versionName, category: $category) {
      id
      title
      bookComponents {
          id
          bookId
          bookComponentVersionId
          title
          componentType
          updated
          created
          publishedDate
          abstract
          # divisionId
          versionName
          status

          publishedVersions {
            ${publishedVersions}
          }
          teams {
            id
            role
            members {
              id
              user {
                username
                givenName
                surname
                id
              }
              status
            }
            object {
              objectId
              objectType
            }
            name
          }
          reviewsResponse {
            id
            messageId
            bookComponentVersionId
            userId
            decision
          }
          publishedVersions {
            ${publishedVersions}
          }
          metadata {
            author {
              ${editorAuthor}
            }
            book_component_id
            language
            alt_title
            sub_title
            date_created
            date_updated
            section_titles
            filename
            hideInTOC
          }
          owner {
            id
            username
            givenName
            surname
          }
        }
      divisions {
        id
        label
        fullBookComponents
      }
    }
  }
`

const UPLOAD_CONVERT_FILE_OPTIMIZED = gql`
  mutation uploadConvertFiles(
    $files: [FileUploadInput!]
    $id: ID!
    $versionName: Int
    $category: String!
  ) {
    uploadConvertFiles(
      files: $files
      id: $id
      versionName: $versionName
      category: $category
    ) {
      id
    }
  }
`

const GET_BOOK_DIVISIONS = gql`
  query getBookDivisions($bookId: ID!) {
    getBookDivisions(bookId: $bookId) {
      id
    }
  }
`

const GET_BOOK_COMPONENTS = gql`
  query getBookComponents($bookId: ID!) {
    getBookComponents(bookId: $bookId) {
      ${bookComponent}
    }
  }
`

const SEARCH_BOOK_COMPONENTS = gql`
  query searchBookComponents($input: SearchModelInput) {
    searchBookComponents(input: $input) {
      metadata {
        total
        skip
        take
      }
      results {
        ${bookComponentRow}
        parts {
          id
          title
        }
      }
    }
  }
`

const GET_BOOK_COMPONENT = gql`
  query getBookComponent($id: ID!, $skip: Int, $take: Int) {
    getBookComponent(id: $id) {
      ${bookComponent}
      bookComponents(skip: $skip, take: $take) {
        metadata {
          total
          skip
          take
        }
        results {
          ${bookComponent}
        }
      }
      publishedVersions {
        ${publishedVersions}
      }
      supplementaryFiles {
        ${bookComponentFile}
      }
      convertedFile {
        ${bookComponentFile}
      }
      supportFiles {
        ${bookComponentFile}
      }
      images {
        ${bookComponentFile}
      }
      pdf {
        ${bookComponentFile}
      }
      sourceFiles {
        ${bookComponentFile}
        relatedBookComponentFile
      }
    }
  }
`

const GET_BOOK_COMPONENT_BOOK = gql`
  ${BOOK_COMPONENT_BOOK}
  query getBookComponent($id: ID!, $skip: Int, $take: Int) {
    getBookComponent(id: $id) {
      ...bookComponentBook
      bookComponents(skip: $skip, take: $take) {
        metadata {
          total
          skip
          take
        }
        results {
          ...bookComponentBook
        }
      }
    }
  }
`

const GET_BOOK_COMPONENT_USER = gql`
  ${BOOK_COMPONENT_USER}
  query getBookComponent($id: ID!, $skip: Int, $take: Int) {
    getBookComponent(id: $id) {
      ...bookComponentUser
      bookComponents(skip: $skip, take: $take) {
        metadata {
          total
          skip
          take
        }
        results {
          ...bookComponentUser
        }
      }
    }
  }
`

const UPLOAD_PROGRESS = gql`
  subscription uploadConvertFileProgress {
    uploadConvertFileProgress {
      file
      percentage
      total
      status
    }
  }
`

const UPDATE_BOOK_COMPONENT = gql`
  mutation updateBookComponent($id: ID!, $input: UpdateBookComponentInput) {
    updateBookComponent(id: $id, input: $input) {
      ${bookComponent}
    }
  }
`

const UPDATE_BOOK_COMPONENTS = gql`
  mutation updateBookComponents($bookComponents: [UpdateBookComponentInput]) {
    updateBookComponents(bookComponents: $bookComponents) {
      ${bookComponent}
    }
  }
`

const UPDATE_BOOK_COMPONENT_ORDER = gql`
  mutation updateBookComponentOrder(
    $divisionId: ID
    $index: Int
    $ids: [ID]
    $partId: ID
    $bookId: ID
  ) {
    updateBookComponentOrder(
      divisionId: $divisionId
      index: $index
      ids: $ids
      partId: $partId
      bookId: $bookId
    ) {
      id
    }
  }
`

const REPEAT_BOOK_COMPONENT = gql`
  mutation repeatBookComponent($partIds: [ID], $componentId: ID, $bookId: ID) {
    repeatBookComponent(
      componentId: $componentId
      partIds: $partIds
      bookId: $bookId
    ) {
      id
    }
  }
`

const UPLOAD_FILE = gql`
  mutation($files: [FileUploadInput!], $id: ID!, $category: String) {
    uploadFile(files: $files, id: $id, category: $category) {
      ${bookComponentFile}
    }
  }
`

const DELETE_FILE = gql`
  mutation deleteFile($versionName: String, $parentId: ID!) {
    deleteFile(versionName: $versionName, parentId: $parentId)
  }
`

const BOOK_COMPONENT_PUBLISH = gql`
  mutation publishBookComponents($bookComponent: [ID!]) {
    publishBookComponents(bookComponent: $bookComponent) {
      id
      status
    }
  }
`

const RETRY = gql`
  mutation retry($id: [ID!]) {
    retry(id: $id) {
      ${bookComponent}
    }
  }
`

const NEW_BOOK_COMPONENT_VERSION = gql`
mutation createNewPublishedVersion($id: ID!) {
  createNewPublishedVersion(id:$id){
    ${bookComponent}
  }
}
`

const NEW_PART = gql`
  mutation addBookComponent($id: ID!, $input: BookPart) {
    addBookComponent(id: $id, input: $input) {
      id
      bookId
      bookComponentVersionId
      componentType
      title
      updated
      created
      publishedDate
      abstract
      # divisionId
      versionName
      status
      teams {
        id
        role
        members {
          id
          user {
            username
            givenName
            surname
            id
          }
          status
        }
        object {
          objectId
          objectType
        }
        name
      }
      metadata {
        author {
          ${editorAuthor}
        }
        book_component_id
        language
        alt_title
        sub_title
        date_created
        date_updated
        section_titles
        filename
      }
      channels {
        id
        topic
      }
      reviewsResponse {
        id
        messageId
        bookComponentVersionId
        userId
        decision
      }
      publishedVersions {
        ${publishedVersions}
      }
    }
  }
`

const NEW_MESSAGE = gql`
  mutation(
    $content: String
    $type: String
    $channelId: String
    $files: [FileUploadInput!]
  ) {
    createMessage(
      content: $content
      type: $type
      channelId: $channelId
      files: $files
    ) {
      content
      user {
        id
      }
      id
      created
      updated
      files {
        id
        name
      }
      type
    }
  }
`

const FEEDBACK_PANEL_MESSAGES = gql`
  query messages($channelId: ID, $first: Int, $after: String, $before: String) {
    messages(
      channelId: $channelId
      first: $first
      after: $after
      before: $before
    ) {
      edges {
        content
        user {
          id
          username
        }
        id
        created
        updated
        files {
          id
          name
        }
        type
      }
      pageInfo {
        startCursor
        hasPreviousPage
        hasNextPage
      }
    }
  }
`

const MESSAGES_SUBSCRIPTION = gql`
  subscription messageCreated($channelId: ID) {
    messageCreated(channelId: $channelId) {
      id
      created
      updated
      content
      type
      files {
        id
        name
      }
      user {
        id
        username
      }
    }
  }
`

const GET_BOOK_COMPONENT_ERRORS = gql`
  query getErrors($objectId: ID, $showHistory: Boolean) {
    getErrors(objectId: $objectId, showHistory: $showHistory) {
      ... on JobReportError {
        id
        jobId
        sessionId
        url
        status
        updated
        errors {
          id
          noticeTypeName
          severity
          assignee
          message
          objectId
        }
      }
      ... on BcmsReportError {
        id
        jobId
        updated
        errors {
          id
          noticeTypeName
          severity
          assignee
          message
          objectId
        }
      }
    }
  }
`

const CONVERT_SUBSCRIPTION = gql`
  subscription conversionCompleted($bookId: ID!) {
    conversionCompleted(bookId: $bookId) {
      id
      title
      updated
      status
      bookId
      # divisionId
      bookComponentVersionId
      componentType
      metadata {
        chapter_number
        filename
      }
    }
  }
`

const OBJECT_UPDATE_SUBSCRIPTION = gql`
  subscription objectUpdated($id: ID) {
    objectUpdated(id: $id) {
      id
    }
  }
`

const RECONVERT_FILE = gql`
  mutation reconvertBookComponent($ids: [ID!]!) {
    reconvertBookComponent(ids: $ids) {
      ${bookComponent}
    }
  }
`

const GET_ORGANISATION_USERS = gql`
  query getOrganisation($input: SearchModelInput, $id: ID!) {
    getOrganisation(input: $input, id: $id) {
      id
      name
      settings {
        type {
          publisher
          funder
        }
        collections {
          funded
          bookSeries
        }
      }
      teams {
        id
        role
        name
        members {
          id
          user {
            id
            username
            givenName
            surname
          }
          status
          awardeeAffiliation
          awardNumber
        }
      }
      users {
        metadata {
          total
          skip
          take
        }
        results {
          id
          username
          email
          givenName
          surname
          teams {
            id
            role
            name
            members {
              id
              user {
                id
              }
              status
              awardeeAffiliation
              awardNumber
            }
          }
        }
      }
    }
  }
`

const GET_VENDOR_ISSUESDT = gql`
  query getIssuesDatatable($input: SearchModelInput) {
    getIssuesDatatable(input: $input) {
      metadata {
        total
        skip
        take
      }
      results {
        id
        title
        content
        objectId
        user {
          username
          givenName
          surname
          id
        }
        status
        created
        updated
        files {
          id
          name
        }
      }
    }
  }
`

const NEW_ISSUE = gql`
  mutation createIssue(
    $title: String
    $content: String
    $objectId: String
    $files: [FileUploadInput!]
  ) {
    createIssue(
      content: $content
      title: $title
      objectId: $objectId
      files: $files
    ) {
      id
      title
      content
      user {
        username
        givenName
        surname
        id
      }
      created
      updated
      files {
        id
        name
      }
    }
  }
`

const NEW_COMMENT = gql`
  mutation(
    $type: String
    $content: String
    $issueId: String
    $files: [FileUploadInput!]
  ) {
    createComment(
      content: $content
      type: $type
      issueId: $issueId
      files: $files
    ) {
      id
      content
      user {
        username
        givenName
        surname
        id
      }
      type
      created
      updated
      files {
        id
        name
      }
    }
  }
`

const GET_COMMENTS = gql`
  query getComments($issueId: ID!) {
    getComments(issueId: $issueId) {
      id
      content
      issueId
      user {
        username
        givenName
        surname
        id
      }
      created
      updated
      files {
        id
        name
      }
      type
    }
  }
`

const UPDATE_ISSUE = gql`
  mutation updateIssue($id: [ID], $status: String) {
    updateIssue(id: $id, status: $status) {
      id
      status
    }
  }
`

const UPDATETAGFILE = gql`
  mutation updateTagFile($id: ID!, $tag: String!) {
    updateTagFile(id: $id, tag: $tag) {
      id
    }
  }
`

const COMMENTS_SUBSCRIPTION = gql`
  subscription commentCreated($issueId: ID) {
    commentCreated(issueId: $issueId) {
      id
      created
      updated
      content
      type
      files {
        id
        name
      }
      user {
        username
        givenName
        surname
        id
      }
    }
  }
`

const GET_ORGANIZATION = gql`
  query GetOrganisation($id: ID!) {
    getOrganisation(id: $id) {
      id
      name

      collections {
        id
        title
      }
      publisherOptions {
        id
        name
      }
      settings {
        pdf
        word
        xml
        collections {
          funded
        }

        type {
          publisher
          funder
        }
      }
      teams {
        id
        role
        name
        members {
          status
          user {
            id
            username
          }
        }
      }
    }
  }
`

const GET_GLOBAL_TEAMS = gql`
  query teams($where: TeamWhereInput) {
    teams(where: $where) {
      id
      role
      name
    }
  }
`

const GET_ALL_USERS = gql`
  query AllUsers($input: SearchModelInput) {
    searchUsers(input: $input) {
      results {
        id
        username
        email
        givenName
        surname
        auth {
          isSysAdmin
          isPDF2XMLVendor
          sysAdminForOrganisation
          orgAdminForOrganisation
        }
        organisations {
          id
          name
          settings {
            type {
              publisher
              funder
            }
          }
          teams {
            id
            role
            name
            members {
              id
              status
              user {
                id
              }
            }
          }
        }
      }
    }
  }
`

const GET_FUNDED_COLLECTIONS_OF_OTHER_ORGANIZATIONS = gql`
  query GetFundedCollectionsOfOtherOrganizations($organisationId: ID!) {
    getFundedCollectionsOfOtherOrganisations(organisationId: $organisationId) {
      id
      collectionType
      title
      organisation {
        id
        name
      }
    }
  }
`

const CREATE_NEW_BOOK_VERSION = gql`
  mutation CreateBookVersion($input: CreateBookVersionInput!) {
    createBookVersion(input: $input) {
      id
      fundedContentType
      workflow
      version
    }
  }
`

const MOVE_ABOVE = gql`
  mutation MoveAbove($input: MoveRelativeInput!) {
    moveAbove(input: $input) {
      id
    }
  }
`

const MOVE_BELOW = gql`
  mutation MoveBelow($input: MoveRelativeInput!) {
    moveBelow(input: $input) {
      id
    }
  }
`

const MOVE_INSIDE = gql`
  mutation MoveInside($input: MoveComponentInput!) {
    moveInsidePart(input: $input) {
      id
    }
  }
`

export default {
  BOOK_COMPONENT_PUBLISH,
  CHECKALL_BOOK,
  COMMENTS_SUBSCRIPTION,
  CONVERT_SUBSCRIPTION,
  CREATE_NEW_BOOK_VERSION,
  DELETE_BOOKS,
  DELETE_FILE,
  FEEDBACK_PANEL_MESSAGES,
  GET_ALL_USERS,
  GET_BOOK_COMPONENT_BOOK,
  GET_BOOK_COMPONENT_CHILDREN,
  GET_BOOK_COMPONENT_ERRORS,
  GET_BOOK_COMPONENT_PUBLISHED,
  GET_BOOK_COMPONENT_USER,
  GET_BOOK_COMPONENT,
  GET_BOOK_COMPONENTS,
  GET_BOOK_DIVISIONS,
  GET_BOOK_USERS,
  GET_BOOK,
  GET_COMMENTS,
  GET_DIVISION,
  GET_FUNDED_COLLECTIONS_OF_OTHER_ORGANIZATIONS,
  GET_GLOBAL_TEAMS,
  GET_MULTIPLE_BOOK_COMPONENT_CHILDREN,
  GET_ORGANISATION_USERS,
  GET_ORGANIZATION,
  GET_VENDOR_ISSUESDT,
  MESSAGES_SUBSCRIPTION,
  MOVE_ABOVE,
  MOVE_BELOW,
  MOVE_INSIDE,
  NEW_BOOK_COMPONENT_VERSION,
  NEW_COMMENT,
  NEW_ISSUE,
  NEW_MESSAGE,
  NEW_PART,
  OBJECT_UPDATE_SUBSCRIPTION,
  RECONVERT_FILE,
  REPEAT_BOOK_COMPONENT,
  RETRY,
  SEARCH_BOOK_COMPONENTS,
  SUBMIT_BOOK_COMPONENTS,
  UPDATE_BOOK_COMPONENT_ORDER,
  UPDATE_BOOK_COMPONENT,
  UPDATE_BOOK_COMPONENTS,
  UPDATE_BOOK_TEAM,
  UPDATE_BOOK,
  UPDATE_COLLECTION,
  UPDATE_ISSUE,
  UPDATETAGFILE,
  UPLOAD_CONVERT_FILE,
  UPLOAD_CONVERT_FILE_OPTIMIZED,
  UPLOAD_FILE,
  UPLOAD_PROGRESS,
}
