/* eslint-disable react/prop-types */

import React, { useRef } from 'react'
import styled from 'styled-components'
import { grid } from '@pubsweet/ui-toolkit'

import { useMutation } from '@apollo/client'

import { FormBookPart } from '../Book/Form'
import query from '../graphql'

import {
  ActionBar,
  ButtonGroup,
  Button,
  Notification,
} from '../../../components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  height: 100%;
  overflow-y: auto;
  padding: ${grid(2)} ${grid(2)} 0 ${grid(2)};
`

const BookComponentPart = props => {
  const {
    // addBodyToParts,
    bookComponent,
    backPressed,
  } = props

  const innerRef = useRef(null)

  const [updateBookComponent, { loading }] = useMutation(
    query.UPDATE_BOOK_COMPONENT,
  )

  const handleSavePart = async formData => {
    if (formData) {
      const input = {
        id: bookComponent.id,
        title: formData.title,
        content: formData.content,
        metadata: {
          editor: formData.metadata.editor,
        },
      }

      await updateBookComponent({
        variables: {
          id: bookComponent.bookComponentVersionId,
          input,
        },
      }).catch(res => {
        console.error(res)

        Notification({
          message: 'Could not update',
          type: 'danger',
          title: 'Error',
        })
      })

      backPressed()
    }
  }

  return (
    <Wrapper>
      <FormWrapper>
        <FormBookPart
          // formikFormRef={innerRef}
          handleSubmit={handleSavePart}
          // showContent={addBodyToParts}
          innerRef={innerRef}
          showContent={false}
          values={{
            title: bookComponent.title,
            metadata: bookComponent.metadata,
            id: bookComponent.id,
          }}
        />
      </FormWrapper>
      <ActionBar
        leftComponent={
          <ButtonGroup>
            <Button
              data-test-id="save-book-metadata"
              disabled={loading}
              onClick={async () => {
                await innerRef.current.handleSubmit()
              }}
              status="primary"
            >
              {loading ? 'Saving...' : 'Save'}
            </Button>
            <Button
              data-test-id="cancel-book-metadata"
              onClick={backPressed}
              outlined
              status="primary"
            >
              {loading ? 'Close' : 'Cancel'}
            </Button>
          </ButtonGroup>
        }
        rightComponent={
          <Button
            icon="trash-2"
            iconPosition="end"
            onClick={() => {
              // TODO: delete functionality
            }}
            outlined
          >
            Delete part
          </Button>
        }
      />
    </Wrapper>
  )
}

export default BookComponentPart
