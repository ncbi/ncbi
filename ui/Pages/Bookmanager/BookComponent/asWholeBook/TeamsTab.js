/* eslint-disable react/prop-types */
import React, { useRef } from 'react'
import { useMutation } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'
import styled from 'styled-components'
import {
  TableWithToolbar,
  Button,
  FixedLoader,
  ActionBar,
} from '../../../../components'

import { FormBookTeam } from '../../Book/Form'
import PermissionsGate from '../../../../../app/components/PermissionsGate'
import query from '../../graphql'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;

  > div:first-child {
    overflow-y: auto;
  }
`

const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;

  > div:first-child {
    flex-grow: 1;
  }
`

const BookTeamsTab = ({ book }) => {
  const modalRef = useRef(null)
  const [updateBookTeam, { loading }] = useMutation(query.UPDATE_BOOK_TEAM)

  const saveBookTeam = async () => {
    const formData = await modalRef.current.triggerSave()

    if (formData) {
      const members = formData.teams.map(team => ({
        id: team.id,
        team: {
          members: team.members.map(tm => ({
            status: tm.status,
            user: { id: tm.user.id },
          })),
        },
      }))

      updateBookTeam({
        awaitRefetchQueries: true,
        refetchQueries: [
          getOperationName(query.GET_BOOK_USERS),
          getOperationName(query.GET_BOOK_COMPONENT),
        ],
        variables: {
          input: members,
        },
      }).catch(res => {
        console.error(res)

        Notification({
          message: 'Could not update',
          type: 'danger',
          title: 'Error',
        })
      })
    }
  }

  return (
    <Wrapper>
      <TableWithToolbar>
        {tableProps => (
          <FormWrapper>
            <PermissionsGate
              errorProps={{ readonly: true }}
              scopes={['edit-book-team']}
            >
              <div>
                <FormBookTeam
                  formikFormRef={modalRef}
                  tableProps={tableProps}
                  values={book}
                />
              </div>
              <ActionBar
                leftComponent={
                  <PermissionsGate scopes={['edit-book-team']}>
                    <Button
                      data-test-id="save-book-team"
                      disabled={loading}
                      onClick={saveBookTeam}
                      status="primary"
                    >
                      {loading ? 'Saving...' : 'Save'}
                    </Button>
                  </PermissionsGate>
                }
                rightComponent={
                  <PermissionsGate scopes={['edit-book-team']}>
                    <Button
                      icon="trash-2"
                      iconPosition="end"
                      onClick={async () => {
                        modalRef.current.unassign(tableProps.selectedRows)
                      }}
                      outlined
                      size="small"
                      status="primary"
                    >
                      Remove from team
                    </Button>
                  </PermissionsGate>
                }
              />
            </PermissionsGate>
          </FormWrapper>
        )}
      </TableWithToolbar>

      {loading && <FixedLoader title="Saving book teams..." />}
    </Wrapper>
  )
}

export default BookTeamsTab
