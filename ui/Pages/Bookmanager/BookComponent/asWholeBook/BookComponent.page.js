/* eslint-disable react/prop-types */

import React, { useEffect } from 'react'
import { useQuery } from '@apollo/client'
import { useParams } from 'react-router-dom'

import WholeBookPage from '../../WholeBookPage'
import { Spinner } from '../../../../components'
import {
  GET_BOOK_USERS,
  CONVERT_SUBSCRIPTION,
  GET_BOOK_VERSIONS,
} from '../../../../../app/graphql/bookComponent'
import Granthub from '../../../../../app/pages/_helpers/Granthub'
import useGetCollections from '../../../../../app/pages/_helpers/useGetCollectionsList'
import { GET_ORGANIZATION } from '../../../../../app/graphql/createBook'

const PAGE = 'bookComponent'

const BookComponentManager = props => {
  const { bookId, organizationId } = useParams()

  const {
    onUpdateBook,
    loadingUpdateBook,
    onUpdateBookTeams,
    loadingUpdateBookTeams,
  } = props

  const { loading: loadingBook, data: bookData, subscribeToMore } = useQuery(
    GET_BOOK_USERS,
    {
      variables: {
        id: bookId,
        input: {
          skip: 0,
          sort: { direction: 'asc', field: ['username'] },
          take: 10,
        },
      },
    },
  )

  const { loading: loadingBookVersion, data: bookVersions } = useQuery(
    GET_BOOK_VERSIONS,
    {
      variables: {
        bookId,
      },
    },
  )

  const differentOrganizations =
    !!bookData?.getBook.organisation.id &&
    organizationId !== bookData?.getBook.organisation.id

  const { data: organizationData } = useQuery(GET_ORGANIZATION, {
    variables: { id: organizationId },
  })

  const { data: bookOrganization } = useQuery(GET_ORGANIZATION, {
    variables: { id: bookData?.getBook.organisation.id },
    skip: !differentOrganizations,
  })

  const organization = organizationData?.getOrganisation

  const collectionOptions = useGetCollections({
    organization,
    bookOrganization: bookOrganization?.getOrganisation,
    differentOrganizations,
  })

  useEffect(() => {
    subscribeToMore({
      document: CONVERT_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev
        return null
      },
      variables: { bookId },
    })
  })

  /* DATA WRANGLING */
  const book = bookData?.getBook

  const workflow = book?.workflow
  // const isWord = workflow === 'word'

  const createNewBookcomponentVersion = async () => {}

  const searchGranthubFn = searchValue => Granthub.search(searchValue, PAGE)

  if (loadingBook) return <Spinner pageLoader />

  return (
    <WholeBookPage
      book={book}
      bookVersions={!loadingBookVersion ? bookVersions?.getBookVersions : []}
      collectionOptions={collectionOptions}
      createNewBookcomponentVersion={createNewBookcomponentVersion}
      iconName="book"
      loadingBook={loadingBook}
      loadingNewVersion={false}
      loadingUpdateBook={loadingUpdateBook}
      loadingUpdateBookTeams={loadingUpdateBookTeams}
      onUpdateBook={onUpdateBook}
      onUpdateBookTeams={onUpdateBookTeams}
      searchGranthub={searchGranthubFn}
      workflow={workflow}
    />
  )
}

export default BookComponentManager
