/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */

import React, { useContext } from 'react'
import styled from 'styled-components'
import 'react-datepicker/dist/react-datepicker.css'

import { Tabs } from '../../../../components'
import { Files, Preview, Errors } from '..'
import IssuesTracker from '../IssueTracker/IssueTracker'
import PermissionsGate from '../../../../../app/components/PermissionsGate'
import CurrentUserContext from '../../../../../app/userContext'
import {
  isOnlyPreviewer,
  canPublish,
  hasOneModifierRole,
} from '../../../../../app/userRights'
import { OpenIssuesIcon } from '../../../../components/Icons'

import WholeBookSettingsTab from './SettingsTab'
import WholeBookMetadataTab from './MetadataTab'
import WholeBookTeamsTab from './TeamsTab'

import FeedBack from '../FeedBack'
import ErrorsHelp from '../ErrorsHelp'
import { getBookComponentReviewResponse } from '../../../../common/utils'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  > div:last-child {
    flex-grow: 1;
  }
`

const TabContainer = styled.div`
  height: 100%;
  width: 100%;
`

const WholeBookTabs = ({
  book,
  organizationId,
  tabsRightComponent,
  collectionOptions,
  isLatestVersion,
}) => {
  const { currentUser } = useContext(CurrentUserContext)

  const isOnlyPreviewerRole = isOnlyPreviewer({
    currentUser,
    organizationId,
    bookId: book.id,
  })

  const hasOneModifierRoles = hasOneModifierRole({
    currentUser,
    organizationId,
    bookId: book.id,
  })

  const canPublishBc =
    canPublish({
      currentUser,
      organizationId,
      bookId: book.id,
    }) && book.settings.approvalOrgAdminEditor

  const componentReviewResponse = getBookComponentReviewResponse({
    component: book,
    bookTeams: book.teams,
  })

  const enableUpload = !PermissionsGate({
    scopes: ['enable-upload'],
  })

  const inReviewConverting = () => {
    if (book.status === 'in-review') {
      return (
        [('converting', 'loading-preview')].includes(
          book.sourceFiles[0].status || null,
        ) ||
        [('converting', 'loading-preview')].includes(
          book.convertedFile[0].status || null,
        )
      )
    }

    return false
  }

  const disabledOptions = {
    requestReview: !['preview', 'approve'].includes(book.status),
    revise:
      !['in-review', 'preview'].includes(book.status) || inReviewConverting(),
    approve:
      !['in-review', 'preview'].includes(book.status) || inReviewConverting(),
  }

  const FeedBackUI = (
    <PermissionsGate scopes={['view-review-comment-panel']}>
      <FeedBack
        book={book}
        bookComponent={book}
        data-test-id="book-component-feedback"
        disableOptions={disabledOptions}
        key="3"
        primary
      />
    </PermissionsGate>
  )

  const previewTab = {
    label: 'Preview',
    key: '#preview',
    content: (
      <TabContainer>
        <Preview
          bookBodyDivision={{}}
          bookId={book.id}
          canPublish={canPublishBc}
          collectionSourceType={book?.collection?.metadata.wholeBookSourceType}
          componentDivisionId={null}
          componentErrors={book.errors}
          componentId={book.id}
          componentReviewResponse={componentReviewResponse}
          componentStatus={book.status}
          componentTitle={book.title}
          convertedFile={book.convertedFile}
          enableUpload={enableUpload}
          FeedBack={FeedBackUI}
          groupChaptersInParts={false}
          isLatestVersion={isLatestVersion}
          isWholeBook
          listOfPartsOfBook={[]}
          parentPartsOfComponent={[]}
          sourceFiles={book.sourceFiles}
          sourceType={book.metadata.sourceType}
          url={book.metadata.url}
          versionName={null}
          workflow={book.workflow}
        />
      </TabContainer>
    ),
  }

  const filesTab = {
    label: 'Files',
    key: '#files',
    content: (
      <TabContainer>
        <PermissionsGate scopes={['view-book-file-tab']} showPermissionInfo>
          <Files
            canUpload={
              book.workflow !== 'pdf'
                ? hasOneModifierRoles
                : hasOneModifierRoles || currentUser.auth.isPDF2XMLVendor
            }
            values={{
              workflow: book.workflow,
              chapterIndependently: book.settings.chapterIndependently,
              bookStatus: book.status,
              sourceType: book.metadata.sourceType,
              collectionSourceType:
                book?.collection?.metadata.wholeBookSourceType,
              isLatestVersion,
              enableUpload,
              ...book,
              bookComponentVersionId:
                book?.bookComponents[0]?.bookComponentVersionId,
            }}
          />
        </PermissionsGate>
      </TabContainer>
    ),
  }

  const canViewFilesTab = PermissionsGate({ scopes: ['view-book-file-tab'] })

  const ErrorsHelpUi = (
    // <PermissionsGate scopes={['view-book-errors-comment']}>
    //   test
    <ErrorsHelp
      book={book}
      bookComponent={book}
      data-test-id="book-component-feedback"
      key="error-help"
      primary
    />
    // </PermissionsGate>
  )

  const errorsTab = {
    label: 'Errors',
    key: '#errors',
    content: (
      <TabContainer>
        <PermissionsGate
          scopes={['view-book-component-errors-tab']}
          showPermissionInfo
        >
          <Errors ErrorsHelp={ErrorsHelpUi} id={book.id} />
        </PermissionsGate>
      </TabContainer>
    ),
  }

  const canViewErrorsTab = PermissionsGate({
    scopes: ['view-book-component-errors-tab'],
  })

  const openIssues = book.issues.filter(issues => issues.status === 'open')
    .length

  const totalIssues = book.issues.length

  const closedIssues = book.issues.filter(issues => issues.status === 'closed')
    .length

  const vendorIssuesTab = {
    label: (
      <>
        {openIssues > 0 && (
          <OpenIssuesIcon denominator={totalIssues} numerator={openIssues} />
        )}
        Vendor Issues
      </>
    ),
    key: '#vendorissues',
    content: (
      <TabContainer>
        <PermissionsGate scopes={['view-vendor-tab']} showPermissionInfo>
          <IssuesTracker
            bookComponentId={book.id}
            closedIssues={closedIssues}
            openIssues={openIssues}
            organizationId={organizationId}
          />
        </PermissionsGate>
      </TabContainer>
    ),
  }

  const canViewBookSettingsTab = PermissionsGate({
    scopes: ['view-book-settings'],
  })

  const bookSettingsTab = {
    label: 'Settings',
    key: '#settings',
    content: (
      <TabContainer>
        <WholeBookSettingsTab
          book={book}
          collectionOptions={collectionOptions}
          readonly={!isLatestVersion}
        />
      </TabContainer>
    ),
  }

  const bookMetadataTab = {
    label: 'Metadata',
    key: '#metadata',
    content: (
      <TabContainer>
        <WholeBookMetadataTab book={book} />
      </TabContainer>
    ),
  }

  const bookTeamsTab = {
    label: 'Team',
    key: '#teams',
    content: (
      <TabContainer>
        <WholeBookTeamsTab book={book} />
      </TabContainer>
    ),
  }

  const canViewVendorTab = PermissionsGate({ scopes: ['view-vendor-tab'] })

  const canViewMetadataTab = PermissionsGate({
    scopes: ['view-book-metadata'],
  })

  const canViewTeamTab = PermissionsGate({
    scopes: ['view-book-team-tab'],
  })

  let sections = null

  switch (book.workflow) {
    case 'word':
      sections = [
        previewTab,
        canViewFilesTab && filesTab,
        canViewErrorsTab && errorsTab,
      ]

      break
    case 'xml':
      sections = [
        previewTab,
        canViewTeamTab && bookTeamsTab,
        canViewFilesTab && filesTab,
        canViewMetadataTab && bookMetadataTab,
        canViewErrorsTab && errorsTab,
        canViewBookSettingsTab && bookSettingsTab,
      ]

      break
    case 'pdf':
      sections = [
        previewTab,
        canViewTeamTab && bookTeamsTab,
        canViewFilesTab && filesTab,
        canViewMetadataTab && bookMetadataTab,
        canViewErrorsTab && errorsTab,
        canViewVendorTab && vendorIssuesTab,
        canViewBookSettingsTab && bookSettingsTab,
      ]

      break
    default:
      sections = [previewTab]
  }

  const canViewTabs =
    isOnlyPreviewerRole ||
    hasOneModifierRoles ||
    currentUser?.auth.isPDF2XMLVendor

  /* Handle saving last tab in localstorage */
  const savedTab = localStorage.getItem(`bookComponentTabKey`)

  const activeKey =
    savedTab ||
    (isOnlyPreviewerRole && previewTab.key) ||
    (sections.length && sections[0].key)

  const handleChangeTab = key =>
    localStorage.setItem(`bookComponentTabKey`, key)

  return (
    <Wrapper>
      {canViewTabs ? (
        <Tabs
          activeKey={activeKey}
          book={book}
          onChangeTab={handleChangeTab}
          rightComponent={tabsRightComponent}
          tabItems={isOnlyPreviewerRole ? [previewTab] : sections}
        />
      ) : (
        <center>
          <br />
          You are not assigned any role yet
        </center>
      )}
    </Wrapper>
  )
}

export default WholeBookTabs
