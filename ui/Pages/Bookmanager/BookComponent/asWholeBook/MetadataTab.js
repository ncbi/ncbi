/* eslint-disable react/prop-types */
import React, { useRef } from 'react'
import { useMutation } from '@apollo/client'
import cloneDeep from 'lodash/cloneDeep'
import omit from 'lodash/omit'
import styled from 'styled-components'
import {
  Button,
  Notification,
  FixedLoader,
  ActionBar,
} from '../../../../components'
import MetadataForm from '../../../../components/MetadataForm'
import {
  bookAPIToBookMetadata,
  bookMetadataToBookAPI,
} from '../../../../../app/pages/_helpers/bookSettingsToApiConverter'
import Granthub from '../../../../../app/pages/_helpers/Granthub'
import PermissionsGate from '../../../../../app/components/PermissionsGate'
import query from '../../graphql'

const PAGE = 'bookManager'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;

  > div:first-child {
    overflow-y: auto;
  }
`

const BookMetadataTab = ({ book }) => {
  const formRef = useRef(null)
  const [updateBookMetadata, { loading }] = useMutation(query.UPDATE_BOOK, {})

  const submissionType = book.settings.chapterIndependently
    ? 'chapterProcessed'
    : 'wholeBook'

  const conversionWorkflow = book.workflow
  const applyCoverFromParent = book.collection?.metadata.applyCoverToBooks

  const applyPermissionsFromParent =
    book.collection?.metadata.applyPermissionsToBooks && book.workflow === 'pdf'

  const applyPublisherFromParent =
    book.collection?.metadata.applyPublisherToBooks &&
    submissionType === 'chapterProcessed'

  const inCollection = !!book.collection

  const formValues = bookAPIToBookMetadata(book)

  const handleSave = () => {
    formRef.current.handleSubmit()
  }

  const handleSubmit = async values => {
    let submittedValues = cloneDeep(values)

    if (values.cover?.id && values.cover.id === book.fileCover?.id) {
      submittedValues = omit(submittedValues, 'cover')
    }

    if (
      values.abstractGraphic?.id &&
      values.abstractGraphic.id === book.fileAbstract?.id
    ) {
      submittedValues = omit(submittedValues, 'abstractGraphic')
    }

    const transformedValues = await bookMetadataToBookAPI(submittedValues, PAGE)

    updateBookMetadata({
      refetchQueries: [
        {
          query: query.GET_BOOK_USERS,
          variables: {
            id: book.id,
          },
        },
      ],
      variables: { id: book.id, input: transformedValues },
    })
      .then(() => {
        Notification({
          type: 'success',
          message: 'Metadata were updated successfully',
        })
      })
      .catch(res => {
        console.error(res)

        Notification({
          type: 'danger',
          message: 'Metadata could not be saved',
        })
      })
  }

  const searchGranthubFn = searchValue => Granthub.search(searchValue, PAGE)

  return (
    <Wrapper>
      <div>
        <PermissionsGate
          errorProps={{ readonly: true }}
          scopes={['edit-book-metadata']}
        >
          <MetadataForm
            applyCoverFromParent={applyCoverFromParent}
            applyPermissionsFromParent={applyPermissionsFromParent}
            applyPublisherFromParent={applyPublisherFromParent}
            conversionWorkflow={conversionWorkflow}
            inCollection={inCollection}
            initialFormValues={formValues}
            innerRef={formRef}
            isSaving={loading}
            onSubmit={handleSubmit}
            searchGranthub={searchGranthubFn}
            submissionType={submissionType}
            variant="book"
          />
        </PermissionsGate>
      </div>

      <ActionBar
        leftComponent={
          <PermissionsGate scopes={['edit-book-metadata']}>
            <Button
              data-test-id="save-book-metadata"
              disabled={loading}
              loading={loading}
              onClick={handleSave}
              status="primary"
            >
              {loading ? 'Saving...' : 'Save'}
            </Button>
          </PermissionsGate>
        }
      />
      {loading && <FixedLoader title="Saving metadata..." />}
    </Wrapper>
  )
}

export default BookMetadataTab
