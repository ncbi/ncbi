/* eslint-disable react/prop-types */
import React, { useContext, useRef } from 'react'
import { useMutation } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'
import styled from 'styled-components'

import {
  Button,
  FixedLoader,
  Notification,
  ActionBar,
} from '../../../../components'

import BookSettingsForm from '../../../../components/BookSettings'
import {
  bookAPIToBookSettings,
  bookSettingsToBookAPI,
} from '../../../../../app/pages/_helpers'
import CurrentUserContext from '../../../../../app/userContext'
import query from '../../graphql'
import PermissionsGate from '../../../../../app/components/PermissionsGate'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;

  > div:first-child {
    overflow-y: auto;
  }
`

const SettingsTab = ({ book, collectionOptions, readonly = false }) => {
  const formRef = useRef(null)
  const { currentUser } = useContext(CurrentUserContext)

  const organizationId = book.organisation.id

  const {
    orgAdminForOrganisation,
    editorForOrganisation,
    isSysAdmin,
  } = currentUser.auth

  const isOrgAdmin = orgAdminForOrganisation.includes(organizationId)
  const isEditor = editorForOrganisation.includes(organizationId)

  const { workflow } = book

  const bookType = book.settings.chapterIndependently
    ? 'chapterProcessed'
    : 'wholeBook'

  const initialFormValues = bookAPIToBookSettings(book)

  const publisherOptions = book.organisation.publisherOptions.map(
    publisher => ({
      value: publisher.id,
      label: publisher.name,
    }),
  )

  const inCollection = !!book.collection
  const inFundedCollection = book.collection?.collectionType === 'funded'

  const [updateBook, { loading }] = useMutation(query.UPDATE_BOOK)

  const handleClickSave = () => {
    formRef.current.handleSubmit()
  }

  const handleSubmit = values => {
    const toSend = bookSettingsToBookAPI(values)

    delete toSend.workflow

    updateBook({
      refetchQueries: [getOperationName(query.GET_BOOK_USERS)],
      variables: { id: book.id, input: toSend },
    }).catch(res => {
      console.error(res)

      Notification({
        message: `Could not update - ${res.message}`,
        type: 'danger',
        title: 'Error',
      })
    })
  }

  return (
    <Wrapper>
      <div>
        <PermissionsGate
          errorProps={{ readonly: true }}
          scopes={['edit-book-settings']}
        >
          <BookSettingsForm
            collectionOptions={collectionOptions}
            inCollection={inCollection}
            inFundedCollection={inFundedCollection}
            initialFormValues={initialFormValues}
            innerRef={formRef}
            isEditor={isEditor}
            isOrgAdmin={isOrgAdmin}
            isSysAdmin={isSysAdmin}
            onSubmit={handleSubmit}
            publisherOptions={publisherOptions}
            readonly={readonly}
            type={bookType}
            variant="bookSettings"
            workflow={workflow}
          />
        </PermissionsGate>
      </div>

      <ActionBar
        leftComponent={
          <PermissionsGate scopes={['edit-book-settings']}>
            <Button
              data-test-id="save-book-settings"
              disabled={loading || readonly}
              loading={loading}
              onClick={handleClickSave}
              status="primary"
            >
              {loading ? 'Saving...' : 'Save'}
            </Button>
          </PermissionsGate>
        }
      />
      {loading && <FixedLoader title="Saving book settings..." />}
    </Wrapper>
  )
}

export default SettingsTab
