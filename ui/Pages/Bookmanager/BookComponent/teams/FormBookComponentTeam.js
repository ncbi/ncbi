/* eslint-disable react/prop-types */
import React, { useImperativeHandle, useState } from 'react'
import { withFormik, Form, Field } from 'formik'
import { th } from '@pubsweet/ui-toolkit'
import { isEmpty, cloneDeep } from 'lodash'
import styled from 'styled-components'
import UsersTable from './UsersTable'
import { Accordion, SelectComponent, Button } from '../../../../components'
import { mapOrder } from '../../../../common/utils'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${th('gridUnit')};
`

const HorizontalWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: ${th('gridUnit')};
`

const UserSelect = styled.div`
  flex-basis: 60%;
`

const teamdata = members =>
  members.map(({ user }) => ({
    ...user,
  }))

const cutOrganizationBook = (
  organizationTeam,
  organizationUserTeam,
  bookTeam,
  bookComponentTeam,
) => {
  const orgNotbookTeam =
    organizationTeam?.members.filter(el => {
      return bookTeam.members.findIndex(x => x.user.id === el.user.id) === -1
    }) || []

  const final = orgNotbookTeam.filter(t => {
    return (
      bookComponentTeam.members.findIndex(x => x.user.id === t.user.id) === -1
    )
  })

  const isUserEnabled = x => {
    const memberInUserTeam = organizationUserTeam.members.find(
      member => member.user.id === x.user.id,
    )

    return memberInUserTeam.status === 'enabled'
  }

  if (final) {
    return final
      .filter(x => isUserEnabled(x))
      .map(tm => ({
        value: tm.user.id,
        label: `${tm.user.givenName} ${tm.user.surname}`,
      }))
  }

  return []
}

const findSpecifiedTeam = ({
  role,
  organizationTeam,
  organizationUserTeam,
  bookComponentTeam,
  bookTeam,
  allBookCompTeam,
}) => {
  //  list of users of one team(from organization) except the ones assigned in the book
  const cutOrganizationBookTeams = cutOrganizationBook(
    organizationTeam,
    organizationUserTeam,
    bookTeam,
    bookComponentTeam,
  )

  //  current editors of the book component
  const editors =
    allBookCompTeam
      .find(x => x.role === 'editor')
      ?.members?.map(x => x.user.id) || []

  //  current previewers of the book component
  const previewers =
    allBookCompTeam
      .find(x => x.role === 'previewer')
      ?.members?.map(x => x.user.id) || []

  //  current authors of the book component
  const authors =
    allBookCompTeam
      .find(x => x.role === 'author')
      ?.members?.map(x => x.user.id) || []

  switch (role) {
    case 'editor':
      //  exlude users that might have more than one role
      return cutOrganizationBookTeams.filter(
        x => !previewers.includes(x.value) && !authors.includes(x.value),
      )

    case 'author':
      //  exlude users that might have more than one role and are editors
      return cutOrganizationBookTeams.filter(
        x => !previewers.includes(x.value) && !editors.includes(x.value),
      )

    case 'previewer':
      //  exlude users that might have more than one role and are editors
      return cutOrganizationBookTeams.filter(
        x => !editors.includes(x.value) && !authors.includes(x.value),
      )

    default:
      return cutOrganizationBookTeams
  }
}

const findSpecifiedTeamUsers = (teams, teamname) => {
  const foundTeam = teams.find(team => team.role === teamname)

  if (foundTeam) {
    return foundTeam.members.map(tm => ({
      ...tm.user,
    }))
  }

  return []
}

const Basic = ({
  setValidationAction,
  submitForm,
  handleSubmit,
  setFieldValue,
  errors,
  editForm,
  editMode,
  form,
  values,
  validateForm,
  book,
  readonly,
  ...props
}) => {
  const { formikFormRef } = props
  const [selectedRows, setSelectedRows] = useState([])

  useImperativeHandle(formikFormRef, () => ({
    triggerSave: async () => {
      submitForm()
      const isValid = await validateForm()

      if (isEmpty(isValid)) {
        return values
      }

      return false
    },
    unassign: async selected => {
      const updatedValues = []

      values.teams.forEach(tm => {
        updatedValues.push({
          ...tm,
          members: tm.members.filter(x => !selectedRows.includes(x.user.id)),
        })
      })

      setFieldValue(`teams`, updatedValues)
    },
  }))

  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <Wrapper>
          {values.teams?.map((team, index) => {
            return (
              <div key={team.id}>
                <Accordion
                  key={`accordion${team.id}`}
                  label={`${team.name}s`}
                  startExpanded
                >
                  <HorizontalWrapper>
                    <UserSelect>
                      <Field
                        cacheOptions
                        className="select-user-role"
                        classNamePrefix="select"
                        component={SelectComponent}
                        defaultOptions
                        disabled={readonly}
                        isClearable={false}
                        menuPortalTarget={document.body}
                        multiple
                        name={`added[${team.role}]`}
                        options={findSpecifiedTeam({
                          role: team.role,
                          organizationTeam: values.organizationTeams.find(
                            x => x.role === team.role,
                          ),
                          organizationUserTeam: values.organizationTeams.find(
                            t => t.role === 'user',
                          ),
                          bookComponentTeam: team,
                          bookTeam: values.bookTeams.find(
                            x =>
                              x.role.toLowerCase() === team.role.toLowerCase(),
                          ) || { members: [] },
                          allBookCompTeam: values.teams,
                        })}
                        setFieldValue={setFieldValue}
                      />
                    </UserSelect>
                    <div>
                      <Button
                        disabled={readonly}
                        onClick={() => {
                          const foundUsers = findSpecifiedTeamUsers(
                            values.organizationTeams,
                            team.role,
                          ).filter(x => values.added[team.role].includes(x.id))

                          if (values.added[team.role]) {
                            const teamMembers = cloneDeep(values.teams)

                            foundUsers.forEach(f => {
                              teamMembers[index].members.push({
                                status: 'enabled',
                                user: f,
                              })
                            })

                            setFieldValue(`teams`, teamMembers)

                            setFieldValue(`added`, {
                              editor: [],
                              previewer: [],
                              author: [],
                            })
                          }
                        }}
                        status="primary"
                      >
                        Add Member
                      </Button>
                    </div>
                  </HorizontalWrapper>

                  <UsersTable
                    data={teamdata(team.members)}
                    key={`users_${team.role}`}
                    owners={values.owners}
                    readonly={readonly}
                    selectedRows={selectedRows}
                    setFieldValue={setFieldValue}
                    setSelectedRows={setSelectedRows}
                    team={team}
                    values={values}
                  />
                  <br />
                </Accordion>
                <br />
              </div>
            )
          })}
        </Wrapper>
      </Form>
    </div>
  )
}

const BookTeams = withFormik({
  handleSubmit: (values, { setSubmitting }) => {
    setSubmitting(false)
  },
  mapPropsToValues: ({ values }) => {
    const itemorder = ['editor', 'previewer', 'author']

    const tempTeam = cloneDeep(
      values.teams.filter(x => !['previewer', 'author'].includes(x.role)),
    ) // todo: remove filter to show 2 roles

    const orderedArray = mapOrder(tempTeam, itemorder, 'role')

    return {
      teams: orderedArray,
      organizationTeams: values.organization?.teams,
      bookTeams: values.book?.teams,
      owners: [values.owner?.id],
      added: {
        editor: [],
        previewer: [],
        author: [],
      },
    }
  },

  validate: values => {
    const errors = {}

    return errors
  },
})(Basic)

export default BookTeams
