/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { useEffect } from 'react'
import styled from 'styled-components'

import { TableLocalData } from '../../../../components/Datatable'
import { TableWithToolbar, Button } from '../../../../components'

const Root = styled.div`
  display: flex;
`

const LastColumn = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
`

const UsersTable = ({
  data,
  setSelectedRows,
  setFieldValue,
  values,
  team,
  key,
  owners,
  readonly,
  props,
}) => {
  const getName = ({ givenName, surname, username }) => {
    if (!givenName && !surname) return username
    return `${givenName} ${surname}`
  }

  const columns = [
    {
      cellRenderer: () => ({ rowData }) => <>{getName(rowData)}</>,
      dataKey: 'givenName',
      disableSort: true,
    },

    {
      cellRenderer: () => ({ rowData, ...params }) => (
        <LastColumn>
          <Button
            disabled={readonly || owners.findIndex(x => x === rowData.id) > -1}
            icon="trash-2"
            onClick={() => {
              const updatedValues = []

              values.teams.forEach(tm => {
                if (tm.id !== team.id) {
                  updatedValues.push(tm)
                } else {
                  updatedValues.push({
                    ...team,
                    members: team.members.filter(x => x.user.id !== rowData.id),
                  })
                }
              })

              setFieldValue(`teams`, updatedValues)
            }}
            outlined
            status="danger"
          />
        </LastColumn>
      ),
      dataKey: 'id',
      disableSort: true,
      label: '',
      width: 200,
    },
  ]

  return (
    <Root>
      <TableWithToolbar selected={[]}>
        {({
          checkboxColumn,
          selectedRows: inerSelectedRows,
          setSelectedRows: inerSetSelctedRows,
        }) => {
          useEffect(() => {
            setSelectedRows(inerSelectedRows)
          }, [inerSelectedRows])

          return (
            <TableLocalData
              columns={[checkboxColumn].concat(columns)}
              dataTable={data || []}
              disableHeader
              key={key}
              props={props}
              rowHeight={40}
              selectedRows={inerSelectedRows}
              setSelectedRows={inerSetSelctedRows}
            />
          )
        }}
      </TableWithToolbar>
    </Root>
  )
}

export default UsersTable
