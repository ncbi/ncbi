/* eslint-disable import/no-cycle */
/* eslint-disable react/prop-types */
import React, { useContext, useRef } from 'react'
import styled from 'styled-components'
import 'react-datepicker/dist/react-datepicker.css'

import { Tabs, ActionBar, Button } from '../../../components'
import { Files, Preview, ManageTeams, Errors, Metadata } from '.'
import IssuesTracker from './IssueTracker/IssueTracker'
import CurrentUserContext from '../../../../app/userContext'
import { isOnlyPreviewer, hasOneModifierRole } from '../../../../app/userRights'
import { OpenIssuesIcon } from '../../../components/Icons'
import PermissionsGate from '../../../../app/components/PermissionsGate'
import FeedBack from './FeedBack'
import ErrorsHelp from './ErrorsHelp'
import { getBookComponentReviewResponse } from '../../../common/utils'
import { selectedParentParts } from '../bookManagerFunctions'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  > div:last-child {
    flex-grow: 1;
  }
`

const TabContainer = styled.div`
  height: 100%;
  width: 100%;
`

const MetadataWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;

  > div:first-child {
    overflow-y: auto;
  }
`

const BookComponentTabs = props => {
  const {
    book,
    bookComponent,
    bookComponentMetadata,
    divisionsMap,
    handleSaveMetadata,
    isBookOrderedManually,
    isComponentInDivisionTopLevel,
    isSaving,
    onMoveSearch,
    onReorder,
    onRepeat,
    organizationId,
    partsTree,
    searchGranthub,
    tabsRightComponent,
  } = props

  if (!bookComponent) return null

  const metadataFormRef = useRef(null)

  const triggerMetadataSubmit = () => {
    metadataFormRef.current.handleSubmit()
  }

  const isOldVersion =
    bookComponent.teams.length === 0 &&
    bookComponent.publishedVersions.findIndex(
      x => x.bookComponentVersionId === bookComponent.bookComponentVersionId,
    ) > -1

  const { currentUser } = useContext(CurrentUserContext)

  const isOnlyPreviewerRole = isOnlyPreviewer({
    currentUser,
    organizationId,
    bookId: book.id,
    bookComponentId: bookComponent.id,
  })

  const hasOneModifierRoles = hasOneModifierRole({
    currentUser,
    organizationId,
    bookId: book.id,
    bookComponentId: bookComponent.id,
  })

  const isMultiChapterBook = book?.settings.chapterIndependently

  const componentReviewResponse = getBookComponentReviewResponse({
    component: bookComponent,
    bookTeams: book.teams,
  })

  const parentParts = selectedParentParts({
    book,
    bookComponentId: bookComponent.id,
  })

  const inReviewConverting = () => {
    if (bookComponent.status === 'in-review') {
      return (
        [('converting', 'loading-preview')].includes(
          bookComponent.sourceFiles[0].status || null,
        ) ||
        [('converting', 'loading-preview')].includes(
          bookComponent.convertedFile[0].status || null,
        )
      )
    }

    return false
  }

  const disabledOptions = {
    requestReview: !['preview', 'approve'].includes(bookComponent.status),
    revise:
      !['in-review', 'preview'].includes(bookComponent.status) ||
      inReviewConverting(),
    approve:
      !['in-review', 'preview'].includes(bookComponent.status) ||
      inReviewConverting(),
  }

  const FeedBackUI = (
    <PermissionsGate scopes={['view-review-comment-panel']}>
      <FeedBack
        book={book}
        bookComponent={bookComponent}
        data-test-id="book-component-feedback"
        disableOptions={disabledOptions}
        key="3"
        primary
      />
    </PermissionsGate>
  )

  const previewTab = {
    label: 'Preview',
    key: '#preview',
    content: (
      <TabContainer>
        <Preview
          bookBodyDivision={book.divisions.find(div => div.label === 'body')}
          canPublish={book.settings.approvalOrgAdminEditor}
          collectionSourceType={
            book?.collection?.metadata?.chapterProcessedSourceType
          }
          componentDivisionId={bookComponent.divisionId}
          componentErrors={bookComponent.errors}
          componentId={bookComponent.id}
          componentReviewResponse={componentReviewResponse}
          componentStatus={bookComponent.status}
          componentTitle={bookComponent.title}
          convertedFile={bookComponent.convertedFile}
          divisionsMap={divisionsMap}
          FeedBack={FeedBackUI}
          groupChaptersInParts={book.settings.toc.group_chapters_in_parts}
          isBookOrderedManually={isBookOrderedManually}
          isComponentInDivisionTopLevel={isComponentInDivisionTopLevel}
          onMoveSearch={onMoveSearch}
          onReorder={onReorder}
          onRepeat={onRepeat}
          parentPartsOfComponent={parentParts}
          partsTree={partsTree}
          sourceFiles={bookComponent.sourceFiles}
          sourceType={book.metadata.sourceType}
          url={bookComponent.metadata.url}
          versionName={bookComponent.versionName}
          workflow={book.workflow}
        />
      </TabContainer>
    ),
  }

  const filesTab = {
    label: 'Files',
    key: '#files',
    content: (
      <TabContainer>
        <PermissionsGate
          scopes={[
            isMultiChapterBook
              ? 'view-book-component-file-tab'
              : 'view-book-file-tab',
          ]}
          showPermissionInfo
        >
          <Files
            canUpload={
              book.workflow !== 'pdf'
                ? hasOneModifierRoles
                : hasOneModifierRoles || currentUser.auth.isPDF2XMLVendor
            }
            values={{
              workflow: book.workflow,
              chapterIndependently: book.settings.chapterIndependently,
              bookStatus: book.status,
              sourceType: book.metadata.sourceType,
              collectionSourceType:
                book?.collection?.metadata?.chapterProcessedSourceType,
              ...bookComponent,
            }}
          />
        </PermissionsGate>
      </TabContainer>
    ),
  }

  const canViewFilesTab = PermissionsGate({
    scopes: [
      isMultiChapterBook
        ? 'view-book-component-file-tab'
        : 'view-book-file-tab',
    ],
  })

  const ErrorsHelpUi = (
    <PermissionsGate scopes={['view-book-component-errors-comment']}>
      <ErrorsHelp
        book={book}
        bookComponent={bookComponent}
        data-test-id="book-component-feedback"
        key="error-help"
        primary
      />
    </PermissionsGate>
  )

  const errorsTab = {
    label: 'Errors',
    key: '#errors',
    content: (
      <TabContainer>
        <PermissionsGate
          scopes={['view-book-component-errors-tab']}
          showPermissionInfo
        >
          <Errors ErrorsHelp={ErrorsHelpUi} id={bookComponent.id} />
        </PermissionsGate>
      </TabContainer>
    ),
  }

  const canViewErrorsTab = PermissionsGate({
    scopes: ['view-book-component-errors-tab'],
  })

  const openIssues = bookComponent.issues.filter(
    issues => issues.status === 'open',
  ).length

  const totalIssues = bookComponent.issues.length

  const closedIssues = bookComponent.issues.filter(
    issues => issues.status === 'closed',
  ).length

  const vendorIssuesTab = {
    label: (
      <>
        {openIssues > 0 && (
          <OpenIssuesIcon denominator={totalIssues} numerator={openIssues} />
        )}
        Vendor Issues
      </>
    ),
    key: '#vendorissues',
    content: (
      <TabContainer>
        <PermissionsGate scopes={['view-vendor-tab']} showPermissionInfo>
          <IssuesTracker
            bookComponentId={bookComponent?.id}
            closedIssues={closedIssues}
            openIssues={openIssues}
            organizationId={organizationId}
          />
        </PermissionsGate>
      </TabContainer>
    ),
  }

  const canViewVendorTab = PermissionsGate({ scopes: ['view-vendor-tab'] })

  const canViewTeamTab = PermissionsGate({
    scopes: ['view-book-component-team-tab'],
  })

  const manageTeamTab = {
    label: 'Team',
    key: '#teams',
    content: (
      <PermissionsGate
        scopes={['view-book-component-team-tab']}
        showPermissionInfo
      >
        <ManageTeams
          bookTeams={book.teams}
          componentOwners={bookComponent.owners}
          componentTeams={bookComponent.teams}
          organizationId={organizationId}
        />
      </PermissionsGate>
    ),
  }

  const canViewMetadataTab = PermissionsGate({
    scopes: [
      isMultiChapterBook
        ? 'view-book-component-metadata-tab'
        : 'view-book-metadata',
    ],
  })

  const metadataTab = {
    label: 'Metadata',
    key: '#metadata',
    content: (
      <TabContainer>
        <PermissionsGate
          scopes={[
            isMultiChapterBook
              ? 'view-book-component-metadata-tab'
              : 'view-book-metadata',
          ]}
          showPermissionInfo
        >
          <MetadataWrapper>
            <div>
              <PermissionsGate
                errorProps={{ readonly: true }}
                scopes={[
                  isMultiChapterBook
                    ? 'edit-book-component-metadata'
                    : 'edit-book-metadata',
                ]}
              >
                <Metadata
                  initialFormValues={bookComponentMetadata}
                  innerRef={metadataFormRef}
                  isSaving={isSaving}
                  onSubmit={handleSaveMetadata}
                  searchGranthub={searchGranthub}
                />
              </PermissionsGate>
            </div>

            <ActionBar
              leftComponent={
                <PermissionsGate scopes={['edit-book-component-metadata']}>
                  <Button
                    disabled={isSaving}
                    loading={isSaving}
                    onClick={triggerMetadataSubmit}
                    status="primary"
                  >
                    Save
                  </Button>
                </PermissionsGate>
              }
            />
          </MetadataWrapper>
        </PermissionsGate>
      </TabContainer>
    ),
  }

  let sections = null

  switch (book.workflow) {
    case 'word':
      if (isOldVersion) {
        sections = isMultiChapterBook
          ? [
              previewTab,
              canViewFilesTab && filesTab,
              canViewMetadataTab && metadataTab,
              canViewErrorsTab && errorsTab,
            ]
          : [
              previewTab,
              canViewFilesTab && filesTab,
              canViewErrorsTab && errorsTab,
            ]
      } else {
        sections = isMultiChapterBook
          ? [
              previewTab,
              canViewTeamTab && manageTeamTab,
              canViewFilesTab && filesTab,
              canViewMetadataTab && metadataTab,
              canViewErrorsTab && errorsTab,
            ]
          : [
              previewTab,
              canViewFilesTab && filesTab,
              canViewErrorsTab && errorsTab,
            ]
      }

      break
    case 'xml':
      sections = isMultiChapterBook
        ? [
            previewTab,
            manageTeamTab,
            canViewFilesTab && filesTab,
            canViewMetadataTab && metadataTab,
            canViewErrorsTab && errorsTab,
          ]
        : []

      break
    case 'pdf':
      sections = isMultiChapterBook
        ? [
            previewTab,
            canViewTeamTab && manageTeamTab,
            canViewFilesTab && filesTab,
            canViewMetadataTab && metadataTab,
            canViewErrorsTab && errorsTab,
            canViewVendorTab && vendorIssuesTab,
          ]
        : []

      break
    default:
      sections = [previewTab]
  }

  /* Handle saving last tab in localstorage */
  const savedTab = localStorage.getItem(`bookComponentTabKey`)

  const activeKey =
    sections.findIndex(x => x?.key === savedTab) > -1 ? savedTab : '#preview'

  const handleChangeTab = key =>
    localStorage.setItem(`bookComponentTabKey`, key)

  return (
    <Wrapper>
      <PermissionsGate scopes={['view-book-component']}>
        <Tabs
          activeKey={activeKey}
          book={book}
          onChangeTab={handleChangeTab}
          outlined
          rightComponent={tabsRightComponent}
          tabItems={isOnlyPreviewerRole ? [previewTab] : sections}
        />
      </PermissionsGate>
    </Wrapper>
  )
}

export default BookComponentTabs
