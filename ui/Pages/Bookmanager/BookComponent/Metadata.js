import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import merge from 'lodash/merge'
import { Formik, Form } from 'formik'
import * as yup from 'yup'
import { v4 as uuid } from 'uuid'
import { grid } from '@pubsweet/ui-toolkit'
import PermissionsGate from '../../../../app/components/PermissionsGate'

import {
  TextFieldComponent as UITextField,
  DateInputComponent as DateInput,
} from '../../../components/FormElements/FormikElements'
import Button from '../../../components/common/Button'
import FormSection from '../../../components/common/FormSection'
import ConstantsContext from '../../../../app/constantsContext'
import Granthub from '../../../components/Granthub/Granthub'
import UINote from '../../../components/common/Note'

// #region styled
const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 800px;
  padding-bottom: 50px;
  padding-top: 20px;

  > form > div:not(:last-child) {
    margin-bottom: ${grid(2)};
  }
`

const Note = styled(UINote)`
  width: 100%;
`

const TextField = styled(UITextField)`
  margin-bottom: 0;
`

const Collaborator = styled.span`
  font-style: italic;
`

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: ${grid(2)};
`
// #endregion styled

// #region form-setup
const defaultInitialValues = {
  altTitle: '',
  authors: [],
  objectId: '',
  chapterId: '',
  collaborativeAuthors: [],
  dateCreated: { day: '', month: '', year: '' },
  dateOfPublication: { day: '', month: '', year: '' },
  dateRevised: { day: '', month: '', year: '' },
  dateUpdated: { day: '', month: '', year: '' },
  editors: [],
  funding: [],
  label: '',
  language: '',
  subTitle: '',
  title: '',
}

const validations = yup.object().shape({
  funding: yup
    .array()
    .of(yup.object())
    .test(
      'grants-empty-row',
      'All grant rows must have an award ID or be deleted',
      val => !val.find(row => !row.number),
    )
    .test(
      'grants-duplicate',
      'Each grant row must have a unique award ID',
      val => {
        const numbers = val.map(row => row.number)
        const deduped = new Set(numbers)
        return !(deduped.size < numbers.length)
      },
    ),
})
// #endregion form-setup

const BookComponentMetadataForm = props => {
  const {
    className,
    initialFormValues,
    innerRef,
    isSaving,
    onSubmit,
    searchGranthub,
    showSubmitButton,
    submitButtonLabel,
    readonly,
  } = props

  const { s } = useContext(ConstantsContext)
  const initialValues = merge({}, defaultInitialValues, initialFormValues)

  const noContributors =
    initialValues.authors?.length === 0 &&
    initialValues.editors?.length === 0 &&
    initialValues.collaborativeAuthors?.length === 0

  // the only thing that is editable is funding, so only send that up
  const handleSubmit = values => {
    const submitData = { funding: values.funding }
    onSubmit(submitData)
  }

  return (
    <Wrapper className={className}>
      <Formik
        initialValues={initialValues}
        innerRef={innerRef}
        onSubmit={handleSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const { values } = formProps

          return (
            <Form noValidate>
              <Note content="Fields containing metadata provided in source files for conversion and tagging are read-only and cannot be updated." />
              <FormSection label="Title Information">
                <TextField
                  disabled
                  field={{ name: 'title', value: values.title }}
                  form={formProps}
                  label={s('form.title')}
                />

                <TextField
                  disabled
                  field={{ name: 'subTitle', value: values.subTitle }}
                  form={formProps}
                  label={s('book.subtitle')}
                />

                <TextField
                  disabled
                  field={{ name: 'altTitle', value: values.altTitle }}
                  form={formProps}
                  label={s('book.altTitle')}
                />

                <TextField
                  disabled
                  field={{ name: 'label', value: values.label }}
                  form={formProps}
                  label={s('book.label')}
                />

                <TextField
                  disabled
                  field={{ name: 'chapterId', value: values.chapterId }}
                  form={formProps}
                  label={s('book.chapterId')}
                />

                <TextField
                  disabled
                  field={{ name: 'objectId', value: values.objectId }}
                  form={formProps}
                  label={s('form.objectId')}
                />

                <TextField
                  disabled
                  field={{ name: 'language', value: values.language }}
                  form={formProps}
                  label={s('form.language')}
                />

                <DateInput
                  data-test-id="date-of-publication"
                  disabled
                  field={{
                    name: 'dateOfPublication',
                    value: values.dateOfPublication,
                  }}
                  form={formProps}
                  label={s('form.dateOfPublication')}
                />
              </FormSection>

              <FormSection label="Publication history">
                <DateInput
                  data-test-id="date-created"
                  disabled
                  field={{
                    name: 'dateCreated',
                    value: values.dateCreated,
                  }}
                  form={formProps}
                  label={s('form.dateCreated')}
                />

                <DateInput
                  data-test-id="date-updated"
                  disabled
                  field={{
                    name: 'dateUpdated',
                    value: values.dateUpdated,
                  }}
                  form={formProps}
                  label={s('form.dateUpdated')}
                />

                <DateInput
                  data-test-id="date-revised"
                  disabled
                  field={{
                    name: 'dateRevised',
                    value: values.dateRevised,
                  }}
                  form={formProps}
                  label={s('form.dateRevised')}
                />
              </FormSection>

              <FormSection label="Contributors">
                {noContributors && 'No contributors'}

                {values.authors.map((author, index) => {
                  return (
                    <div key={uuid()}>
                      <Collaborator>Author {index + 1} </Collaborator>

                      <TextField
                        disabled
                        field={{
                          name: `authors[${index}].surname`,
                          value: values.authors[index].surname,
                        }}
                        form={formProps}
                        label={s('form.surname')}
                      />

                      <TextField
                        disabled
                        field={{
                          name: `authors[${index}].givenName`,
                          value: values.authors[index].givenName,
                        }}
                        form={formProps}
                        label={s('form.givenNames')}
                      />

                      <TextField
                        disabled
                        field={{
                          name: `authors[${index}].suffix`,
                          value: values.authors[index].suffix,
                        }}
                        form={formProps}
                        label={s('form.suffix')}
                      />
                    </div>
                  )
                })}

                {values.editors.map((editor, index) => {
                  return (
                    <div key={uuid()}>
                      <Collaborator>Editor {index + 1} </Collaborator>

                      <TextField
                        disabled
                        field={{
                          name: `editors[${index}].surname`,
                          value: values.editors[index].surname,
                        }}
                        form={formProps}
                        label={s('form.surname')}
                      />

                      <TextField
                        disabled
                        field={{
                          name: `editors[${index}].givenName`,
                          value: values.editors[index].givenName,
                        }}
                        form={formProps}
                        label={s('form.givenNames')}
                      />

                      <TextField
                        disabled
                        field={{
                          name: `editors[${index}].suffix`,
                          value: values.editors[index].suffix,
                        }}
                        form={formProps}
                        label={s('form.suffix')}
                      />
                    </div>
                  )
                })}

                {values.collaborativeAuthors.map((c, index) => {
                  return (
                    <div key={uuid()}>
                      <Collaborator>
                        Collaborative author {index + 1}
                      </Collaborator>

                      <TextField
                        disabled
                        field={{
                          name: `collaborativeAuthors[${index}]`,
                          value: values.collaborativeAuthors[index],
                        }}
                        form={formProps}
                        label={s('form.authorName')}
                      />
                    </div>
                  )
                })}
              </FormSection>

              <FormSection label="funding">
                <PermissionsGate
                  errorProps={{ disabled: true }}
                  scopes={['granthub-metadata-modal']}
                >
                  <Granthub
                    awards={values.funding}
                    disabled={readonly || isSaving}
                    name="funding"
                    onSearch={searchGranthub}
                    showApply={false}
                  />
                </PermissionsGate>
              </FormSection>

              {showSubmitButton && (
                <ButtonWrapper>
                  <Button status="primary" type="submit">
                    {submitButtonLabel}
                  </Button>
                </ButtonWrapper>
              )}
            </Form>
          )
        }}
      </Formik>
    </Wrapper>
  )
}

BookComponentMetadataForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  initialFormValues: PropTypes.shape({
    title: PropTypes.string,
    subTitle: PropTypes.string,
    altTitle: PropTypes.string,
    label: PropTypes.string,
    chapterId: PropTypes.string,
    objectId: PropTypes.string,
    language: PropTypes.string,
    dateOfPublication: PropTypes.shape({
      startYear: PropTypes.string,
      endYear: PropTypes.string,
    }),
    dateCreated: PropTypes.shape({
      startYear: PropTypes.string,
      endYear: PropTypes.string,
    }),
    dateUpdated: PropTypes.shape({
      startYear: PropTypes.string,
      endYear: PropTypes.string,
    }),
    dateRevised: PropTypes.shape({
      startYear: PropTypes.string,
      endYear: PropTypes.string,
    }),
    authors: PropTypes.arrayOf(
      PropTypes.shape({
        givenNames: PropTypes.string,
        surname: PropTypes.string,
      }),
    ),
    collaborativeAuthors: PropTypes.arrayOf(PropTypes.string),
    editors: PropTypes.arrayOf(
      PropTypes.shape({
        givenNames: PropTypes.string,
        surname: PropTypes.string,
      }),
    ),
  }),
  isSaving: PropTypes.bool,
  searchGranthub: PropTypes.func.isRequired,
  showSubmitButton: PropTypes.bool,
  submitButtonLabel: PropTypes.string,
  readonly: PropTypes.bool,
}

BookComponentMetadataForm.defaultProps = {
  initialFormValues: {},
  isSaving: false,
  showSubmitButton: false,
  readonly: false,
  submitButtonLabel: 'Submit',
}

export default BookComponentMetadataForm
