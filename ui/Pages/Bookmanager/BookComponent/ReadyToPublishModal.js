/* eslint-disable react/prop-types */
import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import {
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
} from '../../../components'
import { TableLocalData } from '../../../components/Datatable'
import ConstantsContext from '../../../../app/constantsContext'

const Title = styled.p`
  font-family: ${th('fontInerface')};
  font-size: 12pt;
  font-weight: 600;
`

const ReadyToPublishModal = ({
  title,
  confirmAction,
  setIsOpen,
  modalIsOpen,
  hideModal,
  componentTitle,
  data,
  columns,
  loading,
}) => {
  const { s } = useContext(ConstantsContext)
  return (
    <Modal fullSize isOpen={modalIsOpen} role="dialog">
      <ModalHeader hideModal={hideModal}>{title}</ModalHeader>
      <ModalBody>
        <Title>{`Some Previewers have not approved ${componentTitle}`}</Title>
        <TableLocalData
          columns={columns}
          count={data.length}
          dataTable={data || []}
          headerHeight={40}
          key="table"
          noDataMessage={s('pages.noData')}
          rowHeight={40}
        />
      </ModalBody>

      <ModalFooter>
        <Button
          data-test-id="ready-to-publish"
          disabled={loading}
          onClick={async () => {
            confirmAction()
            setIsOpen(false)
          }}
          status="primary"
        >
          Ready to publish
        </Button>

        <Button
          data-test-id="cancel-ready-to-publish"
          onClick={() => setIsOpen(false)}
          outlined
          status="primary"
        >
          {s('form.cancel')}
        </Button>
      </ModalFooter>
    </Modal>
  )
}

ReadyToPublishModal.propTypes = {
  hideModal: PropTypes.func.isRequired,
  modalIsOpen: PropTypes.bool,
  title: PropTypes.node,
  componentTitle: PropTypes.string,
}

ReadyToPublishModal.defaultProps = {
  modalIsOpen: false,
  title: 'Organization access',
  componentTitle: 'these components',
}

export default ReadyToPublishModal
