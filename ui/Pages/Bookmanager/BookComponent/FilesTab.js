/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import { useMutation } from '@apollo/client'
import { th } from '@pubsweet/ui-toolkit'
import 'react-datepicker/dist/react-datepicker.css'
import styled from 'styled-components'

// eslint-disable-next-line import/no-cycle
import {
  SourceFiles,
  ConvertedFiles,
  SupplementaryFiles,
  Support,
  Images,
  Review,
} from '../../../components/FilesSections'
import {
  Notification,
  TableWithToolbar,
  ActionBar,
  Button,
  FixedLoader,
  Dropdown as uiDropdown,
  Information,
} from '../../../components'
import query from '../graphql'

import {
  validateFileName,
  validateWholeBookSourceFiles,
  openWindowWithPost,
} from '../../../common/utils'
import { serverUrl } from '../../../../app/utilsConfig'
import {
  areFilesTaged,
  disableReloadPreview,
  disabledSubmit,
  disableUploadSource,
  disableUploadConverted,
  disableUploadSupplementary,
  disableUploadImages,
  disableUploadSupport,
  disableUploadPDFs,
} from '../../../common/statusActions'
import PermissionsGate from '../../../../app/components/PermissionsGate'

const Root = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: space-between;
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  overflow-y: auto;
  padding: ${th('gridUnit')};
`

const Dropdown = styled(uiDropdown)`
  background: white;
`

const Files = ({ values = {}, canUpload: hasUploadRight }) => {
  const {
    sourceFiles,
    convertedFile,
    supplementaryFiles,
    supportFiles,
    images,
    pdf,
    reviewsFiles,
    id,
    status,
    bookId,
    versionName,
    workflow: conversionWorkflow,
    chapterIndependently,
    bookStatus,
    bookComponentVersionId,
    sourceType,
    collectionSourceType,
    isLatestVersion,
    enableUpload,
  } = values

  const {
    UPLOAD_CONVERT_FILE,
    UPDATETAGFILE,
    GET_BOOK_COMPONENT,
    RECONVERT_FILE,
    GET_BOOK_USERS,
    DELETE_FILE,
    UPLOAD_FILE,
    SUBMIT_BOOK_COMPONENTS,
  } = query

  const isWord = conversionWorkflow === 'word'
  /* eslint-disable no-unused-vars */
  const isXml = conversionWorkflow === 'xml'

  const refetchQueriesFn = ({ both = false, force = null }) => {
    let isChapterProccessed = chapterIndependently === true

    if (force !== null) {
      if (force === 'chapter') {
        isChapterProccessed = true
      } else if (force === 'book') {
        isChapterProccessed = false
      }
    }

    let queryBuilder = isChapterProccessed
      ? [
          {
            query: GET_BOOK_COMPONENT,
            variables: { id: bookComponentVersionId },
          },
        ]
      : [{ query: GET_BOOK_USERS, variables: { id: bookId || id } }]

    if (both === true && force === null && isChapterProccessed) {
      queryBuilder = [
        { query: GET_BOOK_USERS, variables: { id: bookId || id } },
        {
          query: GET_BOOK_COMPONENT,
          variables: { id: bookComponentVersionId },
        },
      ]
    }

    return queryBuilder
  }

  const [uploadBookComponent, { loading: isUploading }] = useMutation(
    UPLOAD_CONVERT_FILE,
  )

  const [
    updateTagFile,
    { loading: loadingTagUpdate, error: updateTagError },
  ] = useMutation(UPDATETAGFILE)

  const saveTagFile = ({ tag, fileId }) => {
    updateTagFile({
      refetchQueries: refetchQueriesFn({}),
      variables: { id: fileId, tag },
    })

    if (updateTagError)
      Notification({
        message: 'Could not update tag   ',
        type: 'danger',
        title: 'Error',
      })
  }

  const [reconvertBookComponent, { loading: reconvertingBc }] = useMutation(
    RECONVERT_FILE,
  )

  const [bookSourceTypeModal, setBookSourceTypeModalIsOpen] = useState(false)

  const handleReloadPreview = () => {
    // eslint-disable-next-line no-extra-boolean-cast
    if (!!sourceType || !!collectionSourceType) {
      setBookSourceTypeModalIsOpen(false)

      reconvertBookComponent({
        refetchQueries: refetchQueriesFn({ both: true }),
        variables: {
          ids: [id],
        },
      }).catch(res => {
        console.error(res)

        Notification({
          message: 'Could not reload preview',
          type: 'danger',
          title: 'Error',
        })
      })
    } else {
      setBookSourceTypeModalIsOpen(true)
    }
  }

  // #region sourcefiles

  const saveFileVersion = ({
    fileList,
    fileVersion,
    dispatch,
    refetchQueries,
  }) => {
    uploadBookComponent({
      refetchQueries,
      variables: {
        files: fileVersion
          ? [{ data: fileVersion }]
          : fileList.map(f => ({ data: f })),
        id: bookId || id,
        versionName: parseInt(versionName, 10),
        category: 'source',
      },
    })
      .then(() => {
        dispatch()
      })
      .catch(res => {
        console.error(res)

        Notification({
          message: 'Could not upload file ',
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const statusActionObject = { status, sourceFiles, convertedFile }

  // #region  supplementary files
  const [deleteFile, { loading: deletingFile }] = useMutation(DELETE_FILE)

  const [uploadFiles, { loading: isLoadingSup }] = useMutation(UPLOAD_FILE)

  const [uploadConvertedFiles, { loading: isLoadingConv }] = useMutation(
    UPLOAD_CONVERT_FILE,
  )

  const uploadFilesFn = category => ({ fileUpload, dispatch }) => {
    uploadFiles({
      refetchQueries: refetchQueriesFn({}),
      variables: {
        files: fileUpload,
        id,
        category,
      },
    })
      .then(e => {
        dispatch()
      })
      .catch(e => {
        Notification({
          type: 'danger',
          message: e.message,
          title: 'Error uploading file',
        })
      })
  }

  const uploadConvertedFilesFn = () => ({ fileUpload, dispatch }) => {
    uploadConvertedFiles({
      refetchQueries: refetchQueriesFn({}),
      variables: {
        files: fileUpload,
        id: bookId || id,
        versionName: parseInt(versionName, 10),
        category: 'converted',
      },
    })
      .then(() => {
        dispatch()
      })
      .catch(e => {
        Notification({
          type: 'danger',
          message: e.message,
          title: 'Error uploading file',
        })
      })
  }

  const sectionProps = {
    workflow: conversionWorkflow,
    type: isWord && !chapterIndependently ? 'Book' : 'Book Component',
  }

  const [submitComponentsFn, { loading: isLoadingSubmit }] = useMutation(
    SUBMIT_BOOK_COMPONENTS,
  )

  const submitComponents = () => {
    // eslint-disable-next-line no-extra-boolean-cast
    if (!!sourceType || !!collectionSourceType) {
      setBookSourceTypeModalIsOpen(false)

      submitComponentsFn({
        refetchQueries: refetchQueriesFn({ both: true }),
        variables: {
          bookComponents: [id],
        },
      }).catch(e => {
        Notification({
          title: 'Error happened while submitting',
          message: e.message,
          type: 'danger',
        })
      })
    } else {
      setBookSourceTypeModalIsOpen(true)
    }
  }

  const getSourceFileMatch = name => {
    const lastIndex = name.lastIndexOf('.')
    const namearr = name.slice(0, lastIndex).toLowerCase()
    return [`${namearr}.bxml`, `${namearr}.xml`]
  }

  const validateSourceUpload = filesuploaded => {
    if (chapterIndependently) {
      return validateFileName(
        filesuploaded,
        values.metadata.filename,
        conversionWorkflow,
      )
    }

    return validateWholeBookSourceFiles(filesuploaded, conversionWorkflow)
  }

  const optionsDownload = selectedRows => {
    return [
      {
        title: 'Download selected',
        value: 1,
        disabled: (selectedRows && selectedRows.length > 0) === false,
        onClick: async () => {
          openWindowWithPost(
            `${serverUrl}/downloadFileVersionsAsZip?bookComponentId=${values.id}&allFiles=false`,
            selectedRows.map(row => ({ key: 'fileIds', value: row })),
          )
        },
      },
      {
        title: 'Download all',
        value: 2,
        disabled: false,
        onClick: () => {
          openWindowWithPost(
            `${serverUrl}/downloadFileVersionsAsZip?bookComponentId=${values.id}&allFiles=true`,
            [],
          )
        },
      },
    ]
  }

  const isTagged = areFilesTaged({
    workflow: conversionWorkflow,
    chapterIndependently,
    sourceFiles,
  })
  //  //#endregion

  return (
    <TableWithToolbar>
      {({ checkboxColumn, selectedRows, setSelectedRows }) => {
        return (
          <Root>
            <Wrapper>
              <>
                <PermissionsGate
                  errorProps={{ canUpload: false }}
                  scopes={[`upload-source-file`]}
                >
                  <SourceFiles
                    canUpload
                    checkboxColumn={checkboxColumn}
                    disableUploadButton={
                      disableUploadSource(statusActionObject) ||
                      // eslint-disable-next-line no-nested-ternary
                      (chapterIndependently
                        ? false
                        : !isLatestVersion
                        ? enableUpload
                        : '')
                    }
                    files={sourceFiles}
                    isUploading={isUploading}
                    refetchQueries={refetchQueriesFn({ both: true })}
                    saveFileVersion={saveFileVersion}
                    sectionProps={sectionProps}
                    selectedRows={selectedRows}
                    setSelectedRows={setSelectedRows}
                    showTags={isXml && !chapterIndependently}
                    updateTagFile={saveTagFile}
                    validateUpload={validateSourceUpload}
                    versionName={versionName}
                  />
                </PermissionsGate>
                <PermissionsGate
                  errorProps={{ canUpload: false }}
                  scopes={[`upload-converted-file`]}
                >
                  <ConvertedFiles
                    canUpload
                    checkboxColumn={checkboxColumn}
                    conversionWorkflow={conversionWorkflow}
                    disableUploadButton={
                      disableUploadConverted(statusActionObject) ||
                      // eslint-disable-next-line no-nested-ternary
                      (chapterIndependently
                        ? false
                        : !isLatestVersion
                        ? enableUpload
                        : '')
                    }
                    files={convertedFile}
                    isUploading={isLoadingConv}
                    refetchQueries={refetchQueriesFn({ both: true })}
                    saveFileFn={uploadConvertedFilesFn()}
                    sectionProps={sectionProps}
                    selectedRows={selectedRows}
                    setSelectedRows={setSelectedRows}
                    sourceFile={
                      values.metadata.filename &&
                      getSourceFileMatch(values.metadata.filename)
                    }
                    status={status}
                    validateFile={chapterIndependently}
                    versionName={versionName}
                  />
                </PermissionsGate>
              </>
              {!isWord && (
                <PermissionsGate
                  errorProps={{ canUpload: false }}
                  scopes={[
                    conversionWorkflow === 'pdf'
                      ? 'pdf-upload-files-bookShelfDisplayPdf'
                      : 'xml-upload-files-bookShelfDisplayPdf',
                  ]}
                >
                  <SupplementaryFiles
                    canUpload
                    checkboxColumn={checkboxColumn}
                    deleteFileMutation={deleteFile}
                    deleteScope={['pdf-delete-files']}
                    deletingFile={deletingFile}
                    disableUploadButton={disableUploadPDFs(statusActionObject)}
                    files={pdf || []}
                    isUploading={isLoadingSup}
                    multipleUpload={!chapterIndependently}
                    saveFileFn={uploadFilesFn('pdf')}
                    sectionProps={sectionProps}
                    selectedRows={selectedRows}
                    setSelectedRows={setSelectedRows}
                    showTags={isXml && !chapterIndependently}
                    title="Bookshelf Display PDFs"
                    updateTagFile={saveTagFile}
                    versionName={versionName}
                  />
                </PermissionsGate>
              )}
              <PermissionsGate
                errorProps={{ canUpload: false }}
                scopes={['upload-supplementary-files']}
              >
                <SupplementaryFiles
                  canUpload
                  checkboxColumn={checkboxColumn}
                  deleteFileMutation={deleteFile}
                  deleteScope={['delete-supplementary-files']}
                  deletingFile={deletingFile}
                  disableUploadButton={disableUploadSupplementary(
                    statusActionObject,
                  )}
                  files={supplementaryFiles}
                  isUploading={isLoadingSup}
                  saveFileFn={uploadFilesFn('supplement')}
                  sectionProps={sectionProps}
                  selectedRows={selectedRows}
                  setSelectedRows={setSelectedRows}
                  versionName={versionName}
                />
              </PermissionsGate>
              <PermissionsGate
                errorProps={{ canUpload: false }}
                scopes={['upload-image-files']}
              >
                <Images
                  canUpload={!isWord}
                  checkboxColumn={checkboxColumn}
                  deleteFileMutation={deleteFile}
                  deleteScope={['delete-image-files']}
                  deletingFile={deletingFile}
                  disableUploadButton={disableUploadImages(statusActionObject)}
                  files={images || []}
                  isUploading={isLoadingSup}
                  saveFileFn={uploadFilesFn('image')}
                  sectionProps={sectionProps}
                  selectedRows={selectedRows}
                  setSelectedRows={setSelectedRows}
                  versionName={versionName}
                />
              </PermissionsGate>
              <PermissionsGate
                errorProps={{ canUpload: false }}
                scopes={['upload-support-files']}
              >
                <Support
                  canUpload={conversionWorkflow === 'pdf'}
                  checkboxColumn={checkboxColumn}
                  deleteFileMutation={deleteFile}
                  deleteScope={['delete-support-files']}
                  deletingFile={deletingFile}
                  disableUploadButton={disableUploadSupport(statusActionObject)}
                  files={supportFiles || []}
                  isUploading={isLoadingSup}
                  saveFileFn={uploadFilesFn('support')}
                  sectionProps={sectionProps}
                  selectedRows={selectedRows}
                  setSelectedRows={setSelectedRows}
                  versionName={versionName}
                />
              </PermissionsGate>
              <PermissionsGate scopes={['view-review-files']}>
                <Review
                  checkboxColumn={checkboxColumn}
                  files={reviewsFiles || []}
                  sectionProps={sectionProps}
                  selectedRows={selectedRows}
                  setSelectedRows={setSelectedRows}
                  workflow={conversionWorkflow}
                />
              </PermissionsGate>
            </Wrapper>

            {isLoadingSubmit && <FixedLoader title="Submitting files..." />}
            {reconvertingBc && <FixedLoader title=" Reloading preview..." />}
            {loadingTagUpdate && <FixedLoader title=" Updating tags ..." />}
            {deletingFile && <FixedLoader title=" Deleting file ..." />}

            <Information
              hideModal={() => setBookSourceTypeModalIsOpen(false)}
              isOpen={bookSourceTypeModal}
              title="Complete the required 'Book Source type' field in the Book Metadata, and then come back to this action."
              type="warning"
            />
            <ActionBar
              leftComponent={
                <>
                  <PermissionsGate
                    key="book"
                    scopes={['view-book-component-reload-preview']}
                  >
                    <Button
                      disabled={
                        isLoadingSubmit ||
                        reconvertingBc ||
                        disableReloadPreview(statusActionObject)
                      }
                      onClick={handleReloadPreview}
                      primary
                    >
                      Reload Preview
                    </Button>
                  </PermissionsGate>
                  <PermissionsGate
                    key="submit"
                    scopes={['view-book-component-submit']}
                  >
                    <Button
                      disabled={
                        !isTagged ||
                        isLoadingSubmit ||
                        reconvertingBc ||
                        disabledSubmit(statusActionObject)
                      }
                      onClick={submitComponents}
                      primary
                    >
                      Submit
                    </Button>
                  </PermissionsGate>
                </>
              }
              rightComponent={
                <Dropdown
                  data-test-id="files-tab-downloader"
                  direction="up"
                  disabled={
                    chapterIndependently
                      ? ['new-book', 'new-version'].includes(status)
                      : ['new-book', 'new-version'].includes(bookStatus)
                  }
                  icon="download"
                  itemsList={optionsDownload(selectedRows)}
                  key="book"
                  primary
                  status="primary"
                >
                  Download
                </Dropdown>
              }
            />
          </Root>
        )
      }}
    </TableWithToolbar>
  )
}

export default Files
