/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import { useQuery } from '@apollo/client'
import 'react-datepicker/dist/react-datepicker.css'
import { th } from '@pubsweet/ui-toolkit'
import moment from 'moment'
import styled from 'styled-components'
import query from '../graphql'
import {
  ActionBar,
  Status,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from '../../../components'

import { TableLocalData } from '../../../components/Datatable'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const ErrorsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  padding: ${th('gridUnit')};
  width: 100%;
`

const TextWrapper = styled.div`
  background: #eee;
  margin-bottom: ${th('gridUnit')};
  padding: ${th('gridUnit')};
`

const ShowMore = styled.div`
  border-bottom: 1px solid ${th('colorPrimary')};
  display: flex;
  line-height: 1.5em;
  margin: 10px 0 20px;
  position: relative;
  width: 100%;

  button {
    background: ${th('colorBackground')};
    bottom: -18px;
    padding: 0 10px;
    position: absolute;

    & :hover,
    :active,
    :focus {
      background: ${th('colorBackground')};
      opacity: 1;
    }
  }
`

const ErrorHeader = styled.div`
  font-size: 12pt;
  font-weight: 600;
  margin-top: ${th('gridUnit')};
`

const Severities = styled.ul`
  font-size: 12pt;
  list-style: disc;
  margin: 0;
`

const Errors = ({ id: objectId, ErrorsHelp }) => {
  const [showHistory, setShowHistory] = useState(false)
  const [modalIsOpen, setIsOpen] = useState(false)
  const [modalContent, setModalContent] = useState()

  const showErrorModal = data => {
    setModalContent(data)
    setIsOpen(true)
  }

  const { data } = useQuery(query.GET_BOOK_COMPONENT_ERRORS, {
    variables: {
      objectId,
      showHistory,
    },
  })

  const getTablesWithErrors = () => {
    const tables = []

    data &&
      data.getErrors.forEach(errorGroup => {
        const { jobId, sessionId, status, url, updated, errors } = errorGroup

        const noDataMessage =
          status === 3
            ? 'System error: please contact NCBI Bookshelf System Admin by email at booksauthors@ncbi.nlm.nih.gov or by BCMS chat mentioning @Sysadmin.'
            : 'No errors available'

        tables.push(
          <>
            <strong>
              {`Errors reported for Job ID: ${jobId}`}{' '}
              {sessionId && (
                <a href={url} rel="noreferrer" target="_blank">
                  ({sessionId})
                </a>
              )}
              {`  ${moment(updated).format('YYYY-MM-DD hh:mm:ss')}`}
            </strong>

            <TableLocalData
              columns={columns}
              dataTable={errors || []}
              headerHeight={50}
              key={`book-component-errors-${jobId}`}
              noDataMessage={noDataMessage}
            />
          </>,
        )
      })

    return tables
  }

  const columns = [
    {
      dataKey: 'noticeTypeName',
      disableSort: false,
      label: 'Name ',
      width: 550,
    },
    {
      cellRenderer: () => () => <>-</>,
      disableSort: true,
      label: 'Category',
      width: 550,
    },
    {
      cellRenderer: () => ({ cellData }) => (
        <Status status={cellData.toLowerCase()} />
      ),
      dataKey: 'severity',
      disableSort: true,
      label: 'Severity ',
      width: 550,
    },

    {
      dataKey: 'assignee',
      cellRenderer: () => ({ cellData }) =>
        cellData.charAt(0).toUpperCase() + cellData.slice(1),
      disableSort: true,
      label: 'Assignee',
      width: 450,
    },
    {
      cellRenderer: () => ({ cellData }) => (
        <div onClick={() => showErrorModal(cellData)}>{cellData}</div>
      ),
      dynamicHeight: true,
      dataKey: 'message',
      disableSort: true,
      label: 'Message ',
    },
  ]

  return (
    <Wrapper>
      <ErrorsWrapper>
        <TextWrapper>
          Bookshelf errors are automatically reported to NCBI to resolve. View
          the{' '}
          <a href=" https://preview.ncbi.nlm.nih.gov/books/NBK310885">
            NCBI Bookshelf Author Guide
          </a>{' '}
          for guidance on resolving submitter errors or select ‘Show help’
          below.
          <ErrorHeader>Error Severity</ErrorHeader>
          <Severities>
            <li>Error: must be fixed to successfully process submissions.</li>

            <li>Warning: should be reviewed for potential tagging issues.</li>

            <li>Query: should be addressed to ensure quality assurance.</li>
          </Severities>
        </TextWrapper>

        {getTablesWithErrors()}

        <ShowMore>
          <Button
            onClick={() => {
              setShowHistory(!showHistory)
            }}
            outlined
            status="primary"
          >
            {`${showHistory ? 'Hide history' : ' Show history'}`}
          </Button>
        </ShowMore>
      </ErrorsWrapper>

      <ActionBar rightComponent={ErrorsHelp} />

      <Modal isOpen={modalIsOpen}>
        <ModalHeader hideModal={() => setIsOpen(false)} />

        <ModalBody>{modalContent}</ModalBody>

        <ModalFooter>
          <Button
            data-test-id="cancel-book-metadata"
            onClick={() => setIsOpen(false)}
            outlined
            status="primary"
          >
            Close
          </Button>
        </ModalFooter>
      </Modal>
    </Wrapper>
  )
}

export default Errors
