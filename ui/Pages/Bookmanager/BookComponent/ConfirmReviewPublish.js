/* eslint-disable react/prop-types */
import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { grid } from '@pubsweet/ui-toolkit'

import {
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  Status,
} from '../../../components'
import { TableLocalData } from '../../../components/Datatable'
import ConstantsContext from '../../../../app/constantsContext'

const Title = styled.p`
  font-size: 12pt;
  font-weight: 600;
`

const Part = styled.span`
  margin-left: ${grid(3)};
  margin-right: ${grid(3)};
`

const Inline = styled.span`
  align-items: center;
  display: flex;
  flex-direction: row;
`

const ConfirmReviewPublish = ({
  hasTitle,
  publishBookComponent,
  setIsOpen,
  modalIsOpen,
  componentTitle,
  publishing,
  data,
}) => {
  // Data and columns for  publish in review
  const columns = hasTitle
    ? [
        {
          dataKey: 'title',
          dynamicHeight: true,
          label: 'Title',
        },
        {
          dataKey: 'approve',
          label: 'Approved',
        },
        {
          dataKey: 'revise',
          label: 'Revise',
        },
        {
          dataKey: 'pending',
          label: 'Pending',
        },
      ]
    : [
        {
          dataKey: 'approve',
          label: 'Approved',
        },
        {
          dataKey: 'revise',
          label: 'Revise',
        },
        {
          dataKey: 'pending',
          label: 'Pending decision',
        },
      ]

  const { s } = useContext(ConstantsContext)
  return (
    <Modal fullSize isOpen={modalIsOpen} role="dialog">
      <ModalHeader hideModal={() => setIsOpen(false)}>
        {data.length === 1 ? (
          <Inline>
            <Part>
              <strong> {s('pages.publish')}</strong>
            </Part>
            <Part>
              {s('pages.version')} {data[0]?.versionName}{' '}
            </Part>
            <Part>
              <Inline>
                {s('pages.status')}: <Status status={data[0]?.status} />
              </Inline>
            </Part>
          </Inline>
        ) : (
          <>{s('pages.publish')}</>
        )}
      </ModalHeader>
      <ModalBody>
        <Title>{`Some Previewers have not approved ${componentTitle}`}</Title>
        <TableLocalData
          columns={columns}
          count={data.length}
          dataTable={data || []}
          headerHeight={40}
          key="table"
          noDataMessage={s('pages.noData')}
          rowHeight={40}
        />
      </ModalBody>

      <ModalFooter>
        <Button
          data-test-id="confirm-publish"
          disabled={publishing}
          onClick={() =>
            publishBookComponent(data.map(x => ({ checked: true, ...x })))
          }
          status="primary"
        >
          {publishing ? 'Publishing...' : 'Publish'}
        </Button>

        <Button
          data-test-id="cancel-publish"
          onClick={() => setIsOpen(false)}
          outlined
          status="primary"
        >
          {publishing ? s('form.close') : s('form.cancel')}
        </Button>
      </ModalFooter>
    </Modal>
  )
}

ConfirmReviewPublish.propTypes = {
  modalIsOpen: PropTypes.bool,
  hasTitle: PropTypes.bool,
  componentTitle: PropTypes.string,
}

ConfirmReviewPublish.defaultProps = {
  modalIsOpen: false,
  hasTitle: false,
  componentTitle: 'these components',
}

export default ConfirmReviewPublish
