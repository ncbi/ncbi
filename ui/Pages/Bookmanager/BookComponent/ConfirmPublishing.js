/* eslint-disable react/prop-types */
import React, { useState, useContext, useEffect } from 'react'
import styled from 'styled-components'
import { grid } from '@pubsweet/ui-toolkit'
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  TableWithToolbar,
  Button,
  Status,
  QueryIcon,
} from '../../../components'
import ConstantsContext from '../../../../app/constantsContext'
import { TableLocalData } from '../../../components/Datatable'

const Part = styled.span`
  margin-left: ${grid(3)};
  margin-right: ${grid(3)};
`

const Body = styled.div`
  margin: ${grid(3)};
`

const Inline = styled.span`
  align-items: center;
  display: flex;
  flex-direction: row;
`

export default ({
  modalIsOpen,
  setIsOpen,
  bookComponents = [],
  publishBookComponent,
  publishing,
}) => {
  const [selectedRows, setSelectedRows] = useState([])

  const [selectedOptions, setSelectedOptions] = useState(
    bookComponents.map(item => ({
      ...item,
      checked: true,
      option: { label: 'New Published version', value: 'new' },
    })) || [],
  )

  useEffect(() => {
    setSelectedOptions(
      bookComponents.map(item => ({
        ...item,
        checked: true,
        option: { label: 'New Published version', value: 'new' },
      })),
    )
  }, [bookComponents])

  const { s } = useContext(ConstantsContext)

  const columns = [
    {
      cellRenderer: () => ({ cellData, rowData }) => (
        <span
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{
            __html: cellData,
          }}
        />
      ),
      dataKey: 'title',
      disableSort: true,
      label: 'Title ',
    },

    {
      cellRenderer: () => ({ cellData }) => <>V{cellData}</>,
      dataKey: 'version',
      disableSort: true,
      label: 'Version',
      width: 250,
    },
    {
      cellRenderer: () => ({ cellData, rowData }) => (
        <>
          <Status status={cellData} />
          {rowData.hasWarnings ? <QueryIcon /> : ''}
        </>
      ),
      dataKey: 'status',
      disableSort: true,
      label: 'Status',
      width: 350,
    },
  ]

  return (
    <Modal
      fullSize={bookComponents.length > 1}
      isOpen={modalIsOpen}
      style={{
        overlay: { zIndex: 1 },
      }}
    >
      <ModalHeader hideModal={() => setIsOpen(false)}>
        {bookComponents.length === 1 ? (
          <Inline>
            <Part>
              <strong> {s('pages.publish')}</strong>
            </Part>
            <Part>
              {s('pages.version')} {bookComponents[0]?.versionName}{' '}
            </Part>
            <Part>
              <Inline>
                {s('pages.status')}:{' '}
                <Status status={bookComponents[0]?.status} />
                {bookComponents[0].hasWarnings ? <QueryIcon /> : ''}
              </Inline>
            </Part>
          </Inline>
        ) : (
          <>{s('pages.publish')}</>
        )}
      </ModalHeader>
      <ModalBody
        style={{ height: bookComponents.length <= 3 ? '20vh' : '70vh' }}
      >
        <Body>
          {bookComponents.length === 1 ? (
            <>
              {s('confirm.publishBook')} {/* if has Query errors */}
              {bookComponents[0].hasWarnings ? (
                <>
                  ? There are unresolved queries in{' '}
                  <strong>
                    {bookComponents[0].title.replace(/<[^>]+>/g, '')}
                  </strong>
                  . Refer to the Errors tab.
                </>
              ) : (
                <strong>
                  {bookComponents[0].title.replace(/<[^>]+>/g, '')} ?
                </strong>
              )}
            </>
          ) : (
            <div>
              {s('confirm.publishBook')} ?{' '}
              {selectedRows.length > 0 &&
              selectedOptions
                ?.filter(x => selectedRows.includes(x.id))
                .some(arr => arr.hasWarnings)
                ? 'There are unresolved queries. Refer to the Errors tab.'
                : null}
              <TableWithToolbar selected={selectedOptions.map(sel => sel.id)}>
                {({
                  checkboxColumn,
                  selectedRows: inerSelectedRows,
                  setSelectedRows: inerSetSelctedRows,
                }) => {
                  useEffect(() => {
                    setSelectedRows(inerSelectedRows)
                  }, [inerSelectedRows])

                  return (
                    <TableLocalData
                      columns={[checkboxColumn].concat(columns)}
                      dataTable={selectedOptions}
                      headerHeight={40}
                      key="book-component-for-publish"
                      noDataMessage="No book components  available"
                      rowHeight={40}
                      selectedRows={inerSelectedRows}
                      setSelectedRows={inerSetSelctedRows}
                    />
                  )
                }}
              </TableWithToolbar>
            </div>
          )}
        </Body>
      </ModalBody>
      <ModalFooter>
        <Button
          data-test-id="save-book-comp-publish"
          disabled={
            (selectedOptions.length === 1
              ? false
              : selectedRows.length === 0) || publishing
          }
          onClick={() => {
            bookComponents.length > 1
              ? publishBookComponent(
                  selectedOptions?.filter(x => selectedRows.includes(x.id)),
                )
              : publishBookComponent(selectedOptions)

            setIsOpen(false)
          }}
          status="primary"
        >
          {!publishing ? s('pages.publish') : 'Publishing...'}
        </Button>

        <Button
          data-test-id="cancel-book-comp-publish"
          onClick={() => setIsOpen(false)}
          outlined
          status="primary"
        >
          {publishing ? s('form.close') : s('form.cancel')}
        </Button>
      </ModalFooter>
    </Modal>
  )
}
