/* eslint-disable react/prop-types */
import React, { useEffect, useRef, useState } from 'react'
import { useMutation, useQuery } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'
import styled from 'styled-components'
import {
  TableWithToolbar,
  ActionBar,
  Notification,
  Button,
  FixedLoader,
} from '../../../components'
import FormBookComponentTeam from './teams/FormBookComponentTeam'
import PermissionsGate from '../../../../app/components/PermissionsGate'
import query from '../graphql'

const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;

  > div:first-child {
    flex-grow: 1;
  }
`

const ManageTeams = ({
  bookTeams,
  componentTeams,
  componentOwners,
  organizationId,
}) => {
  const modalRef = useRef(null)

  const [updateBookTeam, { loading: updateingTeam }] = useMutation(
    query.UPDATE_BOOK_TEAM,
  )

  const { loading, data } = useQuery(query.GET_ORGANISATION_USERS, {
    variables: {
      id: organizationId,
      input: {
        sort: {
          direction: 'asc',
          field: ['givenName', 'lastName', 'username'],
        },
      },
    },
  })

  const [formValues, setValues] = useState(null)

  useEffect(() => {
    if (!loading) {
      setValues({
        book: { teams: bookTeams },
        organization: data?.getOrganisation,
        teams: componentTeams,
        owners: componentOwners,
      })
    }
  }, [loading])

  const handleClickSave = async () => {
    const formData = await modalRef.current.triggerSave()

    if (formData) {
      const members = formData.teams.map(team => ({
        id: team.id,
        team: {
          members: team.members.map(tm => ({
            status: tm.status,
            user: { id: tm.user.id },
          })),
        },
      }))

      updateBookTeam({
        awaitRefetchQueries: true,
        refetchQueries: [
          getOperationName(query.GET_BOOK_USERS),
          getOperationName(query.GET_BOOK_COMPONENT),
        ],
        variables: {
          input: members,
        },
      })
        .then(() => {
          Notification({
            message: 'User teams saved successfully',
            type: 'success',
          })
        })
        .catch(res => {
          console.error(res)

          Notification({
            message: 'User teams could not be saved ',
            type: 'danger',
            title: 'Error',
          })
        })
    }
  }

  const handleClickRemove = async tableProps => {
    modalRef.current.unassign(tableProps.selectedRows)
    // Notification({
    //   message: 'Can`t delete editors',
    //   title: 'You have selected editors',
    //   type: 'warning',
    // })
  }

  if (loading || !formValues) return null
  return (
    <TableWithToolbar>
      {tableProps => (
        <FormWrapper>
          <PermissionsGate
            errorProps={{ readonly: true }}
            scopes={['edit-team-book-component']}
          >
            <FormBookComponentTeam
              formikFormRef={modalRef}
              tableProps={tableProps}
              values={formValues}
            />
          </PermissionsGate>
          {updateingTeam && <FixedLoader title="Updating team ..." />}

          <ActionBar
            leftComponent={
              <PermissionsGate scopes={['edit-team-book-component']}>
                <Button
                  data-test-id="save-component-team"
                  disabled={updateingTeam}
                  key="save"
                  onClick={handleClickSave}
                  status="primary"
                >
                  {updateingTeam ? 'Saving...' : 'Save'}
                </Button>
              </PermissionsGate>
            }
            rightComponent={
              <PermissionsGate scopes={['remove-from-team-book-component']}>
                <Button
                  disabled={updateingTeam}
                  icon="trash-2"
                  iconPosition="end"
                  key="trash"
                  onClick={() => handleClickRemove(tableProps)}
                  outlined
                  size="small"
                  status="primary"
                >
                  Remove from team
                </Button>
              </PermissionsGate>
            }
          />
        </FormWrapper>
      )}
    </TableWithToolbar>
  )
}

export default ManageTeams
