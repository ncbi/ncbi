/* eslint-disable react/prop-types */
import React, { useContext } from 'react'
import { cloneDeep } from 'lodash'
import { ChatBox } from '../../../components'
import query from '../graphql'
import CurrentUserContext from '../../../../app/userContext'
import { getAllRoles, getAllUsers } from '../../../common/utils'

import { onClickFileDownload } from '../../../../app/utilsConfig'

export default ({ book, bookComponent }) => {
  if (!book) return null
  const bookComp = cloneDeep(bookComponent)

  const CHANNEL_ID =
    bookComp && bookComp?.channels.find(x => x.topic === 'error-channel')?.id

  const { currentUser } = useContext(CurrentUserContext)
  const usersList = getAllUsers(book, bookComp) || []
  const rolesList = getAllRoles(book, bookComp) || []
  const userObj = { currentUser, usersList, rolesList }
  return (
    <ChatBox
      channelId={CHANNEL_ID}
      commentsList={query.FEEDBACK_PANEL_MESSAGES}
      commentSubscription={query.MESSAGES_SUBSCRIPTION}
      hideFiles
      newComment={query.NEW_MESSAGE}
      onClickFileDownload={onClickFileDownload}
      refetchQueries={[query.GET_BOOK_COMPONENT]}
      reviewStatus={{
        status: book.status || bookComponent.status,
        reviewsResponse:
          book.reviewsResponse || bookComponent.reviewsResponse || [],
      }}
      showChatOptions={false}
      title="Help"
      users={cloneDeep(userObj)}
    />
  )
}
