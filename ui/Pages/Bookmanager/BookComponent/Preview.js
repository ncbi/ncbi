/* eslint-disable react/prop-types */

import React, { useEffect, useState } from 'react'
import { useMutation } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'

import styled from 'styled-components'
import isEmpty from 'lodash/isEmpty'
import query from '../graphql'
import {
  ActionBar,
  ButtonGroup,
  Button,
  Notification,
  FixedLoader,
  Spinner,
  Information,
  MoveComponents,
  RepeatComponent,
} from '../../../components'
import ConfirmPublishing from './ConfirmPublishing'
import ConfirmReviewPublishModal from './ConfirmReviewPublish'
import PermissionsGate from '../../../../app/components/PermissionsGate'
import { parentPartsForRepeatedComponent } from '../commonPageHelpers'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const PreviewWrapper = styled.div`
  flex-grow: 1;
`

const NoPreviewNote = styled.div`
  align-content: center;
  align-items: center;
  align-self: center;
  display: flex;
  justify-content: center;
`

const allowedStatusesToMove = [
  'loading-preview',
  'loading-errors',
  'preview',
  'conversion-errors',
  'publish-failed',
  'published',
]

const Preview = props => {
  const {
    bookBodyDivision,
    canPublish,
    collectionSourceType = '',
    componentDivisionId,
    componentErrors,
    componentId,
    componentReviewResponse,
    componentStatus,
    componentTitle,
    convertedFile,
    divisionsMap,
    enableUpload,
    FeedBack, // component
    groupChaptersInParts,
    isBookOrderedManually,
    isComponentInDivisionTopLevel,
    isLatestVersion,
    isWholeBook = false,
    partsTree,
    onMoveSearch,
    onReorder,
    onRepeat,
    parentPartsOfComponent,
    sourceFiles,
    sourceType = '',
    url,
    versionName,
    workflow,
  } = props

  const [previewLink, setLink] = useState(null)
  const [confirmIsOpen, setConfirmIsOpen] = useState(false)
  const [reviewConfirmIsOpen, setReviewConfirmIsOpen] = useState(false)
  const [bookSourceTypeModal, setBookSourceTypeModalIsOpen] = useState(false)

  const bodyDivisionId = bookBodyDivision?.id

  const { BOOK_COMPONENT_PUBLISH } = query

  const [publishBookComponent, { loading: publishing }] = useMutation(
    BOOK_COMPONENT_PUBLISH,
  )

  const publishBookcompFn = () => {
    return publishBookComponent({
      refetchQueries: [getOperationName(query.GET_BOOK_USERS)],
      variables: {
        bookComponent: [componentId],
      },
    })
      .then(() => {
        setConfirmIsOpen(false)
        setReviewConfirmIsOpen(false)
      })
      .catch(e => {
        Notification({
          type: 'danger',
          message: e.message || "Can't publish",
          title: 'Error',
        })
      })
  }

  const handlePublish = () => {
    if (!!sourceType || !!collectionSourceType) {
      setBookSourceTypeModalIsOpen(false)

      componentStatus === 'in-review'
        ? setReviewConfirmIsOpen(true)
        : setConfirmIsOpen(true)
    } else {
      setBookSourceTypeModalIsOpen(true)
    }
  }

  const handleReorder = (
    componentIds,
    targetId,
    action,
    partId,
    parentIdsToRefresh,
  ) => {
    return onReorder(componentIds, targetId, action, partId, parentIdsToRefresh)
  }

  const isAllowedToMove =
    componentDivisionId === bodyDivisionId &&
    allowedStatusesToMove.includes(componentStatus)

  const isAllowedToRepeatChapters =
    isAllowedToMove &&
    parentPartsOfComponent.length > 0 &&
    !isComponentInDivisionTopLevel

  const { approvedUsers, pendingUsers, revisedUsers } = componentReviewResponse

  const blockingLength =
    revisedUsers.length >= pendingUsers.length
      ? revisedUsers.length
      : pendingUsers.length

  const selectedPartsForRepeatedComponent = parentPartsForRepeatedComponent(
    componentId,
    partsTree,
  )

  const getReviewData = () => {
    const reviewData = []

    const approvedLength = approvedUsers.length

    const dataLenth =
      approvedLength >= blockingLength ? approvedLength : blockingLength

    for (let i = 0; i < dataLenth; i += 1) {
      reviewData.push({
        approved: approvedUsers[i]?.name || '',
        revise: revisedUsers[i]?.name || '',
        pending: pendingUsers[i]?.name || '',
      })
    }

    return reviewData
  }

  useEffect(() => {
    url && setLink(url)
  }, [componentId])

  let previewTabText = ''

  const inReviewConverting = () => {
    if (componentStatus === 'in-review') {
      return (
        [('converting', 'loading-preview')].includes(
          sourceFiles[0].status || null,
        ) ||
        [('converting', 'loading-preview')].includes(
          convertedFile[0].status || null,
        )
      )
    }

    return false
  }

  let hasPreviewLink = !isEmpty(url)

  switch (componentStatus) {
    case 'converting':
    case 'loading-preview':
      previewTabText = 'Preview will display when file loads successfully.'
      hasPreviewLink = false
      break

    case 'in-review':
      if (
        (convertedFile?.length > 0 &&
          [('converting', 'loading-preview')].includes(
            convertedFile[0].status,
          )) ||
        (sourceFiles?.length > 0 &&
          [('converting', 'loading-preview')].includes(sourceFiles[0].status))
      ) {
        previewTabText = 'Preview will display when file loads successfully.'
        hasPreviewLink = false
      }

      break
    case 'loading-errors':
    case 'conversion-errors':
      previewTabText =
        'No preview available. Resolve errors to generate the preview.'

      hasPreviewLink = false

      break
    case 'new-book':
    case 'new-upload':
    case 'new-version':
      previewTabText =
        workflow !== 'pdf'
          ? 'Submit files for conversion to generate the preview'
          : 'Submit files for tagging to generate the preview'

      hasPreviewLink = false

      break

    default:
      if (
        (sourceFiles.length &&
          ['converting', 'loading-preview'].includes(sourceFiles[0].status)) ||
        (convertedFile.length &&
          ['converting', 'loading-preview'].includes(convertedFile[0].status))
      ) {
        previewTabText = 'Preview will display when file loads successfully.'
        // hasPreviewLink = false
      } else {
        previewTabText = ''
      }
  }

  let previewContent = <NoPreviewNote>{previewTabText}</NoPreviewNote>

  const latestVersion = (
    <NoPreviewNote>
      {' '}
      Previews are only supported for the latest version
    </NoPreviewNote>
  )

  const disablePublish = () => {
    // if we have preview link
    if (hasPreviewLink) {
      return (
        ![
          'published',
          'in-review',
          'preview',
          'approved',
          'publish-failed',
        ].includes(componentStatus) || inReviewConverting()
      )
    }

    return true
  }

  if (hasPreviewLink) {
    previewContent = (
      <iframe
        frameBorder="0"
        marginHeight="0"
        marginWidth="0"
        // onLoad={setLoading(false)}
        src={previewLink}
        style={{ height: '100%' }}
        title="prev"
        width="100%"
      />
    )
  }

  return (
    <Wrapper>
      <PreviewWrapper>
        {
          // eslint-disable-next-line no-nested-ternary
          !isWholeBook
            ? previewContent
            : isLatestVersion
            ? previewContent
            : latestVersion
        }
      </PreviewWrapper>
      <ActionBar
        leftComponent={
          <ButtonGroup>
            {canPublish && (
              <PermissionsGate
                scopes={[
                  isWholeBook ? 'publish-book' : 'publish-book-component',
                ]}
              >
                <Button
                  data-test-id="book-comp-publish"
                  disabled={
                    publishing ||
                    disablePublish() ||
                    // eslint-disable-next-line no-nested-ternary
                    (!isWholeBook
                      ? false
                      : !isLatestVersion
                      ? enableUpload
                      : '')
                  }
                  onClick={() => handlePublish()}
                  status="primary"
                >
                  {publishing ? 'Publishing...' : 'Publish'}
                </Button>
              </PermissionsGate>
            )}
            <Information
              hideModal={() => setBookSourceTypeModalIsOpen(false)}
              isOpen={bookSourceTypeModal}
              title="Complete the required 'Book Source type' field in the Book Metadata, and then come back to this action."
              type="warning"
            />
            {groupChaptersInParts && (
              <PermissionsGate scopes={['order-by-drag-drop', 'move-book-to']}>
                <MoveComponents
                  autoSorted={!isBookOrderedManually}
                  bodyId={bodyDivisionId}
                  divisionId={componentDivisionId}
                  divisionsMap={divisionsMap}
                  groupInParts={groupChaptersInParts}
                  isDisabled={!isAllowedToMove}
                  key="move-book-component"
                  onReorder={handleReorder}
                  onSearch={onMoveSearch}
                  selectedBookComponents={[
                    { id: componentId, title: componentTitle },
                  ]}
                />

                <RepeatComponent
                  isDisabled={!isAllowedToRepeatChapters}
                  key="repeat-book-component"
                  onRepeat={onRepeat}
                  partsTree={partsTree}
                  selectedBookComponent={{
                    id: componentId,
                    title: componentTitle,
                  }}
                  selectedPartIdsForRepeatedComponent={
                    selectedPartsForRepeatedComponent
                  }
                />
              </PermissionsGate>
            )}
          </ButtonGroup>
        }
        rightComponent={FeedBack}
      />

      <ConfirmReviewPublishModal
        componentTitle={componentTitle}
        data={getReviewData()}
        modalIsOpen={reviewConfirmIsOpen}
        publishing={publishing}
        setIsOpen={setReviewConfirmIsOpen}
      />

      <ConfirmPublishing
        bookComponents={
          componentId
            ? [
                {
                  id: componentId,
                  versionName,
                  status: componentStatus,
                  title: componentTitle,
                  errors: componentErrors,
                },
              ]
            : []
        }
        modalIsOpen={confirmIsOpen}
        publishBookComponent={publishBookcompFn}
        publishing={publishing}
        setIsOpen={setConfirmIsOpen}
      />

      {publishing && (
        <FixedLoader>
          <Spinner />

          <span>Publishing ...</span>
        </FixedLoader>
      )}
    </Wrapper>
  )
}

export default Preview
