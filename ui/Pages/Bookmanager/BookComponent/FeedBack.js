/* eslint-disable react/prop-types */
import React, { useContext } from 'react'
import { cloneDeep } from 'lodash'
import { ChatBox } from '../../../components'
import query from '../graphql'
import CurrentUserContext from '../../../../app/userContext'
import { getAllRoles, getAllUsers } from '../../../common/utils'
import { onClickFileDownload } from '../../../../app/utilsConfig'

export default ({
  book,
  bookComponent,
  setBookComponentStatus,
  disabled,
  disableOptions,
  ...props
}) => {
  if (!(book && bookComponent)) return null

  const CHANNEL_ID =
    (bookComponent &&
      bookComponent?.channels.find(x => x.topic === 'feedback-channel')?.id) ||
    null

  const { currentUser } = useContext(CurrentUserContext)

  const usersList = getAllUsers(book, bookComponent) || []
  const rolesList = getAllRoles(book, bookComponent) || []

  const userObj = { currentUser, usersList, rolesList }
  const bookComp = cloneDeep(bookComponent)
  return (
    <ChatBox
      channelId={CHANNEL_ID}
      commentsList={query.FEEDBACK_PANEL_MESSAGES}
      commentSubscription={query.MESSAGES_SUBSCRIPTION}
      disabled={disabled}
      disableOptions={disableOptions}
      newComment={query.NEW_MESSAGE}
      onClickFileDownload={onClickFileDownload}
      refetchQueries={[query.GET_BOOK_COMPONENT]}
      reviewStatus={{
        status: bookComp.status,
        reviewsResponse: bookComp.reviewsResponse || [],
      }}
      showChatOptions={
        book?.settings?.approvalPreviewer &&
        bookComp.status !== 'published' &&
        bookComp.status !== 'new-version'
      }
      title="review"
      users={cloneDeep(userObj)}
    />
  )
}
