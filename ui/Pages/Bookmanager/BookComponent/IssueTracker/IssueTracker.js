/* eslint-disable react/prop-types */

import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { useMutation, useQuery } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'
import query from '../../graphql'
import {
  Button,
  TableWithToolbar,
  Notification,
  FixedLoader,
} from '../../../../components'
import { getDisplayName, getFromNowDate } from '../../../../common/utils'
import { Table } from '../../../../components/Datatable'
import IssueModal from './IssueModal'
import PermissionsGate from '../../../../../app/components/PermissionsGate'

const Page = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

const TopButtons = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 calc(${th('gridUnit')}* 2);
  width: 100%;
`

const Content = styled.span`
  line-height: 5px;
`

const columns = [
  {
    cellRenderer: () => ({ cellData }) => (
      <span title={cellData}>{cellData}</span>
    ),
    dataKey: 'title',
    disableSort: true,
    label: 'Title',
  },
  {
    cellRenderer: () => ({ cellData }) => (
      <Content>
        <div
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{
            __html: `${cellData && cellData.substring(0, 120)} ...`,
          }}
          title="Click to see full content"
        />
      </Content>
    ),
    dataKey: 'content',
    disableSort: true,
    label: 'Description',
  },
  {
    cellRenderer: () => ({ cellData, rowData }) => (
      <span title={cellData}>
        {getFromNowDate(cellData)} by {getDisplayName(rowData.user || {})}
      </span>
    ),
    dataKey: 'created',
    disableSort: true,
    label: 'Created',
    width: 600,
  },
  {
    cellRenderer: () => ({ cellData }) => (
      <span title={cellData}>{getFromNowDate(cellData)} </span>
    ),
    dynamicHeight: true,
    dataKey: 'updated',
    disableSort: true,
    width: 600,
    label: 'Last Updated',
  },
  {
    cellRenderer: () => ({ cellData }) => (
      <span title={cellData}>
        {cellData.charAt(0).toUpperCase() + cellData.slice(1)}
      </span>
    ),
    dynamicHeight: true,
    dataKey: 'status',
    disableSort: true,
    width: 300,
    label: 'Status',
  },
]

const IssueTracker = ({ bookComponentId, organizationId }) => {
  const [hideClosed, setHideClosed] = useState(true)

  const [updateIssue, { loading: updateingIssue }] = useMutation(
    query.UPDATE_ISSUE,
  )

  const [createComment] = useMutation(query.NEW_COMMENT)

  const [issue, setIssue] = useState({})

  const [modalIsOpen, setIsOpen] = useState(false)
  const [selectedRows, setSelectedRows] = useState([])
  const [criterias, setCriterias] = useState([])

  const updateStatus = async status => {
    await updateIssue({
      refetchQueries: [getOperationName(query.GET_VENDOR_ISSUESDT)],

      variables: {
        id: selectedRows,
        status,
      },
    })
      .then(res => {
        Notification({
          type: 'success',
          message: 'Issues updated',
        })

        setSelectedRows([])
        setIsOpen(false)
      })
      .catch(res => {
        Notification({
          type: 'danger',
          message: 'Issues could not be updated ',
        })
      })

    const verb = status === 'open' ? 'opened' : 'closed'
    const content = `Issue ${verb}`

    await Promise.all(
      selectedRows.map(id =>
        createComment({
          variables: {
            type: 'bot',
            issueId: id,
            content,
          },
        }),
      ),
    )
  }

  const getCheckedItems = checkedValues => {
    const allItems = checkedValues.loadedData
    const checkedItems = checkedValues.selectedRows
    const filtered = allItems.filter(x => checkedItems.includes(x.id))
    const IssueStatuses = [...new Set(filtered.map(item => item.status))]

    return IssueStatuses
  }

  const CloseIssue = checkedValues => {
    const IssueStatuses = getCheckedItems(checkedValues)

    return (
      <PermissionsGate scopes={['close-issue']}>
        <Button
          disabled={
            !(IssueStatuses[0] === 'open' && IssueStatuses.length === 1)
          }
          key="1"
          onClick={() => updateStatus('closed')}
          outlined
          status="primary"
        >
          Close Issue
        </Button>
      </PermissionsGate>
    )
  }

  const OpenIssue = checkedValues => {
    const IssueStatuses = getCheckedItems(checkedValues)

    return (
      <PermissionsGate scopes={['create-issue']}>
        <Button
          disabled={
            !(IssueStatuses[0] === 'closed' && IssueStatuses.length === 1)
          }
          key="1"
          onClick={() => updateStatus('open')}
          outlined
          status="primary"
        >
          Reopen Issue
        </Button>
      </PermissionsGate>
    )
  }

  useEffect(() => {
    if (hideClosed) {
      setCriterias([
        {
          field: 'objectId',
          operator: { eq: bookComponentId },
        },
        {
          field: 'status',
          operator: { eq: 'open' },
        },
      ])
    } else {
      setCriterias([
        {
          field: 'objectId',
          operator: { eq: bookComponentId },
        },
      ])
    }
  }, [hideClosed])

  const queryVariables = {
    input: {
      skip: 0,
      sort: { direction: 'desc', field: ['created'] },
      take: 10,
      search: {
        criteria: criterias,
      },
    },
  }

  const { loading } = useQuery(query.GET_VENDOR_ISSUESDT, {
    variables: queryVariables,
  })

  if (loading) return <FixedLoader title="Loading data" />
  return (
    <Page>
      <br />
      <TopButtons pushRightAfter={1}>
        <PermissionsGate scopes={['create-issue']}>
          <Button
            disabled={updateingIssue}
            onClick={e => {
              setIssue({})
              setIsOpen(true)
            }}
            outlined
            status="primary"
          >
            New Issue
          </Button>
        </PermissionsGate>
        <Button
          disabled={updateingIssue}
          onClick={() => {
            setHideClosed(!hideClosed)
          }}
          outlined
          status="primary"
        >
          {hideClosed ? 'Show' : 'Hide'} Closed
        </Button>
      </TopButtons>

      <TableWithToolbar
        query={query.GET_VENDOR_ISSUESDT}
        selectedRows={selectedRows}
        target="getIssuesDatatable"
        tools={{
          right: [],
          left: [CloseIssue, OpenIssue],
        }}
        variables={queryVariables}
      >
        {({
          checkboxColumn,
          selectedRows: inerSelectedRows,
          setSelectedRows: inerSetSelctedRows,
          loadedData,
          count,
        }) => {
          useEffect(() => {
            setSelectedRows(inerSelectedRows)
          }, [inerSelectedRows])

          return (
            <Table
              columns={[checkboxColumn].concat(columns)}
              count={count}
              headerHeight={50}
              key="issues"
              loadedData={loadedData}
              noDataMessage="No issues created yet"
              onColumnClick={props => {
                if (props.dataKey === 'id') props.event.stopPropagation()
              }}
              onRowClick={({ rowData, event, index }) => {
                setIssue(rowData)
                setIsOpen(true)
              }}
              rowHeight={40}
              selectedRows={selectedRows}
              setSelectedRows={inerSetSelctedRows}
              target="getIssuesDatatable"
              variables={queryVariables}
            />
          )
        }}
      </TableWithToolbar>

      <IssueModal
        bookComponentId={bookComponentId}
        issue={issue}
        modalIsOpen={modalIsOpen}
        organizationId={organizationId}
        setIsOpen={setIsOpen}
      />
    </Page>
  )
}

export default IssueTracker
