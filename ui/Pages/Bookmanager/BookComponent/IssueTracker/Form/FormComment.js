/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useImperativeHandle, useState } from 'react'
import { Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { withFormik, Form } from 'formik'
import { isEmpty } from 'lodash'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'
import { Button } from '../../../../../components'
import useUploadFile from '../../../../../../app/useUploadFile'
import IssueComments from './IssueComments'
import PermissionsGate from '../../../../../../app/components/PermissionsGate'
import { EditorMentions } from '../../../../../components/wax'

const AlignStart = styled.div`
  flex-basis: 100%;
  padding: calc(${th('gridUnit')} / 2) 0;
`

const AlignEnd = styled.div`
  align-self: flex-start;
  padding: calc(${th('gridUnit')} / 2) 0;
`

const Inline = styled.div`
  display: flex;
  flex-direction: row;
`

const FileWrapper = styled.div`
  display: flex;
`

const CommentsBlock = styled.div`
  display: ${props => (props.canComment ? 'block' : 'none')};
`

const Label = styled.p`
  font-family: ${th('fontInterface')};
  font-size: 12pt;
  font-weight: 600;
  margin-top: 0;
`

const Basic = ({
  submitForm,
  handleSubmit,
  values,
  validateForm,
  formikFormRef,
  setFieldValue,
  usersData,
  issue,
  loading,
  creatingComment,
  createComment,
}) => {
  const { dispatch, getInputProps, getRootProps, fileList } = useUploadFile({
    validationFn: files => {
      return files
    },
  })

  const [key, setKey] = useState(uuid())

  useImperativeHandle(formikFormRef, () => ({
    triggerSave: async () => {
      submitForm()
      const isValid = await validateForm()

      if (isEmpty(isValid)) {
        const val = {
          ...values,
          files: fileList.map(fileVersion => ({
            data: fileVersion,
          })),
        }

        fileList &&
          fileList.map(fileVersion =>
            dispatch({
              payload: {
                name: fileVersion.name,
              },
              type: 'remove',
            }),
          )

        return val
      }

      return false
    },
  }))

  const saveComment = async () => {
    if (values.comment !== '') {
      await createComment({
        variables: {
          issueId: issue.id,
          content: values.comment,
          type: 'comment',
          files: fileList.map(fileVersion => ({
            data: fileVersion,
          })),
        },
      }).catch(res => {
        console.error(res)

        Notification({
          message: 'Could not create comment',
          type: 'danger',
          title: 'Error',
        })
      })

      setFieldValue('comment', '')
      setKey(uuid())

      fileList &&
        fileList.map(fileVersion =>
          dispatch({
            payload: {
              name: fileVersion.name,
            },
            type: 'remove',
          }),
        )
    }
  }

  return (
    <Form onSubmit={handleSubmit}>
      {issue.id && (
        <CommentsBlock canComment={issue?.id}>
          <Label>Comments</Label>
          <IssueComments issue={issue} />
          <PermissionsGate scopes={['comment-on-issue']}>
            <EditorMentions
              field={{
                setFieldValue,
                name: 'comment',
                value: values.comment,
              }}
              id="comment-editor"
              key={key}
              mentionsList={usersData}
            />

            <Inline>
              <AlignStart>
                <div>
                  {fileList?.length > 0 &&
                    fileList.map(fileVersion => (
                      <FileWrapper key={fileVersion.name}>
                        {fileVersion.name}
                        {!creatingComment && (
                          <Icon
                            color="red"
                            onClick={() => {
                              dispatch({
                                payload: {
                                  name: fileVersion.name,
                                },
                                type: 'remove',
                              })
                            }}
                            style={{ cursor: 'pointer' }}
                          >
                            x
                          </Icon>
                        )}
                      </FileWrapper>
                    ))}
                </div>
              </AlignStart>
              <AlignEnd>
                <span
                  {...getRootProps({
                    className: 'dropzone',
                    accept:
                      '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    multiple: true,
                  })}
                >
                  <input {...getInputProps({ multiple: true })} type="file" />
                  <Button
                    disabled={creatingComment}
                    icon="paperclip"
                    outline
                    title="Attach Files"
                  />
                </span>
              </AlignEnd>
            </Inline>
            <Button
              disabled={loading}
              loading={creatingComment}
              onClick={saveComment}
              status="primary"
            >
              Comment
            </Button>
          </PermissionsGate>
        </CommentsBlock>
      )}
    </Form>
  )
}

const FormComments = withFormik({
  handleSubmit: (values, { setSubmitting }) => {
    setSubmitting(false)
  },
  mapPropsToValues: values => ({
    comment: values?.comment || '',
  }),

  validate: values => {
    const errors = {}

    if (!values.comment) {
      errors.content = 'Description is required'
    }

    return errors
  },
})(Basic)

export default FormComments
