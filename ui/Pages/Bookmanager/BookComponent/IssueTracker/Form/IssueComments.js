/* eslint-disable no-console */
import React, { useEffect } from 'react'
import { useQuery } from '@apollo/client'
import query from '../../../graphql'
import { Comment } from '../../../../../components'
import { getDisplayName } from '../../../../../common/utils'

const IssueComments = ({ issue }) => {
  let comments = []

  const { loading, data: dataCm, subscribeToMore } = useQuery(
    query.GET_COMMENTS,
    {
      variables: {
        issueId: issue?.id,
      },
    },
  )

  // eslint-disable-next-line no-shadow
  const subscribeToNewMessages = (subscribeToMore, issueId) => {
    subscribeToMore({
      document: query.COMMENTS_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev
        const { commentCreated } = subscriptionData.data

        const exists = prev.getComments.findIndex(
          ({ id }) => id === commentCreated.id,
        )

        if (exists > -1) return prev

        return {
          getComments: [...prev.getComments, commentCreated],
        }
      },
      variables: { issueId },
    })
  }

  useEffect(() => {
    subscribeToNewMessages(subscribeToMore, issue.id)
  })

  comments = dataCm?.getComments || []
  if (loading) return 'loading'
  return comments.map((item, index) => {
    return (
      <Comment
        date={item.created}
        key={`${index + 1}_comment`}
        name={getDisplayName(item.user)}
        username={item.user.username}
        {...item}
      >
        <>
          {/* eslint-disable-next-line react/no-danger */}
          <div dangerouslySetInnerHTML={{ __html: item.content }} />
        </>
      </Comment>
    )
  })
}

export default IssueComments
