/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useImperativeHandle } from 'react'
import { ErrorText, Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { withFormik, Form, Field, ErrorMessage } from 'formik'
import { isEmpty } from 'lodash'
import styled from 'styled-components'
import { TextFieldComponent, Button, Link } from '../../../../../components'
import useUploadFile from '../../../../../../app/useUploadFile'
import { getDownloadLink } from '../../../../../../app/utilsConfig'
import { EditorMentions } from '../../../../../components/wax'

const Block = styled.div`
  display: block;
`

const AlignStart = styled.div`
  flex-basis: 100%;
  padding: calc(${th('gridUnit')} / 2) 0;
`

const AlignEnd = styled.div`
  align-self: flex-start;
  padding: calc(${th('gridUnit')} / 2) 0;
`

const Inline = styled.div`
  display: flex;
  flex-direction: row;
`

const FileWrapper = styled.div`
  display: flex;
`

const File = styled.span`
  padding-left: 8px;
  padding-right: 8px;
`

const Basic = ({
  submitForm,
  handleSubmit,
  values,
  validateForm,
  formikFormRef,
  setFieldValue,
  readOnly,
  usersData,
}) => {
  const { dispatch, getInputProps, getRootProps, fileList } = useUploadFile({
    validationFn: files => {
      return files
    },
  })

  useImperativeHandle(formikFormRef, () => ({
    triggerSave: async () => {
      submitForm()
      const isValid = await validateForm()

      if (isEmpty(isValid)) {
        const val = {
          ...values,
          files: fileList.map(fileVersion => ({
            data: fileVersion,
          })),
        }

        fileList &&
          fileList.map(fileVersion =>
            dispatch({
              payload: {
                name: fileVersion.name,
              },
              type: 'remove',
            }),
          )

        return val
      }

      return false
    },
  }))

  return (
    <Form onSubmit={handleSubmit}>
      <Field
        component={TextFieldComponent}
        label="Title"
        name="title"
        readOnly={readOnly}
      />
      <Block>
        <EditorMentions
          field={{ setFieldValue, name: 'content', value: values.content }}
          id="issue-editor"
          label="Description"
          mentionsList={usersData}
          readOnly={readOnly}
        />

        <div>
          {values.files.map(fileVersion => (
            <File key={fileVersion.id}>
              <Link to={getDownloadLink(fileVersion.id)}>
                {fileVersion.name}
              </Link>
            </File>
          ))}
        </div>
        <Inline>
          <AlignStart>
            <ErrorMessage component={ErrorText} name="content" />
            <div>
              {fileList?.length > 0 &&
                fileList.map(fileVersion => (
                  <FileWrapper key={fileVersion.name}>
                    {fileVersion.name}
                    <Icon
                      color="red"
                      onClick={() => {
                        dispatch({
                          payload: {
                            name: fileVersion.name,
                          },
                          type: 'remove',
                        })
                      }}
                      style={{ cursor: 'pointer' }}
                    >
                      x
                    </Icon>
                  </FileWrapper>
                ))}
            </div>
          </AlignStart>
          <AlignEnd>
            {!values.id && (
              <span
                {...getRootProps({
                  className: 'dropzone',
                  accept:
                    '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                  multiple: true,
                })}
              >
                <input {...getInputProps({ multiple: true })} type="file" />
                <Button icon="paperclip" outline title="Attach Files" />
              </span>
            )}
          </AlignEnd>
        </Inline>
      </Block>
    </Form>
  )
}

const FormIssue = withFormik({
  handleSubmit: (values, { setSubmitting }) => {
    setSubmitting(false)
  },
  mapPropsToValues: values => ({
    id: values.values.id || null,
    title: values.values.title || '',
    content: values.values.content || '',
    files: values.values.files || [],
  }),

  validate: values => {
    const errors = {}

    if (!values.title) {
      errors.title = 'Title is required'
    }

    if (!values.content) {
      errors.content = 'Description is required'
    }

    return errors
  },
})(Basic)

export default FormIssue
