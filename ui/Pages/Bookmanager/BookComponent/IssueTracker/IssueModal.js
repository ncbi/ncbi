import React, { useRef } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { th, darken } from '@pubsweet/ui-toolkit'
import { useMutation, useQuery } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'
import query from '../../graphql'

import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Notification,
} from '../../../../components'
import { FormIssue, FormComment } from './Form'
import PermissionsGate from '../../../../../app/components/PermissionsGate'

const IssueTitle = styled.span`
  color: ${darken('colorpPrimary', 0.5)};
  font-family: ${th('fontInterface')};
`

const Root = styled.div`
  font-family: ${th('fontInterface')};
`

const StyledFooter = ({ children }) => {
  return <ModalFooter>{children}</ModalFooter>
}

const IssueModal = ({
  bookComponentId,
  organizationId,
  issue,
  modalIsOpen,
  setIsOpen,
}) => {
  const modalRef = useRef(null)

  const { data: allusers, loading: loadingUsers } = useQuery(
    query.GET_ALL_USERS,
    {
      variables: {
        input: {
          sort: {
            direction: 'asc',
            field: ['username', 'givenName', 'lastName'],
          },
        },
      },
    },
  )

  const { data: orgDt, loading: loadingOrg } = useQuery(
    query.GET_ORGANIZATION,
    {
      variables: { id: organizationId },
    },
  )

  const { data: globalTeam, loading: loadingTeams } = useQuery(
    query.GET_GLOBAL_TEAMS,
    {
      variables: { where: { global: true } },
    },
  )

  const usersList = loadingUsers
    ? []
    : [
        ...allusers?.searchUsers.results.filter(
          x =>
            x.auth.isSysAdmin ||
            x.auth.isPDF2XMLVendor ||
            x.auth.orgAdminForOrganisation.some(org => org === organizationId),
        ),
      ]

  const roles =
    loadingOrg || loadingTeams
      ? []
      : [
          ...orgDt?.getOrganisation.teams.filter(
            x =>
              x.role === 'orgAdmin' ||
              x.role === 'sysAdmin' ||
              x.role === 'PDF2XMLVendor',
          ),
          globalTeam?.teams.find(x => x.role === 'pdf2xmlVendor'),
        ]?.map(({ id, role, name }) => ({
          username: `${name}s`,
          id,
          name,
          role,
        })) || []

  const [createIssue, { loading: createingIssue }] = useMutation(
    query.NEW_ISSUE,
  )

  const [updateIssue, { loading: updateingIssue }] = useMutation(
    query.UPDATE_ISSUE,
  )

  const [createComment, { loading: creatingComment }] = useMutation(
    query.NEW_COMMENT,
  )

  return (
    <Root>
      <Modal isOpen={modalIsOpen}>
        <ModalHeader hideModal={() => setIsOpen(false)}>
          <IssueTitle>{issue?.title || ' New Issue '}</IssueTitle>
        </ModalHeader>
        <ModalBody height="60vh">
          <FormIssue
            formikFormRef={modalRef}
            readOnly={issue.id}
            usersData={[...roles, ...usersList]}
            values={issue}
          />

          <FormComment
            createComment={createComment}
            creatingComment={creatingComment}
            issue={issue}
            loading={updateingIssue || creatingComment}
            usersData={[...roles, ...usersList]}
          />
        </ModalBody>
        <StyledFooter>
          {issue.id && issue.status === 'open' && (
            <PermissionsGate scopes={['close-issue']}>
              <Button
                data-test-id="open-issue"
                disabled={createingIssue || updateingIssue || creatingComment}
                onClick={async () => {
                  const formData = await modalRef.current.triggerSave()

                  if (formData) {
                    await updateIssue({
                      refetchQueries: [
                        getOperationName(query.GET_VENDOR_ISSUESDT),
                      ],

                      variables: {
                        id: [issue.id],
                        status: 'closed',
                      },
                    })
                      .then(res => {
                        Notification({
                          type: 'success',
                          message: 'Issue closed',
                        })

                        setIsOpen(false)
                      })
                      .catch(res => {
                        Notification({
                          type: 'danger',
                          message: 'Issue could not be updated ',
                        })
                      })

                    await createComment({
                      refetchQueries: [getOperationName(query.GET_COMMENTS)],
                      variables: {
                        type: 'bot',
                        issueId: issue.id,
                        content: 'Issue closed',
                      },
                    }).catch(res => {
                      console.error(res)

                      Notification({
                        message: 'Could not create bot comment',
                        type: 'danger',
                        title: 'Error',
                      })
                    })
                  }
                }}
                status="primary"
              >
                Close Issue
              </Button>
            </PermissionsGate>
          )}
          {!issue.id && (
            <Button
              data-test-id="open-issue"
              disabled={createingIssue || updateingIssue || creatingComment}
              onClick={async () => {
                const formData = await modalRef.current.triggerSave()

                if (formData) {
                  await createIssue({
                    refetchQueries: [
                      getOperationName(query.GET_VENDOR_ISSUESDT),
                    ],
                    variables: {
                      objectId: bookComponentId,
                      ...formData,
                    },
                  })
                    .then(res => {
                      Notification({
                        type: 'success',
                        message: 'Issue saved successfully',
                      })

                      setIsOpen(false)
                    })
                    .catch(res => {
                      Notification({
                        type: 'danger',
                        message: 'Issue could not be saved ',
                      })
                    })
                }
              }}
              status="primary"
            >
              Open Issue
            </Button>
          )}
          <Button
            data-test-id="cancel-issue"
            onClick={() => setIsOpen(false)}
            outlined
            status="primary"
          >
            Cancel
          </Button>
        </StyledFooter>
      </Modal>
    </Root>
  )
}

IssueModal.propTypes = {
  bookComponentId: PropTypes.string,
  issue: PropTypes.node,
  organizationId: PropTypes.string.isRequired,
  modalIsOpen: PropTypes.bool,
  setIsOpen: PropTypes.func,
}

IssueModal.defaultProps = {
  bookComponentId: null,
  issue: null,
  modalIsOpen: false,
  setIsOpen: null,
}

export default IssueModal
