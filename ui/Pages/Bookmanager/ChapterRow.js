import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import moment from 'moment'
import { Link as UILink } from 'react-router-dom'
import { Tooltip } from 'antd'

import { grid, th } from '@pubsweet/ui-toolkit'

import UIStatus from '../../components/Status'
import UITOCLabel from '../../components/common/TOCLabel'
import { getLongDate } from '../../common/utils'

// #region styled
const Wrapper = styled.div`
  position: relative;
`

const Overlay = styled.div`
  height: 100%;
  position: absolute;
  width: 100%;
`

const Link = styled(UILink)`
  color: ${th('colorText')};
  cursor: pointer;
  text-decoration: none;
  text-transform: none;
`

const Left = styled.div`
  align-items: start;
  display: flex;
  flex-grow: 1;
  justify-content: space-between;
  margin-right: ${grid(4)};
`

const Up = styled.div`
  padding: ${grid(0.5)};
`

const Title = styled.div`
  display: inline;

  ${props =>
    props.isPart &&
    css`
      font-weight: bold;
    `}
`

const Down = styled.div`
  background: lilac;
  display: flex;
  font-size: ${th('fontSizeBaseSmall')};
  justify-content: space-between;
  padding: ${grid(1)} ${grid(0.5)};

  > div > span:not(:last-child):not(:nth-child(2)) {
    margin-right: ${grid(0.5)};
  }
`

const Tag = styled.span`
  background-color: gainsboro;
  border-radius: 3px;
  padding: 0 ${grid(1)};

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${props =>
    props.isPart &&
    css`
      background-color: thistle;
      margin-right: ${grid(1)};
    `}
`

const Padded = styled.div`
  border: 1px solid ${th('colorPrimary')};
  border-radius: 3px;
  display: inline-flex;
  margin: 0 ${grid(2)};
  padding: 0 ${grid(0.5)};
`

const AssigneeError = styled.span`
  border-left: 1px solid gray;
  padding: 0 ${grid(0.5)};
`

const Status = styled(UIStatus)`
  display: inline-flex;
  padding: 0 ${grid(0.5)} 0 0;
`

const VendorStatus = styled(Status)`
  border: 1px solid ${th('colorBorder')};
  border-radius: 3px;
  padding: 0 ${grid(1)};
`

const TOCLabel = styled(UITOCLabel)`
  border-radius: 3px;
  display: inline-flex;
  font-weight: normal;
`

const Right = styled.div`
  align-items: start;
  display: flex;
  font-size: ${th('fontSizeBaseSmall')};
  justify-content: right;
`

const RightItem = styled.div`
  padding: 0 ${grid(1)};
  width: 170px;
`

const RightItemLabel = styled.div`
  font-weight: bold;
`

const RightItemValue = styled.div``

const TagList = styled.span`
  /* stylelint-disable-next-line no-descending-specificity */
  > span:not(:last-child),
  > div:not(:last-child) {
    margin-right: ${grid(1)};
  }
`

const TagsWrapper = styled.div`
  align-items: center;
  display: inline-flex;
`

const SubTitle = styled.div`
  font-size: ${th('fontSizeBaseVerySmall')};
`
// #endregion styled

const ChapterRow = props => {
  const {
    assigneeErrors,
    chapterNumber,
    className,
    filename,
    hasIssues,
    hasWarnings,
    href,
    isApproved,
    isDropTarget,
    isDuplicate,
    isHiddenInTOC,
    isMissingFromTOC,
    isPart,
    isUnderRevision,
    lastPublished,
    lastUpdated,
    showAssigneeErrors,
    status: chapterStatus,
    subTitle,
    title,
    version,
  } = props

  const showDetails = !isPart || !!filename

  const makeDate = date => (date ? `${moment(date).fromNow()}` : '-')
  const makeFullDate = date => (date ? `${getLongDate(date)}` : '-')

  const rightItemData = [
    {
      key: 'lastUpdated',
      label: 'Last Updated',
      value: makeDate(lastUpdated),
      title: makeFullDate(lastUpdated),
    },
    {
      key: 'lastPublished',
      label: 'Last Published',
      value: makeDate(lastPublished),
      title: makeFullDate(lastPublished),
    },
    {
      key: 'filename',
      label: 'Filename',
      value: filename,
    },
  ]

  return (
    <Link to={href}>
      <Wrapper className={className}>
        {/* If row is the drop target, add an invisible overlay so that the drop indicator behaves correctly */}
        {isDropTarget && <Overlay />}

        <Up>
          {isPart && <Tag isPart>P</Tag>}

          <Title isPart={isPart}>
            {/* <span>{`${props.level} -- `}</span> */}
            {chapterNumber && `${chapterNumber}. `}
            {/* <span>{`${props.id} ~~ `}</span> */}
            {/* eslint-disable-next-line react/no-danger */}
            <span dangerouslySetInnerHTML={{ __html: title }} />
          </Title>

          <SubTitle>{subTitle}</SubTitle>
        </Up>

        {showDetails && (
          <Down>
            <Left>
              <TagsWrapper>
                <Tag>V{version}</Tag>

                <Padded>
                  <Status
                    approved={isApproved}
                    revise={isUnderRevision}
                    status={chapterStatus}
                  />
                  {showAssigneeErrors &&
                    assigneeErrors &&
                    assigneeErrors.map(assignee => (
                      <AssigneeError key={assignee}>{assignee}</AssigneeError>
                    ))}
                </Padded>

                <TagList>
                  {isDuplicate && <TOCLabel duplicate />}

                  <TOCLabel
                    excluded={isHiddenInTOC}
                    missing={isMissingFromTOC}
                  />
                </TagList>
              </TagsWrapper>

              <TagsWrapper>
                <TagList>
                  {hasWarnings && <VendorStatus status="query" />}
                  {hasIssues && <VendorStatus status="vendor-issues" />}
                </TagList>
              </TagsWrapper>
            </Left>

            <Right onClick={e => e.preventDefault()}>
              {rightItemData.map(i => (
                <Tooltip
                  key={i.key}
                  placement="left"
                  title={i.title}
                  trigger="click"
                >
                  <RightItem>
                    <RightItemLabel>{i.label}</RightItemLabel>
                    <RightItemValue>{i.value}</RightItemValue>
                  </RightItem>
                </Tooltip>
              ))}
            </Right>
          </Down>
        )}
      </Wrapper>
    </Link>
  )
}

ChapterRow.propTypes = {
  assigneeErrors: PropTypes.arrayOf(PropTypes.string),
  chapterNumber: PropTypes.number,
  filename: PropTypes.string,
  hasIssues: PropTypes.bool.isRequired,
  hasWarnings: PropTypes.bool.isRequired,
  href: PropTypes.string.isRequired,
  isApproved: PropTypes.bool.isRequired,
  isDropTarget: PropTypes.bool.isRequired,
  isDuplicate: PropTypes.bool.isRequired,
  isHiddenInTOC: PropTypes.bool.isRequired,
  isMissingFromTOC: PropTypes.bool.isRequired,
  isPart: PropTypes.bool.isRequired,
  isUnderRevision: PropTypes.bool.isRequired,
  lastPublished: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.string,
  ]),
  lastUpdated: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.string,
  ]).isRequired,
  showAssigneeErrors: PropTypes.bool.isRequired,
  status: PropTypes.string.isRequired,
  subTitle: PropTypes.string,
  title: PropTypes.string.isRequired,
  version: PropTypes.number.isRequired,
}

ChapterRow.defaultProps = {
  assigneeErrors: [],
  chapterNumber: null,
  filename: null,
  lastPublished: null,
  subTitle: null,
}

export default ChapterRow
