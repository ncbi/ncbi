import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import startCase from 'lodash/startCase'
import moment from 'moment'

const ItalicText = styled.span`
  font-style: italic;
`

const BoldText = styled.span`
  font-weight: bold;
`

const StyledList = styled.ul`
  margin-block-end: 0;
  margin-block-start: 0;
  padding-inline-start: 24px;
`

const FilterSummary = ({ filters, className }) => {
  const renderFilterValue = filter => {
    if (filter.name === 'division') {
      return (
        <span>
          is <BoldText>{filter.value.label}</BoldText>
        </span>
      )
    }

    if (filter.name === 'status') {
      if (filter.value.length === 1) {
        return (
          <span>
            is <BoldText>{filter.value[0].label}</BoldText>
          </span>
        )
      }

      if (filter.value.length > 1) {
        const statusLabels = filter.value.map(val => val.label)
        return (
          <span>
            is <BoldText>{statusLabels.join(' or ')}</BoldText>
          </span>
        )
      }
    } else if (
      ['lastUpdated', 'lastPublished'].includes(filter.name) &&
      filter.value
    ) {
      const startDate = moment(filter.value[0])
      const endDate = moment(filter.value[1])
      const formattedStartDate = startDate.format('MMM D, YYYY')
      const formattedEndDate = endDate.format('MMM D, YYYY')
      return (
        <span>
          between <BoldText>{formattedStartDate}</BoldText> to{' '}
          <BoldText>{formattedEndDate}</BoldText>
        </span>
      )
    }

    return null
  }

  return (
    <div className={className}>
      <span>Filtered by:</span>
      <StyledList>
        {filters.map((filter, index) => (
          <li key={filter.name}>
            <ItalicText>{startCase(filter.name)}</ItalicText>{' '}
            {renderFilterValue(filter)}
          </li>
        ))}
      </StyledList>
    </div>
  )
}

FilterSummary.propTypes = {
  filters: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([
        PropTypes.shape({
          value: PropTypes.string.isRequired,
          label: PropTypes.string.isRequired,
        }),
        PropTypes.arrayOf(
          PropTypes.shape({
            value: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired,
          }),
        ),
      ]).isRequired,
    }),
  ).isRequired,
}

export default FilterSummary
