/* eslint-disable prefer-destructuring */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React from 'react'
import styled from 'styled-components'
import moment from 'moment'
import { get, uniq } from 'lodash'
import { th, grid } from '@pubsweet/ui-toolkit'
import Status from '../../../components/Status'
import Checkbox from '../../../components/common/Checkbox'
import { TOCLabel, Tag } from '../../../components/common'
import { getLongDate } from '../../../common/utils'

const Wrapper = styled.div`
  opacity: ${props => (props.hide ? 0 : 1)};
  padding-left: ${props => props.space};
`

const BookComponentStyled = styled.div`
  background-color: ${props =>
    props.checked || props.highlight ? th('colorTextPlaceholder') : 'none'};

  border-bottom: 1px solid ${th('colorTextPlaceholder')};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 50px;
  opacity: ${props => props.opacity};
  padding: 8px;
  padding-left: ${props => (props.marginChildren ? props.marginChildren : '0')};

  :hover {
    cursor: pointer;
  }
`

const Separator = styled.div`
  border-right: 2px solid #eee;
  height: 35px;
  margin: 0 ${grid(1)};
`

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  margin-left: 8px;
  text-align: left;
`

const Flex = styled.div`
  align-items: center;
  display: flex;
`

const Column = styled.div`
  align-items: center;
  display: flex;
`

const LeftAlign = styled.div`
  display: flex;
  flex-grow: 1;
`

const RightAlign = styled.div`
  display: flex;
  flex: 1 1 auto;
  justify-content: space-evenly;
`

const CenterSpan = styled.span`
  align-items: center;
  display: flex;
  margin: 0 8px;
`

const Version = styled.span`
  background: ${th('colorTextPlaceholder')};
  border-radius: ${th('borderRadius')};
  font-size: ${th('fontSizeBaseSmall')};
  margin-left: ${th('gridUnit')};
  margin-right: ${th('gridUnit')};
  padding: calc(${th('gridUnit')} / 2);
`

const Label = styled.span`
  align-items: center;
  background: ${props =>
    props.reverseColor ? th('colorText') : 'transparent'};
  border-radius: 16px;
  color: ${props =>
    props.reverseColor ? th('colorTextReverse') : th('colorText')};
  display: flex;
  font-size: ${th('fontSizeBaseSmall')};
  height: min-content;
  justify-content: center;
  padding: 0 8px;

  span {
    font-weight: 600;
    padding: 0 4px;
  }
`

const ColumnTitle = styled.div`
  display: flex;
  flex: 1 1;
  flex-direction: column;
  font-weight: ${props => (props.type === 'part' ? 700 : 400)};
  justify-content: center;
  margin: 0px;
  text-align: left;
`

const FlexRow = styled.div`
  display: flex;
  gap: 4px;
`

const ChapterNumber = styled.div`
  margin: 0 8px 0px 0px;
`

const getLatestFileStatus = (sourceFiles, convertedFiles) => {
  const sortedSource = sourceFiles.sort((a, b) => {
    return new Date(b.created) - new Date(a.created)
  })

  const sortedConverted = convertedFiles.sort((a, b) => {
    return new Date(b.created) - new Date(a.created)
  })

  let lastSource = null
  let lastConverted = null

  if (sortedSource.length) {
    lastSource = sortedSource[0]
  }

  if (sortedConverted.length) {
    lastConverted = sortedConverted[0]
  }

  if (lastSource && lastConverted) {
    return new Date(lastSource.created) - new Date(lastConverted.created) < 0
      ? lastConverted.status
      : lastSource.status
  }

  if (lastSource && !lastConverted) {
    return lastSource.status
  }

  if (!lastSource && lastConverted) {
    return lastConverted.status
  }

  return ''
}

const RowElement = ({
  book,
  divisionName,
  row,
  index,
  selectedRows = [],
  check,
  unCheck,
  onRowClick,
  marginChildren,
  children,
  collapsed,
  setCollapsed,
  disableCheck,
  isOver,
  counter,
  style,
  highlight,
  updateOrder,
}) => {
  let isChecked = !!(
    selectedRows.length && selectedRows.find(x => x.id === row.id)
  )

  let reviewData = {}

  if (row.status === 'in-review') {
    if (row.reviewsResponse) {
      const approved = row.reviewsResponse.filter(
        item => item.decision === 'approve',
      ).length

      const revise = row.reviewsResponse.filter(
        item => item.decision === 'requestChanges',
      ).length

      reviewData = { approved, revise }
    }
  }

  const showRowDetails =
    row.componentType !== 'part' || !!row?.metadata?.filename || ''

  const getErrorAsignees = errors => {
    const asignes = errors
      .filter(x => x.severity === 'error')
      .map(err => err.assignee)
      .sort()

    const unique = (uniq(asignes) || []).filter(
      x => x.toLowerCase() !== 'unknown',
    )

    if (unique.length) {
      return (
        <>
          <Separator /> {unique.join(' | ')}
        </>
      )
    }

    return ''
  }

  const getWarnings = errors => {
    const warnings = errors.filter(err => err.severity === 'query')

    return warnings.length ? <Status status="query" /> : ''
  }

  let showTOCLabel = false

  const tocStatusShow = [
    'conversion-errors',
    'loading-preview',
    'loading-errors',
    'tagging-errors',
    'preview',
    'published',
  ].includes(row.status)

  if (divisionName === 'body' && tocStatusShow) {
    if (book?.settings?.toc?.order_chapters_by === 'title') {
      showTOCLabel = get(row, 'title') === 'Untitled'
    } else if (
      book?.settings?.toc?.order_chapters_by !== 'manual' &&
      row.componentType !== 'part'
    ) {
      // To get here, the order_chpaters_by === 'number'
      // so we need to check the existence of the chapter_number
      // in metadata
      showTOCLabel = !get(row, 'metadata.chapter_number')
    }
  }

  const tagLabel = row.componentType === 'part' ? 'Part' : divisionName

  return (
    <Wrapper
      data-test-id={`${row.title}-wrapper`}
      data-type={row.componentType}
      hide={false}
      index={index}
    >
      <BookComponentStyled
        checked={isChecked}
        highlight={highlight}
        marginChildren={marginChildren}
      >
        <Row
          style={{
            background:
              isOver && row.componentType === 'part' ? '#efeaea8a' : 'none',
          }}
        >
          <Flex>
            <CenterSpan>
              {showRowDetails && (
                <Checkbox
                  checked={isChecked}
                  disabled={disableCheck}
                  name="checkbox"
                  onClick={e => {
                    if (isChecked) {
                      unCheck(row)
                    } else {
                      check(row)
                    }

                    isChecked = !e.target.checked
                  }}
                />
              )}
            </CenterSpan>
          </Flex>
          <ChapterNumber>
            {row.metadata?.chapter_number
              ? `${row.metadata?.chapter_number}`
              : ''}
          </ChapterNumber>
          <ColumnTitle
            data-test-id={`chapter-title-${row.title}`}
            onClick={() => onRowClick(row)}
            type={row.componentType}
          >
            <FlexRow>
              {/* eslint-disable-next-line react/no-danger */}
              <div dangerouslySetInnerHTML={{ __html: row.title || '' }} />
              <Tag color="lightgrey" label={tagLabel} size="small" />
            </FlexRow>
          </ColumnTitle>

          {counter > 1 && <TOCLabel duplicate />}
          <TOCLabel
            duplicate={false}
            excluded={row.metadata?.hideInTOC}
            missing={showTOCLabel}
          />
        </Row>
        {showRowDetails && (
          <Row onClick={() => onRowClick(row)}>
            <LeftAlign>
              {row.componentType === 'part'}
              <Column flexgrow={0}>
                <Version>V{row.versionName}</Version>{' '}
              </Column>
              <Column flexgrow={1}>
                <Status
                  approved={reviewData?.approved}
                  revise={reviewData?.revise}
                  status={row.status}
                />{' '}
                {[
                  'error',
                  'submission-errors',
                  'conversion-errors',
                  'loading-errors',
                ].includes(row.status) && getErrorAsignees(row.errors)}
                {['preview', 'pre-published', 'published'].includes(
                  row.status,
                ) && getWarnings(row.errors)}
              </Column>

              {row.status === 'in-review' &&
              getLatestFileStatus(row.sourceFiles, row.convertedFile) !==
                row.status ? (
                <Column flexgrow={1}>
                  <Status
                    status={getLatestFileStatus(
                      row.sourceFiles,
                      row.convertedFile,
                    )}
                  />{' '}
                </Column>
              ) : null}
            </LeftAlign>
            <RightAlign>
              <Column flexgrow={1}>
                <Label>
                  {row.issues?.length > 0 ? (
                    <Status status="vendor-issues" />
                  ) : (
                    ''
                  )}
                </Label>
              </Column>
              <Column flexgrow={1}>
                <Label>
                  <span>Last Updated:</span>
                  {moment(row.updated).fromNow()}
                </Label>
              </Column>
              <Column flexgrow={1} onClick={() => onRowClick(row)}>
                <CenterSpan>
                  <Label>
                    <span>Last Published:</span>
                    <span
                      title={
                        row.publishedDate ? getLongDate(row.publishedDate) : ''
                      }
                    >
                      {row.publishedDate
                        ? `${moment(row.publishedDate).fromNow()}`
                        : '-'}
                    </span>
                  </Label>
                </CenterSpan>
              </Column>
              <Column flexgrow={2}>
                <Label>
                  <span> File Name: </span>
                  {row.metadata?.filename}
                </Label>
              </Column>
            </RightAlign>
          </Row>
        )}
      </BookComponentStyled>
    </Wrapper>
  )
}

export default RowElement
