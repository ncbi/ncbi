import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid, th } from '@pubsweet/ui-toolkit'

import UINote from '../../../components/common/Note'
import UISpinner from '../../../components/common/Spinner'
import FilterSummary from './FilterSummary'
import { Checkbox } from '../../../components'
import Tree from '../Tree'

// #region styled
const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const HeaderWrapper = styled.div`
  border-bottom: 1px solid ${th('colorBorder')};
`

const Header = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin-bottom: ${grid(1)};
  padding: ${grid(1)} ${grid(2)};
`

const ResultSummary = styled.span`
  font-size: ${th('fontSizeBaseSmall')};
  font-style: italic;
  width: 50%;
`

const ResultSorting = styled.span`
  font-size: ${th('fontSizeBaseSmall')};
  text-align: right;
  width: 50%;
`

const Note = styled(UINote)`
  display: flex;
  font-size: ${th('fontSizeBase')};
  justify-content: center;
  margin: ${grid(1)};
  padding: ${grid(1)};
`

const Spinner = styled(UISpinner)`
  height: 100%;
`

const mapSortByLabel = {
  updated: 'most recently updated',
}

const Empty = styled.div`
  font-weight: bold;
  margin: 0 auto;
  padding: ${grid(4)};
`

const StyledFilterSummary = styled(FilterSummary)`
  font-size: ${th('fontSizeBaseSmall')};
`

const StyledCheckbox = styled(Checkbox)`
  padding-left: 16px;
`
// #endregion styled

const SearchResultList = props => {
  const {
    canSelect,
    loading,
    onSelectAllChange,
    onSelectionChange,
    queryParams,
    searchMetadata,
    searchResults,
    selectAll,
    selectedRows,
  } = props

  const {
    field: [sortField],
    // direction: sortOrder,
  } = queryParams.sort

  if (loading) return <Spinner label="Searching..." pageLoader />

  const { skip, take, total } = searchMetadata

  const startIndex = total === '0' ? 0 : parseInt(skip, 10) + 1

  const endIndex =
    take === '-1'
      ? total
      : parseInt(skip, 10) + parseInt(searchResults.length, 10)

  const handleCheck = nodes => {
    onSelectionChange(nodes)
  }

  const handleCheckAll = () => {
    if (selectAll) {
      onSelectAllChange(false)
    } else {
      onSelectAllChange(true)
    }
  }

  const checkedKeys = selectedRows.map(row => row.key)

  return (
    <Wrapper>
      <HeaderWrapper>
        <Note compact content="You are seeing search results" />
        <Header>
          <ResultSummary>
            Showing results {startIndex}-{endIndex} of {total}
          </ResultSummary>

          <ResultSorting>Sorted by {mapSortByLabel[sortField]}</ResultSorting>

          <div>
            {queryParams.filters.length > 0 ? (
              <StyledFilterSummary filters={queryParams.filters} />
            ) : null}
          </div>
        </Header>

        <StyledCheckbox
          checked={selectAll}
          label="Select all"
          name="selectAll"
          onClick={handleCheckAll}
        />
      </HeaderWrapper>

      {total === 0 && <Empty>There were no results for your query</Empty>}

      <Tree
        canDrag={false}
        canSelect={canSelect}
        checkedKeys={checkedKeys}
        data={searchResults}
        maxNestingLevel={1}
        onCheck={handleCheck}
      />
    </Wrapper>
  )
}

SearchResultList.propTypes = {
  selectedRows: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  ).isRequired,
  loading: PropTypes.bool.isRequired,
  searchResults: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  ),
  searchMetadata: PropTypes.shape({
    skip: PropTypes.string.isRequired,
    take: PropTypes.string.isRequired,
    total: PropTypes.string.isRequired,
  }),
  queryParams: PropTypes.shape({
    filters: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        value: PropTypes.oneOfType([
          PropTypes.shape({
            value: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired,
          }),
          PropTypes.arrayOf(
            PropTypes.shape({
              value: PropTypes.string.isRequired,
              label: PropTypes.string.isRequired,
            }),
          ),
        ]).isRequired,
      }),
    ),
    sort: PropTypes.shape({
      field: PropTypes.arrayOf(PropTypes.string),
      direction: PropTypes.string,
    }),
  }),
  onSelectAllChange: PropTypes.func.isRequired,
  onSelectionChange: PropTypes.func.isRequired,
  selectAll: PropTypes.bool.isRequired,
  canSelect: PropTypes.bool.isRequired,
}

SearchResultList.defaultProps = {
  queryParams: { filters: [] }, // Default value for queryParams
  searchMetadata: null,
  searchResults: null,
}

export default SearchResultList
