/* eslint-disable react/prop-types */
/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
import React from 'react'
import _, { cloneDeep } from 'lodash'
import moment from 'moment'

import query from './graphql'
import { Status } from '../../components'
import { getSelectedParts } from '../../common/utils'

// const PART = 'part'

export const arrayMove = (array, oldIndex, newIndex) => {
  const arr = cloneDeep(array)

  if (newIndex >= arr.length) {
    let k = newIndex - arr.length + 1

    while (k--) {
      arr.push(undefined)
    }
  }

  arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0])
  return arr // for testing
}

const getSubList = (list, allList, parentNode) => {
  const subList = []

  _.forEach(list, node => {
    subList.push({
      ...allList.find(x => x.id === node),
      children: [],
      parent: parentNode.id,
    })
  })

  return subList
}

const getChildren = (rootEl, fulllist) => {
  return (
    rootEl.bookComponents.map(bc => {
      const el = fulllist.find(x => x.id === bc)

      const { id, title, bookComponents, componentType } = el

      return { id, title, bookComponents, componentType }
    }) || []
  )
}

export const getDivisionFlatOrderedList = ({
  division,
  fullBookComponents,
  collapsed = [],
}) => {
  const flatObj = []

  division.bookComponents
    .filter(bc => fullBookComponents.map(f => f.id).includes(bc))
    .forEach((item, index1) => {
      // root elements
      const rootEl = fullBookComponents.find(x => x.id === item)

      flatObj.push({
        level: 0,
        index: index1,
        ...{
          parentId: division.id,
          parentComponentType: 'division',
          childrenLevel: getChildren(rootEl, fullBookComponents),
          // collapsed: collapsed.includes(item),
          ...rootEl,
        },
      })

      if (rootEl.bookComponents.length > 0) {
        rootEl.bookComponents.forEach((item1, indx) => {
          const elementLevel1 = fullBookComponents.find(x => x.id === item1)

          flatObj.push({
            level: 1,
            index: indx,
            ...{
              parentId: item,
              parentComponentType: rootEl.componentType,
              collapsed: collapsed.includes(item),
              childrenLevel: getChildren(elementLevel1, fullBookComponents),
              ...elementLevel1,
            },
          })

          if (elementLevel1.bookComponents.length > 0) {
            flatObj.push(
              ...elementLevel1.bookComponents.map((bcL3, i) => ({
                level: 2,
                index: i,
                parentComponentType: elementLevel1.componentType,
                parentId: elementLevel1.id,
                collapsed:
                  collapsed.includes(item1) || collapsed.includes(item),
                childrenLevel: [],
                ...fullBookComponents.find(x => x.id === bcL3),
              })),
            )
          }
        })
      }
    })

  return flatObj
}

export const getTreeList = (list, allList) => {
  const newTree = []

  // build the list
  _.forEach(list, node => {
    newTree.push({
      ...allList.find(x => x.id === node),
      children: [],
      parent: node.divisionId,
    })
  })

  // iterate to push the children
  _.forEach(newTree, node => {
    if (node.bookComponents) {
      node.children =
        cloneDeep(getSubList(node.bookComponents, allList, node)) || []

      _.forEach(node.children, nodeL1 => {
        if (nodeL1.bookComponents) {
          nodeL1.children = cloneDeep(
            getSubList(nodeL1.bookComponents, allList, nodeL1),
          )

          _.forEach(nodeL1.children, nodeL2 => {
            if (nodeL2.bookComponents) {
              nodeL2.children = cloneDeep(
                getSubList(nodeL2.bookComponents, allList, nodeL2),
              )
            }
          })
        }
      })
    }
  })

  return newTree
}

export const countComponents = ({ type, id, bookComponents }) => {
  try {
    if (!id || !bookComponents?.length || type === 'part') return 0

    let count = 0

    bookComponents.filter(x => x.id === id)

    _.forEach(bookComponents, item => {
      count += item.bookComponents?.filter(x => x === id).length
    })

    return count
  } catch {
    return 0
  }
}

export const pushCardFn = (
  bookComponent,
  index,
  sectionId,
  client,
  data,
  section,
) => {
  const dataUpdate = cloneDeep(
    client.readQuery({
      query: query.GET_BOOK_USERS,
      variables: {
        id: data.getBook.id,
        input: {
          skip: 0,
          sort: { direction: 'asc', field: ['username'] },
          take: 10,
        },
      },
    }),
  )

  // division where book compoennt is moved to
  const foundDivision = section
  const foundDivisionComponents = foundDivision.bookComponents || []

  // previous devision
  const previousDivision = data.getBook.divisions.find(
    div => div.id === bookComponent.listId,
  )

  const previousDivisionComponents = previousDivision?.bookComponents || []

  const componentId = bookComponent.row.id
  bookComponent.listId = sectionId

  const foundIndex = previousDivision.bookComponents.findIndex(
    b => b === componentId,
  )

  if (foundIndex > -1) {
    // we are removing from one section, and puting in another one
    previousDivisionComponents.splice(foundIndex, 1)
    foundDivisionComponents.push(bookComponent.row.id)
  }

  client.writeQuery({
    data: previousDivision,
    id: `Division:${previousDivision.id}`,
  })

  client.writeQuery({
    data: bookComponent.row,
    id: `BookComponent:${bookComponent.row.id}`,
  })

  client.writeQuery({
    data: foundDivision,
    id: `Division:${sectionId}`,
  })

  dataUpdate.getBook.divisions = cloneDeep(dataUpdate.getBook.divisions).map(
    div => {
      if (div.id === previousDivision.id) {
        return previousDivision
      }

      if (div.id === sectionId) {
        return foundDivision
      }

      return div
    },
  )

  const bcOrdered = []

  _.forEach(dataUpdate.getBook.divisions, item => {
    bcOrdered.push(...item.bookComponents)
  })

  const result = data.getBook.bookComponents

  dataUpdate.getBook.bookComponents = _.set(result)

  return dataUpdate
}

export const dragComponentFn = async ({
  operation,
  // monitor,
  // moveIndex,
  targetRow,
  sourceRow,
  // client,
  // data,
  bookData,
}) => {
  const dataUpdate = cloneDeep(bookData)
  // const source = monitor.getItem()
  // const sourceRow = source.row
  const returnObj = cloneDeep(sourceRow)
  let moveIndex = targetRow.index

  if (!moveIndex) {
    moveIndex = 0
  }

  const divisions = cloneDeep(dataUpdate.divisions)
  const parts = cloneDeep(dataUpdate.parts)
  const partBc = dataUpdate.parts.find(x => x.id === targetRow.parentId)

  switch (operation) {
    case 'moveCard':
      // if nested rows reorderPartComponent
      if (targetRow.parentId) {
        if (partBc) {
          const newIndex = partBc.bookComponents.findIndex(
            x => x === targetRow.id,
          )

          const oldIndex = partBc.bookComponents.findIndex(
            x => x === sourceRow.id,
          )

          partBc.bookComponents = arrayMove(
            partBc.bookComponents,
            oldIndex,
            newIndex,
          )

          await dataUpdate.parts.map(i => {
            if (i.id === partBc.id) {
              return partBc
            }

            return i
          })
        }
      }

      // move book component insede  division (first level)
      dataUpdate.divisions = divisions.map(div => {
        if (div.id === targetRow.divisionId) {
          const dIndex = div.bookComponents.findIndex(x => x === sourceRow.id)
          const mIndex = div.bookComponents.findIndex(x => x === targetRow.id)
          const updatedBC = arrayMove(div.bookComponents, dIndex, mIndex)
          div.bookComponents = cloneDeep(updatedBC)
          return div
        }

        return div
      })

      sourceRow.index = moveIndex

      break
    case 'setCardChild':
      // 1.set book component id in target part
      dataUpdate.parts = await cloneDeep(
        parts.map(p => {
          const bc = cloneDeep(p.bookComponents)

          if (p.id === targetRow.id) {
            const index = bc.findIndex(x => x === sourceRow.id)

            if (index === -1) {
              bc.push(sourceRow.id)
              p.bookComponents = bc
            }
          } else {
            const index = bc.findIndex(x => x === sourceRow.id)

            if (index > -1) {
              bc.splice(index, 1)
            }

            p.bookComponents = bc
          }

          return p
        }),
      )

      // 2. update book component part id
      dataUpdate.bookComponents = cloneDeep(
        dataUpdate.bookComponents.map(itm => {
          if (itm.id === sourceRow.id) {
            itm.parentid = targetRow.id
          }

          return itm
        }),
      )

      returnObj.parentid = targetRow.partId
      returnObj.index = targetRow.index
      break

    case 'pushOneLevelUp': {
      // 1. remove book componentid in current part
      let parentPart = null
      // eslint-disable-next-line no-unused-vars
      let parentIndex

      const myFirstPromise = new Promise((resolve, reject) => {
        dataUpdate.parts = cloneDeep(
          parts.map((p, partIndex) => {
            const bc = cloneDeep(p.bookComponents)

            if (p.id === sourceRow.parentid) {
              parentIndex = partIndex

              const index = bc?.findIndex(x => x === sourceRow.id)

              if (index > -1) {
                bc.splice(index, 1)
                p.bookComponents = bc?.length ? bc : []

                if (
                  dataUpdate.bookComponents.find(x => x.id === p.id).parentid
                ) {
                  parentPart = dataUpdate.bookComponents.find(
                    x => x.id === p.id,
                  ).parentid
                }
              }

              return p
            }

            // if possible, put it in parent of parent
            if (parentPart && p.id === parentPart) {
              bc.push(sourceRow.id)
              p.bookComponents = bc
            }

            return p
          }),
        )

        resolve('Success!')
      })

      await myFirstPromise.then(() => {
        const prevParentId = sourceRow.parentid

        // remove item from parent book camponents
        dataUpdate.bookComponents = cloneDeep(
          dataUpdate.bookComponents.map(itm => {
            if (itm.id === prevParentId) {
              const index = itm.bookComponents.findIndex(
                x => x === sourceRow.id,
              )

              const childrenindex = itm.children
                ? itm.children.findIndex(x => x.id === sourceRow.id)
                : -1

              itm.bookComponents.splice(index, 1)

              /* eslint-disable-next-line no-unused-expressions */
              childrenindex > -1 ? itm.children?.splice(childrenindex, 1) : null
            }

            // 2. bookComponents parent id update
            if (itm.id === sourceRow.id) {
              itm.parentid = parentPart
              return itm
            }

            return itm
          }),
        )

        dataUpdate.bookComponents = cloneDeep(
          dataUpdate.bookComponents.map(itm => {
            if (itm.id === sourceRow.id) {
              itm.parentid = parentPart
              return itm
            }

            return itm
          }),
        )

        // const partslength = dataUpdate.bookComponents.filter(
        //   x => x.componentType === PART,
        // )

        returnObj.parentid = parentPart
        returnObj.index = targetRow.index
      })

      break
    }

    default:
      break
  }

  return { returnObj, dataUpdate }
}

export const bcListFlat = data => {
  const bcFullList = cloneDeep([...data?.getBook.bookComponents])
  const bcFullList1 = cloneDeep([...data?.getBook.bookComponents])
  const returnArr = []

  _.forEach(bcFullList, item => {
    const returnItem = { ...item, parentid: null }

    _.forEach(bcFullList1, i => {
      if (item.id === i.id) return null

      if (
        item.id !== i.id &&
        i.bookComponents.findIndex(el => el === item.id) > -1
      ) {
        returnItem.parentid = i.id
        return false
      }

      return true
    })

    returnArr.push(returnItem)
  })

  return returnArr
}

export const bcListFlatDivison = (data, divisionId) => {
  const dt = cloneDeep(
    data?.bookComponents.filter(x => x.divisionId === divisionId) || [],
  )

  const division = data?.divisions.find(x => x.id === divisionId)

  const sortedDivisionBc = division?.bookComponents
    .map(bcID => dt.find(x => x.id === bcID))
    .filter(x => x.divisionId === divisionId)

  const returnArr = []

  _.forEach(sortedDivisionBc, item => {
    returnArr.push(item.id)

    // division book compoennts
    if (item.componentType === 'part') {
      if (item.bookComponents.length) {
        // part level 1  book compoennts
        const constainigBc = item.bookComponents.map(bcID =>
          dt.find(x => x.id === bcID),
        )

        _.forEach(constainigBc, item2 => {
          returnArr.push(item2.id)

          if (item2.componentType === 'part') {
            if (item2.bookComponents.length) {
              // part level 2  book compoennts
              returnArr.push(...item2.bookComponents)
            }
          }
        })
      }
    }
  })

  return returnArr
}

export const casheUpdate = ({ dataUpdate, divisionId, id, index, partId }) => {
  const newDivIndex = dataUpdate.divisions.findIndex(x => x.id === divisionId)

  // step 1: remove it from root if it is there
  _.forEach(dataUpdate.divisions, division => {
    const oldIndex = division.bookComponents.findIndex(x => x === id)

    if (oldIndex >= 0) {
      division.bookComponents.splice(oldIndex, 1)
    }
  })

  // step 2: check if it was previously in a part and remove it
  _.forEach(dataUpdate.parts, part => {
    const partIndex = part.bookComponents.findIndex(x => x === id)

    if (partIndex >= 0) {
      part.bookComponents.splice(partIndex, 1)
    }
  })

  if (partId) {
    // step 3: add it in the new part
    dataUpdate.parts
      .find(x => x.id === partId)
      .bookComponents.splice(index, 0, id)
  } else {
    // step 3: insert it in  its  division
    dataUpdate.divisions[newDivIndex].bookComponents.splice(index, 0, id)
  }

  return cloneDeep(dataUpdate)
}

export const LastPublishedDate = ({ bookComponent }) => {
  if (!bookComponent.publishedDate) return null
  return (
    <>
      Last Published:
      {moment
        .utc(bookComponent.publishedDate)
        .utcOffset(moment().utcOffset())
        .format('L LT')}
    </>
  )
}

export const LastUpdatedDate = ({ bookComponent }) => {
  if (!bookComponent.updated) return null
  return (
    <>
      Last Updated: &nbsp;
      {moment
        .utc(bookComponent.updated)
        .utcOffset(moment().utcOffset())
        .format('L LT')}
      &nbsp;
    </>
  )
}

export const StatusView = ({ bookComponent, book }) => {
  if (!book.settings.chapterIndependently) {
    return <Status hasBackground status={book.status} />
  }

  const revise = bookComponent?.reviewsResponse.filter(
    item => item.decision === 'requestChanges',
  ).length

  const approved = bookComponent?.reviewsResponse.filter(
    item => item.decision === 'approve',
  ).length

  let bcStatus = bookComponent?.status
  if (bookComponent?.status === 'error') bcStatus = 'errors'
  return (
    <Status
      approved={approved}
      hasBackground
      revise={revise}
      status={bcStatus}
    />
  )
}

export const bodyPartsList = book => {
  const { parts: allParts, divisions } = book

  const bodySection = divisions.find(x => x.label === 'body')

  const structParts = partId => {
    const item = allParts.find(p => p.id === partId)
    if (!item) return null

    const parts = item.bookComponents.results
      .map(r => r.id)
      .filter(part => allParts.find(p => p.id === part))

    return {
      id: item.id,
      title: item.title,
      parts: parts.map(p => structParts(p)).filter(el => el),
      bookComponents: item.bookComponents.results.map(r => r.id),
    }
  }

  return bodySection.fullBookComponents
    .map(p => structParts(p))
    .filter(el => el)
}

export const selectedParentParts = ({ book, bookComponentId }) => {
  const parts = bodyPartsList(book)
  return getSelectedParts({ bookComponentId, parts })
}
