/* eslint-disable react/prop-types */

import React, { useContext, useState } from 'react'
import styled from 'styled-components'
import { useHistory, useParams } from 'react-router-dom'
import cloneDeep from 'lodash/cloneDeep'
import isEmpty from 'lodash/isEmpty'

import { canPublishMultiple } from '../../../../app/userRights'
import CurrentUserContext from '../../../../app/userContext'
import ConstantsContext from '../../../../app/constantsContext'
import { Select } from '../../../components'

import { ConfirmPublishing } from '../BookComponent'

import PermissionsGate from '../../../../app/components/PermissionsGate' // PERMISSIONS
import DateRangePicker from '../../../components/common/DateRangePicker'

import Header from './Header'
import Actions from './Actions'
import Footer from './Footer'
import Components from './Components'
import { isAllSelected } from '../pageHelpers'
import { parentPartsForRepeatedComponent } from '../commonPageHelpers'

// #region styled
const Root = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow-y: auto;
`

const RootPage = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const StyledSelect = styled(Select)`
  width: 120px;

  .select__value-container {
    flex-wrap: nowrap;
  }

  .select__control {
    background-color: ${({ selected }) =>
      selected ? '#eaf3ff' : 'transparent'};
    flex-wrap: no-wrap;
  }
`
// #endregion styled

const divisionFilterLabels = {
  frontMatter: 'Front',
  body: 'Body',
  backMatter: 'Back',
}

// #region status actions
export const disableReloadPreview = ({
  status,
  hasConvertedFile,
  convertedFileStatus,
  hasSourceFile,
  sourceFileStatus,
}) => {
  if (['preview', 'approved'].includes(status)) {
    return false
  }

  if (status === 'converting' || (status === 'failed' && !hasConvertedFile))
    return true

  if (!hasConvertedFile) {
    return true
  }

  if (
    hasConvertedFile &&
    ['new-upload', 'failed', 'loading-errors'].includes(convertedFileStatus)
  ) {
    return false
  }

  if (hasSourceFile && sourceFileStatus === 'new-upload') {
    return true
  }

  return [
    'submision-errors',
    'conversion-errors',
    'error',
    'converting',
    'loading-preview',
    'failed',
    'tagging',
    'publishing',
    'published',
    'new-book',
    'publish-failed',
  ].includes(status)
}

export const disabledSubmit = ({ status, hasSourceFile, sourceFileStatus }) => {
  if (!hasSourceFile) {
    return true
  }

  if (
    status === 'tagging' ||
    (status === 'new-upload' && sourceFileStatus === 'new-upload') ||
    status === 'failed'
  ) {
    return false
  }

  if (status === 'in-review') {
    return sourceFileStatus !== 'new-upload'
  }

  return true
}

export const disablePublish = ({ url, status }) => {
  // if we have preview link
  if (!isEmpty(url)) {
    return ![
      'published',
      'in-review',
      'preview',
      'approved',
      'publish-failed',
    ].includes(status)
  }

  return true
}
// #endregion status actions

const BookChapterProcessed = props => {
  const {
    activeTab,
    allowedStatusesToMove,
    appliedSearchQueryParams,
    bodyDivisionId,
    canAddPart,
    canDragAndDrop,
    canSelect,
    chapterIndependently,
    chapterProcessedSourceType,
    collectionOptions,
    currentDivisionBookComponentIds,
    currentPages,
    data,
    divisions,
    divisionsMap,
    isSubmitting,
    loadingAddPart,
    loadingDivision,
    loadingSearchResults,
    loadingUpdateBook,
    loadingUpdateBookTeams,
    onAddPart,
    onAfterSearchAction,
    onLoadNestedData,
    onMoveSearch,
    onPageChange,
    onPublish,
    onReconvert,
    onReorder,
    onRepeat,
    onSearch,
    onSubmit,
    onTabChange,
    onUpdateBook,
    onUpdateBookTeams,
    onUpload,
    pageSize,
    partsTree,
    publishing,
    reconvertingBc,
    searchMetadata,
    searchQueryParams,
    searchResults,
    searchResultsFlat,
    searchResultsKeyList,
    setAppliedSearchQueryParams,
    setSearchQueryParams,
    setShowingSearch,
    showPartContent,
    title,
    tocComponent,
    totalRecords,
    treeData,
    treeDataFlat,
    treeDataKeyList,
    workflow,
  } = props

  const history = useHistory()
  const { organizationId } = useParams()
  const { s } = useContext(ConstantsContext)
  const { currentUser } = useContext(CurrentUserContext)

  // #region state
  const [confirmIsOpen, setConfirmIsOpen] = useState(false)
  const [searchOrFilterActive, setSearchOrFilterActive] = useState()

  const [selectedRows, setSelectedRows] = useState([])
  const selectedRowsCounter = selectedRows.length
  const selectedIds = selectedRows.map(r => r.id)

  let selectAll

  if (searchOrFilterActive) {
    selectAll = isAllSelected(searchResultsKeyList, selectedIds)
  } else {
    selectAll = isAllSelected(treeDataKeyList, selectedIds)
  }

  const isSelectedRowInDivisionTopLevel =
    selectedRows.length === 1 &&
    currentDivisionBookComponentIds.includes(selectedRows[0].id)

  const [bookSourceTypeModal, setBookSourceTypeModalIsOpen] = useState(false)
  const [showFilters, setShowFilters] = useState(false)

  const [filters, setFilters] = useState([
    {
      name: 'division',
      component: StyledSelect,
      'data-test-id': 'division-select',
      isClearable: true,
      options: divisions.map(division => ({
        value: division.id,
        label: divisionFilterLabels[division.label],
      })),
      placeholder: 'Division',
      selected: !!searchQueryParams.filters.find(f => f.name === 'division')
        ?.value,
    },
    {
      name: 'status',
      component: StyledSelect,
      'data-test-id': 'status-select',
      isClearable: true,
      isMulti: true,
      options: [
        { value: 'new-upload', label: s('status.newUpload') },
        {
          value: 'converting',
          label: s('status.converting'),
        },
        { value: 'tagging', label: s('status.tagging') },
        { value: 'preview', label: s('status.preview') },
        { value: 'published', label: s('status.published') },
        { value: 'publishing', label: s('status.publishing') },
        {
          value: 'loading-preview',
          label: s('bookComponent.status.loading-preview'),
        },
        {
          value: 'conversion-errors',
          label: s('status.conversionErrors'),
        },
        {
          value: 'failed',
          label: s('status.failed'),
        },
        {
          value: 'loading-errors',
          label: s('status.loadingErrors'),
        },
        {
          value: 'tagging-errors',
          label: s('status.taggingErrors'),
        },
        {
          value: 'publish-failed',
          label: s('status.publishingFailed'),
        },
      ],
      placeholder: 'Status',
      selected: !!searchQueryParams.filters.find(f => f.name === 'status')
        ?.value,
    },
    {
      name: 'lastUpdated',
      component: DateRangePicker,
      label: 'Last updated',
      dateFormat: 'yyyy-MM-dd',
    },
    {
      name: 'lastPublished',
      component: DateRangePicker,
      label: 'Last published',
      dateFormat: 'yyyy-MM-dd',
    },
  ])

  const selectedPartsForRepeatedComponent =
    selectedRows.length === 1
      ? parentPartsForRepeatedComponent(selectedRows[0].id, partsTree)
      : []
  // #endregion state

  // #region rules
  const disablePublishBtn =
    selectedRows.length > 0 &&
    selectedRows.filter(x =>
      disablePublish({
        url: x.previewLink,
        status: x.status,
        sourceFiles: x.sourceFiles,
        convertedFile: x.convertedFile,
      }),
    ).length > 0

  const disableReloadPreviewBtn =
    reconvertingBc ||
    isSubmitting ||
    (selectedRowsCounter > 0
      ? selectedRows.filter(item => !disableReloadPreview(item)).length !==
        selectedRowsCounter
      : true)

  const isDisabledSubmit =
    isSubmitting ||
    reconvertingBc ||
    (selectedRowsCounter > 0
      ? selectedRows.filter(item => !disabledSubmit(item)).length !==
        selectedRowsCounter
      : true)

  const isAllowedToMoveOne = (item, targetDivId) =>
    item.divisionId === targetDivId &&
    (allowedStatusesToMove.includes(item.status) ||
      item.parentId ||
      item.isPart)

  const isAllowedToMoveChapters =
    selectedRows.length > 0 &&
    selectedRows.every(x => isAllowedToMoveOne(x, divisionsMap[activeTab].id))

  const isAllowedToRepeatChapters =
    selectedRows.length === 1 &&
    isAllowedToMoveChapters &&
    // parentParts.length > 0 &&
    !selectedRows[0].isPart &&
    !isSelectedRowInDivisionTopLevel

  const canPublishBc =
    canPublishMultiple({
      currentUser,
      organizationId,
      bookId: data?.getBook.id,
      bookComponentId: selectedRows.map(x => x.id),
    }) && data?.getBook.settings.approvalOrgAdminEditor
  // #endregion rules

  // #region handlers
  const submitComponents = () => {
    const ids = selectedRows.map(selected => selected.id)

    onSubmit(ids).then(() => {
      if (searchOrFilterActive) onAfterSearchAction()
    })
  }

  const handleSubmit = () => {
    if (chapterProcessedSourceType) {
      submitComponents()
      setSelectedRows([])
    } else {
      setBookSourceTypeModalIsOpen(true)
    }
  }

  const publishBookcompFn = selectedBookComponents => {
    const ids = selectedBookComponents
      .filter(sel => sel.checked)
      .map(item => item.id)

    return onPublish(ids).then(() => {
      setConfirmIsOpen(false)
      setSelectedRows([])
      if (searchOrFilterActive) onAfterSearchAction()
    })
  }

  const handlePublish = () => {
    if (chapterProcessedSourceType) {
      setConfirmIsOpen(true)
    } else {
      setBookSourceTypeModalIsOpen(true)
    }
  }

  const handleReloadPreview = () => {
    if (chapterProcessedSourceType) {
      const ids = selectedRows.map(selected => selected.id)
      onReconvert(ids)
    } else {
      setBookSourceTypeModalIsOpen(true)
    }
  }

  const handleClickTab = async selectedTab => {
    if (selectedTab !== activeTab) {
      setSelectedRows([])
      onTabChange(selectedTab)
    }
  }

  const handlePageChange = async page => {
    setSelectedRows([])
    onPageChange(page)
  }

  const handleReorder = (
    componentIds,
    targetId,
    action,
    partId,
    parentIdsToRefresh,
  ) => {
    return onReorder(
      componentIds,
      targetId,
      action,
      partId,
      parentIdsToRefresh,
    ).then(() => {
      setSelectedRows([])
    })
  }

  const handleRepeat = (sourceId, targetIds, parentIdsToRefresh) => {
    return onRepeat(sourceId, targetIds, parentIdsToRefresh).then(() => {
      setSelectedRows([])
      if (searchOrFilterActive) onAfterSearchAction()
    })
  }

  const clearFilters = () => {
    const currentFilters = cloneDeep(filters)

    filters.forEach((filter, idx) => {
      if (['lastUpdated', 'lastPublished'].includes(filter.name)) {
        currentFilters[idx].value = []
      } else {
        currentFilters[idx].value = ''
        currentFilters[idx].selected = false
      }
    })

    setFilters(currentFilters)
  }

  const handleClear = () => {
    setShowingSearch(false)
    setSearchOrFilterActive(false)
    setAppliedSearchQueryParams(null)

    setSearchQueryParams({
      filters: [],
      search: '',
      sort: { field: ['updated'], direction: 'desc' },
    })

    clearFilters()
  }

  const setFilterValue = (filterName, value) => {
    const currentFilters = cloneDeep(filters)

    const index = currentFilters.findIndex(f => f.name === filterName)

    if (['lastUpdated', 'lastPublished'].includes(filterName)) {
      currentFilters[index].value = value || []
    } else {
      currentFilters[index].value = value
      currentFilters[index].selected = !!value
    }

    setFilters(currentFilters)
  }

  const handleChangeFilterValue = (filterName, value) => {
    const updatedFilters = searchQueryParams.filters.filter(
      filter => filter.name !== filterName,
    )

    if (value !== null) {
      updatedFilters.push({ name: filterName, value })
    }

    setSearchQueryParams({
      ...searchQueryParams,
      filters: updatedFilters,
    })

    setFilterValue(filterName, value)
  }

  const handleSearch = () => {
    onSearch()
    setSearchOrFilterActive(true)
  }

  const handleSelectionChange = nodes => {
    setSelectedRows(nodes)
  }

  const handleSelectAllChange = enable => {
    if (enable) {
      if (searchOrFilterActive) {
        setSelectedRows(searchResultsFlat)
      } else {
        setSelectedRows(treeDataFlat)
      }
    } else {
      setSelectedRows([])
    }
  }
  // #endregion handlers

  return (
    <RootPage>
      <Header
        book={data && data.getBook}
        chapterIndependently={chapterIndependently}
        collectionOptions={collectionOptions}
        history={history}
        loadingUpdateBook={loadingUpdateBook}
        loadingUpdateBookTeams={loadingUpdateBookTeams}
        onUpdateBook={onUpdateBook}
        onUpdateBookTeams={onUpdateBookTeams}
        organizationId={organizationId}
        title={title}
        tocComponent={tocComponent}
        workflow={workflow}
      />

      <PermissionsGate scopes={['view-book']} showPermissionInfo>
        <Root>
          <Actions
            book={data && data.getBook}
            filters={filters}
            handleChangeFilterValue={handleChangeFilterValue}
            handleClear={handleClear}
            handleSearch={handleSearch}
            onUpload={onUpload}
            searchQueryParams={searchQueryParams}
            setSearchQueryParams={setSearchQueryParams}
            setShowFilters={setShowFilters}
            showFilters={showFilters}
          />

          <Components
            activeTab={activeTab}
            allowedStatusesToMove={allowedStatusesToMove}
            appliedSearchQueryParams={appliedSearchQueryParams}
            canAddPart={canAddPart}
            canDragAndDrop={canDragAndDrop}
            canSelect={canSelect}
            divisions={divisions}
            handleClickTab={handleClickTab}
            isSearchOn={searchOrFilterActive && divisions}
            loadingAddPart={loadingAddPart}
            loadingDivision={loadingDivision}
            loadingSearchResults={loadingSearchResults}
            onAddPart={onAddPart}
            onLoadNestedData={onLoadNestedData}
            onReorder={onReorder}
            onSelectAllChange={handleSelectAllChange}
            onSelectionChange={handleSelectionChange}
            organizationId={organizationId}
            searchMetadata={searchMetadata}
            searchResults={searchResults}
            selectAll={selectAll}
            selectedRows={selectedRows}
            showPartContent={showPartContent}
            treeData={treeData}
          />
        </Root>
      </PermissionsGate>

      <Footer
        activeTab={activeTab}
        bodyDivisionId={bodyDivisionId}
        bookSourceTypeModal={bookSourceTypeModal}
        canPublishBc={canPublishBc}
        currentPages={currentPages}
        disablePublishBtn={disablePublishBtn}
        disableReloadPreviewBtn={disableReloadPreviewBtn}
        divisionsMap={divisionsMap}
        groupChaptersInParts={
          data?.getBook?.settings.toc.group_chapters_in_parts
        }
        handlePageChange={handlePageChange}
        handlePublish={handlePublish}
        handleReloadPreview={handleReloadPreview}
        handleSubmit={handleSubmit}
        isAllowedToMoveChapters={isAllowedToMoveChapters}
        isAllowedToRepeatChapters={isAllowedToRepeatChapters}
        isDisabledSubmit={isDisabledSubmit}
        isOrderedManually={
          data?.getBook?.settings.toc.order_chapters_by === 'manual'
        }
        isSubmitting={isSubmitting}
        onMoveSearch={onMoveSearch}
        onReorder={handleReorder}
        onRepeat={handleRepeat}
        pageSize={pageSize}
        partsTree={partsTree}
        publishing={publishing}
        reconvertingBc={reconvertingBc}
        searchOrFilterActive={searchOrFilterActive}
        selectedPartsForRepeatedComponent={selectedPartsForRepeatedComponent}
        selectedRows={selectedRows}
        setBookSourceTypeModalIsOpen={setBookSourceTypeModalIsOpen}
        totalRecords={totalRecords}
      />

      <ConfirmPublishing // ARCH
        bookComponents={selectedRows}
        modalIsOpen={confirmIsOpen}
        publishBookComponent={publishBookcompFn}
        publishing={publishing}
        setIsOpen={setConfirmIsOpen}
      />
    </RootPage>
  )
}

export default BookChapterProcessed
