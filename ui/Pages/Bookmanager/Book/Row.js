/* eslint-disable prefer-destructuring */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { forwardRef, useRef } from 'react'
import styled from 'styled-components'
import moment from 'moment'
import { get, cloneDeep } from 'lodash'
import { useDrag, useDrop } from 'react-dnd'
// import { DragSource, DropTarget } from 'react-dnd'
import { th, grid } from '@pubsweet/ui-toolkit'
import * as icons from 'react-feather'
import { Status, Checkbox, Notification } from '../../../components'
import { getLongDate } from '../../../common/utils'
import { Icon, TOCLabel } from '../../../components/common'
import PermissionsGate from '../../../../app/components/PermissionsGate'

// #region  Styles
const Wrapper = styled.div`
  opacity: ${props => (props.hide ? 0 : 1)};
  padding-left: ${props => props.space};
`

const BookComponentStyled = styled.div`
  background-color: ${props =>
    props.checked || props.highlight ? th('colorTextPlaceholder') : 'none'};
  /* background: ${props => (props.highlight ? '#eee' : 'transparent')}; */

  border-bottom: 1px solid ${th('colorTextPlaceholder')};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 50px;
  opacity: ${props => props.opacity};
  padding: 8px;
  padding-left: ${props => (props.marginChildren ? props.marginChildren : '0')};

  :hover {
    cursor: pointer;
  }
`

const Separator = styled.div`
  border-right: 2px solid #eee;
  height: 35px;
  margin: 0 ${grid(1)};
`

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  text-align: left;
`

const Flex = styled.div`
  align-items: center;
  display: flex;
`

const Column = styled.div`
  align-items: center;
  display: flex;
`

const LeftAlign = styled.div`
  display: flex;
  flex-grow: 1;
`

const RightAlign = styled.div`
  display: flex;
  flex: 1 1 auto;
  justify-content: space-evenly;
`

const CenterSpan = styled.span`
  align-items: center;
  display: flex;
  margin: 0 8px;
`

const EmptySpace = styled.div`
  width: 32px;
`

const Version = styled.span`
  background: ${th('colorTextPlaceholder')};
  border-radius: ${th('borderRadius')};
  font-size: ${th('fontSizeBaseSmall')};
  margin-left: ${th('gridUnit')};
  margin-right: ${th('gridUnit')};
  padding: calc(${th('gridUnit')} / 2);
`

const Label = styled.span`
  align-items: center;
  background: ${props =>
    props.reverseColor ? th('colorText') : 'transparent'};
  border-radius: 16px;
  color: ${props =>
    props.reverseColor ? th('colorTextReverse') : th('colorText')};
  display: flex;
  font-size: ${th('fontSizeBaseSmall')};
  height: min-content;
  justify-content: center;
  padding: 0 8px;

  span {
    font-weight: 600;
    padding: 0 4px;
  }
`

const ColumnTitle = styled.div`
  display: flex;
  flex: 1 1;
  flex-direction: column;
  font-weight: ${props => (props.type === 'part' ? 700 : 400)};
  justify-content: center;
  margin: 0px;
  text-align: left;
`

const ChapterNumber = styled.div`
  margin: 0 8px 0px 0px;
`

const MoveIconContainer = styled.span`
  cursor: grab;
  display: inline-flex;
  padding: calc(${th('gridUnit')} / 2);

  svg {
    height: calc(${props => props.size} * ${th('gridUnit')});
    stroke: ${props => props.theme[props.color] || props.theme.colorText};
    width: calc(${props => props.size} * ${th('gridUnit')});
  }
`

// #endregion

const MoveIcon = forwardRef(
  ({ children, color, size = 3, theme, testId, ...props }, ref) => {
    const icon = icons.Move

    return (
      <MoveIconContainer
        color="colorText"
        data-test-id={testId}
        ref={ref}
        role="img"
        size={size}
      >
        {icon ? icon({}) : ''}
      </MoveIconContainer>
    )
  },
)

const getLatestFileStatus = (sourceFiles, convertedFiles) => {
  const sortedSource = sourceFiles.sort((a, b) => {
    return new Date(b.created) - new Date(a.created)
  })

  const sortedConverted = convertedFiles.sort((a, b) => {
    return new Date(b.created) - new Date(a.created)
  })

  let lastSource = null
  let lastConverted = null

  if (sortedSource.length) {
    lastSource = sortedSource[0]
  }

  if (sortedConverted.length) {
    lastConverted = sortedConverted[0]
  }

  if (lastSource && lastConverted) {
    return new Date(lastSource.created) - new Date(lastConverted.created) < 0
      ? lastConverted.status
      : lastSource.status
  }

  if (lastSource && !lastConverted) {
    return lastSource.status
  }

  if (!lastSource && lastConverted) {
    return lastConverted.status
  }

  return ''
}

const RowElement = ({
  book,
  divisionName,
  row,
  index,
  selectedRows = [],
  check,
  unCheck,
  onRowClick,
  marginChildren,
  children,
  collapsed,
  setCollapsed,
  disableCheck,
  isOver,
  canDrag,
  counter,
  style,
  highlight,
  updateOrder,
  dragComponent,
}) => {
  const elementRef = useRef(null)
  const dragHandlerRef = useRef(null)
  const targetRow = row

  let isChecked = !!(
    selectedRows.length && selectedRows.find(x => x.id === row.id)
  )

  const [{ isDragging }, connectDrag, preview] = useDrag({
    type: 'BOOK_COMPONENT',
    item: { id: row.id, row },
    collect(props, monitor, component) {
      // console.log(index)
      return {
        index,
        listId: row.divisionId,
        row: cloneDeep(row),
        sorceParentType: elementRef.current?.parentNode?.dataset?.type,
        sourceType: elementRef.current?.dataset?.type,
      }
    },
    canDrag(props, monitor) {
      return canDrag
    },
  })

  const [, connectDrop] = useDrop({
    accept: 'BOOK_COMPONENT',

    drop(props, monitor) {
      let partId = null

      if (props?.case === 'nest3levels') {
        Notification({
          message: 'Not allowed to nest more than 3 levels',
          type: 'danger',
        })

        return
      }

      if (props?.case === 'setCardChild') {
        partId = targetRow.id
      } else {
        if (props.row.childrenLevel.length > 0 && targetRow.level >= 1) {
          Notification({
            message: 'Not allowed to nest more than 3 levels',
            type: 'danger',
          })

          return
        }

        partId =
          targetRow.divisionId !== targetRow.parentId
            ? targetRow.parentId
            : null
      }

      const sendObject = {
        index: targetRow.index,
        row: {
          id: monitor.getItem().id,
          partId,
          divisionId: targetRow.divisionId,
        },
      }

      if (!monitor.didDrop()) {
        // const sourceObj = monitor.getItem()

        // if (sourceObj.listId === component.props.divisionId) {
        // eslint-disable-next-line no-param-reassign
        monitor.getItem().index = monitor.index

        // console.log(props)
        targetRow.divisionId === monitor.getItem().row.divisionId &&
          updateOrder(sendObject)
        // }
      }
    },
    async hover(props, monitor) {
      try {
        const item = monitor.getItem()
        // const targetRow = props?.row
        const source = item.row
        // const dragIndex = item.index
        // const hoverIndex = targetRow.index
        // item type we are draging  and its parent type
        const { sorceParentType, componentType: sourceType } = item.row

        const [targetType, targetParentType, moveCordinates] = [
          targetRow.componentType,
          targetRow.parentComponentType,
          monitor.getDifferenceFromInitialOffset(),
        ]

        let CASE = ''

        if (
          sourceType === 'part' &&
          sorceParentType === 'part' &&
          targetType === 'part' &&
          targetParentType !== 'part'
        ) {
          CASE = 'moveCard'
        }

        if (sourceType === 'part' && targetType === 'part') {
          // 1.  when drop target and source are both PARTS

          // if the part being draged contains other parts inside
          if (
            source.childrenLevel.filter(x => x.componentType === 'part')
              .length > 0
          ) {
            CASE = 'nest3Levels'
            monitor.getItem().case = CASE
            source.case = CASE
            return
          }

          if (
            targetRow.parentid === source.parentid &&
            (moveCordinates.x > -40 || moveCordinates.x < 40) &&
            moveCordinates.y !== 0
          ) {
            //   both have same parent id &&   x in [-40, 40]  , y != 0 => reorder
            CASE = 'moveCard'
          }

          if (
            moveCordinates.x < -100 &&
            (moveCordinates.y < -100 || moveCordinates.y > 100)
          ) {
            //  x  out of [-40 , 40] , y [-20, 20] => push one level up
            CASE = 'pushOneLevelUp'
          }

          if (moveCordinates.x > 40 && moveCordinates.y !== 0) {
            //   x > 40  , y != 0 => put element as child (if not possible, reorder)
            if (targetParentType === 'part') {
              CASE = 'nest3Levels'
              monitor.getItem().case = CASE
              source.case = CASE
              return
            }

            CASE = 'setCardChild'
          }
        }

        // 2 source chapter or apendix , target Part => put element as child
        if (sourceType !== 'part' && targetType === 'part') {
          if (
            (moveCordinates.x === moveCordinates.y && moveCordinates.x === 0) ||
            (moveCordinates.x < -5 &&
              (moveCordinates.y > -30 || moveCordinates.y < 30))
          ) {
            //  x  out of [-40 , 40] , y [-20, 20] => push one level up
            CASE = 'pushOneLevelUp'
          } else {
            CASE = 'setCardChild'
          }
        }

        // 3 source hapter or apendix , target chapter or apendix => reorder
        if (sourceType !== 'part' && targetType !== 'part') {
          CASE = 'moveCard'
        }

        if (!CASE) {
          if (targetRow.parentid !== source.parentid) {
            // eslint-disable-next-line no-param-reassign
            monitor.getItem().parentid = targetRow.parentid
          }

          CASE = 'moveCard'
        }

        if (source.id !== targetRow.id) {
          // await dragComponent({
          //   operation: CASE,
          //   monitor,
          //   targetRow,
          //   source,
          //   updateIndex: hoverIndex,
          //   dragIndex,
          //   moveIndex: hoverIndex,
          //   bookData: { getBook: book },
          // })

          monitor.getItem().case = CASE
          source.case = CASE
          // item.row.parentId = sourceChanged.parentid
          // Note: we're mutating the monitor item here!
          // Generally it's better to avoid mutations,
          // but it's good here for the sake of performance
          // to avoid expensive index searches.
          // monitor.index = hoverIndex // sourceChanged.index
          // monitor.getItem()?.index = sourceChanged.index
        }

        return
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e)
      }
    },
  })

  connectDrag(dragHandlerRef)
  connectDrop(preview(elementRef))

  // useImperativeHandle(ref, () => ({
  //   getNode: () => elementRef.current,
  // }))

  let reviewData = {}

  if (row.status === 'in-review') {
    if (row.reviewsResponse) {
      const approved = row.reviewsResponse.filter(
        item => item.decision === 'approve',
      ).length

      const revise = row.reviewsResponse.filter(
        item => item.decision === 'requestChanges',
      ).length

      reviewData = { approved, revise }
    }
  }

  const showRowDetails =
    row.componentType !== 'part' || !!row?.metadata?.filename || ''

  let showTOCLabel = false

  const tocStatusShow = [
    'conversion-errors',
    'loading-preview',
    'loading-errors',
    'tagging-errors',
    'preview',
    'published',
  ].includes(row.status)

  if (divisionName === 'body' && tocStatusShow) {
    const orderChaptersBy = book?.settings?.toc?.order_chapters_by

    if (orderChaptersBy === 'title') {
      showTOCLabel = get(row, 'title') === 'Untitled'
    } else if (orderChaptersBy === 'date_desc') {
      showTOCLabel = !(
        get(row, 'metadata.date_publication', null) ||
        get(row, 'metadata.date_updated', null) ||
        get(row, 'metadata.date_created', null)
      )
    } else if (orderChaptersBy !== 'manual' && row.componentType !== 'part') {
      // To get here, the order_chpaters_by === 'number'
      // so we need to check the existence of the chapter_number
      // in metadata AND that chapter_number endswith a number
      showTOCLabel = get(row, 'metadata.chapter_number')
      showTOCLabel = !showTOCLabel || !showTOCLabel.replace(/\D/g, '')
    }
  }

  let space = ''

  if (row.level === 1) {
    space = '33px'
  } else if (row.level === 2) {
    space = '65px'
  }

  return (
    <Wrapper
      data-test-id={`${row.title}-wrapper`}
      data-type={row.componentType}
      hide={isDragging}
      index={index}
      ref={elementRef}
      space={space}
      style={style}
    >
      <BookComponentStyled
        checked={isChecked}
        highlight={highlight}
        marginChildren={marginChildren}
      >
        <Row
          style={{
            background:
              isOver && row.componentType === 'part' ? '#efeaea8a' : 'none',
          }}
        >
          <Flex>
            {row.componentType === 'part' && (
              <Icon color="colorText" onClick={() => setCollapsed()}>
                {collapsed ? 'chevron-right' : 'chevron-down'}
              </Icon>
            )}

            {canDrag && (
              <PermissionsGate scopes={['order-by-drag-drop']}>
                <MoveIcon ref={dragHandlerRef} testId={`${row?.title}-dnd`} />
              </PermissionsGate>
            )}

            <CenterSpan>
              {showRowDetails && (
                <Checkbox
                  checked={isChecked}
                  disabled={disableCheck}
                  name="checkbox"
                  onClick={e => {
                    if (isChecked) {
                      unCheck(row)
                    } else {
                      check(row)
                    }

                    isChecked = !e.target.checked
                  }}
                />
              )}
            </CenterSpan>
          </Flex>
          <ChapterNumber>
            {row.metadata?.chapter_number
              ? `${row.metadata?.chapter_number}`
              : ''}
          </ChapterNumber>
          <ColumnTitle
            data-test-id={`chapter-title-${row.title}`}
            onClick={() => onRowClick(row)}
            type={row.componentType}
          >
            {/* eslint-disable-next-line react/no-danger */}
            <div dangerouslySetInnerHTML={{ __html: row.title || '' }} />
          </ColumnTitle>
          {counter > 1 && <TOCLabel duplicate />}
          <TOCLabel excluded={row.metadata?.hideInTOC} missing={showTOCLabel} />
        </Row>
        {showRowDetails && (
          <Row onClick={() => onRowClick(row)}>
            <LeftAlign>
              {row.componentType === 'part' && <EmptySpace />}
              <Column flexgrow={0}>
                <Version>V{row.versionName}</Version>{' '}
              </Column>
              <Column flexgrow={1}>
                <Status
                  approved={reviewData?.approved}
                  revise={reviewData?.revise}
                  status={row.status}
                />{' '}
                {[
                  'error',
                  'submission-errors',
                  'conversion-errors',
                  'loading-errors',
                ].includes(row.status) &&
                  (row.assigneeErrors || []).length > 0 && (
                    <>
                      <Separator /> {(row.assigneeErrors || []).join(' | ')}
                    </>
                  )}
                {['preview', 'pre-published', 'published'].includes(
                  row.status,
                ) && row.hasWarnings ? (
                  <Status status="query" />
                ) : (
                  ''
                )}
              </Column>

              {row.status === 'in-review' &&
              getLatestFileStatus(row.sourceFiles, row.convertedFile) !==
                row.status ? (
                <Column flexgrow={1}>
                  <Status
                    status={getLatestFileStatus(
                      row.sourceFiles,
                      row.convertedFile,
                    )}
                  />{' '}
                </Column>
              ) : null}
            </LeftAlign>
            <RightAlign>
              <Column flexgrow={1}>
                <Label>
                  {row.issues?.length > 0 ? (
                    <Status status="vendor-issues" />
                  ) : (
                    ''
                  )}
                </Label>
              </Column>
              <Column flexgrow={1}>
                <Label>
                  <span>Last Updated:</span>
                  {moment(row.updated).fromNow()}
                </Label>
              </Column>
              <Column flexgrow={1} onClick={() => onRowClick(row)}>
                <CenterSpan>
                  <Label>
                    <span>Last Published:</span>
                    <span
                      title={
                        row.publishedDate ? getLongDate(row.publishedDate) : ''
                      }
                    >
                      {row.publishedDate
                        ? `${moment(row.publishedDate).fromNow()}`
                        : '-'}
                    </span>
                  </Label>
                </CenterSpan>
              </Column>
              <Column flexgrow={2}>
                <Label>
                  <span> File Name: </span>
                  {row.metadata?.filename}
                </Label>
              </Column>
            </RightAlign>
          </Row>
        )}
      </BookComponentStyled>
      {!collapsed ? children : null}
    </Wrapper>
  )
}

export default RowElement
