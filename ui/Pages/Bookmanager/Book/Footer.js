/* eslint-disable react/prop-types */

import React from 'react'

import {
  ActionBar,
  Button,
  ButtonGroup,
  Pagination,
  FixedLoader,
  MoveComponents,
  RepeatComponent,
} from '../../../components'
import Information from '../../../components/Information'
import PermissionsGate from '../../../../app/components/PermissionsGate' // PERMISSIONS

const Footer = props => {
  const {
    activeTab,
    bodyDivisionId,
    bookSourceTypeModal,
    canPublishBc,
    currentPages,
    disablePublishBtn,
    disableReloadPreviewBtn,
    divisionsMap,
    groupChaptersInParts,
    handlePageChange,
    handlePublish,
    handleReloadPreview,
    handleSubmit,
    isAllowedToMoveChapters,
    isAllowedToRepeatChapters,
    isDisabledSubmit,
    isOrderedManually,
    isSubmitting,
    onMoveSearch,
    onReorder,
    onRepeat,
    pageSize,
    partsTree,
    publishing,
    reconvertingBc,
    searchOrFilterActive,
    selectedPartsForRepeatedComponent,
    selectedRows,
    setBookSourceTypeModalIsOpen,
    totalRecords,
  } = props

  return (
    <>
      {publishing && <FixedLoader title="Publishing in progress..." />}
      {isSubmitting && <FixedLoader title="Submitting Files..." />}
      {reconvertingBc && <FixedLoader title="Reloading preview..." />}

      <ActionBar
        leftComponent={
          <ButtonGroup>
            {canPublishBc && (
              <PermissionsGate scopes={['publish-book-component']}>
                <Button
                  data-test-id="book-comp-publish"
                  disabled={
                    selectedRows.length === 0 || publishing || disablePublishBtn
                  }
                  loading={publishing}
                  onClick={handlePublish}
                  status="primary"
                >
                  {publishing ? 'Publishing...' : 'Publish'}
                </Button>
              </PermissionsGate>
            )}

            <Information
              hideModal={() => setBookSourceTypeModalIsOpen(false)}
              isOpen={bookSourceTypeModal}
              title="Complete the required 'Book Source type' field in the Book Metadata, and then come back to this action."
              type="warning"
            />

            <PermissionsGate scopes={['book-reload-preview']}>
              <Button
                data-test-id="reload-preview-book-components"
                disabled={disableReloadPreviewBtn || publishing}
                key="reload-preview-book-components"
                loading={reconvertingBc}
                onClick={handleReloadPreview}
                primary
              >
                Reload preview
              </Button>
            </PermissionsGate>

            <PermissionsGate scopes={['view-book-component-submit']}>
              <Button
                data-test-id="submit-book-component"
                disabled={isDisabledSubmit}
                key="submit-book-component"
                loading={isSubmitting}
                onClick={() => handleSubmit()}
                primary
              >
                Submit
              </Button>
            </PermissionsGate>

            {(isOrderedManually || groupChaptersInParts) && (
              <PermissionsGate scopes={['move-book-to']}>
                <MoveComponents
                  autoSorted={!isOrderedManually}
                  bodyId={bodyDivisionId}
                  divisionId={divisionsMap[activeTab].id}
                  divisionsMap={divisionsMap}
                  groupInParts={groupChaptersInParts}
                  isDisabled={!isAllowedToMoveChapters}
                  key="move-book-component"
                  onReorder={onReorder}
                  onSearch={onMoveSearch}
                  selectedBookComponents={selectedRows}
                />
              </PermissionsGate>
            )}

            {groupChaptersInParts && activeTab === 'body' && (
              <PermissionsGate scopes={['repeat-book-to']}>
                <RepeatComponent
                  isDisabled={!isAllowedToRepeatChapters}
                  key="repeat-book-component"
                  onRepeat={onRepeat}
                  partsTree={partsTree}
                  selectedBookComponent={selectedRows[0]}
                  selectedPartIdsForRepeatedComponent={
                    selectedPartsForRepeatedComponent
                  }
                />
              </PermissionsGate>
            )}
          </ButtonGroup>
        }
        middleComponent={
          <Pagination
            currentPage={
              currentPages[searchOrFilterActive ? 'search' : activeTab]
            }
            key="pagination"
            onPageChanged={handlePageChange}
            pageLimit={pageSize}
            pageNeighbours={1}
            showFirstPage
            totalRecords={totalRecords}
          />
        }
      />
    </>
  )
}

Footer.propTypes = {}

Footer.defaultProps = {}

export default Footer
