/* eslint-disable react/prop-types */

import React, { useRef } from 'react'

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  DragAndDrop,
  Button,
  FixedLoader,
} from '../../../../components'

import { FormBookPart } from '../Form'

const BookModal = props => {
  const {
    isOpen,
    setIsOpen,
    onAddPart,
    loadingAddPart,
    // showContent,
  } = props

  const modalRef = useRef(null)

  const handleSubmit = formData => {
    if (!formData) {
      console.error('Add part modal: Submit: No data')
      return
    }

    onAddPart(formData).then(() => {
      setIsOpen(false)
    })
  }

  const closeModal = () => {
    setIsOpen(false)
  }

  return (
    <>
      <Modal isOpen={isOpen} onClose={closeModal}>
        <ModalHeader hideModal={closeModal}>Add new part</ModalHeader>

        <ModalBody>
          <DragAndDrop>
            <FormBookPart
              formikFormRef={modalRef}
              handleSubmit={handleSubmit}
              // showContent={showContent}
              innerRef={modalRef}
              showContent={false}
              values={{ title: '', metadata: { editor: [] } }}
            />
          </DragAndDrop>
        </ModalBody>

        <ModalFooter>
          <Button
            data-test-id="save-add-part"
            disabled={loadingAddPart}
            onClick={() => {
              modalRef.current.handleSubmit()
            }}
            status="primary"
          >
            {loadingAddPart ? 'Saving...' : 'Save'}
          </Button>

          <Button
            data-test-id="cancel-add-part"
            disabled={loadingAddPart}
            onClick={closeModal}
            outlined
            status="primary"
          >
            Cancel
          </Button>
        </ModalFooter>
      </Modal>

      {loadingAddPart && <FixedLoader title="Saving new part..." />}
    </>
  )
}

export default BookModal
