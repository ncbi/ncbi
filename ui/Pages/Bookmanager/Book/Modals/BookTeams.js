/* eslint-disable react/prop-types */

import React, { useState, useRef } from 'react'
import styled from 'styled-components'

import { th, darken } from '@pubsweet/ui-toolkit'

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  TableWithToolbar,
  Button,
  FixedLoader,
} from '../../../../components'

import { FormBookTeam } from '../Form'
import PermissionsGate from '../../../../../app/components/PermissionsGate' // PERMISSIONS

const BookTitle = styled.span`
  color: ${darken('colorPrimary', 0.5)};
  font-family: ${th('fontInterface')};
`

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`

const BookTeams = props => {
  const { book, loadingUpdateBookTeams, onUpdateBookTeams } = props
  const [modalIsOpen, setIsOpen] = useState(false)
  const modalRef = useRef(null)

  const toggleModal = () => setIsOpen(!modalIsOpen)

  const saveBookTeam = async () => {
    const formData = await modalRef.current.triggerSave()

    if (!formData) {
      console.error('Book teams modal: No form data!')
      return
    }

    onUpdateBookTeams(formData.teams)
      .then(() => {
        setIsOpen(false)
      })
      .catch(res => {
        console.error(res)

        Notification({
          message: 'Could not update',
          type: 'danger',
          title: 'Error',
        })
      })
  }

  return (
    <>
      <Button
        data-test-id="bookTeams"
        onClick={e => {
          setIsOpen(true)
        }}
        outlined
        status="primary"
      >
        Team
      </Button>

      <TableWithToolbar>
        {tableProps => (
          <Modal isOpen={modalIsOpen} onClose={toggleModal}>
            <ModalHeader hideModal={toggleModal}>
              Team: <BookTitle>{book.title}</BookTitle>
            </ModalHeader>
            <ModalBody height="60vh">
              <PermissionsGate
                errorProps={{ readonly: true }}
                scopes={['edit-book-team']}
              >
                <FormBookTeam
                  formikFormRef={modalRef}
                  tableProps={tableProps}
                  values={book}
                />
              </PermissionsGate>
            </ModalBody>
            <ModalFooter>
              <Container>
                <div>
                  <PermissionsGate scopes={['edit-book-team']}>
                    <Button
                      data-test-id="save-book-team"
                      disabled={loadingUpdateBookTeams}
                      onClick={saveBookTeam}
                      status="primary"
                    >
                      {loadingUpdateBookTeams ? 'Saving...' : 'Save'}
                    </Button>
                  </PermissionsGate>
                  <Button
                    data-test-id="cancel-book-metadata"
                    onClick={() => setIsOpen(false)}
                    outlined
                    size="small"
                  >
                    {loadingUpdateBookTeams ? 'Close' : 'Cancel'}
                  </Button>
                </div>
                <PermissionsGate scopes={['edit-book-team']}>
                  <Button
                    icon="trash-2"
                    iconPosition="end"
                    onClick={async () => {
                      modalRef.current.unassign(tableProps.selectedRows)
                    }}
                    outlined
                    size="small"
                    status="primary"
                  >
                    Remove from team
                  </Button>
                </PermissionsGate>
              </Container>
            </ModalFooter>
          </Modal>
        )}
      </TableWithToolbar>
      {loadingUpdateBookTeams && <FixedLoader title="Saving book teams..." />}
    </>
  )
}

export default BookTeams
