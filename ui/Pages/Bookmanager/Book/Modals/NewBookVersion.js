/* eslint-disable react/prop-types */
import React, { useState, useRef } from 'react'
import { useMutation } from '@apollo/client'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { useParams, useHistory } from 'react-router-dom'
import query from '../../graphql'
import { CURRENT_USER } from '../../../../../app/graphql'

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Notification,
} from '../../../../components'

import NewBookVersionForm from '../Form/NewBookVersionForm'
import PermissionsGate from '../../../../../app/components/PermissionsGate'
import {
  GET_BOOK_USERS,
  GET_BOOK_VERSIONS,
} from '../../../../../app/graphql/bookComponent'

const Title = styled.p`
  font-family: ${th('fontInerface')};
  font-size: 12pt;
  font-weight: 600;
  padding: 5;
`

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`

const Severities = styled.ul`
  list-style: disc;
  margin: 0;
`

export default ({ book }) => {
  const [modalIsOpen, setIsOpen] = useState(false)
  const formRef = useRef(null)
  const { organizationId, bookId } = useParams()
  const history = useHistory()

  const [createNewBookVersion, { loading: isSaving }] = useMutation(
    query.CREATE_NEW_BOOK_VERSION,
  )

  const handleClickButton = () => {
    setIsOpen(true)
  }

  const handleSave = () => {
    formRef.current.handleSubmit()
  }

  const handleSubmit = values => {
    const transformed = {
      bookId: book.id,
      fundedContentType: values.fundedContentType,
      workflow: values.workflow,
      version: values.version.toString(),
    }

    createNewBookVersion({
      variables: { input: transformed },
      refetchQueries: [
        { query: GET_BOOK_USERS, variables: { id: bookId } },
        { query: GET_BOOK_VERSIONS, variables: { bookId } },
        { query: CURRENT_USER, variables: {} },
      ],
    })
      .then(dt => {
        setIsOpen(false)

        Notification({
          type: 'success',
          message: 'Book Version created successfully',
        })

        history.push(
          `/organizations/${organizationId}/bookmanager/${dt?.data.createBookVersion.id}`,
        )
      })
      .catch(res => {
        console.error(res)

        Notification({
          message: `A version of this book already exists with this combination of content type and version number.`,
          type: 'danger',
          title: 'Error',
        })
      })
  }

  const closeModal = () => {
    setIsOpen(false)
  }

  const newBookVersion = book.version ? Number(book.version) : 1

  return (
    <PermissionsGate scopes={['view-book-new-version']}>
      <Button
        data-test-id="bookSettings"
        disabled={book?.status !== 'published'}
        onClick={handleClickButton}
        status="primary"
      >
        New version
      </Button>

      <Modal isOpen={modalIsOpen} onClose={closeModal} width="45vw">
        <ModalHeader hideModal={closeModal}>New Versions</ModalHeader>
        <ModalBody>
          <Title>For changes to status and scientific content</Title>
          <span>
            You should create a new version under the following events:
          </span>
          <Severities>
            <li>
              The status or format of the content changes in the publisher’s
              publishing workflow.
            </li>
            <li>
              The publisher has reviewed and substantively updated the
              scientific or scholarly content in a new published version under
              the same persistent identifier.
            </li>
          </Severities>

          <NewBookVersionForm
            handleSubmit={handleSubmit}
            innerRef={formRef}
            isSaving={isSaving}
            version={newBookVersion + 1}
          />
        </ModalBody>
        <ModalFooter>
          <Container>
            <Button onClick={handleSave} status="primary" type="submit">
              Create new version
            </Button>

            <Button
              data-test-id="close-error-modal"
              iconPosition="end"
              onClick={() => setIsOpen(false)}
              outlined
              status="primary"
            >
              Cancel
            </Button>
          </Container>
        </ModalFooter>
      </Modal>
    </PermissionsGate>
  )
}
