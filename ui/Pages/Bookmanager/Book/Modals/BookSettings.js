/* eslint-disable react/prop-types */
import React, { useContext, useState, useRef } from 'react'
import styled from 'styled-components'

import { th, darken } from '@pubsweet/ui-toolkit'
import DisplayInlineEditor from '../../../../components/wax/DisplayInlneEditor'

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  FixedLoader,
} from '../../../../components'

import BookSettingsForm from '../../../../components/BookSettings'
import {
  bookAPIToBookSettings,
  bookSettingsToBookAPI,
} from '../../../../../app/pages/_helpers'
import CurrentUserContext from '../../../../../app/userContext'
import PermissionsGate from '../../../../../app/components/PermissionsGate' // PERMISSIONS

const BookTitle = styled.span`
  color: ${darken('colorPrimary', 0.5)};
  font-family: ${th('fontInterface')};
`

export default props => {
  const { book, collectionOptions, loadingUpdateBook, onUpdateBook } = props

  const [modalIsOpen, setIsOpen] = useState(false)
  const formRef = useRef(null)
  const { currentUser } = useContext(CurrentUserContext)

  const organizationId = book.organisation.id

  const {
    orgAdminForOrganisation,
    editorForOrganisation,
    isSysAdmin,
  } = currentUser.auth

  const isOrgAdmin = orgAdminForOrganisation.includes(organizationId)
  const isEditor = editorForOrganisation.includes(organizationId)

  const { workflow } = book

  const bookType = book.settings.chapterIndependently
    ? 'chapterProcessed'
    : 'wholeBook'

  const initialFormValues = bookAPIToBookSettings(book)

  const publisherOptions = book.organisation.publisherOptions.map(
    publisher => ({
      value: publisher.id,
      label: publisher.name,
    }),
  )

  const inCollection = !!book.collection
  const inFundedCollection = book.collection?.collectionType === 'funded'

  const handleClickButton = () => {
    setIsOpen(true)
  }

  const handleClickCancel = () => {
    setIsOpen(false)
  }

  const handleClickSave = () => {
    formRef.current.handleSubmit()
  }

  const handleSubmit = values => {
    const toSend = bookSettingsToBookAPI(values)
    delete toSend.workflow

    onUpdateBook(toSend).then(() => {
      setIsOpen(false)
    })
  }

  const closeModal = () => {
    setIsOpen(false)
  }

  return (
    <>
      <Button
        data-test-id="bookSettings"
        onClick={handleClickButton}
        outlined
        status="primary"
      >
        Settings
      </Button>

      <Modal isOpen={modalIsOpen} onClose={closeModal}>
        <ModalHeader hideModal={closeModal}>
          Settings:
          <BookTitle>
            <DisplayInlineEditor content={book.title} />
          </BookTitle>
        </ModalHeader>

        <ModalBody>
          <PermissionsGate
            errorProps={{ readonly: true }}
            scopes={['edit-book-settings']}
          >
            <BookSettingsForm
              collectionOptions={collectionOptions}
              inCollection={inCollection}
              inFundedCollection={inFundedCollection}
              initialFormValues={initialFormValues}
              innerRef={formRef}
              isEditor={isEditor}
              isOrgAdmin={isOrgAdmin}
              isSysAdmin={isSysAdmin}
              onSubmit={handleSubmit}
              publisherOptions={publisherOptions}
              type={bookType}
              variant="bookSettings"
              workflow={workflow}
            />
          </PermissionsGate>
        </ModalBody>

        <ModalFooter>
          <PermissionsGate scopes={['edit-book-settings']}>
            <Button
              data-test-id="save-book-settings"
              disabled={loadingUpdateBook}
              loading={loadingUpdateBook}
              onClick={handleClickSave}
              status="primary"
            >
              {loadingUpdateBook ? 'Saving...' : 'Save'}
            </Button>
          </PermissionsGate>
          <Button
            data-test-id="cancel-book-metadata"
            onClick={handleClickCancel}
            outlined
            status="primary"
          >
            {loadingUpdateBook ? 'Close' : 'Cancel'}
          </Button>
        </ModalFooter>
      </Modal>

      {loadingUpdateBook && <FixedLoader title="Saving book settings..." />}
    </>
  )
}
