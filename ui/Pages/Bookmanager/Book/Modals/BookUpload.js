/* eslint-disable prefer-object-spread */
/* eslint-disable react/prop-types */

import React, { useState, useContext, useEffect } from 'react'
import isEmpty from 'lodash/isEmpty'

import styled from 'styled-components'
import { v4 as uuid } from 'uuid'

import { ErrorText } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import ConstantsContext from '../../../../../app/constantsContext'
import useUploadFile from '../../../../../app/useUploadFile' // ARCH
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ProgressBar,
  Button,
  FixedLoader,
  Icon,
  Notification,
} from '../../../../components'
import {
  validateFilesBulkupload,
  validateXMLFilesBulkupload,
  WordBulkUploadText,
  PDFBulkUploadText,
  XMLBulkUploadText,
} from '../../../../common/utils'

// #region styled
const Wrapper = styled.div`
  min-height: 300px;
`

const Dropzone = styled.div`
  align-items: center;
  background-color: th('colorBackground');
  border: 2px dashed
    ${props => (!props.isDragging ? th('colorBorder') : th('colorPrimary'))};
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  height: 100px;
  justify-content: center;
  margin: 0 8px;
  margin: 8px;
  padding: 0;
`

const FileList = styled.div`
  background-color: #fff;
  color: black;
  display: flex;
  flex-direction: column;
  font-family: ${props => props.theme.fontInterface};
  margin: 0 8px;
`

const FileRow = styled.div`
  align-content: stretch;
  align-items: center;
  display: flex;
  font-family: ${props => props.theme.fontInterface};
  justify-content: space-between;
  padding: 10px 0px 10px 0px;
  width: 60%;

  &:hover {
    background: #eee;
  }
`

const RowItem = styled.div`
  align-items: center;
  flex-basis: 0;
  flex-grow: 1;
`

const RowItemIcon = styled.div`
  flex: 0 1 40px;
`
// #endregion styled

const notAllowedStatuses = [
  'failed',
  'publish-failed',
  'converting',
  'loading-preview',
  'publishing',
]

const BookUpload = props => {
  const { book, onUpload } = props
  const { s } = useContext(ConstantsContext)
  const { workflow } = book

  let convertingBookComp = []

  if (book?.bookComponents) {
    convertingBookComp = book?.bookComponents.filter(
      x =>
        notAllowedStatuses.includes(x.status) ||
        (x.convertedFile?.length > 0 &&
          x.convertedFile[0]?.status === 'loading-preview') ||
        (x.status === 'in-review' &&
          x.convertedFile?.length > 0 &&
          x.convertedFile[0]?.status === 'loading-preview'),
    )
  }

  convertingBookComp = convertingBookComp.map(x => ({
    filename: x.metadata?.filename?.replace(/\.[^/.]+$/, ''),
    status: x.status,
  }))

  const {
    dispatch,
    getInputProps,
    getRootProps,
    fileList,
    currentFile = {},
    isValid,
  } = useUploadFile({
    validationFn: files =>
      workflow !== 'xml'
        ? validateFilesBulkupload(files, convertingBookComp, workflow)
        : validateXMLFilesBulkupload(files, convertingBookComp, workflow),
  })

  const [modalIsOpen, setIsOpen] = useState(false)
  const [isUploadEnabled, setUpload] = useState(false)

  const conversionBulkFinished =
    fileList?.length > 0 && fileList.every(f => f.status === 'uploaded')

  useEffect(() => {
    if (conversionBulkFinished) {
      setIsOpen(false)
      fileList.length = 0
    }
  }, [conversionBulkFinished])

  const handleUpload = async () => {
    setUpload(true)

    dispatch({
      payload: {
        update: {
          progress: 1,
          status: 'uploading',
        },
      },
      type: 'update',
    })

    onUpload(fileList)
      .catch(err => {
        console.error(err)

        Notification({
          message: 'Could not upload',
          type: 'danger',
          title: 'Error',
        })
      })
      .finally(() => setIsOpen(false))
  }

  const closeModal = () => {
    dispatch({
      type: 'removeConverted',
    })

    setIsOpen(false)
  }

  return (
    <>
      <Button
        data-test-id="bookUpload"
        icon="upload"
        onClick={e => {
          setIsOpen(true)
        }}
        status="primary"
      >
        Upload chapters
      </Button>

      {!conversionBulkFinished && !modalIsOpen && !isEmpty(currentFile) && (
        <FixedLoader hideSpiner>
          <ProgressBar
            circleOneStroke="#d9edfe"
            circleTwoStroke="#0B65CB"
            progress={currentFile.percentage}
            size={30}
            strokeWidth={8}
          />
          <span>{s(`upload.${currentFile.status}`)}</span>{' '}
          <span>{currentFile.name}</span>
        </FixedLoader>
      )}

      <Modal isOpen={modalIsOpen} onClose={closeModal}>
        <ModalHeader hideModal={closeModal} minimize>
          Upload
        </ModalHeader>

        <ModalBody>
          <>
            {workflow === 'word' && <WordBulkUploadText />}
            {workflow === 'pdf' && <PDFBulkUploadText />}
            {workflow === 'xml' && <XMLBulkUploadText />}

            <Wrapper>
              <Dropzone {...getRootProps({ className: 'dropzone' })}>
                <input
                  {...getInputProps({
                    onClick: () => {
                      setUpload(false)

                      dispatch({
                        type: 'removeConverted',
                      })
                    },
                  })}
                  multiple
                />

                <div>
                  <Button status="primary">Select files</Button>

                  <br />

                  <span> or drop files here</span>
                </div>
              </Dropzone>

              <FileList>
                {fileList.map((item, index) => (
                  // eslint-disable-next-line react/no-array-index-key
                  <FileRow data-test-id={item.name} key={index + 1}>
                    <RowItemIcon>
                      {item.progress ? (
                        <ProgressBar
                          circleOneStroke="#d9edfe"
                          circleTwoStroke="#0B65CB"
                          progress={item.progress}
                          size={30}
                          strokeWidth={8}
                        />
                      ) : null}
                    </RowItemIcon>

                    <RowItem>
                      <span>{item.name}</span>
                    </RowItem>

                    <RowItem>
                      <div>
                        <ul>
                          {item.errors.length > 0
                            ? item.errors.map(error => (
                                <li key={uuid()}>
                                  <ErrorText>{error}</ErrorText>
                                </li>
                              ))
                            : s(`upload.${item.status}`)}
                        </ul>
                      </div>
                    </RowItem>

                    <RowItemIcon>
                      {item.status === 'readyToUpload' ||
                      item.errors.length > 0 ? (
                        <Icon
                          color="colorError"
                          onClick={() => {
                            dispatch({
                              payload: {
                                name: item.name,
                              },
                              type: 'remove',
                            })
                          }}
                        >
                          x-circle
                        </Icon>
                      ) : null}
                    </RowItemIcon>
                  </FileRow>
                ))}
              </FileList>
            </Wrapper>
          </>
        </ModalBody>

        <ModalFooter>
          <Button
            data-test-id="book-uplaod"
            disabled={!isValid || isUploadEnabled}
            onClick={handleUpload}
            status="primary"
          >
            Upload
          </Button>
        </ModalFooter>
      </Modal>
    </>
  )
}

export default BookUpload
