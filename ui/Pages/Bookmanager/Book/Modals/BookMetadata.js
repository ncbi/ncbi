/* eslint-disable react/prop-types */

import React, { useState, useRef } from 'react'
import cloneDeep from 'lodash/cloneDeep'
import omit from 'lodash/omit'

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Notification,
  FixedLoader,
} from '../../../../components'
import MetadataForm from '../../../../components/MetadataForm'
import DisplayInlineEditor from '../../../../components/wax/DisplayInlneEditor'
import DragAndDrop from '../../../../components/DragAndDrop'
import {
  bookAPIToBookMetadata,
  bookMetadataToBookAPI,
} from '../../../../../app/pages/_helpers/bookSettingsToApiConverter'
import Granthub from '../../../../../app/pages/_helpers/Granthub'
import PermissionsGate from '../../../../../app/components/PermissionsGate' // PERMISSIONS

const PAGE = 'bookManager'

const BookMetadata = props => {
  const { book, onUpdateBook, loadingUpdateBook: loading } = props

  const [modalIsOpen, setIsOpen] = useState(false)
  const formRef = useRef(null)

  const submissionType = book.settings.chapterIndependently
    ? 'chapterProcessed'
    : 'wholeBook'

  const conversionWorkflow = book.workflow
  const applyCoverFromParent = book.collection?.metadata.applyCoverToBooks

  const applyPermissionsFromParent =
    book.collection?.metadata.applyPermissionsToBooks && book.workflow === 'pdf'

  const applyPublisherFromParent =
    book.collection?.metadata.applyPublisherToBooks &&
    submissionType === 'chapterProcessed'

  const inCollection = !!book.collection

  const formValues = bookAPIToBookMetadata(book)
  const toggleModal = () => setIsOpen(!modalIsOpen)

  const handleSave = () => {
    formRef.current.handleSubmit()
  }

  const handleSubmit = async values => {
    let submittedValues = cloneDeep(values)

    if (values.cover?.id && values.cover.id === book.fileCover?.id) {
      submittedValues = omit(submittedValues, 'cover')
    }

    if (
      values.abstractGraphic?.id &&
      values.abstractGraphic.id === book.fileAbstract?.id
    ) {
      submittedValues = omit(submittedValues, 'abstractGraphic')
    }

    const transformedValues = await bookMetadataToBookAPI(submittedValues, PAGE)

    onUpdateBook(transformedValues).then(() => {
      Notification({
        type: 'success',
        message: 'Metadata were updated successfully',
      })
    })
  }

  const searchGranthubFn = searchValue => Granthub.search(searchValue, PAGE)

  return (
    <DragAndDrop>
      <Button
        data-test-id="bookMetadata"
        onClick={toggleModal}
        outlined
        status="primary"
      >
        Metadata
      </Button>

      <Modal fullSize isOpen={modalIsOpen} onClose={toggleModal}>
        <ModalHeader hideModal={toggleModal}>
          Metadata:{' '}
          <DisplayInlineEditor content={book.title} key={book.title} />
        </ModalHeader>

        <ModalBody noPadding>
          <PermissionsGate
            errorProps={{ readonly: true }}
            scopes={['edit-book-metadata']}
          >
            <MetadataForm
              applyCoverFromParent={applyCoverFromParent}
              applyPermissionsFromParent={applyPermissionsFromParent}
              applyPublisherFromParent={applyPublisherFromParent}
              conversionWorkflow={conversionWorkflow}
              inCollection={inCollection}
              initialFormValues={formValues}
              innerRef={formRef}
              isSaving={loading}
              onSubmit={handleSubmit}
              searchGranthub={searchGranthubFn}
              submissionType={submissionType}
              variant="book"
            />
          </PermissionsGate>
        </ModalBody>

        <ModalFooter>
          <PermissionsGate scopes={['edit-book-metadata']}>
            <Button
              data-test-id="save-book-metadata"
              disabled={loading}
              loading={loading}
              onClick={handleSave}
              status="primary"
            >
              {loading ? 'Saving...' : 'Save'}
            </Button>
          </PermissionsGate>
          <Button
            data-test-id="cancel-book-metadata"
            onClick={toggleModal}
            outlined
            status="primary"
          >
            {loading ? 'Close' : 'Cancel'}
          </Button>
        </ModalFooter>
      </Modal>
      {!modalIsOpen && loading && <FixedLoader title="Saving metadata..." />}
    </DragAndDrop>
  )
}

export default BookMetadata
