/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */

import React, { useState } from 'react'
import styled from 'styled-components'
import { useMutation } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'
import { mapValues, groupBy, forEach } from 'lodash'

import { Accordion } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  FixedLoader,
  Select,
} from '../../../../components'
import query from '../../graphql'

const Root = styled.div`
  font-family: ${th('fontInterface')};
`

const BookComponents = styled.div`
  display: block;
  font-family: ${th('fontInterface')};
  margin-left: 30px;
`

export default ({
  book,
  treeList,
  bookComponents,
  setSelectedRows,
  setSelectAll,
  disabled,
}) => {
  const [modalIsOpen, setIsOpen] = useState(false)
  const [selectedOption, setSelectedOption] = useState()
  const [type, setType] = useState()

  const [updateBookComponentOrder, { loading }] = useMutation(
    query.UPDATE_BOOK_COMPONENT_ORDER,
  )

  const getLevel = item => {
    let level = 1

    if (item.parentid) {
      level = 2
    }

    const l2PArentid =
      item.parentid && treeList.find(x => x.id === item.parentid)

    if (l2PArentid && l2PArentid.parentid) {
      level = 3
    }

    return level
  }

  const optionsList = []

  book.divisions.map(section => {
    if (section.label === 'body')
      optionsList.push({
        label: section.label
          .replace(/([a-z0-9])([A-Z])/g, '$1 $2')
          .toUpperCase(),
        value: section.id,
        type: 'section',
        level: 0,
      })

    treeList &&
      treeList
        .filter(x => x.divisionId === section.id && x.componentType === 'part')
        .map(item => {
          optionsList.push({
            label: item.title,
            value: item.id,
            type: item.componentType,
            level: getLevel(item),
          })

          item.children &&
            item.children
              .filter(x => x.componentType === 'part')
              .map(itm =>
                optionsList.push({
                  label: itm.title,
                  value: itm.id,
                  type: itm.componentType,
                  level: getLevel(itm),
                }),
              )
        })

    return section
  })

  // eslint-disable-next-line no-shadow
  const getPostObject = (type, bookComponents) => {
    const postObject = {
      bookId: book.id,
      divisionId: null,
      ids: bookComponents.map(i => i.id),
      index: 0,
      partId: null,
    }

    if (type) {
      if (type === 'section') {
        postObject.divisionId = selectedOption

        postObject.index =
          book.divisions.find(x => x.id === selectedOption).bookComponents
            .length || 0

        postObject.partId = null
        // postObject.id = item.id
        return postObject
      }

      if (type === 'part') {
        postObject.partId = selectedOption

        postObject.divisionId = book?.bookComponents.find(
          x => x.id === selectedOption,
        ).divisionId

        // postObject.id = item.id
        postObject.index =
          book.parts.find(x => x.id === selectedOption).bookComponents.length ||
          0
      }
    }

    return postObject
  }

  return (
    <>
      <Button
        data-test-id="move-chapters-to"
        disabled={disabled || loading}
        icon="corner-left-up"
        onClick={e => {
          setIsOpen(true)
        }}
        outlined
        status="primary"
      >
        {loading ? 'Moving...' : 'Move'}
      </Button>
      <Root>
        <Modal isOpen={modalIsOpen}>
          <ModalHeader hideModal={() => setIsOpen(false)}>
            Move chapters to
          </ModalHeader>
          <ModalBody>
            {book.divisions.map(section => [
              bookComponents.filter(x => x.divisionId === section.id).length ? (
                <Accordion
                  label={section.label.replace(/([a-z0-9])([A-Z])/g, '$1 $2')}
                  startExpanded
                >
                  {bookComponents.filter(x => x.divisionId === section.id)
                    .length ? (
                    <>
                      {bookComponents
                        .filter(x => x.divisionId === section.id)
                        .map(item => (
                          <BookComponents key={item.title}>
                            {(item.title || '').replace(/<[^>]+>/g, '')}
                          </BookComponents>
                        ))}
                    </>
                  ) : null}
                </Accordion>
              ) : null,
            ])}
            <br />
            <div>Search or select section or part</div>
            <Select
              menuPortalTarget={document.body}
              onChange={selected => {
                if (selected) {
                  setType(selected.type)
                  setSelectedOption(selected.value)
                } else {
                  setSelectedOption(null)
                }
              }}
              options={optionsList}
            />
          </ModalBody>
          <ModalFooter>
            <Button
              data-test-id="save-book-metadata"
              disabled={!selectedOption || loading}
              onClick={async () => {
                if (bookComponents.length) {
                  const grouped = mapValues(
                    groupBy(bookComponents, 'divisionId'),
                  )

                  const finalPostObject = {
                    bookId: book.id,
                    divisionId: null,
                    ids: [],
                    index: 0,
                    partId: null,
                  }

                  forEach(grouped, (itm, key) => {
                    const postObject = getPostObject(type, itm)
                    finalPostObject.ids.push(...postObject.ids)
                    finalPostObject.divisionId = postObject.divisionId
                    finalPostObject.index = postObject.index
                    finalPostObject.partId = postObject.partId
                  })

                  updateBookComponentOrder({
                    refetchQueries: [getOperationName(query.GET_BOOK_USERS)],
                    variables: finalPostObject,
                  }).catch(res => {
                    console.error(res)

                    Notification({
                      message: 'Could not update',
                      type: 'danger',
                      title: 'Error',
                    })
                  })

                  setSelectedRows([])
                  setSelectAll(false)
                  setIsOpen(false)
                }
              }}
              status="primary"
            >
              {loading ? 'Saving...' : 'Save'}
            </Button>

            <Button
              data-test-id="cancel-book-metadata"
              onClick={() => setIsOpen(false)}
              outlined
              status="primary"
            >
              {loading ? 'Close' : 'Cancel'}
            </Button>
          </ModalFooter>
        </Modal>
        {loading && <FixedLoader title="Updating order..." />}
      </Root>
    </>
  )
}
