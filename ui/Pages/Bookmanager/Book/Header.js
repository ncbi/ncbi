/* eslint-disable react/prop-types */

import React from 'react'
// import PropTypes from 'prop-types'
// import styled from 'styled-components'

// import { grid, th } from '@pubsweet/ui-toolkit'

import { BookHeader, Button, ButtonGroup, Status } from '../../../components'
import { BookMetadata, BookSettings, BookTeams } from './Modals'
import PermissionsGate from '../../../../app/components/PermissionsGate' // PERMISSIONS

const Header = props => {
  const {
    book,
    chapterIndependently,
    collectionOptions,
    history,
    loadingUpdateBook,
    loadingUpdateBookTeams,
    onUpdateBook,
    onUpdateBookTeams,
    organizationId,
    title,
    tocComponent,
    workflow,
  } = props

  return (
    <BookHeader
      childrenRight={
        <ButtonGroup>
          <PermissionsGate scopes={['view-book-metadata']}>
            <BookMetadata
              book={book}
              loadingUpdateBook={loadingUpdateBook}
              onUpdateBook={onUpdateBook}
            />
          </PermissionsGate>

          <PermissionsGate scopes={['view-book-settings']}>
            <BookSettings
              book={book}
              collectionOptions={collectionOptions}
              loadingUpdateBook={loadingUpdateBook}
              onUpdateBook={onUpdateBook}
            />
          </PermissionsGate>

          <PermissionsGate scopes={['view-book-team']}>
            <BookTeams
              book={book}
              loadingUpdateBookTeams={loadingUpdateBookTeams}
              onUpdateBookTeams={onUpdateBookTeams}
            />
          </PermissionsGate>

          <PermissionsGate scopes={['view-book-toc']}>
            <Button
              data-test-id="book-toc"
              icon={<Status hideText status={tocComponent?.status} />}
              iconPosition="end"
              onClick={() => {
                const bookUrl = `/organizations/${organizationId}/bookmanager/${book.id}/toc/${tocComponent?.id}`
                history.push(bookUrl)
              }}
              outlined
              separator
              status="primary"
            >
              TOC
            </Button>
          </PermissionsGate>
        </ButtonGroup>
      }
      title={title}
      type={
        workflow === 'word' || chapterIndependently
          ? 'bookWithChapters'
          : 'book'
      }
      workflow={workflow}
    />
  )
}

Header.propTypes = {}

Header.defaultProps = {}

export default Header
