/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { map } from 'lodash'
import { countComponents } from '../bookManagerFunctions'
import PermissionsGate from '../../../../app/components/PermissionsGate'

const canDragComp = ({
  allowedStatusesToMove,
  row,
  canDragItemsInDivision,
  divisionName,
  workflow,
}) => {
  const { componentType, status } = row
  // always allow drag for parts and  any component in front & back Matter
  if (divisionName !== 'body' || componentType === 'part') return true

  // Allowed list of statuses of components to drag and drop
  if (!allowedStatusesToMove.includes(status)) {
    return false
  }

  return canDragItemsInDivision
}

const disableCheck = comp => {
  // eslint-disable-next-line no-underscore-dangle
  return comp.__typename !== 'Book' && comp.status === 'converting'
}

class BookComponentRow extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props)
  }

  check = selectedItem => {
    this.props.setSelectedRows([...this.props.selectedRows, selectedItem])
  }

  unCheck = selectedItem => {
    const selectedRows = [...this.props.selectedRows]

    selectedRows.splice(
      selectedRows.findIndex(x => x.id === selectedItem.id),
      1,
    )

    this.props.setSelectedRows(selectedRows)
  }

  render() {
    const {
      row,
      onRowClick,
      index,
      book = { bookComponents: [] },
      collapsedParts,
      setCollapsed,
      setPartIsOpen,
      setPartModalValues,
      updateOrder,
      canDragItemsInDivision,
      divisionName,
      changeSection,
      listId,
      selectedRows,
      allowedStatusesToMove,
      dragComponent,
      orderSetting,
      showTOCLabel,
      style,
      highlight,
      rowComponent: RowComponent,
    } = this.props

    const workflow = book?.workflow
    return (
      <PermissionsGate scopes={['view-book-component']}>
        <RowComponent
          book={book}
          canDrag={canDragComp({
            row,
            canDragItemsInDivision,
            divisionName,
            workflow,
            allowedStatusesToMove,
          })}
          changeSection={changeSection}
          check={this.check}
          collapsed={
            collapsedParts && collapsedParts.findIndex(x => x === row.id) > -1
          }
          counter={countComponents({
            type: row?.componentType,
            id: row?.id,
            bookComponents: book?.bookComponents || [],
          })}
          disableCheck={disableCheck(row)}
          divisionName={divisionName}
          dragComponent={dragComponent}
          highlight={highlight === index}
          index={index}
          key={`l0${row.id}`}
          listId={listId}
          onRowClick={onRowClick}
          orderSetting={orderSetting}
          props={this.props}
          row={row}
          selectedRows={selectedRows}
          setCollapsed={() => setCollapsed(row.id)}
          setPartIsOpen={setPartIsOpen}
          setPartModalValues={setPartModalValues}
          showTOCLabel={showTOCLabel}
          style={style}
          unCheck={this.unCheck}
          updateOrder={updateOrder}
        >
          {row.children &&
            map(row.children, (rowL1, keyL1) => (
              <RowComponent
                book={book}
                canDrag={canDragComp({
                  allowedStatusesToMove,
                  row: rowL1,
                  canDragItemsInDivision,
                  divisionName,
                  workflow,
                })}
                changeSection={changeSection}
                check={this.check}
                collapsed={
                  collapsedParts &&
                  collapsedParts.findIndex(x => x === rowL1.id) > -1
                }
                counter={countComponents({
                  type: rowL1.componentType,
                  id: rowL1.id,
                  bookComponents: book?.bookComponents,
                })}
                disableCheck={disableCheck(rowL1)}
                divisionName={divisionName}
                dragComponent={dragComponent}
                index={keyL1}
                key={`l1${rowL1.id}`}
                listId={listId}
                // index={`${index}.${keyL1}`}
                marginChildren="30px"
                onRowClick={onRowClick}
                orderSetting={orderSetting}
                props={this.props}
                row={rowL1}
                selectedRows={selectedRows}
                setCollapsed={() => setCollapsed(rowL1.id)}
                setPartIsOpen={setPartIsOpen}
                setPartModalValues={setPartModalValues}
                showTOCLabel={showTOCLabel}
                unCheck={this.unCheck}
                updateOrder={updateOrder}
              >
                {rowL1.children &&
                  map(rowL1.children, (rowL2, keyL2) => (
                    <div>
                      <RowComponent
                        book={book}
                        canDrag={canDragComp({
                          allowedStatusesToMove,
                          row: rowL2,
                          canDragItemsInDivision,
                          divisionName,
                          workflow,
                        })}
                        changeSection={changeSection}
                        check={this.check}
                        collapsed={
                          collapsedParts &&
                          collapsedParts.findIndex(x => x === rowL2.id) > -1
                        }
                        counter={countComponents({
                          type: rowL2.componentType,
                          id: rowL2.id,
                          bookComponents: book?.bookComponents,
                        })}
                        disableCheck={disableCheck(rowL2)}
                        divisionName={divisionName}
                        // index={`${index}.${keyL1}.${keyL2}`}
                        dragComponent={dragComponent}
                        index={keyL2}
                        key={`l2${rowL2.id}`}
                        listId={listId}
                        marginChildren="60px"
                        onRowClick={onRowClick}
                        orderSetting={orderSetting}
                        props={this.props}
                        row={rowL2}
                        selectedRows={selectedRows}
                        setCollapsed={() => setCollapsed(rowL2.id)}
                        showTOCLabel={showTOCLabel}
                        unCheck={this.unCheck}
                        updateOrder={updateOrder}
                      />
                    </div>
                  ))}
              </RowComponent>
            ))}
        </RowComponent>
      </PermissionsGate>
    )
  }
}

export default BookComponentRow
