/* eslint-disable react/prop-types */

import React from 'react'
// import PropTypes from 'prop-types'
import styled from 'styled-components'

// import { grid, th } from '@pubsweet/ui-toolkit'

import { Tabs as UITabs } from '../../../components'

import DivisionWrapper from './DivisionWrapper'
import SearchResultList from '../Search/SearchResultList'

// #region styled
const Tabs = styled(UITabs)`
  overflow-y: auto;
`

const TabHeaderLabel = styled.span`
  text-transform: capitalize;
`
// #endregion styled

const Components = props => {
  const {
    activeTab,
    onAddPart,
    allowedStatusesToMove,
    appliedSearchQueryParams,
    canAddPart,
    canDragAndDrop,
    canSelect,
    divisions,
    handleClickTab,
    isSearchOn,
    loadingAddPart,
    loadingDivision,
    loadingSearchResults,
    onLoadNestedData,
    onReorder,
    onSelectAllChange,
    onSelectionChange,
    searchMetadata,
    searchResults,
    selectAll,
    selectedRows,
    showPartContent,
    treeData,
  } = props

  if (isSearchOn)
    return (
      <SearchResultList
        canSelect={canSelect}
        loading={loadingSearchResults}
        onSelectAllChange={onSelectAllChange}
        onSelectionChange={onSelectionChange}
        queryParams={appliedSearchQueryParams}
        searchMetadata={searchMetadata}
        searchResults={searchResults}
        selectAll={selectAll}
        selectedRows={selectedRows}
      />
    )

  return (
    <Tabs
      activeKey={activeTab}
      onChangeTab={handleClickTab}
      outlined
      tabItems={divisions.map((section, sectionIndex) => {
        const { label } = section
        return {
          key: label,
          label: (
            <TabHeaderLabel>
              {label.replace(/([a-z0-9])([A-Z])/g, '$1 $2')}
            </TabHeaderLabel>
          ),
          content: (
            <DivisionWrapper
              allowedStatusesToMove={allowedStatusesToMove}
              canAddPart={canAddPart}
              canDragAndDrop={canDragAndDrop}
              canSelect={canSelect}
              loadingAddPart={loadingAddPart}
              loadingDivision={loadingDivision}
              onAddPart={onAddPart}
              onLoadNestedData={onLoadNestedData}
              onReorder={onReorder}
              onSelectAllChange={onSelectAllChange}
              onSelectionChange={onSelectionChange}
              selectAll={selectAll}
              selectedRows={selectedRows}
              showPartContent={showPartContent}
              treeData={treeData}
            />
          ),
        }
      })}
    />
  )
}

Components.propTypes = {}

Components.defaultProps = {}

export default Components
