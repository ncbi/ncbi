/* eslint-disable react/prop-types */

import React, { useState } from 'react'
import styled from 'styled-components'

import { th, grid } from '@pubsweet/ui-toolkit'

import { Button, ButtonGroup, Checkbox, Spinner } from '../../../components'
import Tree from '../Tree'
import { BookPartModal } from './Modals'
import PermissionsGate from '../../../../app/components/PermissionsGate' // PERMISSIONS

// #region styled
const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const SelectAll = styled.div`
  align-items: center;
  display: flex;
`

const Container = styled.div`
  align-content: center;
  align-items: center;
  display: flex;
  justify-content: center;
  margin-left: 16px;

  button {
    align-content: 'center';
    display: flex;
    height: 35px;
    justify-content: 'center';
  }
`

const DivisionHeader = styled.div`
  border-bottom: 1px solid ${th('bottomBorderColor')};
  display: flex;
  padding: ${grid(1)};
  padding-left: ${grid(2)};
`
// #endregion styled

const DivisionWrapper = props => {
  const {
    allowedStatusesToMove,
    canAddPart,
    canDragAndDrop,
    canSelect,
    loadingAddPart,
    loadingDivision,
    onAddPart,
    onLoadNestedData,
    onReorder,
    onSelectAllChange,
    onSelectionChange,
    selectAll,
    selectedRows,
    showPartContent,
    treeData,
  } = props

  const [partModalIsOpen, setPartModalIsOpen] = useState(false)

  const handleCheck = nodes => {
    onSelectionChange(nodes)
  }

  const handleCheckAll = () => {
    if (selectAll) {
      onSelectAllChange(false)
    } else {
      onSelectAllChange(true)
    }
  }

  const checkedKeys = selectedRows.map(row => row.key)

  if (loadingDivision)
    return <Spinner label="Loading page data..." pageLoader />

  return (
    <Wrapper>
      <DivisionHeader>
        <SelectAll>
          <Checkbox
            checked={selectAll}
            label="Select all"
            name="selectAll"
            onClick={handleCheckAll}
            value={selectAll}
          />
        </SelectAll>

        {canAddPart && (
          <Container>
            <ButtonGroup>
              <PermissionsGate scopes={['add-part']}>
                <Button
                  data-test-id="addBookPart"
                  icon="plus"
                  onClick={() => setPartModalIsOpen(true)}
                  primary
                  size="small"
                >
                  Add Part
                </Button>
              </PermissionsGate>
            </ButtonGroup>
          </Container>
        )}
      </DivisionHeader>

      <Tree
        canDrag={canDragAndDrop}
        canSelect={canSelect}
        checkedKeys={checkedKeys}
        data={treeData}
        maxNestingLevel={null}
        onCheck={handleCheck}
        onDrop={onReorder}
        onLoadNestedData={onLoadNestedData}
        statusesThatAllowRowMove={allowedStatusesToMove}
        // onLoadNestedData={onLoadData}
      />

      <BookPartModal
        isOpen={partModalIsOpen}
        loadingAddPart={loadingAddPart}
        onAddPart={onAddPart}
        setIsOpen={setPartModalIsOpen}
        showContent={showPartContent}
      />
    </Wrapper>
  )
}

export default DivisionWrapper
