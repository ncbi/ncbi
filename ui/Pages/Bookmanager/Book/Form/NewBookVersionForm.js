/* eslint-disable react/prop-types */
/* stylelint-disable no-descending-specificity */

import React, { useContext } from 'react'
import styled from 'styled-components'
import { Formik, Form } from 'formik'
import * as yup from 'yup'

import { grid } from '@pubsweet/ui-toolkit'

import {
  SelectComponent as Select,
  NumberInputComponent as NumberInput,
} from '../../../../components/FormElements/FormikElements'
import { Spinner } from '../../../../components/common'
import ConstantsContext from '../../../../../app/constantsContext'

// #region styled

const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 800px;
  padding-bottom: 50px;
  padding-top: 25px;

  > form > div:not(:last-child) {
    margin-bottom: ${grid(2)};
  }
`

const divStyle = {
  paddingBottom: '25px',
}

// #endregion styled
const regMatch = /^((([1]$)|[1-9][0-9]{0,1}[0-9]{0,1})|1000)$/

const validations = yup.object().shape({
  workflow: yup.string().nullable().required('Workflow is required'),
  version: yup
    .string()
    .matches(regMatch, 'Version Number should be numeric')
    .required('Version Number is required'),
  fundedContentType: yup
    .string()
    .nullable()
    .required('fundedContentType is required'),
})

const NewBookVersionForm = props => {
  const { version: minVersion, innerRef, isSaving, handleSubmit } = props
  const { s } = useContext(ConstantsContext)

  const initialValues = {
    workflow: null,
    fundedContentType: null,
    version: minVersion || null,
  }

  if (isSaving) return <Spinner pageLoader />

  return (
    <Wrapper>
      <Formik
        initialValues={initialValues}
        innerRef={innerRef}
        onSubmit={handleSubmit}
        validationSchema={validations}
      >
        {formProps => {
          // eslint-disable-next-line no-unused-vars
          const { setFieldValue, values } = formProps
          // eslint-disable-next-line no-shadow
          const { workflow, fundedContentType, version } = values

          // #region options
          const workFlowsOptions = [
            {
              label: 'PDF',
              value: 'pdf',
              isDisabled: false,
            },
            {
              label: 'XML',
              value: 'xml',
              isDisabled: fundedContentType === 'authorManuscript',
            },
          ]

          const fundedContentTypeOptions = [
            {
              label: 'Author manuscript',
              value: 'authorManuscript',
              isDisabled: false,
            },
            {
              label: 'Prepublication draft',
              value: 'prepublicationDraft',
              isDisabled: false,
            },
            {
              label: 'Published PDF',
              value: 'publishedPDF',
              isDisabled: false,
            },
            {
              label: 'Final full-text',
              value: 'finalFullText',
              isDisabled: false,
            },
          ]

          const handleFundedContentTypeChange = option => {
            if (option?.value === 'authorManuscript') {
              setFieldValue('workflow', 'pdf')
            }
          }

          return (
            <Form>
              <div style={divStyle}>
                <Select
                  field={{ name: 'workflow', value: workflow }}
                  form={formProps}
                  label="Conversion workflow"
                  options={workFlowsOptions}
                  required
                />
              </div>
              <div style={divStyle}>
                <Select
                  field={{
                    name: 'fundedContentType',
                    value: fundedContentType,
                  }}
                  form={formProps}
                  label="Content type"
                  onChange={handleFundedContentTypeChange}
                  options={fundedContentTypeOptions}
                  required
                />
              </div>
              <div style={divStyle}>
                <NumberInput
                  description={s(
                    'fieldDescriptions.bookSettingsForm.versionInput',
                  )}
                  field={{
                    name: 'version',
                    value: version,
                  }}
                  form={formProps}
                  label="Version number"
                />
              </div>
            </Form>
          )
        }}
      </Formik>
    </Wrapper>
  )
}

export default NewBookVersionForm
