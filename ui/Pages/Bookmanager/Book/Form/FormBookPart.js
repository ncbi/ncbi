/* eslint-disable no-console */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable react/prop-types */
/* eslint-disable camelcase */
/* eslint-disable prefer-destructuring */
import React, { useMemo, useState } from 'react'
import { Formik, Form, ErrorMessage } from 'formik'
import { ErrorText } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import * as Yup from 'yup'
import { cloneDeep, omit } from 'lodash'
import { Editor, ContributorsComponent } from '../../../../components'

// #region  Styled Components
const TabContainer = styled.div`
  border-bottom: 1px solid ${th('colorBorder')};
  display: flex;
`

const Block = styled.div`
  display: block;
`

const TabTitle = styled.div`
  background: #fff;
  border: 1px solid ${th('colorBorder')};
  border-bottom: 0;
  border-top-left-radius: ${th('borderRadius')};
  border-top-right-radius: ${th('borderRadius')};
  margin-bottom: -1px;
  padding: 10px;
`
// #endregion

const validateEditors = rows => {
  const missing = rows.find(row => !(row.surname && row.givenName))
  if (missing) return false
  return true
}

const validationSchema = Yup.object().shape({
  title: Yup.string().required('Title is required'),
  metadata: Yup.object().shape({
    editor: Yup.array()
      .of(Yup.object())
      .test(
        'editors-test',
        'Please fill out all given names and surnames',
        validateEditors,
      ),
  }),
})

const FormBookPart = ({
  handleSubmit,
  innerRef,
  isSaving,
  values: initialValues,
  showContent,
  ...props
}) => {
  const [affiliations, setAffiliations] = useState([])

  const handleSubmitForm = async submittedValues => {
    const dt = cloneDeep(submittedValues)
    const data = omit(dt, ['__typename'])

    const submitEditor = data.metadata.editor.map(editor =>
      omit(editor, ['__typename']),
    )

    dt.metadata.editor = submitEditor

    return handleSubmit && handleSubmit(dt)
  }

  return (
    <Formik
      initialValues={initialValues}
      innerRef={innerRef}
      onSubmit={handleSubmitForm}
      validationSchema={validationSchema}
    >
      {formProps => {
        const { setFieldValue, values } = formProps

        const handleClickContributors = (name, val) => {
          setFieldValue(name, val)
        }

        const MemorizedWaxTitle = useMemo(
          () => (
            <Editor
              inline
              onChange={data => {
                setFieldValue('title', data)
              }}
              value={values?.title}
            />
          ),
          [],
        )

        const MemorizedWaxContent = useMemo(
          () => (
            <Editor
              onChange={data => {
                setFieldValue('content', data)
              }}
              value={values?.content}
            />
          ),
          [],
        )

        return (
          <Form noValidate>
            <div data-test-id="title-editor">
              <label>Title</label>
              {MemorizedWaxTitle}
              <ErrorMessage component={ErrorText} name="title" />

              {showContent && (
                <Block>
                  <br />
                  <label>Body content</label>
                  {MemorizedWaxContent}
                  <ErrorMessage component={ErrorText} name="content" />
                </Block>
              )}
            </div>
            <br />
            <br />
            <TabContainer>
              <TabTitle>Editors </TabTitle>
            </TabContainer>
            <ContributorsComponent
              affiliations={affiliations}
              name="metadata.editor"
              onChange={val => handleClickContributors('metadata.editor', val)}
              setAffiliations={setAffiliations}
              value={values?.metadata?.editor}
              variant="componentEditors"
            />
          </Form>
        )
      }}
    </Formik>
  )
}

export default FormBookPart
