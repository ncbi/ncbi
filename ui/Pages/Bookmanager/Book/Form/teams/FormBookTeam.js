/* eslint-disable react/prop-types */
import React, { useImperativeHandle, useState } from 'react'
import { withFormik, Form, Field } from 'formik'
import { th } from '@pubsweet/ui-toolkit'
import { isEmpty, cloneDeep } from 'lodash'
import styled from 'styled-components'
import UsersTable from './UsersTable'
import { Accordion, SelectComponent, Button } from '../../../../../components'
import { mapOrder } from '../../../../../common/utils'

const Wraper = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${th('gridUnit')};
`

const HorizontalWraper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: ${th('gridUnit')};
`

const UserSelect = styled.div`
  flex-basis: 60%;
`

const teamdata = members =>
  members.map(({ user }) => ({
    ...user,
  }))

const findSpecifiedTeam = (teams, teamname, exclude, allTeams) => {
  if (!teams) return []

  const foundTeam = teams.find(
    team => team.role.toLowerCase() === teamname.toLowerCase(),
  )

  if (!foundTeam || foundTeam.length === 0) return []

  const isEnabled = userId => {
    const orgUserTeam = teams.find(t => t.role === 'user')
    const member = orgUserTeam.members.find(m => m.user.id === userId)
    return member?.status === 'enabled'
  }

  //  current editors of the book
  const editors =
    allTeams.find(x => x.role === 'editor')?.members?.map(x => x.user.id) || []

  //  current previewers of the book
  const previewers =
    allTeams.find(x => x.role === 'previewer')?.members?.map(x => x.user.id) ||
    []

  //  current authors of the book
  const authors =
    allTeams.find(x => x.role === 'author')?.members?.map(x => x.user.id) || []

  const finalTeam = foundTeam.members
    .filter(el => {
      return (
        isEnabled(el.user.id) &&
        exclude.members.findIndex(x => x.user.id === el.user.id) === -1
      )
    })
    .map(tm => ({
      value: tm.user.id,
      label: `${tm.user.givenName} ${tm.user.surname}`,
    }))

  switch (teamname) {
    case 'editor':
      //  exlude users that might have more than one role
      return finalTeam.filter(
        x => !previewers.includes(x.value) && !authors.includes(x.value),
      )

    case 'author':
      //  exlude users that might have more than one role and are editors
      return finalTeam.filter(x => !editors.includes(x.value))

    case 'previewer':
      //  exlude users that might have more than one role and are editors
      return finalTeam.filter(x => !editors.includes(x.value))

    default:
      return finalTeam
  }
}

const findSpecifiedTeamUsers = (teams, teamname) => {
  const foundTeam = teams.find(team => team.role === teamname)

  if (foundTeam) {
    return foundTeam.members.map(tm => ({
      ...tm.user,
    }))
  }

  return []
}

const Basic = ({
  setValidationAction,
  submitForm,
  handleSubmit,
  setFieldValue,
  errors,
  editForm,
  editMode,
  form,
  values,
  validateForm,
  book,
  readonly,
  ...props
}) => {
  const { formikFormRef } = props
  const [selectedRows, setSelectedRows] = useState([])

  useImperativeHandle(formikFormRef, () => ({
    triggerSave: async () => {
      submitForm()
      const isValid = await validateForm()

      if (isEmpty(isValid)) {
        return values
      }

      return false
    },
    unassign: async selected => {
      const updatedValues = []

      values.teams.forEach(tm => {
        updatedValues.push({
          ...tm,
          members: tm.members.filter(x => !selectedRows.includes(x.user.id)),
        })
      })

      setFieldValue(`teams`, updatedValues)
    },
  }))

  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <Wraper>
          {values.teams?.map((team, index) => {
            return (
              <div key={team.name}>
                <Accordion key={team.id} label={`${team.name}s`} startExpanded>
                  <HorizontalWraper>
                    <UserSelect>
                      <Field
                        cacheOptions
                        className="select-user-role"
                        classNamePrefix="select"
                        component={SelectComponent}
                        defaultOptions
                        disabled={readonly}
                        isClearable={false}
                        menuPortalTarget={document.body}
                        multiple
                        name={`added[${team.role}]`}
                        options={findSpecifiedTeam(
                          values.organizationTeams,
                          team.role,
                          team,
                          values.teams,
                        )}
                        setFieldValue={setFieldValue}
                      />
                    </UserSelect>
                    <div>
                      {!readonly && (
                        <Button
                          onClick={() => {
                            const foundUsers = findSpecifiedTeamUsers(
                              values.organizationTeams,
                              team.role.toLowerCase(),
                            ).filter(x =>
                              values.added[team.role].includes(x.id),
                            )

                            if (values.added[team.role]) {
                              const teamMembers = cloneDeep(values.teams)

                              foundUsers.forEach(f => {
                                teamMembers[index].members.push({
                                  status: 'enabled',
                                  user: f,
                                })
                              })

                              setFieldValue(`teams`, teamMembers)

                              setFieldValue(`added`, {
                                editor: [],
                                previewer: [],
                                author: [],
                              })
                            }
                          }}
                          status="primary"
                        >
                          Add Member
                        </Button>
                      )}
                    </div>
                  </HorizontalWraper>

                  <UsersTable
                    data={teamdata(team.members)}
                    key={`users_${team.role}`}
                    readonly={readonly}
                    selectedRows={selectedRows}
                    setFieldValue={setFieldValue}
                    setSelectedRows={setSelectedRows}
                    team={team}
                    values={values}
                  />
                  <br />
                </Accordion>
                <br />
              </div>
            )
          })}
        </Wraper>
      </Form>
    </div>
  )
}

const BookTeams = withFormik({
  handleSubmit: (values, { setSubmitting }) => {
    setSubmitting(false)
  },
  mapPropsToValues: ({ values }) => {
    const itemorder = ['editor', 'previewer', 'author']

    const tempTeam = cloneDeep(
      values.teams.filter(x => !['previewer', 'author'].includes(x.role)),
    ) // todo: remove filter to show 2 roles

    const orderedArray = mapOrder(tempTeam, itemorder, 'role')
    return {
      teams: orderedArray,
      organizationTeams: values.organisation.teams,
      added: {
        editor: [],
        previewer: [],
        author: [],
      },
    }
  },

  validate: values => {
    const errors = {}

    return errors
  },
})(Basic)

export default BookTeams
