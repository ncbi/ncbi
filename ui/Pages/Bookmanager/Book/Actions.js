/* eslint-disable react/prop-types */

import React from 'react'
// import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid } from '@pubsweet/ui-toolkit'

import PermissionsGate from '../../../../app/components/PermissionsGate' // PERMISSIONS
import { Button, Checkbox } from '../../../components'
import Search from '../../../components/common/Search'
import { BookUpload } from './Modals'

// #region styled
const Wrapper = styled.div`
  align-items: center;
  display: flex;
  gap: ${grid(2)};
  justify-content: space-between;
  padding: ${grid(1)};
`

const ActionRow = styled.div``

const FilterSearchGroup = styled.div`
  display: flex;
  gap: ${grid(0.5)};
  height: 34px;
  justify-content: flex-end;
`

const StyledCheckbox = styled(Checkbox)`
  margin: 0 ${grid(2)};
`

const SearchInput = styled(Search)`
  width: 240px;
`

const SearchButton = styled(Button)``

const ClearButton = styled(Button)`
  margin: 0;
`
// #endregion styled

const Actions = props => {
  const {
    book,
    filters,
    handleChangeFilterValue,
    handleClear,
    handleSearch,
    onUpload,
    searchQueryParams,
    setSearchQueryParams,
    setShowFilters,
    showFilters,
  } = props

  return (
    <Wrapper>
      <div>
        <PermissionsGate scopes={['upload-bulk-chapters']}>
          <ActionRow>
            <BookUpload book={book} onUpload={onUpload} />
          </ActionRow>
        </PermissionsGate>
      </div>

      <FilterSearchGroup>
        {showFilters &&
          filters.map(
            ({ name, component: FilterComponent, ...filterProps }) => (
              <FilterComponent
                key={name}
                {...filterProps}
                onChange={val => handleChangeFilterValue(name, val)}
              />
            ),
          )}

        <StyledCheckbox
          checked={showFilters}
          data-test-id="show-filter-checkbox"
          label="Show filters"
          onClick={() => setShowFilters(!showFilters)}
        />

        <SearchInput
          triggerSearch={v => {
            setSearchQueryParams({ ...searchQueryParams, search: v })
          }}
          value={searchQueryParams.search}
        />

        <SearchButton
          // disabled={
          //   searchQueryParams.search === '' || searchQueryParams.filters.length === 0
          // }
          onClick={handleSearch}
          status="primary"
        >
          Search
        </SearchButton>

        <ClearButton onClick={handleClear} outlined status="primary">
          Clear
        </ClearButton>
      </FilterSearchGroup>
    </Wrapper>
  )
}

Actions.propTypes = {}

Actions.defaultProps = {}

export default Actions
