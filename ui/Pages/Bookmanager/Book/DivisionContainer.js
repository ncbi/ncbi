/* eslint-disable no-unused-expressions */
/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { VariableSizeList as List } from 'react-window'
import styled from 'styled-components'

import BookComponentRow from './BookComponentRow'
import Row from './Row'

const Wrapper = styled.div`
  flex-grow: 1;
  overflow: auto;

  .List {
    /* stylelint-disable-next-line declaration-no-important */
    overflow: unset !important;
  }
`

class Container extends Component {
  constructor(props) {
    super(props)
    this.inputField = React.createRef()
  }

  render() {
    let style = {
      minHeight: '80px',
      paddingLeft: '8px',
    }

    const {
      rootComponents: root,
      book,
      setPartIsOpen,
      setPartModalValues,
      index,
      isMoved,
      isOver,
      testId,
      canDragItemsInDivision,
      pushCard,
      collapsed,
      divisionName,
      dragComponent,
      divisionId,
      moveCard,
      onRowClick,
      pushOneLevelUp,
      removeCard,
      reorderPartComponent,
      selectedRows,
      setCardChild,
      setCollapsedPart,
      setSelectedRows,
      updateOrder,
      allowedStatusesToMove,
      orderSetting,
      showTOCLabel,
    } = this.props

    if (isOver) {
      style = Object.assign(style, { border: '1px dashed grey' })
    }

    if (index) {
      this.inputField?.current?.scrollToItem(index, 'center')
    }

    const rowHeights = root.map(x => {
      if (x.collapsed) return 0

      return x.componentType === 'part' ? 50 : 89
    })

    const getItemSize = itemIndex => {
      return rowHeights[itemIndex]
    }

    const heightChangedHandle = thisIndex => {
      this.inputField?.current?.resetAfterIndex(thisIndex)
    }

    isMoved && this.inputField?.current?.resetAfterIndex(index)

    // eslint-disable-next-line no-shadow
    const Rows = ({ index: innerIndex, style }) => {
      const bookComponent = root[innerIndex]
      if (bookComponent.collapsed) return null
      return (
        <BookComponentRow
          active={bookComponent.isActive ? 'active' : 'inactive'}
          allowedStatusesToMove={allowedStatusesToMove}
          book={book}
          canDragItemsInDivision={canDragItemsInDivision}
          changeSection={pushCard}
          collapsedParts={collapsed}
          divisionName={divisionName}
          dragComponent={dragComponent}
          highlight={index}
          index={innerIndex}
          key={`BookComponentRow-${bookComponent.id}`}
          listId={divisionId}
          moveCard={moveCard}
          onRowClick={onRowClick}
          orderSetting={orderSetting}
          pushOneLevelUp={pushOneLevelUp}
          removeCard={removeCard}
          reorderPartComponent={reorderPartComponent}
          row={bookComponent}
          rowComponent={Row}
          selectedRows={selectedRows}
          setCardChild={setCardChild}
          setCollapsed={itm => {
            heightChangedHandle(innerIndex)
            setCollapsedPart(itm)
          }}
          setPartIsOpen={setPartIsOpen}
          setPartModalValues={setPartModalValues}
          setSelectedRows={setSelectedRows}
          showTOCLabel={showTOCLabel}
          style={style}
          updateOrder={itm => {
            heightChangedHandle(innerIndex)
            updateOrder(itm)
          }}
        />
      )
    }

    const chapterHeight = 89
    const partHeight = 50
    const chaptersLength = root.filter(c => c.componentType !== 'part').length
    const partLength = root.length - chaptersLength

    const listHeight =
      chapterHeight * chaptersLength + partHeight * partLength + 10

    return (
      <Wrapper data-test-id={testId} style={{ ...style }}>
        <List
          className="List"
          height={listHeight}
          itemCount={root.length}
          itemKey={listIndex => {
            const elementI = root[listIndex]
            return `${elementI.id}-${elementI.parentId} `
          }}
          itemSize={getItemSize}
          ref={this.inputField}
        >
          {Rows}
        </List>
      </Wrapper>
    )
  }
}

export default Container
