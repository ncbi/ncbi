/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React, {
  useState,
  useImperativeHandle,
  useEffect,
  useContext,
} from 'react'
import { isEmpty } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { Field, withFormik, ErrorMessage } from 'formik'
import { useLazyQuery } from '@apollo/client'
import { ErrorText } from '@pubsweet/ui'
import ConstantsContext from '../../../../app/constantsContext'
import query from '../graphql'
import {
  Button,
  TextFieldComponent,
  SelectComponent,
  Spinner,
  Checkbox,
  ButtonGroup,
} from '../../../components'

const API_GRANTHUB = 'https://api.ncbi.nlm.nih.gov/granthub'

const OrgSelection = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: calc(${th('gridUnit')} * 3);

  label {
    text-align: start;
  }
`

const Label = styled.label`
  font-weight: ${props => (props.strong ? '600' : '400')};
`

const Inline = styled.div`
  display: flex;
  flex-direction: row;
`

const NoteWrapper = styled.div`
  display: ${props => (props.show ? 'flex' : 'none')};
  margin-bottom: calc(${th('gridUnit')} * 2);
`

const FunderContainer = styled.div`
  display: ${props => (props.show ? 'flex' : 'none')};
`

const SelectField = styled(Field)`
  min-width: 500px;
  width: 50%;
`

const FunderResultWrapper = styled.div`
  flex: 1;
  height: 350px;
  margin-left: 20px;
  overflow-y: scroll;
`

const FunderResult = styled.div`
  background: ${props =>
    props.results ? th('colorTextPlaceholder') : 'transpartent'};
  border-left: 1px solid
    ${props => (props.results ? th('colorPrimary') : 'transpartent')};

  padding-left: ${th('gridUnit')};

  ul > li {
    align-items: center;
    display: flex;
    list-style-type: none;
  }
`

const FunderSearch = styled.div`
  flex: 1;
`

const LiStyled = styled.li`
  background: white;
  border-bottom: 1px solid ${th('colorBorder')};
  cursor: pointer;
  display: flex;
  padding: ${th('gridUnit')};
`

const OrganisationInput = ({ getOrganisations, ...props }) => (
  <OrgSelection>
    <Label strong>Select the organization</Label>
    <SelectComponent {...props} />
  </OrgSelection>
)

const encodeQueryData = data => {
  const ret = []

  // eslint-disable-next-line no-restricted-syntax
  for (const [key, value] of Object.entries(data)) {
    if (value !== '') {
      ret.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    }
  }

  return ret.join('&')
}

const Basic = ({
  handleSubmit,
  errors,
  touched,
  status,
  values,
  submitForm,
  formikFormRef,
  validateForm,
  setFieldValue,
  ...props
}) => {
  const [getOrganisations, { data }] = useLazyQuery(query.GET_ORGANISATIONS)
  const [organisation, setOrganization] = useState(null)
  const [awards, setAwards] = useState(null)
  const [loadingOrg, setLoadingOrg] = useState(false)
  const { s } = useContext(ConstantsContext)
  const [totals, setTotals] = useState({ grantees: 0, grants: 0 })

  useEffect(() => {
    getOrganisations({
      variables: {
        input: {
          skip: 0,
          take: -1,
        },
      },
    })
  }, [])

  useImperativeHandle(formikFormRef, () => ({
    triggerSave: async () => {
      submitForm()
      const isValid = await validateForm()

      if (isEmpty(isValid)) {
        return {
          ...values,
          organisation,
        }
      }

      return false
    },
  }))

  return (
    <form onSubmit={handleSubmit}>
      <SelectField
        cacheOptions
        className="basic-single"
        classNamePrefix="select"
        component={OrganisationInput}
        defaultOptions
        isClearable
        name="organisation"
        onChange={e => {
          setFieldValue("['awardee.number']", null)
          setOrganization(e)
          setFieldValue('organizationType', e.orgType)
        }}
        options={
          data?.getOrganisations?.results
            ?.filter(x => x.settings.type.publisher)
            .map(({ id, name, settings, teams }) => ({
              label: name,
              value: id,
              orgType: settings.type,
              teams,
            })) || []
        }
        placeholder="Select organization name"
      />
      {/* todo: in the future we will show funder org fields */}
      <NoteWrapper>
        <Label strong>
          Provide a grant or contract number to verify your association to this
          funder organization
        </Label>
      </NoteWrapper>
      <FunderContainer>
        <FunderSearch>
          <Field
            component={TextFieldComponent}
            label="Number *"
            name="number"
          />
          <Inline>
            <ButtonGroup>
              <Button
                data-test-id="search-affiliation"
                disabled={
                  !values.number || (values.number && !values.number.trim())
                }
                onClick={() => {
                  const variables = {
                    'award.number': values.number.trim(),
                  }

                  const number = encodeQueryData(variables)

                  setLoadingOrg(true)

                  fetch(`${API_GRANTHUB}/collapsed/?${number}`)
                    .then(res => {
                      return res.json()
                    })
                    .then(result => {
                      let sum = 0

                      result.body.forEach(item => {
                        sum += item.awards.length || 0
                      })

                      setTotals({ grantees: result.meta.total, grants: sum })
                      setAwards(result.body)

                      setLoadingOrg(false)
                    })
                }}
                status="primary"
              >
                {s('form.search')}
              </Button>

              <Button
                data-test-id="clear-affiliation"
                onClick={() => {
                  setFieldValue('number', '')
                  setFieldValue("['awardee.number']", '')
                  setFieldValue("['awardee.name']", '')
                  setAwards(null)
                  setTotals({ grantees: 0, grants: 0 })
                }}
                outlined
                status="primary"
              >
                {s('form.clear')}
              </Button>
            </ButtonGroup>
          </Inline>
          <br />
          <Field
            component={TextFieldComponent}
            label="Institutional affiliation"
            name="['awardee.name']"
          />
        </FunderSearch>
        <FunderResultWrapper>
          {totals.grantees > 0 || totals.grants > 0 ? (
            <strong>
              Found {totals.grantees} grantees and {totals.grants} grants
            </strong>
          ) : (
            ''
          )}

          <ErrorMessage component={ErrorText} name="['awardee.number']" />

          <FunderResult results={awards && awards.length > 0}>
            {loadingOrg ? <Spinner /> : null}
            {awards && awards.length === 0 ? (
              <center>No results were found</center>
            ) : null}
            {awards &&
              awards.map((res, resIndex) => (
                <div key={res.person.pid}>
                  {res.awards[0].awardee.name}
                  <ul>
                    {res.awards.map((award, awardIndex) => (
                      <LiStyled key={award.number} onClick={event => {}}>
                        <Checkbox
                          checked={values['awardee.number'] === award.number}
                          name="checkbox"
                          onClick={event => {
                            if (event) {
                              setFieldValue("['awardee.number']", award.number)
                            } else {
                              setFieldValue("['awardee.number']", null)
                            }
                          }}
                          value={null}
                        />
                        {award.number} {award.title}
                      </LiStyled>
                    ))}
                  </ul>
                </div>
              ))}
          </FunderResult>
        </FunderResultWrapper>
      </FunderContainer>
    </form>
  )
}

const AccessForm = withFormik({
  handleSubmit: (values, { setSubmitting }) => {
    setSubmitting(true)
  },
  mapPropsToValues: () => ({
    number: '',
    'awardee.number': '',
    'awardee.name': '',
  }),
  validate: values => {
    const errors = {}

    if (values.organizationType === 'funder') {
      if (!values.number) errors.number = 'Award number is required'

      if (!values['awardee.number'])
        errors['awardee.number'] = 'You need to select one Grantee'

      if (!values['awardee.name'])
        errors['awardee.name'] = 'Fundation Awardee is required'
    }

    return errors
  },
})(Basic)

export default AccessForm
