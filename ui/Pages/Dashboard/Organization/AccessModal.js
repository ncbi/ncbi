/* eslint-disable react/prop-types */
import React, { useContext, useRef } from 'react'
import styled from 'styled-components'
import { useMutation } from '@apollo/client'
import PropTypes from 'prop-types'

import {
  Button,
  Modal,
  ModalBody as DefaultModalBody,
  ModalHeader,
  ModalFooter,
  Notification,
} from '../../../components'
import AccessForm from './AccessForm'
import ConstantsContext from '../../../../app/constantsContext'
import query from '../graphql'

const ModalBody = styled(DefaultModalBody)`
  flex-grow: 1;
  margin-bottom: 0;
`

const AccessModal = ({ currentUser, modalIsOpen, hideModal }) => {
  const [updateUserTeam] = useMutation(query.REQUEST_ACCESS_TO_ORGANISATION)
  const modalRef = useRef(null)
  const { s } = useContext(ConstantsContext)

  return (
    <Modal fullSize isOpen={modalIsOpen} onClose={hideModal} role="dialog">
      <ModalHeader hideModal={hideModal}>Organization Access</ModalHeader>

      <ModalBody>
        <AccessForm formikFormRef={modalRef} />
      </ModalBody>

      <ModalFooter>
        <Button
          data-test-id="request-access"
          onClick={async () => {
            const formData = await modalRef.current.triggerSave()

            if (formData) {
              updateUserTeam({
                variables: {
                  id: formData.organisation.teams.find(tm => tm.role === 'user')
                    .id,
                  members: [
                    {
                      user: { id: currentUser.id },
                      awardNumber: formData?.['awardee.number'] || null,
                      awardeeAffiliation: formData?.['awardee.name'] || null,
                    },
                  ],
                },
              })
                .then(() => {
                  Notification({
                    type: 'success',
                    title: 'Request Submitted',
                    message:
                      "You'll be notified when the organization accepts your request ",
                    duration: 6000,
                  })
                })
                .catch(res => {
                  console.error(res)

                  Notification({
                    message: 'Could not submit request',
                    type: 'danger',
                    title: 'Error',
                  })
                })

              hideModal()
            }
          }}
          status="primary"
        >
          {s('form.requestAccess')}
        </Button>

        <Button
          data-test-id="cancel-request-access"
          onClick={hideModal}
          outlined
          status="primary"
        >
          {s('form.cancel')}
        </Button>
      </ModalFooter>
    </Modal>
  )
}

AccessModal.propTypes = {
  hideModal: PropTypes.func.isRequired,
  modalIsOpen: PropTypes.bool,
}

AccessModal.defaultProps = {
  modalIsOpen: false,
}

export default AccessModal
