import React, { useContext } from 'react'
import styled from 'styled-components'
import { Redirect } from 'react-router-dom'
import { BooksCollectionsPage, OrganisationsList, AllUsersTab } from './Tabs'
import { Tabs } from '../../components'
import CurrentUserContext from '../../../app/userContext'
import PermissionsGate from '../../../app/components/PermissionsGate'

const Root = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;

  > div:first-child {
    flex-grow: 1;
  }
`

const SysAdminPage = props => {
  const { currentUser } = useContext(CurrentUserContext)

  if (!(currentUser?.auth.isSysAdmin || currentUser?.auth.isPDF2XMLVendor)) {
    return <Redirect to="/organizations" />
  }

  const { isPDF2XMLVendor } = currentUser.auth

  const BooksCollectionsTab = {
    key: '#books-collections',
    label: 'Books and collections',
    content: (
      <BooksCollectionsPage
        isPDF2XMLVendor={currentUser?.auth.isPDF2XMLVendor}
        props={props}
      />
    ),
  }

  const OrganizationTab = {
    key: '#organizations',
    label: 'Organizations',
    content: <OrganisationsList props={props} />,
  }

  const sections = [
    BooksCollectionsTab,
    OrganizationTab,
    {
      key: '#users',
      label: 'Users',
      content: (
        <PermissionsGate scopes={['view-user-tab']}>
          <AllUsersTab props={props} />
        </PermissionsGate>
      ),
    },
    // {
    //   key: '#error_managment',
    //   label: 'Error management',
    //   content: <div>Error management content</div>,
    // },
  ]

  /* Handle saving last visited in localstorage */
  const activeKey = localStorage.getItem(`dash-tab-key`) || sections[0].key
  const handleChangeTab = key => localStorage.setItem(`dash-tab-key`, key)

  return (
    <Root>
      <Tabs
        activeKey={activeKey}
        onChangeTab={handleChangeTab}
        tabItems={
          isPDF2XMLVendor ? [BooksCollectionsTab, OrganizationTab] : sections
        }
      />
    </Root>
  )
}

export default SysAdminPage
