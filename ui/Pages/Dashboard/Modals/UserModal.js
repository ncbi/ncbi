/* eslint-disable react/prop-types */
import React, { useRef, useContext } from 'react'
import styled from 'styled-components'

import {
  Button,
  ButtonGroup,
  Modal as UIModal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalMenu,
} from '../../../components'
import { getDisplayName } from '../../../common/utils'
import { UserForm } from '../Form'
import ConstantsContext from '../../../../app/constantsContext'
import PermissionsGate from '../../../../app/components/PermissionsGate'

const Modal = styled(UIModal)`
  max-height: 770px;
  max-width: 1000px;
`

export default props => {
  const {
    closeModal,
    isEditor,
    isModalOpen,
    isOrgAdmin,
    nextDisplayName,
    onClickNext,
    onClickPrevious,
    previousDisplayName,
    reject,
    rowData,
    updateUser,
    verify,
  } = props

  const { s } = useContext(ConstantsContext)

  const formRef = useRef(null)

  const userId = rowData.id
  const { email, givenName, surname, username } = rowData
  const isVerified = rowData.status !== 'unverified'
  const isEnabled = rowData.status === 'enabled'
  const isStatusDisabled = rowData.status === 'disabled'

  const headerText = !isVerified
    ? s('form.pendingUserRequest')
    : getDisplayName(rowData)

  const currentDisplayName = getDisplayName(rowData)

  const handleSave = () => {
    const formData = formRef.current.values
    updateUser(userId, formData)
  }

  const handleReject = () => {
    reject(userId)
  }

  const handleAccept = () => {
    verify(userId)
  }

  return (
    <Modal isOpen={isModalOpen} onClose={closeModal}>
      <ModalHeader hideModal={closeModal}>{headerText}</ModalHeader>

      <ModalMenu
        current={currentDisplayName}
        next={nextDisplayName && onClickNext}
        nextUser={nextDisplayName}
        prev={previousDisplayName && onClickPrevious}
        prevUser={previousDisplayName}
      />

      <ModalBody>
        <PermissionsGate
          errorProps={{ isReadonly: true }}
          scopes={['edit-user-modal']}
        >
          <UserForm
            email={email}
            givenName={givenName}
            innerRef={formRef}
            isEditor={isEditor}
            isEnabled={isEnabled}
            isOrgAdmin={isOrgAdmin}
            isStatusDisabled={isStatusDisabled}
            isVerified={isVerified}
            surname={surname}
            username={username}
          />
        </PermissionsGate>
      </ModalBody>

      <ModalFooter>
        <ButtonGroup>
          <Button onClick={closeModal} outlined status="primary">
            {s('form.cancel')}
          </Button>

          {!isVerified && (
            <PermissionsGate scopes={['accept-reject-user']}>
              <>
                <Button
                  data-test-id="accept-user-modal"
                  icon="check"
                  onClick={handleAccept}
                  status="primary"
                >
                  Accept user
                </Button>

                <Button
                  data-test-id="reject-user-modal"
                  icon="x"
                  onClick={handleReject}
                  status="danger"
                >
                  Reject user
                </Button>
              </>
            </PermissionsGate>
          )}

          {isVerified && (
            <Button
              data-test-id="saveUserModal"
              onClick={handleSave}
              status="primary"
            >
              {s('form.save')}
            </Button>
          )}
        </ButtonGroup>
      </ModalFooter>
    </Modal>
  )
}
