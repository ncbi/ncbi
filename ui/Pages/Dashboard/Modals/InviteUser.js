/* eslint-disable react/prop-types */
import React, { useState, useRef, useContext } from 'react'

import { ErrorText } from '@pubsweet/ui'

import {
  Button,
  Modal as CreateModal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from '../../../components'
import { InviteUserAdminForm, InviteUserOrgForm } from '../Form'
import ConstantsContext from '../../../../app/constantsContext'

export default ({ organization }) => {
  const [modalIsOpen, setIsOpen] = useState(false)
  const modalRef = useRef(null)
  const { s } = useContext(ConstantsContext)

  const closeModal = () => {
    setIsOpen(false)
  }

  return (
    <>
      <Button onClick={() => setIsOpen(true)} outlined status="primary">
        {organization ? s('form.inviteOrgAdmin') : s('form.inviteUser')}
      </Button>
      <CreateModal isOpen={modalIsOpen}>
        <ModalHeader hideModal={() => closeModal()}>
          {organization ? s('form.inviteOrgAdmin') : s('form.inviteUser')}
        </ModalHeader>

        <ModalBody style={{ maxHeight: organization ? '20vh' : '40vh' }}>
          <ErrorText>
            Attention: This form is not functional yet, you can only see the UI
          </ErrorText>
          {organization ? (
            <InviteUserOrgForm formikFormRef={modalRef} />
          ) : (
            <InviteUserAdminForm formikFormRef={modalRef} />
          )}
        </ModalBody>
        <ModalFooter>
          <Button
            data-test-id="saveUserModal"
            onClick={() => {
              const formData = modalRef.current.triggerSave()

              if (formData) {
                // Mutation here to invite user
                closeModal()
                // const message = s('notifications.succesUpdateUser').replace(
                //   '$name',
                //   savingValues.givenName,
                // )
                // Notification({ message })
              }
            }}
            status="primary"
          >
            {s('form.sendInvitation')}
          </Button>

          <Button onClick={() => closeModal()} outlined status="primary">
            {s('form.cancel')}
          </Button>
        </ModalFooter>
      </CreateModal>
    </>
  )
}
