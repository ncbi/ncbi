/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */

import React, { useState, useRef, useContext } from 'react'
import { useMutation } from '@apollo/client'
import { omit } from 'lodash'

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalMenu,
  Notification,
  Button,
  FixedLoader,
} from '../../../components'
import { getDisplayName } from '../../../common/utils'
import { UserGeneral } from '../Form'
import ConstantsContext from '../../../../app/constantsContext'
import query from '../graphql'
import {
  ORGANIZATION_USERS,
  UPDATE_USER,
} from '../../../../app/graphql/organization'

export default ({ editMode, organizationsList, rowData, setRowData }) => {
  const [modalIsOpen, setIsOpen] = useState(true)
  const modalRef = useRef(null)

  const [updateNcbiUser, { loading: updateingUser }] = useMutation(
    UPDATE_USER,
    {
      refetchQueries: [
        { query: query.GET_ALL_USERS },
        { query: ORGANIZATION_USERS },
        { query: query.GET_ORGANISATIONS },
      ],
    },
  )

  const { s } = useContext(ConstantsContext)

  const closeModal = () => {
    setRowData(null)
    setIsOpen(false)
  }

  const count = rowData.data.length - 1

  const prevProp =
    rowData.index !== 0
      ? e => {
          e.preventDefault()
          const prev = rowData.data[rowData.index - 1]

          setRowData({
            ...Object.assign(prev, {
              data: rowData.data,

              index: rowData.index - 1,
              organisation: rowData.organisation,
            }),
          })
        }
      : null

  const nextProp =
    count !== rowData.index
      ? e => {
          e.preventDefault()
          const next = rowData.data[rowData.index + 1]

          setRowData({
            ...Object.assign(next, {
              data: rowData.data,
              index: rowData.index + 1,
              organisation: rowData.organisation,
            }),
          })
        }
      : null

  return (
    <Modal isOpen={modalIsOpen} onClose={closeModal}>
      <ModalHeader hideModal={closeModal}>
        {getDisplayName(rowData)}
      </ModalHeader>

      <ModalMenu
        current={getDisplayName(rowData.data[rowData.index])}
        next={nextProp}
        nextUser={
          rowData.index + 1 < rowData.data.length
            ? getDisplayName(rowData.data[rowData.index + 1])
            : ''
        }
        prev={prevProp}
        prevUser={
          rowData.index - 1 >= 0
            ? getDisplayName(rowData.data[rowData.index - 1])
            : ''
        }
      />

      <ModalBody style={{ minHeight: '70vh' }}>
        <UserGeneral
          formikFormRef={modalRef}
          organizationsList={organizationsList}
          values={rowData}
        />
      </ModalBody>

      <ModalFooter>
        <Button
          data-test-id="saveUserModal"
          disabled={updateingUser}
          onClick={() => {
            const formData = modalRef.current.triggerSave(rowData)

            if (formData) {
              updateNcbiUser({
                variables: {
                  id: rowData.id,
                  input: omit(formData, 'organizations', 'id'),
                },
              })
                .then(res => {
                  closeModal()

                  const message = s('notifications.succesUpdateUser').replace(
                    '$name',
                    formData.givenName,
                  )

                  Notification({ message })
                })
                .catch(res => {
                  console.error(res)

                  Notification({
                    message: 'Could not update',
                    type: 'danger',
                    title: 'Error',
                  })
                })
            }
          }}
          status="primary"
        >
          {updateingUser ? 'Saving' : s('form.save')}
        </Button>

        <Button onClick={() => closeModal()} outlined status="primary">
          {updateingUser ? s('form.close') : s('form.cancel')}
        </Button>
      </ModalFooter>
      {updateingUser && <FixedLoader title="Updating user..." />}
    </Modal>
  )
}
