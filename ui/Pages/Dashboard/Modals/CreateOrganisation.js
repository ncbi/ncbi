/* eslint-disable react/prop-types */

import React, { useContext, useRef } from 'react'
import { useMutation } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Notification,
  Button,
  FixedLoader,
} from '../../../components'
import { FormOrganisation } from '../Form'
import ConstantsContext from '../../../../app/constantsContext'

import query from '../graphql'

const CreateOrganisationModal = props => {
  const { isOpen, onClose } = props

  const modalRef = useRef(null)
  const [createOrg, { loading }] = useMutation(query.CREATE_ORGANISATION)
  const { s } = useContext(ConstantsContext)

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalHeader hideModal={onClose}>
        {loading ? <>Creating organization</> : s('pages.button.newOrg')}
      </ModalHeader>

      <ModalBody>
        <FormOrganisation formikFormRef={modalRef} />
      </ModalBody>

      <ModalFooter>
        <Button
          data-test-id="save-new-organisation"
          disabled={loading}
          onClick={async () => {
            const data = await modalRef.current.triggerSave()

            if (data) {
              await createOrg({
                refetchQueries: [getOperationName(query.GET_ORGANISATIONS)],
                variables: { input: data },
              })
                .then(res => {
                  onClose()

                  const notification = {
                    message: s('notifications.successNewOrg'),
                    type: 'success',
                  }

                  Notification(notification)
                })
                .catch(res => {
                  const errDetail = res.graphQLErrors[0].extensions.detail
                  let errorMessage = ''

                  if (
                    typeof errDetail === 'string' ||
                    errDetail instanceof String
                  ) {
                    errorMessage =
                      `${errDetail}`.replace(/[<>]/g, "'") ||
                      s('notifications.errorNewOrg')
                  } else {
                    const keys = Object.keys(
                      res.graphQLErrors[0].extensions.detail,
                    )

                    const messages = []

                    if (keys.length > 0) {
                      keys.forEach(item => {
                        messages.push(
                          res.graphQLErrors[0].extensions.detail[item],
                        )
                      })
                    }

                    errorMessage = messages.join(x => `${x}<br/>`)
                  }

                  const notification = {
                    message: errorMessage,
                    type: 'danger',
                    title: res.graphQLErrors[0]?.message,
                  }

                  Notification(notification)
                })
            }
          }}
          status="primary"
        >
          {loading ? 'Saving...' : 'Save'}
        </Button>

        <Button
          data-test-id="cancel-new-organisation"
          disabled={loading}
          onClick={onClose}
          outlined
          status="primary"
        >
          {loading ? 'Close' : 'Cancel'}
        </Button>
      </ModalFooter>
      {!isOpen && loading && <FixedLoader title="Createing organization..." />}
    </Modal>
  )
}

export default CreateOrganisationModal
