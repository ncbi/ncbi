import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import BookSettingsTemplateChooser from './BookSettingsTemplateChooser'
import { Spinner, Tabs } from '../../components'
import PermissionsGate from '../../../app/components/PermissionsGate'
import {
  BooksCollectionsPage,
  SettingsOrganization,
  OrganizationUsersTab,
} from './Tabs'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;

  > div {
    height: 100%;
  }
`

const Organization = props => {
  const {
    canCreateBookCollection,
    canViewBooksCollectionsTab,
    canViewBookSettingsTemplatesTab,
    canViewMetadataTab,
    canViewSettingsTab,
    canViewUsersTab,
    checkAllOrganizationUsers,
    createTemplate,
    isEditor,
    isOrgAdmin,
    isPDF2XMLVendor,
    isPublisherOrganization,
    isSavingBookSettings,
    isSysAdmin,
    loading,
    loadingTemplates,
    loadingUsers,
    onChangeTab,
    onClickCreateBook,
    onClickCreateCollection,
    organization,
    rejectUser,
    savedTabKey,
    searchUsers,
    showCreateNewBook,
    showCreateNewCollection,
    templates,
    updateMemberTeams,
    updateTemplate,
    updateUser,
    users,
    verifyUser,
  } = props

  // store chosen template so that it doesn't disappear if you switch tabs
  const [chosenBookSettingsTemplate, setChosenBookSettingsTemplate] = useState(
    null,
  )

  const handleChooseBookSettingsTemplate = choice => {
    setChosenBookSettingsTemplate(choice)
  }

  if (loading) return <Spinner pageLoader />

  const { teams } = organization

  const booksCollectionsTab = {
    key: 'books-collections',
    label: 'Books and collections',
    content: (
      <BooksCollectionsPage
        canCreateBookCollection={canCreateBookCollection}
        isPDF2XMLVendor={isPDF2XMLVendor}
        onClickCreateBook={onClickCreateBook}
        onClickCreateCollection={onClickCreateCollection}
        organization={organization}
        showCreateNewBook={showCreateNewBook}
        showCreateNewCollection={showCreateNewCollection}
      />
    ),
  }

  const organizationUsersTab = {
    key: 'orgusers',
    label: 'Users',
    content: (
      <OrganizationUsersTab
        checkAllOrganizationUsers={checkAllOrganizationUsers}
        isPublisherOrganization={isPublisherOrganization}
        loading={loadingUsers}
        organizationTeams={teams}
        rejectUser={rejectUser}
        searchUsers={searchUsers}
        updateMemberTeams={updateMemberTeams}
        updateUser={updateUser}
        users={users}
        verifyUser={verifyUser}
      />
    ),
  }

  const settingsTab = {
    key: 'settings',
    label: 'Settings',
    content: (
      <PermissionsGate
        errorProps={{ readonly: true }}
        scopes={['edit-organization']}
      >
        <SettingsOrganization organization={organization} props={props} />
      </PermissionsGate>
    ),
  }

  const metadataTab = {
    key: 'metadataContent',
    label: 'Metadata Content',
    // eslint-disable-next-line react/no-unknown-property
    content: <div organization={organization}>Metadata content</div>,
  }

  const bookSettingsTemplatesTab = {
    key: 'bookSettingsTemplates',
    label: 'Book Templates',
    content: (
      <PermissionsGate scopes={['view-book-template']}>
        <BookSettingsTemplateChooser
          chapterProcessedTemplate={templates?.chapterProcessed}
          chosenTemplate={chosenBookSettingsTemplate}
          createTemplate={createTemplate}
          isEditor={isEditor}
          isOrgAdmin={isOrgAdmin}
          isSaving={isSavingBookSettings}
          isSysAdmin={isSysAdmin}
          loading={loadingTemplates}
          onChooseTemplate={handleChooseBookSettingsTemplate}
          updateTemplate={updateTemplate}
          wholeBookTemplate={templates?.wholeBook}
        />
      </PermissionsGate>
    ),
  }

  return (
    <Wrapper>
      <Tabs
        activeKey={savedTabKey || 'books-collections'}
        onChangeTab={onChangeTab}
        tabItems={[
          canViewBooksCollectionsTab && booksCollectionsTab,
          canViewSettingsTab && settingsTab,
          canViewBookSettingsTemplatesTab && bookSettingsTemplatesTab,
          canViewMetadataTab && metadataTab,
          canViewUsersTab && organizationUsersTab,
        ].filter(Boolean)}
      />
    </Wrapper>
  )
}

Organization.propTypes = {
  canCreateBookCollection: PropTypes.bool.isRequired,
  canViewBooksCollectionsTab: PropTypes.bool.isRequired,
  canViewBookSettingsTemplatesTab: PropTypes.bool.isRequired,
  canViewMetadataTab: PropTypes.bool.isRequired,
  canViewSettingsTab: PropTypes.bool.isRequired,
  canViewUsersTab: PropTypes.bool.isRequired,
  checkAllOrganizationUsers: PropTypes.func.isRequired,
  createTemplate: PropTypes.func.isRequired,
  isEditor: PropTypes.bool.isRequired,
  isOrgAdmin: PropTypes.bool.isRequired,
  isPDF2XMLVendor: PropTypes.bool.isRequired,
  isPublisherOrganization: PropTypes.bool.isRequired,
  isSysAdmin: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  loadingTemplates: PropTypes.bool.isRequired,
  loadingUsers: PropTypes.bool.isRequired,
  onChangeTab: PropTypes.func.isRequired,
  onClickCreateBook: PropTypes.func.isRequired,
  onClickCreateCollection: PropTypes.func.isRequired,
  rejectUser: PropTypes.func.isRequired,
  searchUsers: PropTypes.func.isRequired,
  showCreateNewBook: PropTypes.bool.isRequired,
  showCreateNewCollection: PropTypes.bool.isRequired,
  updateMemberTeams: PropTypes.func.isRequired,
  updateTemplate: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  verifyUser: PropTypes.func.isRequired,

  /* eslint-disable react/forbid-prop-types */
  organization: PropTypes.object,
  templates: PropTypes.object,
  users: PropTypes.object,
  /* eslint-enable react/forbid-prop-types */

  isSavingBookSettings: PropTypes.bool,
  savedTabKey: PropTypes.string,
}

Organization.defaultProps = {
  isSavingBookSettings: false,
  organization: null,
  savedTabKey: null,
  templates: null,
  users: null,
}

export default Organization
