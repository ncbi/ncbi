import React, { useContext, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'

import { th, grid } from '@pubsweet/ui-toolkit'

import ConstantsContext from '../../../app/constantsContext'
import BookSettings from '../../components/BookSettings'
import { Select, Spinner } from '../../components/common'

const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 800px;
  padding-top: 20px;
`

const LabelWrapper = styled.div`
  display: flex;
  justify-content: end;
  visibility: ${props => (props.show ? 'visible' : 'hidden')};

  > div {
    background: gray;
    border-radius: 2px;
    color: ${th('colorTextReverse')};
    font-size: ${th('fontSizeBaseVerySmall')};
    margin-right: ${grid(0.5)};
    padding: 0 6px;

    &:last-child {
      margin-right: 0;
    }
  }
`

const BookSettingsTemplateChooser = props => {
  const {
    className,
    innerRef, // will be passed on to the form

    chapterProcessedTemplate,
    chosenTemplate,
    createTemplate,
    isEditor,
    isOrgAdmin,
    isSaving,
    isSysAdmin,
    loading,
    onChooseTemplate,
    showSubmitButton,
    updateTemplate,
    wholeBookTemplate,
  } = props

  const { s } = useContext(ConstantsContext)
  const templateType = chosenTemplate

  let initialFormValues
  let isCustom
  let initialDomainId
  let initialDomainName

  if (chapterProcessedTemplate && wholeBookTemplate && templateType) {
    if (templateType === 'chapterProcessed') {
      initialFormValues = chapterProcessedTemplate.values
      isCustom = chapterProcessedTemplate.isCustom
      initialDomainId = chapterProcessedTemplate.domainId
      initialDomainName = chapterProcessedTemplate.domainName
    }

    if (templateType === 'wholeBook') {
      initialFormValues = wholeBookTemplate.values
      isCustom = wholeBookTemplate.isCustom
      initialDomainId = wholeBookTemplate.domainId
      initialDomainName = wholeBookTemplate.domainName
    }
  }

  const [domainId, setDomainId] = useState(initialDomainId || null)
  const [domainName, setDomainName] = useState(initialDomainName || null)

  // Make a new form key after saving, so that the initial values reset
  // and we get correct results of when the form is dirty from formik
  // (compared to the saved data, not the original rendered data)
  const [templateKey, setTemplateKey] = React.useState(null)

  useEffect(() => {
    // only when saving is done, otherwise you get a glitch where the form
    // looks like it was before your changes while in saving state
    // (it would still be functional, but it looks strange)
    if (!isSaving) {
      setTemplateKey(`${templateType}-${uuid()}`)

      setDomainId(initialDomainId)
      setDomainName(initialDomainName)
    }
  }, [chosenTemplate, isSaving])

  const handleTemplateChange = choice => {
    onChooseTemplate(choice.value)

    let template
    if (choice.value === 'chapterProcessed') template = chapterProcessedTemplate
    else if (choice.value === 'wholeBook') template = wholeBookTemplate

    setDomainId(template.domainId)
    setDomainName(template.domainName)
  }

  const handleSave = values => {
    let templateId, isTemplateCustom

    if (templateType === 'chapterProcessed') {
      templateId = chapterProcessedTemplate.id
      isTemplateCustom = chapterProcessedTemplate.isCustom
    }

    if (templateType === 'wholeBook') {
      templateId = wholeBookTemplate.id
      isTemplateCustom = wholeBookTemplate.isCustom
    }

    if (!isTemplateCustom) createTemplate(templateType, values)
    else updateTemplate(templateId, values)
  }

  const submitButtonLabel = `${isCustom ? 'Update' : 'Create'} custom template`

  const templateOptions = [
    {
      value: 'chapterProcessed',
      label: s('book.chapterProcessed'),
    },
    {
      value: 'wholeBook',
      label: s('book.wholeBook'),
    },
  ]

  const templateChooserDefaultValue = templateOptions.find(
    opt => opt.value === templateType,
  )

  if (loading) return <Spinner label="Fetching templates..." pageLoader />

  return (
    <Wrapper className={className}>
      <LabelWrapper show={!!templateType}>
        <div>
          {isCustom ? 'CUSTOM ORGANIZATION TEMPLATE' : 'NCBI DEFAULT TEMPLATE'}
        </div>

        {isCustom && domainId && domainName && (
          <>
            <div>DOMAIN ID: {domainId}</div>
            <div>DOMAIN NAME: {domainName}</div>
          </>
        )}
      </LabelWrapper>

      <Select
        defaultValue={templateChooserDefaultValue}
        disabled={isSaving}
        isSearchable={false}
        label="Template"
        onChange={handleTemplateChange}
        options={templateOptions}
        placeholder="Choose a type of template"
      />

      {templateType && (
        <BookSettings
          initialFormValues={initialFormValues}
          innerRef={innerRef}
          isEditor={isEditor}
          isOrgAdmin={isOrgAdmin}
          isSaving={isSaving}
          isSysAdmin={isSysAdmin}
          key={templateKey}
          onSubmit={handleSave}
          showSubmitButton={showSubmitButton}
          submitButtonLabel={submitButtonLabel}
          type={templateType}
          variant="template"
        />
      )}
    </Wrapper>
  )
}

BookSettingsTemplateChooser.propTypes = {
  chapterProcessedTemplate: PropTypes.shape({
    id: PropTypes.string,
    isCustom: PropTypes.bool.isRequired,
    domainId: PropTypes.string,
    domainName: PropTypes.string,
    /* eslint-disable-next-line react/forbid-prop-types */
    values: PropTypes.object.isRequired,
  }),

  wholeBookTemplate: PropTypes.shape({
    id: PropTypes.string,
    isCustom: PropTypes.bool.isRequired,
    domainId: PropTypes.string,
    domainName: PropTypes.string,
    /* eslint-disable-next-line react/forbid-prop-types */
    values: PropTypes.object.isRequired,
  }),

  // getTemplates: PropTypes.func.isRequired,
  isEditor: PropTypes.bool.isRequired,
  isOrgAdmin: PropTypes.bool.isRequired,
  isSysAdmin: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  onChooseTemplate: PropTypes.func.isRequired,

  createTemplate: PropTypes.func.isRequired,
  updateTemplate: PropTypes.func.isRequired,

  chosenTemplate: PropTypes.string,
  isSaving: PropTypes.bool,
  showSubmitButton: PropTypes.bool,
}

BookSettingsTemplateChooser.defaultProps = {
  chapterProcessedTemplate: null,
  chosenTemplate: null,
  isSaving: false,
  wholeBookTemplate: null,
  showSubmitButton: true,
}

export default BookSettingsTemplateChooser
