/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */

import React from 'react'
import { Select } from '@pubsweet/ui'
import styled from 'styled-components'

const OrgSelection = styled.div`
  display: flex;
  flex-direction: column;

  /* label {} */
`

const StyledSelect = styled(Select)`
  max-height: calc(8px * 8);
`

const OrganisationInput = ({
  field,
  form: { setFieldValue, validateField, touched, setTouched },
  options,
  getOrganisations,
  ...props
}) => (
  <OrgSelection>
    <label>Select organization *</label>
    <StyledSelect
      cacheOptions
      className="basic-single"
      classNamePrefix="select"
      defaultOptions
      isClearable
      isMulti={props.isMulti}
      name="search"
      // eslint-disable-next-line consistent-return
      onChange={selected => {
        if (selected) {
          setFieldValue(
            field.name,
            props.isMulti
              ? selected.map(select => select.value)
              : selected.value,
          )
        } else {
          setFieldValue(field.name, null)
          return null
        }
      }}
      onInputChange={value =>
        getOrganisations({
          variables: {
            input: {
              skip: 0,
              take: -1,
              search: {
                criteria: [{ field: 'name', operator: { contains: value } }],
              },
            },
          },
        })
      }
      onMenuClose={() => {
        setTouched({ ...touched, organisationId: true })
        validateField(field.name)
      }}
      options={options}
      placeholder="Type your organization"
    />
  </OrgSelection>
)

export default OrganisationInput
