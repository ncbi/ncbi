/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/label-has-for */
import React, { useImperativeHandle } from 'react'
import { withFormik, Form, Field } from 'formik'

import { Column, ColumnWraper, TextFieldComponent } from '../../../components'
// import ConstantsContext from '../../../../../app/constantsContext'

const Basic = ({
  submitForm,
  handleSubmit,
  formikFormRef,
  values,
  isValid,
}) => {
  // const { s } = useContext(ConstantsContext)

  useImperativeHandle(formikFormRef, () => ({
    triggerSave: () => {
      if (isValid) {
        submitForm()
        return values
      }

      return false
    },
  }))

  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <ColumnWraper>
          <Column grow={0}>
            <Field
              component={TextFieldComponent}
              label="Email *"
              name="email"
            />
          </Column>
        </ColumnWraper>
      </Form>
    </div>
  )
}

const UserForm = withFormik({
  enableReinitialize: true,
  handleSubmit: (values, { setSubmitting }) => {
    setSubmitting(false)
  },

  mapPropsToValues: ({ values }) => {
    return {
      email: values?.email || '',
    }
  },

  validate: values => {
    const errors = {}

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email))
      errors.email = 'Invalid email address'

    if (!values.email) errors.email = 'Email is Required'
    return errors
  },

  validateOnChange: true,
})(Basic)

export default UserForm
