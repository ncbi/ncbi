/* eslint-disable react/prop-types */
/* stylelint-disable no-descending-specificity */

import React from 'react'
import styled from 'styled-components'
import { Formik, Form } from 'formik'
import * as yup from 'yup'

import { grid } from '@pubsweet/ui-toolkit'

import {
  TextFieldComponent as UITextField,
  ToggleComponent as UIToggle,
} from '../../../components/FormElements/FormikElements'
import PermissionsGate from '../../../../app/components/PermissionsGate'

// #region styled
const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 500px;
  padding-bottom: 50px;
  padding-top: 20px;

  > form > div:not(:last-child) {
    margin-bottom: ${grid(2)};
  }
`

const TextField = styled(UITextField)`
  margin-bottom: 0;
`

const ToggleGroup = styled.div`
  > div:first-child {
    font-weight: bold;
    margin-bottom: ${grid(1)};
  }
`

const Toggle = styled(UIToggle)`
  max-width: 500px;
`
// #endregion styled

const validations = yup.object().shape({
  givenName: yup.string().required('Given name is required'),
  surname: yup.string().required('Surname is required'),
  username: yup.string().required('Username is required'),
  email: yup
    .string()
    .email('Not a valid email address')
    .required('Email is required'),
})

const Basic = props => {
  const {
    email,
    givenName,
    innerRef,
    isEditor,
    isEnabled,
    isOrgAdmin,
    isVerified,
    surname,
    username,
    isReadonly,
  } = props

  const readOnly = isReadonly

  const initialValues = {
    givenName: givenName || '',
    surname: surname || '',
    username: username || '',
    email: email || '',
    orgAdmin: isOrgAdmin || false,
    editor: isEditor || false,
    enabled: isEnabled || false,
  }

  const enableEditRole = PermissionsGate({
    scopes: ['edit-role-orgAdmin'],
    getPermission: true,
  })

  const enableUser = PermissionsGate({
    scopes: ['enable-disable-user'],
    getPermission: true,
  })

  return (
    <Wrapper>
      <Formik
        enableReinitialize
        initialValues={initialValues}
        innerRef={innerRef}
        validationSchema={validations}
      >
        {formProps => {
          const { values } = formProps

          return (
            <Form noValidate>
              <TextField
                disabled={readOnly}
                field={{ name: 'givenName', value: values.givenName }}
                form={formProps}
                label="Given name"
                required
              />

              <TextField
                disabled={readOnly}
                field={{ name: 'surname', value: values.surname }}
                form={formProps}
                label="Surname"
                required
              />

              <TextField
                disabled={readOnly}
                field={{ name: 'username', value: values.username }}
                form={formProps}
                label="Username"
                required
              />

              <TextField
                disabled={readOnly}
                field={{ name: 'email', value: values.email }}
                form={formProps}
                label="Email"
                required
              />

              {/* {isFunderOrganization && (
                <>
                  <TextField
                    field={{ name: 'awardNumber', value: values.awardNumber }}
                    form={formProps}
                    label="Verification award number"
                  />

                  <TextField
                    field={{
                      name: 'awardeeAffiliation',
                      value: values.awardeeAffiliation,
                    }}
                    form={formProps}
                    label="Institutional affiliations"
                  />
                </>
              )} */}

              <ToggleGroup>
                <div htmlFor="team">Organization Roles</div>

                <div>
                  <Toggle
                    disabled={!enableEditRole}
                    field={{ name: 'orgAdmin', value: values.orgAdmin }}
                    form={formProps}
                    label="Org admin"
                  />
                  <Toggle
                    disabled={!enableEditRole}
                    field={{ name: 'editor', value: values.editor }}
                    form={formProps}
                    label="Editor"
                  />
                </div>
              </ToggleGroup>

              {isVerified && (
                <ToggleGroup>
                  <div>Organization membership</div>

                  <div>
                    <Toggle
                      disabled={!enableUser}
                      field={{ name: 'enabled', value: values.enabled }}
                      form={formProps}
                      label="Enabled"
                    />
                  </div>
                </ToggleGroup>
              )}
            </Form>
          )
        }}
      </Formik>
    </Wrapper>
  )
}

export default Basic
