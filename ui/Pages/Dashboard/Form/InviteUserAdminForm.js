/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/label-has-for */
import React, { useImperativeHandle, useContext, useState } from 'react'
import { withFormik, Form, Field } from 'formik'
import { Radio } from '@pubsweet/ui'
import {
  Column,
  ColumnWraper,
  TextFieldComponent,
  SelectList,
} from '../../../components'
import ConstantsContext from '../../../../app/constantsContext'

const Basic = ({
  submitForm,
  handleSubmit,
  formikFormRef,
  values,
  isValid,
  setFieldValue,
}) => {
  const { s } = useContext(ConstantsContext)

  useImperativeHandle(formikFormRef, () => ({
    triggerSave: () => {
      if (isValid) {
        submitForm()
        return values
      }

      return false
    },
  }))

  const [checkedOption, setCheckedOption] = useState('sysAdmin')

  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <ColumnWraper>
          <Column>
            <Field
              component={TextFieldComponent}
              label="Email *"
              name="email"
            />

            <div> BCMS access</div>
            <br />
            <Radio
              checked={checkedOption === 'sysAdmin'}
              label={s('form.sysAdmin')}
              name="role"
              onChange={() => setCheckedOption('sysAdmin')}
            />
            <Radio
              checked={checkedOption === 'pdf2xmlVendor'}
              label={s('form.pdf2xmlVendor')}
              name="role"
              onChange={() => setCheckedOption('pdf2xmlVendor')}
            />
          </Column>

          <Column grow={0}>
            {checkedOption === 'sysAdmin' ? (
              <SelectList
                defaultSelection={[]}
                description="System admins have access to all organizations. Selecting specific organizations here will notify this user of the organization's activity."
                label="Organization access"
                onChange={value => {
                  // setFieldValue('organizations', [])
                }}
                options={[]}
                placeholder="Select organizations"
              />
            ) : null}
          </Column>
        </ColumnWraper>
      </Form>
    </div>
  )
}

const UserForm = withFormik({
  enableReinitialize: true,
  handleSubmit: (values, { setSubmitting }) => {
    setSubmitting(false)
  },

  mapPropsToValues: ({ values }) => {
    return {
      email: values?.email || '',
    }
  },

  validate: values => {
    const errors = {}

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email))
      errors.email = 'Invalid email address'

    if (!values.email) errors.email = 'Email is Required'
    return errors
  },

  validateOnChange: true,
})(Basic)

export default UserForm
