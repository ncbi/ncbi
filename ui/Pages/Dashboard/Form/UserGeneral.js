/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */

import React, { useContext, useImperativeHandle } from 'react'
import { pick, cloneDeep, omit } from 'lodash'
import { withFormik, Form, Field } from 'formik'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'

import { Link as UILink, Checkbox } from '@pubsweet/ui'
import { grid, th } from '@pubsweet/ui-toolkit'

import ConstantsContext from '../../../../app/constantsContext'
import {
  Column,
  ColumnWraper as CommonColumnWrapper,
  TextFieldComponent,
  SelectList,
  Icon,
} from '../../../components'
import PermissionsGate from '../../../../app/components/PermissionsGate'

const Organization = styled.div`
  border-left: 3px solid transparent;
  margin-bottom: ${grid(1)};
  padding-left: 4px;
  transition: border-color 0.2s ease-in-out;

  &:hover {
    border-color: ${th('colorBorder')};
  }
`

const OrgMembershipWrapper = styled.div`
  > div:first-child {
    font-weight: bold;
    margin-bottom: ${grid(1)};
  }
`

const OrganizationName = styled.div`
  display: flex;
  margin-bottom: ${grid(0.5)};
`

const Wrapper = styled.div`
  > form > div:not(:last-child) {
    border-bottom: 1px solid ${th('colorFurniture')};
    margin-bottom: ${grid(2)};
    padding-bottom: ${grid(2)};
  }
`

const Link = styled(UILink)`
  color: ${th('colorText')};
  text-decoration: none;
`

const RolesHeader = styled.div`
  font-weight: bold;
  margin-bottom: ${grid(1)};
`

const Member = styled.span`
  background-color: ${th('colorSecondary')};
  border-radius: 5px;
  color: white;
  padding: 2px 6px;
`

const Banner = styled.span`
  background: ${th('colorFurniture')};
  border-radius: 5px;
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  margin-left: ${grid(1)};
  padding: 2px 6px;
`

const RoleList = styled.div`
  font-size: ${th('fontSizeBaseSmall')};

  > span:not(:last-child) {
    margin-right: ${grid(0.5)};
  }
`

const ColumnWrapper = styled(CommonColumnWrapper)`
  padding: 0;
`

const Basic = ({
  submitForm,
  handleSubmit,
  formikFormRef,
  values,
  isValid,
  setFieldValue,
  organizationsList,
  readonly,
}) => {
  const { s } = useContext(ConstantsContext)

  useImperativeHandle(formikFormRef, () => ({
    triggerSave: (userForm = {}) => {
      if (isValid) {
        submitForm()

        values.teams = values.teams.map(tm =>
          pick(tm, ['id', 'role', 'name', 'members']),
        )

        const teams = []

        values.teams.forEach(x => {
          const mem = []

          x.members.forEach(u => {
            mem.push(omit(u, 'user.username'))
          })

          teams.push({ ...x, members: mem })
        })

        values.teams = teams

        return values
      }

      return false
    },
  }))

  const selectedOrganizations = values.teams
    .filter(x => x.members.findIndex(mem => mem.user.id === values.id) > -1)
    .map(tm => ({
      label: organizationsList.find(org => org?.id === tm?.object?.objectId)
        ?.name,
      value: tm?.object?.objectId,
    }))

  const removeAdminRoleInOrganizations = () => {
    const teams = []

    selectedOrganizations.forEach(optSelected => {
      organizationsList.forEach(org => {
        const organisation = cloneDeep(org)

        const tm = organisation.teams.find(t => t.role === 'sysAdmin')

        if (optSelected.value === organisation.id) {
          const userAdminOrg = tm.members.find(v => v.user.id === values.id)

          if (userAdminOrg) {
            const filteredMembers = tm.members.filter(
              v => v.user.id !== values.id,
            )

            tm.members = filteredMembers
          }

          teams.push(tm)
        }
      })
    })

    values.teams.forEach(item => {
      const tmCloned = cloneDeep(item)

      if (!teams.find(tm => tm.id === tmCloned.id)) {
        tmCloned.members = tmCloned.members.filter(
          mb => mb.user.id !== values.id,
        )

        teams.push(tmCloned)
      }
    })

    setFieldValue('teams', teams)
  }

  return (
    <Wrapper>
      <Form onSubmit={handleSubmit}>
        <ColumnWrapper>
          <Column>
            <Field
              component={TextFieldComponent}
              label="Given Name *"
              name="givenName"
              readonly={readonly}
            />

            <Field
              component={TextFieldComponent}
              label="Username *"
              name="username"
              readonly={readonly}
            />
          </Column>

          <Column>
            <Field
              component={TextFieldComponent}
              label="Surname *"
              name="surname"
              readonly={readonly}
            />

            <Field
              component={TextFieldComponent}
              label="Email *"
              name="email"
              readonly={readonly}
            />
          </Column>

          <Column />
        </ColumnWrapper>

        <OrgMembershipWrapper>
          <div>Organization Membership</div>
          {organizationsList.map(org => (
            <Organization key={org.id}>
              {getUserRoles(values.id, org.teams) !== null && (
                <>
                  <OrganizationName>
                    <Link to={`/organizations/${org.id}`}>
                      {` ${org.name}`} <Icon size={2}>link</Icon>
                    </Link>

                    <div>
                      {hasMembershipStatus(
                        values.id,
                        org.teams,
                        'unverified',
                      ) && <Banner>membership pending</Banner>}
                    </div>

                    <div>
                      {hasMembershipStatus(
                        values.id,
                        org.teams,
                        'disabled',
                      ) && <Banner>membership disabled</Banner>}
                    </div>
                  </OrganizationName>

                  {!hasMembershipStatus(values.id, org.teams, 'unverified') && (
                    <RoleList>
                      {getUserRoles(values.id, org.teams).length === 0 && (
                        <div>
                          There are no roles associated with this user in this
                          organization
                        </div>
                      )}

                      {getUserRoles(values.id, org.teams).length > 0 &&
                        getUserRoles(values.id, org.teams).map(role => (
                          <Member key={uuid()}>{role}</Member>
                        ))}
                    </RoleList>
                  )}
                </>
              )}
            </Organization>
          ))}
        </OrgMembershipWrapper>

        <ColumnWrapper>
          <Column>
            <RolesHeader>BCMS role</RolesHeader>
            <PermissionsGate scopes={['edit-role-sysadmin']}>
              <Checkbox
                checked={values.admin}
                label={s('form.sysAdmin')}
                name="admin"
                onChange={element => {
                  const newAdminValue = !values.admin

                  if (newAdminValue === true)
                    setFieldValue('pdf2xmlVendor', false)

                  setFieldValue('admin', newAdminValue)

                  if (!newAdminValue) removeAdminRoleInOrganizations()
                }}
              />
            </PermissionsGate>
            <PermissionsGate scopes={['edit-role-PDF2XML']}>
              <Checkbox
                checked={values.pdf2xmlVendor}
                label={s('form.pdf2xmlVendor')}
                name="pdf2xmlVendor"
                onChange={() => {
                  const newVendorValue = !values.pdf2xmlVendor

                  if (newVendorValue === true) {
                    setFieldValue('admin', false)
                    removeAdminRoleInOrganizations()
                  }

                  setFieldValue('pdf2xmlVendor', newVendorValue)

                  const teams = []

                  values.teams.forEach(team => {
                    const tmCloned = cloneDeep(team)

                    tmCloned.members = tmCloned.members.filter(
                      mb => mb.user.id !== values.id,
                    )

                    teams.push(tmCloned)
                  })

                  setFieldValue('teams', teams)
                }}
              />
            </PermissionsGate>
          </Column>

          <Column grow={0}>
            {values.admin ? (
              <SelectList
                defaultSelection={selectedOrganizations}
                description="System admins have access to all organizations. Selecting specific organizations here will notify this user of the organization's activity."
                label="Organization access"
                onChange={optionsSelected => {
                  const teams = []

                  optionsSelected.forEach(optSelected => {
                    organizationsList.forEach(org => {
                      const organisation = cloneDeep(org)

                      const tm = organisation.teams.find(
                        t => t.role === 'sysAdmin',
                      )

                      if (optSelected.value === organisation.id) {
                        if (!tm.members.find(v => v.user.id === values.id)) {
                          tm.members.push({
                            status: 'enabled',
                            user: { id: values.id, username: values.label },
                          })
                        }

                        teams.push(tm)
                      }
                    })
                  })

                  values.teams.forEach(item => {
                    const tmCloned = cloneDeep(item)

                    if (!teams.find(tm => tm.id === tmCloned.id)) {
                      tmCloned.members = tmCloned.members.filter(
                        mb => mb.user.id !== values.id,
                      )

                      teams.push(tmCloned)
                    }
                  })

                  setFieldValue('teams', teams)
                }}
                options={organizationsList.map(x => ({
                  label: x.name,
                  value: x.id,
                }))}
                placeholder="Select organizations"
              />
            ) : null}
          </Column>
        </ColumnWrapper>
      </Form>
    </Wrapper>
  )
}

const hasMembershipStatus = (userId, teams, status) => {
  const userRole = teams.find(tm => tm.role === 'user')

  if (userRole.members?.find(x => x.user.id === userId)?.status === status)
    return true

  return false
}

const getUserRoles = (userId, teams) => {
  const roles = []
  const userTeam = teams.find(tm => tm.role === 'user')

  // not a member
  if (userTeam.members?.findIndex(x => x.user.id === userId) === -1) return null

  // rejected member
  if (hasMembershipStatus(userId, teams, 'rejected')) return null

  teams.forEach(team => {
    team.members &&
      team.members.forEach(item => {
        if (item.user.id === userId) {
          roles.push(team.name)
        }
      })
  })

  return roles.filter(x => !x.includes('Org User'))
}

const UserForm = withFormik({
  enableReinitialize: true,
  handleSubmit: (values, { setSubmitting }) => {
    setSubmitting(false)
  },

  mapPropsToValues: ({ values, organizationsList }) => {
    return {
      id: values.id,
      email: values.email || '',
      givenName: values.givenName || '',
      surname: values.surname || '',
      username: values.username || '',
      pdf2xmlVendor: values.pdf2xmlVendor || false,
      admin: values.admin || false,
      teams: organizationsList
        .map(org => {
          const teams = org.teams.find(
            tm =>
              tm.role === 'sysAdmin' &&
              tm.members.find(mb => mb.user.id === values.id),
          )

          if (!teams) return null
          const teamCopy = cloneDeep(teams)
          const members = []

          teamCopy.members.forEach(mb =>
            members.push({
              user: {
                id: mb.user.id,
              },
            }),
          )

          teamCopy.members = members
          return teamCopy
        })
        .filter(el => el != null),
    }
  },

  validate: values => {
    const errors = {}

    if (!values.givenName) errors.givenName = 'Given name is Required'

    if (!values.surname) errors.surname = 'Surname is Required'

    if (!values.username) errors.username = 'Username is Required'

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email))
      errors.email = 'Invalid email address'

    if (!values.email) errors.email = 'Email is Required'

    return errors
  },

  validateOnChange: true,
})(Basic)

export default UserForm
