/* eslint-disable react/prop-types */

import React, { useContext, useImperativeHandle } from 'react'
import { useQuery } from '@apollo/client'
import { withFormik, Form, Field } from 'formik'
import { ErrorText } from '@pubsweet/ui'
import { isEmpty, pick, cloneDeep } from 'lodash'
import query from '../graphql'

import ConstantsContext from '../../../../app/constantsContext'
import {
  TextFieldComponent,
  ToggleComponent,
  ColumnWraper,
  Column,
  SelectList,
} from '../../../components'

const Basic = ({
  submitForm,
  handleSubmit,
  readonly,
  values,
  validateForm,
  formikFormRef,
  setFieldValue,
  errors,
  touched,
  disableAbbreviation,
}) => {
  const isTouchedType =
    touched?.settings?.type?.publisher || touched?.settings?.type?.funder

  useImperativeHandle(formikFormRef, () => ({
    triggerSave: async () => {
      submitForm()
      const isValid = await validateForm()

      if (isEmpty(isValid)) {
        const formValues = cloneDeep(values)

        if (formValues.teams.length > 0 && formValues.teams[0].members) {
          // eslint-disable-next-line no-param-reassign
          formValues.teams[0].members = formValues.teams[0].members.map(
            member => ({
              ...member,
              user: pick(member.user, ['id']),
            }),
          )

          // eslint-disable-next-line no-param-reassign
          delete formValues.teams[0].id
        }

        return formValues
      }

      return false
    },
  }))

  const { s } = useContext(ConstantsContext)

  const { data, loading } = useQuery(query.GET_ALL_USERS, {
    variables: {
      input: {
        sort: {
          direction: 'asc',
          field: ['givenName', 'lastName', 'username'],
        },
      },
    },
  })

  return (
    <Form onSubmit={handleSubmit}>
      <ColumnWraper>
        <Column grow={0}>
          <Field
            component={TextFieldComponent}
            disabled={readonly}
            label={`${s('form.label.orgName')} *`}
            name="name"
          />
          {values.publisherId && (
            <Field
              component={TextFieldComponent}
              disabled={readonly}
              label={s('form.publisherID')}
              name="publisherId"
            />
          )}
        </Column>

        <Column grow={0}>
          <Field
            component={TextFieldComponent}
            disabled={readonly || disableAbbreviation}
            label={`${s('form.abbreviatedName')} *`}
            name="abbreviation"
          />
        </Column>

        <Column />
      </ColumnWraper>

      <ColumnWraper>
        <Column grow={0}>
          <div>Bookshelf participant type</div>
          <Field
            component={ToggleComponent}
            disabled={readonly}
            label={s('form.label.publisher')}
            labelPosition="left"
            name="settings.type.publisher"
          />
          <Field
            component={ToggleComponent}
            disabled={readonly}
            label={s('form.label.funder')}
            labelPosition="left"
            name="settings.type.funder"
          />
          {isTouchedType && errors.orgtype && (
            <ErrorText>{errors.orgtype}</ErrorText>
          )}
        </Column>
        <Column grow={0}>
          Create collection type
          <Field
            component={ToggleComponent}
            disabled={readonly}
            label={s('form.label.fundedCollection')}
            labelPosition="left"
            name="settings.collections.funded"
          />
          <Field
            component={ToggleComponent}
            disabled={readonly}
            label={s('form.label.bookSeriesCollection')}
            labelPosition="left"
            name="settings.collections.bookSeries"
          />
        </Column>
        <Column />
      </ColumnWraper>

      <ColumnWraper>
        <Column grow={0}>
          {s('form.acceptedSourceSubmissions')}

          <Field
            component={ToggleComponent}
            disabled={readonly}
            label={s('form.wordBook')}
            labelPosition="left"
            name="settings.word"
          />

          <Field
            component={ToggleComponent}
            disabled={readonly}
            label={s('form.pdfBook')}
            labelPosition="left"
            name="settings.pdf"
          />

          <Field
            component={ToggleComponent}
            disabled={readonly}
            label={s('form.xmlBook')}
            labelPosition="left"
            name="settings.xml"
          />
        </Column>

        <Column grow={0}>
          <SelectList
            defaultSelection={values.teams[0]?.members?.map(member => ({
              value: member.user.id,
              label: member.user.username,
            }))}
            description={s('form.NCBISystemAdminsDescription')}
            disabled={readonly}
            label={s('form.NCBISystemAdmins')}
            onChange={value => {
              if (values.teams.length > 0) {
                setFieldValue('teams', [
                  {
                    ...values.teams[0],
                    members: value.map(v => ({
                      status: 'enabled',
                      user: { id: v.value, username: v.label },
                    })),
                  },
                ])
              } else {
                setFieldValue('teams', [
                  {
                    role: 'sysAdmin',
                    members: value.map(v => ({
                      status: 'enabled',
                      user: { id: v.value, username: v.label },
                    })),
                  },
                ])
              }
            }}
            options={
              !loading
                ? data?.searchUsers.results
                    ?.filter(user => user.auth.isSysAdmin)
                    ?.map(item => ({
                      value: item.id,
                      label:
                        item.givenName && item.surname
                          ? `${item.givenName} ${item.surname}`
                          : item.username,
                    }))
                : []
            }
            placeholder={s('form.selectUsers')}
            secondaryLabel={s('form.username')}
          />
        </Column>
        <Column />
      </ColumnWraper>
    </Form>
  )
}

const FormOrganisation = withFormik({
  handleSubmit: (values, { setSubmitting }) => {
    setSubmitting(false)
  },
  mapPropsToValues: ({ values }) => {
    return {
      name: (values || {}).name || null,
      abbreviation: (values || {}).abbreviation || null,
      publisherId: values?.publisherId,
      teams: ((values || {}).teams || []).filter(tm => tm.role === 'sysAdmin'),
      settings: {
        xml: values?.settings?.xml || false,
        pdf: values?.settings?.pdf || false,
        word: values?.settings?.word || false,
        collections: {
          funded: values?.settings.collections?.funded || false,
          bookSeries: values?.settings.collections?.bookSeries || false,
        },
        type: {
          publisher: values?.settings?.type?.publisher || false,
          funder: values?.settings?.type?.funder || false,
        },
      },
    }
  },
  validate: values => {
    const errors = {}

    if (!values.name) errors.name = 'Name is required'

    if (values.abbreviation?.length > 19) {
      errors.abbreviation = "Abbreviated name can't be more than 20 characters."
    }

    if (!values.abbreviation) {
      errors.abbreviation = 'Abbreviated name is required'
    }

    if (values.abbreviation && !/^[a-zA-Z][\w-]+$/i.test(values.abbreviation)) {
      errors.abbreviation =
        'Abbreviated name may only contain letters, numbers, underscore, and hyphen character'
    }

    if (!(values.settings.type.publisher || values.settings.type.funder)) {
      errors.orgtype = 'Please select at least one option'
    }

    return errors
  },
})(Basic)

export default FormOrganisation
