/* eslint-disable react/no-danger */
/* eslint-disable no-underscore-dangle */
/* eslint-disable react/prop-types */

import React, { useContext, useState } from 'react'
import styled from 'styled-components'
import { debounce, uniq } from 'lodash'
import moment from 'moment'
import { useHistory, useParams } from 'react-router-dom'

import { grid } from '@pubsweet/ui-toolkit'

import query from '../graphql'
import ConstantsContext from '../../../../app/constantsContext'
import { Table } from '../../../components/Datatable'
// import DisplayInlineEditor from '../../../components/wax/DisplayInlneEditor'

import {
  Button,
  ButtonGroup,
  Checkbox,
  DocumentIcon,
  Status,
  SvgIcon,
  TableWithToolbar,
} from '../../../components'
import { getLongDate, getVersionTitle } from '../../../common/utils'

import Select from '../../../components/common/Select'
import Search from '../../../components/common/Search'
import { ErrorIcon, QueryIcon } from '../../../components/Icons'
// import CurrentUserContext from '../../../../app/userContext'

import { DeleteBulk } from '../BulkActions'

// #region styled

const Root = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

const Top = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: ${grid(1)};
`

const FiltersRow = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: ${grid(1)} ${grid(2)};
`

const Filter = styled(Select)`
  width: 200px;

  &:not(:last-child) {
    margin-right: ${grid(1)};
  }
`

const StyledCheckbox = styled(Checkbox)`
  margin-right: ${grid(2)};
`

const ButtonsWrapper = styled.div`
  align-items: center;
  display: flex;
  justify-content: ${props =>
    props.showLeftSide ? 'space-between' : 'flex-end'};
  padding: 0 ${grid(2)};
`

const RightContainer = styled.div`
  display: flex;
`

const TitleWrap = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  line-height: 18px;
  min-height: 50px;
  width: 100%;

  span {
    color: #5c5a5a;
    font-size: 14px;
  }
`

const Secondary = styled.div`
  display: flex;
  width: 100%;
`

const Title = styled.div`
  display: flex;
  flex-direction: column;
`

const MainTitle = styled.div`
  font-weight: 600;
`

const Subtitles = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 14px;
`

const VersionWrapper = styled.div`
  flex-basis: 40%;
`

const Version = styled.div`
  background-color: #f2f2f2;
  border-radius: 3px;
  color: '#000000';
  font-size: 14px;
  font-weight: bold;
  justify-content: center;
  letter-spacing: 0.5px;
  padding: 4px;
  width: fit-content;
`

// #endregion styled

const BooksCollectionsPage = ({
  canCreateBookCollection,
  isPDF2XMLVendor,
  onClickCreateBook,
  onClickCreateCollection,
  organization,
  showCreateNewBook,
  showCreateNewCollection,
}) => {
  const history = useHistory()
  const { organizationId } = useParams()

  const { s } = useContext(ConstantsContext)
  const delay = 1000

  const [searchValue, setSearchValue] = useState('')
  const [showFilters, setShowFilters] = useState(false)
  const [selectedWorkflow, setSelectedWorkflow] = useState(null)
  const [selectedType, setSelectedType] = useState(null)
  const [selectedStatus, setSelectedStatus] = useState(null)

  const bookQuery = query.GET_DASHBOARD

  const getErrorAsignees = (errors = [], chapterIndependently) => {
    const asignes = errors
      .filter(x => x.severity === 'error')
      .map(x => x.assignee)

    const unique = uniq(asignes).filter(x => x && x.toLowerCase() !== 'unknown')

    if (unique.length) {
      return chapterIndependently ? (
        <ErrorIcon title="Chapters have errors" />
      ) : (
        <>
          <ErrorIcon /> {unique.join(' | ')}
        </>
      )
    }

    return ''
  }

  const getWarnings = (errors = [], chapterIndependently) => {
    const warnings = errors.filter(err => err.severity === 'query')

    if (warnings.length) {
      return chapterIndependently ? (
        <QueryIcon title="Chapters have errors with severity Query" />
      ) : (
        <Status status="query" />
      )
    }

    return ''
  }

  const columns = [
    {
      cellRenderer: () => ({ cellData }) =>
        cellData && <DocumentIcon format={cellData} />,
      dataKey: 'workflow',
      disableSort: true,
      width: 120,
      label: 'Source',
    },
    {
      cellRenderer: () => ({ rowData }) => {
        let iconName = ''
        const typeBook = rowData.__typename?.toLowerCase()

        if (typeBook === 'book')
          iconName = rowData.settings.chapterIndependently
            ? 'bookWithChapters'
            : 'book'

        if (typeBook === 'collection') {
          iconName = 'collection'
        }

        return <SvgIcon name={iconName} />
      },
      dataKey: 'workflow',
      disableSort: true,
      width: 100,
      label: 'Type',
    },
    {
      cellRenderer: () => ({ cellData, rowData }) => (
        <TitleWrap>
          {/* <DisplayInlineEditor content={cellData} /> breaking responsivity */}
          <Title>
            <MainTitle>
              <div dangerouslySetInnerHTML={{ __html: cellData }} />
            </MainTitle>
            <Subtitles>
              {(rowData.subTitle || []).map(sub => (
                <div dangerouslySetInnerHTML={{ __html: sub }} key={sub} />
              ))}
            </Subtitles>
          </Title>
          <Secondary>
            <VersionWrapper>
              {rowData.fundedContentType && (
                <Version>{getVersionTitle(rowData)}</Version>
              )}
            </VersionWrapper>
            <div data-test-id={rowData.organisation?.name}>
              {rowData.organisation?.name}
            </div>
          </Secondary>
        </TitleWrap>
      ),
      dynamicHeight: true,
      dataKey: 'title',
      disableSort: true,
      label: 'Title',
    },
    {
      cellRenderer: () => ({ cellData }) => (
        <span title={getLongDate(cellData)}>{moment(cellData).fromNow()}</span>
      ),
      dynamicHeight: true,
      dataKey: 'updated',
      disableSort: true,
      label: 'Last Updated',
      width: 300,
    },
    {
      cellRenderer: () => ({ cellData }) => (
        <span title={cellData ? getLongDate(cellData) : '-'}>
          {cellData ? moment(cellData).fromNow() : '-'}
        </span>
      ),
      dynamicHeight: true,
      dataKey: 'publishedDate',
      disableSort: true,
      label: 'Last Published',
      width: 300,
    },
    {
      cellRenderer: () => ({ cellData, rowData }) => (
        <Status status={cellData} />
      ),
      dataKey: 'status',
      disableSort: true,
      width: 400,
      label: 'Status',
    },
    {
      cellRenderer: () => ({ cellData, rowData }) => {
        return (
          <>
            {[
              'error',
              'submission-errors',
              'conversion-errors',
              'loading-errors',
              'in-production',
            ].includes(cellData) &&
              getErrorAsignees(
                rowData.errors,
                rowData.settings.chapterIndependently,
              )}
            {[
              'preview',
              'pre-published',
              'published',
              'in-production',
            ].includes(cellData) &&
              getWarnings(
                rowData.errors,
                rowData.settings.chapterIndependently,
              )}
            {rowData.__typename === 'Book' &&
            rowData?.bookComponents[0]?.issues.filter(
              issues => issues.status === 'open',
            ).length > 0 ? (
              <Status status="vendor-issues" />
            ) : (
              ''
            )}
          </>
        )
      },
      dataKey: 'status',
      disableSort: true,
      width: 400,
      label: 'Errors and queries',
    },
  ]

  const tools = {
    left: [],
    right: [DeleteBulk], // [DownloadBulk, ArchiveBulk, DeleteBulk],
  }

  const criteria = [
    {
      or: [
        {
          field: 'title',
          operator: {
            contains: searchValue,
          },
        },
        {
          field: 'alias',
          operator: {
            contains: searchValue.replace(/^bcms/, ''),
          },
        },
      ],
    },
    {
      field: 'workflow',
      operator: {
        eq: selectedWorkflow,
      },
    },
    {
      field: 'type',
      operator: {
        eq: selectedType,
      },
    },
    {
      field: 'status',
      operator: {
        eq: selectedStatus,
      },
    },
  ]

  isPDF2XMLVendor
    ? criteria.push({
        field: 'workflow',
        operator: { eq: 'pdf' },
      })
    : criteria.push({
        field: 'organisationId',
        operator: { eq: organizationId },
      })

  const queryVariables = {
    input: {
      skip: 0,
      sort: { direction: 'desc', field: ['updated'] },
      take: 10,
      search: {
        criteria,
      },
    },
  }

  const showFilter = () => {
    if (showFilters) {
      setShowFilters(!showFilters)
      setSelectedWorkflow(null)
      setSelectedStatus(null)
      setSelectedType(null)
    } else {
      setShowFilters(!showFilters)
    }
  }

  return (
    <Root>
      <Top>
        <ButtonsWrapper showLeftSide={canCreateBookCollection}>
          {canCreateBookCollection && (
            <ButtonGroup>
              {showCreateNewBook && (
                <Button
                  data-test-id="create-new-book"
                  onClick={onClickCreateBook}
                  outlined
                  status="primary"
                >
                  New Book
                </Button>
              )}

              {showCreateNewCollection && (
                <Button
                  data-test-id="create-new-collection"
                  onClick={onClickCreateCollection}
                  outlined
                  status="primary"
                >
                  New Collection
                </Button>
              )}
            </ButtonGroup>
          )}

          <RightContainer>
            <StyledCheckbox
              checked={showFilters}
              data-test-id="show-filter-checkbox"
              label="Show filters"
              onClick={() => showFilter()}
            />

            <Search
              triggerSearch={debounce(v => {
                setSearchValue(v)
              }, delay)}
            />
          </RightContainer>
        </ButtonsWrapper>

        {showFilters && (
          <FiltersRow>
            <Filter
              data-test-id="source-select"
              isClearable
              onChange={v => {
                if (!v) setSelectedWorkflow(null)
                if (v) setSelectedWorkflow(v.value)
              }}
              options={[
                { value: 'word', label: 'Word' },
                { value: 'xml', label: 'XML' },
                { value: 'pdf', label: 'PDF' },
              ]}
              placeholder="Source"
            />

            <Filter
              data-test-id="type-select"
              isClearable
              onChange={v => {
                setSelectedType(v ? v.value : null)
              }}
              options={[
                { value: 'book', label: 'Book' },
                { value: 'collection', label: 'Collection' },
              ]}
              placeholder="Type"
            />

            <Filter
              data-test-id="status-select"
              isClearable
              onChange={v => {
                if (!v) setSelectedStatus(null)
                if (v) setSelectedStatus(v.value)
              }}
              options={[
                { value: 'new-book', label: s('status.newBook') },
                { value: 'converting', label: s('status.converting') },
                { value: 'in-production', label: s('status.inProduction') },
                { value: 'tagging', label: s('status.tagging') },
                { value: 'preview', label: s('status.preview') },
                { value: 'in-review', label: s('status.inReview') },
                { value: 'published', label: s('status.published') },
                {
                  value: 'conversion-errors',
                  label: s('status.conversionErrors'),
                },
                { value: 'loading-errors', label: s('status.loadingErrors') },
                {
                  value: 'submission-errors',
                  label: s('status.submissionErrors'),
                },
                {
                  value: 'failed',
                  label: s('status.failed'),
                },
              ]}
              placeholder="Status"
            />
          </FiltersRow>
        )}
      </Top>

      <TableWithToolbar
        pushRightAfter={3}
        query={bookQuery}
        target="getDashboard"
        tools={tools}
        variables={queryVariables}
      >
        {({
          checkboxColumn,
          selectedRows,
          setSelectedRows,
          loadedData,
          count,
        }) => (
          <Table
            columns={[checkboxColumn].concat(columns)}
            count={count}
            key="dashbaord"
            loadedData={loadedData}
            noDataMessage="No books available"
            onColumnClick={props => {
              if (props.dataKey === 'id') props.event.stopPropagation()
            }}
            // eslint-disable-next-line no-shadow
            onRowClick={({ rowData, event, index }) => {
              // eslint-disable-next-line no-underscore-dangle
              const pageType =
                rowData.__typename === 'Book'
                  ? 'bookmanager'
                  : 'collectionmanager'

              if (rowData.organisation?.id) {
                history.push(
                  `/organizations/${rowData.organisation?.id}/${pageType}/${rowData.id}`,
                )
              }
            }}
            rowHeight="100"
            selectedRows={selectedRows}
            setSelectedRows={setSelectedRows}
            target="getBooks"
            variables={queryVariables}
          />
        )}
      </TableWithToolbar>
    </Root>
  )
}

export default BooksCollectionsPage
