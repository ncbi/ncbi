/* eslint-disable react/prop-types */
import React, { useRef, useContext } from 'react'
import styled from 'styled-components'

import { useMutation } from '@apollo/client'
import { omit } from 'lodash'
import query from '../graphql'
import { FormOrganisation } from '../Form'
import ConstantsContext from '../../../../app/constantsContext'
import {
  ActionBar,
  Notification,
  ButtonGroup,
  Button,
  FixedLoader,
} from '../../../components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

const ContentWraper = styled.div`
  flex-grow: 1;
  overflow-y: auto;
  padding: 8px;
`

const SettingsOrganisation = ({ organization, readonly }) => {
  const modalRef = useRef(null)

  const [updateOrg, { loading }] = useMutation(query.UPDATE_ORGANISATION)

  const { s } = useContext(ConstantsContext)

  return (
    <Wrapper>
      <ContentWraper>
        <FormOrganisation
          disableAbbreviation
          formikFormRef={modalRef}
          readonly={readonly}
          values={organization}
        />
      </ContentWraper>

      {!readonly && (
        <ActionBar
          leftComponent={
            <ButtonGroup>
              <Button
                data-test-id="save-edit-organisation"
                disabled={loading}
                onClick={async () => {
                  const createdData = await modalRef.current.triggerSave()

                  if (createdData) {
                    const formData = {
                      id: organization.id,
                      ...omit(createdData, ['abbreviation', 'publisherId']),
                    }

                    updateOrg({
                      refetchQueries: [
                        {
                          query: query.GET_ORGANISATION,
                          variables: { id: organization.id },
                        },
                      ],
                      variables: {
                        input: [formData],
                      },
                    })
                      .then(() => {
                        Notification({
                          message: s('notifications.successUpdateOrg'),
                          type: 'success',
                        })
                      })
                      .catch(() => {
                        Notification({
                          message: s('notifications.errorUpdateOrg'),
                          type: 'danger',
                        })
                      })
                  }
                }}
                status="primary"
              >
                {s('form.save')}
              </Button>

              <Button
                data-test-id="cancel-edit-organisation"
                disabled={loading}
                onClick={() => {
                  // refetch()
                }}
                outlined
                status="primary"
              >
                {s('form.cancel')}
              </Button>
            </ButtonGroup>
          }
        />
      )}
      {loading && <FixedLoader title="Saving organization settings..." />}
      {!organization && <FixedLoader title="Loading organization data..." />}
    </Wrapper>
  )
}

export default SettingsOrganisation
