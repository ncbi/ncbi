/* eslint-disable react/prop-types */

import React, { useState, useContext, useEffect, useCallback } from 'react'
import styled from 'styled-components'
import { cloneDeep, debounce, findIndex, without } from 'lodash'
import { useParams } from 'react-router-dom'

import { TextField as uiTextField } from '@pubsweet/ui'
import { grid } from '@pubsweet/ui-toolkit'

import { Table } from '../../../components/Datatable'
import { UpdateStatusBulk, UpdateTeamsBulk } from '../BulkActions'
import UserModal from '../Modals/UserModal'
import { TableWithToolbar } from '../../../components'
import ConstantsContext from '../../../../app/constantsContext'
import InviteUser from '../Modals/InviteUser'
import { getDisplayName } from '../../../common/utils'
import PermissionsGate from '../../../../app/components/PermissionsGate'

// #region styled
const Root = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

const Top = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  padding: ${grid(1)} ${grid(2)} 0 ${grid(2)};

  > button {
    margin: 0;
  }
`

const TextField = styled(uiTextField)`
  display: inline-flex;
  margin-bottom: 0;
  width: 400px;
`
// #endregion styled

const UsersTab = props => {
  const {
    checkAllOrganizationUsers,
    isPublisherOrganization,
    loading,
    organizationTeams,
    rejectUser,
    searchUsers,
    updateMemberTeams,
    updateUser,
    users,
    verifyUser,
  } = props

  const { organizationId } = useParams()
  const { s } = useContext(ConstantsContext)

  const [searchValue, setSearchValue] = useState('')
  const [rowData, setRowData] = useState(null)
  const [showRowModal, setShowRowModal] = useState(false)

  useEffect(() => {
    if (showRowModal && rowData) {
      const userData = cloneDeep(users.results.find(u => u.id === rowData.id))
      userData.index = findIndex(users.results, u => u.id === rowData.id)

      if (userData) {
        setRowData(userData)
      }
    }
  }, [users])

  const search = useCallback(
    debounce(v => searchUsers(v), 500),
    [],
  )

  const closeModal = () => {
    setRowData(null)
    setShowRowModal(false)
  }

  const reject = userId => {
    rejectUser(userId).then(() => {
      closeModal()
    })
  }

  const columns = [
    {
      cellRenderer: () => ({ ...params }) => params.cellData,
      dataKey: 'givenName',
      disableSort: true,
      label: 'Given Name',
      dynamicHeight: true,
      flexShrink: 0,
      flexGrow: 0,
      width: 400,
    },
    {
      dataKey: 'surname',
      disableSort: true,
      label: 'Surname',
    },
    {
      dataKey: 'username',
      disableSort: true,
      label: 'Username',
    },
    {
      dataKey: 'email',
      disableSort: true,
      label: 'Email',
    },
    {
      cellRenderer: () => ({ cellData }) => {
        const roles = cellData
        const isOrgAdmin = roles.includes('Org Admin')
        const cleanRoles = without(roles, 'Org Admin')

        return (
          <>
            {isOrgAdmin && <strong>Admin</strong>}
            {isOrgAdmin && cleanRoles.length > 0 && ', '}
            {cleanRoles.length > 0 && cleanRoles.join(', ')}
          </>
        )
      },
      dynamicHeight: true,
      dataKey: 'roles',
      disableSort: true,
      label: 'Organization Roles',
    },
    {
      cellRenderer: () => ({ cellData }) => {
        const statusMapper = {
          disabled: 'Disabled',
          enabled: 'Accepted',
          unverified: 'Pending',
        }

        return statusMapper[cellData] || null
      },
      dataKey: 'status',
      disableSort: true,
      label: 'Status',
    },
  ]

  const setRowStyle = ({ index }) =>
    users.results[index]?.status === 'disabled' && {
      color: '#616161',
      background: 'rgb(247, 247, 247)',
    }

  const handleRowClick = ({ rowData: incoming, index, data }) => {
    const clickedRowData = { ...incoming }
    clickedRowData.index = index

    setShowRowModal(true)
    setRowData(clickedRowData)
  }

  const handleSearch = e => {
    const { value } = e.target
    setSearchValue(value)
    search(value)
  }

  const hasRole = role => rowData.roles.includes(role)

  let nextDisplayName, previousDisplayName, onClickNext, onClickPrevious

  if (rowData) {
    const currentIndex = rowData.index
    const userList = users.results

    if (currentIndex !== userList.length) {
      const nextIndex = currentIndex + 1
      const next = { ...userList[nextIndex] }
      next.index = nextIndex

      nextDisplayName = getDisplayName(next)
      onClickNext = () => setRowData(next)
    }

    if (currentIndex > 0) {
      const previousIndex = currentIndex - 1
      const previous = { ...userList[previousIndex] }
      previous.index = previousIndex

      previousDisplayName = getDisplayName(previous)
      onClickPrevious = () => setRowData(previous)
    }
  }

  return (
    <Root>
      <Top>
        <PermissionsGate scopes={['invite-orgAdmin']}>
          <InviteUser key={1} organization={organizationId} />
        </PermissionsGate>

        <TextField
          data-test-id="search"
          key={2}
          onChange={handleSearch}
          placeholder="Search"
          value={searchValue}
        />
      </Top>

      {!loading && (
        <TableWithToolbar
          data={users}
          target="getOrganisation.users"
          toolBarProps={{
            checkAll: checkAllOrganizationUsers,
            isPublisherOrganization,
            teams: organizationTeams,
            updateUserTeam: updateMemberTeams,
            users: users.results,
            verifyUser,
            rejectUser,
          }}
          tools={{ left: [UpdateStatusBulk, UpdateTeamsBulk] }}
        >
          {({
            checkboxColumn,
            selectedRows,
            setSelectedRows,
            loadedData,
            count,
          }) => (
            <Table
              columns={[checkboxColumn].concat(columns)}
              count={count}
              key="usersPage"
              loadedData={loadedData}
              noDataMessage={s('pages.noUsers')}
              onColumnClick={rowProps => {
                if (rowProps.dataKey === 'id') rowProps.event.stopPropagation()
              }}
              onRowClick={handleRowClick}
              rowStyle={setRowStyle}
              selectedRows={selectedRows}
            />
          )}
        </TableWithToolbar>
      )}

      {rowData && (
        <UserModal
          closeModal={closeModal}
          isEditor={hasRole('Editor')}
          isModalOpen={showRowModal}
          isOrgAdmin={hasRole('Org Admin')}
          nextDisplayName={nextDisplayName}
          onClickNext={onClickNext}
          onClickPrevious={onClickPrevious}
          previousDisplayName={previousDisplayName}
          reject={reject}
          rowData={rowData}
          updateUser={updateUser}
          verify={verifyUser}
        />
      )}
    </Root>
  )
}

export default UsersTab
