/* eslint-disable react/prop-types */

import React, { useState, useCallback, useContext } from 'react'
import styled from 'styled-components'
import { useQuery } from '@apollo/client'
import debounce from 'lodash/debounce'

import { TextField as uiTextField } from '@pubsweet/ui'
import { grid } from '@pubsweet/ui-toolkit'

import query from '../graphql'
import { TableLocalData } from '../../../components/Datatable'
import { AdminIcon } from '../../../components'
import ConstantsContext from '../../../../app/constantsContext'
import GeneralUser from '../Modals/GeneralUser'

// #region Styles
const Root = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

const Top = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 0 ${grid(2)};
`

const TextField = styled(uiTextField)`
  display: inline-flex;
  margin-bottom: 0;
  width: 400px;
`

const CheckIcon = styled(AdminIcon)`
  /* margin: 0 auto; */
`
// #endregion

const UsersPage = ({ ...props }) => {
  const [searchValue, setSearchValue] = useState('')
  const [rowModal, setRowModal] = useState()
  const { s } = useContext(ConstantsContext)
  const delay = 1000

  const { loading, data, refetch } = useQuery(query.GET_ALL_USERS, {
    notifyOnNetworkStatusChange: true,
    variables: {
      input: {
        sort: {
          direction: 'asc',
          field: ['givenName', 'lastName', 'username'],
        },
      },
    },
  })

  const { data: orgList } = useQuery(query.GET_ORGANISATIONS)

  const columns = [
    {
      cellRenderer: () => ({ ...params }) => params.cellData,
      dataKey: 'givenName',
      disableSort: true,
      label: 'Given Name',
      dynamicHeight: true,
      flexShrink: 0,
      flexGrow: 0,
      width: 400,
    },
    {
      dataKey: 'surname',
      disableSort: true,
      label: 'Surname',
    },
    {
      dataKey: 'username',
      disableSort: true,
      label: 'Username',
    },
    {
      dataKey: 'email',
      disableSort: true,
      label: 'Email',
    },
    {
      cellRenderer: () => ({ cellData }) =>
        cellData.isSysAdmin ? <CheckIcon /> : '',
      dataKey: 'auth',
      disableSort: true,
      label: 'System Admin',
    },
    {
      cellRenderer: () => ({ cellData }) =>
        cellData.isPDF2XMLVendor ? <CheckIcon /> : '',
      dataKey: 'auth',
      disableSort: true,
      label: 'PDF2XML Vendor',
    },
  ]

  const triggerSearch = useCallback(
    debounce(value => {
      refetch({
        input: {
          search: {
            criteria: [
              {
                or: [
                  {
                    field: 'givenName',
                    operator: {
                      contains: value,
                    },
                  },
                  {
                    field: 'surname',
                    operator: {
                      contains: value,
                    },
                  },
                  {
                    field: 'email',
                    operator: {
                      contains: value,
                    },
                  },
                ],
              },
            ],
          },
        },
      })
    }, delay),
    [],
  )

  return (
    <Root>
      <Top>
        <TextField
          onChange={e => {
            const { value } = e.target
            setSearchValue(value)
            triggerSearch(value)
          }}
          placeholder="Search"
          value={searchValue}
        />
      </Top>

      {!loading && (
        <TableLocalData
          columns={columns}
          dataTable={data?.searchUsers?.results || []}
          headerHeight={40}
          key="usersPage"
          noDataMessage={s('pages.noUsers')}
          onColumnClick={prp => {
            if (props.dataKey === 'id') prp.event.stopPropagation()
          }}
          /* eslint-disable-next-line no-shadow */
          onRowClick={({ rowData, index, data }) => {
            setRowModal({
              index,
              data,
              admin: rowData.auth.isSysAdmin,
              pdf2xmlVendor: rowData.auth.isPDF2XMLVendor,
              ...rowData,
            })
          }}
          rowHeight={40}
        />
      )}

      {rowModal ? (
        <GeneralUser
          editMode
          organization={null}
          organizationsList={orgList?.getOrganisations.results}
          rowData={rowModal}
          setRowData={setRowModal}
        />
      ) : null}
    </Root>
  )
}

export default UsersPage
