/* eslint-disable react/prop-types */
import React, { useContext, useState } from 'react'
import styled from 'styled-components'
import { debounce } from 'lodash'

import { TextField as UITextField } from '@pubsweet/ui'
import { grid } from '@pubsweet/ui-toolkit'

import query from '../graphql'
import { Table } from '../../../components/Datatable'
import { Button, TableWithToolbar } from '../../../components'
import DeleteBulkOrg from '../BulkActions/DeleteBulkOrg'
import CreateOrganisationModal from '../Modals/CreateOrganisation'
import ConstantsContext from '../../../../app/constantsContext'
import PermissionsGate from '../../../../app/components/PermissionsGate'

// #region styled-components
const Root = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

const Top = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 ${grid(2)};
`

const TextField = styled(UITextField)`
  display: inline-flex;
  margin-bottom: 0;
  width: 400px;
`

const Search = ({ triggerSearch }) => {
  const [textSearch, setTextSearch] = useState('')

  const handleChange = e => {
    const val = e.target.value
    setTextSearch(val)

    triggerSearch(val)
  }

  return (
    <TextField
      onChange={handleChange}
      placeholder="Search"
      value={textSearch}
    />
  )
}

const OrganisationsList = ({ props }) => {
  const [searchValue, setSearchValue] = useState('')
  const [organizationModalOpen, setOrganizationModalOpen] = useState(false)

  const columns = [
    {
      cellRenderer: () => ({ cellData, rowData }) => <span>{cellData}</span>,
      dataKey: 'name',
      dynamicHeight: true,
      disableSort: true,
      label: 'Organization',
    },
    {
      cellRenderer: () => ({ cellData }) => (
        <span>
          {cellData.type.publisher && 'Publisher '}
          {cellData.type.funder && 'Funder'}
        </span>
      ),
      dataKey: 'settings',
      label: 'Type',
    },
  ]

  const { s } = useContext(ConstantsContext)
  const delay = 1000

  return (
    <>
      <Root>
        <Top>
          <PermissionsGate scopes={['create-organization']}>
            <Button
              data-test-id="create-organisation"
              onClick={() => setOrganizationModalOpen(true)}
              outlined
              status="primary"
            >
              {s('pages.button.newOrg')}
            </Button>
          </PermissionsGate>
          <Search
            triggerSearch={debounce(v => {
              setSearchValue(v)
            }, delay)}
          />
        </Top>

        <TableWithToolbar
          pushRightAfter={-1}
          query={query.GET_ORGANISATIONS}
          target="getOrganisations"
          tools={{ right: [DeleteBulkOrg] }}
          variables={{
            input: {
              skip: 0,
              sort: { direction: 'asc', field: ['name'] },
              take: 10,
              search: {
                criteria: [
                  {
                    or: [
                      {
                        field: 'name',
                        operator: {
                          contains: searchValue,
                        },
                      },
                      {
                        field: 'email',
                        operator: {
                          contains: searchValue,
                        },
                      },
                    ],
                  },
                ],
              },
            },
          }}
        >
          {({
            checkboxColumn,
            selectedRows,
            setSelectedRows,
            loadedData,
            count,
          }) => {
            return (
              <Table
                columns={[checkboxColumn].concat(columns)}
                count={count}
                key="table"
                loadedData={loadedData}
                noDataMessage={s('pages.noOrg')}
                onColumnClick={prop => {
                  if (prop.dataKey === 'id') prop.event.stopPropagation()
                }}
                onRowClick={({ rowData }) => {
                  return props.history?.push(`/organizations/${rowData.id}`)
                }}
                selectedRows={selectedRows}
                setSelectedRows={setSelectedRows}
              />
            )
          }}
        </TableWithToolbar>
      </Root>

      <CreateOrganisationModal
        isOpen={organizationModalOpen}
        onClose={() => setOrganizationModalOpen(false)}
      />
    </>
  )
}

export default OrganisationsList
