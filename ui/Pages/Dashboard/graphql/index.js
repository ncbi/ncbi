import { gql } from '@apollo/client'

const CREATE_BOOK = gql`
  mutation($input: CreateBookInput!) {
    createBook(input: $input) {
      id
      organisation {
        id
        name
      }
      updated
      title
      workflow
      status
      settings {
        chapterIndependently
      }
    }
  }
`

const GET_BOOK = gql`
  query($id: ID!) {
    getBook(id: $id) {
      id
      title
      bookComponents {
        id
      }
      publishedDate
      settings {
        toc {
          add_body_to_parts
          group_chapters_in_parts
        }
      }
    }
  }
`

const GET_DASHBOARD = gql`
  query getDashboard($input: SearchModelInput) {
    getDashboard(input: $input) {
      metadata {
        total
        skip
        take
      }
      results {
        ... on Book {
          id
          title
          subTitle
          status
          workflow
          fundedContentType
          version
          updated
          errors {
            id
            severity
            assignee
          }
          publishedDate
          bookComponents {
            id
            bookComponentVersionId
            issues {
              id
              status
              title
            }
          }
          settings {
            toc {
              group_chapters_in_parts
              add_body_to_parts
            }
            chapterIndependently
          }
          organisation {
            name
            id
          }
        }
        ... on Collection {
          id
          title
          workflow
          updated
          status
          organisation {
            name
            id
          }
          settings {
            groupBooks
            groupBooksBy
            orderBooksBy
            toc {
              contributors
              publisher
            }
          }
        }
      }
    }
  }
`

const GET_CHAPTERS_OVERVIEW = gql`
  query getChaptersOverview($input: SearchModelInput) {
    getChaptersOverview(input: $input) {
      metadata {
        total
        skip
        take
      }
      results {
        id
        url
        status
        bookComponentVersionId
        bookId
        organisationId
        alias
        notification {
          id
          created
          updated
          jobId
          topic
          proccessed
          uploaded
          options
          packageType
          timeout
        }
      }
    }
  }
`

const DELETE_BOOKS = gql`
  mutation($id: [ID]) {
    deleteBook(id: $id)
  }
`

const CHECKALL_BOOK = gql`
  mutation($action: Actions!) {
    checkAllBook(action: $action)
  }
`

const CREATE_ORGANISATION = gql`
  mutation($input: CreateOrganisationInput!) {
    createOrganisation(input: $input) {
      id
      name
      agreement
    }
  }
`

const UPDATE_ORGANISATION = gql`
  mutation($input: [UpdateOrganisationInput]!) {
    updateOrganisation(input: $input) {
      id
      name
      agreement
      abbreviation
      publisherId
      settings {
        type {
          publisher
          funder
        }
        collections {
          funded
          bookSeries
        }
        xml
        pdf
        word
      }
      teams {
        id
        role
        name
        members {
          status
          user {
            id
            username
          }
        }
      }
    }
  }
`

const GET_ORGANISATION = gql`
  query($id: ID!) {
    getOrganisation(id: $id) {
      id
      name
      agreement
      abbreviation
      publisherId
      collections {
        id
        title
      }
      settings {
        type {
          publisher
          funder
        }
        collections {
          funded
          bookSeries
        }
        xml
        pdf
        word
      }
      teams {
        id
        role
        name
        members {
          status
          user {
            id
            username
          }
        }
      }
    }
  }
`

const CHECKALL_ORGANISATION = gql`
  mutation($input: CheckAllOrganisationInput, $action: Actions!) {
    checkAll(input: $input, action: $action)
  }
`

const UPDATE_MEMBER_TEAMS = gql`
  mutation($id: ID!, $members: [TeamMemberInput]) {
    assignMembersToTeam(id: $id, members: $members) {
      name
      members {
        id
        user {
          username
          givenName
          surname
          id
        }
        status
      }
    }
  }
`

const REQUEST_ACCESS_TO_ORGANISATION = gql`
  mutation RequestAccessToOrganisation($id: ID!, $members: [TeamMemberInput]) {
    requestAccessToOrganisation(id: $id, members: $members) {
      name
      members {
        id
        user {
          username
          givenName
          surname
          id
        }
        status
      }
    }
  }
`

const GET_ORGANISATIONS = gql`
  query getOrganisations($input: SearchModelInput) {
    getOrganisations(input: $input) {
      metadata {
        total
        skip
        take
      }
      results {
        id
        name
        agreement
        settings {
          type {
            publisher
            funder
          }
        }
        teams {
          id
          role
          object {
            objectId
            objectType
          }
          name
          members {
            id
            status
            user {
              id
              username
            }
          }
        }
      }
    }
  }
`

const DELETE_ORGANISATION = gql`
  mutation($id: [ID]) {
    deleteOrganisation(id: $id)
  }
`

const GET_SYSADMIN_USERS = gql`
  query sysAdmins($status: String) {
    sysAdmins(status: $status) {
      id
      role
      name
      members {
        id
        user {
          username
          givenName
          surname
          id
        }
        status
      }
    }
  }
`

const CREATE_USER = gql`
  mutation($input: NcbiUserCreateInput) {
    createNcbiUser(input: $input) {
      id
      username
      email
      givenName
      surname
      # verificationAwardNumber
    }
  }
`

const GET_ALL_USERS = gql`
  query AllUsers($input: SearchModelInput) {
    searchUsers(input: $input) {
      results {
        id
        username
        email
        givenName
        surname
        auth {
          isSysAdmin
          isPDF2XMLVendor
          sysAdminForOrganisation
          orgAdminForOrganisation
        }
        organisations {
          id
          name
          teams {
            id
            role
            name
            members {
              id
              status
              user {
                id
              }
            }
          }
        }
      }
    }
  }
`

const CREATE_COLLECTION = gql`
  mutation($input: CreateCollectionInput) {
    createCollection(input: $input) {
      id
      title
    }
  }
`

const GET_COLLECTIONS = gql`
  query getCollections($input: SearchModelInput) {
    getCollections(input: $input) {
      results {
        id
        title
      }
    }
  }
`

export default {
  CHECKALL_BOOK,
  CHECKALL_ORGANISATION,
  CREATE_BOOK,
  CREATE_COLLECTION,
  CREATE_ORGANISATION,
  CREATE_USER,
  DELETE_BOOKS,
  DELETE_ORGANISATION,
  GET_ALL_USERS,
  GET_BOOK,
  GET_COLLECTIONS,
  GET_CHAPTERS_OVERVIEW,
  GET_DASHBOARD,
  GET_ORGANISATION,
  GET_ORGANISATIONS,
  GET_SYSADMIN_USERS,
  REQUEST_ACCESS_TO_ORGANISATION,
  UPDATE_MEMBER_TEAMS,
  UPDATE_ORGANISATION,
}
