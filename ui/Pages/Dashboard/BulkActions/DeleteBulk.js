/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import { useMutation } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'
// import styled from 'styled-components'
// import { th } from '@pubsweet/ui-toolkit'

import query from '../graphql'
import {
  DeleteConfirmation,
  SelectInformation,
  Button,
  Notification,
} from '../../../components'

export default ({ selectedRows, setSelectedRows }) => {
  const [modalIsOpen, setIsOpen] = useState(false)
  const [ErrorMutation, setErrorMutation] = useState(false)

  const [deleteMutation, { loading: deleting, error }] = useMutation(
    query.DELETE_BOOKS,
  )

  const [checkAll, { loading: checking }] = useMutation(query.CHECKALL_BOOK)

  React.useEffect(() => {
    error ? setErrorMutation(error) : setErrorMutation(null)
  }, [modalIsOpen])

  if (ErrorMutation) {
    setErrorMutation(null)
    return Notification({
      message: 'Books can`t be deleted',
      type: 'danger',
    })
  }

  return (
    <>
      <Button
        data-test-id="delete-bulk"
        disabled={checking || deleting}
        icon="trash-2"
        onClick={() => {
          setIsOpen(true)
        }}
        outlined
        status="danger"
      />
      {selectedRows.length ? (
        <DeleteConfirmation
          confirmAction={async () => {
            const selectedAll = selectedRows.includes('all')

            if (selectedAll) {
              checkAll({
                refetchQueries: [getOperationName(query.GET_DASHBOARD)],
                variables: {
                  action: 'DELETE',
                },
              })
                .then(() => {
                  setSelectedRows([])
                  setIsOpen(false)
                })
                .catch(res => {
                  console.error(res)

                  Notification({
                    message: 'Could not delete',
                    type: 'danger',
                    title: 'Error',
                  })
                })
            }

            deleteMutation({
              refetchQueries: [getOperationName(query.GET_DASHBOARD)],
              variables: { id: selectedRows },
            })
              .catch(res => {
                console.error(res)

                Notification({
                  message: 'Could not delete',
                  type: 'danger',
                  title: 'Error',
                })
              })
              .finally(() => {
                setSelectedRows([])
                setIsOpen(false)
              })
          }}
          modalIsOpen={modalIsOpen}
          setIsOpen={setIsOpen}
          title="Are you sure you want to delete books?"
        />
      ) : (
        <SelectInformation modalIsOpen={modalIsOpen} setIsOpen={setIsOpen} />
      )}
    </>
  )
}
