/* eslint-disable react/prop-types */
import React, { useState, useContext } from 'react'
import { useMutation } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'
import ConstantsContext from '../../../../app/constantsContext'
import query from '../graphql'

import {
  Notification,
  DeleteConfirmation,
  SelectInformation,
  Button,
  FixedLoader,
} from '../../../components'

export default ({ selectedRows, setSelectedRows }) => {
  const [modalIsOpen, setIsOpen] = useState(false)
  const { s } = useContext(ConstantsContext)

  const [deleteOrg, { loading: deleting }] = useMutation(
    query.DELETE_ORGANISATION,
  )

  const [checkAll, { loading: deletingAll }] = useMutation(
    query.CHECKALL_ORGANISATION,
  )

  return (
    <>
      <Button
        data-test-id="bulk-delete-organisations"
        disabled={deleting || deletingAll}
        icon="trash-2"
        onClick={() => {
          setIsOpen(true)
        }}
        outlined
        status="danger"
      />
      {(deleting || deletingAll) && (
        <FixedLoader title="Deleting organizations..." />
      )}
      {selectedRows.length ? (
        <DeleteConfirmation
          confirmAction={async () => {
            try {
              const selectedAll = selectedRows.includes('all')

              if (selectedAll) {
                checkAll({
                  refetchQueries: [getOperationName(query.GET_ORGANISATIONS)],
                  variables: {
                    action: 'DELETE',
                  },
                })
                  .then(() => {
                    setSelectedRows([])
                    setIsOpen(false)
                    const message = s('notifications.successDeleteOrgs')
                    Notification({ message })
                  })
                  .catch(res => {
                    console.error(res)

                    Notification({
                      message: 'Could not delete',
                      type: 'danger',
                      title: 'Error',
                    })
                  })
              } else {
                await deleteOrg({
                  refetchQueries: [getOperationName(query.GET_ORGANISATIONS)],
                  variables: { id: selectedRows },
                })
                  .then(props => {
                    setSelectedRows([])
                    setIsOpen(false)

                    const message =
                      selectedRows.length > 1
                        ? s('notifications.successDeleteOrgs')
                        : s('notifications.successDeleteOrg')

                    Notification({ message })
                  })
                  .catch(res => {
                    console.error(res)

                    Notification({
                      message: 'Could not delete',
                      type: 'danger',
                      title: 'Error',
                    })
                  })
              }
            } catch (e) {
              Notification({
                message: (
                  <>
                    {s('notifications.orgsRelated')} <br />
                    <small> {e.toString()}</small>
                  </>
                ),
                title: s('notifications.errorDeleteOrgTitle'),
                type: 'danger',
              })

              setIsOpen(false)
            }
          }}
          modalIsOpen={modalIsOpen}
          setIsOpen={setIsOpen}
          title={s('confirm.deleteOrgs')}
        />
      ) : (
        <SelectInformation modalIsOpen={modalIsOpen} setIsOpen={setIsOpen} />
      )}
    </>
  )
}
