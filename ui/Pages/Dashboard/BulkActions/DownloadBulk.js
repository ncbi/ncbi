/* eslint-disable react/prop-types */
import React from 'react'

import { Button } from '../../../components'

export default ({ selectedRows, setSelectedRows }) => (
  <Button
    data-test-id="download-bulk"
    icon="download"
    outlined
    status="primary"
  >
    Download
  </Button>
)
