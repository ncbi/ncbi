/* eslint-disable react/prop-types */
import React from 'react'

import { Button } from '../../../components'

export default ({ selectedRows, setSelectedRows }) => (
  <Button data-test-id="approve-bulk" icon="check" outlined status="primary">
    Approve
  </Button>
)
