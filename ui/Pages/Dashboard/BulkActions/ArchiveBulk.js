/* eslint-disable react/prop-types */
import React from 'react'

import { Button } from '../../../components'

export default ({ selectedRows, setSelectedRows }) => (
  <Button data-test-id="archive-bulk" icon="archive" outlined status="primary">
    Archive
  </Button>
)
