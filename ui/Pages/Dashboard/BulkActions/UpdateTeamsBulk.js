/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import styled from 'styled-components'
import PermissionsGate from '../../../../app/components/PermissionsGate'

import {
  SelectInformation,
  UpdateConfirmation,
  Dropdown as uiDropdown,
} from '../../../components'

const Dropdown = styled(uiDropdown)`
  background: white;
`

export default ({
  selectedRows = [],
  setSelectedRows,
  teams = [],
  isPublisherOrganization,
  users = [],

  checkAll,
  updateUserTeam,
}) => {
  const [modalIsOpen, setIsOpen] = useState(false)
  const [team, setTeam] = useState({})

  const options = []

  const filterRolesPublisher = tm => {
    return PermissionsGate({
      scopes: [`edit-${tm.role}`],
    })
  }

  const filterRolesFunder = tm => tm.role === 'orgAdmin' // || tm.role === 'awardee'

  const filter = isPublisherOrganization
    ? filterRolesPublisher
    : filterRolesFunder

  // remove the first item as it is Organisation Admin
  teams.filter(filter).forEach((item, i) => {
    options.push({
      title: item.name,
      value: item.id,
      onClick: itm => {
        setTeam(item)
        setIsOpen(true)
      },
    })
  })

  // todo: As of now we are not using these roles. we can remove below 2 line in future
  const unUsedRole = ['Previewer', 'Awardee', 'Author', 'Org User', 'Sys Admin']
  const results = options.filter(({ title }) => !unUsedRole.includes(title))

  const disabled = users
    .filter(usr => selectedRows.includes(usr.id))
    .every(sel => sel.status === 'unverified' || sel.status === 'disabled')

  const onlyEnabled = users
    .filter(usr => selectedRows.includes(usr.id))
    .every(sel => sel.status === 'enabled')

  const handleConfirm = () => {
    const selectedAll = selectedRows.includes('all')

    if (selectedAll) {
      const action = 'ASSIGNTEAM'
      const { role } = team

      checkAll(action, role, onlyEnabled).then(() => {
        setSelectedRows([])
        setIsOpen(false)
      })
    } else {
      let userName = 'Selected users'

      const members = selectedRows.map(id => ({
        user: { id },
        status: 'enabled',
      }))

      if (selectedRows.length === 1) {
        userName = team.members.filter(e => e.user.id === selectedRows[0])
      }

      updateUserTeam(
        team.id,
        members,
        team.role,
        onlyEnabled,
        userName[0]?.user?.username,
      ).then(() => {
        setIsOpen(false)
        setSelectedRows([])
      })
    }
  }

  return (
    <>
      <Dropdown
        data-test-id="bulk-update-teams"
        direction="up"
        disabled={selectedRows.length > 0 ? disabled : true}
        itemsList={results}
        primary
      >
        Assign Roles
      </Dropdown>

      {!!selectedRows.length && (
        <UpdateConfirmation
          confirmAction={handleConfirm}
          modalIsOpen={modalIsOpen}
          setIsOpen={setIsOpen}
          title={`Are you sure you want to assign "${team.name}" to these users? `}
        />
      )}

      {!selectedRows.length && (
        <SelectInformation modalIsOpen={modalIsOpen} setIsOpen={setIsOpen} />
      )}
    </>
  )
}
