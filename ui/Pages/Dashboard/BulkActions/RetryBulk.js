/* eslint-disable react/prop-types */
import React from 'react'
import { useMutation } from '@apollo/client'

import query from '../../Bookmanager/graphql'
import { Button, FixedLoader, Notification } from '../../../components'

export default ({
  selectedRows,
  setSelectedRows,
  status,
  queryFn,
  variables,
  loadedData,
}) => {
  const refetchQueries = [
    {
      query: queryFn,
      variables,
    },
  ]

  const [submitComponentsFn, { loading: loadingSubmit }] = useMutation(
    query.SUBMIT_BOOK_COMPONENTS,
    {
      refetchQueries,
    },
  )

  const [reconvertBookComponent, { loading: loadingReConvert }] = useMutation(
    query.RECONVERT_FILE,
    {
      refetchQueries,
    },
  )

  const [publishBookComponent, { loading: loadingPublish }] = useMutation(
    query.BOOK_COMPONENT_PUBLISH,
    {
      refetchQueries,
    },
  )

  const [retry, { loading }] = useMutation(query.RETRY, {
    refetchQueries,
  })

  const shouldDisable =
    selectedRows.length === 0 ||
    selectedRows.filter(item => {
      const found = loadedData.find(ld => ld.id === item)
      // Disable the button if hidden rows are selected OR there are rows with
      // unsupported statuses
      return !['converting', 'loading-preview', 'publishing'].includes(
        found?.status,
      )
    }).length > 0

  return (
    <>
      <Button
        disabled={loading || shouldDisable}
        onClick={async e => {
          e.stopPropagation()

          try {
            await retry({
              variables: {
                id: selectedRows || [],
              },
            })

            if (status === 'converting') {
              await submitComponentsFn(
                {
                  variables: {
                    bookComponents: selectedRows,
                  },
                },
                {
                  refetchQueries,
                },
              ) // .then(() => setDisable(false))
            } else if (status === 'loading-preview') {
              await reconvertBookComponent(
                {
                  variables: {
                    ids: selectedRows,
                  },
                },
                {
                  refetchQueries,
                },
              ) // .then(() => setDisable(false))
            } else if (status === 'publishing') {
              await publishBookComponent(
                {
                  variables: {
                    bookComponent: selectedRows,
                  },
                },
                {
                  refetchQueries,
                },
              ).then(() => window.location.reload(false))
            }
          } catch (err) {
            console.error(err)

            Notification({
              type: 'danger',
              title: 'Error',
              message: err.message,
            })
          } finally {
            setSelectedRows([])
          }
        }}
      >
        Retry Action
      </Button>

      {/* <FixedLoader title="loading" /> */}
      {loading && <FixedLoader title="Retrying Action ..." />}
      {loadingSubmit && <FixedLoader title="Submitting ..." />}
      {loadingReConvert && <FixedLoader title="Reconverting ..." />}
      {loadingPublish && <FixedLoader title="Publishing ..." />}
    </>
  )
}
