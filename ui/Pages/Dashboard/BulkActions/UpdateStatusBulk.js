/* eslint-disable react/prop-types */
import React, { useState, useContext } from 'react'
import { ErrorText as uiErrorText } from '@pubsweet/ui'
import styled from 'styled-components'

import {
  SelectInformation,
  UpdateConfirmation,
  Dropdown,
  Icon,
} from '../../../components'
import ConstantsContext from '../../../../app/constantsContext'
import PermissionsGate from '../../../../app/components/PermissionsGate'

const SuccessText = styled.div`
  align-content: center;
  align-items: center;
  color: ${props => props.theme.colorPrimary};
  display: flex;
`

const ErrorText = styled(uiErrorText)`
  align-content: center;
  align-items: center;
  display: flex;
`

export default ({
  selectedRows = [],
  setSelectedRows,
  users = [],

  checkAll,
  verifyUser,
  rejectUser,
}) => {
  const [modalIsOpen, setIsOpen] = useState(false)
  const { s } = useContext(ConstantsContext)
  const [userQuery, setQuery] = useState('verify')

  const options = [
    {
      title: (
        <SuccessText>
          <Icon color="colorPrimary">check</Icon> Accept user
        </SuccessText>
      ),
      value: true,
      onClick: e => {
        setQuery('accept')
        setIsOpen(true)
      },
    },
    {
      title: (
        <ErrorText>
          <Icon color="colorError">x</Icon> Reject user
        </ErrorText>
      ),
      value: false,
      onClick: e => {
        setQuery('reject')
        setIsOpen(true)
      },
    },
  ]

  const disabled = users
    .filter(usr => selectedRows.includes(usr.id))
    .every(sel => sel.status === 'enabled' || sel.status === 'disabled')

  const onConfirm = () => {
    const selectedAll = selectedRows.includes('all')

    if (selectedAll) {
      const action = userQuery === 'accept' ? 'VERIFY' : 'REJECT'

      checkAll(action).then(() => {
        setSelectedRows([])
        setIsOpen(false)
      })
    } else if (userQuery === 'accept') {
      verifyUser(selectedRows).then(() => {
        setIsOpen(false)
        setSelectedRows([])
      })
    } else if (userQuery === 'reject') {
      rejectUser(selectedRows).then(() => {
        setIsOpen(false)
        setSelectedRows([])
      })
    }
  }

  return (
    <>
      <PermissionsGate scopes={['accept-reject-user']}>
        <Dropdown
          data-test-id="bulk-update-accept"
          direction="up"
          disabled={selectedRows.length > 0 ? disabled : true}
          itemsList={options}
          primary
        >
          {s('pages.button.acceptUser')}
        </Dropdown>
      </PermissionsGate>

      {!!selectedRows.length && (
        <UpdateConfirmation
          confirmAction={onConfirm}
          modalIsOpen={modalIsOpen}
          setIsOpen={setIsOpen}
          title={`Are you sure you want to ${userQuery} these users? `}
        />
      )}

      {!selectedRows.length && (
        <SelectInformation modalIsOpen={modalIsOpen} setIsOpen={setIsOpen} />
      )}
    </>
  )
}
