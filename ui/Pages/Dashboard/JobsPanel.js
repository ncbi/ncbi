/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/prop-types */
import React, { useContext, useState } from 'react'
import moment from 'moment'
import styled from 'styled-components'
import { Redirect } from 'react-router-dom'
import { Link } from '@pubsweet/ui'
import { grid } from '@pubsweet/ui-toolkit'
import { useQuery } from '@apollo/client'
import { cloneDeep } from 'lodash'
import query from './graphql'
import { Table } from '../../components/Datatable'

import { TableWithToolbar, Icon, Status } from '../../components'

import CurrentUserContext from '../../../app/userContext'
import Select from '../../components/common/Select'
import { RetryBulk } from './BulkActions'

const Root = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

const Top = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 ${grid(2)};
`

const FiltersRow = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: ${grid(1)} ${grid(2)};
`

const Filter = styled(Select)`
  width: 200px;

  &:not(:last-child) {
    margin-right: ${grid(1)};
  }
`

const JobLink = styled(Link)`
  margin-right: ${grid(1)};
  padding: 10px ${grid(1)} ${grid(0.5)} ${grid(0.5)};
`

const ChaptersPanel = () => {
  const { currentUser } = useContext(CurrentUserContext)
  const [selectedStatus, setSelectedStatus] = useState('converting')
  const [selectedRows, setSelectedRows] = useState([])

  const handleCopy = (e, object) => {
    navigator.clipboard.writeText(object)
  }

  const columns = [
    {
      cellRenderer: () => ({ cellData, rowData }) => (
        <JobLink
          onClick={e => handleCopy(e, cellData)}
          target="_blank"
          to={`/organizations/${rowData.organisationId}/bookmanager/${
            rowData.bookId || rowData.id
          }/${rowData.bookId ? rowData.bookComponentVersionId : ''}`}
        >
          {`BCMS${cellData}`}
        </JobLink>
      ),
      dataKey: 'alias',
      dynamicHeight: true,
      disableSort: true,
      label: 'BCMS Id',
      width: 150,
    },
    {
      cellRenderer: () => ({ cellData }) => (
        <span onClick={e => handleCopy(e, cellData)} title="click to copy">
          {cellData}
        </span>
      ),
      dataKey: 'id',
      dynamicHeight: true,
      disableSort: true,
      label: 'BookComponent Id',
      width: 250,
    },
    {
      cellRenderer: () => ({ cellData }) => <Status status={cellData} />,
      dataKey: 'status',
      dynamicHeight: true,
      disableSort: true,
      label: 'Status',
      width: 250,
    },
    {
      cellRenderer: () => ({ rowData }) => {
        return (
          <span
            title={moment(rowData?.notification?.updated).format(
              'DD MMM YYYY hh:mm A',
            )}
          >
            {rowData?.notification?.updated
              ? moment(rowData.notification.updated).fromNow()
              : '-'}
          </span>
        )
      },
      dynamicHeight: true,
      dataKey: 'updated',
      disableSort: true,
      label: 'Last Notification',
      width: 180,
    },
    {
      cellRenderer: () => ({ rowData }) => (
        <span
          title={moment(rowData.notification?.created).format(
            'DD MMM YYYY hh:mm A',
          )}
        >
          {rowData?.notification?.created
            ? moment(rowData.notification.created).fromNow()
            : '-'}
        </span>
      ),
      dynamicHeight: true,
      dataKey: 'created',
      disableSort: true,
      label: 'Last Created',
      width: 180,
    },
    {
      cellRenderer: () => ({ rowData }) => (
        <span
          onClick={e => handleCopy(e, rowData?.notification?.id)}
          title="click to copy"
        >
          {rowData?.notification?.id}
        </span>
      ),
      dataKey: 'jobId',
      dynamicHeight: true,
      disableSort: true,
      label: 'Notification ID',
      width: 200,
    },
    {
      cellRenderer: () => ({ rowData }) => (
        <span
          onClick={e => handleCopy(e, rowData?.notification?.jobId)}
          title="click to copy"
        >
          {rowData?.notification?.jobId}
        </span>
      ),
      dataKey: 'jobId',
      dynamicHeight: true,
      disableSort: true,
      label: 'JOB ID',
      width: 300,
    },
    {
      cellRenderer: () => ({ rowData }) => (
        <span
          onClick={e => handleCopy(e, rowData?.notification?.topic)}
          title={rowData.notification?.topic}
        >
          {rowData?.notification?.topic}
        </span>
      ),
      dataKey: 'topic',
      dynamicHeight: true,
      disableSort: true,
      label: 'Topic',
      width: 300,
    },
    {
      cellRenderer: () => ({ rowData }) => {
        return (
          <span>
            {rowData?.notification?.proccessed ? (
              <Icon color="colorSuccess">check</Icon>
            ) : (
              <Icon color="colorError">x</Icon>
            )}
          </span>
        )
      },
      dataKey: 'proccessed',
      dynamicHeight: true,
      disableSort: true,
      label: 'Proccessed',
      width: 150,
    },
    {
      cellRenderer: () => ({ rowData }) => (
        <span>
          {rowData?.notification?.uploaded ? (
            <Icon color="colorSuccess">check</Icon>
          ) : (
            <Icon color="colorError">x</Icon>
          )}
        </span>
      ),
      dataKey: 'uploaded',
      dynamicHeight: true,
      disableSort: true,
      label: 'Uploaded',
      width: 150,
    },
    {
      cellRenderer: () => ({ rowData }) => (
        <span>{rowData?.notification?.packageType}</span>
      ),
      dataKey: 'packageType',
      dynamicHeight: true,
      disableSort: true,
      label: 'Package Type',
      width: 200,
    },
    {
      cellRenderer: () => ({ rowData }) => (
        <span
          onClick={e => handleCopy(e, rowData?.notification?.options)}
          title={`${rowData.notification?.options}`}
        >
          {rowData?.notification?.options}
        </span>
      ),
      dataKey: 'options',
      dynamicHeight: true,
      disableSort: true,
      label: 'Options',
      width: 700,
    },
  ]

  if (!currentUser?.auth.isSysAdmin) {
    return <Redirect to="/organizations" />
  }

  const variables = {
    input: {
      skip: 0,
      sort: { direction: 'desc', field: ['updated'] },
      take: 10,
      search: {
        criteria: [
          {
            or: [
              {
                field: 'status',
                operator: {
                  eq: selectedStatus,
                },
              },
            ],
          },
        ],
      },
    },
  }

  const { data, loading } = useQuery(query.GET_CHAPTERS_OVERVIEW, {
    variables,
  })

  const loadedData =
    cloneDeep(
      data?.getChaptersOverview?.results.map(item => ({
        checked: selectedRows.includes(item.id),
        ...item,
      })),
    ) || []

  // ?.getChaptersOverview.results || []
  const RetryBulkAction = () => (
    <RetryBulk
      loadedData={loadedData}
      queryFn={query.GET_CHAPTERS_OVERVIEW}
      selectedRows={selectedRows}
      setSelectedRows={setSelectedRows}
      status={selectedStatus}
      variables={variables}
    />
  )

  return (
    <Root>
      <Top>
        <FiltersRow>
          <Filter
            defaultValue={{ value: 'converting', label: 'Converting' }}
            isClearable
            onChange={v => {
              if (!v) setSelectedStatus(null)
              if (v) setSelectedStatus(v.value)
              setSelectedRows([])
            }}
            options={[
              { value: 'converting', label: 'Converting' },
              { value: 'loading-preview', label: 'Loading Preview' },
              { value: 'publishing', label: 'Publishing' },
            ]}
            placeholder="Status"
          />
        </FiltersRow>
      </Top>

      <TableWithToolbar
        data={data?.getChaptersOverview}
        noPagination
        selected={selectedRows}
        setSelected={setSelectedRows}
        tools={{
          right: [RetryBulkAction],
          left: [],
        }}
      >
        {props => {
          const { checkboxColumn } = props

          return (
            <Table
              columns={[checkboxColumn].concat(columns)}
              headerHeight={40}
              key="usersPage"
              loadedData={loadedData}
              loading={loading}
              onColumnClick={col => {
                if (col.dataKey === 'id') col.event.stopPropagation()
              }}
              onRowClick={() => {}}
              selectedRows={selectedRows}
              setSelectedRows={setSelectedRows}
              // target="getChaptersOverview"
              // variables={variables}
            />
          )
        }}
      </TableWithToolbar>
    </Root>
  )
}

export default ChaptersPanel
