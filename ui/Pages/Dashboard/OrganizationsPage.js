/* eslint-disable react/prop-types */
import React, { useContext } from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { TableLocalData } from '../../components/Datatable'
import ConstantsContext from '../../../app/constantsContext'
import { NavBar } from '../../components'

const Root = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

const OrganizationsPage = props => {
  const { s } = useContext(ConstantsContext)
  const history = useHistory()
  const { organizationsList } = props

  const columns = [
    {
      cellRenderer: () => ({ cellData, rowData }) => <span>{cellData}</span>,
      dataKey: 'name',
      dynamicHeight: true,
      disableSort: true,
      label: 'Name',
    },
    {
      dataKey: 'email',
      disableSort: true,
      label: 'Email',
    },

    {
      cellRenderer: () => ({ cellData }) => (
        <span>
          {cellData.type.publisher && 'Publisher '}
          {cellData.type.funder && 'Funder'}
        </span>
      ),
      dataKey: 'settings',
      label: 'Type',
    },
  ]

  return (
    <Root>
      <div>
        <NavBar>Organizations</NavBar>
      </div>
      <TableLocalData
        columns={columns}
        dataTable={organizationsList}
        headerHeight={40}
        key="usersPage"
        noDataMessage={s('pages.noOrgAssigned')}
        onColumnClick={cprops => {
          if (cprops.dataKey === 'id') cprops.event.stopPropagation()
        }}
        onRowClick={({ rowData, index, data }) => {
          history.push(`/organizations/${rowData.id}`)
        }}
        rowHeight={40}
      />
    </Root>
  )
}

export default OrganizationsPage
