/* eslint-disable react/prop-types */
import React from 'react'

import { Button } from '../../components'

export default ({ selectedRows, setSelectedRows }) => (
  <Button icon="archive"> Archive</Button>
)
