import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Steps } from '@pubsweet/ui'
// import { grid, th } from '@pubsweet/ui-toolkit'

import { Spinner } from '../../components/common'
import StepOne from './StepOne'
import BookSettings from '../../components/BookSettings'

// #region styled
const Wrapper = styled.div`
  height: 100%;
  padding: 30px 20px 50px 20px;
`

const StepsWrapper = styled.div`
  margin: 0 auto 30px auto;
  max-width: 600px;
`

const Main = styled.div`
  height: calc(100% - 131px);
`
// #endregion styled

const CreateBook = props => {
  const {
    className,

    // initial data
    acceptedWorkflows,
    collectionOptions,
    currentStep,
    organizationId,
    publisherOptions,
    showFundedContentType,

    // second step (after creation)
    bookType,
    workflow,
    stepTwoInitialValues,
    inCollection,
    inFundedCollection,

    // submit methods
    createBook,
    updateBookSettings,

    // loading states
    loading,
    isSaving,

    // permissions
    isEditor,
    isOrgAdmin,
    isSysAdmin,

    isFunderOrganization,
    isPublisherOrganization,
  } = props

  if (loading) return <Spinner pageLoader />

  return (
    <Wrapper className={className}>
      <StepsWrapper>
        <Steps currentStep={currentStep}>
          <Steps.Step title="Create book" />
          <Steps.Step title="Book settings" />
        </Steps>
      </StepsWrapper>

      <Main>
        {isSaving && currentStep === 0 && (
          <Spinner label="Creating book..." pageLoader />
        )}

        {isSaving && currentStep === 1 && (
          <Spinner label="Updating book settings..." pageLoader />
        )}

        {!isSaving && currentStep === 0 && (
          <StepOne
            acceptedWorkflows={acceptedWorkflows}
            collectionOptions={collectionOptions}
            isFunderOrganization={isFunderOrganization}
            isPublisherOrganization={isPublisherOrganization}
            onSubmit={createBook}
            organizationId={organizationId}
            publisherOptions={publisherOptions}
            showFundedContentType={showFundedContentType}
          />
        )}

        {!isSaving && currentStep === 1 && stepTwoInitialValues && (
          <BookSettings
            inCollection={inCollection}
            inFundedCollection={inFundedCollection}
            initialFormValues={stepTwoInitialValues}
            isEditor={isEditor}
            isOrgAdmin={isOrgAdmin}
            isSysAdmin={isSysAdmin}
            onSubmit={updateBookSettings}
            showSubmitButton
            submitButtonLabel="Save"
            type={bookType}
            variant="newBook"
            workflow={workflow}
          />
        )}
      </Main>
    </Wrapper>
  )
}

CreateBook.propTypes = {
  acceptedWorkflows: PropTypes.arrayOf(PropTypes.oneOf(['pdf', 'xml', 'word'])),
  collectionOptions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ),
  currentStep: PropTypes.number.isRequired,
  organizationId: PropTypes.string.isRequired,
  publisherOptions: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      value: PropTypes.string,
    }),
  ),
  showFundedContentType: PropTypes.bool,

  createBook: PropTypes.func.isRequired,
  updateBookSettings: PropTypes.func.isRequired,

  inCollection: PropTypes.bool,
  inFundedCollection: PropTypes.bool,

  bookType: PropTypes.oneOf(['chapterProcessed', 'wholeBook']),
  workflow: PropTypes.oneOf(['word', 'xml', 'pdf']),
  /* eslint-disable-next-line react/forbid-prop-types */
  stepTwoInitialValues: PropTypes.object,

  loading: PropTypes.bool,
  isSaving: PropTypes.bool,

  isEditor: PropTypes.bool.isRequired,
  isOrgAdmin: PropTypes.bool.isRequired,
  isSysAdmin: PropTypes.bool.isRequired,

  isFunderOrganization: PropTypes.bool.isRequired,
  isPublisherOrganization: PropTypes.bool.isRequired,
}

CreateBook.defaultProps = {
  acceptedWorkflows: [],
  bookType: null,
  collectionOptions: [],
  workflow: null,
  stepTwoInitialValues: null,
  publisherOptions: [],
  showFundedContentType: false,

  inCollection: false,
  inFundedCollection: false,

  loading: false,
  isSaving: false,
}

export default CreateBook
