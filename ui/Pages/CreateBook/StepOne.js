import React, { useContext, useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Formik, Form } from 'formik'
import * as yup from 'yup'
import { v4 as uuid } from 'uuid'

import { grid } from '@pubsweet/ui-toolkit'

import ConstantsContext from '../../../app/constantsContext'
import { Button } from '../../components/common'
import FormSection from '../../components/common/FormSection'
import {
  SelectComponent as Select,
  RadioComponent as Radio,
  TextFieldComponent as UITextField,
  NumberInputComponent as NumberInput,
} from '../../components/FormElements/FormikElements'

// #region styled
const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 800px;

  > form > div:not(:last-child) {
    margin-bottom: ${grid(2)};
  }
`

const TextField = styled(UITextField)`
  margin-bottom: 0;
`

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: end;
`
// #endregion styled

const StepOne = props => {
  const {
    className,

    acceptedWorkflows,
    collectionOptions,
    isFunderOrganization,
    isPublisherOrganization,
    onSubmit,
    organizationId,
    publisherOptions,
  } = props

  const { s } = useContext(ConstantsContext)
  const [submissionTypeKey, setSubmissionTypeKey] = useState(uuid())
  const [workflowKey, setWorkflowKey] = useState(uuid())
  const [wholeBookType, setWholeBookType] = useState(false)
  const [wordBook, setWordBook] = useState(false)

  const initialFormValues = {
    collection: null,
    fundedContentType: null,
    conversionWorkflow: null,
    publisher:
      (publisherOptions.length === 1 && publisherOptions[0].value) ||
      publisherOptions.find(option => option.value === organizationId)?.value ||
      null,
    submissionType: null,
    title: '',
    version: 1,
  }

  const regMatch = /^((([1]$)|[1-9][0-9]{0,1}[0-9]{0,1})|1000)$/

  const validations = yup.object().shape({
    // collection: null,
    conversionWorkflow: yup
      .string()
      .nullable()
      .required('Conversion workflow is required'),
    publisher: yup.string().nullable().required('Publisher is required'),
    submissionType: yup
      .string()
      .nullable()
      .required('Submission type is required'),
    title: yup.string().required('Title is required'),
    fundedContentType: yup
      .string()
      .nullable()
      .required('Content type is required'),
    version:
      wholeBookType &&
      wordBook &&
      yup
        .string()
        .matches(regMatch, 'Version Number should be numeric')
        .required('Version Number is required'),
  })

  return (
    <Wrapper className={className}>
      <Formik
        initialValues={initialFormValues}
        onSubmit={onSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const { setFieldValue, values } = formProps

          const {
            title,
            publisher,
            collection,
            fundedContentType,
            conversionWorkflow,
            submissionType,
            version,
          } = values

          // #region options
          const fundedContentTypeOptions = [
            {
              label: 'Author manuscript',
              value: 'authorManuscript',
            },
            {
              label: 'Published PDF',
              value: 'publishedPDF',
            },
            {
              label: 'Prepublication draft',
              value: 'prepublicationDraft',
            },
            {
              label: 'Final full-text',
              value: 'finalFullText',
            },
          ]

          const conversionWorkflowOptions = [
            {
              label: 'Word',
              value: 'word',
              disabled:
                !acceptedWorkflows.includes('word') ||
                (fundedContentType && fundedContentType !== 'finalFullText'),
            },
            {
              label: 'PDF',
              value: 'pdf',
              disabled: !acceptedWorkflows.includes('pdf'),
            },
            {
              label: 'XML',
              value: 'xml',
              disabled:
                !acceptedWorkflows.includes('xml') ||
                fundedContentType === 'authorManuscript',
            },
          ]

          const submissionTypeOptions = [
            {
              label: 'Complete books and documents',
              value: 'wholeBook',
            },
            {
              label: 'Individual chapters',
              value: 'individualChapters',
              disabled: ['prepublicationDraft', 'publishedPDF'].includes(
                fundedContentType,
              ),
            },
          ]

          let finalCollectionOptions = collectionOptions

          if (
            isFunderOrganization &&
            isPublisherOrganization &&
            publisher !== organizationId
          ) {
            finalCollectionOptions = collectionOptions.filter(
              o => o.isOwnCollection,
            )
          }
          // #endregion options

          const handlePublisherChange = ({ value }) => {
            if (!(isFunderOrganization && isPublisherOrganization)) return
            const newPublisherIsSelf = value === organizationId
            const currentCollectionIsOfAnotherOrg = !!collection.organization

            // if not you, you shouldn't see funded collections of other orgs
            if (!newPublisherIsSelf && currentCollectionIsOfAnotherOrg) {
              setFieldValue('collection', null)
            }
          }

          const handleWorkflowChange = v => {
            if (v === 'word') {
              setWordBook(false)
              // setFieldValue('submissionType', 'individualChapters')
              // HACK bypass pubsweet radio group not being a controlled component
              setSubmissionTypeKey(uuid())
            } else setWordBook(true)
          }

          const handleFundedContentTypeChange = option => {
            if (
              ['prepublicationDraft', 'publishedPDF'].includes(option?.value)
            ) {
              setFieldValue('submissionType', 'wholeBook')
              setSubmissionTypeKey(uuid())
              setWholeBookType(true)

              if (conversionWorkflow === 'word') {
                setFieldValue('conversionWorkflow', null)
                setWorkflowKey(uuid())
              }
            }

            if (option?.value === 'authorManuscript') {
              setFieldValue('conversionWorkflow', 'pdf')
              setWorkflowKey(uuid())
              setWordBook(true)
            }

            if (option?.value === 'finalFullText') {
              setFieldValue('conversionWorkflow', null)
              setWorkflowKey(uuid())
            }

            if (option?.value === 'finalFullText') {
              setFieldValue('conversionWorkflow', null)
              setWorkflowKey(uuid())
            }
          }

          const getDescription = key =>
            s(`fieldDescriptions.bookSettingsForm.${key}`)

          return (
            <Form noValidate>
              <FormSection gutterSize={3} label="Content Submission">
                <TextField
                  field={{ name: 'title', value: title }}
                  form={formProps}
                  label="Book title"
                  required
                />
                <Select
                  field={{ name: 'publisher', value: publisher }}
                  form={formProps}
                  isClearable={false}
                  isSearchable
                  label="Publisher"
                  onChange={handlePublisherChange}
                  options={publisherOptions}
                  required
                />

                <Select
                  data-test-id="collection-select"
                  description={getDescription('collection')}
                  disabled={
                    isFunderOrganization &&
                    isPublisherOrganization &&
                    !publisher
                  }
                  field={{ name: 'collection', value: collection }}
                  form={formProps}
                  hasHtmlLabels
                  isClearable
                  label="Collection"
                  options={finalCollectionOptions}
                />

                <Select
                  data-test-id="contentType-select"
                  description={s(
                    'fieldDescriptions.bookSettingsForm.contentType',
                  )}
                  field={{
                    name: 'fundedContentType',
                    value: fundedContentType,
                  }}
                  form={formProps}
                  isClearable
                  isSearchable={false}
                  label="Content type"
                  onChange={handleFundedContentTypeChange}
                  options={fundedContentTypeOptions}
                  required
                />

                <Radio
                  description={s(
                    'fieldDescriptions.bookSettingsForm.conversionWorkflow',
                  )}
                  field={{
                    name: 'conversionWorkflow',
                    value: conversionWorkflow,
                  }}
                  form={formProps}
                  key={workflowKey}
                  label="Conversion workflow"
                  onChange={handleWorkflowChange}
                  options={conversionWorkflowOptions}
                  required
                />

                <Radio
                  description={s(
                    'fieldDescriptions.bookSettingsForm.submissionType',
                  )}
                  field={{
                    name: 'submissionType',
                    value: submissionType,
                  }}
                  form={formProps}
                  key={submissionTypeKey}
                  label="Submission type"
                  onChange={value => {
                    value === 'wholeBook'
                      ? setWholeBookType(true)
                      : setWholeBookType(false)
                  }}
                  options={submissionTypeOptions}
                  required
                />
                {wholeBookType && wordBook && (
                  <NumberInput
                    description={s(
                      'fieldDescriptions.bookSettingsForm.versionInput',
                    )}
                    field={{
                      name: 'version',
                      value: version,
                    }}
                    form={formProps}
                    label="Version number"
                  />
                )}
              </FormSection>

              <ButtonWrapper>
                <Button data-test-id="next-step" status="primary" type="submit">
                  Next
                </Button>
              </ButtonWrapper>
            </Form>
          )
        }}
      </Formik>
    </Wrapper>
  )
}

StepOne.propTypes = {
  acceptedWorkflows: PropTypes.arrayOf(PropTypes.oneOf(['pdf', 'xml', 'word']))
    .isRequired,
  collectionOptions: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string,
    }),
  ).isRequired,
  isFunderOrganization: PropTypes.bool.isRequired,
  isPublisherOrganization: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  organizationId: PropTypes.string.isRequired,
  publisherOptions: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      value: PropTypes.string,
    }),
  ).isRequired,
}

StepOne.defaultProps = {}

export default StepOne
