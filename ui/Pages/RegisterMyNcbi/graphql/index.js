import { gql } from '@apollo/client'

const REGISTER_REQUEST_NCBI_USER = gql`
  mutation($input: NcbiUserCreateInput) {
    createMyNcbiUser(input: $input) {
      givenName
      surname
    }
  }
`

export default {
  REGISTER_REQUEST_NCBI_USER,
}
