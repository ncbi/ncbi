import React from 'react'
import { withFormik } from 'formik'
import { useMutation } from '@apollo/client'
import query from './graphql'
import Signup from './SignUpForm'
import { getNcbiCookie } from '../../../app/cookie'

const handleSubmit = (
  values,
  { props: { registerUser }, setSubmitting, setStatus, resetForm },
) => {
  const cookie = getNcbiCookie()

  return registerUser({
    variables: { input: { ...values, cookie } },
  })
    .then(({ errors }) => {
      if (!errors) {
        resetForm()
        setStatus({ success: 'User has been created successfully!' })
      }
    })
    .catch(e => {
      if (e.graphQLErrors) {
        setStatus({ error: e.graphQLErrors[0].message })
        setSubmitting(false)
      }
    })
}

const SignupFormik = withFormik({
  displayName: 'signup',
  handleSubmit,
  initialValues: {
    givenName: '',
    surname: '',
  },
  mapPropsToValues: props => ({
    givenName: props.givenName,
    surname: props.surname,
  }),
  validate: values => {
    const errors = {}

    if (!values.givenName) errors.givenName = 'Given name is required'
    if (!values.surname) errors.surname = 'Surname is required'

    return errors
  },
})(Signup)

const SignupContainer = () => {
  const [registerUser] = useMutation(query.REGISTER_REQUEST_NCBI_USER)

  return <SignupFormik registerUser={registerUser} />
}

export default SignupContainer
