/* eslint-disable react/prop-types */
import React from 'react'
import styled from 'styled-components'
import { ErrorText } from '@pubsweet/ui'
import { Field } from 'formik'
import { Redirect } from 'react-router-dom'

import { FormWrapper } from '../../components/FormElements'

import { TextFieldComponent, Button } from '../../components'

const InlineInputs = styled.div`
  display: flex;
  flex-direction: row;

  > div {
    width: 50%;
  }
`

const SignupForm = ({
  handleSubmit,
  errors,
  touched,
  isValid,
  status,
  setFieldValue,
}) => {
  return (
    <FormWrapper>
      <form onSubmit={handleSubmit}>
        <InlineInputs>
          <Field
            component={TextFieldComponent}
            label="Given name"
            name="givenName"
          />
          <Field
            component={TextFieldComponent}
            label="Surname"
            name="surname"
          />
        </InlineInputs>
        {status?.success && (
          <Redirect to={`/loginNcbi${window.location.search}`} />
        )}
        {status?.error && <ErrorText>{status.error}</ErrorText>}
        <Button disabled={!isValid} onClick={handleSubmit} status="primary">
          Register
        </Button>
      </form>
    </FormWrapper>
  )
}

export default SignupForm
