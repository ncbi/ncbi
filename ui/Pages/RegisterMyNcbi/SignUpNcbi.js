import React from 'react'
import styled from 'styled-components'

import Header from '../../components/LayoutElement/PageHeader'
import Footer from '../../components/LayoutElement/PageFooter'
import SignupContainer from './SignUpContainer'

const PageWrapper = styled.div`
  font-weight: 400;
`

const SignUpNcbi = () => (
  <PageWrapper>
    <Header />
    <SignupContainer />
    <Footer />
  </PageWrapper>
)

export default SignUpNcbi
