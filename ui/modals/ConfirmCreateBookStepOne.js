import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid, th } from '@pubsweet/ui-toolkit'

import { Button, ButtonGroup } from '../components/common'

import {
  Modal as DefaultModal,
  ModalBody as DefaultBody,
  ModalFooter as DefaultFooter,
  ModalHeader,
} from '../components/Modal'

const Modal = styled(DefaultModal)`
  min-height: unset;
`

const ModalBody = styled(DefaultBody)`
  margin-bottom: 0;
  padding: ${grid(6)} ${grid(2)};
  /* text-align: center; */
`

const ModalFooter = styled(DefaultFooter)`
  border-top: 1px solid ${th('colorFurniture')};
  height: unset;
  justify-content: end;
  padding: ${grid(1)};

  button {
    margin-left: 0;
    margin-right: ${grid(0.5)};
  }
`

const ConfirmCreateBookStepOne = props => {
  const { isOpen, onClickCancel, onClickSuccess } = props

  return (
    <Modal isOpen={isOpen} onClose={onClickCancel} width="700px">
      <ModalHeader hideModal={onClickCancel}>Create new book</ModalHeader>

      <ModalBody>
        Are you sure? Only NCBI System Admins can change these settings after
        the book is created.
      </ModalBody>

      <ModalFooter>
        <ButtonGroup>
          <Button onClick={onClickCancel} outlined>
            Cancel
          </Button>

          <Button onClick={onClickSuccess} status="primary">
            Yes, create book
          </Button>
        </ButtonGroup>
      </ModalFooter>
    </Modal>
  )
}

ConfirmCreateBookStepOne.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClickCancel: PropTypes.func.isRequired,
  onClickSuccess: PropTypes.func.isRequired,
}

ConfirmCreateBookStepOne.defaultProps = {}

export default ConfirmCreateBookStepOne
