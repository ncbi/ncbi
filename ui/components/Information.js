import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { Button, Icon } from './common'
// eslint-disable-next-line import/no-cycle
import { Modal, ModalBody, ModalHeader, ModalFooter } from '../index'

const Center = styled.div`
  text-align: center;
`

const ButtonRight = styled(Button)`
  margin-left: auto;
`

const Information = ({ isOpen, title, description, hideModal, type }) => {
  let color = 'white'

  switch (type) {
    case 'info':
      color = '#0B65CB'
      break
    case 'warning':
      color = 'orange'
      break
    case 'error':
      color = 'red'
      break
    case 'danger':
      color = 'red'
      break
    default:
      color = '#eee'
  }

  return (
    <Modal isOpen={isOpen} role="dialog" width="500px">
      <ModalHeader hideModal={hideModal} />
      <ModalBody type="small">
        <Center>
          <Icon color={color} size={6} title="Warning">
            alert-circle
          </Icon>
          <h4>{title}</h4>
          <div>{description}</div>
        </Center>
      </ModalBody>

      <ModalFooter>
        <ButtonRight onClick={hideModal} status="primary">
          Okay
        </ButtonRight>
      </ModalFooter>
    </Modal>
  )
}

Information.propTypes = {
  title: PropTypes.node,
  description: PropTypes.node,
  hideModal: PropTypes.func.isRequired,
  isOpen: PropTypes.bool,
  // eslint-disable-next-line react/no-typos
  type: PropTypes.string, // oneOf[('info', 'warning', 'error', 'danger')],
}

Information.defaultProps = {
  title: 'Are you sure ?',
  description: null,
  type: 'info',
  isOpen: false,
}

export default Information
