import { store } from 'react-notifications-component'

const capitalize = s => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

const getDefaultType = notificationType => {
  return notificationType === 'danger' ? 'Error' : capitalize(notificationType)
}

export default ({ message, type = 'success', title, duration = 5000 }) => {
  return store.addNotification({
    animationIn: ['animated', 'fadeIn'],
    animationOut: ['animated', 'fadeOut'],
    container: 'top-right',
    dismiss: {
      duration,
      showIcon: true,
    },
    message,
    title: title || getDefaultType(type),
    type,
  })
}
