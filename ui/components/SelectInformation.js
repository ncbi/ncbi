/* eslint-disable react/prop-types */
import React from 'react'
// eslint-disable-next-line import/no-cycle
import Information from './Information'

const SelectInformation = ({ setIsOpen, modalIsOpen }) => (
  // <StyledModal isOpen={modalIsOpen}>
  <Information
    hideModal={() => setIsOpen(false)}
    isOpen={modalIsOpen}
    title="Select at least one record"
    type="warning"
  />
  // </StyledModal>
)

export default SelectInformation
