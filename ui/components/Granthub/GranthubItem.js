import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { components } from 'react-select'

import { grid } from '@pubsweet/ui-toolkit'

import UIButton from '../common/Button'
import Icon from '../common/Icon'
import Select from '../common/Select'
import UIToggle from '../common/Toggle'

// #region styled
const Wrapper = styled.div`
  display: flex;
`

const SelectWrapper = styled.div`
  flex-grow: 1;
  margin-right: ${grid(3)};
`

const ToggleWrapper = styled.div`
  margin-right: ${grid(3)};
  /* visibility: ${props => (props.awardHasValue ? 'visible' : 'hidden')}; */

  ${props =>
    !props.showApply &&
    css`
      display: none;
    `}
`

const Toggle = styled(UIToggle)`
  display: inline-block;

  > div {
    border: none;
    padding: 1px 0;

    &:hover {
      border: none;
    }
  }
`

const ButtonWrapper = styled.div``

const Button = styled(UIButton)`
  margin: 0;
`
// #endregion styled

const nullFn = () => null

const IndicatorsContainer = props => {
  /* eslint-disable-next-line react/prop-types */
  const { children, hasValue, ...rest } = props

  return (
    <components.IndicatorsContainer {...rest}>
      {hasValue && <Icon color="colorSuccess">check</Icon>}
      {children}
    </components.IndicatorsContainer>
  )
}

const Granthub = props => {
  const {
    className,

    apply,
    applyName,
    awardNumber,
    awardNumberName,
    disabled,
    onApplyChange,
    onAwardNumberChange,
    onBlur,
    onDelete,
    onSearch,
    showApply,
  } = props

  let incomingValue = null

  if (awardNumber) {
    incomingValue = {
      label: awardNumber,
      value: awardNumber,
    }
  }

  const handleAwardChange = val => {
    // setSelectedValue(val?.value || null)
    onAwardNumberChange(val?.value)
  }

  const handleLoadOptions = searchValue => {
    return new Promise(resolve => {
      onSearch(searchValue).then(results => {
        if (!results) resolve([])
        let resultValues = results
        if (!Array.isArray(results)) resultValues = [results]

        const options = resultValues.map(v => ({
          label: v,
          value: v,
        }))

        resolve(options)
      })
    })
  }

  const IndicatorSContainerEnhanced = itemProps => (
    <IndicatorsContainer {...itemProps} hasValue={!!incomingValue?.value} />
  )

  return (
    <Wrapper className={className}>
      <SelectWrapper>
        <Select
          async
          components={{
            DropdownIndicator: nullFn,
            IndicatorsContainer: IndicatorSContainerEnhanced,
          }}
          data-test-id="granthub-id"
          disabled={disabled}
          isClearable
          isSearchable
          loadOptions={handleLoadOptions}
          name={awardNumberName}
          noOptionsMessage={() => 'Award ID not found'}
          onBlur={onBlur}
          onChange={handleAwardChange}
          openMenuOnClick={false}
          openMenuOnFocus={false}
          placeholder="Search Granthub by exact award ID"
          value={incomingValue}
        />
      </SelectWrapper>

      <ToggleWrapper
        awardHasValue={!!incomingValue?.value}
        showApply={showApply}
      >
        <Toggle
          checked={apply}
          disabled={disabled}
          name={applyName}
          onBlur={onBlur}
          onClick={onApplyChange}
        />
      </ToggleWrapper>

      <ButtonWrapper>
        <Button
          disabled={disabled}
          icon="trash-2"
          onClick={onDelete}
          status="danger"
        >
          Delete
        </Button>
      </ButtonWrapper>
    </Wrapper>
  )
}

Granthub.propTypes = {
  onDelete: PropTypes.func.isRequired,
  /** A function that returns a promise, that resolves to either a string or an array of strings */
  onSearch: PropTypes.func.isRequired,

  apply: PropTypes.bool,
  applyName: PropTypes.string,
  awardNumber: PropTypes.string,
  awardNumberName: PropTypes.string,
  disabled: PropTypes.bool,
  onApplyChange: PropTypes.func,
  onAwardNumberChange: PropTypes.func,
  showApply: PropTypes.bool,
}

Granthub.defaultProps = {
  apply: false,
  applyName: null,
  awardNumber: null,
  awardNumberName: null,
  disabled: false,
  onApplyChange: nullFn,
  onAwardNumberChange: nullFn,
  showApply: false,
}

export default Granthub
