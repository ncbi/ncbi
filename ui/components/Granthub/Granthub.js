import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { FieldArray, ErrorMessage } from 'formik'
import { v4 as uuid } from 'uuid'

import { grid, th } from '@pubsweet/ui-toolkit'

import { Error } from '../FormElements/FormikElements'
import Button from '../common/Button'
import Note from '../common/Note'
import GranthubItem from './GranthubItem'

// #region styled
const Wrapper = styled.div``

const ButtonWrapper = styled.div``

const AwardsWrapper = styled.div`
  border-radius: ${th('borderRadius')};

  > div {
    margin-top: ${grid(2)};
  }
`

const Empty = styled.div`
  border: 1px solid ${th('colorBorder')};
  padding: ${grid(1)};
  text-align: center;
`

const LabelWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`
// #endregion styled

const Granthub = props => {
  const {
    className,

    awards,
    disabled,
    name,
    onSearch,
    showApply,
    variant,
    isWholeBook,
  } = props

  const applyMessage = `Turn the toggle on to apply that grant to all ${
    variant === 'collection' ? 'whole books' : 'chapters'
  } in this ${variant === 'collection' ? 'collection' : 'book'}`

  const disabledMessage = `Add a grant from the NCBI grant database. If this grant has already been provided for the ${
    variant === 'book' ? 'collection' : 'book'
  } and set to be applied to all ${
    variant === 'book' ? 'books' : 'chapters'
  } in that ${isWholeBook ? 'collection' : 'unit'}, it will not be editable.`

  return (
    <FieldArray name={name}>
      {arrayHelpers => {
        const handleAdd = () => {
          arrayHelpers.push({
            id: uuid(),
            number: '',
            apply: false,
          })
        }

        const handleDelete = index => {
          arrayHelpers.remove(index)
        }

        const handleAwardNumberChange = (val, index) => {
          arrayHelpers.form.setFieldValue(`${name}[${index}].number`, val)
        }

        const handleApplyChange = (val, index) => {
          arrayHelpers.form.setFieldValue(`${name}[${index}].apply`, val)
        }

        const handleBlur = () => {
          arrayHelpers.form.setFieldTouched(name)
        }

        const hasParentAwards = awards.find(g => g.appliedFromParent)

        return (
          <Wrapper className={className}>
            <ButtonWrapper>
              <Button
                data-test-id="add-grant"
                disabled={disabled}
                icon="plus"
                onClick={handleAdd}
                status="primary"
              >
                Add grant
              </Button>
            </ButtonWrapper>

            <AwardsWrapper>
              {awards.length === 0 && <Empty>No awards have been added</Empty>}

              {awards.length > 0 && (
                <>
                  {showApply && <Note content={applyMessage} />}
                  {hasParentAwards && <Note content={disabledMessage} />}

                  <LabelWrapper>
                    <div>Award ID</div>
                    {/* <div>Apply to all wholebooks in this collection</div> */}
                  </LabelWrapper>

                  {awards.map((award, index) => {
                    return (
                      <GranthubItem
                        apply={award.apply}
                        applyName={`${name}[${index}].apply`}
                        awardNumber={award.number}
                        awardNumberName={`${name}[${index}].number`}
                        disabled={disabled || award.appliedFromParent}
                        key={award.id}
                        onApplyChange={val => handleApplyChange(val, index)}
                        onAwardNumberChange={val =>
                          handleAwardNumberChange(val, index)
                        }
                        onBlur={handleBlur}
                        onDelete={() => handleDelete(index)}
                        onSearch={onSearch}
                        showApply={showApply}
                      />
                    )
                  })}
                </>
              )}
            </AwardsWrapper>

            <ErrorMessage component={Error} name={name} />
          </Wrapper>
        )
      }}
    </FieldArray>
  )
}

Granthub.propTypes = {
  name: PropTypes.string.isRequired,
  onSearch: PropTypes.func.isRequired,

  awards: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string,
      apply: PropTypes.bool,
    }),
  ),
  disabled: PropTypes.bool,
  showApply: PropTypes.bool,
  variant: PropTypes.string,
  isWholeBook: PropTypes.bool,
}

Granthub.defaultProps = {
  awards: [],
  disabled: false,
  showApply: false,
  variant: null,
  isWholeBook: false,
}

export default Granthub
