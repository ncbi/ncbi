import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export const HasNextPage = styled(Link)`
  align-items: center;
  background: ${th('colorBackground')};
  display: flex;
  justify-content: center;
  text-decoration: none;
  width: 100%;
`

export const NextPageButton = styled.span`
  color: ${th('colorPrimary')};
  display: flex;
  flex: 1;
  font-size: 15px;
  font-weight: 500;
  justify-content: center;
  min-height: 40px;
  padding: 8px;
  position: relative;
  width: 100%;

  &:hover {
    cursor: pointer;
  }
`
