/* eslint-disable react/prop-types */
import React from 'react'
import moment from 'moment'
import styled from 'styled-components'
import { Link } from '@pubsweet/ui'
import { getDisplayName } from '../../common/utils'
import { getDownloadLink } from '../../../app/utilsConfig'
import { DeleteFileVersion } from '.'
import { ButtonGroup, Button, Status } from '..'

const StyledLink = styled(Link)`
  min-width: 0;
  text-transform: none;
`

export const name = {
  cellRenderer: () => ({ cellData, rowData }) => {
    return (
      <StyledLink to={getDownloadLink(rowData.id, '')}>{cellData}</StyledLink>
    )
  },
  dataKey: 'name',
  disableSort: true,
  label: 'Filename ',
}

export const versionName = bcVersion => ({
  cellRenderer: () => ({ cellData }) => (
    <>
      V{bcVersion}.{cellData}
    </>
  ),
  dataKey: 'versionName',
  disableSort: true,
  label: 'File Version',
  width: 250,
})

export const created = {
  cellRenderer: () => ({ cellData, rowData }) => (
    <>
      {moment.utc(cellData).utcOffset(moment().utcOffset()).format('L LT')}

      {rowData.owner
        ? `by ${getDisplayName(rowData.owner)}`
        : 'by Unknown User'}
    </>
  ),
  dataKey: 'created',
  disableSort: true,
  label: 'Uploaded ',
  width: 600,
}

export const updated = {
  cellRenderer: () => ({ cellData, rowData }) => (
    <>
      {moment.utc(cellData).utcOffset(moment().utcOffset()).format('L LT')}{' '}
      {rowData.owner?.username && `by ${getDisplayName(rowData.owner)}`}
    </>
  ),
  dataKey: 'updated',
  disableSort: true,
  label: 'Uploaded ',
  width: 400,
}

export const sourceFile = {
  cellRenderer: () => ({ cellData, rowData }) => {
    return (
      <ButtonGroup>
        <StyledLink to={getDownloadLink(rowData.sourceFile?.id, 'source')}>
          {cellData}
        </StyledLink>
      </ButtonGroup>
    )
  },
  dataKey: 'name',
  disableSort: true,
  label: 'Source ',
}

export const status = {
  cellRenderer: () => ({ cellData, rowData }) => {
    return (
      <Status
        //   locked={files[0]?.id !== rowData.id}
        status={cellData}
        type="file"
      />
    )
  },
  dataKey: 'status',
  disableSort: true,
  label: 'Status',
  width: 450,
}

const isMaxfileVersion = (rowData, files) =>
  Number(rowData.versionName) ===
  (Math.max(
    ...files.map(o =>
      rowData.parentId === o.parentId ? Number(o.versionName) : 0,
    ),
  ) || 1)

export const deleteFile = ({
  files,
  //   isMaxfileVersion,
  deleteFileMutation,
  disableUploadButton,
  section,
  setSection,
}) => ({
  cellRenderer: () => ({ rowData }) => {
    const hasMultipleVersions = () => {
      const count = files.filter(i => i.parentId === rowData.parentId).length

      return count > 1
    }

    if (!isMaxfileVersion(rowData, files)) return null

    return (
      <DeleteFileVersion
        deleteFileMutation={deleteFileMutation}
        disabled={disableUploadButton}
        fileDetails={rowData}
        filesList={section}
        hasMultipleVersions={hasMultipleVersions}
        setSection={setSection}
        versionName={rowData.versionName}
      />
    )
  },
  dataKey: 'bcfId',
  disableSort: true,
  width: 400,
})

export const converted = ({ files, reconvertBookComponent }) => {
  return {
    cellRenderer: () => ({ cellData, rowData, blockLoadPrev }) => {
      return (
        <>
          <StyledLink to={getDownloadLink(rowData.sourceFile.id, 'source')}>
            {cellData?.name}
          </StyledLink>

          {rowData.id === files[0].id && (
            <div>
              <Button
                disabled={blockLoadPrev || rowData.status === 'loading-preview'}
                icon="eye"
                onClick={() => {
                  reconvertBookComponent()
                  //   setblockLoadPrev(true)
                }}
                outlined
                size="small"
                title="Load preview"
              />

              <Button
                disabled
                icon="share"
                onClick={() => {
                  // reconvertBookComponent()
                }}
                outlined
                size="small"
                title="Replace converted xml"
              />
            </div>
          )}
        </>
      )
    },
    dataKey: 'sourceFile',
    disableSort: true,
    label: 'File name ',
  }
}
