/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-param-reassign */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import React from 'react'
import styled from 'styled-components'

import { TableLocalData } from '../Datatable'
// eslint-disable-next-line import/no-cycle
import { Accordion, Button, Line } from '..'
import { FileUpload } from '.'

const StyledTableLocalData = styled(TableLocalData)`
  .ReactVirtualized__Table__rowColumn {
    color: #eee;
  }
`

const RightButton = styled(Button)`
  bottom: -8px;
  position: absolute;
  right: 20px;
`

const Footer = styled.div`
  align-content: center;
  display: block;
  /* border-top: 1px solid blue; */
  /* display: flex; */
  margin-bottom: 20px;
  margin-top: 20px;
  position: relative;
  width: 100%;
`

const FileSection = ({
  title,
  data = [],
  columns,
  startExpanded = false,
  more = false,
  showMoreFn,
  showLessFn,
  headerRightComponent,
  selectedRows,
  setSelectedRows,
  uploadingFiles,
  saveuploadFileFn,
  removeFn,
  isUploading,
  onRowClick,
}) => {
  const showFooterButton = showMoreFn && showLessFn

  return (
    <>
      <Accordion
        headerRightComponent={headerRightComponent}
        label={title}
        startExpanded={startExpanded}
      >
        {uploadingFiles?.length > 0 && (
          <FileUpload
            isUploading={isUploading}
            removeFn={removeFn}
            saveuploadFileFn={saveuploadFileFn}
            sectionLabel={title}
            uploadingFiles={uploadingFiles}
          />
        )}

        <StyledTableLocalData
          columns={columns}
          dataTable={data}
          headerHeight={50}
          key="book-component-files"
          noDataMessage="No files available"
          onRowClick={() => onRowClick}
          rowHeight={50}
          selectedRows={selectedRows}
          setSelectedRows={setSelectedRows}
        />

        {showFooterButton &&
          (!more ? (
            <Footer>
              <Line />

              <RightButton
                background="white"
                onClick={e => showMoreFn()}
                outlined
                status="primary"
              >
                Show all versions
              </RightButton>
            </Footer>
          ) : (
            <Footer>
              <Line />

              <RightButton
                background="white"
                onClick={e => showLessFn()}
                outlined
                status="primary"
              >
                Show Less
              </RightButton>
            </Footer>
          ))}
      </Accordion>

      <Line />
    </>
  )
}

export default FileSection
