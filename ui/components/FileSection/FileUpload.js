/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-param-reassign */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import React, { useContext, useEffect, useState } from 'react'

import styled from 'styled-components'
import { ErrorText } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { Icon } from '../common'
// eslint-disable-next-line import/no-cycle
import { Button, ProgressBar, ButtonGroup } from '..'
import ConstantsContext from '../../../app/constantsContext'

const FileUploadWrapper = styled.span`
  border: 1px solid ${th('colorGreyLight')};
  display: flex;
  flex-direction: column;
  margin: ${th('gridUnit')};
  padding: calc(${th('gridUnit')}*2);
`

const FileRow = styled.div`
  align-content: stretch;
  align-items: center;
  display: flex;
  justify-content: space-between;
  width: 60%;

  &:hover {
    background: ${th('colorGreyLight')};
  }
`

const RowItem = styled.div`
  align-items: center;
  flex-basis: 0;
  flex-grow: 1;
`

const RowItemIcon = styled.div`
  flex: 0 1 40px;
`

const FileUpload = ({
  isUploading,
  uploadingFiles,
  saveuploadFileFn,
  sectionLabel,
  removeFn,
}) => {
  const { s } = useContext(ConstantsContext)
  const [hasOneError, setHasOneError] = useState(false)

  useEffect(() => {
    setHasOneError(
      uploadingFiles.filter(item => item.errors?.length > 0)?.length > 0,
    )
  }, [uploadingFiles])

  return (
    <FileUploadWrapper>
      {uploadingFiles?.progress ? (
        <ProgressBar
          circleOneStroke="#d9edfe"
          circleTwoStroke="#0B65CB"
          progress={uploadingFiles?.progress}
          size={30}
          strokeWidth={8}
        />
      ) : null}

      <>
        {uploadingFiles.map((item, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <FileRow data-test-id={item.name} key={index + 1}>
            <RowItemIcon>
              {item.progress ? (
                <ProgressBar
                  circleOneStroke="#d9edfe"
                  circleTwoStroke="#0B65CB"
                  progress={item.progress}
                  size={30}
                  strokeWidth={8}
                />
              ) : null}
            </RowItemIcon>

            <RowItem>
              <span>{item.name}</span>
            </RowItem>

            <RowItem>
              <span>
                <ul>
                  {item.errors?.length > 0
                    ? item.errors?.map(error => (
                        <li key={error}>
                          <ErrorText>{error}</ErrorText>
                        </li>
                      ))
                    : s(`upload.${item.status}`)}
                </ul>
              </span>
            </RowItem>

            <RowItemIcon>
              {item.status === 'readyToUpload' || item.errors?.length > 0 ? (
                <Icon color="colorError" onClick={() => removeFn(item)}>
                  x-circle
                </Icon>
              ) : null}
            </RowItemIcon>
          </FileRow>
        ))}
      </>
      <br />

      <ButtonGroup>
        <Button
          data-test-id="save-btn"
          disabled={hasOneError || isUploading}
          onClick={() => saveuploadFileFn()}
          outlined
          status="primary"
        >
          {isUploading ? `Saving...` : 'Save'}
        </Button>

        <Button
          data-test-id="remove-btn"
          disabled={isUploading}
          onClick={() => removeFn()}
          outlined
          status="primary"
        >
          Remove {uploadingFiles.length > 1 ? ' all' : ''}
        </Button>
      </ButtonGroup>
    </FileUploadWrapper>
  )
}

export default FileUpload
