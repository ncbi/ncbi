/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react'
import { TextField as uiTextField } from '@pubsweet/ui'
import { grid } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { Button, Link } from '..'
import { getDownloadLink } from '../../../app/utilsConfig'

const TextField = styled(uiTextField)`
  margin-bottom: 0;
  max-height: 30px;
  width: 80%;
`

const RightButton = styled(Button)`
  margin-left: auto;
  margin-right: ${grid(1)};
`

const RenameButton = styled(RightButton)`
  margin-right: 30px;
`

export default ({ colName, colId, renameFn, isDisabled }) => {
  const [rename, openRename] = useState(false)
  const [newName, setNewName] = useState(colName)
  const link = getDownloadLink(colId)

  useEffect(() => {
    setNewName(colName)
  }, [colName])

  if (!rename) {
    return (
      <>
        <Link title={newName} to={link}>
          {newName}
        </Link>
        <RenameButton
          disabled={isDisabled}
          onClick={e => openRename(true)}
          outlined
          status="primary"
        >
          Rename
        </RenameButton>
      </>
    )
  }

  return (
    <>
      <TextField
        onChange={e => {
          setNewName(e.currentTarget.value)
        }}
        value={newName}
      />
      <RightButton
        onClick={e => {
          openRename(false)
          renameFn(newName)
        }}
        status="primary"
      >
        Save
      </RightButton>
      <RenameButton
        disabled={isDisabled}
        onClick={e => {
          setNewName(colName)
          openRename(false)
        }}
        outlined
      >
        Cancel
      </RenameButton>
    </>
  )
}

// export  ColumnRename
