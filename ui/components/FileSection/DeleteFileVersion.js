/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
/* eslint-disable camelcase */

import React, { useState, useEffect } from 'react'
import { SelectDropdown } from '@pubsweet/ui'
import styled from 'styled-components'
// eslint-disable-next-line import/no-cycle
import { Button, Modal, ModalFooter } from '..'

const ModalBody = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  min-height: 150px;
  padding: 10px;
`

const DeleteFileVersion = ({
  versionName,
  fileDetails,
  hasMultipleVersions,
  setSupplementaryFiles,
  filesList,
  disabled,
  deleteFileMutation,
}) => {
  const [modalIsOpen, setIsOpen] = useState(false)

  const options = [
    {
      label: `Current Version (V${versionName})`,
      selected: true,
      value: versionName,
    },
    { label: 'All Versions', selected: false, value: null },
  ]

  const [selectOptions, setSelectedOptions] = useState(options)

  useEffect(() => {
    const items = selectOptions.map((el, index) => {
      el.selected = index === 0
      return el
    })

    setSelectedOptions([...items])
  }, [modalIsOpen])

  return (
    <>
      <Button
        data-test-id="book-comp-delete"
        disabled={disabled}
        icon="trash-2"
        onClick={e => {
          setIsOpen(true)
        }}
        outlined
        status="danger"
      />
      <Modal isOpen={modalIsOpen}>
        <ModalBody>
          {hasMultipleVersions ? (
            <>
              <span> Which file version do you want to delete: </span>
              <SelectDropdown
                onChange={item => {
                  const items = selectOptions.map(el => {
                    el.selected = el.value === item.value

                    return el
                  })

                  setSelectedOptions([...items])
                }}
                options={selectOptions}
                value={selectOptions.find(item => item.selected)}
              />
            </>
          ) : (
            <span>
              Are you sure you want to delete this file? This action cannot be
              undone.
            </span>
          )}
        </ModalBody>
        <ModalFooter>
          <Button
            data-test-id="confirm"
            onClick={async () => {
              await deleteFileMutation({
                variables: {
                  parentId: fileDetails.parentId,
                  versionName:
                    selectOptions.find(sel => sel.selected).value || undefined,
                },
              })

              if (
                selectOptions.find(x => x.selected).label === 'All Versions'
              ) {
                const files = filesList.files.filter(
                  item => item.parentId !== fileDetails.parentId,
                )

                const { more } = filesList
                setSupplementaryFiles({ files, more })
              } else {
                const files = filesList.files.filter(
                  item => item.id !== fileDetails.id,
                )

                const { more } = filesList
                setSupplementaryFiles({ files, more })
              }

              setIsOpen(false)
            }}
            status="primary"
          >
            Delete
          </Button>

          <Button
            data-test-id="cancel"
            onClick={() => setIsOpen(false)}
            outlined
            status="primary"
          >
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </>
  )
}

export default DeleteFileVersion
