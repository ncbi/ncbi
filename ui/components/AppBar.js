import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { Action, Link } from '@pubsweet/ui'
import { grid, th } from '@pubsweet/ui-toolkit'

import UserMenuItem from './UserMenuItem'
import {
  BackLink,
  Button as DefaultButton,
  SvgIcon as DefaultSvgIcon,
} from './common'

import DisplayInlineEditor from './wax/DisplayInlneEditor'

import Logo from '../../public/bcms-logo.png'
import PermissionsGate from '../../app/components/PermissionsGate'

// #region styled
const Root = styled.nav`
  background: ${th('colorPrimary')};
  color: ${th('colorTextReverse')};
  display: flex;
  justify-content: space-between;
  padding: ${grid(0.5)} ${grid(2)};
`

const Side = styled.div`
  align-items: center;
  display: flex;
`

const TopLogo = styled.img`
  height: 20px;
`

const LogoLink = styled(Link)`
  margin-right: ${grid(1)};
  min-width: 67px;
  padding: 10px ${grid(1)} ${grid(0.5)} ${grid(0.5)};
`

const Item = styled.div``

const Button = styled(DefaultButton)`
  border-color: ${th('colorTextReverse')};
  color: ${th('colorTextReverse')};

  &:hover,
  &:focus {
    border-color: ${th('colorTextReverse')};
    color: ${th('colorTextReverse')};
  }
`

const AdminLabel = styled.span`
  border: 1px solid ${th('colorPrimary')};
  border-radius: 3px;
  color: ${th('colorPrimary')};
  font-size: ${th('fontSizeBaseSmall')};
  font-weight: bold;
  height: 24px;
  line-height: 23px;
  margin-left: ${grid(1)};
  margin-top: 4px;
  padding: 0 ${grid(1)};
  text-transform: uppercase;
`

const SvgIcon = styled(DefaultSvgIcon)`
  margin-right: ${grid(1)};
`
// #endregion styled

const AppBar = ({
  loginLink,
  onLogoutClick,

  displayName,
  isSysAdmin,

  canViewOrganizationAccess,
  onClickOrganizationAccess,

  showOrganization,
  showBackToOrganization,
  showBackToBook,
  organizationName,
  organizationUrl,
  bookName,
  bookUrl,
  showBackToCollection,
  collectionUrl,
  collectionName,
  organizationId,
}) => {
  const userExists = !!displayName

  const removeHistoryItem = () => {
    const localHistory =
      JSON.parse(sessionStorage.getItem('history') || '[]') || []

    if (localHistory?.length >= 0) {
      localHistory.splice(0, 1)
      sessionStorage.setItem(`history`, JSON.stringify(localHistory))
    }
  }

  return (
    <Root>
      <Side onClick={removeHistoryItem}>
        <LogoLink to="/">
          <TopLogo src={Logo} />
        </LogoLink>

        {showOrganization && (
          <Item>
            <SvgIcon name="organization" reverse />
            {organizationName}
          </Item>
        )}

        {showBackToOrganization && (
          <BackLink to={organizationUrl}>Back to {organizationName}</BackLink>
        )}

        {showBackToBook && (
          <BackLink to={bookUrl}>
            Back to <DisplayInlineEditor color="#fff" content={bookName} />
          </BackLink>
        )}

        {showBackToCollection && (
          <BackLink to={collectionUrl}>
            Back to{' '}
            <DisplayInlineEditor color="#fff" content={collectionName} />
          </BackLink>
        )}
      </Side>

      <Side>
        {canViewOrganizationAccess && (
          <PermissionsGate scopes={['view-organization-access-tab']}>
            <Button
              data-test-id="org-access-btn"
              onClick={onClickOrganizationAccess}
              outlined
            >
              Organization Access
            </Button>
          </PermissionsGate>
        )}

        {userExists && (
          <UserMenuItem
            itemsList={[{ id: 1, title: 'Log out', onClick: onLogoutClick }]}
            size="small"
          >
            {displayName}
            {isSysAdmin && <AdminLabel>NCBI Admin</AdminLabel>}
          </UserMenuItem>
        )}

        {!userExists && (
          <Item>
            <Action to={loginLink}>Login</Action>
          </Item>
        )}
      </Side>
    </Root>
  )
}

AppBar.propTypes = {
  loginLink: PropTypes.string.isRequired,
  onLogoutClick: PropTypes.func.isRequired,

  displayName: PropTypes.string,
  isSysAdmin: PropTypes.bool.isRequired,

  canViewOrganizationAccess: PropTypes.bool.isRequired,
  onClickOrganizationAccess: PropTypes.func.isRequired,

  showOrganization: PropTypes.bool,
  showBackToOrganization: PropTypes.bool,
  showBackToBook: PropTypes.bool,
  organizationName: PropTypes.string,
  organizationUrl: PropTypes.string,
  bookName: PropTypes.string,
  bookUrl: PropTypes.string,
  showBackToCollection: PropTypes.bool,
  collectionUrl: PropTypes.string,
  collectionName: PropTypes.string,
  organizationId: PropTypes.string,
}

AppBar.defaultProps = {
  displayName: null,
  showOrganization: false,
  showBackToOrganization: false,
  showBackToBook: false,
  organizationName: null,
  organizationUrl: null,
  bookName: null,
  bookUrl: null,
  showBackToCollection: false,
  collectionUrl: false,
  collectionName: null,
  organizationId: null,
}

export default AppBar
