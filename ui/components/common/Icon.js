import React from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'

import { Icon } from '@pubsweet/ui'

const ExtendedIcon = props => {
  const { className, children, color, theme, ...rest } = props

  let appliedColor = color

  if (theme[color]) {
    appliedColor = theme[color]
  }

  return (
    <Icon className={className} color={appliedColor} {...rest}>
      {children}
    </Icon>
  )
}

ExtendedIcon.propTypes = {
  color: PropTypes.string,
}

ExtendedIcon.defaultProps = {
  color: null,
}

export default withTheme(ExtendedIcon)
