import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { H5 as UIH5 } from '@pubsweet/ui'
import { grid, th } from '@pubsweet/ui-toolkit'

const Wrapper = styled.div`
  border: 1px solid ${th('colorTextPlaceholder')};
  border-radius: ${th('borderRadius')};
  padding: 0 ${grid(2)} ${grid(2)} ${grid(2)};

  > div:not(:last-child) {
    margin-bottom: ${props => grid(props.gutterSize)};
  }
`

const H5 = styled(UIH5)`
  border-bottom: 1px solid ${th('colorBorder')};
  color: ${th('colorText')};
  margin: ${grid(2)} 0;

  &:first-letter {
    text-transform: capitalize;
  }
`

const FormSection = props => {
  const { className, children, gutterSize, label } = props

  return (
    <Wrapper
      className={className}
      data-test-id={label.toLowerCase().split(' ').join('-')}
      gutterSize={gutterSize}
    >
      <H5>{label}</H5>
      {children}
    </Wrapper>
  )
}

FormSection.propTypes = {
  /** Title of the section */
  label: PropTypes.string.isRequired,
  /** How many gridSize units to use to space out contained elements */
  gutterSize: PropTypes.number,
}

FormSection.defaultProps = {
  gutterSize: 2,
}

export default FormSection
