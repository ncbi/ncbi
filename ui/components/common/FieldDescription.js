import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'

import Icon from './Icon'
import Tooltip from './Tooltip'

const Wrapper = styled.div`
  cursor: pointer;
  display: inline-block;
`

const StyledIcon = styled(Icon)`
  padding: 0;
`

const FieldDescription = props => {
  const { className, content } = props
  const id = uuid()

  const handleClick = e => e.stopPropagation()

  return (
    <Wrapper className={className} onClick={handleClick}>
      <StyledIcon data-event="click focus" data-for={id} data-tip size={2}>
        help_circle
      </StyledIcon>

      <Tooltip id={id} place="right">
        {content}
      </Tooltip>
    </Wrapper>
  )
}

FieldDescription.propTypes = {
  content: PropTypes.string.isRequired,
}

FieldDescription.defaultProps = {}

export default FieldDescription
