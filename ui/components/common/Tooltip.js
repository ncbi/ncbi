/* stylelint-disable declaration-no-important */

import React from 'react'
// import PropTypes from 'prop-types'
import styled from 'styled-components'
import ReactTooltip from 'react-tooltip'

import { th } from '@pubsweet/ui-toolkit'

// Unfortunately, styled css gets overwritten by the lib-provided component
const StyledTooltip = styled(ReactTooltip)`
  background: ${th('colorText')} !important;
  color: ${th('colorTextReverse')} !important;
  font-size: ${th('fontSizeBaseSmall')} !important;
  max-width: 80ch;
`

const Tooltip = props => {
  const { className, children, ...rest } = props

  return (
    <StyledTooltip
      className={className}
      effect="solid"
      globalEventOff="click"
      {...rest}
    >
      {children}
    </StyledTooltip>
  )
}

Tooltip.propTypes = {}

Tooltip.defaultProps = {}

export default Tooltip
