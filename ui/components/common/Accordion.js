import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Accordion as AccordionUI, Icon } from '@pubsweet/ui'
import { th, grid } from '@pubsweet/ui-toolkit'

const Header = styled.div`
  align-items: center;
  color: ${th('colorPrimary')};
  cursor: pointer;
  display: flex;
  font-family: ${th('fontInterace')};
  font-size: ${th('fontSizeBase')};
  padding: ${th('gridUnit')} 0;
`

const HeaderIcon = styled(Icon)`
  transform: ${props =>
    props.isExpanded ? 'rotateZ(0deg)' : 'rotateZ(-90deg)'};
  transition: 0.2s ease transform;
`

const Label = styled.span`
  margin-right: ${grid(1)};
  text-transform: capitalize;
`

const Section = styled.div`
  align-items: center;
  display: flex;
`

/* eslint-disable react/prop-types */
const AccordionHeader = ({
  label,
  headerRightComponent,
  expanded,
  icon,
  toggle,
}) => {
  return (
    <Section>
      <Header onClick={toggle}>
        <HeaderIcon color="#205493" isExpanded={expanded}>
          {icon}
        </HeaderIcon>
        <Label>{label}</Label>
        {headerRightComponent}
      </Header>
    </Section>
  )
}
/* eslint-enable react/prop-types */

const Accordion = ({
  label,
  headerRightComponent,
  startExpanded,
  children,
  icon,
  indexes,
  currentIndex,
  setCurrentIndex,
  setIndexes,
  getTreeList,
  section,
  treeList,
}) => {
  return (
    <AccordionUI
      currentIndex={currentIndex}
      getTreeList={getTreeList}
      header={AccordionHeader}
      headerRightComponent={headerRightComponent}
      icon={icon}
      indexes={indexes}
      label={label}
      section={section}
      setCurrentIndex={setCurrentIndex}
      setIndexes={setIndexes}
      startExpanded={startExpanded}
      treeList={treeList}
    >
      {children}
    </AccordionUI>
  )
}

Accordion.propTypes = {
  /** Component to the right of label */
  headerRightComponent: PropTypes.node,
  /** Icon name (An icon name, from the Feather icon set.) */
  icon: PropTypes.string,
  /** Accordion title */
  label: PropTypes.string.isRequired,
  /** Expands accordion content */
  startExpanded: PropTypes.bool,
  indexes: PropTypes.string,
  treeList: PropTypes.string,
  section: PropTypes.arrayOf(PropTypes.string),
  currentIndex: PropTypes.string,
  setCurrentIndex: PropTypes.string,
  setIndexes: PropTypes.string,
  getTreeList: PropTypes.string,
}

Accordion.defaultProps = {
  headerRightComponent: null,
  icon: 'chevron-down',
  startExpanded: false,
  indexes: null,
  treeList: null,
  section: null,
  currentIndex: null,
  setCurrentIndex: null,
  setIndexes: null,
  getTreeList: null,
}

export default Accordion
