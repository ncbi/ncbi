import styled from 'styled-components'

const RightAlign = styled.div`
  display: flex;
  justify-content: end;
`

export default RightAlign
