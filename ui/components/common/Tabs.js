import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { th, grid } from '@pubsweet/ui-toolkit'
import Tab from './Tab'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

// #region styled-components
const TabsContainer = styled.div`
  background: ${props =>
    props.outlined ? th('colorBackground') : th('colorTextPlaceholder')};
  border-bottom: ${props => (props.outlined ? '2px solid ' : 'none')};
  border-color: ${th('colorText')};
  display: flex;
  padding-right: ${grid(2)};
`

const TabItemsWrapper = styled.div`
  margin-left: ${grid(2)};
`

const RightBox = styled.div`
  display: inline-flex;
  flex-grow: 1;
`

const TabContent = styled.div`
  flex-grow: 1;
  height: calc(100% - 47px);
`

const TabItemContentWrapper = styled.div`
  height: 100%;
  overflow-y: auto;

  /* > div:first-child {
    padding-top: ${grid(1)};
  } */
`
// #endregion

const Tabs = ({
  activeKey,
  className,
  tabItems,
  outlined,
  rightComponent,
  onChangeTab,
}) => {
  const [allTabs, setAllTabs] = useState([])
  const [activeTab, setActiveTab] = useState(activeKey)

  const onClickTabItem = key => {
    setActiveTab(key)
    onChangeTab && onChangeTab(key)
  }

  useEffect(() => {
    setAllTabs(tabItems.filter(z => z !== null))
  }, [tabItems])

  const activeTabItem = allTabs.find(t => t.key === activeTab)

  return (
    <Wrapper className={className}>
      <TabsContainer outlined={outlined}>
        <TabItemsWrapper>
          {allTabs.map(tabItem => (
            <Tab
              isActive={activeTab === tabItem.key}
              key={tabItem.key}
              label={tabItem.label ? tabItem.label : tabItem.key}
              onClick={() => onClickTabItem(tabItem.key)}
              outlined={outlined}
            />
          ))}
        </TabItemsWrapper>
        <RightBox>{rightComponent}</RightBox>
      </TabsContainer>

      <TabContent>
        {activeTabItem && (
          <TabItemContentWrapper>{activeTabItem.content}</TabItemContentWrapper>
        )}
      </TabContent>
    </Wrapper>
  )
}

Tabs.propTypes = {
  /** The key of Active Tab */
  activeKey: PropTypes.string,
  /** Boolean, Tab design is outlined if `outlined=true` */
  outlined: PropTypes.bool,
  /** List of Tab elements */
  tabItems: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
      content: PropTypes.node,
    }),
  ),
  /** An optional component aligned to the right */
  rightComponent: PropTypes.node,
  /** Function to run when the tab changes */
  onChangeTab: PropTypes.func,
}

Tabs.defaultProps = {
  activeKey: '',
  outlined: false,
  tabItems: [],
  rightComponent: null,
  onChangeTab: null,
}

export default Tabs
