import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { th, grid } from '@pubsweet/ui-toolkit'

// #region styled-components
const Wrapper = styled.nav`
  align-items: center;
  background: ${th('colorTextPlaceholder')};
  display: flex;
  min-height: ${grid(6)};
  padding: ${grid(1)} ${grid(2)};
`

const LeftBox = styled.div`
  display: flex;
  flex: 1;
`

// #endregion

const NavBar = ({ children, className, rightComponent }) => (
  <Wrapper className={className}>
    <LeftBox>{children}</LeftBox>
    {rightComponent}
  </Wrapper>
)

NavBar.propTypes = {
  /** Optional component aligned to the right */
  rightComponent: PropTypes.node,
}

NavBar.defaultProps = {
  rightComponent: null,
}

export default NavBar
