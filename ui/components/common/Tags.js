import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import without from 'lodash/without'
import { grid, th } from '@pubsweet/ui-toolkit'
import Tag from './Tag'

import SelectUI from './Select'

// #region styled

const Wrapper = styled.div`
  align-items: center;
  display: inline-flex;
`

const TagsWrapper = styled.div`
  display: inline-block;
  font-size: ${th('fontSizeBaseSmall')};
  margin-right: ${grid(1)};

  > span:not(:last-child) {
    margin-right: ${grid(0.5)};
  }
`

const Select = styled(SelectUI)`
  .select__control {
    background: transparent;
    border: 0;
    padding: 0;
    width: 200px;
  }

  .select__value-container {
    padding: 0;
  }

  .select__placeholder {
    margin: 0;
  }

  .select__indicators {
    display: none;
  }
`

// #endregion styled

const Tags = props => {
  const { addLabel, className, onChange, options, values } = props

  const dropdownOptions = options.filter(
    option => !values.find(v => v.value === option.value),
  )

  const handleAdd = selected => {
    onChange && onChange([...values, selected])
  }

  const handleRemove = deleted => {
    const item = values.find(v => v.value === deleted)
    onChange && onChange(without(values, item))
  }

  return (
    <Wrapper className={className}>
      <TagsWrapper>
        {values.map(selected => {
          const { value, label, color } = selected

          return (
            <Tag
              color={color}
              key={label}
              label={label}
              onClickRemove={() => handleRemove(value)}
            />
          )
        })}
      </TagsWrapper>

      <Select
        isSearchable={false}
        menuPortalTarget={document.body}
        onChange={handleAdd}
        options={dropdownOptions}
        placeholder={addLabel}
        value={null}
      />
    </Wrapper>
  )
}

Tags.propTypes = {
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      color: PropTypes.string,
    }),
  ).isRequired,
  values: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      color: PropTypes.string,
    }),
  ).isRequired,

  addLabel: PropTypes.string,
}

Tags.defaultProps = {
  addLabel: '+ add tag',
}

export default Tags
