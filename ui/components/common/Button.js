import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import { Button as UIButton } from '@pubsweet/ui'
import { th, grid } from '@pubsweet/ui-toolkit'

import DefaultSpinner from './Spinner'
import UIIcon from './Icon'

const Spinner = styled(DefaultSpinner)`
  margin: auto 6px auto 0;

  > div {
    display: inline-flex;
    height: ${grid(3)};
    width: ${grid(3)};

    &:after {
      border-color: ${props => {
        const { outlined, status } = props
        let color = 'colorTextReverse'

        if (outlined) {
          if (status === 'primary') color = 'colorPrimary'
          if (status === 'secondary') color = 'colorSecondary'
          if (status === 'danger') color = 'colorError'
        }

        return css`
          ${th(color)} transparent
        `
      }} !important;
      border-width: 2px;
    }
  }
`

const StyledButton = styled(UIButton)`
  align-items: center;
  background: ${props => {
    const { status, outlined } = props
    if (outlined) return props.background || 'transparent'
    if (status === 'secondary') return th('colorSecondary')
    if (status === 'danger') return th('colorError')
    return th('colorPrimary')
  }};

  border: ${th('borderWidth')} ${th('borderStyle')};

  border-color: ${props => {
    const { status } = props
    if (status === 'secondary') return th('colorSecondary')
    if (status === 'danger') return th('colorError')
    return th('colorPrimary')
  }};

  border-radius: ${th('borderRadius')};

  color: ${props => {
    const { outlined, status } = props

    if (outlined) {
      if (status === 'danger') return th('colorError')
      if (status === 'secondary') return th('colorSecondary')
      return th('colorPrimary')
    }

    return th('colorTextReverse')
  }};

  cursor: pointer;
  display: inline-flex;
  line-height: unset;
  justify-content: space-around;
  margin: ${grid(0.25)} ${grid(0.25)};
  min-width: auto;
  padding: ${grid(0.25)} ${props => (props.separator ? grid(1) : grid(1.5))};
  text-decoration: none;
  text-transform: none;
  white-space: nowrap;

  &:hover,
  &:active,
  &:focus {
    background: ${props => {
      const { status, outlined } = props
      if (outlined) return props.background || 'transparent'
      if (status === 'secondary') return th('colorSecondary')
      if (status === 'danger') return th('colorError')
      return th('colorPrimary')
    }};

    border-color: ${props => {
      const { status } = props
      if (status === 'secondary') return th('colorSecondary')
      if (status === 'danger') return th('colorError')
      return th('colorPrimary')
    }};

    border-radius: 2px;
    border-style: ${th('borderStyle')};
    border-width: ${th('borderWidth')};

    color: ${props => {
      const { outlined, status } = props

      if (outlined) {
        if (status === 'danger') return th('colorError')
        return th('colorPrimary')
      }

      return th('colorTextReverse')
    }};

    opacity: ${props => !props.disabled && 0.9};
  }

  &[disabled] {
    background: ${props => {
      const { outlined } = props

      if (outlined) {
        return props.background || 'transparent'
      }

      return th('colorDisabled')
    }};
    border-color: ${th('colorDisabled')};
    color: ${props => {
      const { outlined } = props

      if (outlined) {
        return th('colorDisabled')
      }

      return th('colorTextReverse')
    }};

    &:focus,
    &:hover,
    &:active {
      background: ${props => {
        const { outlined } = props
        if (outlined) return props.background || 'transparent'
        return th('colorDisabled')
      }};
      cursor: not-allowed;
    }
  }
`

const ChildrenWrapper = styled.div`
  padding: ${grid(0.5)} 0;
`

const VerticalSeparator = styled.div`
  align-self: stretch;
  border-left: ${props =>
    props.hasLine &&
    props.linePosition === 'left' &&
    `2px solid ${props.theme.colorBorder}`};
  border-right: ${props =>
    props.hasLine &&
    props.linePosition === 'right' &&
    `2px solid ${props.theme.colorBorder}`};
  margin: 4px ${props => (props.hasLine ? grid(1) : grid(0.5))};
`

const Icon = styled(UIIcon)`
  padding: 0;
`

const IconWraper = styled.div`
  display: flex;
  align-content: center;
  align-items: center;
  justify-content: center;
  padding: 0 0;
`

const Button = props => {
  const {
    className,
    children,
    disabled,
    icon,
    iconPosition,
    loading,
    onClick,
    outlined,
    status,
    separator,
    ...rest
  } = props

  let buttonIconColor = ''

  // Button Icon Color
  if (outlined) {
    if (disabled) {
      buttonIconColor = 'colorDisabled'
    } else if (status === 'danger') {
      buttonIconColor = 'colorError'
    } else {
      buttonIconColor = 'colorPrimary'
    }
  } else {
    buttonIconColor = '#ffffff'
  }

  // eslint-disable-next-line no-shadow
  const IconComponent = () => {
    let iconComponent

    if (typeof icon === 'string') {
      iconComponent = (
        <Icon
          color={buttonIconColor}
          iconPosition={children != null && iconPosition}
        >
          {icon}
        </Icon>
      )
    } else {
      iconComponent = <IconWraper>{icon}</IconWraper>
    }

    return (
      <>
        {!!children && iconPosition === 'end' && (
          <VerticalSeparator hasLine={separator} linePosition="left" />
        )}

        {iconComponent}

        {!!children && iconPosition === 'start' && (
          <VerticalSeparator hasLine={separator} linePosition="right" />
        )}
      </>
    )
  }

  return (
    <StyledButton
      className={className}
      disabled={disabled}
      onClick={onClick}
      outlined={outlined}
      separator={separator}
      status={status}
      {...rest}
    >
      {loading && <Spinner outlined={outlined} status={status} />}

      {icon && iconPosition === 'start' && (
        <IconComponent hasChildren={!!children} />
      )}

      <ChildrenWrapper>{children}</ChildrenWrapper>

      {icon && iconPosition === 'end' && (
        <IconComponent hasChildren={!!children} />
      )}
    </StyledButton>
  )
}

Button.propTypes = {
  /** Defines button type */
  status: PropTypes.oneOf(['primary', 'secondary', 'danger']),
  /** Makes button disabled */
  disabled: PropTypes.bool,
  /** Adds a vertical separator in the button */
  separator: PropTypes.bool,
  /** Icon name (An icon name, from the Feather icon set.) */
  icon: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  /** Icon Position (Defines the position of the icon (if is is at the start of the button , or at the end)) */
  iconPosition: PropTypes.oneOf(['start', 'end']),
  /** Indicates whether the spinner should be displayed */
  loading: PropTypes.bool,
  /** Makes button a outlined */
  outlined: PropTypes.bool,
}

Button.defaultProps = {
  status: 'secondary',
  disabled: false,
  separator: false,
  icon: null,
  iconPosition: 'start',
  loading: false,
  outlined: false,
}

export default Button
