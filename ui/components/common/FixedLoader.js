import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import PropTypes from 'prop-types'
import Spinner from './Spinner'

const FixedLoaderUi = styled.div`
  align-items: center;
  background: #fff;
  border: 1px solid #0b65cb;
  bottom: 60px;
  color: ${th('colorPrimary')};
  display: flex;
  /* stylelint-disable-next-line declaration-no-important */
  height: 54px !important;
  padding: 10px;
  position: fixed;
  right: 4px;

  > svg,
  > span {
    color: ${th('colorPrimary')};
    margin-right: 5px;
  }
`

const FixedLoader = ({ title, hideSpiner, children }) => {
  return (
    <FixedLoaderUi>
      {!hideSpiner && <Spinner />}
      {title}
      {children}
    </FixedLoaderUi>
  )
}

FixedLoader.propTypes = {
  title: PropTypes.string,
  hideSpiner: PropTypes.bool,
  children: PropTypes.node,
}

FixedLoader.defaultProps = {
  title: 'Processing...',
  hideSpiner: false,
  children: null,
}

export default FixedLoader
