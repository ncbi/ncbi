import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import Icon from './Icon'

const StyledTag = styled.span`
  align-items: center;
  background-color: ${props => props.color || 'grey'};
  border-radius: 6px;
  color: ${th('colorTextReverse')};
  display: inline-flex;

  ${props =>
    props.size === 'small' &&
    `
    font-size: ${props.theme.fontSizeBaseVerySmall};
    line-height: 1;
    padding: 0 6px;
  `}

  padding: 2px 8px;

  > span:last-child {
    padding-right: 0;
  }
`

const Tag = props => {
  const { className, color, label, onClickRemove, size } = props

  return (
    <StyledTag className={className} color={color} size={size}>
      {' '}
      <span>{label}</span>
      {onClickRemove && (
        <Icon color="white" onClick={onClickRemove} size={2}>
          x
        </Icon>
      )}
    </StyledTag>
  )
}

Tag.propTypes = {
  color: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  onClickRemove: PropTypes.func,
  size: PropTypes.string,
}

Tag.defaultProps = {
  color: 'grey',
  onClickRemove: null,
  size: 'middle',
}

export default Tag
