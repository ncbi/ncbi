import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { rotate360, th, grid } from '@pubsweet/ui-toolkit'

// Courtesy of loading.io/css
const SpinnerAnimation = styled.div`
  align-items: center;
  display: flex;
  height: ${props => (props.pageLoader ? grid(7) : grid(4))};
  justify-content: center;
  width: ${props => (props.pageLoader ? grid(7) : grid(4))};

  &:after {
    animation: ${rotate360} 1s linear infinite;
    border: ${props => (props.pageLoader ? '5px' : '3px')} solid
      ${th('colorSecondary')};
    border-color: ${th('colorSecondary')} transparent ${th('colorSecondary')}
      transparent;
    border-radius: 50%;
    content: ' ';
    display: block;
    height: ${props => (props.pageLoader ? grid(5) : grid(2))};
    margin: 1px;
    width: ${props => (props.pageLoader ? grid(5) : grid(2))};
  }
`

const LoadingPage = styled.div`
  align-items: center;
  color: ${th('colorSecondary')};
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: center;
  margin-top: ${props => (props.hasLabel ? '-5%' : '0')};

  > div:first-child {
    margin-bottom: ${grid(2)};
  }
`

const Wrapper = styled.div`
  align-items: center;
  color: ${th('colorSecondary')};
  display: inline-flex;
  justify-content: center;
`

const Spinner = ({ className, pageLoader, label }) =>
  pageLoader ? (
    <LoadingPage className={className} hasLabel={!!label}>
      <div>{label}</div>
      <SpinnerAnimation pageLoader />
    </LoadingPage>
  ) : (
    <Wrapper className={className}>
      <SpinnerAnimation />
      {label}
    </Wrapper>
  )

Spinner.propTypes = {
  /** Used to define a page blocking spinner */
  pageLoader: PropTypes.bool,
  /** Optional information text */
  label: PropTypes.string,
}

Spinner.defaultProps = {
  pageLoader: false,
  label: null,
}

export default Spinner
