import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th, grid } from '@pubsweet/ui-toolkit'

const Wrapper = styled.div`
  background-color: ${th('colorBackgroundHue')};
  border-bottom: 6px solid;
  border-bottom-color: ${props => {
    const { format } = props
    if (format === 'word') return th('colorPrimary')
    if (format === 'pdf') return th('colorWarning')
    if (format === 'xml') return th('colorSuccess')
    return th('colorBorder')
  }};
  border-radius: 5px;
  color: ${th('colorText')};
  display: inline-block;
  font-size: 10pt;
  font-weight: 600;
  text-align: center;
  text-transform: uppercase;
  width: ${grid(5)};
`

/**
 * Small component that just displays the workflow type with a corresponding color
 */

const DocumentIcon = ({ format }) => (
  <Wrapper data-test-id={format} format={format}>
    {format}
  </Wrapper>
)

DocumentIcon.propTypes = {
  format: PropTypes.oneOf(['word', 'pdf', 'xml']).isRequired,
}

DocumentIcon.defaultProps = {}

export default DocumentIcon
