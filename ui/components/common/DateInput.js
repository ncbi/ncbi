import React, { useEffect, useReducer } from 'react'
import styled, { css } from 'styled-components'
import moment from 'moment'
import PropTypes from 'prop-types'

import { TextField } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import Icon from './Icon'
import Label from './Label'

// #region Styled components
const Main = styled.div`
  align-content: center;
  align-items: center;
  display: flex;
  line-height: 16px;
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const Separator = styled.div`
  align-self: center;
  margin-left: ${th('gridUnit')};
  margin-right: ${th('gridUnit')};
`

const TextFieldUI = styled(TextField)`
  border-width: 0px;
  height: 34px;
  line-height: 16px;
  margin-bottom: 0;
  max-width: 64px;
  width: ${props => (props.width ? props.width : '100%')};

  [type='text'] {
    border-color: ${props =>
      props.validationStatus === 'error'
        ? th('colorError')
        : th('colorBorder')};
    border-radius: ${th('borderRadius')};
    border-style: solid;
    border-width: 1px;
    color: ${th('colorText')};
  }

  input {
    ${props =>
      props.disabled &&
      css`
        background-color: ${th('colorBackgroundHue')};
        cursor: not-allowed;
      `}
  }
`

const Input = styled.input`
  display: none;
`

const UIcon = styled(Icon)`
  cursor: pointer;
`

const get2DigitNumber = number => {
  if (number) {
    return Number(number) < 10 ? `0${Number(number)}` : number
  }

  return number
}

const reducer = (state, action) => {
  let newState

  switch (action.type) {
    case 'setDay':
      {
        let inputValue = action.payload.value
        if (Number.isNaN(inputValue)) inputValue = ''
        if (inputValue > 31 && inputValue !== '') inputValue = '31'
        if (inputValue < 0 && inputValue !== '') inputValue = '01'
        newState = { ...state, day: inputValue }
      }

      break

    case 'setMonth': {
      let inputValue = action.payload.value
      if (Number.isNaN(inputValue)) inputValue = ''
      if (inputValue > 12 && inputValue !== '') inputValue = '12'
      if (inputValue < 0 && inputValue !== '') inputValue = '01'
      newState = { ...state, month: inputValue }
      break
    }

    case 'setYear': {
      let inputValue = action.payload.value
      if (Number.isNaN(inputValue) || inputValue < 0) inputValue = ''

      newState = {
        day: state.day || '',
        month: state.month || '',
        year: inputValue,
      }

      break
    }

    case 'setStartMonth': {
      let inputValue = action.payload.value
      if (Number.isNaN(inputValue)) inputValue = ''
      if (inputValue > 12 && inputValue !== '') inputValue = '12'
      if (inputValue < 0 && inputValue !== '') inputValue = '01'

      newState = {
        startMonth: inputValue,
        startYear: state.startYear,
        endMonth: state.endMonth,
        endYear: state.endYear,
      }

      break
    }

    case 'setEndMonth': {
      let inputValue = action.payload.value
      if (Number.isNaN(inputValue)) inputValue = ''
      if (inputValue > 12 && inputValue !== '') inputValue = '12'
      if (inputValue < 0 && inputValue !== '') inputValue = '01'

      newState = {
        startMonth: state.startMonth,
        startYear: state.startYear,
        endMonth: inputValue,
        endYear: state.endYear,
      }

      break
    }

    case 'setStartYear': {
      let inputValue = action.payload.value
      if (Number.isNaN(inputValue) || inputValue < 0) inputValue = ''

      // if (inputValue < 1900 && inputValue !== '') inputValue = 1900
      newState = {
        startYear: inputValue,
        endYear: state.endYear,
        startMonth: state.startMonth,
        endMonth: state.endMonth,
      }

      break
    }

    case 'setEndYear': {
      let inputValue = action.payload.value
      if (Number.isNaN(inputValue) || inputValue < 0) inputValue = ''

      // if (inputValue < 1900 && inputValue !== '') inputValue = 1900
      newState = {
        startYear: state.startYear,
        endYear: inputValue,
        startMonth: state.startMonth,
        endMonth: state.endMonth,
      }

      break
    }

    case 'onBlurStartMonth': {
      const inputValue = action.payload.value
      newState = { ...state, startMonth: get2DigitNumber(inputValue) }
      break
    }

    case 'onBlurEndMonth': {
      const inputValue = action.payload.value
      newState = { ...state, endMonth: get2DigitNumber(inputValue) }
      break
    }

    case 'clearDate': {
      const { isDateRange } = action.payload

      newState = !isDateRange
        ? { day: '', month: '', year: '' }
        : { startMonth: '', startYear: '', endMonth: '', endYear: '' }

      break
    }

    case 'setTodayDate': {
      const thisDay = moment().format('DD')
      const thisMonth = moment().format('MM')
      const thisyear = moment().format('YYYY')
      const { isDateRange } = action.payload

      newState = !isDateRange
        ? { day: thisDay, month: thisMonth, year: thisyear }
        : {
            startMonth: thisMonth,
            startYear: thisyear,
            endMonth: thisMonth,
            endYear: thisyear,
          }

      break
    }

    case 'onBlurInputDay': {
      const inputValue = action.payload.value
      newState = { ...state, day: get2DigitNumber(inputValue) }
      break
    }

    case 'onBlurInputMonth': {
      const inputValue = action.payload.value
      newState = { ...state, month: get2DigitNumber(inputValue) }
      break
    }

    case 'onBlurInputYear': {
      let inputValue = action.payload.value

      if (inputValue) {
        if (inputValue < 1900 && inputValue !== '') inputValue = 1900
        newState = { ...state, year: get2DigitNumber(inputValue) }
      } else {
        newState = state
      }

      break
    }

    default:
      throw new Error(`none of actions above${action.type}`)
  }

  return newState
}

const DateInput = ({
  disabled,
  onBlur,
  isDateRange,
  label,
  name,
  onChange,
  required,
  separator,
  validationStatus,
  value,
}) => {
  const [state, dispatch] = useReducer(reducer, value)

  useEffect(() => {
    onChange(state)
  }, [state])

  const handleDayChange = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'setDay',
    })

  const handleDayBlur = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'onBlurInputDay',
    })

  const handleMonthChange = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'setMonth',
    })

  const handleMonthBlur = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'onBlurInputMonth',
    })

  const handleYearChange = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'setYear',
    })

  const handleYearBlur = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'onBlurInputYear',
    })

  const handleStartYearChange = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'setStartYear',
    })

  const handleStartMonthChange = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'setStartMonth',
    })

  const handleEndMonthChange = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'setEndMonth',
    })

  const handleEndYearChange = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'setEndYear',
    })

  const handleSetToday = () =>
    dispatch({
      type: 'setTodayDate',
      payload: {
        isDateRange,
      },
    })

  const handleStartMonthBlur = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'onBlurStartMonth',
    })

  const handleEndMonthBlur = event =>
    dispatch({
      payload: {
        value: event.target.value,
      },
      type: 'onBlurEndMonth',
    })

  const handleOnKeyDownToday = event => {
    if (event.key === 'Enter') {
      handleSetToday()
    }
  }

  const handleClearDate = () =>
    dispatch({
      type: 'clearDate',
      payload: {
        isDateRange,
      },
    })

  const handleOnKeyDownClear = event => {
    if (event.key === 'Enter') {
      handleClearDate()
    }
  }

  return (
    <Wrapper onBlur={onBlur}>
      {label && <Label name={name} required={required} value={label} />}

      <Main>
        <Input name={name} type="checkbox" value={state} />
        {!isDateRange && (
          <>
            <TextFieldUI
              disabled={disabled}
              onBlur={handleMonthBlur}
              onChange={handleMonthChange}
              placeholder="MM"
              validationStatus={validationStatus}
              value={state?.month || ''}
            />

            <Separator>{separator}</Separator>

            <TextFieldUI
              disabled={disabled}
              onBlur={handleDayBlur}
              onChange={handleDayChange}
              placeholder="DD"
              validationStatus={validationStatus}
              value={state?.day || ''}
            />

            <Separator>{separator}</Separator>

            <TextFieldUI
              disabled={disabled}
              onBlur={handleYearBlur}
              onChange={handleYearChange}
              placeholder="YYYY"
              validationStatus={validationStatus}
              value={state?.year || ''}
            />
          </>
        )}
        {isDateRange && (
          <>
            <TextFieldUI
              data-test-id="start-month"
              disabled={disabled}
              onBlur={handleStartMonthBlur}
              onChange={handleStartMonthChange}
              placeholder="MM"
              validationStatus={validationStatus}
              value={state?.startMonth || ''}
            />{' '}
            <Separator>{separator}</Separator>
            <TextFieldUI
              data-test-id="start-year"
              disabled={disabled}
              onChange={handleStartYearChange}
              placeholder="YYYY"
              validationStatus={validationStatus}
              value={state?.startYear || ''}
            />
            <Separator>-</Separator>
            <TextFieldUI
              data-test-id="end-month"
              disabled={disabled}
              onBlur={handleEndMonthBlur}
              onChange={handleEndMonthChange}
              placeholder="MM"
              validationStatus={validationStatus}
              value={state?.endMonth || ''}
            />{' '}
            <Separator>{separator}</Separator>
            <TextFieldUI
              data-test-id="end-year"
              disabled={disabled}
              onChange={handleEndYearChange}
              placeholder="YYYY"
              validationStatus={validationStatus}
              value={state?.endYear || ''}
            />
          </>
        )}

        {!disabled && (
          <>
            <UIcon
              color="#8a8383"
              data-test-id="setdate-icon"
              onClick={handleSetToday}
              onKeyDown={handleOnKeyDownToday}
              tabIndex={0}
              title="Today Date"
            >
              crosshair
            </UIcon>
            <UIcon
              color="red"
              onClick={handleClearDate}
              onKeyDown={handleOnKeyDownClear}
              tabIndex={0}
              title="Clear Date"
            >
              x
            </UIcon>
          </>
        )}
      </Main>
    </Wrapper>
  )
}

DateInput.propTypes = {
  /** Boolean, determines if Date is  year range or date (day-month-year) */
  isDateRange: PropTypes.bool,
  /** Optional separator for in between date fields */
  separator: PropTypes.oneOf(['/', '-', '.']),
  /** Date disabled state */
  disabled: PropTypes.bool,
  /** Label of date input */
  label: PropTypes.string,
  /** value can be only object of two types */
  value: PropTypes.oneOfType([
    PropTypes.shape({
      day: PropTypes.string,
      month: PropTypes.string,
      year: PropTypes.string,
    }),
    PropTypes.shape({
      startYear: PropTypes.string,
      endYear: PropTypes.string,
    }),
  ]),
  /**  input name */
  name: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  required: PropTypes.bool,
  validationStatus: PropTypes.oneOf(['error', 'default']),
}

DateInput.defaultProps = {
  isDateRange: false,
  separator: '/',
  disabled: false,
  label: null,
  value: { day: '', month: '', year: '' },
  name: null,
  onBlur: () => {},
  onChange: () => {},
  required: false,
  validationStatus: 'default',
}

export default DateInput
