import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th, grid } from '@pubsweet/ui-toolkit'

const Wrapper = styled.div`
  background: ${th('colorBackground')};
  border-top: 2px solid ${th('colorPrimary')};
  display: flex;
  padding: ${grid(1)};
`

const Box = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
`

const InnerLeft = styled.div`
  margin-right: auto;
`

const InnerRight = styled.div`
  margin-left: auto;
`

/**
 * A component that can acts as footer action bar for the  bulk actions.
 * Often useful in dashboards, but not limited to them.
 */
const ActionBar = ({
  className,
  leftComponent,
  middleComponent,
  rightComponent,
}) => {
  return (
    <Wrapper className={className}>
      <Box>
        <InnerLeft>{leftComponent}</InnerLeft>
      </Box>
      <Box>{middleComponent}</Box>
      <Box>
        <InnerRight>{rightComponent}</InnerRight>
      </Box>
    </Wrapper>
  )
}

ActionBar.propTypes = {
  /** The component to be shown left */
  leftComponent: PropTypes.node,
  /** The component to be shown in the middle */
  middleComponent: PropTypes.node,
  /** The component to be shown right */
  rightComponent: PropTypes.node,
}

ActionBar.defaultProps = {
  leftComponent: null,
  middleComponent: null,
  rightComponent: null,
}

export default ActionBar
