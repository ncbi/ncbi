import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th, grid } from '@pubsweet/ui-toolkit'
import Icon from './Icon'

const CheckBox = styled.span`
  border: 1px solid
    ${props => (props.disabled ? th('colorDisabled') : th('colorText'))};
  border-radius: 2px;
  color: ${props => (props.disabled ? th('colorDisabled') : th('colorText'))};
  content: '';
  height: 20px;
  margin-right: ${grid(0.5)};
  width: 20px;
`

const Label = styled.div`
  align-items: center;
  color: ${props => (props.disabled ? th('colorDisabled') : th('colorText'))};
  display: inline-flex;

  &:hover {
    cursor: ${props => (props.disabled ? 'not-allowed' : 'pointer')};

    > span {
      color: ${props =>
        props.disabled ? th('colorDisabled') : th('colorText')};
    }

    > span:first-child {
      border-color: ${props =>
        props.disabled ? th('colorDisabled') : th('colorPrimary')};
      box-shadow: ${props => (props.disabled ? 'none' : '0 0 1px #205493')};
    }
  }
`

const StyledIcon = styled(Icon)`
  padding: 2px;
`

const Input = styled.input`
  display: none;
`

/**
 * A component that can acts as switch input  .
 * Often useful in forms, but not limited to them.
 */
const Checkbox = ({
  checked,
  className,
  'data-test-id': testId,
  disabled,
  label,
  name,
  onClick,
}) => (
  <Label
    className={className}
    data-test-id={testId}
    disabled={disabled}
    onClick={disabled ? null : onClick}
  >
    <CheckBox checked={checked} disabled={disabled} value={checked}>
      {checked && (
        <StyledIcon color={disabled ? 'colorDisabled' : 'colorText'} size={2}>
          check
        </StyledIcon>
      )}
      <Input
        checked={checked}
        disabled={disabled}
        name={name}
        onChange={() => null}
        type="checkbox"
      />
    </CheckBox>
    <span>{label}</span>
  </Label>
)

Checkbox.propTypes = {
  /** Boolean value defining checked state */
  checked: PropTypes.bool,
  /** True if checkbox is disabled */
  disabled: PropTypes.bool,
  /** Checkbox label */
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  /** Input name */
  name: PropTypes.string,
}

Checkbox.defaultProps = {
  checked: false,
  disabled: false,
  label: null,
  name: null,
}

export default Checkbox
