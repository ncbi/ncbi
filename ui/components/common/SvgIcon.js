import React from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components'

import { grid } from '@pubsweet/ui-toolkit'

/* HELPERS */

/**
 * Base SVG component to extend in order to make icons
 * Values here are just the default values
 * For an explanation of vertical-align, see https://stackoverflow.com/a/24626986
 */
const Svg = styled.svg.attrs(() => ({
  version: '1.1',
  xmlns: 'http://www.w3.org/2000/svg',
  xmlnsXlink: 'http://www.w3.org/1999/xlink',
}))`
  height: ${grid(3)};
  vertical-align: top;
  width: ${grid(3)};
`

/* END HELPERS */

const Book = props => {
  const { className, theme, testId } = props

  return (
    <Svg
      className={className}
      data-test-id={testId}
      height="51"
      viewBox="0 0 54 51"
      width="54"
    >
      <path
        clipRule="evenodd"
        d="M2 0C0.89543 0 0 0.89543 0 2V49C0 50.1046 0.89543 51 2 51H52C53.1046 51 54 50.1046 54 49V2C54 0.89543 53.1046 0 52 0H2ZM4 47V4H25L25 47H4ZM29 47H50V4H29L29 47Z"
        fill={theme.colorPrimary}
        // fill="#205493"
        fillRule="evenodd"
      />
    </Svg>
  )
}

Book.propTypes = {
  testId: PropTypes.string,
}

Book.defaultProps = { testId: 'Book' }

const BookWithChapters = props => {
  const { className, theme, testId } = props

  return (
    <Svg
      className={className}
      data-test-id={testId}
      height="71"
      viewBox="0 0 54 71"
      width="54"
    >
      <path
        clipRule="evenodd"
        d="M2.97237e-06 2C2.97237e-06 0.89543 0.895433 0 2 0H52C53.1046 0 54 0.89543 54 2V29V38V42V47L54 51V60V69C54 70.1046 53.1046 71 52 71L2 71C0.89543 71 -1.49236e-07 70.1046 0 69L2.97237e-06 60.0022L0 60L2.97237e-06 51.0022L0 51L2.97237e-06 47V42V38V29V2ZM50 4V29V38V40H29V38V28L29 4H50ZM25 4L25 28V38V40H4V38V29V4H25ZM29 44H50V47L50 49L29 49V48L29 47V44ZM4 44H25V47L25 48V49L4 49L4 47V44ZM29 53V58L50 58V53L29 53ZM25 58V53L4 53L4 58L25 58ZM25 62V67L4 67L4 62L25 62ZM29 67V62L50 62V67L29 67Z"
        fill={theme.colorWarning}
        // fill="#FDB81E"
        fillRule="evenodd"
      />
    </Svg>
  )
}

BookWithChapters.propTypes = {
  testId: PropTypes.string,
}

BookWithChapters.defaultProps = { testId: 'BookWithChapters' }

const Collection = props => {
  const { className, theme, testId } = props

  return (
    <Svg
      className={className}
      data-test-id={testId}
      height="71"
      viewBox="0 0 54 71"
      width="54"
    >
      <path
        clipRule="evenodd"
        d="M0 0V9C0 10.1046 0.89543 11 2 11H52C53.1046 11 54 10.1046 54 9V0H50V7H29V0H25V7H4V0H0ZM54 63V71H50V65H29V71H25V65H4L4 71H0V63C0 61.8954 0.895433 61 2 61L52 61C53.1046 61 54 61.8954 54 63ZM0 17C0 15.8954 0.89543 15 2 15H52C53.1046 15 54 15.8954 54 17V55C54 56.1046 53.1046 57 52 57H2C0.89543 57 0 56.1046 0 55V17ZM4 19V53H25V19H4ZM29 19V53H50V19H29Z"
        fill={theme.colorSuccess}
        // fill="#2E8540"
        fillRule="evenodd"
      />
    </Svg>
  )
}

Collection.propTypes = {
  testId: PropTypes.string,
}

Collection.defaultProps = { testId: 'collection' }

const Organization = props => {
  const { className, theme, reverse, testId } = props

  return (
    <Svg
      className={className}
      data-test-id={testId}
      fill={reverse ? theme.colorTextReverse : theme.colorPrimary}
      viewBox="0 0 24 24"
    >
      <path d="M0 0h24v24H0V0z" fill="none" />
      <path d="M12 7V3H2v18h20V7H12zM6 19H4v-2h2v2zm0-4H4v-2h2v2zm0-4H4V9h2v2zm0-4H4V5h2v2zm4 12H8v-2h2v2zm0-4H8v-2h2v2zm0-4H8V9h2v2zm0-4H8V5h2v2zm10 12h-8v-2h2v-2h-2v-2h2v-2h-2V9h8v10zm-2-8h-2v2h2v-2zm0 4h-2v2h2v-2z" />
    </Svg>
  )
}

Organization.propTypes = {
  reverse: PropTypes.bool,
  testId: PropTypes.string,
}

Organization.defaultProps = { reverse: false, testId: 'organization' }

export const MoveComponents = props => {
  const { className, testId } = props

  return (
    <Svg
      className={className}
      data-test-id={testId}
      fill="none"
      viewBox="0 0 43 43"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M33.7418 3.72266L23.9102 13.5543L26.8637 16.5077L31.6531 11.7183V23.3859H35.8305V11.7183L40.6199 16.5077L43.5734 13.5543L33.7418 3.72266Z"
        fill="#205493"
      />
      <path
        d="M35.8303 25.4746H31.6528V29.6521H35.8303V25.4746Z"
        fill="#205493"
      />
      <path
        d="M35.8303 31.7412H31.6528V35.9187H35.8303V31.7412Z"
        fill="#205493"
      />
      <path
        d="M19.1207 23.3857H0.322266V42.1842H19.1207V23.3857Z"
        fill="#205493"
      />
      <path
        d="M0.322266 19.2086H19.1207V0.410156H0.322266V19.2086ZM4.49974 4.58763H14.9434V15.0313H4.49974V4.58763Z"
        fill="#205493"
      />
    </Svg>
  )
}

MoveComponents.propTypes = {
  testId: PropTypes.string,
}

MoveComponents.defaultProps = { testId: 'move' }

export const RepeatComponents = props => {
  const { className, testId } = props

  return (
    <Svg
      className={className}
      data-test-id={testId}
      fill="none"
      viewBox="0 0 43 43"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M33.7418 3.72266L23.9102 13.5543L26.8637 16.5077L31.6531 11.7183V23.3859H35.8305V11.7183L40.6199 16.5077L43.5734 13.5543L33.7418 3.72266Z"
        fill="#205493"
      />
      <path
        d="M35.8303 25.4746H31.6528V29.6521H35.8303V25.4746Z"
        fill="#205493"
      />
      <path
        d="M35.8303 31.7412H31.6528V35.9187H35.8303V31.7412Z"
        fill="#205493"
      />
      <path
        d="M19.1207 23.3857H0.322266V42.1842H19.1207V23.3857Z"
        fill="#205493"
      />
      <path
        d="M0.322266 19.2086H19.1207V0.410156H0.322266V19.2086ZM4.49974 4.58763H14.49974V4.58763Z"
        fill="#205493"
      />
    </Svg>
  )
}

RepeatComponents.propTypes = {
  testId: PropTypes.string,
}

RepeatComponents.defaultProps = { testId: 'repeat' }

/* EXPORTED COMPONENT */

const mapper = {
  book: Book,
  bookWithChapters: BookWithChapters,
  collection: Collection,
  organization: Organization,
}

/** All different SVG icons used in NCBI */
const SvgIcon = ({ className, name, theme, ...rest }) => {
  const Comp = mapper[name]
  return <Comp className={className} theme={theme} {...rest} testId={name} />
}

SvgIcon.propTypes = {
  /** Name of the icon you want rendered */
  name: PropTypes.oneOf([
    'book',
    'bookWithChapters',
    'collection',
    'organization',
  ]).isRequired,
}

export default withTheme(SvgIcon)
