import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Icon, Link } from '@pubsweet/ui'
import { grid, th } from '@pubsweet/ui-toolkit'

const StyledLink = styled(Link)`
  align-items: center;
  border: 1px solid transparent;
  border-radius: ${th('borderRadius')};
  color: white;
  display: flex;
  padding: 0 ${grid(1)} 0 0;
  text-decoration: none;
  transition: border 0.1s ease-in;

  svg {
    stroke: ${th('colorTextReverse')};
  }

  &:hover {
    border: 1px solid white;
  }
`

const BackLink = props => {
  const { children, to } = props

  return (
    <StyledLink data-test-id="back-link" to={to}>
      <Icon>arrow-left</Icon>
      <span>{children}</span>
    </StyledLink>
  )
}

BackLink.propTypes = {
  to: PropTypes.string.isRequired,
}

export default BackLink
