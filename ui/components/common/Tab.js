import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { th, grid } from '@pubsweet/ui-toolkit'

const Item = styled.div`
  align-items: center;
  background: ${props =>
    props.isActive ? th('colorBackground') : 'transparent'};
  border: ${props => (props.outlined && props.isActive ? '2px solid' : 'none')};
  border-color: ${th('colorText')};
  border-radius: ${props => (props.isActive ? '4px 4px 0 0 ' : '0')};
  cursor: pointer;
  display: inline-flex;
  margin: 0;
  margin-bottom: -2px;
  padding: ${grid(1)} ${grid(2)};

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${props =>
    props.outlined &&
    props.isActive &&
    css`
      border-bottom: 2px solid ${th('colorBackground')};
    `}

  > button {
    margin: 0 ${grid(1)};
  }
`

const Tab = ({ isActive, label, onClick, outlined, ...props }) => {
  return (
    <Item
      data-test-id={`${
        typeof label === 'string' ? label.toLowerCase().replace(' ', '-') : ''
      }-tab`}
      isActive={isActive}
      onClick={onClick}
      outlined={outlined}
    >
      {label}
    </Item>
  )
}

Tab.propTypes = {
  /** Boolean, defines if the tab is Active */
  isActive: PropTypes.bool,
  /** Tab label */
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  /** Defines if the tab is Outlined */
  outlined: PropTypes.bool,
}

Tab.defaultProps = {
  isActive: false,
  outlined: false,
}

export default Tab
