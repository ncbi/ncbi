import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import { th, grid, lighten } from '@pubsweet/ui-toolkit'
import UIFieldDescription from './FieldDescription'
import Icon from './Icon'
import Note from './Note'

// #region styled
const spacing = css`
  > * {
    margin-left: ${grid(0.5)};
    margin-right: ${grid(0.5)};

    &:first-child {
      margin-left: 0;
    }

    &:last-child {
      margin-right: 0;
    }
  }
`

const Wrapper = styled.div``

const ToggleWrapper = styled.div`
  align-items: center;
  border-bottom: 1px solid transparent;
  cursor: ${props => (props.disabled ? 'not-allowed' : 'pointer')};
  display: inline-flex;
  height: 36px;
  justify-content: space-between;
  padding-right: 2px;
  transition: border-bottom 0.1s ease-in-out;
  width: 100%;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${spacing};

  &:hover {
    border-bottom: 1px solid
      ${props =>
        (props.disabled && props.theme.colorTextPlaceholder) ||
        props.theme.colorBorder};
  }
`

const SideWrapper = styled.div`
  align-items: center;
  display: inline-flex;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${spacing};
`

const ToggleUI = styled.div`
  background: ${props =>
    props.disabled ? th('colorTextPlaceholder') : th('colorDisabled')};
  border-radius: 10px;
  height: 6px;
  min-width: ${grid(5)};
  position: relative;
  transition: all 0.2s;
  width: ${grid(5)};
`

const ToggleButton = styled.div`
  align-items: center;
  background: ${props => {
    const { checked, disabled } = props
    let color

    if (!checked && !disabled) color = '#D5D5D5'
    if (checked && !disabled) color = th('colorPrimary')
    if (!checked && disabled) color = th('colorDisabled')
    if (checked && disabled) color = lighten('colorSecondary', 30)

    return color
  }};
  border-radius: 50%;
  display: inline-flex;
  height: ${grid(3)};
  justify-content: center;
  left: ${props => (props.checked ? '16px' : '0')};
  position: absolute;
  top: -10px;
  transition: all 0.2s ease-in-out;
  width: ${grid(3)};
`

const Label = styled.div`
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  line-height: ${grid(2)};
`

const Input = styled.input`
  display: none;
`

const StyledIcon = styled(Icon)`
  padding: 0;

  > svg {
    transition: stroke 0.2s;
  }
`

const LockIcon = styled(Icon)`
  cursor: pointer;
`

const FieldDescription = styled(UIFieldDescription)`
  margin-top: 2px;
`
// #endregion styled

/**
 * A component that can acts as switch input  .
 * Often useful in forms, but not limited to them.
 */
const Toggle = ({
  checked,
  className,
  description,
  disabled,
  label,
  labelChecked,
  labelPosition,
  locked,
  lockPosition,
  name,
  note,
  onBlur,
  onClick,
  'data-test-id': testId,
}) => {
  const [isLocked, setLocked] = useState(locked)

  let lock

  if (!disabled && locked !== null) {
    lock = (
      <LockIcon
        color={isLocked ? 'black' : '#3d4446'}
        data-test-id={testId}
        onClick={e => {
          e.stopPropagation()
          setLocked(!isLocked)
        }}
        size={2}
      >
        {isLocked ? 'lock' : 'unlock'}
      </LockIcon>
    )
  }

  const LabelPart = (
    <SideWrapper>
      <Label>{labelChecked && checked ? labelChecked : label}</Label>
      {description && <FieldDescription content={description} />}
    </SideWrapper>
  )

  return (
    <Wrapper
      className={className}
      disabled={disabled || isLocked}
      onBlur={onBlur}
      onClick={() => !isLocked && !disabled && onClick(!checked)}
    >
      <div>{note && <Note compact content={note} />}</div>

      <ToggleWrapper disabled={disabled || isLocked}>
        {labelPosition === 'left' && LabelPart}

        <SideWrapper>
          {lockPosition === 'left' && lock}

          <ToggleUI checked={checked} disabled={disabled || isLocked}>
            <Input name={name} type="checkbox" value={checked} />
            <ToggleButton checked={checked} disabled={disabled || isLocked}>
              {checked ? (
                <StyledIcon color="colorBackground" size={3}>
                  check
                </StyledIcon>
              ) : (
                <StyledIcon
                  color={
                    disabled || isLocked ? 'colorBackground' : 'colorPrimary'
                  }
                  size={3}
                >
                  x
                </StyledIcon>
              )}
            </ToggleButton>
          </ToggleUI>

          {lockPosition === 'right' && lock}
        </SideWrapper>

        {labelPosition === 'right' && LabelPart}
      </ToggleWrapper>
    </Wrapper>
  )
}

Toggle.propTypes = {
  /** Boolean, determines if Toggle is checked */
  checked: PropTypes.bool,
  /** Optional tooltip with extra info about field */
  description: PropTypes.string,
  /** Toggle disabled state */
  disabled: PropTypes.bool,
  /** Boolean, determines if Toggle is locked */
  locked: PropTypes.bool,
  /** Label if Toggle is unchecked */
  label: PropTypes.string,
  /** Label if Toggle is checked */
  labelChecked: PropTypes.string,
  /** Position of the label */
  labelPosition: PropTypes.oneOf(['left', 'right']),
  /** Show lock to the left or right of the toggle button */
  lockPosition: PropTypes.oneOf(['left', 'right']),
  /** Toggle input name */
  name: PropTypes.string,
  /** Display a note about the toggle above it */
  note: PropTypes.string,
}

Toggle.defaultProps = {
  checked: false,
  description: null,
  disabled: false,
  locked: null,
  label: null,
  labelChecked: null,
  labelPosition: 'left',
  lockPosition: 'right',
  name: null,
  note: null,
}

export default Toggle
