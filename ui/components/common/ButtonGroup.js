import React from 'react'
import styled from 'styled-components'
import { grid } from '@pubsweet/ui-toolkit'

const Wrapper = styled.div`
  display: inline-flex;

  > button {
    margin: 0 ${grid(0.25)};
  }

  > button:first-child {
    margin-left: 0;
  }

  > button:last-child {
    margin-right: 0;
  }
`

/** A container of multiple buttons that will consistently provide space between them */
const ButtonGroup = ({ children }) => {
  return <Wrapper>{children}</Wrapper>
}

export default ButtonGroup
