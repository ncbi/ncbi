import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import ReactSelect, { components } from 'react-select'
import AsyncSelect from 'react-select/async'
import debounce from 'debounce-promise'

import { grid, th } from '@pubsweet/ui-toolkit'

import DisplayInlineEditor from '../wax/DisplayInlneEditor'
import Label from './Label'

// #region styles
const Wrapper = styled.div``

const SelectWrapper = styled.div`
  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
    `}
`

const customStyles = {
  control: (provided, state) => ({
    ...provided,
    backgroundColor: `${state.selectProps.isDisabled ? '#f1f1f1' : 'white'}`,
    border: `1px solid ${state.selectProps.hasError ? 'red' : '#aeb0b5'}`,
    borderRadius: '2px',
    // boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.25)',
    boxShadow: 'none',
    lineHeight: th('lineHeightBase'),
    // marginBottom: '16px',
    padding: 0,
    width: '100%',
    height: '34px',
  }),
  dropdownIndicator: (provided, state) => ({
    ...provided,
    border: 'none',
  }),
  indicatorSeparator: (provided, state) => ({
    ...provided,
    display: 'none',
  }),
  menu: (provided, state) => ({
    ...provided,
    border: `1px solid ${th('colorBorder')}`,
    borderRadius: '2px',
    boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.25)',
    boxSizing: 'border-box',
    fontFamily: 'Source Sans Pro',
    marginTop: '4px',
  }),
  menuList: (provided, state) => ({
    ...provided,
    padding: 0,
  }),
  option: (styles, { data, isDisabled, isFocused, isSelected }) => ({
    ...styles,
    // eslint-disable-next-line no-nested-ternary
    backgroundColor: isSelected
      ? '#205493'
      : isFocused
      ? '#9CB8D9'
      : 'transparent',
    borderBottom: '1px solid #aeb0b5',
    // eslint-disable-next-line no-nested-ternary
    color: isDisabled ? '#888' : isSelected ? 'white' : `${th('colorText')}`,
    cursor: isDisabled ? 'not-allowed' : 'pointer',
    fontFamily: 'Source Sans Pro',
    paddingLeft: '24px',
    textAlign: 'start',
  }),
}
// #endregion styles

const CustomOptionWrapper = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
`

const Tag = styled.div`
  align-self: center;
  background-color: gray;
  border-radius: 3px;
  color: ${th('colorTextReverse')};
  display: inline;
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding: ${grid(0.25)};
`

/* eslint-disable react/prop-types */
const CustomOption = props => {
  const { data, hasHtmlLabels, ...rest } = props
  const { label, tag } = data

  return (
    <components.Option data={data} {...rest}>
      <CustomOptionWrapper>
        {hasHtmlLabels && <DisplayInlineEditor content={label} />}
        {!hasHtmlLabels && label}
        {tag && <Tag>{tag}</Tag>}
      </CustomOptionWrapper>
    </components.Option>
  )
}
/* eslint-enable react/prop-types */

const CustomValue = props => {
  const { children, ...rest } = props

  return (
    <components.SingleValue {...rest}>
      <DisplayInlineEditor content={children} key={children} />
    </components.SingleValue>
  )
}

const Select = props => {
  const {
    async,
    asyncDebounceValue,
    className,
    boldLabel,
    description,
    disabled,
    hasError,
    hasHtmlLabels,
    isSearchable,
    label,
    loadOptions,
    multiple,
    name,
    placeholder,
    'data-test-id': testId,
    required,
    // value,
    ...rest
  } = props

  const componentsProp = {
    Option: itemProps => (
      <CustomOption hasHtmlLabels={hasHtmlLabels} {...itemProps} />
    ),
  }

  if (hasHtmlLabels) {
    componentsProp.SingleValue = CustomValue
  }

  const defaultPlaceholder = async ? 'Search...' : 'Select...'

  const selectProps = {
    classNamePrefix: 'select',
    closeMenuOnSelect: !multiple,
    components: componentsProp,
    hasError,
    isDisabled: disabled,
    isMulti: multiple,
    menuPortalTarget: document.body,
    name,
    testId,
    placeholder: placeholder || defaultPlaceholder,
    styles: customStyles,
    ...rest,
  }

  const handleLoadOptions = debounce(
    async val => loadOptions(val),
    asyncDebounceValue,
  )

  return (
    <Wrapper className={className}>
      {label && (
        <Label
          bold={boldLabel}
          description={description}
          htmlFor={name}
          name={name}
          required={required}
          value={label}
        />
      )}

      <SelectWrapper data-test-id={testId} disabled={disabled}>
        {!async && <ReactSelect {...selectProps} />}

        {async && (
          <AsyncSelect {...selectProps} loadOptions={handleLoadOptions} />
        )}
      </SelectWrapper>
    </Wrapper>
  )
}

Select.propTypes = {
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ),

  async: PropTypes.bool,
  /** [`async` only] */
  asyncDebounceValue: PropTypes.number,
  boldLabel: PropTypes.bool,
  defaultValue: PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
  }),
  description: PropTypes.string,
  disabled: PropTypes.bool,
  hasError: PropTypes.bool,
  hasHtmlLabels: PropTypes.bool,
  isSearchable: PropTypes.bool,
  label: PropTypes.string,
  /** [`async` only] A function that returns a promise. */
  loadOptions: PropTypes.func,
  multiple: PropTypes.bool,
  testId: PropTypes.string,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
}

Select.defaultProps = {
  async: false,
  asyncDebounceValue: 500,
  boldLabel: false,
  defaultValue: null,
  description: null,
  disabled: false,
  hasError: false,
  hasHtmlLabels: false,
  isSearchable: true,
  label: null,
  loadOptions: null,
  multiple: false,
  name: null,
  options: [],
  placeholder: null,
  required: false,
  testId: null,
}

export default Select
