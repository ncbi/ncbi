import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid, th } from '@pubsweet/ui-toolkit'

import DocumentIcon from './DocumentIcon'
import SvgIcon from './SvgIcon'
import DisplayInlineEditor from '../wax/DisplayInlneEditor'

const Wrapper = styled.div`
  background-color: ${th('colorTextPlaceholder')};
  display: flex;
  justify-content: space-between;
  padding: ${grid(1)} ${grid(2)};

  > div:first-child {
    align-items: center;
    display: flex;

    > :not(:last-child) {
      margin-right: ${grid(2)};
    }
  }
`

const Side = styled.div``

const Title = styled.div`
  align-items: center;
  display: flex;

  > svg {
    margin-right: ${grid(0.5)};
    min-width: 25px;
  }
`

const BookHeader = props => {
  const {
    className,
    childrenLeft,
    childrenRight,
    title,
    type,
    workflow,
  } = props

  return (
    <Wrapper className={className}>
      <Side>
        {workflow && <DocumentIcon format={workflow} />}

        <Title>
          <SvgIcon name={type} />
          <DisplayInlineEditor content={title} key={title} />
        </Title>

        {childrenLeft}
      </Side>

      <Side>{childrenRight}</Side>
    </Wrapper>
  )
}

BookHeader.propTypes = {
  title: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['book', 'bookWithChapters', 'collection']).isRequired,
  workflow: PropTypes.oneOf(['pdf', 'xml', 'word']),

  childrenLeft: PropTypes.oneOfType([
    PropTypes.elementType,
    PropTypes.node,
    PropTypes.func,
  ]),

  childrenRight: PropTypes.oneOfType([
    PropTypes.elementType,
    PropTypes.node,
    PropTypes.func,
  ]),
}

BookHeader.defaultProps = {
  childrenLeft: null,
  childrenRight: null,
  workflow: null,
}

export default BookHeader
