import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid, th } from '@pubsweet/ui-toolkit'

import FieldDescription from './FieldDescription'

// #region styled

const Wrapper = styled.div`
  align-items: center;
  display: ${props => (props.inline ? 'inline-flex' : 'flex')};

  > label {
    font-weight: ${props => (props.bold ? 'bold' : 'normal')};
  }

  > label,
  > span {
    margin-bottom: ${grid(0.5)};
  }
`

const Required = styled.span`
  color: ${th('colorError')};
  margin-left: ${grid(0.25)};
`

const Description = styled(FieldDescription)`
  margin-left: ${grid(1)};
`

// #endregion styled

const Label = props => {
  const {
    bold,
    className,
    description,
    htmlFor,
    inline,
    name,
    required,
    value,
  } = props

  return (
    <Wrapper bold={bold} className={className} inline={inline} name={name}>
      <label htmlFor={htmlFor}>{value}</label>
      {required && <Required>*</Required>}
      {description && <Description content={description} />}
    </Wrapper>
  )
}

Label.propTypes = {
  value: PropTypes.string.isRequired,
  name: PropTypes.string,
  bold: PropTypes.bool,
  description: PropTypes.string,
  htmlFor: PropTypes.string,
  inline: PropTypes.bool,
  required: PropTypes.bool,
}

Label.defaultProps = {
  bold: false,
  description: null,
  name: null,
  htmlFor: null,
  inline: false,
  required: false,
}

export default Label
