import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid, th } from '@pubsweet/ui-toolkit'

import UIIcon from './Icon'

const ICON_GRID_SIZE = 2.5

const Wrapper = styled.div`
  background-color: ${th('colorTextPlaceholder')};
  border-radius: 3px;
  display: inline-flex;
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding: ${props => (props.compact ? grid(0.5) : grid(2))};
`

const Icon = styled(UIIcon)`
  padding: 0;
`

const IconWrapper = styled.div`
  height: ${grid(ICON_GRID_SIZE)};
  margin-right: ${grid(1)};
`

const ContentWrapper = styled.div`
  padding-top: 1px;
`

const Note = props => {
  const { className, compact, content } = props

  return (
    <Wrapper className={className} compact={compact}>
      <IconWrapper>
        <Icon size={ICON_GRID_SIZE}>alert-circle</Icon>
      </IconWrapper>

      <ContentWrapper>{content}</ContentWrapper>
    </Wrapper>
  )
}

Note.propTypes = {
  content: PropTypes.string.isRequired,
  compact: PropTypes.bool,
}

Note.defaultProps = {
  compact: false,
}

export default Note
