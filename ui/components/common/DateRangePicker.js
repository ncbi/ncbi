import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

import Button from './Button'

const Wrapper = styled.div`
  position: relative;
`

const StyledButtonContainer = styled.div`
  align-items: center;
  display: flex;
`

const StyledButton = styled(Button)`
  align-items: center;
  background-color: ${({ selected }) => (selected ? '#eaf3ff' : 'transparent')};
  border: 1px solid #aeb0b5;
  color: #898a8c;
  display: flex;
  margin: 0;
  padding-right: 26px;

  &:hover {
    background: ${({ selected }) => (selected ? '#eaf3ff' : 'transparent')};
  }
`

const ClearButton = styled.div`
  background: transparent;
  border: none;
  cursor: pointer;
  font-size: 16px;
  font-weight: bold;
  outline: none;
  position: absolute;
  right: 12px;
`

const DatePickerWrapper = styled.div`
  position: absolute;
  z-index: 1000;
`

const DateRangePicker = ({ label, onChange, value, ...props }) => {
  const [startDate, setStartDate] = useState(null)
  const [endDate, setEndDate] = useState(null)
  const [isDatepickerOpen, setIsDatePickerOpen] = useState(false)
  const [showClearButton, setShowClearButton] = useState(false)

  const wrapperRef = useRef(null)

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside)

    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [])

  const toggleDatepicker = () => {
    setIsDatePickerOpen(!isDatepickerOpen)
  }

  const handleClickOutside = event => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      setIsDatePickerOpen(false)
    }
  }

  const handleDateChange = date => {
    if (!startDate && !endDate) {
      setStartDate(date)
      setEndDate(date)
      onChange([date, date])
    } else if (date <= startDate) {
      setStartDate(date)
      onChange([date, endDate])
    } else if (date >= endDate) {
      setEndDate(date)
      onChange([startDate, date])
    } else {
      setStartDate(date)
      onChange([date, endDate])
    }

    setShowClearButton(true)
  }

  const clearDates = () => {
    setStartDate(null)
    setEndDate(null)
    onChange(null)
    setShowClearButton(false)
  }

  useEffect(() => {
    if (value.length === 0 && startDate !== null && endDate !== null) {
      clearDates()
    }
  }, [value])

  return (
    <Wrapper ref={wrapperRef}>
      <StyledButton
        onClick={toggleDatepicker}
        outlined
        selected={startDate || endDate}
        status="primary"
      >
        <StyledButtonContainer>
          {label}
          {showClearButton && <ClearButton onClick={clearDates}>X</ClearButton>}
        </StyledButtonContainer>
      </StyledButton>
      {isDatepickerOpen && (
        <DatePickerWrapper>
          <DatePicker
            dateFormat="yyyy-mm-dd"
            dateFormatCalendar="MMMM"
            endDate={endDate}
            inline
            onChange={handleDateChange}
            scrollableYearDropdown
            selected={startDate}
            selectsRange
            showYearDropdown
            startDate={startDate}
            yearDropdownItemNumber={15}
            {...props}
          />
        </DatePickerWrapper>
      )}
    </Wrapper>
  )
}

DateRangePicker.propTypes = {
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.arrayOf(PropTypes.string),
}

DateRangePicker.defaultProps = {
  value: [],
}

export default DateRangePicker
