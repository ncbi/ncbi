import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { TextField } from '@pubsweet/ui'

const StyledTextField = styled(TextField)`
  display: inline-flex;
  margin-bottom: 0;
  width: 400px;
`

const Search = ({
  triggerSearch,
  className,
  onChange,
  customSearchValue,
  value,
}) => {
  const [textSearch, setTextSearch] = useState(value)

  const handleChange = e => {
    const val = e.target.value
    setTextSearch(val)
    triggerSearch(val)
  }

  const handleKeyDown = e => {
    // enter key
    if (e.keyCode === 13) {
      triggerSearch(customSearchValue || textSearch)
    }
  }

  useEffect(() => {
    if (value === '' && value !== textSearch) {
      setTextSearch('')
    }
  }, [value])

  return (
    <StyledTextField
      className={className}
      onChange={onChange || handleChange}
      onKeyDown={handleKeyDown}
      placeholder="Search"
      value={customSearchValue || textSearch}
    />
  )
}

Search.propTypes = {
  /** Expands accordion content */
  triggerSearch: PropTypes.func,
  onChange: PropTypes.func,
  customSearchValue: PropTypes.string,
  value: PropTypes.string,
}

Search.defaultProps = {
  triggerSearch: () => {},
  onChange: null,
  customSearchValue: null,
  value: '',
}

export default Search
