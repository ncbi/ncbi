import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { RadioGroup } from '@pubsweet/ui'
import { grid, th } from '@pubsweet/ui-toolkit'

import Label from './Label'

// #region styled

const Wrapper = styled.div`
  > div:last-child {
    display: inline-flex;

    label:not(:last-child) {
      margin-bottom: ${grid(1)};
    }
  }
`

const StyledRadio = styled(RadioGroup)`
  span {
    font-style: normal;

    &:before {
      border: 4px solid white;
      color: ${th('colorText')};
      height: 10px;
      margin-left: 2px;
      transition: background-color 0.1s ease-in;
      width: 10px;
    }

    &[disabled] {
      color: ${th('colorDisabled')};

      &:before {
        color: ${th('colorDisabled')};
      }

      &:hover {
        color: ${th('colorDisabled')};
      }
    }
  }

  &:hover {
    span {
      color: ${th('colorText')};

      &[disabled] {
        &:before {
          box-shadow: 0 0 0 ${th('borderWidth')} ${th('colorBorder')};
          color: ${th('colorDisabled')};
        }
      }

      &:before {
        animation: none;
      }
    }
  }

  input {
    display: none;
  }
`

// #endregion styled

const Radio = props => {
  const { className, description, label, required, ...rest } = props

  return (
    <Wrapper>
      {label && (
        <Label
          bold
          description={description}
          required={required}
          value={label}
        />
      )}

      <StyledRadio className={className} {...rest} />
    </Wrapper>
  )
}

Radio.propTypes = {
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string,
    }),
  ).isRequired,

  description: PropTypes.string,
  label: PropTypes.string,
  required: PropTypes.bool,
}

Radio.defaultProps = {
  description: null,
  label: null,
  required: false,
}

export default Radio
