import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'
import { Tooltip } from 'antd'
import { QuestionOutlined, SyncOutlined } from '@ant-design/icons'

import { th, grid } from '@pubsweet/ui-toolkit'

const StopLine = () => (
  <svg
    fill="none"
    height="52"
    viewBox="0 0 86 52"
    width="86"
    xmlns="http://www.w3.org/2000/svg"
  >
    <line
      stroke="white"
      strokeWidth="4"
      x1="1.00772"
      x2="85.0077"
      y1="50.2635"
      y2="2.26351"
    />
  </svg>
)

const Label = styled.span`
  align-items: center;
  background: ${props =>
    props.missing ? th('colorError') : th('colorPrimary')};
  border-radius: ${grid(2)};
  color: ${th('colorTextReverse')};
  cursor: pointer;
  display: flex;
  font-size: '22px';
  font-weight: 600;
  height: min-content;
  justify-content: space-around;
  max-width: 70px;
  padding: 0 ${grid(1)};
  position: relative;
`

const Excluded = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  position: absolute;
  width: 100%;
`

const TOCLabelWrapper = styled.span`
  margin-right: ${grid(0.5)};
`

const explainers = {
  excluded: 'Excluded from table of contents.',
  duplicate: 'Appears in more than one part.',
  missing:
    'Can not place in TOC because the ordering value specified in Settings is missing from the source file.',
}

const TOCLabel = props => {
  const { className, excluded, duplicate, missing } = props

  if (!excluded && !duplicate && !missing) return null

  const id = uuid()

  const handleClick = e => {
    e.preventDefault()
  }

  const tooltipText = Object.keys(explainers)
    .reduce((accumulated, current) => {
      if (props[current]) accumulated.push(explainers[current])
      return accumulated
    }, [])
    .join(' ')

  return (
    <Tooltip title={tooltipText} trigger="click">
      <Label
        className={className}
        data-event="click hover focus"
        data-for={id}
        data-tip
        missing={missing}
        onClick={handleClick}
      >
        <TOCLabelWrapper>TOC</TOCLabelWrapper>

        {excluded && (
          <Excluded>
            <StopLine />
          </Excluded>
        )}

        {duplicate && <SyncOutlined />}

        {missing && <QuestionOutlined />}
      </Label>
    </Tooltip>
  )
}

TOCLabel.propTypes = {
  duplicate: PropTypes.bool,
  excluded: PropTypes.bool,
  missing: PropTypes.bool,
}

TOCLabel.defaultProps = {
  duplicate: false,
  excluded: false,
  missing: false,
}

export default TOCLabel
