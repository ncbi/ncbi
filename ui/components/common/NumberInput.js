import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import { grid, th } from '@pubsweet/ui-toolkit'

import FieldDescription from './FieldDescription'

// #region styled
const Wrapper = styled.div`
  align-items: center;
  display: ${props => (props.display ? props.display : 'flex')};

  > div:last-child {
    margin-left: 4px;
    margin-top: 3px;
  }

  > input {
    color: ${props => {
      const { max, min, value } = props

      if (value) {
        const valueToCompare = Number(value)

        if ((min && valueToCompare < min) || (max && valueToCompare > max)) {
          return th('colorError')
        }
      }

      return th('colorText')
    }};
    font-family: ${th('fontInterface')};
    font-size: ${th('fontSizeBase')};
    line-height: ${th('lineHeightBase')};
    padding-left: ${grid(1)};
    width: 100px;

    /* stylelint-disable-next-line order/properties-alphabetical-order */
    ${props =>
      props.disabled &&
      css`
        cursor: not-allowed;
      `}
  }
`

const Label = styled.span`
  margin-right: ${grid(1)};

  &:after {
    content: ${props => (props.display ? '' : ':')};
  }
`
// #endregion styled

/**
 * This is still an input, so it prefers using text as values.
 * As a component though, it makes more sense to use numbers, so we coerce the
 * types to accommodate for the component, but still keep an intuitive API.
 */

const NumberInput = props => {
  const {
    className,
    disabled,
    description,
    label,
    min,
    max,
    name,
    onBlur,
    onChange,
    value,
    display,
  } = props

  const handleChange = e => {
    let currentValue = e.target.value
    if (!currentValue) currentValue = ''
    onChange && onChange(currentValue)
  }

  const handleBlur = e => {
    if (min && e.target.value && Number(e.target.value) < min) {
      onChange && onChange(String(min))
    }

    if (max && e.target.value && Number(e.target.value) > max) {
      onChange && onChange(String(max))
    }

    onBlur && onBlur(e)
  }

  return (
    <Wrapper
      className={className}
      disabled={disabled}
      display={display}
      max={max}
      min={min}
      value={value}
    >
      {label && <Label>{label}</Label>}

      <input
        disabled={disabled}
        max={max}
        min={min}
        name={name}
        onBlur={handleBlur}
        onChange={handleChange}
        type="number"
        value={value}
      />

      {description && <FieldDescription content={description} />}
    </Wrapper>
  )
}

NumberInput.propTypes = {
  description: PropTypes.string,
  disabled: PropTypes.bool,
  display: PropTypes.string,
  label: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
  name: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  value: PropTypes.string,
}

NumberInput.defaultProps = {
  description: null,
  disabled: false,
  display: null,
  label: null,
  min: null,
  max: null,
  name: null,
  onBlur: null,
  onChange: null,
  value: '',
}

export default NumberInput
