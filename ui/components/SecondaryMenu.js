/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/require-default-props */
/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { th } from '@pubsweet/ui-toolkit'

// #region styled-components
const Root = styled.nav`
  align-items: center;
  display: flex;
  /* height: calc(${th('gridUnit')} * 4); */
  justify-content: space-between;
  margin-left: calc(${th('gridUnit')} * 2);
  margin-right: calc(${th('gridUnit')} * 2);
`

const Section = styled.div`
  align-items: center;
  display: flex;
`

const Right = styled.span`
  margin-left: auto;
`

const Item = styled.span`
  align-items: center;
  display: inline-flex;
`
// #endregion

const SecondaryMenu = ({ navLinkComponents, navLinkComponentsRight }) => (
  <Root>
    <Section>
      {navLinkComponents &&
        navLinkComponents.map((NavLinkComponent, idx) => (
          <span key={NavLinkComponent.props.to}>
            <Item>{NavLinkComponent}</Item>
          </span>
        ))}
    </Section>
    <Right>
      {navLinkComponentsRight &&
        navLinkComponentsRight.map((NavLinkComponent, idx) => (
          <span key={NavLinkComponent.props.to}>
            <Item>{NavLinkComponent}</Item>
          </span>
        ))}
    </Right>
  </Root>
)

SecondaryMenu.propTypes = {
  navLinkComponents: PropTypes.arrayOf(PropTypes.element),
  rightComponent: PropTypes.oneOfType([PropTypes.element, PropTypes.func]),
}

export default SecondaryMenu
