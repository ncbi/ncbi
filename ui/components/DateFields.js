/* eslint-disable react/prop-types */
/* eslint-disable consistent-return */

import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import moment from 'moment'

import { TextField } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { Icon as uiIcon, Label } from './common'

// #region Styled components
const Root = styled.div`
  align-content: center;
  display: flex;
  line-height: 16px;
`

const ElementWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const SeparatourUi = styled.div`
  margin-left: ${th('gridUnit')};
  margin-right: ${th('gridUnit')};
`

const TextFieldUI = styled(TextField)`
  border-width: 0px;
  height: 34px;
  line-height: 16px;
  margin-bottom: 0;
  max-width: 64px;
  width: ${props => (props.width ? props.width : '100%')};

  [type='text'] {
    border-color: ${th('colorBorder')};
    border-radius: ${th('borderRadius')};
    border-style: solid;
    border-width: 1px;
    color: ${th('colorText')};
  }
`

const Icon = styled(uiIcon)`
  cursor: pointer;
`
// #endregion

// #region Constants and functions
const [Year, YearRange, MonthYear, DayMonthYear, StandartFormat] = [
  'YYYY',
  'YYYY-YYYY',
  'MM/YYYY',
  'DD/MM/YYYY',
  'YYYY-MM-DD',
]

const get2DigitNumber = number =>
  Number(number) < 10 ? `0${Number(number)}` : number

const convertStringToDate = (format, dateString) => {
  const emptyObject = { day: null, month: null, year: null, year2: null }
  let isValid, returnObject
  let returnDate = emptyObject

  switch (format) {
    case DayMonthYear: {
      isValid = moment(dateString, DayMonthYear, true).isValid()
      const isValidStandart = moment(dateString, StandartFormat).isValid()

      if (isValid || isValidStandart) {
        returnDate = isValid
          ? moment(dateString, DayMonthYear)
          : moment(dateString, StandartFormat)

        const day = returnDate.date()
        const month = returnDate.month() + 1

        returnObject = {
          day: get2DigitNumber(day),
          month: get2DigitNumber(month),
          year: returnDate.year(),
          year2: null,
        }
      }

      break
    }

    case MonthYear: {
      isValid =
        moment(dateString, MonthYear, true).isValid() ||
        moment(dateString, DayMonthYear, true).isValid()

      const isValidStandart = moment(dateString, StandartFormat).isValid()

      if (isValid || isValidStandart) {
        returnDate = isValid
          ? moment(dateString, DayMonthYear)
          : moment(dateString, StandartFormat)

        const month = returnDate.month() + 1

        returnObject = {
          day: null,
          month: get2DigitNumber(month),
          year: returnDate.year(),
          year2: null,
        }
      }

      break
    }

    case Year: {
      isValid = moment(dateString, Year, true).isValid()
      const isValidCustom = moment(dateString, DayMonthYear).isValid()

      if (isValid) {
        returnObject = {
          day: '01',
          month: '01',
          year: moment(dateString, Year).year(),
          year2: null,
        }
      } else if (isValidCustom) {
        returnObject = {
          day: '01',
          month: '01',
          year: moment(dateString, DayMonthYear).year(),
          year2: null,
        }
      } else {
        returnObject = emptyObject
      }

      break
    }

    case YearRange: {
      const years = dateString?.split('-') || ['', '']
      if (years.length < 2) years.push('')
      years[0] = moment(years[0], Year, true).isValid() ? years[0] : null
      years[1] = moment(years[1], Year, true).isValid() ? years[1] : null

      returnObject = {
        day: null,
        month: null,
        year: years[0],
        year2: years[1],
      }

      break
    }

    default:
  }

  return returnObject
}

const convertDateToObj = (_format, day, month, year, year2) => {
  if (_format === YearRange) {
    return { startYear: String(year), endYear: String(year2) }
  }

  return { day: String(day), month: String(month), year: String(year) }
}

const convertDateToString = (_format, _day, _month, _year, _year2) => {
  let date = ''

  switch (_format) {
    case DayMonthYear: {
      date = _day || _month || _year ? `${_day}/${_month}/${_year}` : ''
      break
    }

    case MonthYear: {
      date = _month || _year ? `01/${_month}/${_year}` : ''
      break
    }

    case Year: {
      date = _year ? `01/01/${_year}` : ''
      break
    }

    case YearRange: {
      date = _year && _year2 ? `${_year}-${_year2}` : _year
      break
    }

    default:
      date = ''
  }

  return date
}
// #endregion

const DateFields = ({
  format,
  separator = '/',
  field,
  label,
  form: { setFieldValue, validateForm },
  required,
  shuldReturnObject = false,
}) => {
  // conditions to show the fields
  const showDay = format === DayMonthYear
  const showMonth = format === DayMonthYear || format === MonthYear

  const showYear =
    format === DayMonthYear ||
    format === MonthYear ||
    format === Year ||
    format === YearRange

  const showYear2 = format === YearRange

  const [day, setDay] = useState()
  const [month, setMonth] = useState()
  const [year, setYear] = useState()
  const [year2, setYear2] = useState()

  useEffect(() => {
    if (day || month || year || year2) {
      const dt = shuldReturnObject
        ? convertDateToObj(format, day, month, year, year2)
        : convertDateToString(format, day, month, year, year2)

      setFieldValue(field.name, dt)

      validateForm()
    }
  }, [day, month, year, year2])

  useEffect(() => {
    if (typeof field.value === 'string') {
      const dateObject = convertStringToDate(format, field.value)

      if (dateObject) {
        setDay(dateObject.day)
        setMonth(dateObject.month)
        setYear(dateObject.year)
        setYear2(dateObject.year2)
      }
    } else if (format === YearRange) {
      setYear(field.value.startYear)
      setYear2(field.value.endYear)
    } else {
      setDay(field.value?.day)
      setMonth(field.value?.month)
      setYear(field.value?.year)
      setYear2(field.value?.year2)
    }
  }, [])

  // #region Handle sepparate filed changes
  const changeDay = event => {
    const { value } = event.target
    if (Number.isNaN(value)) return setDay('')
    if (value > 31 && value !== '') return setDay(31)
    if (value < 0 && value !== '') return setDay(1)
    setDay(value)
  }

  const changeMonth = event => {
    const { value } = event.target
    if (Number.isNaN(value)) return setMonth('')
    if (value > 12 && value !== '') return setMonth(12)
    if (value < 0 && value !== '') return setMonth(1)
    setMonth(value)
  }

  const changeYear = event => {
    const { value } = event.target
    const IsValidDate = Number.isNaN(value) || (value < 0 && value !== '')
    if (IsValidDate) return setYear('')
    setYear(value)
  }

  const changeYear2 = event => {
    const { value } = event.target
    const IsValidDate = Number.isNaN(value) || (value < 0 && value !== '')
    if (IsValidDate) return setYear2('')
    setYear2(value)
  }
  // #endregion

  // #region Icons click functions
  const setTodayDate = () => {
    clearDate()
    const thisDay = moment().format('DD')
    const thisMonth = moment().format('MM')
    const thisyear = moment().format('YYYY')
    setDay(thisDay)
    setMonth(thisMonth)
    setYear(thisyear)
    setYear2(thisyear)
  }

  const clearDate = () => {
    setDay('')
    setMonth('')
    setYear('')
    setYear2('')

    const dt = shuldReturnObject
      ? convertDateToObj(format, day, month, year, year2)
      : ''

    setFieldValue(field.name, dt)
  }
  // #endregion

  // #region Day and month number format
  const onBlurDay = event => {
    const { value } = event.target

    if (value) {
      setDay(get2DigitNumber(value))
      convertDateToString(format, day, month, year, null)
    }
  }

  const onBlurMonth = event => {
    const { value } = event.target

    if (value) {
      setMonth(get2DigitNumber(value))
      convertDateToString(format, day, month, year, null)
    }
  }
  // #endregion

  return (
    <ElementWrapper>
      {label && <Label required={required} value={label} />}

      <Root>
        {showDay ? (
          <TextFieldUI
            onBlur={event => onBlurDay(event)}
            onChange={event => changeDay(event)}
            placeholder="DD"
            value={day}
          />
        ) : null}

        {showDay && showMonth ? <SeparatourUi>{separator}</SeparatourUi> : ''}
        {showMonth ? (
          <TextFieldUI
            onBlur={event => onBlurMonth(event)}
            onChange={event => changeMonth(event)}
            placeholder="MM"
            value={month}
          />
        ) : null}
        {showMonth && showYear ? <SeparatourUi>{separator}</SeparatourUi> : ''}
        {showYear ? (
          <TextFieldUI
            onChange={event => changeYear(event)}
            placeholder="YYYY"
            value={year}
          />
        ) : null}
        {showYear && showYear2 ? <SeparatourUi>-</SeparatourUi> : ''}
        {showYear2 ? (
          <TextFieldUI
            onChange={event => changeYear2(event)}
            placeholder="YYYY"
            value={year2}
          />
        ) : null}
        <Icon
          color="#8a8383"
          onClick={() => setTodayDate()}
          onKeyDown={event => {
            if (event.key === 'Enter') {
              setTodayDate()
            }
          }}
          tabIndex={0}
          title="Today Date"
        >
          crosshair
        </Icon>
        <Icon
          color="red"
          onClick={() => clearDate()}
          onKeyDown={event => {
            if (event.key === 'Enter') {
              clearDate()
            }
          }}
          tabIndex={0}
          title="Clear Date"
        >
          x
        </Icon>
      </Root>
    </ElementWrapper>
  )
}

export default DateFields
