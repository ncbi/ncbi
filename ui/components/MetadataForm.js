import React, { useContext, useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import merge from 'lodash/merge'
import uniq from 'lodash/uniq'
import { Formik, Form, FieldArray } from 'formik'
import * as yup from 'yup'
import { grid, th } from '@pubsweet/ui-toolkit'
import {
  ValidDateCheck,
  DayValidation,
  MonthValidation,
  DayYearValidation,
  validatePublicationDate,
} from '../common/metadataDateValidation'

import {
  EditorComponent as Editor,
  SelectComponent as Select,
  TextFieldComponent as UITextField,
  ToggleComponent as UIToggle,
  DateInputComponent as DateInput,
  Error,
} from './FormElements/FormikElements'
import Button from './common/Button'
import FormSection from './common/FormSection'
import CoverComponent from './CoverComponent'
import Granthub from './Granthub/Granthub'
import ConstantsContext from '../../app/constantsContext'
import ContributorsComponent from './Contributors/ContributorsComponent'
import NotesContainer from './Note'
import Note from './common/Note'
import ScrollToFieldError from './common/ScrollToFieldError'
import PermissionsGate from '../../app/components/PermissionsGate'
import Checkbox from './common/Checkbox'

// #region styled
const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 800px;
  padding-bottom: 50px;
  padding-top: 20px;

  > form > div:not(:last-child) {
    margin-bottom: ${grid(2)};
  }
`

const TextField = styled(UITextField)`
  margin-bottom: 0;
`

const Toggle = styled(UIToggle)`
  max-width: 500px;
`

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: ${grid(2)};
`

const Tag = styled.div`
  background-color: gray;
  border-radius: ${th('borderRadius')};
  color: ${th('colorTextReverse')};
  display: inline-block;
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding: ${grid(0.25)} ${grid(0.5)};
`

const StyledCheckbox = styled(Checkbox)`
  display: flex;
  margin: 10px ${grid(2)} 0px 0px;
`

// #endregion styled

// #region display & permissions
const readOnlyValues = ['sourceType', 'domain', 'bookSubmitId', 'objectId']
const publisherOverrideValues = ['publisherLocation', 'publisherName']

const permissionsOverrideValues = [
  'copyrightStatement',
  'openAccessLicense',
  'licenseStatement',
  'licenseType',
]

const handlePermissions = (
  key,
  variant,
  submissionType,
  readOnly,
  applyCoverFromParent,
  applyPermissionsFromParent,
  applyPublisherFromParent,
  inCollection,
  conversionWorkflow,
) => {
  if (readOnly) return false
  if (readOnlyValues.includes(key)) return false

  const isWholeBook =
    variant === 'book' &&
    submissionType === 'wholeBook' &&
    conversionWorkflow !== 'word'

  const mapper = {
    abstract: true,
    abstractGraphic: true,
    alternativeTitle: !isWholeBook,
    applyCoverToBooks: true,
    applyPermissions: true,
    applyPublisherToBooks: true,
    authors: true,
    objectId: false,
    bookSourceType: !inCollection,
    bookSubmitId: false,
    chapterProcessedSourceType: true,
    collaborativeAuthors: true,
    copyrightStatement: !applyPermissionsFromParent,
    cover: !applyCoverFromParent,
    dateCreated: true,
    dateOfPublication: !isWholeBook,
    dateUpdated: true,
    doi: !isWholeBook,
    domain: false,
    edition: !isWholeBook,
    editors: true,
    funding: true,
    isbn: true,
    issn: true,
    eissn: true,
    licenseStatement: !applyPermissionsFromParent,
    licenseType: !applyPermissionsFromParent,
    licenseUrl: false,
    notes: true,
    openAccessLicense: !applyPermissionsFromParent,
    publicationDateType: !isWholeBook,
    publisherBookSeriesTitle: true,
    publisherLocation: !isWholeBook && !applyPublisherFromParent,
    publisherName: !isWholeBook && !applyPublisherFromParent,
    seriesEditors: true,
    sourceType: false,
    subtitle: !isWholeBook,
    title: !isWholeBook,
    volume: !isWholeBook,
    wholeBookSourceType: true,

    // language: false,
    // nlmId: false,
  }

  if (typeof mapper[key] === 'undefined') {
    console.error(
      `Collection metadata form permissions mapper: Requested key ${key} does not exist!`,
    )
  }

  return mapper[key]
}

const handleShow = (
  key,
  variant,
  submissionType,
  conversionWorkflow,
  isBookSeriesCollection,
  isFundedCollection,
) => {
  const isBook = variant === 'book'
  const isCollection = variant === 'collection'

  const isChapterProcessed =
    isBook &&
    (submissionType === 'chapterProcessed' || conversionWorkflow === 'word')

  // const isWholeBook = isBook && submissionType === 'wholeBook'
  const isWord = isBook && conversionWorkflow === 'word'
  const isXML = isBook && conversionWorkflow === 'xml'
  const isPdf = isBook && conversionWorkflow === 'pdf'

  const mapper = {
    abstract: isCollection || isChapterProcessed,
    abstractGraphic: isWord,
    alternativeTitle: isBook,
    applyCoverToBooks: isCollection,
    applyPermissions: isCollection && isBookSeriesCollection,
    applyPublisherToBooks: isCollection && isBookSeriesCollection,
    authors: isChapterProcessed,
    objectId: isBook,
    bookSourceType: isBook,
    bookSubmitId: isXML || isPdf,
    chapterProcessedSourceType: isCollection,
    collaborativeAuthors: isCollection || isChapterProcessed,
    copyrightStatement: isCollection || isChapterProcessed || isXML || isPdf,
    cover: true,
    dateCreated: isChapterProcessed,
    dateOfPublication: true,
    dateUpdated: isChapterProcessed,
    doi: isBook,
    domain: true,
    edition: isBook,
    editors: isChapterProcessed,
    funding: isCollection || isBook,
    processinginstructions: isWord,
    pdfprocessinginstruction: isPdf && isChapterProcessed,
    isbn: isChapterProcessed,
    issn: isChapterProcessed || isCollection,
    eissn: isCollection,
    licenseStatement: isCollection || isChapterProcessed || isXML || isPdf,
    licenseType: isCollection || isChapterProcessed || isXML || isPdf,
    licenseUrl: isCollection || isChapterProcessed || isXML || isPdf,
    notes: isCollection || isChapterProcessed,
    openAccessLicense: isCollection || isChapterProcessed || isXML || isPdf,
    publicationDateType: true,
    publisherBookSeriesTitle: isCollection && isBookSeriesCollection,
    publisherLocation: true,
    publisherName: true,
    seriesEditors: isCollection,
    sourceType: isCollection,
    subtitle: isBook,
    title: true,
    volume: isBook,
    wholeBookSourceType: isCollection,

    // language: isWholeBook,
    // nlmId: isWholeBook,

    // sections
    contributorsSection: isCollection || isChapterProcessed,
    coverSection: true,
    customMetadataSection: true,
    permissionsSection: isCollection || isChapterProcessed || isXML || isPdf,
    publicationDateSection: true,
    publicationHistorySection: isChapterProcessed,
    publisherSection: true,
    titleSection: true,
  }

  if (typeof mapper[key] === 'undefined') {
    console.error(
      `Collection metadata form show mapper: Requested key ${key} does not exist!`,
    )
  }

  return mapper[key]
}
// #endregion display & permissions

// #region form-setup
const publicationDateTypeOptions = {
  printPublication: {
    label: 'Print publication',
    value: 'pub-print',
  },
  printPublicationRange: {
    label: 'Print publication range',
    value: 'pubr-print',
  },
  electronicPublication: {
    label: 'Electronic publication',
    value: 'pub-electronic',
  },
  electronicPublicationRange: {
    label: 'Electronic publication range',
    value: 'pubr-electronic',
  },
}

const {
  printPublication,
  printPublicationRange,
  electronicPublication,
  electronicPublicationRange,
} = publicationDateTypeOptions

const bookPublicationDateTypeOptions = [
  printPublication,
  printPublicationRange,
  electronicPublication,
  electronicPublicationRange,
]

const collectionPublicationDateTypeOptions = [
  printPublicationRange,
  electronicPublicationRange,
]

const sourceTypeOptions = [
  { label: 'Report', value: 'Report' },
  { label: 'Book', value: 'Book' },
  { label: 'Database', value: 'Database' },
  { label: 'Documentation', value: 'Documentation' },
]

const licenseTypeOptions = [
  { label: 'CC BY', value: 'by' },
  { label: 'CC BY-NC', value: 'by-nc' },
  { label: 'CC BY-NC-ND', value: 'by-nc-nd' },
  { label: 'CC BY-NC-SA', value: 'by-nc-sa' },
  { label: 'CC BY-ND', value: 'by-nd' },
  { label: 'CC BY-SA', value: 'by-sa' },
  { label: 'Public domain (CC0 1.0)', value: 'public-domain' },
  { label: 'Other', value: 'other' },
]

// const languageOptions = [
//   { label: 'English', value: 'en' },
//   { label: 'Spanish', value: 'es' },
// ]

const defaultInitialValues = {
  abstract: '',
  abstractGraphic: null,
  alternativeTitle: '',
  applyCoverToBooks: false,
  applyPermissions: false,
  applyPublisherToBooks: false,
  authors: [],
  objectId: '',
  bookSourceType: null,
  bookSubmitId: '',
  chapterProcessedSourceType: null,
  collaborativeAuthors: [],
  copyrightStatement: '',
  cover: null,
  dateCreated: { day: '', month: '', year: '' },
  dateOfPublication: {},
  dateUpdated: { day: '', month: '', year: '' },
  doi: '',
  domain: '',
  edition: '',
  editors: [],
  funding: [],
  isbn: '',
  issn: '',
  eissn: '',
  // language: '',
  licenseStatement: '',
  licenseType: null,
  // nlmId: '',
  notes: [],
  processinginstructions: [],
  openAccessLicense: false,
  publicationDateType: null,
  publisherBookSeriesTitle: '',
  publisherLocation: '',
  publisherName: '',
  seriesEditors: [],
  sourceType: null,
  subtitle: '',
  title: '',
  volume: '',
  wholeBookSourceType: null,
}

function validateLicense(val, { parent }) {
  const { openAccessLicense } = parent

  if (openAccessLicense) return !!val

  return true
}

function validateLicenseUrl(val, { parent }) {
  const { openAccessLicense, licenseType, licenseUrl } = parent

  if (openAccessLicense && licenseType === 'other' && licenseUrl) {
    const pattern = new RegExp(
      "^http(s?)://[0-9a-zA-Z]([-.w]*[0-9a-zA-Z])*(:(0-9)*)*(/?)([a-zA-Z0-9-.?,'/\\+&amp;%$#_]*)?$",
    ) // fragment locator

    return !!pattern.test(licenseUrl)
  }

  return true
}

function licenseUrlRequired(val, { parent }) {
  const { openAccessLicense, licenseType, licenseUrl } = parent

  if (openAccessLicense && licenseType === 'other' && !licenseUrl) {
    return false
  }

  return true
}

const makeValidations = (variant, submissionType, conversionWorkflow) => {
  const isChapterProcessed =
    variant === 'book' && submissionType === 'chapterProcessed'

  const isCollection = variant === 'collection'

  const isWholeBook = variant === 'book' && submissionType === 'wholeBook'

  const isWordWholeBook =
    variant === 'book' &&
    submissionType === 'wholeBook' &&
    conversionWorkflow === 'word'

  const validateEditors = rows => {
    const missing = rows.find(row => !(row.surname && row.givenName))
    if (missing) return false
    return true
  }

  const validateContributors = rows => {
    // find a field in any row that is empty
    const missing = rows.find(row => Object.keys(row).find(key => !row[key]))
    if (missing) return false
    return true
  }

  const regMatchISSN = /^\d{4}(-)\d{4}$/

  const possibleValidations = {
    title: yup.string().required('Title is required'),

    publisherName:
      (isCollection || conversionWorkflow === 'word' || isChapterProcessed) &&
      yup.string().required('Publisher name is required'),
    publisherLocation:
      (isCollection || conversionWorkflow === 'word' || isChapterProcessed) &&
      yup.string().required('Publisher location is required'),

    publicationDateType:
      (isCollection || conversionWorkflow === 'word' || isChapterProcessed) &&
      yup.string().nullable().required('Publication date type is required'),
    dateCreated:
      isChapterProcessed &&
      yup
        .object()
        .shape({
          day: yup.string().nullable(true),
          month: yup.string().nullable(true),
          year: yup.string().nullable(true),
        })
        .test('date-valid', 'Please enter valid date', val =>
          ValidDateCheck(val),
        )
        .test(
          'date-day-validation',
          'If day is provided, month and year must be provided too.',
          val => DayValidation(val),
        )
        .test(
          'date-day-year-validation',
          'If day is provided, month must be provided too.',
          val => DayYearValidation(val),
        )
        .test(
          'date-month-year-validation',
          'If month is provided, year must be provided too.',
          val => MonthValidation(val),
        ),
    dateUpdated:
      isChapterProcessed &&
      yup
        .object()
        .shape({
          day: yup.string().nullable(true),
          month: yup.string().nullable(true),
          year: yup.string().nullable(true),
        })
        .test('date-valid', 'Please enter valid date', val =>
          ValidDateCheck(val),
        )
        .test(
          'date-day-validation',
          'If day is provided, month and year must be provided too.',
          val => DayValidation(val),
        )
        .test(
          'date-day-year-validation',
          'If day is provided, month must be provided too.',
          val => DayYearValidation(val),
        )
        .test(
          'date-month-year-validation',
          'If month is provided, year must be provided too.',
          val => MonthValidation(val),
        ),
    dateOfPublication:
      (isCollection || conversionWorkflow === 'word' || isChapterProcessed) &&
      yup
        .object()
        .shape({
          startMonth: yup.string().nullable(),
          startYear: yup.string().nullable(),
          endMonth: yup.string().nullable(),
          endYear: yup.string().nullable(),
        })
        .test('validator-custom', (value, { parent, createError, path }) => {
          const customValidation = validatePublicationDate(value, {
            parent,
            createError,
            path,
          })

          if (customValidation) return customValidation
          return true
        }),
    bookSourceType:
      (isChapterProcessed || isWholeBook) &&
      yup.string().nullable().required('Book source type is required'),
    chapterProcessedSourceType:
      isCollection &&
      yup
        .string()
        .nullable()
        .required('Chapter processed source type is required'),
    wholeBookSourceType:
      isCollection &&
      yup.string().nullable().required('Whole book source type is required'),
    licenseType:
      (isCollection || isChapterProcessed) &&
      yup
        .string()
        .nullable()
        .test(
          'license-type-valid',
          'License type is required',
          validateLicense,
        ),
    licenseStatement:
      (isCollection || isChapterProcessed) &&
      yup
        .string()
        .nullable()
        .test(
          'license-statement-valid',
          'License statement is required',
          validateLicense,
        ),
    licenseUrl:
      (isCollection || isChapterProcessed || isWordWholeBook) &&
      yup
        .string()
        .nullable()
        .test(
          'license-url-field-required',
          'License url is required',
          licenseUrlRequired,
        )
        .test('license-url-valid', 'Enter the valid url', validateLicenseUrl),
    notes:
      (isCollection || isChapterProcessed) &&
      yup
        .array()
        .of(yup.object())
        .test(
          'notes-required-fields',
          'Please fill out all required fields',
          function validateNotes(val) {
            const missing = val.find(note => !note.type || !note.description)
            if (missing) return false
            return true
          },
        ),

    seriesEditors:
      isCollection &&
      yup
        .array()
        .of(yup.object())
        .test(
          'series-editors-test',
          'Please fill out all given names and surnames',
          validateEditors,
        ),
    collaborativeAuthors:
      (isCollection || isChapterProcessed) &&
      yup
        .array()
        .of(yup.object())
        .test(
          'collaborative-authors-test',
          'Please fill out all fields',
          validateContributors,
        ),
    authors:
      isChapterProcessed &&
      yup
        .array()
        .of(yup.object())
        .test(
          'authors-test',
          'Please fill out all given names and surnames',
          validateEditors,
        ),
    editors:
      isChapterProcessed &&
      yup
        .array()
        .of(yup.object())
        .test(
          'editors-test',
          'Please fill out all given names and surnames',
          validateEditors,
        ),

    funding: yup
      .array()
      .of(yup.object())
      .test(
        'grants-empty-row',
        'All grant rows must have an award ID or be deleted',
        val => !val.find(row => !row.number),
      )
      .test(
        'grants-duplicate',
        'Each grant row must have a unique award ID',
        val => {
          const numbers = val.map(row => row.number)
          const deduped = new Set(numbers)
          return !(deduped.size < numbers.length)
        },
      ),

    issn:
      isCollection &&
      yup.string().nullable().matches(regMatchISSN, {
        message:
          'ISSNs need to be provided in the format 0000-0000 where the real digits replace the zeroes and are separated by a hyphen',
        excludeEmptyString: true,
      }),

    eissn:
      isCollection &&
      yup.string().nullable().matches(regMatchISSN, {
        message:
          'ISSNs need to be provided in the format 0000-0000 where the real digits replace the zeroes and are separated by a hyphen',
        excludeEmptyString: true,
      }),
  }

  const relevantValidations = Object.keys(possibleValidations)
    .filter(key => !!possibleValidations[key])
    .reduce(
      (newObj, key) =>
        Object.assign(newObj, { [key]: possibleValidations[key] }),
      {},
    )

  return yup.object().shape(relevantValidations)
}
// #endregion form-setup

const CollectionMetadataForm = props => {
  const {
    className,

    applyCoverFromParent,
    applyPermissionsFromParent,
    applyPublisherFromParent,
    conversionWorkflow,
    inCollection,
    initialFormValues,
    innerRef,
    isBookSeriesCollection,
    isFundedCollection,
    isSaving,
    onSubmit,
    searchGranthub,
    showSubmitButton,
    submissionType,
    submitButtonLabel,
    variant,
    readonly,
  } = props

  const { s } = useContext(ConstantsContext)
  const initialValues = merge({}, defaultInitialValues, initialFormValues)

  const validations = makeValidations(
    variant,
    submissionType,
    conversionWorkflow,
  )

  const readOnly = isSaving || readonly

  const isWholeBook =
    variant === 'book' &&
    submissionType === 'wholeBook' &&
    conversionWorkflow !== 'word'

  const titleSectionLabel =
    variant === 'book' &&
    submissionType === 'wholeBook' &&
    conversionWorkflow !== 'word'
      ? 'Title & IDs'
      : 'Title, Abstract & IDs'

  let dateTypeOptions, notesVariant

  if (variant === 'collection') {
    dateTypeOptions = collectionPublicationDateTypeOptions
    notesVariant = 'collection'
  }

  if (variant === 'book') {
    dateTypeOptions = bookPublicationDateTypeOptions
    notesVariant = 'chapterProcessed'
  }

  const NoteEnhanced = noteProps => (
    <NotesContainer
      disabled={readOnly}
      isFundedCollection={isFundedCollection}
      variant={notesVariant}
      {...noteProps}
    />
  )

  const appliedTag = (
    <div>
      <Tag>Applied from collection</Tag>
    </div>
  )

  const allow = key =>
    handlePermissions(
      key,
      variant,
      submissionType,
      readOnly,
      applyCoverFromParent,
      applyPermissionsFromParent,
      applyPublisherFromParent,
      inCollection,
      conversionWorkflow,
    )

  const show = key =>
    handleShow(
      key,
      variant,
      submissionType,
      conversionWorkflow,
      isBookSeriesCollection,
      isFundedCollection,
    )

  const showGranthubApply =
    variant === 'collection' ||
    (variant === 'book' && submissionType === 'chapterProcessed')

  // Only send outside the values that are relevant in the current scenario
  const handleSubmit = values => {
    const submittedValues = Object.keys(values)
      .filter(
        key =>
          !readOnlyValues.includes(key) &&
          !(applyCoverFromParent && key === 'cover') &&
          !(
            applyPublisherFromParent && publisherOverrideValues.includes(key)
          ) &&
          !(
            applyPermissionsFromParent &&
            permissionsOverrideValues.includes(key)
          ) &&
          show(key),
      )
      .reduce((obj, key) => {
        /* eslint-disable-next-line no-param-reassign */
        obj[key] = values[key]
        return obj
      }, {})

    onSubmit && onSubmit(submittedValues)
  }

  const getAffiliationsList = () => {
    const affiliationsList = []

    initialValues.authors.forEach(author =>
      affiliationsList.push(...(author.affiliation || [])),
    )

    initialValues.editors.forEach(editor =>
      affiliationsList.push(...(editor.affiliation || [])),
    )

    const uniqueElements = uniq(affiliationsList)

    return uniqueElements.map(x => ({ value: x, label: x }))
  }

  const getProcessingInstructions = () => {
    return conversionWorkflow && conversionWorkflow === 'word'
      ? [
          '<?create-gtr-links?>',
          '<?pdfbuild-skip-alt-title-as-rhead?>',
          '<?get-external-navigation-xml related?>',
          '<?external-xml-source-base /pmcdata/bookshelf/gene/external-xml/ready/?> <?external-xml-source-name .molgene.db.xml?>',
        ]
      : ['<?pdfbuild-skip-alt-title-as-rhead?>']
  }

  const [affiliations, setAffiliations] = useState(getAffiliationsList())

  const [processingInstructions] = useState(getProcessingInstructions())

  return (
    <Wrapper className={className}>
      <Formik
        initialValues={initialValues}
        innerRef={innerRef}
        onSubmit={handleSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const { setFieldValue, values, errors } = formProps

          const transformedLicenseTypeOptions = values.openAccessLicense
            ? licenseTypeOptions
            : licenseTypeOptions.map(option => ({
                ...option,
                isDisabled: option.value !== 'other',
              }))

          const handleCoverChange = file => setFieldValue('cover', file)

          const handleAbstractChange = file =>
            setFieldValue('abstractGraphic', file)

          const handleOpenAccessChange = val => {
            if (values.licenseType !== 'other') {
              setFieldValue('licenseType', null)
            }
          }

          const handleClickContributors = (name, val) => {
            setFieldValue(name, val)
          }

          const handleAddNote = () => {
            const emptyNote = {
              type: null,
              title: null,
              description: '',
            }

            setFieldValue('notes', [...values.notes, emptyNote])
          }

          const handleProcessingInstructions = value => {
            const isExist = values.processinginstructions.includes(value)

            if (isExist) {
              values.processinginstructions.splice(
                values.processinginstructions.indexOf(value),
                1,
              )

              setFieldValue('processinginstructions', [
                ...values.processinginstructions,
              ])
            } else {
              setFieldValue('processinginstructions', [
                ...values.processinginstructions,
                value,
              ])
            }
          }

          return (
            <Form noValidate>
              <ScrollToFieldError errors={errors} />

              {isWholeBook && (
                <Note content="Fields containing metadata provided in source files for conversion and tagging are read-only and cannot be updated." />
              )}

              {show('coverSection') && (
                <FormSection label="Cover">
                  {applyCoverFromParent && appliedTag}

                  {show('cover') && (
                    <CoverComponent
                      data-test-id="cover"
                      disabled={!allow('cover')}
                      imageName={values.cover?.name}
                      imageUrl={values.cover?.url}
                      onChange={handleCoverChange}
                      placeholder={s('form.dropCover')}
                    />
                  )}

                  {show('applyCoverToBooks') && (
                    <Toggle
                      description={s(
                        'fieldDescriptions.metadataForm.applyCoverToBooks',
                      )}
                      disabled={!allow('applyCoverToBooks')}
                      field={{
                        name: 'applyCoverToBooks',
                        value: values.applyCoverToBooks,
                      }}
                      form={formProps}
                      label={s('form.applyCoverToBooks')}
                      locked
                      lockPosition="left"
                    />
                  )}
                </FormSection>
              )}

              {show('titleSection') && (
                <FormSection label={titleSectionLabel}>
                  {show('title') &&
                    (isWholeBook ? (
                      <TextField
                        disabled={!allow('title')}
                        field={{ name: 'title', value: values.title }}
                        form={formProps}
                        label={s('form.title')}
                        required={allow('title')}
                      />
                    ) : (
                      <Editor
                        disabled={!allow('title')}
                        editorType="inline"
                        field={{ name: 'title', value: values.title }}
                        form={formProps}
                        label={s('form.title')}
                        required={allow('title')}
                      />
                    ))}

                  {show('subtitle') &&
                    (isWholeBook ? (
                      (values.subtitle || []).map((subtitle, index) => (
                        <TextField
                          disabled={!allow('subtitle')}
                          field={{ name: 'subtitle', value: subtitle }}
                          form={formProps}
                          key={`wholeBook-subtitle-${subtitle}`}
                          label={`${s('form.subtitle')} ${index + 1}`}
                        />
                      ))
                    ) : (
                      <Editor
                        disabled={!allow('subtitle')}
                        editorType="inline"
                        field={{
                          name: 'subtitle',
                          value: values.subtitle,
                        }}
                        form={formProps}
                        key="chapterProcessed-subtitle"
                        label={`${s('form.subtitle')}`}
                      />
                    ))}

                  {show('alternativeTitle') &&
                    (isWholeBook ? (
                      <TextField
                        disabled={!allow('alternativeTitle')}
                        field={{
                          name: 'alternativeTitle',
                          value: values.alternativeTitle,
                        }}
                        form={formProps}
                        label={s('form.alternativeTitle')}
                      />
                    ) : (
                      <Editor
                        disabled={!allow('alternativeTitle')}
                        editorType="inline"
                        field={{
                          name: 'alternativeTitle',
                          value: values.alternativeTitle,
                        }}
                        form={formProps}
                        label={s('form.alternativeTitle')}
                      />
                    ))}

                  {show('edition') && (
                    <TextField
                      disabled={!allow('edition')}
                      field={{
                        name: 'edition',
                        value: values.edition,
                      }}
                      form={formProps}
                      label={s('form.edition')}
                    />
                  )}

                  {show('volume') && (
                    <TextField
                      disabled={!allow('volume')}
                      field={{
                        name: 'volume',
                        value: values.volume,
                      }}
                      form={formProps}
                      label={s('form.volume')}
                    />
                  )}

                  {/* {show('language') && (
                   <Select
                     disabled={!allow('language')}
                     field={{
                       name: 'language',
                       value: values.language,
                     }}
                     form={formProps}
                     label={s('form.language')}
                     options={languageOptions}
                     required
                   />
                 )} */}

                  {show('doi') && (
                    <TextField
                      disabled={!allow('doi')}
                      field={{
                        name: 'doi',
                        value: values.doi,
                      }}
                      form={formProps}
                      label={s('form.doi')}
                    />
                  )}

                  {show('isbn') && (
                    <TextField
                      disabled={!allow('isbn')}
                      field={{
                        name: 'isbn',
                        value: values.isbn,
                      }}
                      form={formProps}
                      label={s('form.isbn')}
                    />
                  )}

                  {show('bookSubmitId') && (
                    <TextField
                      disabled={!allow('bookSubmitId')}
                      field={{
                        name: 'bookSubmitId',
                        value: values.bookSubmitId,
                      }}
                      form={formProps}
                      label={s('form.bookSubmitId')}
                    />
                  )}

                  {show('objectId') && (
                    <TextField
                      disabled={!allow('objectId')}
                      field={{
                        name: 'objectId',
                        value: values.objectId,
                      }}
                      form={formProps}
                      label={s('form.objectId')}
                    />
                  )}

                  {show('domain') && (
                    <TextField
                      disabled={!allow('domain')}
                      field={{ name: 'domain', value: values.domain }}
                      form={formProps}
                      label={s('form.pmcDomainName')}
                    />
                  )}

                  {/* {show('nlmId') && (
                   <TextField
                     disabled={!allow('nlmId')}
                     field={{ name: 'nlmId', value: values.nlmId }}
                     form={formProps}
                     label={s('form.nlmIdName')}
                   />
                 )} */}

                  {show('publisherBookSeriesTitle') && (
                    <Editor
                      disabled={!allow('publisherBookSeriesTitle')}
                      editorType="inline"
                      field={{
                        name: 'publisherBookSeriesTitle',
                        value: values.publisherBookSeriesTitle,
                      }}
                      form={formProps}
                      label={s('form.publisherBookSeriesTitle')}
                    />
                  )}

                  {show('issn') && (
                    <TextField
                      disabled={!allow('issn')}
                      field={{ name: 'issn', value: values.issn }}
                      form={formProps}
                      label={
                        variant === 'collection'
                          ? s('form.collectionIssn')
                          : s('form.issn')
                      }
                    />
                  )}

                  {show('eissn') && (
                    <TextField
                      disabled={!allow('eissn')}
                      field={{ name: 'eissn', value: values.eissn }}
                      form={formProps}
                      label={s('form.eissn')}
                    />
                  )}

                  {show('abstractGraphic') && (
                    <CoverComponent
                      data-test-id="abstractgraphic"
                      disabled={!allow('abstractGraphic')}
                      imageName={values.abstractGraphic?.name}
                      imageUrl={values.abstractGraphic?.url}
                      label={s('form.abstractGraphic')}
                      onChange={handleAbstractChange}
                      placeholder={s('form.dropGraphic')}
                    />
                  )}

                  {show('abstract') && (
                    <Editor
                      disabled={!allow('abstract')}
                      editorType="abstract"
                      field={{
                        name: 'abstract',
                        value: values.abstract,
                      }}
                      form={formProps}
                      label={s('form.abstract')}
                    />
                  )}
                </FormSection>
              )}

              {show('publisherSection') && (
                <FormSection label="Publisher">
                  {applyPublisherFromParent && appliedTag}

                  {show('publisherName') && (
                    <TextField
                      disabled={!allow('publisherName')}
                      field={{
                        name: 'publisherName',
                        value: values.publisherName,
                      }}
                      form={formProps}
                      label={s('form.publisherName')}
                      required={allow('publisherName')}
                    />
                  )}

                  {show('publisherLocation') && (
                    <TextField
                      disabled={!allow('publisherLocation')}
                      field={{
                        name: 'publisherLocation',
                        value: values.publisherLocation,
                      }}
                      form={formProps}
                      label={s('form.publisherLocation')}
                      required={allow('publisherLocation')}
                    />
                  )}

                  {show('applyPublisherToBooks') && (
                    <Toggle
                      description={s(
                        'fieldDescriptions.metadataForm.applyPublisherToBooks',
                      )}
                      disabled={!allow('applyPublisherToBooks')}
                      field={{
                        name: 'applyPublisherToBooks',
                        value: values.applyPublisherToBooks,
                      }}
                      form={formProps}
                      label={s('form.applyPublisherToBooks')}
                      locked
                      lockPosition="left"
                    />
                  )}
                </FormSection>
              )}

              {show('publicationDateSection') && (
                <FormSection label="Publication date">
                  {show('publicationDateType') && (
                    <Select
                      data-test-id="publication-date-type-select"
                      disabled={!allow('publicationDateType')}
                      field={{
                        name: 'publicationDateType',
                        value: values.publicationDateType,
                      }}
                      form={formProps}
                      label={s('form.type')}
                      options={dateTypeOptions}
                      required={allow('publicationDateType')}
                    />
                  )}

                  {show('dateOfPublication') && values.publicationDateType && (
                    <DateInput
                      disabled={!allow('dateOfPublication')}
                      field={{
                        name: 'dateOfPublication',
                        value: values.dateOfPublication,
                      }}
                      form={formProps}
                      isDateRange={
                        values.publicationDateType?.split('-')[0] === 'pubr'
                      }
                      label={s('form.dateOfPublication')}
                      required={allow('dateOfPublication')}
                    />
                  )}
                </FormSection>
              )}

              {show('publicationHistorySection') && (
                <FormSection label="Publication history">
                  {show('dateCreated') && (
                    <DateInput
                      disabled={!allow('dateCreated')}
                      field={{
                        name: 'dateCreated',
                        value: values.dateCreated,
                      }}
                      form={formProps}
                      label={s('form.dateCreated')}
                    />
                  )}

                  {show('dateUpdated') && (
                    <DateInput
                      disabled={!allow('dateUpdated')}
                      field={{
                        name: 'dateUpdated',
                        value: values.dateUpdated,
                      }}
                      form={formProps}
                      label={s('form.dateUpdated')}
                    />
                  )}
                </FormSection>
              )}

              {show('customMetadataSection') && (
                <FormSection label="NCBI Custom metadata">
                  {show('bookSourceType') && (
                    <Select
                      data-test-id="bookSourceType-select"
                      disabled={!allow('bookSourceType')}
                      field={{
                        name: 'bookSourceType',
                        value: values.bookSourceType,
                      }}
                      form={formProps}
                      label={s('form.bookSourceType')}
                      options={sourceTypeOptions}
                      required
                    />
                  )}

                  {show('sourceType') && (
                    <TextField
                      disabled={!allow('sourceType')}
                      field={{
                        name: 'sourceType',
                        value: values.sourceType,
                      }}
                      form={formProps}
                      label={s('form.sourceType')}
                    />
                  )}

                  {show('wholeBookSourceType') && (
                    <Select
                      disabled={!allow('wholeBookSourceType')}
                      field={{
                        name: 'wholeBookSourceType',
                        value: values.wholeBookSourceType,
                      }}
                      form={formProps}
                      label={s('form.wholeBookSourceType')}
                      options={sourceTypeOptions}
                      required={allow('wholeBookSourceType')}
                    />
                  )}

                  {show('chapterProcessedSourceType') && (
                    <Select
                      disabled={!allow('chapterProcessedSourceType')}
                      field={{
                        name: 'chapterProcessedSourceType',
                        value: values.chapterProcessedSourceType,
                      }}
                      form={formProps}
                      label={s('form.chapterProcessedSourceType')}
                      options={sourceTypeOptions}
                      required={allow('chapterProcessedSourceType')}
                    />
                  )}
                </FormSection>
              )}

              {show('contributorsSection') && (
                <FormSection label="Contributors">
                  {show('seriesEditors') && (
                    <ContributorsComponent
                      affiliations={affiliations}
                      disabled={!allow('seriesEditors')}
                      name="seriesEditors"
                      onChange={val =>
                        handleClickContributors('seriesEditors', val)
                      }
                      setAffiliations={setAffiliations}
                      value={values.seriesEditors}
                      variant="seriesEditors"
                    />
                  )}

                  {show('collaborativeAuthors') && (
                    <ContributorsComponent
                      affiliations={affiliations}
                      disabled={!allow('collaborativeAuthors')}
                      name="collaborativeAuthors"
                      onChange={val =>
                        handleClickContributors('collaborativeAuthors', val)
                      }
                      setAffiliations={setAffiliations}
                      value={values.collaborativeAuthors}
                      variant="collaborativeAuthors"
                    />
                  )}

                  {show('editors') && (
                    <ContributorsComponent
                      affiliations={affiliations}
                      disabled={!allow('editors')}
                      name="editors"
                      onChange={val => handleClickContributors('editors', val)}
                      setAffiliations={setAffiliations}
                      value={values.editors}
                      variant="editors"
                    />
                  )}

                  {show('authors') && (
                    <ContributorsComponent
                      affiliations={affiliations}
                      disabled={!allow('authors')}
                      name="authors"
                      onChange={val => handleClickContributors('authors', val)}
                      setAffiliations={setAffiliations}
                      value={values.authors}
                      variant="authors"
                    />
                  )}
                </FormSection>
              )}

              {show('permissionsSection') && (
                <FormSection label="Permissions">
                  {applyPermissionsFromParent && appliedTag}

                  {show('copyrightStatement') && (
                    <Editor
                      disabled={isWholeBook || !allow('copyrightStatement')}
                      editorType="statement"
                      field={{
                        name: 'copyrightStatement',
                        value: values.copyrightStatement,
                      }}
                      form={formProps}
                      label={s('form.copyrightStatement')}
                      name="copyrightStatement"
                    />
                  )}

                  {show('openAccessLicense') && (
                    <Toggle
                      data-test-id="open-access-license-lock"
                      disabled={isWholeBook || !allow('openAccessLicense')}
                      field={{
                        name: 'openAccessLicense',
                        value: values.openAccessLicense,
                      }}
                      form={formProps}
                      label={s('form.openAccessLicense')}
                      locked
                      lockPosition="left"
                      onClick={handleOpenAccessChange}
                    />
                  )}

                  {show('licenseType') && (
                    <Select
                      disabled={isWholeBook || !allow('licenseType')}
                      field={{
                        name: 'licenseType',
                        value: values.licenseType,
                      }}
                      form={formProps}
                      label={s('form.licenseType')}
                      options={transformedLicenseTypeOptions}
                      required={values.openAccessLicense}
                    />
                  )}

                  {show('licenseStatement') && (
                    <Editor
                      disabled={isWholeBook || !allow('licenseStatement')}
                      editorType="licenseStatement"
                      field={{
                        name: 'licenseStatement',
                        value: values.licenseStatement,
                        required: values.openAccessLicense,
                      }}
                      form={formProps}
                      label={s('form.licenseStatement')}
                      name="licenseStatement"
                      required={values.openAccessLicense}
                    />
                  )}

                  {show('licenseUrl') && (
                    <TextField
                      disabled={
                        isWholeBook ||
                        !(
                          values.openAccessLicense &&
                          values.licenseType === 'other'
                        )
                      }
                      field={{
                        name: 'licenseUrl',
                        value: values.licenseUrl,
                        required: !(
                          values.openAccessLicense &&
                          values.licenseType === 'other'
                        ),
                      }}
                      form={formProps}
                      label={s('form.licenseUrl')}
                      name="licenseUrl"
                      required={
                        values.openAccessLicense &&
                        values.licenseType === 'other'
                      }
                    />
                  )}

                  {show('applyPermissions') && (
                    <Toggle
                      disabled={!allow('applyPermissions')}
                      field={{
                        name: 'applyPermissions',
                        value: values.applyPermissions,
                      }}
                      form={formProps}
                      label={s('form.applyToPDFBooksCollection')}
                      locked
                      lockPosition="left"
                    />
                  )}
                </FormSection>
              )}

              {show('notes') && (
                <FormSection label="notes">
                  {errors.notes && <Error>{errors.notes}</Error>}

                  <FieldArray
                    component={NoteEnhanced}
                    form={formProps}
                    name="notes"
                    readOnly={!allow('notes')}
                  />

                  <Button
                    data-test-id="add-notes"
                    disabled={readOnly}
                    icon="plus"
                    onClick={handleAddNote}
                    status="primary"
                  >
                    {values.notes?.length === 0
                      ? s('form.addNote')
                      : s('form.addAnotherNote')}
                  </Button>
                </FormSection>
              )}

              {show('funding') && (
                <FormSection label="funding">
                  <PermissionsGate
                    errorProps={{ disabled: true }}
                    scopes={['granthub-metadata-modal']}
                  >
                    <Granthub
                      awards={values.funding}
                      disabled={isSaving || readOnly}
                      isWholeBook={submissionType === 'wholeBook'}
                      name="funding"
                      onSearch={searchGranthub}
                      readOnly={!allow('funding')}
                      showApply={showGranthubApply}
                      variant={variant}
                    />
                  </PermissionsGate>
                </FormSection>
              )}

              {show('processinginstructions') && (
                <FormSection label="Processing instructions">
                  {processingInstructions.map(instruction => (
                    <PermissionsGate
                      errorProps={{ disabled: true }}
                      key={instruction}
                      scopes={['processing-instructions']}
                    >
                      <StyledCheckbox
                        checked={values.processinginstructions.includes(
                          instruction,
                        )}
                        label={instruction}
                        name={instruction}
                        onClick={() =>
                          handleProcessingInstructions(instruction)
                        }
                      />
                    </PermissionsGate>
                  ))}
                </FormSection>
              )}

              {show('pdfprocessinginstruction') && (
                <FormSection label="Processing instructions">
                  <PermissionsGate
                    errorProps={{ disabled: true }}
                    scopes={['processing-instructions']}
                  >
                    <StyledCheckbox
                      checked={values.processinginstructions.includes(
                        '<?pdfbuild-skip-alt-title-as-rhead?>',
                      )}
                      label="<?pdfbuild-skip-alt-title-as-rhead?>"
                      name="<?pdfbuild-skip-alt-title-as-rhead?>"
                      onClick={() =>
                        handleProcessingInstructions(
                          '<?pdfbuild-skip-alt-title-as-rhead?>',
                        )
                      }
                    />
                  </PermissionsGate>
                </FormSection>
              )}
              {showSubmitButton && (
                <ButtonWrapper>
                  <Button
                    disabled={isSaving}
                    loading={isSaving}
                    status="primary"
                    type="submit"
                  >
                    {submitButtonLabel}
                  </Button>
                </ButtonWrapper>
              )}
            </Form>
          )
        }}
      </Formik>
    </Wrapper>
  )
}

CollectionMetadataForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  searchGranthub: PropTypes.func.isRequired,
  variant: PropTypes.oneOf(['book', 'collection']).isRequired,

  applyCoverFromParent: PropTypes.bool,
  applyPermissionsFromParent: PropTypes.bool,
  applyPublisherFromParent: PropTypes.bool,
  conversionWorkflow: PropTypes.oneOf(['word', 'xml', 'pdf']),
  inCollection: PropTypes.bool,
  /* eslint-disable-next-line react/forbid-prop-types */
  initialFormValues: PropTypes.object,
  isBookSeriesCollection: PropTypes.bool,
  isFundedCollection: PropTypes.bool,
  isSaving: PropTypes.bool,
  showSubmitButton: PropTypes.bool,
  submissionType: PropTypes.oneOf(['chapterProcessed', 'wholeBook']),
  submitButtonLabel: PropTypes.string,
  readonly: PropTypes.bool,
}

CollectionMetadataForm.defaultProps = {
  applyCoverFromParent: false,
  applyPermissionsFromParent: false,
  applyPublisherFromParent: false,
  conversionWorkflow: null,
  inCollection: false,
  initialFormValues: {},
  isBookSeriesCollection: false,
  isFundedCollection: false,
  isSaving: false,
  showSubmitButton: false,
  submissionType: null,
  submitButtonLabel: 'Submit',
  readonly: false,
}

export default CollectionMetadataForm
