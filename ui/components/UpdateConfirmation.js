/* eslint-disable react/prop-types */
import React from 'react'

// eslint-disable-next-line import/no-cycle
import Confirm from './Confirm'

const UpdateConfirmation = ({
  confirmAction,
  setIsOpen,
  modalIsOpen,
  ...props
}) => (
  <Confirm
    confirmAction={confirmAction}
    hideModal={() => setIsOpen(false)}
    isOpen={modalIsOpen}
    // title={title}

    {...props}
  />
)

export default UpdateConfirmation
