/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/require-default-props */
/* eslint-disable react/prop-types */
import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { th } from '@pubsweet/ui-toolkit'

// #region styled-components
const Root = styled.nav`
  align-content: center;
  align-items: center;
  background: ${th('colorTextPlaceholder')};
  display: flex;
  justify-content: space-between;
  min-height: calc(${th('gridUnit')} * 6);
  padding: ${th('gridUnit')} calc(${th('gridUnit')} * 2);
`

const Section = styled.div`
  align-items: left;
  display: flex;
`

const Title = styled.div`
  align-items: center;
  color: ${th('colorText')};
  display: flex;
  flex-grow: 1;
  font-family: ${th('fontInterface')};
  font-size: 12pt;
  font-weight: 600;
  margin: 0;
`

const MenuItems = styled.div`
  display: flex;

  div {
    align-self: center;
  }

  div:nth-child(${props => props.pushRightAfter}) {
    margin-left: auto;
  }
`

const Item = styled.div`
  align-items: center;
  display: inline-flex;
  height: ${th('menuItemHeight')};
  padding: 0 ${props => (props.lastItem ? 0 : th('gridUnit'))};
`

// #endregion

const NavBar = ({ children, className, pushRightAfter, navComponents }) => (
  <Root className={className}>
    <Title>{children}</Title>
    <Section>
      <MenuItems pushRightAfter={pushRightAfter}>
        {navComponents &&
          navComponents.map((NavLinkComponent, index) => (
            <Item key={`nav_comp_${index + 1}`} {...NavLinkComponent.props}>
              {NavLinkComponent}
            </Item>
          ))}
      </MenuItems>
    </Section>
  </Root>
)

NavBar.propTypes = {
  children: PropTypes.node,
  navComponents: PropTypes.arrayOf(PropTypes.element),
}

export default NavBar
