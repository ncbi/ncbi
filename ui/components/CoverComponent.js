import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th, grid } from '@pubsweet/ui-toolkit'
import { useDropzone } from 'react-dropzone'

import { Button } from './common'
import Label from './common/Label'

const IMAGE_CONTAINER_HEIGHT = '154px'
const IMAGE_CONTAINER_WIDTH = '100px'
const BORDER_WIDTH = '2px'

// #region styled
const Wrapper = styled.div``

const Main = styled.div`
  display: flex;
`

const Dropzone = styled.div`
  align-items: center;
  border: ${BORDER_WIDTH} dashed
    ${props => (!props.isDragActive ? th('colorBorder') : th('colorPrimary'))};
  display: flex;
  height: ${IMAGE_CONTAINER_HEIGHT};
  justify-content: center;
  opacity: ${props => (props.disabled ? '0.3' : '1')};
  transition: border 0.2s ease-in-out;
  width: ${IMAGE_CONTAINER_WIDTH};
`

const Img = styled.img`
  max-height: calc(${IMAGE_CONTAINER_HEIGHT} - calc(${BORDER_WIDTH} * 2));
  width: calc(${IMAGE_CONTAINER_WIDTH} - calc(${BORDER_WIDTH} * 2));
`

const Placeholder = styled.span`
  color: ${th('colorBorder')};
  text-align: center;
`

const NoPreviewMessage = styled.span`
  text-align: center;
`

const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  margin-left: ${grid(3)};
`

const Filename = styled.div``
// #endregion styled

const acceptedImageFileTypes = [
  '.jpeg',
  '.jpg',
  '.png',
  '.tif',
  '.tiff',
  '.eps',
  '.pds',
]
// old list
// .jpeg,.jpg,.png,.gif,.tif,.tiff,.bmp,.eps,.raw, .cr2,.nef,.orf,.sr2

const CoverComponent = ({
  acceptedFileTypes,
  disabled,
  imageName,
  imageUrl,
  label,
  onChange,
  placeholder,
  'data-test-id': testId,
}) => {
  const [file, setFile] = useState(null)
  const [previewSuccess, setPreviewSuccess] = useState(true)

  useEffect(() => {
    if (imageUrl && imageUrl !== file?.url) {
      setFile({
        name: imageName,
        url: imageUrl,
      })
    }
  }, [imageUrl])

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: acceptedFileTypes,
    disabled,
    maxFiles: 1,
    onDrop: acceptedFiles => {
      const newFile = acceptedFiles[0]
      if (!newFile) return

      setFile(newFile)
      onChange(newFile)
    },
  })

  const { ref, ...rootProps } = getRootProps({ className: 'dropzone' })
  const inputProps = getInputProps()
  const preview = file && (file.url || URL.createObjectURL(file))

  const handleClickRemove = () => {
    setFile(null)
    onChange(null)
  }

  const handleNoPreview = () => {
    setPreviewSuccess(false)
  }

  return (
    <Wrapper>
      {label && <Label value={label} />}

      <Main>
        <Dropzone
          disabled={disabled}
          isDragActive={isDragActive}
          {...rootProps}
        >
          <input data-test-id={`upload${testId}`} {...inputProps} />

          {file && previewSuccess && (
            <Img alt="Cover image" onError={handleNoPreview} src={preview} />
          )}

          {file && !previewSuccess && (
            <NoPreviewMessage>No preview available</NoPreviewMessage>
          )}

          {!file && <Placeholder>{placeholder}</Placeholder>}
        </Dropzone>

        <ButtonsContainer>
          <Button
            data-test-id="upload-cover"
            disabled={disabled}
            icon="upload-cloud"
            outlined
            {...rootProps}
            status="primary"
          >
            Upload
          </Button>

          <Button
            disabled={disabled}
            icon="trash-2"
            onClick={handleClickRemove}
            outlined
            status="danger"
          >
            Remove
          </Button>
        </ButtonsContainer>
      </Main>

      <Filename data-test-id={`${testId}-filename`}>{file?.name}</Filename>
    </Wrapper>
  )
}

CoverComponent.propTypes = {
  acceptedFileTypes: PropTypes.arrayOf(PropTypes.string),
  disabled: PropTypes.bool,
  imageName: PropTypes.string,
  imageUrl: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
}

CoverComponent.defaultProps = {
  acceptedFileTypes: acceptedImageFileTypes,
  disabled: false,
  imageName: null,
  imageUrl: null,
  label: null,
  onChange: () => {},
  placeholder: 'Drop file here',
}

export default CoverComponent
