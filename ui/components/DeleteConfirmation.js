/* eslint-disable react/prop-types */
import React from 'react'
// eslint-disable-next-line import/no-cycle
import Confirm from './Confirm'

const DeleteConfirmation = ({
  title,
  confirmAction,
  setIsOpen,
  modalIsOpen,
}) => (
  <Confirm
    confirmAction={confirmAction}
    description="This action can't be reversed"
    hideModal={() => setIsOpen(false)}
    isOpen={modalIsOpen}
    title={title}
  />
)

export default DeleteConfirmation
