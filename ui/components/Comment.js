/* eslint-disable react/prop-types */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import moment from 'moment'
// eslint-disable-next-line import/no-cycle
import { Link } from '.'
import { getDownloadLink } from '../../app/utilsConfig'

const Wrapper = styled.div`
  background: ${th('colorBackground')};
  border: 1px solid ${th('colorInnerBorder')};
  border-radius: ${th('borderRadius')};
  display: flex;
  flex-direction: column;
  font-family: ${th('fontInterface')};
  margin-bottom: ${th('gridUnit')};
  padding: calc(${th('gridUnit')} * 2);
`

const Header = styled.div`
  display: flex;
  flex-direction: column;
`

const Title = styled.div`
  display: flex;
  margin-bottom: ${th('gridUnit')};
`

const Name = styled.span`
  font-size: 11pt;
  font-weight: 600;
`

const Default = styled.span`
  font-size: 11pt;
  margin-left: ${th('gridUnit')};
`

const Username = styled.span`
  color: ${th('colorText')};
  cursor: pointer;
  font-size: 11pt;
  margin-left: ${th('gridUnit')};
`

const Body = styled.div`
  display: flex;

  span.mention-tag {
    background-color: #b6d2f3;
    border-radius: 5px;
    color: ${th('colorPrimary')};
  }
`

const Files = styled.div`
  display: flex;
`

const File = styled.div`
  padding-left: 8px;
  padding-right: 8px;
`

const Comment = ({
  name,
  role,
  username,
  date,
  files,
  type,
  content,
  children,
}) => {
  if (type === 'bot') {
    return (
      <Wrapper>{`${content} by ${name} ${moment(date).fromNow()}`}</Wrapper>
    )
  }

  return (
    <Wrapper>
      <Header>
        <Title>
          <Name>{name}</Name>
          {role && <Default>({role})</Default>}
          {username && <Username>@{username}</Username>}
          {date && <Default> ∙ {moment(date).fromNow()}</Default>}
        </Title>
      </Header>
      <Body>{children}</Body>
      <Files>
        {files.map(fileVersion => (
          <File key={fileVersion.name}>
            <Link to={getDownloadLink(fileVersion.id)}>{fileVersion.name}</Link>
          </File>
        ))}
      </Files>
    </Wrapper>
  )
}

Comment.propTypes = {
  name: PropTypes.string,
  role: PropTypes.string,
  username: PropTypes.string,
  date: PropTypes.string,
  children: PropTypes.node,
}

Comment.defaultProps = {
  name: '',
  role: '',
  username: '',
  date: '',
  children: '',
}

export default Comment
