/* eslint-disable react/prop-types */
/* stylelint-disable no-descending-specificity */

import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Formik, Form } from 'formik'
import merge from 'lodash/merge'
import * as yup from 'yup'

import { grid } from '@pubsweet/ui-toolkit'

import {
  SelectComponent as Select,
  TextFieldComponent as UITextField,
  ToggleComponent as UIToggle,
} from './FormElements/FormikElements'

import Button from './common/Button'
import FormSection from './common/FormSection'

import ConstantsContext from '../../app/constantsContext'

// #region styled

const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 800px;
  padding-bottom: 50px;
  padding-top: 20px;

  > form > div:not(:last-child) {
    margin-bottom: ${grid(2)};
  }
`

const TextField = styled(UITextField)`
  margin-bottom: 0;
`

const Toggle = styled(UIToggle)`
  max-width: 400px;
`

const ToggleGroup = styled.div`
  > div:first-child {
    font-weight: bold;
    margin-bottom: ${grid(1)};
  }
`

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: ${grid(2)};
`

// #endregion styled

const handlePermissions = (
  key,
  roles,
  type,
  workflow,
  variant,
  contentType,
  inCollection,
) => {
  if (['__typename', 'requireApprovalByPreviewers'].includes(key)) {
    return false
  }

  const { isEditor, isOrgAdmin, isSysAdmin } = roles
  // const isNewBook = variant === 'newBook'
  const isWholeBook = type === 'wholeBook'
  const isChapterProcessed = type === 'chapterProcessed'

  const isWordWorkflow = workflow === 'word'

  const isAuthorManuscript = contentType === 'authorManuscript'

  const isEditorAndAbove = isEditor || isOrgAdmin || isSysAdmin

  const mapper = {
    addBodyToParts: isSysAdmin,
    bookLevelAffiliationStyle: isEditorAndAbove,
    bookLevelLinks: isEditorAndAbove,
    bookLevelLinksMarkdown: isEditorAndAbove,
    chapterLevelAffiliationStyle: isEditorAndAbove,
    citationSelfUrl: isEditorAndAbove,
    citationType: isEditorAndAbove,
    collection: isSysAdmin,
    contentType: false,
    conversionWorkflow: false,
    createBookLevelPdf: isSysAdmin,
    createChapterLevelPdf: isSysAdmin,
    createVersionLink: isEditorAndAbove,
    displayBookLevelPdf:
      isSysAdmin &&
      !(isChapterProcessed && (isWordWorkflow || isAuthorManuscript)),
    displayChapterLevelPdf:
      isSysAdmin &&
      !(isChapterProcessed && (isWordWorkflow || isAuthorManuscript)),
    displayHeadingLevel: isEditorAndAbove,
    displayObjectsLocation: isEditorAndAbove,
    footnotesDecimal: isEditorAndAbove,
    groupChaptersInParts: isSysAdmin,
    indexChaptersInPubmed: (isWholeBook || isChapterProcessed) && isSysAdmin,
    indexInPubmed: (isWholeBook || isChapterProcessed) && isSysAdmin,
    openAccessStatus: isSysAdmin,
    orderChaptersBy: isEditorAndAbove,
    publisher: isSysAdmin,
    publisherUrl: isEditorAndAbove,
    qaStatus: false,
    questionAnswerStyle: isEditorAndAbove,
    referenceListStyle: isEditorAndAbove,
    releaseStatus: false,
    requireApprovalByOrgAdminOrEditor:
      !inCollection && !isChapterProcessed && isEditorAndAbove,
    // requireApprovalByPreviewers: ,
    specialPublisherLinkText: isEditorAndAbove,
    specialPublisherLinkUrl: isEditorAndAbove,
    submissionType: false,
    supportMultiplePublishedVersions: isSysAdmin,
    tocAltTitle: isEditorAndAbove,
    tocChapterLevelDates: isEditorAndAbove,
    tocContributors: isEditorAndAbove,
    tocExpansionLevel: isEditorAndAbove,
    tocSubtitle: isEditorAndAbove,
    UKPMC: isSysAdmin,
    versionLinkText: isEditorAndAbove,
    versionLinkUri: isEditorAndAbove,
    xrefAnchorStyle: isEditorAndAbove,
  }

  if (!(key in mapper)) {
    console.error(
      `Book settings form permissions mapper: Requested key ${key} does not exist!`,
    )
  }

  return mapper[key]
}

const handleShow = (
  key,
  variant,
  type,
  workflow,
  inCollection,
  inFundedCollection,
  contentType,
  formValues,
) => {
  if (['__typename', 'requireApprovalByPreviewers'].includes(key)) {
    return false
  }

  const isTemplate = variant === 'template'
  const isNewBook = variant === 'newBook'
  const isBookSettings = variant === 'bookSettings'

  // const appliedType = (!isTemplate && type) || formValues.template
  // const isWholeBook = appliedType === 'wholeBook'
  // const isChapterProcessed = appliedType === 'chapterProcessed'
  const isWholeBook = type === 'wholeBook'
  const isChapterProcessed = type === 'chapterProcessed'

  const isWordWorkflow = workflow === 'word'

  const isAuthorManuscript =
    inCollection &&
    inFundedCollection &&
    (formValues.contentType === 'authorManuscript' ||
      contentType === 'authorManuscript')

  // show nothing in templates before you select one
  // if (isTemplate && !appliedType && key !== 'template') return false

  const mapper = {
    addBodyToParts: isChapterProcessed && formValues.groupChaptersInParts,
    bookLevelAffiliationStyle: isTemplate || isBookSettings,
    bookLevelLinks: isBookSettings,
    bookLevelLinksMarkdown: isBookSettings && formValues.bookLevelLinks,
    chapterLevelAffiliationStyle: isTemplate || isBookSettings,
    citationSelfUrl: isTemplate || isBookSettings,
    citationType:
      (isChapterProcessed || isWordWorkflow) &&
      (isTemplate || (isWordWorkflow && (isNewBook || isBookSettings))),
    collection: isBookSettings,
    contentType: isBookSettings && inCollection && inFundedCollection,
    conversionWorkflow: isBookSettings,
    createBookLevelPdf:
      isBookSettings &&
      (isChapterProcessed || isWordWorkflow) &&
      (isWordWorkflow || isAuthorManuscript),
    createChapterLevelPdf:
      isBookSettings &&
      isChapterProcessed &&
      (isWordWorkflow || isAuthorManuscript),
    createVersionLink: isBookSettings && isWholeBook,
    displayBookLevelPdf: isBookSettings,
    displayChapterLevelPdf:
      isBookSettings && (isWordWorkflow ? isChapterProcessed : true),
    displayHeadingLevel: true,
    displayObjectsLocation: isTemplate || isBookSettings,
    footnotesDecimal: isTemplate || isBookSettings,
    groupChaptersInParts: isChapterProcessed,
    indexChaptersInPubmed: isTemplate || isBookSettings,
    indexInPubmed: isTemplate || isBookSettings,
    openAccessStatus: isBookSettings,
    orderChaptersBy: isChapterProcessed,
    publisher: isBookSettings,
    publisherUrl: isTemplate || isBookSettings,
    // qaStatus: isBookSettings,
    qaStatus: false,
    questionAnswerStyle: isTemplate || isBookSettings,
    referenceListStyle: isTemplate || isBookSettings,
    // releaseStatus: isBookSettings,
    releaseStatus: false,
    requireApprovalByOrgAdminOrEditor:
      (isNewBook && !inCollection) || isBookSettings,
    // requireApprovalByPreviewers: (isNewBook && !inCollection) || isBookSettings,
    specialPublisherLinkText: isTemplate || isBookSettings,
    specialPublisherLinkUrl: isTemplate || isBookSettings,
    submissionType: isBookSettings,
    supportMultiplePublishedVersions: isChapterProcessed || isWordWorkflow,
    // template: isTemplate,
    tocAltTitle: isTemplate || isBookSettings,
    tocChapterLevelDates: true,
    tocContributors: true,
    tocExpansionLevel: isTemplate || isBookSettings,
    tocSubtitle: isTemplate || isBookSettings,
    UKPMC: isBookSettings,
    versionLinkText:
      isBookSettings && isWholeBook && formValues.createVersionLink,
    versionLinkUri:
      isBookSettings && isWholeBook && formValues.createVersionLink,
    xrefAnchorStyle: isTemplate || isBookSettings,

    // toggle groups
    displayMetadataOnTOC: true,
    requireApproval: (isNewBook && !inCollection) || isBookSettings,

    // sections
    contentSubmission: isBookSettings,
    publishingSettings: true,
    landingPageLinks: isTemplate || isBookSettings,
    landingPageDownloads: isBookSettings,
    landingPageTableOfContents: !(isWholeBook && isWordWorkflow),
    displayContent: isTemplate || isBookSettings,
    displayMetadata: isTemplate || isBookSettings,
    displayReferenceCitations:
      isTemplate || (isNewBook && isWordWorkflow) || isBookSettings,
    archivingAndUpdating: isBookSettings && isWholeBook,
  }

  if (typeof mapper[key] === 'undefined') {
    console.error(
      `Book settings form show mapper: Requested key ${key} does not exist!`,
    )
  }

  return mapper[key]
}

// const handleRequired = (key, variant, workflow) => {
//   const isNewBook = variant === 'newBook'
//   const isWord = workflow === 'word'

//   const rules = {
//     citationType: isNewBook && isWord,
//   }

//   const isRequired = rules[key] || false
//   return isRequired
// }

const defaultInitialValues = {
  addBodyToParts: false,
  bookLevelAffiliationStyle: null,
  bookLevelLinks: false,
  bookLevelLinksMarkdown: '',
  chapterLevelAffiliationStyle: null,
  citationSelfUrl: '',
  citationType: null,
  collection: null,
  contentType: null,
  conversionWorkflow: null,
  createBookLevelPdf: false,
  createChapterLevelPdf: false,
  createVersionLink: false,
  displayBookLevelPdf: false,
  displayChapterLevelPdf: false,
  displayHeadingLevel: null,
  displayObjectsLocation: null,
  footnotesDecimal: false,
  groupChaptersInParts: false,
  indexChaptersInPubmed: false,
  indexInPubmed: false,
  openAccessStatus: false,
  orderChaptersBy: null,
  publisher: null,
  publisherUrl: '',
  qaStatus: null,
  questionAnswerStyle: null,
  referenceListStyle: null,
  releaseStatus: false,
  requireApprovalByOrgAdminOrEditor: false,
  // requireApprovalByPreviewers: false,
  specialPublisherLinkText: '',
  specialPublisherLinkUrl: '',
  submissionType: null,
  supportMultiplePublishedVersions: false,
  // template: '',
  tocAltTitle: false,
  tocChapterLevelDates: false,
  tocContributors: false,
  tocExpansionLevel: null,
  tocSubtitle: false,
  UKPMC: false,
  versionLinkText: '',
  versionLinkUri: '',
  xrefAnchorStyle: null,
}

const makeValidations = (initialFormValues, showHandler) => {
  //  function to validate value of toc Expansion Level
  const validateTocExpansionLevel = row => {
    const shouldMakeValidations = showHandler(
      'tocExpansionLevel',
      initialFormValues,
    )

    if (shouldMakeValidations) return !!row
    return true
  }

  const regMatch = /^((http|https):\/\/)/
  const doiRegMatch = /^((http|https):\/\/)|^doi$/

  // list of validations for the form
  const possibleValidations = {
    publisherUrl: yup
      .string()
      .matches(regMatch, 'URL should start with https:// or http://'),
    citationSelfUrl: yup
      .string()
      .matches(
        doiRegMatch,
        'Value should be doi or start with https:// and http://',
      ),
    specialPublisherLinkUrl: yup
      .string()
      .matches(
        doiRegMatch,
        'Value should be doi or start with https:// and http://',
      ),
    citationType: yup.string().when('conversionWorkflow', {
      is: 'word',
      then: yup
        .string()
        .nullable()
        .required('Citation style is required for the word workflow'),
      otherwise: yup.string().nullable(),
    }),
    displayHeadingLevel:
      !initialFormValues.displayHeadingLevel === '' &&
      yup.string().nullable().required('Display heading level is required'),
    tocExpansionLevel:
      !initialFormValues.tocExpansionLevel === '' &&
      yup
        .string()
        .nullable()
        .test(
          'collaborative-authors-test',
          'Expand heading level is required',
          validateTocExpansionLevel,
        ),
    // .required(''),
  }

  const relevantValidations = Object.keys(possibleValidations)
    .filter(key => !!possibleValidations[key])
    .reduce(
      (newObj, key) =>
        Object.assign(newObj, { [key]: possibleValidations[key] }),
      {},
    )

  return yup.object().shape(relevantValidations)
}

const BookSettings = props => {
  const {
    className,
    collectionOptions,
    inCollection,
    inFundedCollection,
    initialFormValues,
    innerRef,
    isEditor,
    isOrgAdmin,
    isSaving,
    isSysAdmin,
    onSubmit,
    publisherOptions,
    qaStatusOptions,
    showSubmitButton,
    submitButtonLabel,
    type,
    variant,
    workflow,
    readonly,
  } = props

  const isWordWorkflow = workflow === 'word'

  const { s } = useContext(ConstantsContext)
  const getDescription = key => s(`fieldDescriptions.bookSettingsForm.${key}`)
  const initialValues = merge({}, defaultInitialValues, initialFormValues)
  const { contentType } = initialValues

  const showHandler = (key, formValues) =>
    handleShow(
      key,
      variant,
      type,
      workflow,
      inCollection,
      inFundedCollection,
      contentType,
      formValues,
    )

  const validations = makeValidations(initialFormValues, showHandler)

  const allowed = key => {
    if (readonly) return false
    // disable all fields if form is being saved
    if (isSaving) return false

    return handlePermissions(
      key,
      { isEditor, isOrgAdmin, isSysAdmin },
      type,
      workflow,
      variant,
      contentType,
      inCollection,
    )
  }

  // const isRequired = key => handleRequired(key, variant, workflow)

  // Only send outside the values that are relevant in the current scenario
  const handleSubmit = values => {
    const submittedValues = Object.keys(values)
      .filter(key => showHandler(key, values))
      .reduce((obj, key) => {
        /* eslint-disable-next-line no-param-reassign */
        obj[key] = values[key]
        return obj
      }, {})

    onSubmit && onSubmit(submittedValues)
  }

  const bindPdfSettings =
    variant === 'bookSettings' &&
    type === 'chapterProcessed' &&
    (workflow === 'word' || contentType === 'authorManuscript')

  const displayHeadingOptions = [
    {
      value: '1',
      label: 1,
    },
    {
      value: '2',
      label: 2,
    },
    {
      value: '3',
      label: 3,
    },
    {
      value: '4',
      label: 4,
    },
    {
      value: '5',
      label: 5,
    },
    {
      value: '99',
      label: 99,
    },
    {
      value: '1:nojava',
      label: '1:nojava',
    },
    {
      value: '2:nojava',
      label: '2:nojava',
    },
    {
      value: '3:nojava',
      label: '3:nojava',
    },
    {
      value: ' ',
      label: '<blank>',
    },
  ]

  return (
    <Wrapper className={className}>
      <Formik
        initialValues={initialValues}
        innerRef={innerRef}
        onSubmit={handleSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const { dirty, setFieldValue, values } = formProps

          const {
            // addBodyToParts,
            bookLevelAffiliationStyle,
            bookLevelLinks,
            bookLevelLinksMarkdown,
            chapterLevelAffiliationStyle,
            citationSelfUrl,
            citationType,
            collection,
            // eslint-disable-next-line no-shadow
            contentType,
            conversionWorkflow,
            createBookLevelPdf,
            createChapterLevelPdf,
            createVersionLink,
            displayBookLevelPdf,
            displayChapterLevelPdf,
            displayHeadingLevel,
            displayObjectsLocation,
            footnotesDecimal,
            groupChaptersInParts,
            indexChaptersInPubmed,
            indexInPubmed,
            orderChaptersBy,
            openAccessStatus,
            publisher,
            publisherUrl,
            qaStatus,
            questionAnswerStyle,
            referenceListStyle,
            releaseStatus,
            requireApprovalByOrgAdminOrEditor,
            // requireApprovalByPreviewers,
            specialPublisherLinkText,
            specialPublisherLinkUrl,
            submissionType,
            supportMultiplePublishedVersions,
            tocAltTitle,
            tocChapterLevelDates,
            tocContributors,
            tocExpansionLevel,
            tocSubtitle,
            UKPMC,
            versionLinkText,
            versionLinkUri,
            xrefAnchorStyle,
          } = values

          const show = key => showHandler(key, values)

          const handleCreateBookLevelPdfChange = value => {
            if (bindPdfSettings) setFieldValue('displayBookLevelPdf', value)
          }

          const handleCreateChapterLevelPdfChange = value => {
            if (bindPdfSettings) setFieldValue('displayChapterLevelPdf', value)
          }

          return (
            <Form>
              {show('contentSubmission') && (
                <FormSection label="Content submission">
                  {show('collection') && (
                    <Select
                      data-test-id="collection-select"
                      description={getDescription('collection')}
                      disabled={!allowed('collection')}
                      field={{ name: 'collection', value: collection }}
                      form={formProps}
                      hasHtmlLabels
                      isClearable
                      isSearchable={false}
                      label="Collection"
                      options={collectionOptions}
                    />
                  )}
                  {
                    <Select
                      description={getDescription('contentType')}
                      disabled={!allowed('contentType')}
                      field={{
                        name: 'contentType',
                        value: contentType,
                      }}
                      form={formProps}
                      isSearchable={false}
                      label="Content type"
                      options={[
                        {
                          label: 'Author manuscript',
                          value: 'authorManuscript',
                        },
                        {
                          label: 'Published PDF',
                          value: 'publishedPDF',
                        },
                        {
                          label: 'Prepublication draft',
                          value: 'prepublicationDraft',
                        },
                        {
                          label: 'Final full-text',
                          value: 'finalFullText',
                        },
                      ]}
                    />
                  }
                  {show('conversionWorkflow') && (
                    <Select
                      description={getDescription('conversionWorkflow')}
                      disabled={!allowed('conversionWorkflow')}
                      field={{
                        name: 'conversionWorkflow',
                        value: conversionWorkflow,
                      }}
                      form={formProps}
                      isSearchable={false}
                      label={s('book.conversionWorkflow.name')}
                      options={[
                        {
                          value: 'word',
                          label: s('book.conversionWorkflow.word'),
                        },
                        {
                          value: 'xml',
                          label: s('book.conversionWorkflow.xml'),
                        },
                        {
                          value: 'pdf',
                          label: s('book.conversionWorkflow.pdf'),
                        },
                      ]}
                    />
                  )}

                  {show('submissionType') && (
                    <Select
                      description={getDescription('submissionType')}
                      disabled={!allowed('submissionType')}
                      field={{ name: 'submissionType', value: submissionType }}
                      form={formProps}
                      isSearchable={false}
                      label={s('book.submissionType.name')}
                      options={[
                        {
                          value: 'wholeBook',
                          label: s('book.submissionType.wholeBook'),
                        },
                        {
                          value: 'chapterProcessed',
                          label: s('book.submissionType.chapterProcessed'),
                        },
                      ]}
                    />
                  )}

                  {show('contentType') && (
                    <Select
                      description={getDescription('contentType')}
                      disabled={!allowed('contentType')}
                      field={{ name: 'contentType', value: contentType }}
                      form={formProps}
                      isSearchable={false}
                      label={s('book.contentType.name')}
                      options={[
                        {
                          value: 'authorManuscript',
                          label: s('book.contentType.authorManuscript'),
                        },
                        {
                          value: 'publishedPDF',
                          label: s('book.contentType.publishedPDF'),
                        },
                        {
                          value: 'prepublicationDraft',
                          label: s('book.contentType.prepublicationDraft'),
                        },
                      ]}
                    />
                  )}
                </FormSection>
              )}

              {show('publishingSettings') && (
                <FormSection label="Publishing settings">
                  {show('requireApproval') && (
                    <ToggleGroup>
                      <div>Require approval before publishing</div>

                      <div>
                        {show('requireApprovalByOrgAdminOrEditor') && (
                          <Toggle
                            data-test-id="org-admin-or-editor-lock"
                            description={getDescription(
                              'requireApprovalByOrgAdminOrEditor',
                            )}
                            disabled={
                              !allowed('requireApprovalByOrgAdminOrEditor')
                            }
                            field={{
                              name: 'requireApprovalByOrgAdminOrEditor',
                              value: requireApprovalByOrgAdminOrEditor,
                            }}
                            form={formProps}
                            label="Org Admin or Editor"
                            labelPosition="left"
                            locked={isWordWorkflow ? null : true}
                            lockPosition="left"
                          />
                        )}

                        {/* {show('requireApprovalByPreviewers') && (
                          <Toggle
                            description={getDescription('requireApprovalByPreviewers')}
                            disabled
                            field={{
                              name: 'requireApprovalByPreviewers',
                              // value: requireApprovalByPreviewers,
                              value: false,
                            }}
                            form={formProps}
                            label="Previewers"
                            labelPosition="left"
                            // locked
                            // lockPosition="left"
                          />
                        )} */}
                      </div>
                    </ToggleGroup>
                  )}

                  {show('publisher') && (
                    <Select
                      disabled={!allowed('publisher')}
                      field={{ name: 'publisher', value: publisher }}
                      form={formProps}
                      isSearchable={false}
                      label="Publisher"
                      options={publisherOptions}
                    />
                  )}

                  {show('releaseStatus') && (
                    <Toggle
                      description={getDescription('releaseStatus')}
                      disabled={!allowed('releaseStatus')}
                      field={{
                        name: 'releaseStatus',
                        value: releaseStatus,
                      }}
                      form={formProps}
                      label="Release status"
                      labelPosition="left"
                    />
                  )}

                  {show('qaStatus') && (
                    <Select
                      disabled={!allowed('qaStatus')}
                      field={{ name: 'qaStatus', value: qaStatus }}
                      form={formProps}
                      isSearchable={false}
                      label="QA status"
                      options={qaStatusOptions}
                    />
                  )}

                  {show('openAccessStatus') && (
                    <Toggle
                      data-test-id="open-access-status-lock"
                      description={getDescription('openAccessStatus')}
                      disabled={!allowed('openAccessStatus')}
                      field={{
                        name: 'openAccessStatus',
                        value: openAccessStatus,
                      }}
                      form={formProps}
                      label="Open access status"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                    />
                  )}

                  {show('UKPMC') && (
                    <Toggle
                      data-test-id="ukpmc-lock"
                      description={getDescription('UKPMC')}
                      disabled={!allowed('UKPMC')}
                      field={{
                        name: 'UKPMC',
                        value: UKPMC,
                      }}
                      form={formProps}
                      label="UKPMC"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                    />
                  )}

                  {show('supportMultiplePublishedVersions') && (
                    <Toggle
                      data-test-id="support-multiple-published-versions-lock"
                      description={getDescription(
                        'supportMultiplePublishedVersions',
                      )}
                      disabled={!allowed('supportMultiplePublishedVersions')}
                      field={{
                        name: 'supportMultiplePublishedVersions',
                        value: supportMultiplePublishedVersions,
                      }}
                      form={formProps}
                      label="Support multiple published versions"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                    />
                  )}

                  {show('indexInPubmed') && (
                    <Toggle
                      data-test-id="index-book-in-pubmed-lock"
                      description={getDescription('indexInPubmed')}
                      disabled={!allowed('indexInPubmed')}
                      field={{
                        name: 'indexInPubmed',
                        value: indexInPubmed,
                      }}
                      form={formProps}
                      label="Index book in PubMed"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                    />
                  )}

                  {show('indexChaptersInPubmed') && (
                    <Toggle
                      data-test-id="index-chapters-in-pubmed-lock"
                      description={getDescription('indexChaptersInPubmed')}
                      disabled={!allowed('indexChaptersInPubmed')}
                      field={{
                        name: 'indexChaptersInPubmed',
                        value: indexChaptersInPubmed,
                      }}
                      form={formProps}
                      label="Index chapters in PubMed"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                    />
                  )}
                </FormSection>
              )}

              {show('landingPageLinks') && (
                <FormSection label="Landing Page: Links">
                  {show('publisherUrl') && (
                    <TextField
                      description={getDescription('publisherUrl')}
                      disabled={!allowed('publisherUrl')}
                      field={{ name: 'publisherUrl', value: publisherUrl }}
                      form={formProps}
                      label="Publisher URL"
                    />
                  )}

                  {show('citationSelfUrl') && (
                    <TextField
                      description={getDescription('citationSelfUrl')}
                      disabled={!allowed('citationSelfUrl')}
                      field={{
                        name: 'citationSelfUrl',
                        value: citationSelfUrl,
                      }}
                      form={formProps}
                      label="Self-citation URL"
                    />
                  )}

                  {show('specialPublisherLinkUrl') && (
                    <TextField
                      description={getDescription('specialPublisherLinkUrl')}
                      disabled={!allowed('specialPublisherLinkUrl')}
                      field={{
                        name: 'specialPublisherLinkUrl',
                        value: specialPublisherLinkUrl,
                      }}
                      form={formProps}
                      label="Special Publisher Link URL"
                    />
                  )}

                  {show('specialPublisherLinkText') && (
                    <TextField
                      description={getDescription('specialPublisherLinkText')}
                      disabled={!allowed('specialPublisherLinkText')}
                      field={{
                        name: 'specialPublisherLinkText',
                        value: specialPublisherLinkText,
                      }}
                      form={formProps}
                      label="Special Publisher Link Text"
                    />
                  )}

                  {show('bookLevelLinks') && (
                    <Toggle
                      description={getDescription('bookLevelLinks')}
                      disabled={!allowed('bookLevelLinks')}
                      field={{
                        name: 'bookLevelLinks',
                        value: bookLevelLinks,
                      }}
                      form={formProps}
                      label="Book-level links"
                      labelPosition="left"
                    />
                  )}

                  {show('bookLevelLinksMarkdown') && (
                    <TextField
                      description={getDescription('bookLevelLinksMarkdown')}
                      disabled={!allowed('bookLevelLinksMarkdown')}
                      field={{
                        name: 'bookLevelLinksMarkdown',
                        value: bookLevelLinksMarkdown,
                      }}
                      form={formProps}
                      label="Book-level links markdown"
                    />
                  )}
                </FormSection>
              )}

              {show('landingPageDownloads') && (
                <FormSection label="Landing Page: Downloads">
                  {show('createBookLevelPdf') && (
                    <Toggle
                      data-test-id="create-book-level-pdf-for-download-lock"
                      description={getDescription('createBookLevelPdf')}
                      disabled={!allowed('createBookLevelPdf')}
                      field={{
                        name: 'createBookLevelPdf',
                        value: createBookLevelPdf,
                      }}
                      form={formProps}
                      label="Create book-level PDF for download"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                      onClick={handleCreateBookLevelPdfChange}
                    />
                  )}

                  {show('displayBookLevelPdf') && (
                    <Toggle
                      description={getDescription('displayBookLevelPdf')}
                      disabled={!allowed('displayBookLevelPdf')}
                      field={{
                        name: 'displayBookLevelPdf',
                        value: displayBookLevelPdf,
                      }}
                      form={formProps}
                      label="Display book-level PDF for download"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                    />
                  )}

                  {show('createChapterLevelPdf') && (
                    <Toggle
                      data-test-id="create-chapter-level-pdf-for-download-lock"
                      description={getDescription('createChapterLevelPdf')}
                      disabled={!allowed('createChapterLevelPdf')}
                      field={{
                        name: 'createChapterLevelPdf',
                        value: createChapterLevelPdf,
                      }}
                      form={formProps}
                      label="Create chapter-level PDF for download"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                      onClick={handleCreateChapterLevelPdfChange}
                    />
                  )}

                  {show('displayChapterLevelPdf') && (
                    <Toggle
                      description={getDescription('displayChapterLevelPdf')}
                      disabled={!allowed('displayChapterLevelPdf')}
                      field={{
                        name: 'displayChapterLevelPdf',
                        value: displayChapterLevelPdf,
                      }}
                      form={formProps}
                      label="Display chapter-level PDF for download"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                    />
                  )}
                </FormSection>
              )}

              {show('landingPageTableOfContents') && (
                <FormSection label="Landing Page: Table of Contents">
                  {show('groupChaptersInParts') && (
                    <Toggle
                      data-test-id="group-chapters-into-parts-lock"
                      description={getDescription('groupChaptersInParts')}
                      disabled={!allowed('groupChaptersInParts')}
                      field={{
                        name: 'groupChaptersInParts',
                        value: groupChaptersInParts,
                      }}
                      form={formProps}
                      label="Group chapters into parts"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                    />
                  )}

                  {/* {show('addBodyToParts') && (
                    <Toggle
                      description={getDescription('addBodyToParts')}
                      disabled={!allowed('addBodyToParts')}
                      field={{
                        name: 'addBodyToParts',
                        value: addBodyToParts,
                      }}
                      form={formProps}
                      label="Add body content to parts"
                      labelPosition="left"
                      locked
                      lockPosition="left"
                    />
                  )} */}

                  {show('orderChaptersBy') && (
                    <Select
                      data-test-id="order-chapters-select"
                      description={getDescription('orderChaptersBy')}
                      disabled={!allowed('orderChaptersBy')}
                      field={{
                        name: 'orderChaptersBy',
                        value: orderChaptersBy,
                      }}
                      form={formProps}
                      isSearchable={false}
                      label={s('book.orderChaptersBy.name')}
                      options={[
                        {
                          value: 'manual',
                          label: s('book.orderChaptersBy.manual'),
                        },
                        {
                          value: 'title',
                          label: s('book.orderChaptersBy.title'),
                        },
                        {
                          value: 'number',
                          label: s('book.orderChaptersBy.chapterNumber'),
                        },
                        {
                          value: 'number_desc',
                          label: s('book.orderChaptersBy.chapterNumberDesc'),
                        },
                        {
                          value: 'date_desc',
                          label: s('book.orderChaptersBy.dateDesc'),
                        },
                        // {
                        //   value: 'created',
                        //   label: s('book.orderChaptersBy.dateCreated'),
                        // },
                        // {
                        //   value: 'updated',
                        //   label: s('book.orderChaptersBy.dateUpdated'),
                        // },
                      ]}
                    />
                  )}

                  {show('displayHeadingLevel') && (
                    <Select
                      data-test-id="displayHeadingLevel"
                      description={getDescription('displayHeadingLevel')}
                      disabled={!allowed('displayHeadingLevel')}
                      field={{
                        name: 'displayHeadingLevel',
                        value:
                          displayHeadingLevel !== ''
                            ? displayHeadingLevel
                            : ' ',
                      }}
                      form={formProps}
                      label="Display heading level"
                      options={displayHeadingOptions}
                      required
                    />
                  )}

                  {show('tocExpansionLevel') && (
                    <Select
                      data-test-id="tocExpansionLevel"
                      description={getDescription('tocExpansionLevel')}
                      disabled={!allowed('tocExpansionLevel')}
                      field={{
                        name: 'tocExpansionLevel',
                        value:
                          tocExpansionLevel !== '' ? tocExpansionLevel : ' ',
                      }}
                      form={formProps}
                      label="Expand heading level"
                      options={[
                        {
                          value: '1',
                          label: 1,
                        },
                        {
                          value: '2',
                          label: 2,
                        },
                        {
                          value: '3',
                          label: 3,
                        },
                        {
                          value: '4',
                          label: 4,
                        },
                        {
                          value: ' ',
                          label: '<blank>',
                        },
                      ]}
                      required
                    />
                  )}

                  {show('displayMetadataOnTOC') && (
                    <ToggleGroup>
                      <div>Display metadata on TOC</div>

                      <div>
                        {show('tocContributors') && (
                          <Toggle
                            description={getDescription('tocContributors')}
                            disabled={!allowed('tocContributors')}
                            field={{
                              name: 'tocContributors',
                              value: tocContributors,
                            }}
                            form={formProps}
                            label="Contributors"
                            labelPosition="left"
                          />
                        )}

                        {show('tocChapterLevelDates') && (
                          <Toggle
                            description={getDescription('tocChapterLevelDates')}
                            disabled={!allowed('tocChapterLevelDates')}
                            field={{
                              name: 'tocChapterLevelDates',
                              value: tocChapterLevelDates,
                            }}
                            form={formProps}
                            label="Chapter-level dates"
                            labelPosition="left"
                          />
                        )}

                        {show('tocSubtitle') && (
                          <Toggle
                            description={getDescription('tocSubtitle')}
                            disabled={!allowed('tocSubtitle')}
                            field={{
                              name: 'tocSubtitle',
                              value: tocSubtitle,
                            }}
                            form={formProps}
                            label="Subtitle"
                            labelPosition="left"
                          />
                        )}

                        {show('tocAltTitle') && (
                          <Toggle
                            description={getDescription('tocAltTitle')}
                            disabled={!allowed('tocAltTitle')}
                            field={{
                              name: 'tocAltTitle',
                              value: tocAltTitle,
                            }}
                            form={formProps}
                            label="Alternative title"
                            labelPosition="left"
                          />
                        )}
                      </div>
                    </ToggleGroup>
                  )}
                </FormSection>
              )}

              {show('displayContent') && (
                <FormSection label="Display: Content">
                  {show('footnotesDecimal') && (
                    <Toggle
                      description={getDescription('footnotesDecimal')}
                      disabled={!allowed('footnotesDecimal')}
                      field={{
                        name: 'footnotesDecimal',
                        value: footnotesDecimal,
                      }}
                      form={formProps}
                      label="Display footnotes in decimal style"
                      labelPosition="left"
                    />
                  )}

                  {show('questionAnswerStyle') && (
                    <Select
                      data-test-id="question-answer-style-select"
                      description={getDescription('questionAnswerStyle')}
                      disabled={!allowed('questionAnswerStyle')}
                      field={{
                        name: 'questionAnswerStyle',
                        value: questionAnswerStyle,
                      }}
                      form={formProps}
                      isSearchable={false}
                      label={s('book.questionAnswerStyle.name')}
                      options={[
                        {
                          value: 'faq',
                          label: s('book.questionAnswerStyle.faq'),
                        },
                        {
                          value: 'faq-with-toc',
                          label: s('book.questionAnswerStyle.faq-with-toc'),
                        },
                        {
                          value: 'normal',
                          label: s('book.questionAnswerStyle.normal'),
                        },
                      ]}
                    />
                  )}

                  {show('displayObjectsLocation') && (
                    <Select
                      data-test-id="display-objects-location-select"
                      description={getDescription('displayObjectsLocation')}
                      disabled={!allowed('displayObjectsLocation')}
                      field={{
                        name: 'displayObjectsLocation',
                        value: displayObjectsLocation,
                      }}
                      form={formProps}
                      isSearchable={false}
                      label={s('book.displayObjectsLocation.name')}
                      options={[
                        {
                          value: 'at-xref',
                          label: s('book.displayObjectsLocation.at-xref'),
                        },
                        {
                          value: 'in-place',
                          label: s('book.displayObjectsLocation.in-place'),
                        },
                      ]}
                    />
                  )}
                </FormSection>
              )}

              {show('displayMetadata') && (
                <FormSection label="Display: Metadata">
                  {show('chapterLevelAffiliationStyle') && (
                    <Select
                      data-test-id="chapter-level-affiliation-select"
                      description={getDescription(
                        'chapterLevelAffiliationStyle',
                      )}
                      disabled={!allowed('chapterLevelAffiliationStyle')}
                      field={{
                        name: 'chapterLevelAffiliationStyle',
                        value: chapterLevelAffiliationStyle,
                      }}
                      form={formProps}
                      isSearchable={false}
                      label={s('book.chapterLevelAffiliationStyle.name')}
                      options={[
                        {
                          value: 'at-end',
                          label: s('book.bookLevelAffiliationStyle.at-end'),
                        },
                        {
                          value: 'by-contrib',
                          label: s('book.bookLevelAffiliationStyle.by-contrib'),
                        },
                      ]}
                    />
                  )}

                  {show('bookLevelAffiliationStyle') && (
                    <Select
                      data-test-id="book-level-affiliation-select"
                      description={getDescription('bookLevelAffiliationStyle')}
                      disabled={!allowed('bookLevelAffiliationStyle')}
                      field={{
                        name: 'bookLevelAffiliationStyle',
                        value: bookLevelAffiliationStyle,
                      }}
                      form={formProps}
                      isSearchable={false}
                      label={s('book.bookLevelAffiliationStyle.name')}
                      options={[
                        {
                          value: 'by-contrib',
                          label: s('book.bookLevelAffiliationStyle.by-contrib'),
                        },
                        {
                          value: 'at-end',
                          label: s('book.bookLevelAffiliationStyle.at-end'),
                        },
                      ]}
                    />
                  )}
                </FormSection>
              )}

              {show('displayReferenceCitations') && (
                <FormSection label="Display: Reference Citations">
                  {show('citationType') && (
                    <Select
                      data-test-id="citiation-select"
                      description={getDescription('citationType')}
                      disabled={!allowed('citationType')}
                      field={{
                        name: 'citationType',
                        value: citationType,
                      }}
                      form={formProps}
                      isSearchable={false}
                      label={s('book.citationType.name')}
                      // required={isRequired('citationType')}
                      options={[
                        {
                          value: 'harvard',
                          label: s('book.citationType.harvard'),
                        },
                        {
                          value: 'numberedSquare',
                          label: s('book.citationType.numberedSquare'),
                        },
                        {
                          value: 'numberedParentheses',
                          label: s('book.citationType.numberedParentheses'),
                        },
                        {
                          value: 'harvardNumberedSquare',
                          label: s('book.citationType.harvardNumberedSquare'),
                        },
                        {
                          value: 'harvardNumberedParentheses',
                          label: s(
                            'book.citationType.harvardNumberedParentheses',
                          ),
                        },
                      ]}
                    />
                  )}

                  {show('referenceListStyle') && (
                    <Select
                      data-test-id="reference-list-select"
                      description={getDescription('referenceListStyle')}
                      disabled={!allowed('referenceListStyle')}
                      field={{
                        name: 'referenceListStyle',
                        value: referenceListStyle,
                      }}
                      form={formProps}
                      isSearchable={false}
                      label={s('book.referenceListStyle.name')}
                      options={[
                        {
                          value: 'none',
                          label: s('book.referenceListStyle.none'),
                        },
                        {
                          value: 'disc',
                          label: s('book.referenceListStyle.disc'),
                        },
                        {
                          value: 'circle',
                          label: s('book.referenceListStyle.circle'),
                        },
                        {
                          value: 'square',
                          label: s('book.referenceListStyle.square'),
                        },
                        {
                          value: 'decimal',
                          label: s('book.referenceListStyle.decimal'),
                        },
                        {
                          value: 'lower-roman',
                          label: s('book.referenceListStyle.lower-roman'),
                        },
                        {
                          value: 'upper-roman',
                          label: s('book.referenceListStyle.upper-roman'),
                        },
                        {
                          value: 'lower-alpha',
                          label: s('book.referenceListStyle.lower-alpha'),
                        },
                        {
                          value: 'upper-alpha',
                          label: s('book.referenceListStyle.upper-alpha'),
                        },
                        {
                          value: 'use-source-label',
                          label: s('book.referenceListStyle.use-source-label'),
                        },
                        {
                          value: '1st-line-shifted-left',
                          label: s(
                            'book.referenceListStyle.1st-line-shifted-left',
                          ),
                        },
                      ]}
                    />
                  )}

                  {show('xrefAnchorStyle') && (
                    <Select
                      data-test-id="xref-anchor-select"
                      description={getDescription('xrefAnchorStyle')}
                      disabled={!allowed('xrefAnchorStyle')}
                      field={{
                        name: 'xrefAnchorStyle',
                        value: xrefAnchorStyle,
                      }}
                      form={formProps}
                      isSearchable={false}
                      label={s('book.xrefAnchorStyle.name')}
                      options={[
                        {
                          value: 'blue-triangle',
                          label: s('book.xrefAnchorStyle.blue-triangle'),
                        },
                        {
                          value: 'superscripted',
                          label: s('book.xrefAnchorStyle.superscripted'),
                        },
                        {
                          value: 'default',
                          label: s('book.xrefAnchorStyle.default'),
                        },
                        {
                          value: 'autodetect',
                          label: s('book.xrefAnchorStyle.autodetect'),
                        },
                      ]}
                    />
                  )}
                </FormSection>
              )}

              {show('archivingAndUpdating') && (
                <FormSection label="Archiving and Updating">
                  {show('createVersionLink') && (
                    <Toggle
                      description={getDescription('createVersionLink')}
                      disabled={!allowed('createVersionLink')}
                      field={{
                        name: 'createVersionLink',
                        value: createVersionLink,
                      }}
                      form={formProps}
                      label="Create version link"
                      labelPosition="left"
                    />
                  )}

                  {show('versionLinkText') && (
                    <TextField
                      description={getDescription('versionLinkText')}
                      disabled={!allowed('versionLinkText')}
                      field={{
                        name: 'versionLinkText',
                        value: versionLinkText,
                      }}
                      form={formProps}
                      label="Version Link Text"
                    />
                  )}

                  {show('versionLinkUri') && (
                    <TextField
                      description={getDescription('versionLinkUri')}
                      disabled={!allowed('versionLinkUri')}
                      field={{ name: 'versionLinkUri', value: versionLinkUri }}
                      form={formProps}
                      label="Version Link URI"
                    />
                  )}
                </FormSection>
              )}

              {showSubmitButton && (
                <ButtonWrapper>
                  <Button
                    data-test-id="saving-settings"
                    disabled={
                      (variant !== 'newBook' && !dirty) || isSaving || readonly
                    }
                    loading={isSaving}
                    status="primary"
                    type="submit"
                  >
                    {submitButtonLabel}
                  </Button>
                </ButtonWrapper>
              )}
            </Form>
          )
        }}
      </Formik>
    </Wrapper>
  )
}

BookSettings.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  variant: PropTypes.oneOf(['template', 'newBook', 'bookSettings']).isRequired,

  collectionOptions: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }),
  ),
  inCollection: PropTypes.bool,
  inFundedCollection: PropTypes.bool,
  isEditor: PropTypes.bool,
  isOrgAdmin: PropTypes.bool,
  isSysAdmin: PropTypes.bool,
  publisherOptions: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }),
  ),
  showSubmitButton: PropTypes.bool,
  submitButtonLabel: PropTypes.string,
  type: PropTypes.oneOf(['chapterProcessed', 'wholeBook']),
  workflow: PropTypes.oneOf(['word', 'xml', 'pdf']),
}

BookSettings.defaultProps = {
  collectionOptions: null,
  inCollection: false,
  inFundedCollection: false,
  isEditor: false,
  isOrgAdmin: false,
  isSysAdmin: false,
  publisherOptions: [],
  showSubmitButton: false,
  submitButtonLabel: 'Submit',
  type: null,
  workflow: null,
}

export default BookSettings
