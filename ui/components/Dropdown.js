/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid, th } from '@pubsweet/ui-toolkit'

import { Button as UIButton, Icon as UIIcon } from './common'

const Icon = styled(UIIcon)`
  vertical-align: top;
`

const ChevronIcon = styled(Icon)`
  float: right;
`

const StyledDropdown = styled.div`
  display: inline-block;
  height: 100%;
  margin: 0;
  position: relative;
`

const DropdownTitle = styled(UIButton)`
  background: ${props => {
    const { status, outlined } = props
    if (outlined) return props.background || 'transparent'
    if (status === 'secondary') return th('colorSecondary')
    if (status === 'danger') return th('colorError')
    return th('colorPrimary')
  }};
  border-color: ${props => {
    const { status } = props
    if (status === 'secondary') return th('colorSecondary')
    if (status === 'danger') return th('colorError')
    return th('colorPrimary')
  }};

  border-radius: ${th('borderRadius')};

  color: ${props => {
    const { outlined, status } = props

    if (outlined) {
      if (status === 'danger') return th('colorError')
      if (status === 'secondary') return th('colorSecondary')
      return th('colorPrimary')
    }

    return th('colorTextReverse')
  }};
  cursor: pointer;
  display: inline-block;
  font-family: ${th('fontInterface')};
  min-width: calc(${th('gridUnit')} * 12);
  text-decoration: none;
  text-transform: capitalize;
  white-space: nowrap;

  &:hover {
    color: ${props => props.color};
  }

  [disabled] {
    background-color: #f1f1f1;
  }

  > span {
    line-height: ${grid(4)};
  }
`

const DropdownMenu = styled.ul`
  background-color: ${th('colorBackground')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorPrimary')};
  border-radius: ${th('borderRadius')};
  bottom: ${props => (props.direction === 'up' ? '35px' : '-')};
  color: ${th('colorText')};
  display: ${props => (props.menuIsOpen ? 'block' : 'none')};
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  list-style-type: none;
  margin: 0;
  padding: 0;
  position: absolute;
  top: ${props => (props.direction === 'down' ? '100%' : '')};
  width: 100%;
  z-index: 9;
`

const Item = styled.li`
  align-content: center;
  align-items: center;
  cursor: pointer;
  display: flex;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  white-space: normal;
  width: 100%;
  word-break: break-word;

  &:hover {
    background-color: ${th('colorBackgroundHue')};
  }

  &[disabled] {
    color: ${th('colorDisabled')};
    cursor: not-allowed;
  }
`

const Dropdown = ({
  children,
  direction,
  disabled = false,
  icon,
  iconPosition,
  itemsList,
  primary,
  size,
  variant,
  'data-test-id': testId,
  color,
  outlined,
  status,
}) => {
  const [menuIsOpen, setMenuIsOpen] = useState(false)

  let colorIcon

  if (color) {
    colorIcon = color
  } else {
    primary ? (colorIcon = '#fff') : (colorIcon = '#0B65CB')
  }

  const getIconDirection = () => {
    if (direction === 'up') {
      return menuIsOpen ? 'chevron-down' : 'chevron-up'
    }

    return !menuIsOpen ? 'chevron-down' : 'chevron-up'
  }

  return (
    <StyledDropdown isDropdown size={size}>
      <DropdownTitle
        data-test-id={testId}
        disabled={disabled}
        onBlur={() => setMenuIsOpen(false)}
        onClick={() => setMenuIsOpen(!menuIsOpen)}
        outlined={outlined}
        primary={primary}
        size={size}
        status={status}
        variant={variant}
      >
        <span>
          {icon && iconPosition === 'start' && (
            <Icon color={colorIcon} primary={primary} size={2}>
              {icon}{' '}
            </Icon>
          )}

          <span>{children}</span>

          {icon && iconPosition === 'end' && (
            <Icon color={colorIcon} size={2}>
              {icon}{' '}
            </Icon>
          )}
        </span>
        <ChevronIcon color={colorIcon} size={2}>
          {getIconDirection()}
        </ChevronIcon>
      </DropdownTitle>
      <DropdownMenu direction={direction} menuIsOpen={menuIsOpen}>
        {itemsList.map(item => (
          <Item
            key={item.value}
            onClick={() => {
              if (!item.disabled) {
                item.onClick()
                setMenuIsOpen(false)
              }
            }}
            {...item.props}
            disabled={item.disabled}
            onMouseDown={e => e.preventDefault()}
          >
            {item.title}
          </Item>
        ))}
      </DropdownMenu>
    </StyledDropdown>
  )
}

Dropdown.propTypes = {
  /** Content of the button of the dropdown */
  children: PropTypes.node.isRequired,

  /** Dropdow direction */
  direction: PropTypes.oneOf(['up', 'down']),
  /** Icon name (An icon name, from the Feather icon set.) */
  icon: PropTypes.string,
  /** Icon Position (Defines the position of the icon (if is is at the start of the button , or at the end)) */
  iconPosition: PropTypes.oneOf(['start', 'end']),

  /** List of the items for the drop down items */
  itemsList: PropTypes.arrayOf(
    PropTypes.shape({
      /** The key for the item  */
      value: PropTypes.number,
      /** The click function for the item  */
      onClick: PropTypes.func,

      /** The title for the item  */
      title: PropTypes.node.isRequired,

      /** Is item  disabled */
      disabled: PropTypes.bool,
    }),
  ),

  size: PropTypes.string,
  variant: PropTypes.string,

  /** Primary property for the dropdown, if it is false(or not set) then the dropdown will be secondary  */
  primary: PropTypes.bool,
}

Dropdown.defaultProps = {
  direction: 'down',
  icon: null,
  iconPosition: 'start',
  itemsList: [],
  primary: false,
  size: '',
  variant: '',
}

export default Dropdown
