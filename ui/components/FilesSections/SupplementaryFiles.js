/* eslint-disable react/jsx-newline */
/* eslint-disable react/prop-types */

import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import moment from 'moment'

import { Link } from '@pubsweet/ui'
import { grid } from '@pubsweet/ui-toolkit'

import { getDataTableFiles } from './common'
import useUploadFile from '../../../app/useUploadFile'
// eslint-disable-next-line import/no-cycle
import { ConfirmDeleteSupplementaryFile } from '../../Pages/Bookmanager/BookComponent'

import {
  ButtonGroup,
  Button,
  FileSection,
  Tags as TagsDefault,
  FlexSpace,
} from '..'

import {
  getDisplayName,
  validateSupplementaryFile,
  validateFileName,
} from '../../common/utils'
import { getDownloadLink } from '../../../app/utilsConfig'
import PermissionsGate from '../../../app/components/PermissionsGate'

const ACCEPTED_TYPES_SUPPLEMENTARY =
  '.doc,.docx,application/mswor.pdf,.ppt,.txt,.xml,.csv,.xls ,.mov,.avi,.mpg,.mpeg,.mp4,.mkv,.flv,.wmv,.zip'

const Tags = styled(TagsDefault)`
  margin: 0 ${grid(1)};
`

const SupplementaryFiles = ({
  files = [],
  disableUploadButton,
  saveFileFn,
  isUploading,
  deleteFileMutation,
  deletingFile,
  title = 'Supplementary',
  selectedRows,
  setSelectedRows,
  checkboxColumn,
  canUpload,
  multipleUpload = true,
  versionName = 1,
  updateTagFile,
  showTags = false,
  deleteScope = [],
}) => {
  const [supplementary, setSupplementaryFiles] = useState({
    files,
    more: false,
    uploading: false,
  })

  useEffect(() => {
    setSupplementaryFiles({
      files,
      more: supplementary.more,
      uploading: false,
    })
  }, [files])

  const {
    dispatch,
    getInputProps,
    getRootProps,
    fileList,
    open,
  } = useUploadFile({
    validationFn: uploadedfiles =>
      multipleUpload
        ? validateSupplementaryFile(
            uploadedfiles,
            files.map(x => x.name),
          )
        : validateFileName(
            uploadedfiles,
            (files?.length && files[0].name) || '',
          ),
  })

  const isMaxfileVersion = rowData =>
    Number(rowData.versionName) ===
    (Math.max(
      ...files.map(o =>
        rowData.parentId === o.parentId ? Number(o.versionName) : 0,
      ),
    ) || 1)

  const tagOptions = [{ label: 'book_pdf', value: 'book_pdf', color: 'red' }]

  const columns = [
    {
      cellRenderer: () => ({ cellData, rowData }) => {
        return (
          <>
            <ButtonGroup>
              <Link to={getDownloadLink(rowData.id, 'source')}>{cellData}</Link>
            </ButtonGroup>

            {showTags && (
              <Tags
                onChange={selected => {
                  const value = selected[0]?.value || ''

                  updateTagFile({
                    fileId: rowData.bcfId,
                    tag: value,
                  })
                }}
                options={tagOptions}
                values={tagOptions.filter(x => x.value === rowData.tag)}
              />
            )}
          </>
        )
      },
      dataKey: 'name',
      disableSort: true,
      label: 'Filename',
    },

    {
      cellRenderer: () => ({ cellData }) => (
        <>
          V{versionName}.{cellData}
        </>
      ),
      dataKey: 'versionName',
      disableSort: true,
      label: 'File Version',
      width: 250,
    },
    {
      cellRenderer: () => ({ cellData, rowData }) =>
        `${
          rowData.copied ? `Copied from V${parseInt(versionName, 10) - 1} ` : ''
        }${moment
          .utc(cellData)
          .utcOffset(moment().utcOffset())
          .format('L LT')}  ${
          rowData.owner ? `by ${getDisplayName(rowData.owner)}` : 'Unknown User'
        }`,

      dataKey: 'created',
      disableSort: true,
      label: 'Uploaded ',
      width: 800,
    },
    {
      cellRenderer: () => ({ cellData, rowData }) => {
        const hasMultipleVersions = () => {
          const count = files.filter(i => i.parentId === rowData.parentId)
            .length

          return count > 1
        }

        if (!(isMaxfileVersion(rowData) && canUpload)) return null

        return (
          <FlexSpace>
            <PermissionsGate scopes={deleteScope}>
              <ConfirmDeleteSupplementaryFile
                deleteFileMutation={deleteFileMutation}
                deletingFile={deletingFile}
                disabled={disableUploadButton}
                fileDetails={rowData}
                filesList={supplementary}
                hasMultipleVersions={hasMultipleVersions}
                setSupplementaryFiles={setSupplementaryFiles}
                versionName={rowData.versionName}
              />
            </PermissionsGate>
          </FlexSpace>
        )
      },
      dataKey: 'bcfId',
      disableSort: true,
      width: 400,
    },
  ]

  const saveuploadFileFn = () => {
    const filesUpl = fileList.map(file => ({
      data: file,
    }))

    saveFileFn({
      fileUpload: filesUpl,
      dispatch: () =>
        dispatch({
          type: 'reset',
        }),
    })
  }

  const dataTable = getDataTableFiles({ displayFiles: supplementary })

  return (
    <FileSection
      columns={[checkboxColumn].concat(columns)}
      data={dataTable || []}
      headerRightComponent={[
        canUpload && (
          <span
            disabled={disableUploadButton}
            key="upload_btn"
            {...getRootProps({
              className: 'dropzone',
              accept: ACCEPTED_TYPES_SUPPLEMENTARY,
              multiple: multipleUpload,
              onClick: e => {
                e.stopPropagation()
                if (!disableUploadButton) open()
              },
            })}
          >
            <input
              data-test-id={
                title === 'Bookshelf Display PDFs'
                  ? 'upload-displayPDFs-input'
                  : 'upload-supplementary-input'
              }
              {...getInputProps({ multiple: multipleUpload })}
              disabled={disableUploadButton}
            />

            <Button disabled={disableUploadButton} icon="plus" status="primary">
              Upload
            </Button>
          </span>
        ),
      ]}
      isUploading={isUploading}
      more={supplementary.more}
      removeFn={file =>
        file
          ? dispatch({
              payload: {
                name: file.name,
              },
              type: 'remove',
            })
          : dispatch({
              type: 'reset',
            })
      }
      saveuploadFileFn={() => saveuploadFileFn()}
      selectedRows={selectedRows}
      setSelectedRows={setSelectedRows}
      showLessFn={() =>
        setSupplementaryFiles({ files: supplementary.files, more: false })
      }
      showMoreFn={() =>
        setSupplementaryFiles({ files: supplementary.files, more: true })
      }
      title={title}
      uploadingFiles={fileList || []}
    />
  )
}

export default SupplementaryFiles
