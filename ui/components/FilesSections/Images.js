/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'
import moment from 'moment'
import { Button, FileSection, FlexSpace } from '..'
import { getDisplayName, validateImagesFile } from '../../common/utils'
import { getDownloadLink } from '../../../app/utilsConfig'
import useUploadFile from '../../../app/useUploadFile'
// eslint-disable-next-line import/no-cycle
import { ConfirmDeleteSupplementaryFile } from '../../Pages/Bookmanager/BookComponent'
import { getDataTableFiles } from './common'
import { name as nameCol } from '../FileSection/Columns'

const ACCEPTED_TYPES = '.jpeg,.png,.jpg'

const Images = ({
  files = [],
  disableUploadButton,
  saveFileFn,
  isUploading,
  deleteFileMutation,
  sectionProps: { workflow, type },
  selectedRows,
  setSelectedRows,
  checkboxColumn,
  canUpload,
  versionName,
  deletingFile,
  deleteScope,
}) => {
  const [image, setImageFiles] = useState({
    files,
    more: false,
  })

  const canEditImages = workflow !== 'word'

  useEffect(() => {
    setImageFiles({
      files,
      more: image.more,
      uploading: false,
    })
  }, [files])

  const {
    dispatch,
    getInputProps,
    getRootProps,
    fileList,
    open,
  } = useUploadFile({
    validationFn: uploadedfiles =>
      validateImagesFile(
        uploadedfiles,
        files.map(x => x.name),
      ),
  })

  const isMaxfileVersion = rowData =>
    Number(rowData.versionName) ===
    (Math.max(
      ...files.map(o =>
        rowData.parentId === o.parentId ? Number(o.versionName) : 0,
      ),
    ) || 1)

  const saveuploadFileFn = () => {
    const filesUpl = fileList.map(file => ({
      data: file,
    }))

    saveFileFn({
      fileUpload: filesUpl,
      dispatch: () =>
        dispatch({
          type: 'reset',
        }),
    })
  }

  useEffect(() => {
    if (files.length > 0) {
      setImageFiles({ files, more: false })
    }
  }, [files])

  const columns = [
    nameCol,
    {
      cellRenderer: () => ({ cellData }) => cellData.replace('image/', ''),
      dataKey: 'mimeType',
      disableSort: true,
      label: 'Type',
      width: 250,
    },
    {
      cellRenderer: () => ({ cellData }) => (
        <>
          V{versionName}.{cellData}
        </>
      ),
      dataKey: 'versionName',
      disableSort: true,
      label: 'File Version',
      width: 280,
    },
    {
      cellRenderer: () => ({ cellData, rowData }) => (
        <>
          {moment.utc(cellData).utcOffset(moment().utcOffset()).format('L LT')}{' '}
          {rowData.owner
            ? `by ${getDisplayName(rowData.owner)}`
            : 'Unknown User'}
        </>
      ),
      dataKey: 'created',
      disableSort: true,
      label: 'Uploaded ',
      width: 900,
    },
    {
      cellRenderer: () => ({ cellData, rowData }) => {
        const hasMultipleVersions = () => {
          const count = files.filter(i => i.parentId === rowData.parentId)
            .length

          return count > 1
        }

        if (!(canEditImages && isMaxfileVersion(rowData) && canUpload))
          return null

        return (
          <FlexSpace>
            <ConfirmDeleteSupplementaryFile
              deleteFileMutation={deleteFileMutation}
              deleteScope={deleteScope}
              deletingFile={deletingFile}
              disabled={disableUploadButton}
              fileDetails={rowData}
              filesList={image}
              hasMultipleVersions={hasMultipleVersions}
              setSupplementaryFiles={setImageFiles}
              versionName={rowData.versionName}
            />
          </FlexSpace>
        )
      },
      dataKey: 'bcfId',
      disableSort: true,
      width: 400,
    },
  ]

  const dataTable = getDataTableFiles({ displayFiles: image })

  return (
    <FileSection
      columns={[checkboxColumn].concat(columns)}
      data={dataTable}
      headerRightComponent={
        canEditImages &&
        canUpload && (
          <span
            disabled={disableUploadButton}
            key="upload_btn"
            {...getRootProps({
              className: 'dropzone',
              accept: ACCEPTED_TYPES,
              multiple: true,
              onClick: e => {
                e.stopPropagation()
                if (!disableUploadButton) open()
              },
            })}
          >
            <input
              data-test-id="upload-images-input"
              {...getInputProps({ multiple: true })}
              disabled={disableUploadButton}
            />

            <Button disabled={disableUploadButton} icon="plus" status="primary">
              Upload
            </Button>
          </span>
        )
      }
      isUploading={isUploading}
      more={image.more}
      onRowClick={props => {
        const rowData = image?.files[props.index]
        window.location.href = getDownloadLink(rowData.id, 'image')
      }}
      removeFn={file =>
        file
          ? dispatch({
              payload: {
                name: file.name,
              },
              type: 'remove',
            })
          : dispatch({
              type: 'reset',
            })
      }
      saveuploadFileFn={() => saveuploadFileFn()}
      selectedRows={selectedRows}
      setSelectedRows={setSelectedRows}
      showLessFn={() => {
        setImageFiles({ files: image.files, more: false })
      }}
      showMoreFn={() => {
        setImageFiles({ files: image.files, more: true })
      }}
      title="Images"
      uploadingFiles={fileList || []}
    />
  )
}

export default Images
