/* eslint-disable react/prop-types */
import React from 'react'
import sortBy from 'lodash/sortBy'

import { FileSection } from '..'
import { getDownloadLink } from '../../../app/utilsConfig'
import { name as nameCol, created as createdCol } from '../FileSection/Columns'

const Review = ({ files, selectedRows, setSelectedRows, checkboxColumn }) => {
  const columns = [nameCol, createdCol]
  const data = sortBy(files, 'created')
  return (
    <FileSection
      columns={[checkboxColumn].concat(columns)}
      data={data}
      onRowClick={props => {
        const rowData = files[props.index]
        window.location.href = getDownloadLink(rowData.id, 'image')
      }}
      selectedRows={selectedRows}
      setSelectedRows={setSelectedRows}
      title="Review"
    />
  )
}

export default Review
