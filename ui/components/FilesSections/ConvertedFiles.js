/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Link } from '@pubsweet/ui'
import moment from 'moment'
import useUploadFile from '../../../app/useUploadFile'
import { FileSection, Button } from '..'
import { getDisplayName, validateFileNameConverted } from '../../common/utils'
import { getDownloadLink } from '../../../app/utilsConfig'
import { getDataTableFiles } from './common'
import {
  versionName as versionNameCol,
  status as statusCol,
} from '../FileSection/Columns'

const StyledLink = styled(Link)`
  min-width: 0;
  text-transform: none;
`

const ConvertedFiles = ({
  files = [],
  selectedRows,
  canUpload,
  setSelectedRows,
  checkboxColumn,
  versionName: bcVersion,
  disableUploadButton,
  saveFileFn,
  refetchQueries,
  isUploading,
  validateFile,
  sourceFile,
  conversionWorkflow,
}) => {
  const {
    dispatch,
    getInputProps,
    getRootProps,
    fileList,
    open,
  } = useUploadFile({
    validationFn: uploadedFiles =>
      validateFileNameConverted({
        uploadedFiles,
        fileNameCompare: sourceFile,
        conversionWorkflow,
        chapterIndependently: validateFile,
      }),
  })

  const [displayFiles, setDisplayFiles] = useState({
    files,
    more: false,
    uploading: false,
  })

  useEffect(() => {
    setDisplayFiles({
      files,
      more: displayFiles.more,
      uploading: false,
    })
  }, [files])

  const columns = [
    {
      cellRenderer: () => ({ cellData, rowData }) => {
        return (
          <StyledLink to={getDownloadLink(rowData.id || '', 'converted')}>
            {cellData}
          </StyledLink>
        )
      },
      dataKey: 'name',
      disableSort: true,
      label: 'File name ',
    },
    versionNameCol(bcVersion),
    statusCol,
    {
      cellRenderer: () => ({ cellData, rowData }) => (
        <>
          {moment.utc(cellData).utcOffset(moment().utcOffset()).format('L LT')}{' '}
          {rowData.owner
            ? `by ${getDisplayName(rowData.owner)}`
            : 'Unknown User'}
        </>
      ),
      dataKey: 'created',
      disableSort: true,
      label: 'Uploaded ',
      width: 600,
    },
  ]

  const saveuploadFileFn = () => {
    const filesUpl = fileList.map(file => ({
      data: file,
    }))

    saveFileFn({
      fileUpload: filesUpl,
      dispatch: () =>
        dispatch({
          type: 'reset',
        }),
    })
  }

  const dataTable = getDataTableFiles({ displayFiles })

  return (
    <FileSection
      columns={[checkboxColumn].concat(columns)}
      data={dataTable || []}
      headerRightComponent={[
        canUpload && (
          <span
            disabled={disableUploadButton}
            key="upload_btn"
            {...getRootProps({
              className: 'dropzone',
              accept:
                '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
              multiple: false,
              onClick: e => {
                e.stopPropagation()
                if (!disableUploadButton) open()
              },
            })}
          >
            <input
              data-test-id="upload-converted-input"
              {...getInputProps({ multiple: false })}
              disabled={disableUploadButton}
            />

            <Button disabled={disableUploadButton} icon="plus" status="primary">
              Upload
            </Button>
          </span>
        ),
      ]}
      isUploading={isUploading}
      more={displayFiles.more}
      removeFn={file =>
        file
          ? dispatch({
              payload: {
                name: file.name,
              },
              type: 'remove',
            })
          : dispatch({
              type: 'reset',
            })
      }
      saveuploadFileFn={() => saveuploadFileFn()}
      selectedRows={selectedRows}
      setSelectedRows={setSelectedRows}
      showLessFn={() =>
        setDisplayFiles({ files: displayFiles.files, more: false })
      }
      showMoreFn={() =>
        setDisplayFiles({ files: displayFiles.files, more: true })
      }
      startExpanded
      title="Converted"
      uploadingFiles={fileList || []}
    />
  )
}

export default ConvertedFiles
