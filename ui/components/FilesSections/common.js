// eslint-disable-next-line import/no-unresolved
import { groupBy, map } from 'lodash'

// eslint-disable-next-line import/prefer-default-export
export const getDataTableFiles = ({ displayFiles }) => {
  const groupByParentId = {}

  displayFiles.files.forEach(file => {
    if (groupByParentId[file.parentId]) {
      groupByParentId[file.parentId].push(file)
    } else {
      groupByParentId[file.parentId] = [file]
    }
  })

  let dataTable = []

  map(groupByParentId, grouped => {
    grouped.sort((a, b) =>
      Number(a.versionName) < Number(b.versionName) ? 1 : -1,
    )

    dataTable = dataTable.concat(grouped)
  })

  if (!displayFiles.more) {
    dataTable = Object.entries(
      groupBy(displayFiles.files, item => item.parentId),
    ).map(entries =>
      entries[1].reduce((prev, current) =>
        Number(prev.versionName) > Number(current.versionName) ? prev : current,
      ),
    )
  }

  return dataTable
}

// eslint-disable-next-line import/prefer-default-export
export const getLatestVersionFiles = files => {
  const groupByParentId = {}

  files.forEach(file => {
    if (groupByParentId[file.parentId]) {
      groupByParentId[file.parentId].push(file)
    } else {
      groupByParentId[file.parentId] = [file]
    }
  })

  let dataTable = []

  map(groupByParentId, grouped => {
    grouped.sort((a, b) =>
      Number(a.versionName) < Number(b.versionName) && a.name !== b.name
        ? 1
        : -1,
    )

    dataTable = dataTable.concat(grouped)
  })

  // if (!displayFiles.more) {
  dataTable = Object.entries(
    groupBy(files, item => item.parentId),
  ).map(entries =>
    entries[1].reduce((prev, current) =>
      Number(prev.versionName) > Number(current.versionName) &&
      prev.name !== current.name
        ? prev
        : current,
    ),
  )

  // }

  return dataTable
}
