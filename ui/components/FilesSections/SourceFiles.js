/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import moment from 'moment'

import { Link } from '@pubsweet/ui'
import { grid } from '@pubsweet/ui-toolkit'

import useUploadFile from '../../../app/useUploadFile'
import { getDataTableFiles } from './common'
import { getDisplayName } from '../../common/utils'
import { getDownloadLink } from '../../../app/utilsConfig'
import { ButtonGroup, Button, FileSection, Tags as TagsDefault } from '..'
import {
  versionName as versionNameCol,
  status as statusCol,
} from '../FileSection/Columns'

const Tags = styled(TagsDefault)`
  margin: 0 ${grid(1)};
`

const Source = ({
  files = [],
  disableUploadButton,
  saveFileVersion,
  refetchQueries,
  isUploading,
  selectedRows,
  setSelectedRows,
  checkboxColumn,
  canUpload,
  versionName: bcVersion,
  updateTagFile,
  showTags = false,
  validateUpload,
}) => {
  const {
    dispatch,
    getInputProps,
    getRootProps,
    fileList,
    open,
  } = useUploadFile({
    validationFn: filesuploaded => validateUpload(filesuploaded),
  })

  const [displayFiles, setDisplayFiles] = useState({
    files,
    more: false,
    uploading: false,
  })

  useEffect(() => {
    setDisplayFiles({
      files,
      more: displayFiles.more,
      uploading: false,
    })
  }, [files])

  const sourceTagsOptions = [
    { label: 'main_xml', value: 'main_xml', color: 'green' },
  ]

  const columns = [
    {
      cellRenderer: () => ({ cellData, rowData }) => {
        return (
          <>
            <ButtonGroup>
              <Link to={getDownloadLink(rowData.id, 'source')}>{cellData}</Link>
            </ButtonGroup>

            {showTags && (
              <Tags
                onChange={selected => {
                  const value = selected[0]?.value || ''

                  updateTagFile({
                    fileId: rowData.bcfId,
                    tag: value,
                  })
                }}
                options={sourceTagsOptions}
                values={sourceTagsOptions.filter(x => x.value === rowData.tag)}
              />
            )}
          </>
        )
      },
      dataKey: 'name',
      disableSort: true,
      label: 'Source ',
    },
    versionNameCol(bcVersion),
    statusCol,
    {
      cellRenderer: () => ({ cellData, rowData }) => (
        <>
          {moment.utc(cellData).utcOffset(moment().utcOffset()).format('L LT')}{' '}
          {rowData?.owner
            ? `by ${getDisplayName(rowData.owner)}`
            : 'by Unknown User'}
        </>
      ),
      dataKey: 'created',
      disableSort: true,
      label: 'Uploaded ',
      width: 600,
    },
  ]

  const dataTable = getDataTableFiles({ displayFiles })

  return (
    <FileSection
      columns={[checkboxColumn].concat(columns)}
      data={dataTable || []}
      headerRightComponent={[
        canUpload && (
          <span
            disabled={disableUploadButton}
            key="upload_btn"
            {...getRootProps({
              className: 'dropzone',
              accept:
                '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
              multiple: true,
              onClick: e => {
                if (!disableUploadButton) {
                  e.stopPropagation()
                  open()
                }
              },
            })}
          >
            <input
              {...getInputProps({ multiple: true })}
              data-test-id="upload-source-input"
              disabled={disableUploadButton}
            />

            <Button disabled={disableUploadButton} icon="plus" status="primary">
              Upload
            </Button>
          </span>
        ),
      ]}
      isUploading={isUploading}
      more={displayFiles.more}
      removeFn={file =>
        file
          ? dispatch({
              payload: {
                name: file.name,
              },
              type: 'remove',
            })
          : dispatch({
              type: 'reset',
            })
      }
      saveuploadFileFn={() =>
        saveFileVersion({
          dispatch: () =>
            dispatch({
              type: 'reset',
            }),
          fileList,
          refetchQueries,
        })
      }
      selectedRows={selectedRows}
      setSelectedRows={setSelectedRows}
      showLessFn={() =>
        setDisplayFiles({ files: displayFiles.files, more: false })
      }
      showMoreFn={() =>
        setDisplayFiles({ files: displayFiles.files, more: true })
      }
      startExpanded
      title="Source"
      uploadingFiles={fileList || []}
    />
  )
}

export default Source
