/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'
import { FileSection, FlexSpace, Button } from '..'
import { validateSupportFile } from '../../common/utils'
import useUploadFile from '../../../app/useUploadFile'
// eslint-disable-next-line import/no-cycle
import { ConfirmDeleteSupplementaryFile } from '../../Pages/Bookmanager/BookComponent'
import { getDataTableFiles } from './common'
import {
  name as nameCol,
  versionName as versionNameCol,
  created as createdCol,
} from '../FileSection/Columns'

const ACCEPTED_TYPES =
  '.doc,.docx,application/mswor.pdf,.ppt,.txt,.xml,.csv,.xls ,.mov,.avi,.mpg,.mpeg,.mp4,.mkv,.flv,.wmv,.zip'

const Support = ({
  files,
  selectedRows,
  setSelectedRows,
  checkboxColumn,
  canUpload,
  disableUploadButton,
  saveFileFn,
  isUploading,
  deleteFileMutation,
  versionName,
  deletingFile,
  deleteScope,
}) => {
  const [support, setSupportFiles] = useState({
    files,
    more: false,
  })

  useEffect(() => {
    if (files.length > 0) {
      setSupportFiles({ files, more: false })
    }
  }, [files])

  useEffect(() => {
    setSupportFiles({
      files,
      more: support.more,
      uploading: false,
    })
  }, [files])

  const {
    dispatch,
    getInputProps,
    getRootProps,
    fileList,
    open,
  } = useUploadFile({
    validationFn: uploadedfiles =>
      validateSupportFile(
        uploadedfiles,
        files.map(x => x.name),
      ),
  })

  const saveuploadFileFn = () => {
    const filesUpl = fileList.map(file => ({
      data: file,
    }))

    saveFileFn({
      fileUpload: filesUpl,
      dispatch: () =>
        dispatch({
          type: 'reset',
        }),
    })
  }

  const isMaxfileVersion = rowData =>
    Number(rowData.versionName) ===
    (Math.max(
      ...files.map(o =>
        rowData.parentId === o.parentId ? Number(o.versionName) : 0,
      ),
    ) || 1)

  const columns = [
    nameCol,
    versionNameCol(versionName),
    createdCol,
    {
      cellRenderer: () => ({ cellData, rowData }) => {
        const hasMultipleVersions = () => {
          const count = files.filter(i => i.parentId === rowData.parentId)
            .length

          return count > 1
        }

        if (!isMaxfileVersion(rowData) && canUpload) return null

        return (
          <FlexSpace>
            <ConfirmDeleteSupplementaryFile
              deleteFileMutation={deleteFileMutation}
              deleteScope={deleteScope}
              deletingFile={deletingFile}
              disabled={disableUploadButton}
              fileDetails={rowData}
              filesList={support}
              hasMultipleVersions={hasMultipleVersions}
              setSupplementaryFiles={setSupportFiles}
              versionName={rowData.versionName}
            />
          </FlexSpace>
        )
      },
      dataKey: 'bcfId',
      disableSort: true,
      width: 400,
    },
  ]

  const dataTable = getDataTableFiles({ displayFiles: support })

  return (
    <FileSection
      columns={[checkboxColumn].concat(columns)}
      data={dataTable || []}
      headerRightComponent={[
        canUpload && (
          <span
            disabled={disableUploadButton}
            key="upload_btn"
            {...getRootProps({
              className: 'dropzone',
              accept: ACCEPTED_TYPES,
              multiple: true,
              onClick: e => {
                e.stopPropagation()
                if (!disableUploadButton) open()
              },
            })}
          >
            <input
              data-test-id="upload-support-input"
              {...getInputProps({ multiple: true })}
              disabled={disableUploadButton}
            />

            <Button disabled={disableUploadButton} icon="plus" status="primary">
              Upload
            </Button>
          </span>
        ),
      ]}
      isUploading={isUploading}
      more={support.more}
      removeFn={file =>
        file
          ? dispatch({
              payload: {
                name: file.name,
              },
              type: 'remove',
            })
          : dispatch({
              type: 'reset',
            })
      }
      saveuploadFileFn={() => saveuploadFileFn()}
      selectedRows={selectedRows}
      setSelectedRows={setSelectedRows}
      showLessFn={() => setSupportFiles({ files: support.files, more: false })}
      showMoreFn={() => setSupportFiles({ files: support.files, more: true })}
      title="Support"
      uploadingFiles={fileList || []}
    />
  )
}

export default Support
