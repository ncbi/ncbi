import React from 'react'
import Modal from 'react-modal'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { th } from '@pubsweet/ui-toolkit'

Modal.setAppElement('#root')
Modal.defaultStyles.overlay.backgroundColor = 'rgba(0,0,0,0.8)'
Modal.defaultStyles.overlay.zIndex = '1'

const StyledModal = styled(Modal)`
  left: 50%;
  position: fixed;
  top: 50%;
  transform: translate(-50%, -50%);
`

const ModalStyled = styled.div`
  background-color: ${th('colorBackground')};
  border: 1px solid rgba(104, 104, 104, 0.3);
  color: ${th('colorText')};
  display: flex;
  flex-direction: column;
  font-family: ${th('fontInterface')};
  line-height: ${th('lineHeightBase')};
  min-height: ${props => (props.fullSize ? '70vh' : '40vh')};
  overflow: hidden;
  width: ${props => (props.width ? props.width : '50vw')};
`

const ModalComponent = props => {
  const { children, className, fullSize, isOpen, onClose, style, width } = props

  return (
    <StyledModal
      isOpen={isOpen}
      onRequestClose={onClose}
      // shouldCloseOnEsc
      // shouldCloseOnOverlayClick
      style={style || {}}
    >
      <ModalStyled className={className} fullSize={fullSize} width={width}>
        {children}
      </ModalStyled>
    </StyledModal>
  )
}

ModalComponent.propTypes = {
  /** The contents of the Modal   */
  children: PropTypes.node,
  fullSize: PropTypes.bool,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  width: PropTypes.string,
}

ModalComponent.defaultProps = {
  children: null,
  fullSize: false,
  isOpen: false,
  onClose: null,
  style: {},
  width: '90vw',
}

export default ModalComponent
