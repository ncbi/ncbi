import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Modal as AntModal } from 'antd'
import { CloseCircleOutlined } from '@ant-design/icons'

import { grid, th } from '@pubsweet/ui-toolkit'

const StyledModal = styled(AntModal)`
  .ant-modal-content {
    padding: 0;
  }

  .ant-modal-header,
  .ant-modal-body,
  .ant-modal-footer {
    padding: ${grid(2)};
  }

  .ant-modal-header {
    border-bottom: 1px solid ${th('colorBorder')};
    margin-bottom: 0;
  }

  .ant-modal-footer {
    border-top: 1px solid ${th('colorBorder')};
    margin-top: 0;
  }
`

const Modal = props => {
  const { children, footer, isOpen, onCancel, onOk, title } = props

  return (
    <StyledModal
      centered
      closeIcon={<CloseCircleOutlined />}
      footer={footer}
      onCancel={onCancel}
      onOk={onOk}
      open={isOpen}
      title={title}
    >
      {children}
    </StyledModal>
  )
}

Modal.propTypes = {
  footer: PropTypes.node,
  isOpen: PropTypes.bool,
  onCancel: PropTypes.func.isRequired,
  onOk: PropTypes.func,
  title: PropTypes.node,
}

Modal.defaultProps = {
  footer: null,
  isOpen: false,
  onOk: () => {},
  title: false,
}

export default Modal
