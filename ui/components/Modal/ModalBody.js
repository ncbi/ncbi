import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { grid } from '@pubsweet/ui-toolkit'

const ModalBodyStyled = styled.div`
  height: auto;
  margin-bottom: 10px;
  max-height: calc(100vh - 225px);
  min-height: ${props => (props.height ? props.height : 'none')};
  overflow: auto;
  padding: ${props => (props.noPadding ? 0 : grid(2))};
`

const ModalBody = ({ children, type, ...props }) => (
  <ModalBodyStyled {...props} data-test-id="modalBody">
    {children}
  </ModalBodyStyled>
)

ModalBody.propTypes = {
  /** The contents of the ModalBody   */
  children: PropTypes.node,
  type: PropTypes.oneOf(['modal', 'small']),
}

ModalBody.defaultProps = {
  children: null,
  type: 'modal',
}

export default ModalBody
