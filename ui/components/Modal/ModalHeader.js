import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { th } from '@pubsweet/ui-toolkit'
import { Icon as UIIcon } from '../common'

const ModalHeaderStyled = styled.div`
  align-items: center;
  border-bottom: 1px solid ${th('colorFurniture')};
  color: ${th('colorText')};
  display: flex;
  font-family: ${th('fontInterface')};
  font-size: 12pt;
  font-weight: 600;
  line-height: ${th('lineHeightBase')};
  padding: ${th('gridUnit')};
`

const Title = styled.div`
  flex-grow: 1;
  line-height: ${th('lineHeightBase')};
  padding: ${th('gridUnit')};
`

const CloseModal = styled.div`
  flex-grow: 0;
`

const Icon = styled(UIIcon)`
  cursor: pointer;
  float: right;
  height: 30px;
  justify-content: flex-end;
  width: 30px;

  > svg {
    stroke-width: 1px;
  }
`

const ModalHeader = ({ children, hideModal, minimize, disableClose }) => (
  <ModalHeaderStyled>
    <Title> {children}</Title>
    <CloseModal>
      {!disableClose && (
        <Icon color="#686868" data-test-id="close-modal" onClick={hideModal}>
          x-circle
        </Icon>
      )}
      {minimize && (
        <Icon color="#686868" onClick={hideModal}>
          minimize
        </Icon>
      )}
    </CloseModal>
  </ModalHeaderStyled>
)

ModalHeader.propTypes = {
  /** The contents of the ModalHeader   */
  children: PropTypes.node,

  /** The function to close the modal  */
  hideModal: PropTypes.func.isRequired,

  /* Minimize Icon Boolean */
  minimize: PropTypes.bool,
  disableClose: PropTypes.bool,
}

ModalHeader.defaultProps = {
  children: null,
  minimize: false,
  disableClose: false,
}

export default ModalHeader
