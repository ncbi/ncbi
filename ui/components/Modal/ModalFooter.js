import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { th, grid } from '@pubsweet/ui-toolkit'

const ModalFooterStyled = styled.div`
  border-top: 1px solid ${th('colorBorder')};
  display: flex;
  height: ${grid(5)};
  justify-content: flex-end;
  margin-top: auto;
  padding: ${grid(1)};
`

const ModalFooter = ({ children, className }) => (
  <ModalFooterStyled className={className}>{children}</ModalFooterStyled>
)

ModalFooter.propTypes = {
  /** The contents of the ModalFooter   */
  children: PropTypes.node,
}

ModalFooter.defaultProps = {
  children: null,
}

export default ModalFooter
