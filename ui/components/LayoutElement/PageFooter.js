import React from 'react'
import styled from 'styled-components'
import Twitter from '../../../public/twitter-logo.png'
import Facebook from '../../../public/facebook-logo.png'
import Youtube from '../../../public/youtube-logo.png'
import Linkedin from '../../../public/linkedin-logo.png'
import Github from '../../../public/github-logo.png'
import Rss from '../../../public/rss-logo.png'

import TwitterW from '../../../public/twitter-white.png'
import FacebookW from '../../../public/facebook-white.png'
import YoutubeW from '../../../public/youtube-white.png'
import Device from '../../common/responsiveDevices'

const Wrapper = styled.div`
  display: flex;
  display: block;
  flex-direction: column;
  margin-bottom: 0;
  width: 100%;
`

const ContactSection = styled.div`
  background-color: #205493;
  display: table;
  height: 36px;
  text-align: center;
  width: 100%;
`

const Contact = styled.p`
  color: #ffffff;
  font-size: 16pt;
  font-style: normal;
  line-height: 0;
  vertical-align: middle;
`

const SocialSection = styled.div`
  background-color: #f1f1f1;
  display: flex;
  flex-direction: row;
  padding: 24px 46px;
`

const SocialIconWrapper = styled.div`
  flex-grow: 1;
  height: 36px;
  text-align: center;
`

const SocialIcon = styled.img`
  flex-grow: 1;
  height: 100%;
`

const FooterSection = styled.div`
  background-color: #205493;
  padding: 26px;

  @media ${Device.tablet} {
    flex-direction: row;
    padding: 56px 96px 12px 96px;
  }
`

const FooterMenuWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 36px;
  width: 100%;

  @media ${Device.tablet} {
    flex-direction: row;
  }
`

const FooterMenu = styled.div`
  flex-grow: 1;
`

const FollowNlm = styled.p`
  color: #ffffff;
  font-size: 12pt;
  font-style: normal;
  line-height: 0;
`

const SmallIconWrapper = styled.div`
  flex-grow: 1;
  height: 42px;
`

const SmallIcon = styled.img`
  flex-grow: 1;
  height: 36px;
  padding: 6px;
`

const FooterNotes = styled.div`
  background-color: #205493;
  display: flex;
  flex-direction: column;
  width: 100%;
`

const MenuElement = styled.p`
  color: #ffffff;
  margin: 0 0 6px 0;
`

const Footer = () => (
  <Wrapper>
    <ContactSection>
      <Contact>CONTACT NCBI ON</Contact>
    </ContactSection>
    <SocialSection>
      <SocialIconWrapper>
        <SocialIcon alt="Twitter logo" src={Twitter} />
      </SocialIconWrapper>
      <SocialIconWrapper>
        <SocialIcon alt="Linkedin logo" src={Linkedin} />
      </SocialIconWrapper>
      <SocialIconWrapper>
        <SocialIcon alt="Youtube logo" src={Youtube} />
      </SocialIconWrapper>
      <SocialIconWrapper>
        <SocialIcon alt="Facebook logo" src={Facebook} />
      </SocialIconWrapper>
      <SocialIconWrapper>
        <SocialIcon alt="Github logo" src={Github} />
      </SocialIconWrapper>
      <SocialIconWrapper>
        <SocialIcon alt="Rss logo" src={Rss} />
      </SocialIconWrapper>
    </SocialSection>
    <FooterSection>
      <FooterMenuWrapper>
        <FooterMenu>
          <FollowNlm>Follow NLM</FollowNlm>
          <SmallIconWrapper>
            <SmallIcon
              alt="Facebook logo"
              src={FacebookW}
              style={{ paddingLeft: 0 }}
            />
            <SmallIcon alt="Twitter logo" src={TwitterW} />
            <SmallIcon
              alt="Youtube logo"
              src={YoutubeW}
              style={{ paddingRight: 0 }}
            />
          </SmallIconWrapper>
        </FooterMenu>
        <FooterMenu>
          <MenuElement>National Library of Medicine</MenuElement>
          <MenuElement>8600 Rockville Pike</MenuElement>
          <MenuElement>Bethesda, MD, 20894</MenuElement>
        </FooterMenu>
        <FooterMenu>
          <MenuElement>Copyright</MenuElement>
          <MenuElement>FOIA</MenuElement>
          <MenuElement>Privacy</MenuElement>
        </FooterMenu>
        <FooterMenu>
          <MenuElement>Help</MenuElement>
          <MenuElement>Accessibility</MenuElement>
          <MenuElement>Careers</MenuElement>
        </FooterMenu>
      </FooterMenuWrapper>
      <FooterNotes>
        <MenuElement style={{ textAlign: 'center' }}>
          NLM | NIH | HHS | USA.gov
        </MenuElement>
      </FooterNotes>
    </FooterSection>
  </Wrapper>
)

export default Footer
