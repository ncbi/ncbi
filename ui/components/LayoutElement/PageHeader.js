import React from 'react'
import styled from 'styled-components'
import Logo from '../../../public/top-logo.png'

const Wrapper = styled.div`
  background: #205493;
  display: flex;
  height: 46px;
  justify-content: space-between;
  width: 100%;
`

const TopLogo = styled.img`
  margin-left: 24px;
  margin-top: 5px;
  max-height: 36px;
`

const LoginLink = styled.a`
  color: white;
  cursor: pointer;
  font-size: large;
  margin: 9px;
  max-height: 36px;
`

const Header = () => (
  <Wrapper>
    <TopLogo alt="Logo" src={Logo} />
    <LoginLink href={`loginNcbi${window.location.search}`}>
      Login with myNCBI
    </LoginLink>
  </Wrapper>
)

export default Header
