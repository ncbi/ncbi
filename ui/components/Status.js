/* eslint-disable react/prop-types */
import React, { useContext } from 'react'
import { th } from '@pubsweet/ui-toolkit'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {
  ExclamationCircleOutlined,
  QuestionCircleFilled,
} from '@ant-design/icons'

import {
  WarningIcon,
  // QueryIcon,
  // ErrorIcon,
  ApproveIcon,
  ReviewIcon,
  ReviseIcon,
  PreviewIcon,
  NewVersionIcon,
  TechnicalReviewIcon,
  PublishedIcon,
  PublishingIcon,
  ArchivedIcon,
  InProductionIcon,
  NewFtpIcon,
  LockIcon,
  UnLockIcon,
  ConvertingIcon,
  UnPublishedIcon,
  // VendorIcon,
  OpenIssuesIcon,
} from './Icons'
import ConstantsContext from '../../app/constantsContext'

const ErrorIcon = styled(ExclamationCircleOutlined)`
  color: ${th('colorError')};
`

const VendorIcon = styled(QuestionCircleFilled)`
  color: ${th('colorWarning')};
`

const Root = styled.div`
  align-items: center;
  background: ${props => (props.hasBackground ? '#eee' : 'transparent')};
  display: flex;
  justify-content: center;
  padding: 8px;
`

const VerticalSeparator = styled.div`
  align-self: stretch;
  border-left: 2px solid ${th('colorArchive')};
  margin-left: 4px;
  margin-right: 4px;
`

const Number = styled.div`
  margin-left: 4px;
  margin-right: 4px;
`

const IconWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-right: 4px;
`

const EmptyIconWrapper = styled.div`
  display: flex;
  padding: 0;
`

/**
 * A component that can shows  Status of book and book components
 */
const Status = ({ className, status, hideText, ...props }) => {
  let icon, text
  const { locked, type } = props

  if (type === 'file') {
    icon = locked ? <LockIcon /> : <UnLockIcon />
  }

  const { s } = useContext(ConstantsContext)

  const [
    NewVersion, // 0
    Error, // 1
    Errors, // 2
    Query, // 3
    Warning, // 4
    Submitter, // 5
    Bookshelf, // 6
    BookshelfSubmitter, // 7
    TechnicalReview, // 8
    InProduction, // 9
    NewFtp, // 10
    Preview, // 11
    InReview, // 12
    Revise, // 13
    Approve, // 14
    Published, // 15
    Publishing, // 16
    Archived, // 17
    ReviewRequested, // 18
    RequestChanges, // 19
    Converting, // 20
    LoadingPreview, // 21
    NewUpload, // 22
    NewBook, // 23
    Production, // 24
    SubmissionErrors, // 25
    Tagging, // 26
    Failed, // 27
    ConversionErrors, // 28
    PreviewErrors, // 29
    PublishingFailed, // 30
    LoadingErrors, // 31
    NewCollection, // 32
    Unpublished, // 33
    Vendor, // 34
    OpenIssues, // 35
    OldBookVersion, // 36
    TaggingErrors, // 37
  ] = [
    ['new-version', 'status.newVersion'], // 0
    ['error', 'status.errors'], // 1
    ['errors', 'status.errors'], // 2
    ['query', 'status.query'], // 3
    ['warning', 'status.warning'], // 4
    ['submitter', 'status.submitter'], // 5
    ['bookshelf', 'status.bookshelf'], // 6
    ['bookshelfSubmitter', 'status.bookshelfSubmitter'], // 7
    ['technicalReview', 'status.technicalReview'], // 8
    ['in-production', 'status.inProduction'], // 9
    ['newFtp', 'status.newFtp'], // 10
    ['preview', 'status.preview'], // 11
    ['in-review', 'status.inReview'], // 12
    ['revise', 'status.revise'], // 13
    ['approve', 'status.approve'], // 14
    ['published', 'status.published'], // 15
    ['publishing', 'status.publishing'], // 16
    ['archived', 'status.archived'], // 17
    ['reviewRequested', 'status.reviewRequested'], // 18
    ['requestChanges', 'status.revise'], // 19
    ['converting', 'bookComponent.status.converting'], // 20
    ['loading-preview', 'bookComponent.status.loading-preview'], // 21 LoadingPreview
    ['new-upload', 'status.newUpload'], // 22 new Upload
    ['new-book', 'status.newBook'], // 23 new Book
    ['progress', 'status.inProduction'], // 24 new Book
    ['submission-errors', 'status.submissionErrors'], // 25 Submission errors
    ['tagging', 'status.tagging'], // 26 Tagging
    ['failed', 'status.failed'], // 27 Failed
    ['conversion-errors', 'status.conversionErrors'], // 28
    ['preview-errors', 'status.previewErrors'], // 29
    ['publish-failed', 'status.publishingFailed'], // 30
    ['loading-errors', 'status.loadingErrors'],
    ['new-collection', 'status.newCollection'], // 32
    ['unpublished', 'status.unpublished'], // 33
    ['vendor-issues', 'status.vendor'], // 34
    ['open-issues', 'status.vendor'], // 35
    ['old-book-version', 'status.oldBookVersion'], // 36
    ['tagging-errors', 'status.taggingErrors'], // 37
  ]

  switch (status) {
    case Error[0]:
      icon = !icon ? <ErrorIcon /> : icon
      text = s(Error[1])
      break
    case Errors[0]:
      icon = !icon ? <ErrorIcon /> : icon
      text = s(Errors[1])
      break

    case Query[0]:
      // icon = !icon ? <QueryIcon /> : icon
      icon = !icon ? <VendorIcon /> : icon
      text = s(Query[1])
      break
    case Warning[0]:
      icon = !icon ? <WarningIcon /> : icon
      text = s(Warning[1])
      break
    case Submitter[0]:
      icon = !icon ? <ErrorIcon /> : icon
      text = s(Submitter[1])
      break
    case Bookshelf[0]:
      icon = !icon ? <ErrorIcon /> : icon
      text = s(Bookshelf[1])
      break
    case BookshelfSubmitter[0]:
      icon = !icon ? <ErrorIcon /> : icon
      text = s(BookshelfSubmitter[1])
      break
    case TechnicalReview[0]:
      icon = !icon ? <TechnicalReviewIcon /> : icon
      text = s(TechnicalReview[1])
      break
    case InProduction[0]:
      icon = !icon ? <InProductionIcon /> : icon
      text = s(InProduction[1])
      break
    case NewFtp[0]:
      icon = !icon ? <NewFtpIcon /> : icon
      text = s(NewFtp[1])
      break
    case Preview[0]:
      icon = !icon ? <PreviewIcon /> : icon
      text = s(Preview[1])
      break
    case ReviewRequested[0]:
      icon = !icon ? <ReviewIcon /> : icon
      text = s(ReviewRequested[1])
      break
    case InReview[0]:
      text = s(InReview[1])

      if (!icon) {
        icon =
          props.revise || props.approved ? (
            <>
              <ReviewIcon /> <div>{text}</div> <VerticalSeparator />
              <Number>{props.approved}</Number> <ApproveIcon />
              <Number>{props.revise}</Number> <ReviseIcon />
            </>
          ) : (
            <>
              <ReviewIcon />
              {text}
            </>
          )

        text = ''
      }

      break
    case Revise[0]:
    case RequestChanges[0]:
      icon = !icon ? <ReviseIcon /> : icon
      text = s(Revise[1])
      break
    case Approve[0]:
      icon = !icon ? <ApproveIcon /> : icon
      text = s(Approve[1])
      break
    case NewVersion[0]:
      icon = !icon ? <NewVersionIcon /> : icon
      text = s(NewVersion[1])
      break

    case Published[0]:
      icon = !icon ? <PublishedIcon /> : icon
      text = s(Published[1])
      break
    case Publishing[0]:
      icon = !icon ? <PublishingIcon /> : icon
      text = s(Publishing[1])
      break
    case Archived[0]:
      icon = !icon ? <ArchivedIcon /> : icon
      text = s(Archived[1])
      break
    case Converting[0]:
      icon = !icon ? <ConvertingIcon /> : icon
      text = s(Converting[1])
      break
    case LoadingPreview[0]:
      icon = !icon ? <ConvertingIcon /> : icon
      text = s(LoadingPreview[1])
      break
    case NewUpload[0]:
      icon = !icon ? <NewVersionIcon /> : icon
      text = s(NewUpload[1])
      break
    case NewBook[0]:
      icon = !icon ? <NewVersionIcon /> : icon
      text = s(NewBook[1])
      break
    case Production[0]:
      icon = !icon ? <InProductionIcon /> : icon
      text = s(InProduction[1])
      break
    case SubmissionErrors[0]: // 25
      icon = !icon ? <ErrorIcon /> : icon
      text = s(SubmissionErrors[1])
      break
    case Tagging[0]: // 26
      icon = !icon ? <ConvertingIcon /> : icon
      text = s(Tagging[1])
      break
    case Failed[0]: // 27
      icon = !icon ? <ErrorIcon /> : icon
      text = s(Failed[1])
      break
    case ConversionErrors[0]: // 28
      icon = !icon ? <ErrorIcon /> : icon
      text = s(ConversionErrors[1])
      break
    case PreviewErrors[0]: // 29
      icon = !icon ? <ErrorIcon /> : icon
      text = s(PreviewErrors[1])
      break
    case PublishingFailed[0]: // 30
      icon = !icon ? <ErrorIcon /> : icon
      text = s(PublishingFailed[1])
      break
    case LoadingErrors[0]: // 31
      icon = !icon ? <ErrorIcon /> : icon
      text = s(LoadingErrors[1])
      break
    case NewCollection[0]: // 32
      icon = !icon ? <NewVersionIcon /> : icon
      text = s(NewCollection[1])
      break
    case Unpublished[0]: // 33
      icon = !icon ? <UnPublishedIcon /> : icon
      text = s(Unpublished[1])
      break
    case Vendor[0]: // 34
      icon = !icon ? <VendorIcon /> : icon
      text = s(Vendor[1])
      break
    case OpenIssues[0]: // 35
      icon = !icon ? <OpenIssuesIcon /> : icon
      text = s(OpenIssues[1])
      break
    case OldBookVersion[0]:
      icon = !icon ? <WarningIcon color="white" /> : icon

      text = <small>{s(OldBookVersion[1])}</small>

      break
    case TaggingErrors[0]: // 36
      icon = !icon ? <ErrorIcon /> : icon
      text = s(TaggingErrors[1])
      break
    default:
      icon = ''
      text = status
  }

  return !hideText ? (
    <Root {...props} className={className}>
      <IconWrapper>{icon}</IconWrapper>
      {text}
    </Root>
  ) : (
    <EmptyIconWrapper> {icon}</EmptyIconWrapper>
  )
}

Status.propTypes = {
  status: PropTypes.string.isRequired,
  hideText: PropTypes.bool,
}

Status.defaultProps = {
  hideText: false,
}

export default Status
