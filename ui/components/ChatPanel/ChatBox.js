/* eslint-disable no-param-reassign */
/* eslint-disable object-shorthand */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'
import { useQuery } from '@apollo/client'
import styled from 'styled-components'
import _ from 'lodash'

import ModalBodyUi from '../Modal/ModalBody'
import { Button, Spinner } from '../common'
import NextPageButton from '../NextPageButton'

import {
  Root,
  ContainerRight,
  FeedBackItemsContainer,
  StyledModal,
} from './styles'
import ChatItem from './ChatItem'
import ChatInput from './ChatInput'

const ModalBody = styled(ModalBodyUi)`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  margin-bottom: 0;
  max-height: none;
  padding-right: 0;
`

export default ({
  channelId,
  commentsList,
  commentSubscription,
  newComment,
  refetchQueries,
  showChatOptions,
  reviewStatus,
  users,
  title,
  hideFiles,
  disabled,
  disableOptions,
  onClickFileDownload,
}) => {
  if (users.currentUser)
    users.currentUser.rolesList = users.usersList
      .filter(i => i.id === users.currentUser.id)
      .map(i => ({ role: i.role }))

  const [modalIsOpen, setIsOpen] = useState(false)

  const { loading, data, subscribeToMore, fetchMore } = useQuery(commentsList, {
    fetchPolicy: 'network-only',
    variables: {
      channelId,
    },
  })

  // #region Add group messages by days
  const groupedArray = _.map(
    _.groupBy(data?.messages?.edges, props1 => props1.created.substring(0, 10)),
  )

  _.forEach(groupedArray, item => {
    const newItemDate = {
      created: item[0].created,
      type: 'date',
    }

    item.unshift(newItemDate)
  })

  const finalChatArray = [...new Set([].concat(...groupedArray))]
  // #endregion

  const scrollToBottom = () => {
    const main = document.getElementById('messages')

    if (!main || !data || !data.messages || data.messages.length === 0) {
      return
    }

    const { scrollHeight, clientHeight } = main
    main.scrollTop = scrollHeight - clientHeight
  }

  const firstMessage = data?.messages?.edges[0]

  const fetchMoreOptions = {
    updateQuery: (prev, { fetchMoreResult }) => {
      if (!fetchMoreResult) return prev
      return {
        ...prev,
        messages: {
          ...prev.messages,
          edges: [...fetchMoreResult.messages.edges, ...prev.messages.edges],
          pageInfo: fetchMoreResult.messages.pageInfo,
        },
      }
    },
    variables: {
      before: firstMessage && firstMessage.id,
      channelId: channelId,
    },
  }

  // eslint-disable-next-line no-shadow
  const subscribeToNewMessages = (subscribeToMore, channelId) =>
    subscribeToMore({
      document: commentSubscription,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev
        const { messageCreated } = subscriptionData.data

        const exists = prev.messages.edges.find(
          ({ id }) => id === messageCreated.id,
        )

        if (exists) return prev

        return {
          ...prev,
          messages: {
            ...prev.messages,
            edges: [...prev.messages.edges, messageCreated],
          },
        }
      },
      variables: { channelId: channelId },
    })

  useEffect(() => {
    if (modalIsOpen) {
      scrollToBottom()
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflow = 'unset'
    }
  }, [modalIsOpen])

  useEffect(() => {
    subscribeToNewMessages(subscribeToMore, channelId)
  })

  return (
    <Root>
      <Button
        data-test-id="show-feedback"
        disabled={disabled}
        icon={modalIsOpen ? 'chevron-down' : 'chevron-up'}
        iconPosition="end"
        onClick={e => {
          setIsOpen(!modalIsOpen)
        }}
        outlined
        status="primary"
      >
        {modalIsOpen ? `Hide ${title}` : `Show ${title}`}
      </Button>

      <StyledModal
        fullSize
        isOpen={modalIsOpen}
        style={{
          overlay: { position: 'static' },
          transform: 'translate: (0%, -50%)',
          right: '0px',
          top: '0px',
          bottom: '0px',
          height: 'calc(100vh-200px)',
        }}
        width="100%"
      >
        <ModalBody>
          {loading ? (
            <Spinner />
          ) : (
            <FeedBackItemsContainer id="messages">
              {data?.messages?.pageInfo.hasPreviousPage && (
                <NextPageButton
                  automatic={false}
                  fetchMore={() => fetchMore(fetchMoreOptions)}
                  isFetchingMore={false}
                >
                  Show previous messages
                </NextPageButton>
              )}
              {data?.messages && !data?.messages.edges.length && (
                <center>
                  No discussion here yet. Start by typing a message below.
                </center>
              )}
              {finalChatArray.map(item =>
                ChatItem(item, users, onClickFileDownload),
              )}
              <ChatInput
                channelId={channelId}
                disableOptions={disableOptions}
                hideFiles={hideFiles}
                newComment={newComment}
                refetchQueries={refetchQueries}
                reviewStatus={reviewStatus}
                showChatOptions={showChatOptions}
                users={users}
              />
            </FeedBackItemsContainer>
          )}
          <ContainerRight>
            <Button
              data-test-id="show-feedback"
              icon={modalIsOpen ? 'chevron-down' : 'chevron-up'}
              iconPosition="end"
              onClick={e => {
                setIsOpen(!modalIsOpen)
              }}
              outlined
              status="primary"
            >
              {modalIsOpen ? `Hide ${title}` : `Show ${title}`}
            </Button>
          </ContainerRight>
        </ModalBody>
      </StyledModal>
    </Root>
  )
}
