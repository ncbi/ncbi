/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-unreachable */
/* eslint-disable react/no-danger */
import React from 'react'
import moment from 'moment'
import { capitalize } from 'lodash'
import { Icon } from '../common'
import { Container, FeedBack, Author, Date, File, Action } from './styles'
import Status from '../Status'

// eslint-disable-next-line consistent-return
const formatComment = comment => {
  if (comment && comment !== '') {
    const regex = /@\[.+?\]\(.+?\)/gm
    const displayRegex = /@\[.+?\]/g
    const idRegex = /\(.+?\)/g
    const matches = comment.match(regex)
    const arr = []

    matches &&
      matches.forEach(m => {
        const id = m.match(idRegex)[0].replace('(', '').replace(')', '')
        let display = m
          .match(displayRegex)[0]
          .replace('@[', '')
          .replace(']', '')

        if (display.charAt(0) === '#') {
          display.replace('[', '')
        } else {
          display = `@${display}`
        }

        arr.push({ display, id })
      })

    const newComment = comment.split(regex)
    let output = ''

    for (let i = 0; i < newComment.length; i += 1) {
      const c = newComment[i]

      if (i === newComment.length - 1) output += c
      else {
        const { display, id } = arr[i]

        const correctDisplay = display.replace('[', '')
        let color = 'rgb(208, 229, 242)'

        if (display.charAt(0) === '#') {
          color = 'rgb(242, 213, 208)'
        }

        output += `${c}<span userId="${id}" style="background: ${color}; color: #686868">${correctDisplay}</span>`
      }
    }

    return output.replace(/\n\r?/g, '<br />')
  }
}

const [colorComment] = ['#686868']

const FeedBackItem = (item, users, onClickFileDownload) => {
  const { type, user, role, content, files, created } = item

  const foundRole =
    users?.usersList?.find(u => u.id === user?.id)?.roleName || ''

  const userRole = !role ? foundRole : role
  const formatedDate = moment(created).format('DD-MM-YYYY | HH:mm')
  const formatedComment = formatComment(content)
  const author = capitalize(user?.username)

  const fileComp =
    files?.length > 0 ? (
      <Container onClick={() => onClickFileDownload(item.files[0]?.id)}>
        <Icon color={colorComment} size={2}>
          paperclip
        </Icon>
        <File> {files && files[0]?.name}</File>
      </Container>
    ) : null

  switch (type) {
    case 'comment':
      return (
        <FeedBack data-test-id="feedback-container">
          <Container>
            <Author data-test-id="author-name">
              {author} {userRole ? `(${userRole})` : null}
            </Author>
            <Date>{formatedDate}</Date>
          </Container>
          <div
            dangerouslySetInnerHTML={{
              __html: formatedComment,
            }}
            data-test-id="message-content"
          />
          {fileComp}
        </FeedBack>
      )
      break
    case 'revise':
    case 'requestChanges':
      return (
        <FeedBack>
          <Container>
            <Author>
              {author} {userRole ? `(${userRole})` : null}
            </Author>
            <Date>{formatedDate}</Date>
          </Container>
          <Status status={type} />
          <div
            dangerouslySetInnerHTML={{
              __html: formatedComment,
            }}
          />
          {fileComp}
        </FeedBack>
      )
      break
    case 'in-review':
      return (
        <FeedBack>
          <Container>
            <Author>
              {author} {userRole ? `(${userRole})` : null}
            </Author>
            <Date>{formatedDate}</Date>
          </Container>
          <Status status="reviewRequested" />
          <div
            dangerouslySetInnerHTML={{
              __html: formatedComment,
            }}
          />
          {fileComp}
        </FeedBack>
      )
      break
    case 'approve':
      return (
        <FeedBack>
          <Container>
            <Author>
              {author} {userRole ? `(${userRole})` : null}
            </Author>
            <Date>{formatedDate}</Date>
          </Container>
          <Status status={type} />
        </FeedBack>
      )
      break

    case 'bot':
      return (
        <Action>
          <span>
            {content} {formatedDate}
          </span>
        </Action>
      )
      break
    case 'date':
      return (
        <Action>
          <span> {moment(created).format('ddd, DD MMM,YY')}</span>
        </Action>
      )
    default:
      return (
        <FeedBack>
          <Container>
            <Author>
              {author}
              {userRole ? `(${userRole})` : null}
            </Author>
            <Date>{formatedDate}</Date>
          </Container>
          <div
            dangerouslySetInnerHTML={{
              __html: formatedComment,
            }}
          />
          {fileComp}
        </FeedBack>
      )
  }
}

export default FeedBackItem
