import styled from 'styled-components'
import { Link as uiLink } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import Modal from 'react-modal'
import { Icon, Button } from '../common'
import uiModalBody from '../Modal/ModalBody'

export const Root = styled.div`
  font-family: ${th('fontInterface')};
`

export const PapperIcon = styled(Icon)`
  background: #eee;
  color: ${th('colorArchive')};
  margin: ${th('gridUnit')};
`

export const ModalBody = styled(uiModalBody)`
  box-shadow: -15px 0px calc(${th('gridUnit')}*3) 0px rgba(33, 33, 33, 0.15);
  display: flex;
  flex-direction: column;
  font-family: ${th('fontInterface')};
  height: inherit;
  justify-content: flex-end;
  margin-bottom: 0;
  max-height: calc(100vh - 170px);
  max-width: 700px;
  overflow-y: auto;
  padding-right: 0;
`

export const FeedBackItemsContainer = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: auto;
`

export const StyledModal = styled(Modal)`
  background: ${th('colorBackground')};
  border: 1px solid #eee;
  bottom: 0px;
  display: flex;
  flex-direction: column;
  font-family: ${th('fontInterface')};
  justify-content: flex-end;
  left: none;
  max-width: 37%;
  min-width: 35%;
  position: fixed;
  right: 0;
  top: 143px;
`

export const FeedBack = styled.div`
  align-items: start;
  color: ${th('colorText')};
  display: flex;
  flex-direction: column;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  line-height: calc(${th('gridUnit')}*3);
  margin-bottom: ${th('gridUnit')};

  div {
    word-break: break-word;
  }
`

export const FeedBackOptions = styled.div`
  color: ${th('colorText')};
  display: flex;
  flex-direction: column;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  line-height: calc(${th('gridUnit')}*3);
  margin-bottom: ${th('gridUnit')};
  margin-right: ${th('gridUnit')};
  margin-top: ${th('gridUnit')};
`

export const ChatContainer = styled.div`
  border-top: calc(${th('gridUnit')} / 4) solid #eee;
  display: flex;
  justify-content: space-between;
  margin-bottom: ${th('gridUnit')};
  width: 100%;
`

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`

export const ContainerRight = styled.div`
  display: flex;
  flex-direction: row-reverse;
  justify-content: end;
  margin-bottom: -${th('gridUnit')};
  margin-top: 5px;
  max-width: 100%;
  padding-right: calc(${th('gridUnit')}*3);
`

export const Author = styled.div`
  font-weight: 700;
  width: 50%;
`

export const Date = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-right: ${th('gridUnit')};
  width: 50%;
`

export const Type = styled.div`
  color: ${props => props.color};
  width: 100%;
`

export const File = styled.div`
  color: #686868;
  cursor: s-resize;
  display: flex;
  text-decoration: underline;
  width: 100%;
`

export const Label = styled.label`
  color: ${props => (props.disabled ? th('colorBorder') : th('colorText'))};
  cursor: ${props => (props.disabled ? 'disabled' : 'pointer')};
  display: flex;
`

export const Action = styled.div`
  border-bottom: 1px solid ${th('colorPrimary')};
  line-height: 0.1em;
  margin: ${th('gridUnit')} 0 calc(${th('gridUnit')}*4);
  text-align: center;
  width: 100%;

  span {
    background: ${th('colorTextPlaceholder')};
    color: ${th('colorPrimary')};
    font-weight: 600;
    padding: ${th('gridUnit')};
  }
`

export const VerticalContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  justify-content: space-between;
  margin-top: ${th('gridUnit')};
  position: relative;

  .mentions--multiLine .mentions__control {
    font-size: 14pt;
  }

  .mentions--multiLine .mentions__highlighter {
    border: 1px solid transparent;
    height: 160px;
    line-height: calc(${th('gridUnit')}*3);
    max-height: 160px;
    outline: 0;
    overflow: auto;
    padding: ${th('gridUnit')};
    padding-bottom: 40px;
    text-rendering: geometricPrecision;
  }

  textarea {
    border-radius: ${th('borderRadius')};
    height: 160px;
    max-height: 160px;
    overflow: auto;
    padding-bottom: 40px;
    resize: none;
  }

  .mentions--multiLine .mentions__input {
    border: 1px solid silver;
    height: 160px;
    line-height: calc(${th('gridUnit')}*3);
    max-height: 160px;
    outline: 0;
    overflow: auto;
    padding: ${th('gridUnit')};
    text-rendering: geometricPrecision;
  }

  .mentions__suggestions__list {
    background-color: white;
    border: 1px solid rgba(0, 0, 0, 0.15);
    font-size: 10pt;
  }

  .mentions__suggestions__item {
    border-bottom: 1px solid rgba(0, 0, 0, 0.15);
    padding: 5px 15px;
  }

  .mentions__suggestions__item--focused {
    background-color: #cee4e5;
  }

  .mentions__mention {
    color: blue;
    pointer-events: none;
    position: relative;
    text-decoration: underline;
    text-shadow: 1px 1px 1px white, 1px -1px 1px white, -1px 1px 1px white,
      -1px -1px 1px white;
    z-index: 1;
  }
`

export const ButtonsContainer = styled.div`
  align-content: flex-end;
  bottom: ${props =>
    props.hasFileUploaded && !props.hideFiles ? '30px' : ' 4px'};
  display: flex;
  justify-content: space-between;
  position: absolute;
  right: 4;
  width: 100%;
`

export const SendButton = styled(Button)`
  align-content: center;
  align-items: center;
  border-radius: ${th('borderRadius')};
  display: flex;
  height: 36px;
  justify-content: center;
  margin-right: ${th('gridUnit')};
  padding: 0 3px;
`

export const Line = styled.div`
  border-top: 1px solid ${th('colorPrimaryDarker')};
  height: 0px;
  margin-bottom: ${th('gridUnit')};
`

export const Link = styled(uiLink)`
  line-height: calc(${th('gridUnit')} * 2);
  text-align: left;
  text-transform: none;
  white-space: break-spaces;
`

export const FlexSpace = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  width: 100%;
`

export const InputWrapper = styled.div`
  align-items: flex-start;
  display: ${props => (props.hidden ? 'none' : 'flex')};
  margin-bottom: calc(${th('gridUnit')} / 2);

  label {
    color: ${th('colorText')};
    padding: 10px;
    width: 55%;
  }

  > div {
    width: 45%;
  }
`

export const ColumnWraper = styled.div`
  display: flex;
  flex-wrap: nowrap;
  padding: ${th('gridUnit')};
`

export const Column = styled.div`
  /* flex-grow: 1; */
  display: flex;
  flex: ${props => (props.grow === 0 ? '0 50%' : '0 30%')};
  flex-basis: ${props => props.flexBasis};
  flex-direction: column;
  flex-shrink: 1;
  padding-right: calc(${th('gridUnit')} * 5);
`
