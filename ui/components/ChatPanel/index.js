/* eslint-disable padding-line-between-statements */

export {
  Root,
  PapperIcon,
  StyledModal,
  FeedBack,
  FeedBackOptions,
  Container,
  Author,
  Date,
  Type,
  File,
  Action,
  VerticalContainer,
  ButtonsContainer,
  SendButton,
  ChatContainer,
  ContainerRight,
  ModalBody,
  Label,
  FeedBackItemsContainer,
  InputWrapper,
} from './styles'

export { default as ChatInput } from './ChatInput'
export { default as ChatItem } from './ChatItem'
export { default as ChatBox } from './ChatBox'
