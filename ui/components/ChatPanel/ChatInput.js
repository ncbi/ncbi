/* eslint-disable no-param-reassign */
/* eslint-disable object-shorthand */
/* eslint-disable react/prop-types */

import React, { useState } from 'react'
import { useMutation } from '@apollo/client'
import { getOperationName } from '@apollo/client/utilities'
import { Radio, ErrorText } from '@pubsweet/ui'
import { MentionsInput, Mention } from 'react-mentions'
import _ from 'lodash'
import useUploadFile from '../../../app/useUploadFile'

import {
  PapperIcon,
  FeedBackOptions,
  File,
  VerticalContainer,
  ButtonsContainer,
  SendButton,
  ChatContainer,
  Label,
} from './styles'

import { ApproveIcon, ReviewIcon, ReviseIcon } from '../Icons'
import { Spinner, Icon } from '../common'
import Notification from '../Notification'
import PermissionsGate from '../../../app/components/PermissionsGate'

const [RequestReview, RequestChanges, Approve, Comment] = [
  'in-review',
  'requestChanges',
  'approve',
  'comment',
]

export default ({
  channelId,
  newComment,
  refetchQueries,
  showChatOptions,
  reviewStatus,
  users,
  hideFiles,
  disableOptions,
  ...props
}) => {
  const [commentInput, setCommentInput] = useState('')
  const [checkedOption, setCheckedOption] = useState(Comment)
  const [, setmentionData] = useState()

  const {
    dispatch,
    getInputProps,
    getRootProps,
    fileList: [fileVersion],
  } = useUploadFile({
    validationFn: files => {
      return files
    },
  })

  const [newMessage, { loading, error }] = useMutation(newComment)

  const scrollToBottom = () => {
    const main = document.getElementById('messages')

    if (!main) {
      return
    }

    const { scrollHeight, clientHeight } = main
    main.scrollTop = scrollHeight - clientHeight
  }

  // function to handle mentions in the text area of chat
  const handleChange = (event, newValue, newPlainTextValue, mentions) => {
    setCommentInput(newValue)
    setmentionData({ newValue, newPlainTextValue, mentions })
  }

  const uniqueUsers = _.uniqBy(users.usersList, 'id')
  const uniqueRoles = _.uniqBy(users.rolesList, 'name')

  const OPTIONS = {
    '@': [...uniqueUsers, ...uniqueRoles],
  }

  // use ref for last element in the chat to make it scrollable to the end
  const [inputRef, setInputRef] = React.useState(null)
  const [emptyComment, setEmptyComment] = React.useState()

  const sendMessage = () => {
    if (
      (checkedOption === RequestChanges || checkedOption === Comment) &&
      commentInput === '' &&
      !fileVersion
    ) {
      setEmptyComment('Please write a comment')
      return
    }

    setCommentInput('')
    setmentionData({ newValue: null, newPlainTextValue: null, mentions: null })
    setCheckedOption(Comment)

    //   remove file
    if (fileVersion && fileVersion.name) {
      dispatch({
        payload: {
          name: fileVersion.name,
        },
        type: 'remove',
      })
    }

    newMessage({
      refetchQueries:
        refetchQueries &&
        refetchQueries.map(queryName => getOperationName(queryName)),
      variables: {
        channelId: channelId,
        content: commentInput,
        files: fileVersion && [
          {
            data: fileVersion,
          },
        ],
        type: checkedOption,
      },
    })

    inputRef && inputRef.focus()
  }

  React.useEffect(() => {
    if (inputRef) inputRef.focus()
  }, [inputRef])

  React.useEffect(() => {
    if (commentInput) setEmptyComment(null)
  }, [commentInput])

  React.useEffect(() => {
    if (checkedOption === RequestReview) {
      if (
        users.usersList.filter(
          x => x.role === 'previewer' || x.role === 'previewer',
        )?.length === 0
      ) {
        setCheckedOption(Comment)

        Notification({
          title: "Can't request review",
          message: 'Assign Previewers before requesting a review',
          type: 'danger',
        })
      }
    }
  }, [checkedOption])

  React.useEffect(() => {
    scrollToBottom()
  })

  const showReviewRequest = reviewStatus.status !== 'in-review'

  const currentUserResponse = (reviewStatus.reviewsResponse &&
    reviewStatus.reviewsResponse.find(
      item => item.userId === users.currentUser.id,
    )) || { decision: null }

  const hasNotRequestedChanges = currentUserResponse?.decision === null

  const hasNotApproved =
    currentUserResponse?.decision === null ||
    currentUserResponse?.decision === 'requestChanges'

  const showRevise =
    reviewStatus.status === 'in-review' &&
    !showReviewRequest &&
    hasNotRequestedChanges

  const showApprove =
    reviewStatus.status === 'in-review' && !showReviewRequest && hasNotApproved

  showChatOptions =
    showChatOptions && (showReviewRequest || showRevise || showApprove)

  return (
    <>
      {loading ? <Spinner size="small" /> : ''}

      {error ? 'An error happended during sending your message' : ''}

      {emptyComment ? <ErrorText>{emptyComment}</ErrorText> : ''}

      <ChatContainer>
        {showChatOptions ? (
          <FeedBackOptions>
            <PermissionsGate scopes={['view-book-component-request-review']}>
              {showReviewRequest && (
                <Label disabled={disableOptions.requestReview}>
                  <Radio
                    checked={checkedOption === RequestReview}
                    disabled={disableOptions.requestReview}
                    name="action"
                    onChange={event => setCheckedOption(RequestReview)}
                  />
                  <ReviewIcon /> Request Review
                </Label>
              )}
            </PermissionsGate>
            <PermissionsGate scopes={['view-book-component-revise']}>
              {showRevise && (
                <Label disabled={disableOptions.revise}>
                  <Radio
                    checked={checkedOption === RequestChanges}
                    disabled={disableOptions.revise}
                    name="action"
                    onChange={event => setCheckedOption(RequestChanges)}
                  />
                  <ReviseIcon />
                  Revise
                </Label>
              )}
            </PermissionsGate>
            <PermissionsGate scopes={['view-book-component-approve']}>
              {showApprove && (
                <Label disabled={disableOptions.approve}>
                  <Radio
                    checked={checkedOption === Approve}
                    disabled={disableOptions.approve}
                    name="action"
                    onChange={event => setCheckedOption(Approve)}
                  />
                  <ApproveIcon />
                  Approve
                </Label>
              )}
            </PermissionsGate>
            <Label>
              <Radio
                checked={checkedOption === Comment}
                name="action"
                onChange={event => setCheckedOption(Comment)}
              />
              Comment
            </Label>
          </FeedBackOptions>
        ) : null}

        <VerticalContainer fullWidth={!showChatOptions}>
          <span />

          <MentionsInput
            className="mentions"
            inputRef={node => {
              if (props.onRef) props.onRef(node)
              setInputRef(node)
            }}
            onChange={handleChange}
            placeholder="Type comment here, @mention to notify users"
            value={commentInput}
          >
            <Mention
              allowSuggestionsAboveCursor
              appendSpaceOnAdd
              className="mentions--multiLine mentions__control"
              data={OPTIONS['@']}
              displayTransform={(id, display) => `@${display}`}
              prefix="@"
              style={{
                background: 'rgb(208, 229, 242)',
              }}
              trigger="@"
            />

            {/* <Mention
            allowSuggestionsAboveCursor
            appendSpaceOnAdd
            data={OPTIONS['#']}
            displayTransform={(id, display) => `#${display}`}
            markup="@[#[__display__]__](id)"
            prefix="#"
            style={{
              background: 'rgb(242, 213, 208) ',
            }}
            trigger="#"
          /> */}
          </MentionsInput>

          <ButtonsContainer hideFiles={hideFiles}>
            <span
              disabled={!!fileVersion}
              {...getRootProps({
                className: 'dropzone',
                accept:
                  '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                multiple: false,
              })}
            >
              {' '}
              <input
                data-test-id="file-input-feedback"
                {...getInputProps({ multiple: false })}
                type="file"
              />
              {!hideFiles ? (
                <PapperIcon
                  background="#eee"
                  color="#686868"
                  disabled={!!fileVersion}
                  size={3}
                >
                  paperclip
                </PapperIcon>
              ) : (
                <span> </span>
              )}
            </span>

            <File>
              {fileVersion?.name && (
                <div>
                  {fileVersion.name}

                  <Icon
                    color="red"
                    onClick={() => {
                      dispatch({
                        payload: {
                          name: fileVersion.name,
                        },
                        type: 'remove',
                      })
                    }}
                  >
                    x
                  </Icon>
                </div>
              )}
            </File>
            <PermissionsGate scopes={['view-review-comment-panel']}>
              <SendButton
                data-test-id="send-feedback"
                onClick={() => sendMessage()}
                status="primary"
              >
                Send
              </SendButton>
            </PermissionsGate>
          </ButtonsContainer>
        </VerticalContainer>
      </ChatContainer>
    </>
  )
}
