import { css } from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

/* All styles regarding ProseMirror surface and elements */

export default css`
  .ProseMirror {
    border-top: none;
    color: ${th('colorText')};
    counter-reset: footnote 0;
    font-family: ${th('fontWriting')};
    font-feature-settings: 'liga' 0;
    font-size: ${th('fontSizeBase')};
    font-variant-ligatures: none;
    line-height: 12px;
    overflow-x: auto;
    white-space: pre;
    width: 497px;
    word-wrap: break-word;

    p::selection,
    h1::selection,
    h2::selection,
    h3::selection,
    code::selection,
    span::selection,
    p span::selection,
    h1 span::selection,
    h2 span::selection,
    h3 span::selection,
    code span::selection title::selection {
      background-color: transparent;
    }

    &:focus {
      outline: none;
    }
  }

  .ProseMirror .wax-selection-marker {
    background-color: ${th('colorSelection')};
  }

  div[contenteditable='false'] {
    pointer-events: none;
    user-select: none;
  }

  .ProseMirror title {
    display: inline;
    font-size: 14px;
  }

  ul,
  ol {
    padding-left: 30px;
  }

  sup,
  sub {
    line-height: 0;
  }
`
