/* eslint-disable react/prop-types */
import React from 'react'
import { Wax } from 'wax-prosemirror-core'
import configAbstract from './configAbstract'
import configAffiliations from './configAffiliations'
import configNotes from './configNotes'
import configInline from './configInline'
import configIssues from './configIssues'
import configInlineStatement from './configInlineStatement'
import configInlineLicenseStatement from './configInlineLicenseStatement'
import config from './config'
import layout from './layout'
import layoutInline from './layoutInline'

const Editor = ({
  onChange,
  value,
  editorType,
  autocompleteReducer,
  ...rest
}) => {
  let configuration = config
  let layoutEditor = layout

  switch (editorType) {
    case 'inline':
      configuration = configInline
      layoutEditor = layoutInline
      break
    case 'statement':
      configuration = configInlineStatement
      layoutEditor = layoutInline

      break
    case 'licenseStatement':
      configuration = configInlineLicenseStatement
      layoutEditor = layoutInline

      break
    case 'abstract':
      configuration = configAbstract
      break
    case 'affiliations':
      configuration = configAffiliations
      break
    case 'notes':
      configuration = configNotes
      break
    case 'issues':
      configuration = configIssues(autocompleteReducer)
      break
    default:
      configuration = config
  }

  return (
    <Wax
      className={editorType}
      config={configuration}
      layout={layoutEditor}
      onChange={data => onChange(data)}
      value={value}
      {...rest}
    />
  )
}

export default Editor
