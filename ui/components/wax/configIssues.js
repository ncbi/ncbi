import { DefaultSchema } from 'wax-prosemirror-utilities'
import {
  InlineAnnotationsService,
  AnnotationToolGroupService,
  ListsService,
  ListToolGroupService,
  BaseService,
  BaseToolGroupService,
  LinkService,
  TrackChangeService,
  CommentsService,
  CodeBlockService,
  CodeBlockToolGroupService,
} from 'wax-prosemirror-services'
import { WaxSelectionPlugin } from 'wax-prosemirror-plugins'
import invisibles, { hardBreak } from '@guardian/prosemirror-invisibles'
import autocomplete from 'prosemirror-autocomplete'
import MentionService from './MentionService'

const configIssues = autocompleteReducer => {
  const options = {
    reducer: autocompleteReducer,
    triggers: [
      {
        name: 'mention',
        trigger: /(@)$/,
        decorationAttrs: { class: 'mention-tag' },
      },
    ],
  }

  return {
    MenuService: [
      {
        templateArea: 'topBar',
        toolGroups: [
          {
            name: 'Base',
            exclude: ['Save'],
          },
          {
            name: 'Annotations',
            exclude: ['StrikeThrough', 'Underline', 'SmallCaps'],
          },
          'CodeBlock',
          'Lists',
        ],
      },
    ],
    OrderedListService: { subList: false },
    BulletListService: { subList: false },
    JoinUpService: { subList: false },
    RulesService: [],
    ShortCutsService: {},
    LinkService: {},
    MentionService: {},
    SchemaService: DefaultSchema,
    PmPlugins: [
      WaxSelectionPlugin,
      invisibles([hardBreak()]),
      ...autocomplete(options),
    ],

    services: [
      new MentionService(),
      new InlineAnnotationsService(),
      new AnnotationToolGroupService(),
      new LinkService(),
      new ListToolGroupService(),
      new BaseService(),
      new TrackChangeService(),
      new CommentsService(),
      new CodeBlockService(),
      new CodeBlockToolGroupService(),

      new ListsService(),
      new BaseToolGroupService(),
    ],
  }
}

export default configIssues
