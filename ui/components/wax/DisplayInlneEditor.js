import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { Wax } from 'wax-prosemirror-core'

import configInline from './configInline'

const WrapperEdditor = styled.div`
  display: inline-block;

  .ProseMirror {
    color: ${props => th(props.color)};
  }
`

const Wrapper = styled.div`
  display: inline;

  > div {
    display: inline;
  }

  .ProseMirror {
    border-top: none;
    counter-reset: footnote 0;
    font-feature-settings: 'liga' 0;
    font-size: ${th('fontSizeBase')};
    font-variant-ligatures: none;
    line-height: 12px;
    overflow-x: auto;
    white-space: pre;
    width: 497px;
    word-wrap: break-word;
  }
`

/* eslint-disable-next-line react/prop-types */
const Layout = ({ editor }) => {
  return <Wrapper>{editor}</Wrapper>
}

const DisplayInlineEditor = props => {
  const { content, color } = props
  return (
    <WrapperEdditor color={color}>
      <Wax
        color={color}
        config={configInline}
        layout={Layout}
        readonly
        value={content}
      />
    </WrapperEdditor>
  )
}

DisplayInlineEditor.propTypes = {
  content: PropTypes.string,
  color: PropTypes.string,
}

DisplayInlineEditor.defaultProps = {
  content: '',
  color: 'colorText',
}

export default DisplayInlineEditor
