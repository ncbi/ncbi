import {
  InlineAnnotationsService,
  AnnotationToolGroupService,
  BaseService,
  BaseToolGroupService,
  LinkService,
} from 'wax-prosemirror-services'

import { WaxSelectionPlugin } from 'wax-prosemirror-plugins'
import invisibles, { hardBreak } from '@guardian/prosemirror-invisibles'

const configInline = {
  MenuService: [
    {
      templateArea: 'topBar',
      toolGroups: [
        {
          name: 'Base',
          exclude: ['Save'],
        },
        {
          name: 'Annotations',
          exclude: [
            'Code',
            'LinkTool',
            'StrikeThrough',
            'Underline',
            'SmallCaps',
          ],
        },
      ],
    },
  ],

  SchemaService: {
    nodes: {
      doc: {
        content: 'inline*',
      },
      text: {
        group: 'inline',
      },
      title: {
        group: 'inline',
        content: 'inline*',
        inline: true,
        parseDOM: [
          {
            tag: 'title',
          },
        ],
        toDOM(node) {
          return ['title', node.attrs, 0]
        },
      },
    },
    marks: {},
  },
  OrderedListService: { subList: false },
  BulletListService: { subList: false },
  JoinUpService: { subList: false },
  RulesService: [],
  ShortCutsService: {},
  LinkService: {},
  PmPlugins: [WaxSelectionPlugin, invisibles([hardBreak()])],

  services: [
    new InlineAnnotationsService(),
    new AnnotationToolGroupService(),
    new LinkService(),
    new BaseService(),
    new BaseToolGroupService(),
  ],
}

export default configInline
