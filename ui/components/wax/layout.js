/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { ComponentPlugin } from 'wax-prosemirror-core'
import { grid, th } from '@pubsweet/ui-toolkit'
import EditorElements from './EditorElements'
import 'wax-prosemirror-core/dist/index.css'

const WaxOverlays = ComponentPlugin('waxOverlays')

const Wrapper = styled.div`
  background: ${th('colorBackground')};
  border: 1px solid grey;
  clear: both;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  max-width: 100%;
  position: relative;
`

const Main = styled.div`
  display: flex;
  max-width: 100%;
  position: relative;
`

const TopMenu = styled.div`
  background: ${th('colorTextPlaceholder')};
  display: inline-flex;
  flex-grow: 1;
  height: 32px;
  margin-right: 1px;
  max-width: 100%;
  user-select: none;
  z-index: 999;

  > div:not(:last-child) {
    border: none;
  }
`

const EditorArea = styled.div`
  flex-grow: 1;
  max-width: 100%;
  position: relative;
`

const WaxSurfaceScroll = styled.div`
  border: none;
  box-sizing: border-box;
  display: flex;
  height: 100%;
  max-width: 100%;
  overflow-y: auto;
  position: relative;

  /* PM styles for main content*/

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${EditorElements}
`

const EditorContainer = styled.div`
  font-feature-settings: 'tnum';
  font-variant: tabular-nums;
  height: 100%;
  line-height: 1.5715;
  max-width: 100%;
  min-width: 99%;
  position: relative;

  .ProseMirror {
    border-top: none;
    box-sizing: border-box;
    counter-reset: footnote 0;
    font-feature-settings: 'liga' 0;
    font-size: ${th('fontSizeBase')};
    font-variant-ligatures: none;
    line-height: 12px;
    margin-right: ${grid(1)};
    max-width: 100%;
    min-height: 100%;
    min-width: 99%;
    overflow-wrap: break-word;
    overflow-x: auto;
    padding: ${grid(1)};
    position: relative;
    white-space: break-spaces;
    width: 100%;
    word-break: break-word;

    p {
      max-width: 100%;
      min-width: 0;
      word-break: break-word;
    }

    code {
      background-color: #f1f1f1;
      color: crimson;
      padding: 2px;
    }

    span.mention-tag {
      background-color: #b6d2f3;
      border-radius: 5px;
      color: ${th('colorPrimary')};
    }
  }

  /* invisible characters */
  .invisible {
    pointer-events: none;
    user-select: none;
  }

  .invisible:before {
    caret-color: inherit;
    color: gray;
    display: inline-block;
    font-style: normal;
    font-weight: 400;
    line-height: 1em;
    width: 0;
  }

  .invisible--break:before {
    color: red;
    content: '¬';
    font-size: 20px;
    font-weight: bold;
  }

  .ProseMirror[contenteditable='false'] {
    width: 100%;
  }
`

const TopBar = ComponentPlugin('topBar')

// pending fix in wax
const OverlayWrapper = styled.div`
  z-index: 9;
`

const Layout = ({ editor }) => (
  <Wrapper>
    <TopMenu>
      <TopBar />
    </TopMenu>
    <Main>
      <EditorArea>
        <WaxSurfaceScroll>
          <EditorContainer>{editor}</EditorContainer>
          <OverlayWrapper>
            <WaxOverlays />
          </OverlayWrapper>
        </WaxSurfaceScroll>
      </EditorArea>
    </Main>
  </Wrapper>
)

export default Layout
