/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import styled from 'styled-components'
import { EditorComponent as Editor } from '../FormElements/FormikElements'

const EditorWrapper = styled.div`
  position: relative;
`

const Suggestions = styled.div`
  background-color: white;
  left: 24px;
  margin-top: 14px;
  min-width: 100px;
  position: absolute;
  top: 8px;
  z-index: 99999;
`

const List = styled.ul`
  background-color: 'white';
  border: '1px solid rgba(0,0,0,0.15)';
  font-size: 14;
  list-style: none;
  margin: 0;
  padding: 0;
  width: 100%;

  .mentions__suggestions__item {
    border-bottom: 1px solid rgba(0, 0, 0, 0.15);
    cursor: pointer;
    padding: 5px 15px;
  }

  .mentions__suggestions__item--focused {
    background-color: #bbdaff;
  }
`

const ActionKind = {
  open: 'open',
  close: 'close',
  filter: 'filter',
  up: 'ArrowUp',
  down: 'ArrowDown',
  left: 'ArrowLeft',
  right: 'ArrowRight',
  enter: 'enter',
}

const EditorMentions = ({
  field: { setFieldValue, name, value },
  label,
  mentionsList,
  readOnly,
  id,
  key,
}) => {
  let filter = ''
  let currentIndex = 0

  const [stateFilter, setFilter] = useState()
  const [listStyle, setListStyle] = useState({ top: `${0}px`, left: `${0}px` })

  const [picker, setPicker] = useState({
    view: null,
    open: false,
    current: 0,
    range: null,
  })

  let filteredList = mentionsList

  let NUM_SUGGESTIONS = filteredList.length

  stateFilter
    ? (filteredList = mentionsList.filter(x =>
        x.username.includes(stateFilter),
      ))
    : (filteredList = mentionsList)

  NUM_SUGGESTIONS = filteredList.length

  // show items, handle positioning
  const placeSuggestion = () => {
    const rect = document
      .getElementsByClassName('autocomplete')[0]
      .getBoundingClientRect()

    const editorDescription = document
      .getElementById(`${id}`)
      .getBoundingClientRect()

    setListStyle({
      top: `${rect.top - editorDescription.top}px`,
      left: `${rect.left - editorDescription.left + 16}px`,
    })
  }

  const handleClickOption = item => {
    const { from, to } = picker.range

    const tr = picker.view.state.tr
      .deleteRange(from, to) // This is the full selection
      .insertText(`@${item.username} `)
      .addMark(
        from,
        from + item.username.length + 1,
        picker.view.state.schema.marks.mention_tag.create({
          class: 'mention-tag',
          id: item.id,
        }),
      )

    picker.view.dispatch(tr)
    picker.view.focus()

    setPicker({
      ...picker,
      open: false,
    })

    filter = ''
    setFilter('')
    currentIndex = 0
    return true
  }

  // mentions reducer
  const autocompleteReducer = action => {
    switch (action.kind) {
      case ActionKind.open:
        setPicker({
          view: action.view,
          current: 0,
          open: true,
          range: action.range,
        })

        placeSuggestion(action)

        return true
      case ActionKind.close:
        currentIndex = 0

        setPicker({
          ...picker,
          current: 0,
          view: action.view,
          open: false,
        })

        return true
      case ActionKind.filter:
        currentIndex = 0
        filter = action.filter
        setFilter(action.filter)
        return true

      case ActionKind.up: {
        filter
          ? (filteredList = mentionsList.filter(x =>
              x.username.includes(filter),
            ))
          : (filteredList = mentionsList)

        NUM_SUGGESTIONS = filteredList.length

        currentIndex -= 1
        currentIndex += NUM_SUGGESTIONS // negative modulus doesn't work
        currentIndex %= NUM_SUGGESTIONS

        setPicker({
          open: true,
          range: action.range,
          view: action.view,
          current: currentIndex,
        })

        return true
      }

      case ActionKind.down: {
        filter
          ? (filteredList = mentionsList.filter(x =>
              x.username.includes(filter),
            ))
          : (filteredList = mentionsList)

        NUM_SUGGESTIONS = filteredList.length

        currentIndex += 1

        currentIndex %= NUM_SUGGESTIONS

        setPicker({
          open: true,
          range: action.range,
          view: action.view,
          current: currentIndex,
        })

        return true
      }

      case ActionKind.enter: {
        filter
          ? (filteredList = mentionsList.filter(x =>
              x.username.includes(filter),
            ))
          : (filteredList = mentionsList)

        NUM_SUGGESTIONS = filteredList.length

        const item = filteredList[currentIndex]
        const oldFrom = action.range.from

        const tr = action.view.state.tr
          .deleteRange(action.range.from, action.range.to)
          .insertText(`@${item.username} `)

        action.view.dispatch(
          tr.addMark(
            oldFrom,
            oldFrom + item.username.length + 1,
            action.view.state.schema.marks.mention_tag.create({
              class: 'mention-tag',
              id: item.id,
            }),
          ),
        )

        action.view.focus()
        setFilter('')
        filter = ''

        return true
      }

      default:
        return false
    }
  }

  return (
    <EditorWrapper id={id}>
      {picker.open && (
        <Suggestions style={listStyle}>
          <List
            className="mentions__suggestions__list"
            id="suggestion"
            role="listbox"
          >
            {filteredList.map((item, i) => (
              <li
                aria-selected="false"
                className={`mentions__suggestions__item ${
                  picker.current === i && 'mentions__suggestions__item--focused'
                }`}
                id="aeae9ce74b08e-0"
                key={item.id}
                onClick={() => handleClickOption(item)}
                onKeyPress={() => handleClickOption(item)}
                role="option"
              >
                {item.username}
              </li>
            ))}
          </List>
        </Suggestions>
      )}
      <Editor
        autocompleteReducer={autocompleteReducer}
        editorType="issues"
        field={{ name, value }}
        form={{ setFieldValue }}
        key={key}
        label={label}
        readonly={readOnly}
      />
    </EditorWrapper>
  )
}

export default EditorMentions
