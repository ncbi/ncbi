import React from 'react'
import { Wax } from 'wax-prosemirror-core'
import configInline from './configInline'
import config from './config'
import layout from './layout'

// eslint-disable-next-line react/prop-types
const Editor = ({ onChange, value, inline, ...rest }) => {
  return (
    <Wax
      config={inline ? configInline : config}
      layout={layout}
      onChange={data => onChange(data)}
      value={value}
      {...rest}
    />
  )
}

export default Editor
