import { DefaultSchema } from 'wax-prosemirror-utilities'
import {
  InlineAnnotationsService,
  AnnotationToolGroupService,
  ListsService,
  ListToolGroupService,
  BaseService,
  BaseToolGroupService,
  LinkService,
} from 'wax-prosemirror-services'
import { WaxSelectionPlugin } from 'wax-prosemirror-plugins'
import invisibles, { hardBreak } from '@guardian/prosemirror-invisibles'

const configInline = {
  MenuService: [
    {
      templateArea: 'topBar',
      toolGroups: [
        {
          name: 'Base',
          exclude: ['Save'],
        },
        {
          name: 'Annotations',
          exclude: [
            'Code',
            'Superscript',
            'Subscript',
            'StrikeThrough',
            'Underline',
            'SmallCaps',
          ],
        },
        'Lists',
      ],
    },
  ],
  OrderedListService: { subList: false },
  BulletListService: { subList: false },
  JoinUpService: { subList: false },
  RulesService: [],
  ShortCutsService: {},
  LinkService: {},
  SchemaService: DefaultSchema,
  PmPlugins: [WaxSelectionPlugin, invisibles([hardBreak()])],

  services: [
    new InlineAnnotationsService(),
    new AnnotationToolGroupService(),
    new LinkService(),
    new ListToolGroupService(),
    new BaseService(),
    new ListsService(),
    new BaseToolGroupService(),
  ],
}

export default configInline
