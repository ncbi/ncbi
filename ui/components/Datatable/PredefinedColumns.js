/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React from 'react'
import styled from 'styled-components'
import { Checkbox as UICheckbox } from '../common'

const Checkbox = styled(UICheckbox)`
  padding-left: 1px;
`

const CheckboxHead = styled.div`
  padding-bottom: 7px;
`

const headerRendererCheck = (loadedData, columnProps) => () => {
  const check =
    loadedData.length > 0
      ? loadedData.every(item => item.checked === true)
      : false

  return (
    <CheckboxHead data-test-id="select-all">
      <Checkbox
        checked={check}
        name="checkbox"
        onClick={event => {
          const toggleCheck = !check
          event.target.checked = toggleCheck

          const data = loadedData.map(obj => {
            obj.checked = toggleCheck
            return obj
          })

          columnProps.checkBoxFn(data)
        }}
      />
    </CheckboxHead>
  )
}

const cellRendererCheck = (loadedData, columnProps) => ({
  cellData,
  rowData,
}) => {
  const index = loadedData.findIndex(item => item.id === cellData)
  const isChecked = loadedData[index].checked

  const testId =
    (rowData.title && rowData.title.replace(/\s/g, '')) ||
    (rowData.name && rowData.name.replace(/\s/g, '')) ||
    (rowData.username && rowData.username.replace(/\s/g, '')) ||
    rowData.id

  return (
    <Checkbox
      checked={loadedData[index].checked}
      data-test-id={`checkbox-${testId}`}
      name="checkbox"
      onClick={() => {
        loadedData[index].checked = !isChecked
        columnProps.checkBoxFn(loadedData[index])
      }}
    />
  )
}

export default {
  cells: {
    checkbox: cellRendererCheck,
  },
  headers: {
    checkbox: headerRendererCheck,
  },
}
