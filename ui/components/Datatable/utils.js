/* eslint-disable react/prop-types */
import * as predefined from './PredefinedColumns'

export const calculateColumnProps = (data, props) => {
  const columnProps = {
    columnData: props.columnData || {},
    dataKey: props.dataKey,
    disableSort: props.disableSort,
    dynamicHeight: props.dynamicHeight || false,
    key: props.dataKey,
    label: props.label,
    width: props.width || 'auto',
  }

  if (props.flexGrow !== undefined) {
    columnProps.flexGrow = props.flexGrow
  }

  if (props.flexShrink !== undefined) {
    columnProps.flexShrink = props.flexShrink
  }

  if (props.headerRowRenderer) {
    const headerRowRenderer = predefined.default.headers[
      props.headerRowRenderer
    ]
      ? predefined.default.headers[props.headerRowRenderer]
      : props.headerRowRenderer

    columnProps.headerRowRenderer = headerRowRenderer(data, props)
  }

  if (props.headerRenderer) {
    const headerRenderer = predefined.default.headers[props.headerRenderer]
      ? predefined.default.headers[props.headerRenderer]
      : props.headerRenderer

    columnProps.headerRenderer = headerRenderer(data, props)
  }

  if (props.cellRenderer) {
    const cellRenderer = predefined.default.cells[props.cellRenderer]
      ? predefined.default.cells[props.cellRenderer]
      : props.cellRenderer

    columnProps.cellRenderer = cellRenderer(data, props)
  }

  return columnProps
}

export const calculatePages = (totalRecords, recordsPerPage) =>
  Math.ceil(totalRecords / recordsPerPage)
