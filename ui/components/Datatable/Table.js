/* eslint-disable no-empty-pattern */
/* eslint-disable react/prop-types */
/* eslint-disable no-param-reassign */
/* eslint-disable jsx-a11y/label-has-for */
import React from 'react'
import { th } from '@pubsweet/ui-toolkit'
import {
  InfiniteLoader,
  Table as Tablet,
  AutoSizer,
  Column,
  CellMeasurerCache,
} from 'react-virtualized'
import 'react-virtualized/styles.css'

import { uniqueId } from 'lodash'

import styled from 'styled-components'

import { calculateColumnProps } from './utils'
import CellMeasurer from './CellMeasurer'

// #region styled-components
const Root = styled.div`
  height: 100%;
`

const NoData = styled.div`
  color: #939393;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  margin: 20px;
  text-align: center;
  text-transform: capitalize;
`

const TableWrapper = styled.div`
  height: 100%;

  .ReactVirtualized__Table {
    background-color: ${th('colorBackground')};
    color: ${th('colorText')};
    margin-bottom: 20px;
  }

  .ReactVirtualized__Table__headerRow {
    border-bottom: 1px solid #212121;
    font-family: ${th('fontInterface')};
    font-size: 11pt;
    font-weight: 600;
    padding-top: ${th('gridUnit')};
    text-transform: none;
  }

  .ReactVirtualized__Table__headerColumn {
    margin-left: 0;
    margin-right: 0;
    padding-left: calc(${th('gridUnit')} * 2);
  }

  .ReactVirtualized__Table__row {
    border-bottom: 1px solid #eee;
  }

  .ReactVirtualized__Table__row :hover {
    background: #f4f4f4b5;
    cursor: pointer;
  }
`

const AutoSizerWrapper = styled.div`
  display: block;
  flex: 1 1 auto;
  height: 100%;
`

const TableContainer = styled.div`
  .ReactVirtualized__Table__headerColumn,
  .ReactVirtualized__Table__rowColumn {
    border-left: none;
    height: 100%;
    margin: 0;
    padding-left: calc(${th('gridUnit')} * 2);
    width: 100%;
  }

  .ReactVirtualized__Table__rowColumn > div {
    justify-content: left;
  }

  .ReactVirtualized__Table__rowColumn {
    align-items: center;
    display: flex;
  }

  .ReactVirtualized__Table__rowColumn > span {
    padding-left: 0;
  }

  .ReactVirtualized__Table__headerColumn:first-of-type,
  .ReactVirtualized__Table__rowColumn:first-of-type {
    margin-left: 0;
    max-width: 52px;
  }

  .noCells {
    align-items: center;
    bottom: 0;
    color: #bdbdbd;
    display: flex;
    font-size: 1em;
    justify-content: center;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
  }

  .ReactVirtualized__Table__row,
  .ReactVirtualized__Table__Grid {
    outline: none;
  }

  .ReactVirtualized__Table__row {
    border-bottom: 1px solid #e4e4e4;
  }
`

// #endregion

const ROW_HEIGHT = 44

const Table = ({
  columns,
  count,
  infinityScroll,
  loadedData,
  mergeDataVariables,
  noDataMessage,
  onColumnClick,
  onRowClick,
  rowStyle,
}) => {
  return (
    <Root>
      {infinityScroll && (
        <InfiniteLoader
          isRowLoaded={({ index }) => !!loadedData[index]}
          loadMoreRows={({ startIndex, stopIndex }) => {
            const take = stopIndex - startIndex
            return mergeDataVariables({ input: { skip: startIndex, take } })
          }}
          rowCount={count}
          threshold={4}
        >
          {({ onRowsRendered, registerChild }) => {
            const deferredMeasurementCache = new CellMeasurerCache({
              fixedWidth: true,
              minHeight: ROW_HEIGHT,
            })

            return (
              <AutoSizerWrapper>
                <AutoSizer>
                  {({ width, height }) => (
                    <TableContainer>
                      <Tablet
                        headerHeight={ROW_HEIGHT}
                        height={height}
                        noRowsRenderer={() => (
                          <NoData>
                            {' '}
                            {noDataMessage ||
                              ' No data available in table'}{' '}
                          </NoData>
                        )}
                        onColumnClick={onColumnClick}
                        onRowClick={({ event, index, rowData }) => {
                          onRowClick({
                            data: loadedData,
                            event,
                            index,
                            rowData,
                          })
                        }}
                        // onRowsRendered={props => {
                        //   handleRowsScroll(props)
                        // }}
                        rowCount={loadedData.length}
                        rowGetter={({ index }) => loadedData[index]}
                        rowHeight={
                          deferredMeasurementCache
                            ? deferredMeasurementCache.rowHeight
                            : ROW_HEIGHT
                        }
                        rowStyle={rowStyle}
                        width={width - 2}
                      >
                        {columns.map(column => {
                          const columnProps = calculateColumnProps(
                            loadedData,
                            column,
                          )

                          if (columnProps.dynamicHeight) {
                            columnProps.cellRenderer = CellMeasurer(
                              columnProps.cellRenderer,
                              deferredMeasurementCache,
                            )
                          }

                          return (
                            <Column
                              key={uniqueId('column-')}
                              {...columnProps}
                              width={Number(columnProps.width)}
                            />
                          )
                        })}
                      </Tablet>
                    </TableContainer>
                  )}
                </AutoSizer>
              </AutoSizerWrapper>
            )
          }}
        </InfiniteLoader>
      )}
      {!infinityScroll && (
        <TableWrapper>
          <AutoSizerWrapper>
            <AutoSizer>
              {({ width, height }) => {
                const deferredMeasurementCache = new CellMeasurerCache({
                  fixedWidth: true,
                  minHeight: ROW_HEIGHT,
                })

                return (
                  <TableContainer>
                    <Tablet
                      headerHeight={ROW_HEIGHT}
                      height={height}
                      noRowsRenderer={() => (
                        <NoData>
                          {' '}
                          {noDataMessage || ' No data available in table'}{' '}
                        </NoData>
                      )}
                      onColumnClick={onColumnClick}
                      onRowClick={({ event, index, rowData }) => {
                        onRowClick({
                          data: loadedData,
                          event,
                          index,
                          rowData,
                        })
                      }}
                      rowCount={loadedData.length}
                      rowGetter={({ index }) => loadedData[index]}
                      rowHeight={
                        deferredMeasurementCache
                          ? deferredMeasurementCache.rowHeight
                          : ROW_HEIGHT
                      }
                      rowStyle={rowStyle}
                      width={width}
                    >
                      {columns.map(column => {
                        const columnProps = calculateColumnProps(
                          loadedData,
                          column,
                        )

                        if (columnProps.dynamicHeight) {
                          columnProps.cellRenderer = CellMeasurer(
                            columnProps.cellRenderer,
                            deferredMeasurementCache,
                          )
                        }

                        return (
                          <Column
                            key={uniqueId('column-')}
                            {...columnProps}
                            width={Number(columnProps.width)}
                          />
                        )
                      })}
                    </Tablet>
                  </TableContainer>
                )
              }}
            </AutoSizer>
          </AutoSizerWrapper>
        </TableWrapper>
      )}
    </Root>
  )
}

export default Table
