import React from 'react'
import { CellMeasurer } from 'react-virtualized'

export default (
  cellRenderer = params => params.cellData,
  deferredMeasurementCache,
) => ({ ...params }) => (
  <CellMeasurer
    cache={deferredMeasurementCache}
    columnIndex={params.columnIndex}
    key={params.dataKey}
    parent={params.parent}
    rowIndex={params.rowIndex}
  >
    <div
      className="tableColumn"
      style={{
        whiteSpace: 'normal',
        width: '100%',
      }}
    >
      {cellRenderer(params)}
    </div>
  </CellMeasurer>
)
