/* eslint-disable react/prop-types */
/* eslint-disable no-param-reassign */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react'
import PropTypes from 'prop-types'
import { th, grid } from '@pubsweet/ui-toolkit'
import {
  Table as Tablet,
  AutoSizer,
  Column,
  CellMeasurerCache,
} from 'react-virtualized'
import 'react-virtualized/styles.css'

import styled from 'styled-components'

import { calculateColumnProps } from './utils'
import CellMeasurer from './CellMeasurer'

// #region styled-components
const Root = styled.div`
  display: flex;
  flex: auto;
  flex-direction: column;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};

  .current-page {
    font-size: 1.5rem;
    vertical-align: middle;
  }
`

const NoData = styled.div`
  color: #939393;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  margin-top: ${th('gridUnit')};
  text-align: center;
  text-transform: capitalize;
`

const TableWrapper = styled.div`
  height: ${props => props.minHeight}px;

  .ReactVirtualized__Table {
    background-color: ${th('colorBackground')};
    color: ${th('colorText')};
    display: 'flex';
    margin-bottom: 20px;
  }

  .ReactVirtualized__Table__headerRow {
    border-bottom: 1px solid ${th('colorPrimaryDark')};
  }

  .ReactVirtualized__Table__headerColumn {
    font-size: ${th('fontSizeBase')};
    margin-right: 0;
  }

  .ReactVirtualized__Table__rowColumn {
    font-size: ${th('fontSizeBase')};
  }

  .ReactVirtualized__Table__row {
    border-bottom: 1px solid #eee;
  }

  .ReactVirtualized__Table__row :hover {
    background: #f4f4f4b5;
    cursor: pointer;
  }
`

const TableContainer = styled.div`
  margin-top: ${grid(1)};

  .ReactVirtualized__Table__headerColumn,
  .ReactVirtualized__Table__rowColumn {
    align-items: center;
    display: flex;
    flex-direction: row;
    font-size: ${th('fontSizeBase')};
    height: 100%;
    margin: 0;
    padding: 0 0.5em;
    width: 100%;
  }

  .ReactVirtualized__Table__headerRow {
    text-transform: none;
  }

  .ReactVirtualized__Table__row,
  .ReactVirtualized__Table__Grid {
    height: 100%;
    outline: none;
  }

  .ReactVirtualized__Grid__innerScrollContainer {
    /* height: 100%; */

    .ReactVirtualized__Table__row {
      &:nth-child(1) {
        font-weight: ${props => (props.highlightFirstRow ? 'bold' : '')};
      }
    }
  }

  .noCells {
    align-items: center;
    bottom: 0;
    color: #bdbdbd;
    display: flex;
    font-size: 1em;
    justify-content: center;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
  }

  .ReactVirtualized__Table__headerColumn {
    min-height: 27px;
  }

  .ReactVirtualized__Table__row :hover {
    background: #f4f4f4b5;
    cursor: pointer;
  }
`

// #endregion
let columnField = ''

const Table = ({
  columns,
  selectedRows,
  setSelectedRows,
  noDataMessage,
  onRowClick,
  onColumnClick,
  dataTable,
  rowHeight = 80,
  headerHeight = 80,
  rowRenderer,
  rowStyle,
  highlightFirstRow,
  disableHeader,
}) => {
  const deferredMeasurementCache = new CellMeasurerCache({
    fixedWidth: true,
    minHeight: rowHeight,
  })

  const loadedData = dataTable.map(row => {
    row = { ...row, checked: (selectedRows || []).includes(row.id) }
    return row
  })

  return (
    <Root>
      <TableWrapper
        minHeight={
          parseInt(rowHeight, 10) + loadedData.length * parseInt(rowHeight, 10)
        }
      >
        <AutoSizer>
          {({ width, height }) => (
            <TableContainer highlightFirstRow={highlightFirstRow}>
              <Tablet
                disableHeader={disableHeader}
                // height={height}
                headerHeight={headerHeight}
                height={
                  parseInt(rowHeight, 10) +
                  loadedData.length * parseInt(rowHeight, 10)
                }
                noRowsRenderer={() => (
                  <NoData>
                    {noDataMessage || ' No data available in table'}{' '}
                  </NoData>
                )}
                onColumnClick={props => {
                  columnField = props.dataKey

                  if (onColumnClick) {
                    onColumnClick({
                      data: loadedData,
                      ...props,
                    })
                  }
                }}
                onRowClick={props => {
                  if (onRowClick) {
                    onRowClick({
                      columnField,
                      data: loadedData,
                      ...props,
                    })
                  }
                }}
                // onRowsRendered={props => {
                //   handleRowsScroll(props)
                // }}
                rowCount={loadedData.length}
                rowGetter={({ index }) => loadedData[index]}
                rowHeight={
                  deferredMeasurementCache
                    ? deferredMeasurementCache.rowHeight
                    : rowHeight
                }
                rowRenderer={rowRenderer}
                rowStyle={rowStyle}
                setSelectedRows={setSelectedRows}
                width={width - 2}
              >
                {columns.map((column, index) => {
                  const columnProps = calculateColumnProps(loadedData, column)

                  if (columnProps.dynamicHeight) {
                    columnProps.cellRenderer = CellMeasurer(
                      columnProps.cellRenderer,
                      deferredMeasurementCache,
                    )
                  }

                  return (
                    <Column
                      key={`column_${index + 1}`}
                      {...columnProps}
                      width={Number(columnProps.width)}
                    />
                  )
                })}
              </Tablet>
            </TableContainer>
          )}
        </AutoSizer>
      </TableWrapper>

      {loadedData.length === 0 ? (
        <center>
          <br />

          {noDataMessage}
        </center>
      ) : null}
    </Root>
  )
}

Table.propTypes = {
  selectedRows: PropTypes.arrayOf(Object),
  setSelectedRows: PropTypes.func,
  noDataMessage: PropTypes.string,
  onRowClick: PropTypes.func,
  onColumnClick: PropTypes.func,
  dataTable: PropTypes.arrayOf(Object),
  rowHeight: PropTypes.number,
  headerHeight: PropTypes.number,
  rowStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  highlightFirstRow: PropTypes.bool,
}

Table.defaultProps = {
  selectedRows: [],
  setSelectedRows: null,
  noDataMessage: '',
  onRowClick: null,
  onColumnClick: null,
  dataTable: [],
  rowHeight: 80,
  headerHeight: 80,
  rowStyle: {},
  highlightFirstRow: false,
}

export default Table
