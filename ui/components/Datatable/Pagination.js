/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */

import React, { Component } from 'react'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'
import { th } from '@pubsweet/ui-toolkit'
import { Button, Icon } from '../common'

const Ul = styled.ul`
  background: transparent;
  display: flex;
  list-style: none;
  margin: 0;
  padding-left: 0;

  button.page-link {
    align-items: center;
    border: 2px solid
      ${props => (props.versionStyle ? th('colorBorder') : th('colorPrimary'))};
    box-shadow: none;
    color: ${props =>
      props.versionStyle ? th('colorText') : th('colorPrimary')};
    display: flex;
    height: 35px;
    justify-content: center;
    text-align: center;
    text-decoration: none;
  }

  button.page-link:hover {
    background-color: #e7e7e7;
  }

  button.elipsis {
    border: none;
    color: ${props =>
      props.versionStyle ? th('colorInterface') : th('colorPrimary')};
    font-size: 16pt;
    /* margin-right: -1px; */
  }

  button.elipsis:hover {
    background-color: #ffffff;
    cursor: default;
  }

  button.navigation {
    padding: 0 calc(${th('gridUnit')} / 2);
    text-transform: none;
    width: auto;
  }

  > li.page-item {
    margin: 0 ${props => (props.versionStyle ? '-1px' : '4px')};
  }

  > li.page-item:first-child {
    margin-left: 8px;
  }

  > li.page-item.active button.page-link {
    background-color: ${props =>
      props.versionStyle ? th('colorBorder') : th('colorPrimary')};
    color: ${props => (props.versionStyle ? th('colorText') : '#fff')};
    font-weight: bold;
    text-decoration: none;
  }
`

const LEFT_PAGE = 'LEFT'
const RIGHT_PAGE = 'RIGHT'

const range = (from, to, step = 1) => {
  let i = from
  const rng = []

  while (i <= to) {
    rng.push(i)
    i += step
  }

  return rng
}

class Pagination extends Component {
  constructor(props) {
    super(props)

    const {
      totalRecords = null,
      pageLimit = 30,
      pageNeighbours = 0,
      currentPage = 1,
      navigationIcons = false,
      versionStyle = false,
    } = props

    this.currentPage = currentPage
    this.pageLimit = typeof pageLimit === 'number' ? pageLimit : 30
    this.totalRecords = typeof totalRecords === 'number' ? totalRecords : 0
    this.navigationIcons = navigationIcons
    this.versionStyle = versionStyle

    this.pageNeighbours =
      typeof pageNeighbours === 'number'
        ? Math.max(0, Math.min(pageNeighbours, 2))
        : 0

    this.totalPages = Math.ceil(this.totalRecords / this.pageLimit)

    this.state = {
      currentPage,
      next: this.totalPages < 1 || this.currentPage < this.totalPages,
      previous: this.currentPage > 1,
      totalPages: this.totalPages,
      totalRecords: this.totalRecords,
      navigationIcons: this.navigationIcons,
    }
  }

  componentDidMount() {
    const currentPage =
      this.props.currentPage || Math.max(0, Math.min(1, this.totalPages))

    this.setState({ currentPage })
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.totalRecords !== this.props.totalRecords) {
      // Perform some operation
      this.setState({
        totalPages: Math.ceil(nextProps.totalRecords / this.pageLimit),
        totalRecords: nextProps.totalRecords,
      })
    }

    if (nextProps.currentPage !== this.props.currentPage) {
      // Perform some operation
      this.setState({
        currentPage: nextProps.currentPage,
      })

      if (nextProps.currentPage === 1) {
        this.setState({ previous: false })
        this.setState({ next: true })
      } else if (nextProps.currentPage === this.state.totalPages) {
        this.setState({ next: false })
        this.setState({ previous: true })
      } else {
        this.setState({ next: true, previous: true })
      }
    }
  }

  gotoPage = page => {
    const { onPageChanged = f => f } = this.props

    const currentPage = Math.max(0, Math.min(page, this.state.totalPages))

    const paginationData = {
      currentPage,
      pageLimit: this.pageLimit,
      totalPages: this.state.totalPages,
      totalRecords: this.state.totalRecords,
    }

    this.setState({ currentPage }, () => onPageChanged(paginationData))
  }

  handleClick = (page, evt) => {
    evt.preventDefault()
    if (this.state.currentPage !== page) this.gotoPage(page)
  }

  handleMoveLeft = evt => {
    evt.preventDefault()

    if (this.state.previous) {
      this.gotoPage(this.state.currentPage - 1)
    }
  }

  handleMoveRight = evt => {
    evt.preventDefault()

    if (this.state.next) {
      this.gotoPage(this.state.currentPage + 1)
    }
  }

  fetchPageNumbers = () => {
    const {
      pageNeighbours,
      state: { currentPage, totalPages },
    } = this

    const totalNumbers = this.pageNeighbours * 2 + 3
    // total page numbers to be shown plus two additional blocks for the left
    // and right page indicators
    const totalBlocks = totalNumbers + 2

    if (totalPages > totalBlocks) {
      let pages = []

      const leftBound = currentPage - pageNeighbours
      const rightBound = currentPage + pageNeighbours
      const beforeLastPage = totalPages - 1

      const startPage = leftBound > 2 ? leftBound : 2
      const endPage = rightBound < beforeLastPage ? rightBound : beforeLastPage

      pages = range(startPage, endPage)

      const pagesCount = pages.length
      const singleSpillOffset = totalNumbers - pagesCount - 1

      const leftSpill = startPage > 2
      const rightSpill = endPage < beforeLastPage

      const leftSpillPage = LEFT_PAGE
      const rightSpillPage = RIGHT_PAGE

      if (leftSpill && !rightSpill) {
        const extraPages = range(startPage - singleSpillOffset, startPage - 1)
        pages = [leftSpillPage, ...extraPages, ...pages]
      } else if (!leftSpill && rightSpill) {
        const extraPages = range(endPage + 1, endPage + singleSpillOffset)
        pages = [...pages, ...extraPages, rightSpillPage]
      } else if (leftSpill && rightSpill) {
        pages = [leftSpillPage, ...pages, rightSpillPage]
      }

      return [1, ...pages, totalPages]
    }

    return range(1, totalPages)
  }

  render() {
    if (!this.state.totalRecords) return null

    if (this.state.totalPages === 1 && !this.props.showFirstPage) return null

    const { currentPage } = this.state
    const pages = this.fetchPageNumbers()

    return (
      <nav aria-label="Pagination">
        <Ul className="pagination" versionStyle={this.versionStyle}>
          <li className="page-item" key="pagination-prev">
            {this.state.totalPages > 1 && (
              <Button
                className="page-link navigation"
                disabled={!this.state.previous}
                onClick={this.handleMoveLeft}
                outlined
                status="primary"
              >
                {this.state.navigationIcons ? (
                  <Icon color="colorPrimaryDark">chevrons-left</Icon>
                ) : (
                  <span className="">Previous</span>
                )}
              </Button>
            )}
          </li>
          {pages.map((page, index) => {
            if (page === LEFT_PAGE)
              return (
                <li className="page-item" key={`pagination-${uuid()}`}>
                  <Button className="page-link elipsis" outlined>
                    <span aria-hidden="true">&#8230;</span>
                  </Button>
                </li>
              )

            if (page === RIGHT_PAGE)
              return (
                <li className="page-item" key={`pagination-${uuid()}`}>
                  <Button className="page-link elipsis" outlined>
                    <span aria-hidden="true">&#8230;</span>
                  </Button>
                </li>
              )

            return (
              <li
                className={`page-item${currentPage === page ? ' active' : ''}`}
                key={`pagination-${uuid()}`}
              >
                <Button
                  className="page-link"
                  onClick={e => this.handleClick(page, e)}
                  outlined
                  status="primary"
                >
                  {this.props.renderPage ? this.props.renderPage(page) : page}
                </Button>
              </li>
            )
          })}
          <li className="page-item" key="pagination-next">
            {this.state.totalPages > 1 && (
              <Button
                className="page-link navigation"
                disabled={!this.state.next}
                onClick={this.handleMoveRight}
                outlined
                status="primary"
              >
                {this.state.navigationIcons ? (
                  <Icon color="colorPrimaryDark">chevrons-right</Icon>
                ) : (
                  <span className="">Next</span>
                )}
              </Button>
            )}
          </li>
        </Ul>
      </nav>
    )
  }
}

export default Pagination
