/* eslint-disable react/no-danger */

import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { xor } from 'lodash'
import { grid, th } from '@pubsweet/ui-toolkit'
import { notification } from 'antd'

import DisplayInlineEditor from './wax/DisplayInlneEditor'

import { AntModal as Modal } from './Modal'
import {
  Button,
  ButtonGroup,
  Search as UISearch,
  Spinner,
  Tag as UITag,
  Toggle,
} from './common'

import { MoveComponents as MoveIcon } from './common/SvgIcon'
import { parseMoveSearchData } from '../Pages/Bookmanager/commonPageHelpers'

// #region styled
const BookComponents = styled.div`
  align-items: center;
  display: inline-block;
`

const Item = styled.div`
  box-sizing: border-box;
  cursor: pointer;
  display: flex;
  width: 100%;
`

const Label = styled.span`
  align-items: center;
  display: flex;
  justify-content: center;
  margin-right: ${grid(1)};
  text-transform: ${({ searchLabel }) => (searchLabel ? 'none' : 'capitalize')};
`

const SearchWrapper = styled.div`
  align-items: center;
  display: flex;
  gap: ${grid(0.5)};
  width: 100%;
`

const Search = styled(UISearch)`
  flex-grow: 1;
`

const ResultsLabel = styled.div`
  border-bottom: 1px solid ${th('colorText')};
  display: flex;
  justify-content: space-between;
  margin-bottom: ${grid(1)};
`

const Result = styled.div`
  border: 1px solid transparent;
  cursor: pointer;
  display: flex;
  gap: ${grid(1)};
  padding: ${grid(1)};
  transition: all 0.1s ease-in;

  &:hover {
    border: 1px solid ${th('colorPrimary')};
  }

  /* stylelint-disable order/properties-alphabetical-order */
  ${({ selected }) =>
    selected &&
    css`
      border: 1px solid ${th('colorPrimary')};
      box-shadow: -4px 0px ${th('colorPrimary')};
    `}
`

const ResultsWrapper = styled.div`
  > div:nth-of-type(odd):not(:first-child) {
    background: ${th('colorBackgroundHue')};
  }
`

const Tag = styled(UITag)`
  background-color: ${th('colorPrimaryLighter')};
  color: ${th('colorPrimary')};
`

const NoResults = styled.div`
  padding: ${grid(2)} 0;
  text-align: center;
`

const ModalBodyWrapper = styled.div`
  max-height: 60vh;
  overflow-y: auto;
`
// #endregion styled

const MoveComponents = props => {
  const {
    autoSorted,
    bodyId,
    divisionId,
    divisionsMap,
    groupInParts,
    isDisabled,
    onReorder,
    onSearch,
    selectedBookComponents,
  } = props

  const [modalIsOpen, setIsOpen] = useState(false)
  const [selectedComponentId, setSelectedComponentId] = useState(null)
  const [selectedParentId, setSelectedParentId] = useState(null)
  const [searchTerm, setSearchTerm] = useState('')
  const [showSearchResults, setShowSearchResults] = useState(false)
  const [searchResults, setSearchResults] = useState([])
  const [loadingSearchResults, setLoadingSearchResults] = useState(false)
  const [isMoving, setIsMoving] = useState(false)

  const [showPartOnlyForDuplicates, setShowPartOnlyForDuplicates] = useState(
    true,
  )

  const [notificationApi, notificationContext] = notification.useNotification()

  useEffect(() => {
    setSelectedComponentId(null)
  }, [searchResults, modalIsOpen])

  const handleSelectPart = (partId, parentId) => {
    setSelectedComponentId(partId)
    setSelectedParentId(parentId)
  }

  const handleSearch = () => {
    setLoadingSearchResults(true)

    onSearch({
      searchTerm,
      divisionId,
      autoSorted,
      selectedRows: selectedBookComponents.map(bc => bc.id),
    })
      .then(searchResponse => {
        const divisions = Object.values(divisionsMap).map(d => ({
          id: d.id,
          fullBookComponents: d.fullBookComponents,
        }))

        const parsedData = parseMoveSearchData(searchResponse.data, divisions)
        setSearchResults(parsedData)
      })
      .catch(e => {
        notificationApi.error({
          message: 'Error',
          description: 'Could not search for item',
        })
      })
      .finally(() => {
        setLoadingSearchResults(false)
      })

    setShowSearchResults(true)
  }

  const handleClear = () => {
    setSearchTerm('')
    setShowSearchResults(false)
    setSelectedParentId(null)
  }

  const handleCloseModal = () => {
    if (!isMoving) {
      setSelectedComponentId(null)
      setIsOpen(false)
      setShowSearchResults(false)
      setSearchTerm('')
    }
  }

  const handleReorder = async action => {
    setIsMoving(true)
    const componentIds = handleRemoveChildren()
    const targetId = selectedComponentId
    const partId = selectedParentId

    const componentIdsParents = selectedBookComponents.map(bc => bc.parentId)
    const targetIdParent = searchResults.find(i => i.id === targetId).parentId
    const parentIdsToRefresh = [...componentIdsParents, targetIdParent]

    onReorder(componentIds, targetId, action, partId, parentIdsToRefresh)
      .then(() => {
        handleCloseModal()

        notificationApi.success({
          message: 'Success',
          description: 'Items moved successfully',
        })
      })
      .catch(() => {
        notificationApi.error({
          message: 'Error',
          description: `Failed to move item${
            selectedBookComponents.length > 1 && 's'
          }`,
        })
      })
      .finally(() => {
        setIsMoving(false)
      })
  }

  const getChildrenIds = parentComponent => {
    if (!parentComponent.children.length) {
      return []
    }

    const childrenIds = parentComponent.children.map(p => p.id)

    return xor([
      ...childrenIds,
      ...parentComponent.children.map(p => getChildrenIds(p)).flat(1),
    ])
  }

  const handleRemoveChildren = () => {
    if (selectedBookComponents.length === 1) {
      return [selectedBookComponents[0].id]
    }

    const childrenIds = selectedBookComponents
      .map(bc => getChildrenIds(bc))
      .flat(1)

    const fullIds = selectedBookComponents.map(bc => bc.id)
    const filteredIds = fullIds.filter(f => !childrenIds.includes(f))
    return filteredIds
  }

  const onlyPartsSelected = selectedBookComponents.every(bc => bc.isPart)

  const allSelectedChildrenHaveParents = selectedBookComponents
    .filter(f => !f.isPart)
    .every(c => c.parentId)

  const isAllMoveDisabled = isMoving || !selectedComponentId

  const isMoveAboveOrBelowDisabled =
    isAllMoveDisabled ||
    selectedComponentId === bodyId ||
    (autoSorted &&
      divisionId === bodyId &&
      !(onlyPartsSelected || allSelectedChildrenHaveParents))

  const isMoveInsideDisabled =
    isAllMoveDisabled ||
    searchResults.length === 0 ||
    !searchResults
      .filter(p => p.componentType === 'part')
      .find(pL => pL.id === selectedComponentId)

  const modalFooter = (
    <ButtonGroup>
      {groupInParts && (
        <Button
          data-test-id="move-into-target-metadata"
          disabled={isMoveInsideDisabled}
          onClick={() => handleReorder('inside')}
          status="primary"
          title="Select one or more parts to save"
        >
          {isMoving ? 'Moving...' : 'Move inside'}
        </Button>
      )}

      <Button
        data-test-id="move-below-target-metadata"
        disabled={isMoveAboveOrBelowDisabled}
        onClick={() => handleReorder('below')}
        status="primary"
        title="Select one or more parts to save"
      >
        {isMoving ? 'Moving...' : 'Move below'}
      </Button>

      <Button
        data-test-id="move-above-target-metadata"
        disabled={isMoveAboveOrBelowDisabled}
        onClick={() => handleReorder('above')}
        status="primary"
        title="Select one or more parts to save"
      >
        {isMoving ? 'Moving...' : 'Move above'}
      </Button>

      <Button
        data-test-id="cancel-book-metadata"
        onClick={handleCloseModal}
        outlined
        status="primary"
      >
        Close
      </Button>
    </ButtonGroup>
  )

  return (
    <>
      {notificationContext}
      <Button
        data-test-id="move-chapters-to"
        disabled={isDisabled}
        icon={<MoveIcon />}
        onClick={() => {
          setIsOpen(true)
        }}
        outlined
        status="primary"
      >
        {'Move '}
      </Button>

      <Modal
        footer={modalFooter}
        isOpen={modalIsOpen}
        onCancel={handleCloseModal}
        title="Move"
      >
        <ModalBodyWrapper>
          {selectedBookComponents.length &&
            selectedBookComponents.map(item => (
              <li key={item.title}>
                <BookComponents>
                  <DisplayInlineEditor content={item.title} />
                </BookComponents>
              </li>
            ))}

          <br />

          <Item>
            <Label searchLabel>Search for the position:</Label>
          </Item>

          <Item>
            <SearchWrapper>
              <Search
                customSearchValue={searchTerm}
                onChange={e => setSearchTerm(e.target.value)}
                triggerSearch={handleSearch}
              />

              <ButtonGroup>
                <Button onClick={handleSearch} status="primary">
                  Search
                </Button>

                <Button onClick={handleClear} outlined status="primary">
                  Clear
                </Button>
              </ButtonGroup>
            </SearchWrapper>
          </Item>

          <br />

          {showSearchResults && (
            <ResultsWrapper>
              {loadingSearchResults && <Spinner label="Searching..." />}

              {!loadingSearchResults && (
                <>
                  <ResultsLabel>
                    <span>Results:</span>
                    <Toggle
                      checked={showPartOnlyForDuplicates}
                      label="Show tags only for duplicates"
                      onClick={() =>
                        setShowPartOnlyForDuplicates(!showPartOnlyForDuplicates)
                      }
                    />
                  </ResultsLabel>

                  {searchResults.length === 0 && (
                    <NoResults>
                      There were no matches for your search terms
                    </NoResults>
                  )}

                  {searchResults.length > 0 &&
                    searchResults.map(item => {
                      return (
                        <Result
                          key={item.key}
                          onClick={() =>
                            handleSelectPart(item.id, item.parentId)
                          }
                          selected={
                            selectedComponentId === item.id &&
                            selectedParentId === item.parentId
                          }
                        >
                          {item.componentType === 'part' && (
                            <Tag label="Part" size="small" />
                          )}
                          <Label>
                            <span
                              dangerouslySetInnerHTML={{
                                __html: item.title,
                              }}
                            />
                          </Label>
                          {(!showPartOnlyForDuplicates ||
                            (showPartOnlyForDuplicates &&
                              item.isDuplicate)) && (
                            <Tag
                              label={
                                item.parentTitle ? (
                                  <>
                                    Within Part: <em>{item.parentTitle}</em>
                                  </>
                                ) : (
                                  <>Within Body</>
                                )
                              }
                              size="small"
                            />
                          )}
                        </Result>
                      )
                    })}
                </>
              )}
            </ResultsWrapper>
          )}
        </ModalBodyWrapper>
      </Modal>
    </>
  )
}

MoveComponents.propTypes = {
  /** Array of Objects defining selected components,in repeat case has only one item */
  selectedBookComponents: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
      parentid: PropTypes.string,
    }),
  ), // tree list of parts inside body

  /** Boolean value defining if the save button will be disabled */
  isDisabled: PropTypes.bool,
  onReorder: PropTypes.func,
  onSearch: PropTypes.func,
  bodyId: PropTypes.string,
  groupInParts: PropTypes.bool,
  divisionId: PropTypes.string,
  divisionsMap: PropTypes.shape({
    backMatter: PropTypes.shape({
      id: PropTypes.string,
      fullBookComponents: PropTypes.arrayOf(PropTypes.string),
    }),
    body: PropTypes.shape({
      id: PropTypes.string,
      fullBookComponents: PropTypes.arrayOf(PropTypes.string),
    }),
    frontMatter: PropTypes.shape({
      id: PropTypes.string,
      fullBookComponents: PropTypes.arrayOf(PropTypes.string),
    }),
  }),
  autoSorted: PropTypes.bool,
}

MoveComponents.defaultProps = {
  selectedBookComponents: [],
  isDisabled: false,
  bodyId: null,
  groupInParts: false,
  divisionId: null,
  divisionsMap: {},
  autoSorted: false,
  onReorder: null,
  onSearch: null,
}

export default MoveComponents
