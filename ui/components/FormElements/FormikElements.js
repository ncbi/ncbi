/* eslint-disable react/prop-types */
import React, { useMemo } from 'react'
import styled, { css } from 'styled-components'
import { ErrorMessage } from 'formik'

import { TextField as UITextField, TextArea } from '@pubsweet/ui'
import { fadeIn, grid, th } from '@pubsweet/ui-toolkit'

import {
  Checkbox,
  Label,
  Radio,
  Select,
  Toggle,
  NumberInput,
  DateInput,
  CreatableSelect,
} from '../common'

import Editor from '../wax/EditorCustom'

// #region styled

export const Error = styled.div.attrs(props => ({ role: 'alert' }))`
  animation: ${fadeIn} 0.2s;
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  margin-top: 4px;
  text-align: left;
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: ${props => (props.inline ? 'row' : 'column')};
  justify-content: start;
  ${props =>
    props.inline &&
    css`
      justify-content: space-between;
    `};
  width: 100%;
`

const FieldWithError = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0;
  width: 100%;
`

const EditorWrapper = styled.div`
  position: relative;

  > div:nth-child(2) {
    ${props =>
      props.disabled &&
      css`
        cursor: not-allowed;
      `}
  }

  /* wax toolbar */
  > div:nth-child(2) > div:first-child {
    margin: 0;
    z-index: 0;

    button {
      ${props =>
        props.disabled &&
        css`
          /* background-color: ${th('colorBackgroundHue')}; */
          cursor: not-allowed;
          pointer-events: none;
        `}
    }
  }

  div[contenteditable] {
    line-height: ${th('lineHeightBase')};
    margin: 0;

    p {
      margin: ${grid(1)} 0;
    }
  }

  div[contenteditable='false'] {
    background-color: ${th('colorBackgroundHue')};
  }
`

const TextField = styled(UITextField)`
  margin-bottom: 0;

  input {
    ${props =>
      props.disabled &&
      css`
        background-color: ${th('colorBackgroundHue')};
        cursor: not-allowed;
      `}
  }
`
// #endregion styled

const showError = (name, form) =>
  form.errors[name] && (form.submitCount > 0 || form.touched[name])

export const TextFieldComponent = props => {
  const {
    className,
    description,
    disabled,
    field,
    form,
    inline,
    label,
    required,
    type,
    ...rest
  } = props

  const { handleBlur, handleChange } = form
  const { name, value } = field
  const showThisError = showError(name, form)
  const validationStatus = showThisError ? 'error' : 'default'

  return (
    <Wrapper className={className} inline={inline}>
      {label && (
        <Label
          description={description}
          htmlFor={name}
          required={required}
          value={label}
        />
      )}

      <TextField
        disabled={disabled}
        name={name}
        onBlur={handleBlur}
        onChange={handleChange}
        type={type}
        validationStatus={validationStatus}
        value={value === null ? '' : value}
        {...rest}
      />

      <ErrorMessage component={Error} name={name} />
    </Wrapper>
  )
}

export const CreatableSelectComponent = ({
  field: { name, value },
  form,
  multiple,
  onChange,
  options,
  'data-test-id': testId,
  hideSelectedOptions,
  ...rest
}) => {
  const modifiedValue = value.map(val =>
    typeof val === 'string' ? { label: val, value: val } : val,
  )

  const { setFieldTouched, setFieldValue } = form

  const handleBlur = () => setFieldTouched(name)

  const handleChange = (selected, actionMeta) => {
    if (selected) {
      setFieldValue(
        name,
        multiple ? selected.map(select => select.value) : selected.value,
      )
    } else {
      setFieldValue(name, null)
    }

    if (onChange) {
      onChange(selected, actionMeta)
    }
  }

  const hasError = showError(name, form)

  const selectValue = multiple
    ? options?.filter(
        option =>
          (modifiedValue || []).findIndex(
            el => el === option.value || el?.value === option.value,
          ) > -1,
      )
    : options?.filter(option => option.value === value)

  return (
    <Wrapper>
      <CreatableSelect
        hasError={hasError}
        multiple={multiple}
        name={name}
        onBlur={handleBlur}
        onChange={handleChange}
        options={options.filter(
          x => hideSelectedOptions && !(value || []).includes(x.value),
        )}
        {...rest}
        data-test-id={testId}
        value={selectValue.filter(x => !!x)}
      />

      <ErrorMessage component={Error} name={name} />
    </Wrapper>
  )
}

export const SelectComponent = ({
  field: { name, value },
  form,
  multiple,
  onChange,
  options,
  ...rest
}) => {
  const { setFieldTouched, setFieldValue } = form

  const handleBlur = () => setFieldTouched(name)

  const handleChange = selected => {
    if (selected) {
      setFieldValue(
        name,
        multiple ? selected.map(select => select.value) : selected.value,
      )
    } else {
      setFieldValue(name, null)
    }

    if (onChange) {
      onChange(selected)
    }
  }

  const hasError = showError(name, form)

  const selectValue = multiple
    ? options?.filter(option => (value || []).includes(option.value))
    : options?.filter(option => option.value === value)

  return (
    <Wrapper>
      <Select
        hasError={hasError}
        multiple={multiple}
        name={name}
        onBlur={handleBlur}
        onChange={handleChange}
        options={options}
        value={selectValue}
        {...rest}
      />

      <ErrorMessage component={Error} name={name} />
    </Wrapper>
  )
}

export const ToggleComponent = ({
  className,
  field,
  form,
  onClick,
  label,
  ...rest
}) => {
  const { name, value } = field
  const { setFieldTouched, setFieldValue } = form

  const handleClick = v => {
    setFieldTouched(name)
    setFieldValue(name, v)
    if (onClick) onClick(v)
  }

  return (
    <Wrapper>
      <Toggle
        checked={value}
        className={className}
        label={label}
        onClick={handleClick}
        {...field}
        {...rest}
      />
    </Wrapper>
  )
}

export const CheckBoxComponent = ({
  field,
  checked,
  disabled,
  onClick,
  label,
}) => {
  return (
    <Checkbox
      checked={checked}
      disabled={disabled}
      label={label}
      name={field.name}
      onClick={onClick}
      value={checked}
    />
  )
}

export const TextAreaFieldComponent = ({ field, form, ...props }) => {
  const { className, inline, label, value, required } = props
  const { name } = { ...field }

  const showThisError = showError(name, form)

  const validationStatus = showThisError ? 'error' : 'default'

  return (
    <Wrapper className={className} inline={inline}>
      {label && <Label htmlFor={name} required={required} value={label} />}

      <FieldWithError>
        <TextArea
          validationStatus={validationStatus}
          value={value}
          {...props}
          {...field}
        />

        <ErrorMessage component={Error} name={name} />
      </FieldWithError>
    </Wrapper>
  )
}

export const DateInputComponent = props => {
  const { field, form, 'data-test-id': testId, ...rest } = props

  const { name, value } = field
  const { setFieldTouched, setFieldValue } = form

  const showThisError = showError(name, form)
  const handleBlur = () => setFieldTouched(name)
  const handleChange = v => setFieldValue(name, v)

  const validationStatus = showThisError ? 'error' : 'default'
  return (
    <div data-test-id={testId}>
      <DateInput
        name={name}
        onBlur={handleBlur}
        onChange={handleChange}
        validationStatus={validationStatus}
        value={value}
        {...rest}
      />

      <ErrorMessage component={Error} name={name} />
    </div>
  )
}

export const NumberInputComponent = props => {
  const { field, form, ...rest } = props

  const { name, value } = field
  const { handleBlur, setFieldValue } = form

  const handleChange = v => {
    setFieldValue(name, v)
  }

  return (
    <div>
      <NumberInput
        name={name}
        onBlur={handleBlur}
        onChange={handleChange}
        value={value}
        {...rest}
      />
      <ErrorMessage component={Error} name={name} />
    </div>
  )
}

export const RadioComponent = props => {
  const { field, form, onChange, ...rest } = props
  const { name, value } = field
  const { setFieldTouched, setFieldValue } = form

  const handleBlur = () => {
    setFieldTouched(name)
  }

  const handleChange = v => {
    setFieldValue(name, v)
    onChange && onChange(v)
  }

  return (
    <div>
      <Radio
        name={name}
        onBlur={handleBlur}
        onChange={handleChange}
        value={value}
        {...rest}
      />

      <ErrorMessage component={Error} name={name} />
    </div>
  )
}

export const EditorComponent = props => {
  const { disabled, field, form, label = '', required, key, ...rest } = props
  const { name, value } = field
  const { setFieldTouched, setFieldValue } = form

  const handleBlur = () => setFieldTouched(name)
  const handleChange = data => setFieldValue(name, data)

  const Wax = useMemo(
    e => {
      return (
        <Editor
          inline
          key={key || disabled}
          name={name}
          onBlur={handleBlur}
          onChange={handleChange}
          readonly={disabled}
          value={value}
          {...rest}
        />
      )
    },
    [value, disabled],
  )

  return (
    <EditorWrapper
      data-test-id={label.toLowerCase().split(' ').join('_')}
      disabled={disabled}
    >
      <Label name={name} required={required} value={label} />
      {Wax}
      <ErrorMessage component={Error} name={name} />
    </EditorWrapper>
  )
}
