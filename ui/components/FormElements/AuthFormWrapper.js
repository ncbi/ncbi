/* eslint-disable react/prop-types */
import React from 'react'
import { CenteredColumn } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import styled from 'styled-components'

import FormLogo from '../../../public/form-logo.png'
import Device from '../../common/responsiveDevices'

const Container = styled.div`
  background-color: #f1f1f1;
  display: flex;
  flex-direction: column;
  text-align: center;
`

const FormWrapper = styled.div`
  background-color: #fbfbfb;
  border-radius: 6px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.25);
  text-align: center;

  @media ${Device.mobileS} {
    margin: 12px auto;
    max-width: 80%;
    padding: calc(${th('gridUnit')} * 3);
    text-align: center;
  }

  @media ${Device.tablet} {
    margin-top: calc(${th('gridUnit')} * 2);
    max-width: 600px;
    padding: calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 6);
  }
`

const Logo = styled.img`
  max-width: 112px;
  position: relative;
  text-align: center;
`

const StyledColumn = styled(CenteredColumn)`
  margin: calc(${th('gridUnit')} * 4) auto;
  max-width: 80%;
  width: 600px;

  @media ${Device.tablet} {
    margin: calc(${th('gridUnit')} * 8) auto;
    max-width: 100%;
  }
`

const Warning = styled.p`
  color: #ff2d1a;
  @media ${Device.tablet} {
    display: none;
  }
`

const AuthFormWrapper = ({ className, children }) => (
  <Container className={className}>
    <StyledColumn>
      <Logo alt="Logo" src={FormLogo} />
      <Warning>This the tool is not suitable for mobile use !</Warning>
      <FormWrapper>{children}</FormWrapper>
    </StyledColumn>
  </Container>
)

export default AuthFormWrapper
