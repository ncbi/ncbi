import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Formik, Form } from 'formik'
import { isFunction } from 'lodash'

import { grid } from '@pubsweet/ui-toolkit'

import Button from '../common/Button'

// #region styled
const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 800px;
  padding-bottom: 50px;
  padding-top: 20px;

  > form > div:not(:last-child) {
    margin-bottom: ${grid(2)};
  }
`

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: ${grid(2)};
`
// #endregion styled

const FormHelper = props => {
  const {
    className,
    children,
    isSaving,
    showSubmitButton,
    submitButtonLabel,
    ...rest
  } = props

  // return <Wrapper className={className}>some content</Wrapper>

  return (
    <Wrapper className={className}>
      <Formik {...rest}>
        {formikProps => {
          return (
            <Form noValidate>
              {isFunction(children) ? children(formikProps) : children}

              {showSubmitButton && (
                <ButtonWrapper>
                  <Button
                    disabled={isSaving}
                    loading={isSaving}
                    status="primary"
                    type="submit"
                  >
                    {submitButtonLabel}
                  </Button>
                </ButtonWrapper>
              )}
            </Form>
          )
        }}
      </Formik>
    </Wrapper>
  )
}

FormHelper.propTypes = {
  isSaving: PropTypes.bool,
  showSubmitButton: PropTypes.bool,
  submitButtonLabel: PropTypes.string,
}

FormHelper.defaultProps = {
  isSaving: false,
  showSubmitButton: false,
  submitButtonLabel: 'Submit',
}

export default FormHelper
