/* eslint-disable react/prop-types */
import React from 'react'
import styled from 'styled-components'

const Text = styled.div`
  color: ${props => props.theme.colorSuccess};
  margin-top: 20px;
`

const SuccessText = ({ children }) => <Text>{children}</Text>
export default SuccessText
