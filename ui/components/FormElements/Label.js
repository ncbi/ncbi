import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const Label = styled.label`
  font-family: ${th('fontInterface')};
  font-weight: 600;
  margin-bottom: ${th('gridUnit')};
  text-align: start;
`

export default Label
