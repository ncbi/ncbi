/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { cloneDeep } from 'lodash'

import { grid, th } from '@pubsweet/ui-toolkit'
import Icon from '../common/Icon'

import SelectUI from '../common/Select'

// #region styled

const Row = styled.div`
  align-items: center;
  border-bottom: 1px solid ${th('colorBorder')};
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  margin-bottom: ${th('gridUnit')};
  padding: 0 0 ${th('gridUnit')} ${th('gridUnit')};
  width: 100%;
`

const ListLabel = styled.div`
  border-bottom: 1px solid ${th('colorText')};
  font-weight: 600;
  margin-bottom: ${th('gridUnit')};
  padding-bottom: ${th('gridUnit')};
`

const SecondaryText = styled.div`
  color: ${th('colorBorder')};
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 500px;
`

const Select = styled(SelectUI)`
  margin-bottom: ${grid(1)};
`

// #endregion styled

const SelectList = ({
  label,
  secondaryLabel,
  description,
  placeholder,
  options,
  field,
  onChange,
  defaultSelection,
  disabled,
  ...props
}) => {
  const [selectedOpt, setSelectedOpt] = useState(defaultSelection || [])

  useEffect(() => {
    setSelectedOpt(defaultSelection || [])
  }, [defaultSelection])

  const handleChange = selected => {
    const exists = selectedOpt.filter(item => item.value === selected.value)

    if (!exists.length) {
      selectedOpt.push(selected)
      setSelectedOpt([...selectedOpt])
    }

    onChange(selectedOpt)
  }

  const selectOptions = options.filter(
    x => selectedOpt.findIndex(y => y.value === x.value) === -1,
  )

  return (
    <Wrapper>
      <Select
        description={description}
        disabled={disabled}
        label={label}
        onChange={handleChange}
        options={selectOptions}
        placeholder={placeholder}
        props={props}
        value={[]}
      />

      {secondaryLabel && <ListLabel>{secondaryLabel}</ListLabel>}

      {selectedOpt.length === 0 && secondaryLabel && (
        <SecondaryText>Nothing selected</SecondaryText>
      )}

      {selectedOpt.map((x, index) => (
        <Row key={x.value}>
          {x.label}
          {!disabled && (
            <Icon
              color="colorError"
              onClick={() => {
                const selectedOptCopy = cloneDeep(selectedOpt)
                selectedOptCopy.splice(index, 1)
                setSelectedOpt([...selectedOptCopy])
                onChange(selectedOptCopy)
              }}
            >
              x
            </Icon>
          )}
        </Row>
      ))}
    </Wrapper>
  )
}

export default SelectList
