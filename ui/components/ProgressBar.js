/* eslint-disable react/prop-types */
import React, { useEffect, useState, useRef } from 'react'
import styled from 'styled-components'

const MainSvg = styled.svg`
  display: inline-block;
  max-width: 100%;
  vertical-align: middle;
`

const CircleSvgBg = styled.circle`
  fill: none;
`

const CircleSvg = styled.circle`
  fill: none;
`

const ProgressBar = props => {
  const [offset, setOffset] = useState(0)
  const circleRef = useRef(null)

  const {
    size,
    progress,
    strokeWidth,
    circleOneStroke,
    circleTwoStroke,
  } = props

  const center = size / 2
  const radius = size / 2 - strokeWidth / 2
  const circumference = 2 * Math.PI * radius

  useEffect(() => {
    const progressOffset = ((100 - progress) / 100) * circumference
    setOffset(progressOffset)
  }, [setOffset, progress, circumference, offset])

  return (
    <MainSvg height={size} width={size}>
      <CircleSvgBg
        cx={center}
        cy={center}
        r={radius}
        stroke={circleOneStroke}
        strokeWidth={strokeWidth}
      />
      <CircleSvg
        cx={center}
        cy={center}
        r={radius}
        ref={circleRef}
        stroke={circleTwoStroke}
        strokeDasharray={circumference}
        strokeDashoffset={offset}
        strokeWidth={strokeWidth}
      />
    </MainSvg>
  )
}

export default ProgressBar
