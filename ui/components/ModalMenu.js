/* eslint-disable react/prop-types */
import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import Icon from './common/Icon'

const UiModalMenu = styled.a`
  align-items: center;
  background: #eeeeee;
  display: flex;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  height: 25px;
  max-width: 100%;
  padding: 2px 8px;
`

const Part = styled.div`
  align-items: center;
  text-align: ${props => props.align};
  width: 33%;
`

const Center = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
  height: 100%;
`

const Right = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
  height: 100%;
  justify-content: flex-end;
`

const ModalMenu = ({ prev, prevUser, current, next, nextUser }) => (
  <UiModalMenu>
    <Part align="left">
      {prev ? (
        <Center onClick={prev}>
          <Icon>chevron-left</Icon>
          <span>Previous: {prevUser}</span>
        </Center>
      ) : (
        ''
      )}
    </Part>
    <Part align="center">{current}</Part>
    <Part align="right">
      {next ? (
        <Right onClick={next}>
          <span>Next: {nextUser}</span>
          <Icon>chevron-right</Icon>
        </Right>
      ) : (
        ''
      )}
    </Part>
  </UiModalMenu>
)

export default ModalMenu
