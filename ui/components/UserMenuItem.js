/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th, grid } from '@pubsweet/ui-toolkit'
// import UserIconPath from '../../public/profile-placeholder.png'
import { Button as UIButton } from './common'

const StyledDropdown = styled.div`
  position: relative;
`

const DropdownTitle = styled(UIButton)`
  background-color: ${th('colorBackground')};
  border: 1px solid ${th('colorBackground')};
  color: ${th('colorPrimary')};

  &:hover,
  &:focus {
    background-color: ${th('colorBackground')};
    border: 1px solid ${th('colorBackground')};
    color: ${th('colorPrimary')};
  }
`

const DropdownMenu = styled.ul`
  background-color: ${th('colorBackground')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorPrimary')};
  border-radius: ${props => props.theme.borderRadius};
  color: ${th('colorPrimary')};
  display: ${props => (props.menuIsOpen ? 'block' : 'none')};
  left: 1px;
  list-style-type: none;
  margin: ${grid(0.25)} 0 0 0;
  padding: 0;
  position: absolute;
  width: calc(100% - 2px);
  z-index: 9;
`

const Item = styled.li`
  cursor: pointer;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  padding: ${th('gridUnit')};
  white-space: normal;
  word-break: break-word;

  &:hover {
    background-color: ${th('colorBackgroundHue')};
  }
`

const UserMenuItem = ({
  children,
  direction,
  disabled = false,
  iconPosition,
  itemsList,
  primary,
  size,
  variant,
  color,
}) => {
  const [menuIsOpen, setMenuIsOpen] = useState(false)

  return (
    <StyledDropdown size={size}>
      <DropdownTitle
        data-test-id="user-menu"
        disabled={disabled}
        icon="user"
        onBlur={() => setMenuIsOpen(false)}
        onClick={() => setMenuIsOpen(!menuIsOpen)}
        outlined
        primary={primary}
        variant={variant}
      >
        {children}
      </DropdownTitle>

      <DropdownMenu direction={direction} menuIsOpen={menuIsOpen}>
        {itemsList.map(item => (
          <Item
            key={item.id}
            onClick={() => {
              item.onClick()
              setMenuIsOpen(false)
            }}
            {...item.props}
            onMouseDown={e => e.preventDefault()}
          >
            {item.title}
          </Item>
        ))}
      </DropdownMenu>
    </StyledDropdown>
  )
}

UserMenuItem.propTypes = {
  /** Content of the button of the dropdown */
  children: PropTypes.node.isRequired,

  /** Dropdow direction */
  direction: PropTypes.oneOf(['up', 'down']),
  /** Icon Position (Defines the position of the icon (if is is at the start of the button , or at the end)) */
  iconPosition: PropTypes.oneOf(['start', 'end']),
  /** List of the items for the drop down items */
  itemsList: PropTypes.arrayOf(
    PropTypes.shape({
      /** The key for the item  */
      id: PropTypes.number.isRequired,

      /** The click function for the item  */
      onClick: PropTypes.func,

      /** The title for the item  */
      title: PropTypes.node.isRequired,
    }),
  ),

  size: PropTypes.string,
  variant: PropTypes.string,

  /** Primary property for the dropdown, if it is false(or not set) then the dropdown will be secondary  */
  primary: PropTypes.bool,
}

UserMenuItem.defaultProps = {
  direction: 'down',
  iconPosition: 'end',
  itemsList: [],
  primary: false,
  size: '',
  variant: '',
}

export default UserMenuItem
