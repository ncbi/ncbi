/* eslint-disable react/prop-types */
import React, { useContext } from 'react'
import { cloneDeep, sortBy } from 'lodash'
import styled from 'styled-components'

import { grid, th } from '@pubsweet/ui-toolkit'

import { RightAlign } from './common'
import ConstantsContext from '../../app/constantsContext'
import {
  EditorComponent as Editor,
  SelectComponent as Select,
  TextFieldComponent as UITextField,
  ToggleComponent as UIToggle,
} from './FormElements/FormikElements'
import Button from './common/Button'

const Note = styled.div`
  > div {
    margin-bottom: ${grid(2)};
  }
`

const Notes = styled.div`
  > div:not(:last-child) {
    border-bottom: 1px solid ${th('colorBorder')};
    margin-bottom: ${grid(4)};
    padding-bottom: ${grid(3)};
  }
`

const TextField = styled(UITextField)`
  margin: 0;
`

const Toggle = styled(UIToggle)`
  max-width: 500px;
`

const Tag = styled.div`
  background-color: gray;
  border-radius: ${th('borderRadius')};
  color: ${th('colorTextReverse')};
  display: inline-block;
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding: ${grid(0.25)} ${grid(0.5)};
`

const commonOptions = [
  { label: 'Author note', value: 'author-note' },
  { label: 'Disclaimer', value: 'disclaimer' },
  { label: 'Funder courtesy note', value: 'courtesy-note' },
  { label: 'Generic', value: 'generic' },
]

const fundedCollectionOptions = commonOptions

const bookSeriesCollectionOptions = commonOptions.filter(
  o => o.value !== 'courtesy-note',
)

const chapterProcessedOptions = [
  ...commonOptions,
  { label: 'Editorial note: genetic counselling', value: 'geneticCounseling' },
  { label: 'Editorial note: resources', value: 'resources' },
  {
    label: 'Manuscript final version note',
    value: 'manuscriptFinalVersionNote',
  },
  { label: 'Suggested citation', value: 'suggestedCitation' },
]

const NotesContainer = props => {
  const { remove, form, name, disabled, variant, isFundedCollection } = props

  const arrayValues = cloneDeep(form.values.notes || [])
  const { s } = useContext(ConstantsContext)

  let typeOptions = []
  if (variant === 'collection' && !isFundedCollection)
    typeOptions = bookSeriesCollectionOptions
  if (variant === 'collection' && isFundedCollection)
    typeOptions = fundedCollectionOptions
  if (variant === 'chapterProcessed') typeOptions = chapterProcessedOptions
  const noteTypeOptions = sortBy(typeOptions, 'label')

  return (
    <Notes>
      {arrayValues.map((note, index) => {
        const noteName = `${name}[${index}]`
        const noteValue = form.values[name][index]
        const readOnly = disabled || noteValue.fromCollection

        const handleRemove = () => remove(index)

        const handleTypeChange = option => {
          const newNoteValue = {
            ...noteValue,
            type: option.value,
          }

          if (variant === 'collection' && option.value === 'courtesy-note') {
            newNoteValue.applyNotePdf = false
          }

          form.setFieldValue(noteName, newNoteValue)
        }

        return (
          /* eslint-disable-next-line react/no-array-index-key */
          <Note key={index}>
            {noteValue.fromCollection && (
              <div>
                <Tag>Applied from collection</Tag>
              </div>
            )}

            <Select
              data-test-id="typeOfNote-select"
              disabled={readOnly}
              field={{
                name: `${noteName}.type`,
                value: noteValue.type,
              }}
              form={form}
              label="Type"
              onChange={handleTypeChange}
              options={noteTypeOptions}
              placeholder={s('form.selectTypeNote')}
              required
            />

            <TextField
              disabled={readOnly}
              field={{
                name: `${noteName}.title`,
                value: noteValue.title,
              }}
              form={form}
              label={s('form.title')}
            />
            <div id="suggestion" style={{ display: 'none' }}>
              <div>Suggestion 1</div>
              <div>Suggestion 2</div>
              <div>Suggestion 3</div>
            </div>
            <Editor
              disabled={readOnly}
              editorType="notes"
              field={{
                name: `${noteName}.description`,
                value: noteValue.description,
              }}
              form={form}
              key={readOnly}
              label="Description"
              required
            />

            {variant === 'collection' &&
              noteValue.type === 'courtesy-note' &&
              isFundedCollection && (
                <Toggle
                  description={s(
                    'fieldDescriptions.metadataForm.applyToPDFBooks',
                  )}
                  disabled={disabled}
                  field={{
                    name: `${noteName}.applyNotePdf`,
                    value: noteValue.applyNotePdf,
                  }}
                  form={form}
                  label={s('form.applyToPDFBooksCollection')}
                  locked
                  lockPosition="left"
                />
              )}

            <RightAlign>
              <Button
                data-test-id="delete-note"
                disabled={disabled}
                icon="trash-2"
                onClick={handleRemove}
                status="danger"
              >
                Delete Note
              </Button>
            </RightAlign>
          </Note>
        )
      })}
    </Notes>
  )
}

export default NotesContainer
