/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
import React, { useState, forwardRef } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Field } from 'formik'
import * as icons from 'react-feather'

import { grid, th } from '@pubsweet/ui-toolkit'

import {
  TextFieldComponent as TextField,
  CreatableSelectComponent as Select,
} from '../FormElements/FormikElements'
import UIButton from '../common/Button'

// #region styled
const Wrapper = styled.div`
  border: 1px solid
    ${props =>
      props.hovering
        ? props.theme.colorDisabled
        : props.theme.colorTextPlaceholder};
  border-radius: ${th('borderRadius')};
  position: relative;
  transition: border 0.1s ease-in-out;
`

const MoveIconContainer = styled.span`
  cursor: grab;
  display: inline-flex;
  padding: ${grid(0.5)};

  svg {
    height: calc(${props => props.size} * ${th('gridUnit')});
    stroke: ${props => props.color || props.theme.colorText};
    width: calc(${props => props.size} * ${th('gridUnit')});
  }
`

const InnerWrapper = styled.div`
  display: flex;
  padding: ${grid(2)};
`

const DragWrapper = styled.div`
  align-items: center;
  display: flex;
  padding-top: ${props => (props.singleRow ? '20px' : '0')};
`

const Index = styled.span`
  background-color: ${th('colorDisabled')};
  border-radius: ${th('borderRadius')} 0 ${th('borderRadius')} 0;
  color: ${th('colorTextReverse')};
  padding: 0 ${grid(0.5)};
  position: absolute;
`

const TextFieldWrapper = styled.div`
  display: grid;
  flex-grow: 1;
  gap: ${grid(2)};
  grid-template-columns: repeat(3, minmax(0, 1fr));
  margin: 0 ${grid(2)};
`

const ButtonWrapper = styled.div`
  align-items: ${props => (props.singleRow ? 'flex-end' : 'center')};
  display: flex;
`

const SelectWrapper = styled.div`
  min-width: 580px;

  .select__control {
    height: auto;
  }

  .select__multi-value__label {
    white-space: normal;
  }
`

const Button = styled(UIButton)`
  margin: 0;
`

// #endregion styled

const MoveIcon = forwardRef(
  ({ children, color, size = 3, theme, ...props }, ref) => {
    const icon = icons.Move

    return (
      <MoveIconContainer
        color={th('colorText')}
        ref={ref}
        role="img"
        size={size}
      >
        {icon ? icon({}) : ''}
      </MoveIconContainer>
    )
  },
)

const ContributorRow = props => {
  const {
    className,
    /* eslint-disable-next-line react/prop-types */
    ref,
    disabled,
    fields,
    index,
    onDelete,
    arrayHelpers,
    addOption,
    dragHandlerRef,
  } = props

  const [hovering, setHovering] = useState(false)
  const isSingleRow = fields.length <= 3
  const { form } = arrayHelpers

  const handleSelectChange = (item, event, field) => {
    let selectedItems = []

    switch (event.action) {
      case 'clear':
        form.setFieldValue(field.name, [])

        break
      case 'create-option':
        {
          const lastIndex = item?.length - 1

          const createdOption =
            typeof item === 'string'
              ? { value: item, label: item }
              : item[lastIndex]

          addOption(createdOption)

          form.setFieldValue(
            field.name,
            [...field.value, createdOption.value].filter(x => !!x),
          )
        }

        break
      case 'deselect-option':
      case 'select-option':
        if (typeof item === 'string') {
          selectedItems.push({ label: item, value: item })
        } else {
          item.map(itm => {
            if (typeof itm === 'string') {
              return { label: itm, value: itm }
            }

            return itm
          })

          selectedItems = item
        }

        form.setFieldValue(
          field.name,
          selectedItems.map(i => i.value).filter(x => !!x),
        )

        break
      case 'set-value':
      case 'pop-value':
      case 'remove-value':
        break
      default:
    }
  }

  return (
    <Wrapper className={className} hovering={hovering} ref={ref}>
      <Index>{index + 1}</Index>

      <InnerWrapper>
        <DragWrapper singleRow={isSingleRow}>
          {/* <Icon
            disabled={disabled}
            onMouseEnter={() => setHovering(true)}
            onMouseLeave={() => setHovering(false)}
            size={2}
          >
            move
          </Icon> */}
          <MoveIcon ref={dragHandlerRef} />
        </DragWrapper>

        <TextFieldWrapper>
          {fields.map(field => {
            if (field.component === 'select') {
              return (
                <SelectWrapper>
                  <Select
                    field={{
                      name: field.name,
                      value: field.value || [],
                    }}
                    form={form}
                    isSearchable
                    multiple
                    onChange={(item, event) =>
                      handleSelectChange(item, event, field)
                    }
                    {...field}
                    createOptionPosition="first"
                    hideSelectedOptions
                    label={field.label}
                    options={field.options}
                  />
                </SelectWrapper>
              )
            }

            return (
              <Field
                {...field}
                component={field.component || TextField}
                disabled={disabled}
                key={field.key}
                label={field.label}
                name={field.name}
                placeholder={field.placeholder}
              />
            )
          })}
        </TextFieldWrapper>

        <ButtonWrapper singleRow={isSingleRow}>
          <Button
            data-test-id={`delete-${index + 1}`}
            disabled={disabled}
            icon="trash-2"
            onClick={onDelete}
            onMouseEnter={() => setHovering(true)}
            onMouseLeave={() => setHovering(false)}
            status="danger"
          >
            Delete
          </Button>
        </ButtonWrapper>
      </InnerWrapper>
    </Wrapper>
  )
}

ContributorRow.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  arrayHelpers: PropTypes.objectOf(PropTypes.object).isRequired,
  addOption: PropTypes.func,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      placeholder: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ).isRequired,
  index: PropTypes.number.isRequired,
  onDelete: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
}

ContributorRow.defaultProps = {
  addOption: () => {},
  disabled: false,
}

export default ContributorRow
