import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { ErrorMessage } from 'formik'

import ConstantsContext from '../../../app/constantsContext'
import Accordion from '../common/Accordion'
import Button from '../common/Button'
import { Error } from '../FormElements/FormikElements'
import ContributorsContainer from './ContributorsContainer'

const Wrapper = styled.div``

const ContributorsComponent = props => {
  const {
    className,
    disabled,
    name,
    onChange,
    value,
    variant,
    affiliations,
    setAffiliations,
  } = props

  const { s } = useContext(ConstantsContext)

  const columnTypes = {
    affiliation: {
      label: 'Affiliation',
      key: 'affiliation',
      placeholder: 'Affiliation',
      component: 'select',
      multiple: true,
      isSearchable: true,
      options: affiliations,
      'data-test-id': 'affiliation-select',
    },
    degrees: {
      label: 'Degrees',
      key: 'degrees',
      placeholder: 'Degrees',
    },
    email: {
      label: 'Email',
      key: 'email',
      placeholder: 'Email',
    },
    givenName: {
      label: 'Given name',
      key: 'givenName',
      placeholder: 'Given name',
    },
    role: {
      label: 'Role',
      key: 'role',
      placeholder: 'Role',
    },
    suffix: {
      label: 'Suffix',
      key: 'suffix',
      placeholder: 'Suffix',
    },
    surname: {
      label: 'Surname',
      key: 'surname',
      placeholder: 'Surname',
    },
    authorName: {
      label: 'Author name',
      key: 'givenName',
      placeholder: 'Author name',
    },
  }

  const {
    affiliation,
    degrees,
    email,
    givenName,
    role,
    suffix,
    surname,
    authorName,
  } = columnTypes

  const authorEditorColumns = [
    surname,
    givenName,
    suffix,
    degrees,
    role,
    email,
    affiliation,
  ]

  const variants = {
    collaborativeAuthors: {
      label: 'Collaborative authors',
      columns: [authorName],
    },
    seriesEditors: {
      label: 'Series editors',
      columns: [surname, givenName, role, degrees],
    },
    authors: {
      label: 'Authors',
      columns: authorEditorColumns,
    },
    editors: {
      label: 'Editors',
      columns: authorEditorColumns,
    },
    componentEditors: {
      label: 'Editors',
      columns: [surname, givenName, suffix],
    },
  }

  const makeEmptyRow = thisVariant => {
    const emptyRow = {}

    variants[thisVariant].columns.forEach(column => {
      if (column.component === 'select') {
        emptyRow[column.key] = []
      } else {
        emptyRow[column.key] = ''
      }
    })

    return emptyRow
  }

  const handleClickAdd = e => {
    e.stopPropagation()
    const newValue = [...value, makeEmptyRow(variant)]
    onChange(newValue)
  }

  const addOption = option => {
    const submitOption =
      typeof option === 'string'
        ? { value: option, label: option }
        : { value: option.value, label: option.label }

    setAffiliations([...affiliations, submitOption])
  }

  return (
    <Wrapper className={className}>
      <Accordion
        headerRightComponent={
          <Button
            data-test-id={`add-${name}`}
            disabled={disabled}
            icon="plus"
            onClick={handleClickAdd}
            status="primary"
          >
            {s('form.add')}
          </Button>
        }
        label={variants[variant].label}
        startExpanded
      >
        <ContributorsContainer
          addOption={addOption}
          columns={variants[variant].columns}
          contributors={value}
          disabled={disabled}
          name={name}
        />

        <ErrorMessage component={Error} name={name} />
      </Accordion>
    </Wrapper>
  )
}

const contributorShape = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  key: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
}

const affiliationShape = {
  label: PropTypes.string,
  value: PropTypes.string,
}

ContributorsComponent.propTypes = {
  affiliations: PropTypes.arrayOf(PropTypes.shape(affiliationShape)),
  setAffiliations: PropTypes.func,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  value: PropTypes.arrayOf(PropTypes.shape(contributorShape)),

  variant: PropTypes.oneOf([
    'seriesEditors',
    'collaborativeAuthors',
    'editors',
    'authors',
  ]).isRequired,
}

ContributorsComponent.defaultProps = {
  affiliations: [],
  setAffiliations: () => {},
  disabled: false,
  onChange: () => {},
  value: [],
}

export default ContributorsComponent
