/* eslint-disable react/prop-types */
import React from 'react'
import { FieldArray } from 'formik'
import styled from 'styled-components'
import { cloneDeep } from 'lodash'

import { grid } from '@pubsweet/ui-toolkit'

import Row from './Row'

const FieldWrapper = styled.div`
  > div:not(:last-child) {
    margin-bottom: ${grid(1)};
  }
`

const ContributorsContainer = ({
  contributors,
  disabled,
  name,
  columns,
  addOption,
}) => {
  const arrayValues = cloneDeep(contributors || [])

  return (
    <FieldArray
      key="contributors-header"
      name={name}
      render={arrayHelpers => {
        const moveCard = (dragIndex, hoverIndex) => {
          arrayHelpers.swap(dragIndex, hoverIndex)
        }

        if (arrayValues.length === 0)
          return <center>No contributors added yet</center>

        return (
          <FieldWrapper>
            {arrayValues.map((item, index) => (
              <Row
                addOption={addOption}
                arrayHelpers={arrayHelpers}
                columns={columns}
                disabled={disabled}
                id={item.id}
                index={index}
                item={item}
                key={item.id || name + index}
                moveCard={moveCard}
                name={name}
              />
            ))}
          </FieldWrapper>
        )
      }}
    />
  )
}

export default ContributorsContainer
