/* eslint-disable no-param-reassign */
/* eslint-disable consistent-return */
/* eslint-disable react/prop-types */

import React, { useRef, useState, forwardRef } from 'react'
import { useDrag, useDrop } from 'react-dnd'
import cloneDeep from 'lodash/cloneDeep'
import { Field } from 'formik'
import * as icons from 'react-feather'
import styled from 'styled-components'
import { grid, th } from '@pubsweet/ui-toolkit'
// import ContributorRow from './ContributorRow'

import {
  TextFieldComponent as TextField,
  CreatableSelectComponent as Select,
} from '../FormElements/FormikElements'
import UIButton from '../common/Button'

// #region styled
const Wrapper = styled.div`
  border: 1px solid
    ${props =>
      props.hovering
        ? props.theme.colorDisabled
        : props.theme.colorTextPlaceholder};
  border-radius: ${th('borderRadius')};
  position: relative;
  transition: border 0.1s ease-in-out;
`

const MoveIconContainer = styled.span`
  cursor: grab;
  display: inline-flex;
  padding: ${grid(0.5)};

  svg {
    height: calc(${props => props.size} * ${th('gridUnit')});
    stroke: ${props => props.color || props.theme.colorText};
    width: calc(${props => props.size} * ${th('gridUnit')});
  }
`

const InnerWrapper = styled.div`
  display: flex;
  padding: ${grid(2)};
`

const DragWrapper = styled.div`
  align-items: center;
  display: flex;
  padding-top: ${props => (props.singleRow ? '20px' : '0')};
`

const Index = styled.span`
  background-color: ${th('colorDisabled')};
  border-radius: ${th('borderRadius')} 0 ${th('borderRadius')} 0;
  color: ${th('colorTextReverse')};
  padding: 0 ${grid(0.5)};
  position: absolute;
`

const TextFieldWrapper = styled.div`
  display: grid;
  flex-grow: 1;
  gap: ${grid(2)};
  grid-template-columns: repeat(3, minmax(0, 1fr));
  margin: 0 ${grid(2)};
`

const ButtonWrapper = styled.div`
  align-items: ${props => (props.singleRow ? 'flex-end' : 'center')};
  display: flex;
`

const SelectWrapper = styled.div`
  min-width: 580px;

  .select__control {
    height: auto;
  }

  .select__multi-value__label {
    white-space: normal;
  }
`

const Button = styled(UIButton)`
  margin: 0;
`

// #endregion styled

const MoveIcon = forwardRef(
  ({ children, color, size = 3, theme, ...props }, ref) => {
    const icon = icons.Move

    return (
      <MoveIconContainer
        color={th('colorText')}
        ref={ref}
        role="img"
        size={size}
      >
        {icon ? icon({}) : ''}
      </MoveIconContainer>
    )
  },
)

const Row = ({
  arrayHelpers,
  columns,
  disabled,
  index,
  item,
  name,
  addOption,
  fields,
  moveCard,
}) => {
  const [hovering, setHovering] = useState(false)
  const isSingleRow = fields?.length <= 3
  const { form } = arrayHelpers

  // eslint-disable-next-line no-shadow
  const handleSelectChange = (item, event, field) => {
    let selectedItems = []

    switch (event.action) {
      case 'clear':
        form.setFieldValue(field.name, [])

        break
      case 'create-option':
        {
          const lastIndex = item?.length - 1

          const createdOption =
            typeof item === 'string'
              ? { value: item, label: item }
              : item[lastIndex]

          addOption(createdOption)

          form.setFieldValue(
            field.name,
            [...field.value, createdOption.value].filter(x => !!x),
          )
        }

        break
      case 'deselect-option':
      case 'select-option':
        if (typeof item === 'string') {
          selectedItems.push({ label: item, value: item })
        } else {
          item.map(itm => {
            if (typeof itm === 'string') {
              return { label: itm, value: itm }
            }

            return itm
          })

          selectedItems = item
        }

        form.setFieldValue(
          field.name,
          selectedItems.map(i => i.value).filter(x => !!x),
        )

        break
      case 'set-value':
      case 'pop-value':
      case 'remove-value':
        break
      default:
    }
  }

  const elementRef = useRef(null)
  const dragHandlerRef = useRef(null)

  const targetRow = item
  const targetIndex = index

  const [, connectDrag, preview] = useDrag({
    type: 'Row',
    item: { index, row: item },
    beginDrag: props => ({
      id: props.id,
      index: props.index,
    }),
    collect(props, monitor, component) {
      // console.log(index)
      return {
        index: props.index,
        listId: props.listId,
        row: cloneDeep(props.row),
        sorceParentType: component?.getNode().parentNode?.dataset?.type,
        sourceType: component?.getNode().dataset?.type,
      }
    },
  })

  const [, connectDrop] = useDrop({
    accept: 'Row',
    item,
    canDrag() {
      return true
    },
    hover(props, monitor) {
      const component = targetRow
      const hoverIndex = targetIndex

      if (!component) {
        return null
      }

      // Don't replace items with themselves
      if (props.index === hoverIndex) {
        return
      }

      moveCard(props.index, hoverIndex)

      monitor.getItem().index = hoverIndex
    },
  })

  connectDrag(dragHandlerRef)
  connectDrop(preview(elementRef))

  const deleteRow = () => arrayHelpers.remove(index)

  const columnData = columns.map(column => ({
    ...column,
    name: `${name}[${index}].${column.key}`,
    value: item[column.key],
  }))

  return (
    <Wrapper hovering={hovering} ref={elementRef}>
      <Index>{index + 1}</Index>

      <InnerWrapper>
        <DragWrapper singleRow={isSingleRow}>
          {/* <Icon
          disabled={disabled}
          onMouseEnter={() => setHovering(true)}
          onMouseLeave={() => setHovering(false)}
          size={2}
        >
          move
        </Icon> */}
          <MoveIcon ref={dragHandlerRef} />
        </DragWrapper>

        <TextFieldWrapper>
          {columnData.map(field => {
            if (field.component === 'select') {
              return (
                <SelectWrapper key={field.name}>
                  <Select
                    field={{
                      name: field.name,
                      value: field.value || [],
                    }}
                    form={form}
                    isSearchable
                    multiple
                    onChange={(it, event) =>
                      handleSelectChange(it, event, field)
                    }
                    {...field}
                    createOptionPosition="first"
                    hideSelectedOptions
                    label={field.label}
                    options={field.options}
                  />
                </SelectWrapper>
              )
            }

            return (
              <Field
                key={field.name}
                {...field}
                component={field.component || TextField}
                disabled={disabled}
                label={field.label}
                name={field.name}
                placeholder={field.placeholder}
              />
            )
          })}
        </TextFieldWrapper>

        <ButtonWrapper singleRow={isSingleRow}>
          <Button
            data-test-id={`delete-${index + 1}`}
            disabled={disabled}
            icon="trash-2"
            onClick={deleteRow}
            onMouseEnter={() => setHovering(true)}
            onMouseLeave={() => setHovering(false)}
            status="danger"
          >
            Delete
          </Button>
        </ButtonWrapper>
      </InnerWrapper>
    </Wrapper>
  )
}

export default Row

// export default DropTarget(
//   'Row',
//   {
//     hover(props, monitor, component) {
//       if (!component) {
//         return null
//       }

//       // node = HTML Div element from imperative API
//       const node = component.getNode()

//       if (!node) {
//         return null
//       }

//       const dragIndex = monitor.getItem().index
//       const hoverIndex = props.index

//       // Don't replace items with themselves
//       if (dragIndex === hoverIndex) {
//         return
//       }

//       // Determine rectangle on screen
//       const hoverBoundingRect = node.getBoundingClientRect()

//       // Get vertical middle
//       const hoverMiddleY =
//         (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

//       // Determine mouse position
//       const clientOffset = monitor.getClientOffset()
//       // Get pixels to the top
//       const hoverClientY = clientOffset.y - hoverBoundingRect.top

//       // Only perform the move when the mouse has crossed half of the items height
//       // When dragging downwards, only move when the cursor is below 50%
//       // When dragging upwards, only move when the cursor is above 50%
//       // Dragging downwards
//       if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
//         return
//       }

//       // Dragging upwards
//       if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
//         return
//       }

//       // Time to actually perform the action
//       props.moveCard(dragIndex, hoverIndex)
//       // Note: we're mutating the monitor item here!
//       // Generally it's better to avoid mutations,
//       // but it's good here for the sake of performance
//       // to avoid expensive index searches.
//       monitor.getItem().index = hoverIndex
//     },
//   },
//   connect => ({
//     connectDropTarget: connect.dropTarget(),
//   }),
// )(
//   DragSource(
//     'Row',
//     {
//       beginDrag: props => ({
//         id: props.id,
//         index: props.index,
//       }),
//     },
//     (connect, monitor) => ({
//       connectDragSource: connect.dragSource(),
//       isDragging: monitor.isDragging(),
//     }),
//   )(Row),
// )
