/* eslint-disable react/no-danger */

import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { notification, Tree } from 'antd'
import { DownOutlined } from '@ant-design/icons'

import { Button, ButtonGroup } from './common'
import { RepeatComponents as RepeatIcon } from './common/SvgIcon'
import { AntModal as Modal } from './Modal'

// #region styled

const Item = styled.div``

const ModalBodyWrapper = styled.div`
  max-height: 60vh;
  overflow-y: auto;
`
// #endregion styled

const RepeatComponent = props => {
  const {
    isDisabled,
    onRepeat,
    partsTree,
    selectedBookComponent,
    selectedPartIdsForRepeatedComponent,
  } = props

  const [modalIsOpen, setIsOpen] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [selectedPartIds, setSelectedPartIds] = useState([])
  const [notificationApi, notificationContext] = notification.useNotification()

  useEffect(() => {
    if (modalIsOpen) {
      setSelectedPartIds(selectedPartIdsForRepeatedComponent)
    }
  }, [modalIsOpen, partsTree])

  const handleSelectPart = data => {
    setSelectedPartIds(data.checked)
  }

  const handleCloseModal = () => {
    if (!isLoading) {
      setSelectedPartIds([])
      setIsOpen(false)
    }
  }

  const handleSave = () => {
    setIsLoading(true)

    onRepeat(selectedBookComponent.id, selectedPartIds, [
      ...selectedPartIds,
      ...selectedPartIdsForRepeatedComponent,
      selectedBookComponent.parentId,
    ])
      .then(() => {
        handleCloseModal()

        notificationApi.success({
          message: 'Success',
          description: 'Item repeated successfully',
        })
      })
      .catch(() => {
        notificationApi.error({
          message: 'Error',
          description: 'Could not repeat item',
        })
      })
      .finally(() => {
        setIsLoading(false)
      })
  }

  const modalTitle = (
    <div>
      Repeat{' '}
      <span
        dangerouslySetInnerHTML={{
          __html: selectedBookComponent ? selectedBookComponent.title : '',
        }}
      />
    </div>
  )

  const modalFooter = (
    <ButtonGroup>
      <Button
        data-test-id="save-book-metadata"
        disabled={isLoading || selectedPartIds.length < 1}
        loading={isLoading}
        onClick={handleSave}
        status="primary"
        title="Select one or more parts to save"
      >
        {isLoading ? 'Saving...' : 'Save'}
      </Button>

      <Button
        data-test-id="cancel-book-metadata"
        onClick={handleCloseModal}
        outlined
        status="primary"
      >
        Close
      </Button>
    </ButtonGroup>
  )

  return (
    <>
      {notificationContext}
      <Button
        data-test-id="repeat-chapters-to"
        disabled={isDisabled}
        icon={<RepeatIcon />}
        onClick={() => {
          setIsOpen(true)
        }}
        outlined
        status="primary"
      >
        {'Repeat '}
      </Button>

      <Modal
        footer={modalFooter}
        isOpen={modalIsOpen}
        onCancel={handleCloseModal}
        title={modalTitle}
      >
        <ModalBodyWrapper>
          <Tree
            checkable
            checkedKeys={selectedPartIds}
            checkStrictly
            defaultExpandAll
            fieldNames={{ title: 'title', key: 'id', children: 'children' }}
            onCheck={handleSelectPart}
            selectable={false}
            switcherIcon={<DownOutlined />}
            titleRender={rowProps => {
              return (
                <Item {...rowProps}>
                  <span dangerouslySetInnerHTML={{ __html: rowProps.title }} />
                </Item>
              )
            }}
            treeData={partsTree}
          />
        </ModalBodyWrapper>
      </Modal>
    </>
  )
}

RepeatComponent.propTypes = {
  isDisabled: PropTypes.bool,
  /** Array of parts in a tree structure */
  partsTree: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      key: PropTypes.string,
      title: PropTypes.string,
      children: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string,
          title: PropTypes.string,
        }),
      ),
      componentType: PropTypes.string,
    }),
  ),
  onRepeat: PropTypes.func.isRequired,
  selectedBookComponent: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    parentId: PropTypes.string,
  }),
  selectedPartIdsForRepeatedComponent: PropTypes.arrayOf(PropTypes.string),
}

RepeatComponent.defaultProps = {
  selectedBookComponent: null,
  partsTree: [],
  isDisabled: false,
  selectedPartIdsForRepeatedComponent: [],
}

export default RepeatComponent
