import React from 'react'
import styled from 'styled-components'
import { th, darken } from '@pubsweet/ui-toolkit'
import PropTypes from 'prop-types'
import { Icon } from './common'

/* List of icons :
      Warning Icon
      Query Icon
      Error Icon
      Approve Icon
      Review Icon
      Revise Icon
      Preview Icon
      Technical Review Icon
      Published Icon
      Ready To Publish Icon
      Archived Icon
      In Production Icon
      New Ftp Icon
      Lock
      Admin Icon
 */

export const LetterIcon = styled.div`
  background: ${props => props.background || th('colorArchive')};
  border-radius: 50%;
  color: ${props => props.color || 'white'};
  display: inline-block;
  height: 23px;
  position: relative;
  text-align: center;
  width: 23px;

  span {
    padding: 0;
  }
`

export const CircleIcon = styled.div`
  background-color: ${props =>
    props.fill ? props.background : 'rgba(0, 0, 0, 0)'};
  border: calc(${th('gridUnit')} / 2) solid
    ${props => props.background || th('colorArchive')};
  border-radius: 50%;
  display: inline-block;
  height: 23px;
  width: 23px;

  span {
    padding: 0;
  }
`

CircleIcon.propTypes = {
  fill: PropTypes.bool,
}

CircleIcon.defaultProps = {
  fill: false,
}

export const WarningIcon = styled.span`
  border-bottom: 20px solid ${props => props.background || th('colorWarning')};
  border-left: 12px solid transparent;
  border-right: 12px solid transparent;
  display: inline-block;
  height: 0;
  position: relative;
  width: 0;

  &::after {
    align-content: center;
    color: ${props => props.color || 'black'};
    content: '!';
    display: flex;
    font-size: 18px;
    justify-content: center;
  }

  @-moz-document url-prefix() {
    &::after {
      margin-top: -1px;
    }
  }
`

export const QueryIcon = styled.span`
  border-bottom: 20px solid ${props => props.background || th('colorWarning')};
  border-left: 12px solid transparent;
  border-right: 12px solid transparent;
  display: inline-block;
  height: 0;
  position: relative;
  width: 0;

  &::after {
    align-content: center;
    color: ${props => props.color || 'black'};
    content: '?';
    display: flex;
    font-size: 17px;
    justify-content: center;
    /* margin-top: 1px; */
    position: absolute;
    right: -3px;
  }

  @-moz-document url-prefix() {
    &::after {
      margin-top: -1px;
    }
  }
`

export const ErrorIcon = styled.div`
  background: ${props => props.background || th('colorError')};
  border-radius: 50%;
  display: inline-block;
  height: 20px;
  position: relative;
  width: 20px;

  &::after {
    align-content: center;
    color: ${props => props.color || 'white'};
    content: '! ';
    display: flex;
    font-size: 20px;
    justify-content: center;
    line-height: 1;
    width: 20px;
  }

  @-moz-document url-prefix() {
    &::after {
      margin-top: -1px;
    }
  }
`
export const VendorIcon = styled.div`
  align-items: center;
  border: 2px solid ${props => props.background || th('colorWarning')};
  border-radius: 50%;
  /* stylelint-disable-next-line declaration-no-important */
  display: flex !important;
  height: 21px;
  justify-content: center;
  position: relative;
  width: 21px;

  &::after {
    align-content: center;
    background: ${props => props.background || th('colorWarning')};
    border-radius: 50%;
    color: ${props => props.color || 'white'};
    content: '? ';
    display: flex;
    font-size: 12px;
    height: 12px;
    justify-content: center;
    line-height: 1;
    width: 12px;
  }

  @-moz-document url-prefix() {
    &::after {
      margin-top: -1px;
    }
  }
`

export const OpenIssuesIcon = styled.div`
  border: 1px solid ${props => props.background || th('colorPrimary')};
  height: 20px;
  margin: 4px;
  padding: 4px;

  &::after {
    align-items: center;
    color: ${props => props.color || th('colorPrimary')};
    content: 'OPEN ${props => `${props.numerator}/${props.denominator}` || ''}';
    display: flex;
    font-size: 12px;
    font-weight: 400;
    height: 100%;
    justify-content: center;

    /* stylelint-disable-next-line declaration-no-important */
    /* stylelint-disable-next-line declaration-no-important */
  }
`

export const ApproveIcon = () => (
  <LetterIcon background={th('colorApprove')}>
    <Icon color="colorBackground" size={3}>
      check
    </Icon>
  </LetterIcon>
)

export const ReviewIcon = () => (
  <LetterIcon background={th('colorReview')}>R</LetterIcon>
)

export const ReviseIcon = () => (
  <LetterIcon background={th('colorRevise')}>X</LetterIcon>
)

export const PreviewIcon = () => (
  <LetterIcon background={th('colorReview')}>P</LetterIcon>
)

export const NewVersionIcon = () => (
  <LetterIcon background={th('colorReview')}>N</LetterIcon>
)

export const TechnicalReviewIcon = () => (
  <LetterIcon background={th('colorReview')}>T</LetterIcon>
)

export const PublishedIcon = () => (
  <CircleIcon background={th('colorSuccess')} fill />
)

export const PublishingIcon = () => (
  <LetterIcon background={th('colorSuccess')}>
    <Icon color="colorBackground" size={1.7}>
      refresh-ccw
    </Icon>
  </LetterIcon>
)

export const ArchivedIcon = () => (
  <CircleIcon background={th('colorArchive')} fill />
)

export const InProductionIcon = () => (
  <CircleIcon background={th('colorReview')} fill />
)

export const NewFtpIcon = () => (
  <LetterIcon background={th('colorReview')}>F</LetterIcon>
)

export const LockIcon = () => (
  <Icon color="#9d9d9d" size={2}>
    lock
  </Icon>
)

export const UnLockIcon = () => (
  <Icon color="colorPrimaryDark" size={2}>
    unlock
  </Icon>
)

export const ConvertingIcon = () => (
  <LetterIcon background={th('colorBlue')}>
    <Icon color="colorBackground" size={1.7}>
      refresh-ccw
    </Icon>
  </LetterIcon>
)

export const UnPublishedIcon = () => (
  <LetterIcon background={th('colorPrimaryDark')}>
    <Icon color="colorBackground" size={1.8}>
      pause
    </Icon>
  </LetterIcon>
)
export const DeleteIcon = styled(Icon)`
  background: ${th('colorError')};
  border-radius: 8px;
  cursor: pointer;

  &:hover {
    background: ${darken('colorError', 20)};
  }
`

export const AdminIcon = ({ className }) => (
  <LetterIcon background={th('colorBlue')} className={className}>
    <Icon color="colorBackground" size={1.7}>
      check
    </Icon>
  </LetterIcon>
)
