/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import merge from 'deepmerge'
import { get } from 'lodash'

import useCustomQuery from '../../app/useCustomQuery'
import FixedLoader from './common/FixedLoader'
import { ActionBar, ButtonGroup } from './common'
import Pagination from './Datatable/Pagination'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

export default ({
  children,
  className,
  data: dataFromProps,
  pushRightAfter,
  query,
  selected,
  setSelected,
  target,
  toolBarProps,
  tools,
  variables: initVariables,
  noPagination,
}) => {
  const [getData, { data, variables }, loading] = query
    ? useCustomQuery(query)
    : [null, [null, null]]

  let [selectedRows, setSelectedRows] = useState(selected || [])

  if (selected && setSelected) {
    selectedRows = selected
    setSelectedRows = setSelected
  }

  const useDataFromProps = !query && dataFromProps

  useEffect(() => {
    if (getData != null) {
      getData({
        variables: { ...variables, ...initVariables },
      })
    }
  }, [JSON.stringify(initVariables)])

  const rootResults = useDataFromProps
    ? dataFromProps
    : get(data || [], target) || {
        metadata: { total: 0 },
        results: [],
      }

  const loadedData = rootResults.results.map(row => {
    const rowChecked = { ...row, checked: selectedRows.includes(row.id) }
    return rowChecked
  })

  const count = rootResults.metadata.total

  if (parseInt(count, 10) > 0 && loadedData.length === 0) {
    getData({
      variables: {
        ...variables,
        input: {
          ...variables?.input,
          skip: (variables?.input?.skip || 0) - (variables?.input?.take || 0),
        },
      },
    })
  }

  const mergeDataVariables = input => {
    getData({
      variables: merge(variables, input),
    })
  }

  const rowsSelection = item => {
    if (item === true) {
      setSelectedRows(['all'])
      return
    }

    if (item === false) {
      setSelectedRows([])
      return
    }

    if (Array.isArray(item)) {
      const itemIds = item.map(tm => tm.id)

      if (item[0]?.checked) {
        const addSelectedRow = selectedRows.concat(itemIds)
        setSelectedRows([...new Set(addSelectedRow)])
      } else {
        setSelectedRows([...selectedRows.filter(id => !itemIds.includes(id))])
      }

      return
    }

    if (item.checked) {
      selectedRows.push(item.id)
      setSelectedRows([...selectedRows])
    } else {
      const index = selectedRows.indexOf(item.id)

      if (index > -1) {
        selectedRows.splice(index, 1)
      }

      setSelectedRows([...selectedRows])
    }
  }

  const checkboxColumn = {
    cellRenderer: 'checkbox',
    checkBoxFn: item => {
      rowsSelection(item)
    },
    dataKey: 'id',
    disableSort: true,
    headerRenderer: 'checkbox',
    headerRowRenderer: 'checkbox',
    width: 150,
  }

  return (
    <Wrapper className={className}>
      {children({
        checkboxColumn,
        count,
        loadedData,
        mergeDataVariables,
        selectedRows,
      })}
      {tools && (
        <ActionBar
          leftComponent={
            <ButtonGroup>
              {tools.left &&
                tools.left.map(Tool => (
                  <Tool
                    key={`tool-${Tool.toString()}`}
                    loadedData={loadedData}
                    selectedRows={selectedRows}
                    setSelectedRows={setSelectedRows}
                    {...toolBarProps}
                  />
                ))}
            </ButtonGroup>
          }
          middleComponent={[
            !noPagination && (
              <Pagination
                currentPage={
                  variables
                    ? variables?.input?.skip / variables?.input?.take + 1
                    : 1
                }
                key="pagination"
                onPageChanged={page => {
                  const { currentPage } = page
                  setSelectedRows([])
                  const variable = variables || initVariables

                  getData({
                    variables: merge(variable, {
                      input: {
                        skip:
                          variable?.input?.take * currentPage -
                          variable?.input?.take,
                        take: variable?.input?.take,
                      },
                    }),
                  })
                }}
                pageLimit={
                  variables ? parseInt(variables?.input?.take, 10) : 10
                }
                pageNeighbours={1}
                showFirstPage={false}
                totalRecords={parseInt(count, 10)}
              />
            ),
          ]}
          rightComponent={
            <ButtonGroup>
              {tools.right &&
                tools.right.map(Tool => (
                  <Tool
                    key={`tool-${Tool.toString()}`}
                    loadedData={loadedData}
                    selectedRows={selectedRows}
                    setSelectedRows={setSelectedRows}
                    {...toolBarProps}
                  />
                ))}
            </ButtonGroup>
          }
        />
      )}
      {loading && <FixedLoader title="Loading data" />}
    </Wrapper>
  )
}
