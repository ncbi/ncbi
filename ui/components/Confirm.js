import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { Button } from './common'

// eslint-disable-next-line import/no-cycle
import { Modal as UIModal, ModalBody, ModalHeader, ModalFooter } from '../index'

const Center = styled.div`
  text-align: center;
`

const Modal = styled(UIModal)`
  min-height: unset;
`

const Confirm = ({
  title,
  isOpen,
  description,
  confirmAction,
  hideModal,
  confirmText,
  cancelText,
}) => (
  <Modal isOpen={isOpen} onClose={hideModal} role="dialog" width="500px">
    <ModalHeader hideModal={hideModal} />
    <ModalBody>
      <Center>
        <h4>{title}</h4>
        <div>{description}</div>
      </Center>
    </ModalBody>

    <ModalFooter>
      <Button
        data-test-id="confirmation-cancel"
        onClick={hideModal}
        outlined
        status="primary"
      >
        {cancelText}
      </Button>
      <Button
        data-test-id="confirmation-confirm"
        onClick={confirmAction}
        status="primary"
      >
        {confirmText}
      </Button>
    </ModalFooter>
  </Modal>
)

Confirm.propTypes = {
  title: PropTypes.node,
  description: PropTypes.node,
  confirmAction: PropTypes.func.isRequired,
  hideModal: PropTypes.func.isRequired,
  isOpen: PropTypes.bool,
  confirmText: PropTypes.node,
  cancelText: PropTypes.node,
}

Confirm.defaultProps = {
  title: 'Are you sure ?',
  description: null,
  confirmText: 'Yes',
  cancelText: 'Cancel',
  isOpen: false,
}

export default Confirm
