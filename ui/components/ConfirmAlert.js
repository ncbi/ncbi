/* eslint-disable react/prop-types */
import React from 'react'
import { confirmAlert } from 'react-confirm-alert' // Import
import './react-confirm-alert.css'

const ConfirmAlert = ({ title, message, onConfirm, onCancel = () => {} }) => {
  return confirmAlert({
    customUI: ({ onClose }) => {
      const closeFn = () => {
        onCancel()
        onClose()
      }

      return (
        <div id="react-confirm-alert">
          <div className="react-confirm-alert-overlay">
            <div className="react-confirm-alert">
              <div className="react-confirm-alert-body">
                <h1>{title}</h1>
                {message}
                <div className="react-confirm-alert-button-group1">
                  <button
                    onClick={() => {
                      onConfirm()
                      closeFn()
                    }}
                    type="button"
                  >
                    Yes
                  </button>
                  <button onClick={closeFn} type="button">
                    No
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    },
  })
}

export default ConfirmAlert
