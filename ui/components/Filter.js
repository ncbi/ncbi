import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'

import { th } from '@pubsweet/ui-toolkit'

import { Checkbox } from './common'

const Wrapper = styled.div`
  background: #ffffff;
  box-shadow: 0px 0px 25px rgba(33, 33, 33, 0.15);
  display: ${props => (props.isOpened ? 'block' : 'none')};
  min-height: 100px;
  min-width: 200px;
  padding: ${th('gridUnit')} ${th('gridUnit')} ${th('gridUnit')}
    calc(${th('gridUnit')} * 2);
  position: absolute;
  right: calc(${th('gridUnit')} * 2);
  top: calc(${th('gridUnit')} * 14);
  z-index: 300;
`

const HeaderItem = styled.div`
  font-size: ${th('fontSizeBaseSmall')};
  font-weight: 600;
`

const Item = styled.div`
  font-size: ${th('fontSizeBaseSmall')};
`

const Filter = ({ items, isOpened }) => (
  // const [filterItem , setFilterItem] = useState
  <Wrapper isOpened={isOpened}>
    {items.map(item => {
      const component = []

      if (item.elements && item.elements.length > 0) {
        component.push(<HeaderItem>{item.label}</HeaderItem>)

        component.push(
          item.elements.map(element => {
            let checked = element.isChecked
            return (
              <Item key={uuid()}>
                <Checkbox
                  checked={checked}
                  label={element.label}
                  onClick={() => {
                    checked = !checked
                  }}
                />
              </Item>
            )
          }),
        )
      }

      return component
    })}
  </Wrapper>
)

Filter.propTypes = {
  isOpened: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.element),
}

Filter.defaultProps = {
  isOpened: false,
  items: [],
}

export default Filter
