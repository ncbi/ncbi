/* eslint-disable no-console */
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  on('task', {
    async NCBI_Kafka_Mock({ topic, message }) {
      // eslint-disable-next-line global-require
      const KafkaRest = require('kafka-rest')

      const kafka = new KafkaRest({
        url: 'https://test.ncbi.nlm.nih.gov/books/kafka',
      })

      kafka
        .topic(topic)
        // eslint-disable-next-line func-names
        .produce({ partition: 0, value: message }, function (err, res) {
          console.log(err)
          console.log(res)
        })

      return true
    },
  })
}
