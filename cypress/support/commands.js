/* eslint-disable consistent-return */
import 'cypress-file-upload'

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
Cypress.Commands.add('login', ({ username, password }) => {
  cy.visit('/login')

  cy.get('input[name="username"]').type(username)
  cy.get('input[name="password"]').type(password)
  cy.get('button[type="submit"]').click()

  // cy.url().should('eq', `${Cypress.config().baseUrl}/dashboard`)
})

Cypress.Commands.add('logout', () => {
  cy.get('[data-test-id="user-menu"]').click()
  cy.contains('Logout').click()
  //   cy.url().contains(`${Cypress.config().baseUrl}/login`)
})

Cypress.Commands.add('addUser', user => {
  cy.visit('/signup')
  cy.get('[name="givenName"]').type(user.givenName)
  cy.get('[name="surname"]').type(user.surname)
  cy.get('[name="username"]').type(user.username)
  cy.get('[name="email"]').type(user.email)
  cy.get('[name="password"]').type(user.password)
  cy.get('[name="confirmPassword"]').type(user.password)
  cy.contains('button', 'Register').click()
  cy.contains('div', 'User has been created successfully!')
})

Cypress.Commands.add(
  'addOrganisation',
  (
    organisation,
    workflow = null,
    bookShelfType = 'name="settings.type.funder"',
    collectionType = 'name="settings.collections.funded"',
  ) => {
    cy.get('button[data-test-id="create-organisation"]').click()
    cy.contains('New Organization')

    cy.get('input[name="name"]').type(organisation.name)
    cy.get('input[name="abbreviation"]').type(organisation.abbreviatedName)
    cy.get(`input[${bookShelfType}]`).click({ force: true })
    cy.get(`input[${collectionType}]`).click({ force: true })

    if (workflow && !Array.isArray(workflow)) {
      cy.get(`input[${workflow.selector}]`).click({ force: true })
    } else if (workflow) {
      workflow.forEach(wf => {
        cy.get(`input[${wf.selector}]`).click({ force: true })
      })
    }

    cy.get('button[data-test-id="save-new-organisation"]').click()
    cy.contains(organisation.name)
  },
)

Cypress.Commands.add(
  'addBook',
  (
    bookTitle,
    wfName1,
    bookType,
    contentType = 'Final full-text',
    collectionName = '',
  ) => {
    cy.get('[data-test-id="create-new-book"]').click()
    cy.get('[name="title"]').type(bookTitle)
    cy.get('[data-test-id="contentType-select"]').type(`${contentType} {enter}`)

    cy.contains('span', wfName1).click({ force: true })
    cy.contains('span', bookType).click()

    cy.get('[data-test-id="collection-select"]').type(
      `${collectionName} {enter}`,
    )

    cy.get('[data-test-id="next-step"]').click({ force: true })
    cy.contains('button', 'Yes, create book').click()
    cy.get('[data-test-id="saving-settings"]').click({ force: true })
  },
)

Cypress.Commands.add('addCollection', (title, publisher, location) => {
  cy.get('button[data-test-id="create-new-collection"]').click()
  cy.get('[data-test-id="collection-type"]').click()
  cy.contains('div', 'Book Series Collection').click()
  cy.get('.ProseMirror').type(title)
  cy.get('[name="publisherName"]').type(publisher)
  cy.get('[name="publisherLocation"]').type(location)
  cy.get('[data-test-id="chapterProcessedSourceType-select"]').scrollIntoView()

  cy.get('[data-test-id="pubdate-type-select"]').type(
    `Print publication range {enter}`,
  )

  cy.get('[color="#8a8383"]').click()
  cy.get('[data-test-id="wholeBookSourceType-select"]').type(`Book {enter}`)

  cy.get('[data-test-id="chapterProcessedSourceType-select"]').type(
    `Book {enter}`,
  )

  cy.contains('button', 'Next').click()
  // cy.contains("div",title);
  cy.get('[data-test-id="order-books-by"]').scrollIntoView()
  cy.get('[data-test-id="order-books-by"]').type(`Title {enter}`)
  cy.contains('button', 'Submit').click()
})

Cypress.Commands.add(
  'updateChapterProcessedMetadata',
  (word, orgName, bookTitle) => {
    cy.get(`input[data-test-id="uploadcover"]`).attachFile('img12.png')
    cy.contains('[data-test-id="cover-filename"]', 'img12.png')
    // cy.get('[data-test-id="subtitle"] div.ProseMirror').type('booksubtitle')

    cy.get('[data-test-id="alternative_title"] div.ProseMirror').type(
      'alternative title 1',
    )

    cy.get('input[name="issn"]').scrollIntoView()
    cy.get('input[name="edition"]').type('Edition')
    cy.get('input[name="volume"]').type('volume')
    cy.get('input[name="doi"]').type('doi')
    cy.get('input[name="isbn"]').type('isbn12')
    cy.get('input[name="issn"]').type('issn12')
    cy.get('input[name="publisherLocation"]').scrollIntoView()

    if (word) {
      cy.get('input[data-test-id="uploadabstractgraphic"]').attachFile(
        'img13.jpg',
      )

      cy.contains('[data-test-id="abstractgraphic-filename"]', 'img13.jpg')
    }

    cy.get('[data-test-id="abstract"] div.ProseMirror').type('abstract')
    cy.get('[name="publisherName"]').type('publisher name')
    cy.get('input[name="publisherLocation"]').type('pub location')

    cy.get('[data-test-id="publication-date-type-select"]').type(
      'Print publication {enter}',
    )

    cy.get(
      '[data-test-id="publication-date"] [data-test-id="setdate-icon"]:first',
    ).click()

    cy.get('[data-test-id="ncbi-custom-metadata"]').scrollIntoView()
    cy.get('[data-test-id="bookSourceType-select"]').type('Book {enter}')

    cy.get('[data-test-id="license_statement"]').scrollIntoView()

    cy.get('[data-test-id="copyright_statement"] div.ProseMirror').type(
      'all rights reserved',
    )

    cy.get('[data-test-id="license_statement"] div.ProseMirror').type(
      ' licenced',
    )

    cy.get('[data-test-id="add-collaborativeAuthors"]').click()

    cy.get('[name="collaborativeAuthors[0].givenName"]').type(
      'collaborative author1',
    )

    cy.get('[data-test-id="add-editors"]').click()
    cy.get('[name="editors[0].surname"]').type('Editor surname')
    cy.get('[name="editors[0].givenName"]').type('Editor givenName')
    cy.get('[name="editors[0].suffix"]').type('Editor suffix')
    cy.get('[name="editors[0].degrees"]').type('Editor degree')

    cy.get('[data-test-id="affiliation-select"]').type(
      'Editor affiliation {enter}',
    )

    cy.get('[name="editors[0].role"]').type('Editor role')
    cy.get('[name="editors[0].email"]').type('Editor email')

    cy.get('[data-test-id="add-authors"]').click()
    cy.get('[name="authors[0].surname"]').type('Author surname')
    cy.get('[name="authors[0].givenName"]').type('Author givenName')
    cy.get('[name="authors[0].suffix"]').type('Author suffix')
    cy.get('[name="authors[0].degrees"]').type('Author degree')

    cy.get('[data-test-id="affiliation-select"]:last').type(
      'Author affiliation {enter}',
    )

    cy.get('[name="authors[0].role"]').type('Author role')
    cy.get('[name="authors[0].email"]').type('Author email')

    cy.get('[data-test-id="add-notes"]').click()
    cy.get('[data-test-id="typeOfNote-select"]').click()
    cy.contains('Disclaimer').click()
    cy.get('[name="notes[0].title"]').type('notes title')

    cy.get('[data-test-id="description"] .ProseMirror').type(
      'notes description',
    )

    // cy.get('[data-test-id="add-grant"]').click()

    // cy.intercept({
    //   method: 'GET',
    //   url:
    //     'https://api.ncbi.nlm.nih.gov/granthub/collapsed/?award.number=108580',
    // }).as('waitForAwardId')

    // cy.get('[name="funding[0].number"]', { force: true }).type('108580', {
    //   force: true,
    // })
    // cy.wait('@waitForAwardId')
    // cy.get('#react-select-6-option-0').click({ multiple: true, force: true })

    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.wait('@gqlGetOrganisationQuery')
    cy.contains('div', 'Metadata were updated successfully')
  },
)

Cypress.Commands.add(
  'checkChapterProcessedMetadata',
  (word, orgName, bookTitle) => {
    cy.contains(orgName).click()
    cy.contains(bookTitle).click()
    cy.get('[data-test-id="bookMetadata"]').click()
    cy.contains('[data-test-id="cover-filename"]', 'img12.png')
    // cy.contains('[data-test-id="subtitle"] div.ProseMirror', 'booksubtitle')
    cy.contains('[data-test-id="alternative_title"]', 'alternative title 1')
    cy.get('input[name="issn"]').scrollIntoView()
    cy.get('input[name="edition"]').should('have.value', 'Edition')
    cy.get('input[name="volume"]').should('have.value', 'volume')
    cy.get('input[name="doi"]').should('have.value', 'doi')
    cy.get('input[name="isbn"]').should('have.value', 'isbn12')
    cy.get('input[name="issn"]').should('have.value', 'issn12')
    cy.get('input[name="publisherLocation"]').scrollIntoView()

    if (word) {
      cy.contains('[data-test-id="abstractgraphic-filename"]', 'img13.jpg')
    }

    cy.contains('[data-test-id="abstract"] div.ProseMirror', 'abstract')
    cy.get('[name="publisherName"]').should('have.value', 'publisher name')

    cy.get('input[name="publisherLocation"]').should(
      'have.value',
      'pub location',
    )

    cy.contains(
      '[data-test-id="publication-date-type-select"] div.css-1uccc91-singleValue',
      'Print publication',
    )

    cy.get('[data-test-id="ncbi-custom-metadata"]').scrollIntoView()

    cy.contains(
      '[data-test-id="bookSourceType-select"] div.css-1uccc91-singleValue',
      'Book',
    )

    cy.get(
      '[data-test-id="publication-date"] [data-test-id="setdate-icon"]:first',
    ).click()

    cy.get('[data-test-id="license_statement"]').scrollIntoView()

    cy.contains(
      '[data-test-id="copyright_statement"] div.ProseMirror',
      'all rights reserved',
    )

    cy.contains(
      '[data-test-id="license_statement"] div.ProseMirror',
      'licenced',
    )

    cy.get('[name="collaborativeAuthors[0].givenName"]').should(
      'have.value',
      'collaborative author1',
    )

    cy.get('[data-test-id="add-editors"]').click()
    cy.get('[name="editors[0].surname"]').should('have.value', 'Editor surname')

    cy.get('[name="editors[0].givenName"]').should(
      'have.value',
      'Editor givenName',
    )

    cy.get('[name="editors[0].suffix"]').should('have.value', 'Editor suffix')
    cy.get('[name="editors[0].degrees"]').should('have.value', 'Editor degree')

    cy.contains(
      '[data-test-id="affiliation-select"]:first',
      'Editor affiliation',
    )

    cy.get('[name="editors[0].role"]').should('have.value', 'Editor role')
    cy.get('[name="editors[0].email"]').should('have.value', 'Editor email')

    cy.get('[name="authors[0].surname"]').should('have.value', 'Author surname')

    cy.get('[name="authors[0].givenName"]').should(
      'have.value',
      'Author givenName',
    )

    cy.get('[name="authors[0].suffix"]').should('have.value', 'Author suffix')
    cy.get('[name="authors[0].degrees"]').should('have.value', 'Author degree')

    cy.contains(
      '[data-test-id="affiliation-select"]:last',
      'Author affiliation',
    )

    cy.get('[name="authors[0].role"]').should('have.value', 'Author role')
    cy.get('[name="authors[0].email"]').should('have.value', 'Author email')

    // cy.get('[data-test-id="typeOfNote-select"]').click()
    cy.contains('[data-test-id="typeOfNote-select"]', 'Disclaimer')
    cy.get('[name="notes[0].title"]').should('have.value', 'notes title')

    cy.contains(
      '[data-test-id="description"] .ProseMirror',
      'notes description',
    )
  },
)

Cypress.Commands.add(
  'updateChapterProcessedSettings',
  (word, orgName, bookTitle) => {
    cy.contains(orgName).click()
    cy.contains(bookTitle).click()
    cy.get('[data-test-id="bookSettings"]').click()
    cy.get('[data-test-id="open-access-status-lock"]').click({ force: true })
    cy.get('[name="openAccessStatus"]').click({ force: true })
    cy.get('[data-test-id="ukpmc-lock"]').click({ force: true })
    cy.get('[data-test-id="ukpmc-lock"]').scrollIntoView({ force: true })
    cy.get('[name="UKPMC"]').click({ force: true })

    cy.get('[data-test-id="support-multiple-published-versions-lock"]').click({
      force: true,
    })

    cy.get('[name="supportMultiplePublishedVersions"]').click({ force: true })
    cy.get('[data-test-id="index-book-in-pubmed-lock"]').click({ force: true })
    cy.get('[name="indexInPubmed"]').click({ force: true })

    cy.get('[data-test-id="index-chapters-in-pubmed-lock"]').click({
      force: true,
    })

    cy.get('[name="indexChaptersInPubmed"]').click({ force: true })
    cy.get('[name="publisherUrl"]').type('https://google.com')
    cy.get('[name="citationSelfUrl"]').type('https://yahoo.com')
    cy.get('[name="specialPublisherLinkUrl"]').type('https://reddit.com')
    cy.get('[name="specialPublisherLinkText"]').type('special link text')
    cy.get('[name="bookLevelLinks"]').click({ force: true })
    cy.get('[name="bookLevelLinksMarkdown"]').type('https://amazon.com')

    if (word) {
      cy.get('[data-test-id="create-book-level-pdf-for-download-lock"]').click({
        force: true,
      })

      cy.get('[name="createBookLevelPdf"]').click({ force: true })

      cy.get(
        '[data-test-id="create-chapter-level-pdf-for-download-lock"]',
      ).click({ force: true })
    }

    cy.get('[data-test-id="group-chapters-into-parts-lock"]').click()
    cy.get('[name="groupChaptersInParts"]').click({ force: true })
    cy.get('[name="tocExpansionLevel"]:first').scrollIntoView()
    cy.get('[data-test-id="order-chapters-select"]').type(`A > z {enter}`)
    cy.get('[data-test-id="displayHeadingLevel"]').type(`2 {enter}`)
    cy.get('[data-test-id="tocExpansionLevel"]').type(`2 {enter}`)
    cy.get('[data-test-id="tocExpansionLevel"]').scrollIntoView()
    cy.get('[name="tocContributors"]').click({ force: true })
    cy.get('[name="tocChapterLevelDates"]').click({ force: true })
    cy.get('[name="tocSubtitle"]').click({ force: true })
    cy.get('[name="tocAltTitle"]').click({ force: true })
    cy.get('[name="footnotesDecimal"]').click({ force: true })
    cy.get('[name="footnotesDecimal"]').scrollIntoView()
    cy.get('[data-test-id="question-answer-style-select"]').type(`FAQ {enter}`)

    cy.get('[data-test-id="display-objects-location-select"]').type(
      `In place {enter}`,
    )

    cy.get('[data-test-id="chapter-level-affiliation-select"]').type(
      `By contrib {enter}`,
    )

    cy.get('[data-test-id="book-level-affiliation-select"]').type(
      `By contrib {enter}`,
    )

    if (word) {
      cy.get('[data-test-id="citiation-select"]').type(
        `Numbered square {enter}`,
      )
    }

    cy.get('[data-test-id="book-level-affiliation-select"]').scrollIntoView()
    cy.get('[data-test-id="reference-list-select"]').type(`Circle {enter}`)
    cy.get('[data-test-id="xref-anchor-select"]').type(`Superscripted {enter}`)
    cy.get('[data-test-id="save-book-settings"]').click()
    cy.wait('@gqlgetBookQuery')
    cy.wait('@gqlGetOrganisationQuery')
    cy.get('[data-test-id="modalBody"]').should('not.exist')
  },
)

Cypress.Commands.add(
  'checkChapterProcessedSettings',
  (word, orgName, bookTitle) => {
    cy.contains(orgName).click()
    cy.contains(bookTitle).click()
    cy.get('[data-test-id="bookSettings"]').click()
    cy.get('[name="openAccessStatus"]').should('have.value', 'true')
    cy.get('[data-test-id="ukpmc-lock"]').scrollIntoView()
    cy.get('[name="UKPMC"]').should('have.value', 'true')

    cy.get('[name="supportMultiplePublishedVersions"]').should(
      'have.value',
      'true',
    )

    cy.get('[name="indexInPubmed"]').should('have.value', 'true')
    cy.get('[name="indexChaptersInPubmed"]').should('have.value', 'true')

    cy.get('[name="publisherUrl"]').should('have.value', 'https://google.com')

    cy.get('[name="citationSelfUrl"]').should('have.value', 'https://yahoo.com')

    cy.get('[name="specialPublisherLinkUrl"]').should(
      'have.value',
      'https://reddit.com',
    )

    cy.get('[name="specialPublisherLinkText"]').should(
      'have.value',
      'special link text',
    )

    cy.get('[name="bookLevelLinks"]').should('have.value', 'true')

    cy.get('[name="bookLevelLinksMarkdown"]').should(
      'have.value',
      'https://amazon.com',
    )

    if (word) {
      cy.get('[name="createBookLevelPdf"]').should('have.value', 'true')
    }

    cy.get('[name="groupChaptersInParts"]').should('have.value', 'true')
    cy.get('[name="tocExpansionLevel"]:first').scrollIntoView()

    cy.contains(
      '[data-test-id="order-chapters-select"] div.css-1uccc91-singleValue',
      `A > Z`,
    )

    cy.contains('[data-test-id="displayHeadingLevel"]', `2`)
    cy.contains('[data-test-id="tocExpansionLevel"]', `2`)
    cy.get('[name="tocExpansionLevel"]:first').scrollIntoView()
    cy.get('[name="tocContributors"]').should('have.value', 'false')
    cy.get('[name="tocChapterLevelDates"]').should('have.value', 'false')
    cy.get('[name="tocSubtitle"]').should('have.value', 'false')
    cy.get('[name="tocAltTitle"]').should('have.value', 'true')
    cy.get('[name="footnotesDecimal"]').should('have.value', 'true')
    cy.get('[name="footnotesDecimal"]').scrollIntoView()

    cy.contains(
      '[data-test-id="question-answer-style-select"] div.css-1uccc91-singleValue',
      `FAQ`,
    )

    cy.contains(
      '[data-test-id="display-objects-location-select"] div.css-1uccc91-singleValue',
      `In place`,
    )

    cy.contains(
      '[data-test-id="chapter-level-affiliation-select"] div.css-1uccc91-singleValue',
      `By contrib`,
    )

    cy.contains(
      '[data-test-id="book-level-affiliation-select"] div.css-1uccc91-singleValue',
      `By contrib`,
    )

    if (word) {
      cy.contains(
        '[data-test-id="citiation-select"] div.css-1uccc91-singleValue',
        `Numbered square`,
      )
    }

    cy.get(
      '[data-test-id="book-level-affiliation-select"] div.css-1uccc91-singleValue',
    ).scrollIntoView()

    cy.contains(
      '[data-test-id="reference-list-select"] div.css-1uccc91-singleValue',
      `Circle`,
    )

    cy.contains(
      '[data-test-id="xref-anchor-select"] div.css-1uccc91-singleValue',
      `Superscripted`,
    )
  },
)

Cypress.Commands.add(
  'updateCompleteBookSettings',
  (word, orgName, bookTitle) => {
    cy.contains(orgName).click()
    cy.contains(bookTitle).click()
    cy.get('[data-test-id="settings-tab"]').click()
    cy.get('[data-test-id="open-access-status-lock"]').click({ force: true })
    cy.get('[name="openAccessStatus"]').click({ force: true })
    cy.get('[data-test-id="ukpmc-lock"]').click({ force: true })
    cy.get('[data-test-id="ukpmc-lock"]').scrollIntoView({ force: true })
    cy.get('[name="UKPMC"]').click({ force: true })
    cy.get('[data-test-id="index-book-in-pubmed-lock"]').click({ force: true })
    cy.get('[name="indexInPubmed"]').click({ force: true })

    cy.get('[data-test-id="index-chapters-in-pubmed-lock"]').click({
      force: true,
    })

    cy.get('[name="indexChaptersInPubmed"]').click({ force: true })
    cy.get('[name="publisherUrl"]').type('https://google.com')
    cy.get('[name="citationSelfUrl"]').type('https://yahoo.com')
    cy.get('[name="specialPublisherLinkUrl"]').type('https://reddit.com')
    cy.get('[name="specialPublisherLinkText"]').type('special link text')
    cy.get('[name="bookLevelLinks"]').click({ force: true })
    cy.get('[name="bookLevelLinksMarkdown"]').type('https://amazon.com')

    if (word) {
      cy.get('[data-test-id="create-book-level-pdf-for-download-lock"]').click({
        force: true,
      })

      cy.get('[name="createBookLevelPdf"]').click({ force: true })

      cy.get(
        '[data-test-id="create-chapter-level-pdf-for-download-lock"]',
      ).click({ force: true })
    }

    cy.get('[name="tocExpansionLevel"]:first').scrollIntoView()
    cy.get('[data-test-id="displayHeadingLevel"]').type(`2 {enter}`)
    cy.get('[data-test-id="tocExpansionLevel"]').type(`2 {enter}`)
    cy.get('[name="tocExpansionLevel"]:first').scrollIntoView()
    cy.get('[name="tocContributors"]').click({ force: true })
    cy.get('[name="tocChapterLevelDates"]').click({ force: true })
    cy.get('[name="tocSubtitle"]').click({ force: true })
    cy.get('[name="tocAltTitle"]').click({ force: true })
    cy.get('[name="footnotesDecimal"]').click({ force: true })
    cy.get('[name="footnotesDecimal"]').scrollIntoView()
    cy.get('[data-test-id="question-answer-style-select"]').type(`FAQ {enter}`)

    cy.get('[data-test-id="display-objects-location-select"]').type(
      `In place {enter}`,
    )

    cy.get('[data-test-id="chapter-level-affiliation-select"]').type(
      `By contrib {enter}`,
    )

    cy.get('[data-test-id="book-level-affiliation-select"]').type(
      `By contrib {enter}`,
    )

    if (word) {
      cy.get('[data-test-id="citiation-select"]').type(
        `Numbered square {enter}`,
      )
    }

    cy.get('[data-test-id="book-level-affiliation-select"]').scrollIntoView()
    cy.get('[data-test-id="reference-list-select"]').type(`Circle {enter}`)
    cy.get('[data-test-id="xref-anchor-select"]').type(`Superscripted {enter}`)
    cy.get('[name="createVersionLink"]').click({ force: true })
    cy.get('[name="versionLinkText"]').type('version link text')
    cy.get('[name="versionLinkUri"]').type('versionpage.in')
    cy.get('[data-test-id="save-book-settings"]').click()
  },
)

Cypress.Commands.add(
  'checkCompleteBookSettings',
  (word, orgName, bookTitle) => {
    cy.contains(orgName).click()
    cy.contains(bookTitle).click()
    cy.get('[data-test-id="settings-tab"]').click()
    cy.get('[name="openAccessStatus"]').should('have.value', 'true')
    cy.get('[data-test-id="ukpmc-lock"]').scrollIntoView()
    cy.get('[name="UKPMC"]').should('have.value', 'true')

    cy.get('[name="indexInPubmed"]').should('have.value', 'true')
    cy.get('[name="indexChaptersInPubmed"]').should('have.value', 'true')

    cy.get('[name="publisherUrl"]').should('have.value', 'https://google.com')

    cy.get('[name="citationSelfUrl"]').should('have.value', 'https://yahoo.com')

    cy.get('[name="specialPublisherLinkUrl"]').should(
      'have.value',
      'https://reddit.com',
    )

    cy.get('[name="specialPublisherLinkText"]').should(
      'have.value',
      'special link text',
    )

    cy.get('[name="bookLevelLinks"]').should('have.value', 'true')

    cy.get('[name="bookLevelLinksMarkdown"]').should(
      'have.value',
      'https://amazon.com',
    )

    if (word) {
      cy.get('[name="createBookLevelPdf"]').should('have.value', 'true')
    }

    cy.get('[name="tocExpansionLevel"]:first').scrollIntoView()

    cy.contains('[data-test-id="displayHeadingLevel"]', `2`)
    cy.contains('[data-test-id="tocExpansionLevel"]', `2`)
    cy.get('[data-test-id="tocExpansionLevel"]').scrollIntoView()
    cy.get('[name="tocContributors"]').should('have.value', 'false')
    cy.get('[name="tocChapterLevelDates"]').should('have.value', 'false')
    cy.get('[name="tocSubtitle"]').should('have.value', 'false')
    cy.get('[name="tocAltTitle"]').should('have.value', 'true')
    cy.get('[name="footnotesDecimal"]').should('have.value', 'true')
    cy.get('[name="footnotesDecimal"]').scrollIntoView()

    cy.contains(
      '[data-test-id="question-answer-style-select"] div.css-1uccc91-singleValue',
      `FAQ`,
    )

    cy.contains(
      '[data-test-id="display-objects-location-select"] div.css-1uccc91-singleValue',
      `In place`,
    )

    cy.contains(
      '[data-test-id="chapter-level-affiliation-select"] div.css-1uccc91-singleValue',
      `By contrib`,
    )

    cy.contains(
      '[data-test-id="book-level-affiliation-select"] div.css-1uccc91-singleValue',
      `By contrib`,
    )

    if (word) {
      cy.contains(
        '[data-test-id="citiation-select"] div.css-1uccc91-singleValue',
        `Numbered square`,
      )
    }

    cy.get(
      '[data-test-id="book-level-affiliation-select"] div.css-1uccc91-singleValue',
    ).scrollIntoView()

    cy.contains(
      '[data-test-id="reference-list-select"] div.css-1uccc91-singleValue',
      `Circle`,
    )

    cy.contains(
      '[data-test-id="xref-anchor-select"] div.css-1uccc91-singleValue',
      `Superscripted`,
    )

    cy.get('[name="versionLinkText"]').should('have.value', 'version link text')

    cy.get('[name="versionLinkUri"]').should('have.value', 'versionpage.in')
  },
)

Cypress.Commands.add(
  'checkFileSection',
  (sectionName, fileName, selectOption = 2) => {
    let dataId = sectionName.toLowerCase()
    let versionCol = 3

    if (sectionName === 'Bookshelf Display PDFs') {
      dataId = 'displayPDFs'
    } else if (sectionName === 'Images') {
      versionCol = 4
    }

    cy.contains(sectionName).click()

    // cy.get(`[data-test-id="upload-${dataId}-input"]`).attachFile(fileName)
    // cy.get(`[data-test-id="save-btn"]`).click()
    // cy.wait('@waitForUpload')
    // cy.contains(`[aria-rowindex="1"] [aria-colindex="${versionCol}"]`, 'V1.1')

    /* eslint-disable-next-line no-plusplus */
    for (let i = 1; i < 3; i++) {
      cy.get(`[data-test-id="upload-${dataId}-input"]`).attachFile(fileName)
      cy.get(`[data-test-id="save-btn"]`).click()

      cy.contains(
        `[aria-rowindex="1"] [aria-colindex="${versionCol}"]`,
        `V1.${i}`,
      )
    }

    cy.contains('button', 'Show all versions').click()
    cy.contains(`[aria-rowindex="1"] [aria-colindex="${versionCol}"]`, 'V1.2')
    cy.contains(`[aria-rowindex="2"] [aria-colindex="${versionCol}"]`, 'V1.1')
    cy.get('[data-test-id="book-comp-delete"]').click()
    cy.contains('Current Version (V2)').click()
    cy.get(`[id=react-select-${selectOption}-option-1]`).click()
    // #react-select-3-option-1
    cy.get('[data-test-id="confirm"]').click()
    cy.contains('No files available')
    cy.contains(sectionName).click()
  },
)

// eslint-disable-next-line handle-callback-err
Cypress.on('uncaught:exception', (err, runnable, promise) => {
  if (promise) {
    return false
  }
})

const resizeObserverLoopErrRe = /^[^(ResizeObserver loop limit exceeded)]/

Cypress.on('uncaught:exception', err => {
  /* returning false here prevents Cypress from failing the test */
  if (resizeObserverLoopErrRe.test(err.message)) {
    return false
  }
})
