const pipeline = true

const randomNumber = pipeline
  ? ''
  : Math.floor(Math.random() * (1000 - 100 + 1)) + 100

const credentials = {
  admin: {
    username: 'admin',
    password: 'password',
  },
  user: {
    username: 'user',
    password: 'User123456',
    email: 'user@test.com',
    givenName: 'Peter',
    surname: 'Paul',
  },
  generalUser: {
    username: `generaluser${randomNumber}`,
    password: 'User123456',
    email: `generaluser${randomNumber}@test.com`,
    givenName: `user${randomNumber}`,
    surname: `surname${randomNumber}`,
  },
  seededUser: {
    username: `seedUser${randomNumber}`,
    password: 'Password@123',
    email: `seedUser${randomNumber}@test.com`,
    givenName: `seedUser`,
    surname: `seed`,
  },
  testUserBookTeam: {
    username: 'generaluser123',
    password: 'User123456',
    email: 'generaluser123@test.com',
  },
  author: {
    username: 'author',
    password: 'password',
  },
  productionEditor: {
    username: 'productionEditor',
    password: 'password',
  },
  globalProductionEditor: {
    username: 'globalProductionEditor',
    password: 'password',
  },
  copyEditor: {
    username: 'copyEditor',
    password: 'password',
  },
  organisation: {
    name: `TestOrganisation${randomNumber}`,
    email: 'cypress@testorg.com',
    abbreviatedName: `TestOrganisation${randomNumber}`,
  },
  organisation2: {
    name: `another one ${randomNumber}`,
    email: 'another@testorg.com',
    abbreviatedName: `another-Org${randomNumber}`,
  },
  book: {
    title: `Cypress Test Book Title ${randomNumber}`,
    organisation: `CypressTestOrganisation${randomNumber}`,
  },
  completeBook: {
    title: `CompleteBook${randomNumber}`,
    organisation: `CypressTestOrganisation${randomNumber}`,
  },
  chapterProcessedBook: {
    title: `chapterProcessedBook${randomNumber}`,
    organisation: `CypressTestOrganisation${randomNumber}`,
  },
  collection: {
    title: `testCollection${randomNumber}`,
    publisher: `name${randomNumber}`,
    location: `Place${randomNumber}`,
  },
  workflows: [
    { name: 'PDF', selector: 'name="settings.pdf"' },
    { name: 'Word', selector: 'name="settings.word"' },
    { name: 'XML', selector: 'name="settings.xml"' },
  ],
}

module.exports = credentials
