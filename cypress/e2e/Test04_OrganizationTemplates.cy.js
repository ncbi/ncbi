/* eslint-disable jest/no-commented-out-tests */
// /* eslint-disable cypress/no-unnecessary-waiting */

// const { admin, organisation, workflows } = require('../support/credentials')

// describe('No templates  ', () => {
//   beforeEach(() => {
//     cy.login(admin)
//     cy.get('[data-test-id="organizations-tab"]').click()
//   })

//   it('Create a new organisation ', () => {
//     cy.addOrganisation(
//       organisation,
//       workflows,
//       'name="settings.type.publisher"',
//       'name="settings.collections.bookSeries"',
//     )
//   })

//   it('Creating a new chapter-processed book', () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a chapter-processed book
//     cy.get('button[data-test-id="create-new-book"]').click()

//     cy.get('input[name="title"]').type(
//       'Test for chapter-processed default settings book',
//     )

//     cy.contains('span', 'Word').click({ force: true })
//     cy.contains('span', 'Individual chapters').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')

//     cy.get('input[name="orderChaptersBy"]').should('have.value', 'manual')
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '4')
//     cy.get('input[name="tocContributors"]').should('have.value', 'true')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'true')

//     cy.get('input[name="citationType"]').should(
//       'have.value',
//       'numberedParentheses',
//     )

//     cy.get('[data-test-id="save-settings"]').click({ force: true })
//     cy.wait('@waitForSave')
//   })

//   it('Checking that a Chapter-Processed book has the default settings when there are no templates yet', () => {
//     // Check if the settings of the book match with the settings of the default template
//     cy.checkBookSettings(
//       'word',
//       organisation.name,
//       'Test for chapter-processed default settings book', // bookTitle
//       'individualChapters', // submissionType
//       'false', // openAccessStatus
//       'false', // UKPMC
//       'false', // supportMultiplePublishedVersions
//       'false', // indexInPubmed
//       'false', // indexChaptersInPubmed
//       '', // publisherUrl,
//       '', // citationSelfUrl,
//       '', // specialPublisherLinkUrl,
//       '', // specialPublisherLinkText,
//       'false', // bookLevelLinks
//       // '', // bookLevelLinksMarkdown
//       'false', // createBookLevelPdf
//       'false', // displayBookLevelPdf
//       'true', // createChapterLevelPdf
//       'true', // displayChapterLevelPdf
//       'false', // groupChaptersInParts
//       'manual', // orderChapters
//       '4', // displayHeadingLevel
//       '4', // tocExpansionLevel
//       'true', // tocContributors
//       'true', // tocChapterLevelDates
//       'true', // tocSubtitle
//       'false', // tocAltTitle
//       'false', // footnotesDecimal
//       'normal', // QAStyle
//       'at-xref', // displayObjectsLocation
//       'at-end', // chapterLevelAffiliation
//       'at-end', // bookLevelAffiliation
//       'numberedParentheses', // citation
//       'decimal', // referenceList
//       'default', // xrefAnchor
//       'false', // createVersionLink
//     )
//   })

//   it('Create a new WHOLEBOOK', () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a complete book
//     cy.get('button[data-test-id="create-new-book"]').click()

//     cy.get('input[name="title"]').type(
//       'Test for Default Template Wholebook Test',
//     )

//     cy.contains('span', 'Word').click({ force: true })
//     cy.contains('span', 'Complete books and documents').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')
//   })

//   it('Check that a new COMPLETED book has the default template settings for WHOLEBOOK', () => {
//     // Check if the settings of the book match with the settings of the default template
//     cy.checkBookSettings(
//       'word',
//       organisation.name,
//       'Test for Default Template Wholebook Test', // bookTitle
//       'wholeBook', // submissionType
//       'false', // openAccessStatus
//       'false', // UKPMC
//       'false', // supportMultiplePublishedVersions
//       'false', // indexInPubmed
//       'false', // indexChaptersInPubmed
//       '', // publisherUrl,
//       '', // citationSelfUrl,
//       '', // specialPublisherLinkUrl,
//       '', // specialPublisherLinkText,
//       'false', // bookLevelLinks
//       // '', // bookLevelLinksMarkdown
//       'false', // createBookLevelPdf
//       'true', // displayBookLevelPdf
//       'true', // createChapterLevelPdf
//       'true', // displayChapterLevelPdf
//       'false', // groupChaptersInParts
//       'manual', // orderChapters
//       '4', // displayHeadingLevel
//       '4', // tocExpansionLevel
//       'true', // tocContributors
//       'true', // tocChapterLevelDates
//       'true', // tocSubtitle
//       'false', // tocAltTitle
//       'false', // footnotesDecimal
//       'normal', // QAStyle
//       'at-xref', // displayObjectsLocation
//       'at-end', // chapterLevelAffiliation
//       'at-end', // bookLevelAffiliation
//       'numberedParentheses', // citation
//       'decimal', // referenceList
//       'default', // xrefAnchor
//       'false', // createVersionLink
//     )
//   })
// })

// describe('Check if chapter processed template applies to new chapter processed books', () => {
//   beforeEach(() => {
//     cy.login(admin)
//     cy.get('[data-test-id="organizations-tab"]').click()
//     cy.contains(organisation.name).click()
//   })

//   it('Create template for CHAPTER PROCESSED book', () => {
//     // Go to Book Templates of the Organization
//     cy.contains('Book Templates').click()

//     // Create template for Chapter-processed book
//     cy.contains('Choose a type of template').click()
//     cy.get('#react-select-2-option-0 ').click()

//     // Publishing Settings
//     cy.get('[data-test-id="support-multiple-published-versions-lock"]').click({
//       force: true,
//     })

//     cy.get('input[name="supportMultiplePublishedVersions"]').click({
//       force: true,
//     })

//     cy.get('[data-test-id="support-multiple-published-versions-lock"]').click({
//       force: true,
//     })

//     cy.get('[data-test-id="index-book-in-pubmed-lock"]').click({
//       force: true,
//     })

//     cy.get('input[name="indexInPubmed"]').click({ force: true })

//     cy.get('[data-test-id="index-book-in-pubmed-lock"]').click({
//       force: true,
//     })

//     cy.get('[data-test-id="index-chapters-in-pubmed-lock"]').click({
//       force: true,
//     })

//     cy.get('input[name="indexChaptersInPubmed"]').click({ force: true })

//     cy.get('[data-test-id="index-chapters-in-pubmed-lock"]').click({
//       force: true,
//     })

//     // Landing Page: Links
//     cy.get('input[name="publisherUrl"]').type('http://www.publisherurl.com')

//     cy.get('input[name="citationSelfUrl"]').type(
//       'http://www.citationselfurl.com',
//     )

//     cy.get('input[name="specialPublisherLinkUrl"]').type(
//       'http://www.specialpublisherurl.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').type(
//       'Cypress Test Special Publisher Link Text',
//     )

//     // Landing Page: Table of Contents
//     cy.get('[data-test-id="group-chapters-into-parts-lock"]').click({
//       force: true,
//     })

//     cy.get('input[name="groupChaptersInParts"]').click({
//       force: true,
//     })

//     cy.get('[data-test-id="group-chapters-into-parts-lock"]').click({
//       force: true,
//     })

//     // Display Metadata on TOC
//     cy.get('[data-test-id="order-chapters-select"]').type('A > Z{enter}')
//     cy.get('div[max="4"] > input').clear().type('3')
//     cy.get('div[max="5"] > input').clear().type('3')

//     cy.get('input[name="tocContributors"]').click({ force: true })
//     cy.get('input[name="tocChapterLevelDates"]').click({ force: true })
//     cy.get('input[name="tocSubtitle"]').click({ force: true })
//     cy.get('input[name="tocAltTitle"]').click({ force: true })

//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').click({ force: true })

//     cy.get('[data-test-id="question-answer-style-select"]')
//       .click({ force: true })
//       .type('FAQ{enter}')

//     cy.get('[data-test-id="display-objects-location-select"]')
//       .click({ force: true })
//       .type('In place{enter}')

//     cy.get('[data-test-id="chapter-level-affiliation-select"]')
//       .click({ force: true })
//       .type('By contrib{enter}')

//     cy.get('[data-test-id="book-level-affiliation-select"]')
//       .click({ force: true })
//       .type('By contrib{enter}')

//     cy.get('[data-test-id="citiation-select"]')
//       .click({ force: true })
//       .type('Harvard{enter}')

//     cy.get('[data-test-id="reference-list-select"]')
//       .click({ force: true })
//       .type('None{enter}')

//     cy.get('[data-test-id="xref-anchor-select"]')
//       .click({ force: true })
//       .type('Superscripted{enter}')

//     // Save changes
//     cy.get('button[type="submit"]').click()

//     cy.contains('Template successfully saved', {
//       timeout: 10000,
//     })
//   })

//   it('Creating a new CHAPTER PROCESSED book', () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a chapter-processed book
//     cy.get('button[data-test-id="create-new-book"]').click()

//     cy.get('input[name="title"]').type(
//       'Test for Chapter-Processed Template Book',
//     )

//     cy.contains('span', 'Word').click({ force: true })
//     cy.contains('span', 'Individual chapters').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')

//     cy.get('input[name="orderChaptersBy"]').should('have.value', 'title')
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '3')
//     cy.get('input[name="tocContributors"]').should('have.value', 'false')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'false')
//     cy.get('input[name="citationType"]').should('have.value', 'harvard')

//     cy.get('[data-test-id="save-settings"]').click({ force: true })
//     cy.wait('@waitForSave')
//   })

//   it('Checking that settings of a Chapter-Processed book match with the template of the organisation', () => {
//     // Check if the settings of the book match with the settings of the default template
//     cy.checkBookSettings(
//       'word',
//       organisation.name,
//       'Test for Chapter-Processed Template Book', // bookTitle
//       'individualChapters', // submissionType
//       'false', // openAccessStatus
//       'false', // UKPMC
//       'true', // supportMultiplePublishedVersions
//       'true', // indexInPubmed
//       'true', // indexChaptersInPubmed
//       'http://www.publisherurl.com', // publisherUrl,
//       'http://www.citationselfurl.com', // citationSelfUrl,
//       'http://www.specialpublisherurl.com', // specialPublisherLinkUrl,
//       'Cypress Test Special Publisher Link Text', // specialPublisherLinkText,
//       'false', // bookLevelLinks
//       // '', // bookLevelLinksMarkdown
//       'false', // createBookLevelPdf
//       'false', // displayBookLevelPdf
//       'true', // createChapterLevelPdf
//       'true', // displayChapterLevelPdf
//       'true', // groupChaptersInParts
//       'title', // orderChapters
//       '3', // displayHeadingLevel
//       '3', // tocExpansionLevel
//       'false', // tocContributors
//       'false', // tocChapterLevelDates
//       'false', // tocSubtitle
//       'true', // tocAltTitle
//       'true', // footnotesDecimal
//       'faq', // QAStyle
//       'in-place', // displayObjectsLocation
//       'by-contrib', // chapterLevelAffiliation
//       'by-contrib', // bookLevelAffiliation
//       'harvard', // citation
//       'none', // referenceList
//       'superscripted', // xrefAnchor
//       '', // versionLink
//     )
//   })
// })

// describe('Check if wholebook template applies to new wholebooks', () => {
//   beforeEach(() => {
//     cy.login(admin)
//     cy.get('[data-test-id="organizations-tab"]').click()
//     cy.contains(organisation.name).click()
//   })

//   it('Create template for Wholebook', () => {
//     // Go to Book Templates of the Organization
//     cy.get("div[data-test-id='book-templates-tab']").click()

//     // Create template for completed book
//     cy.get('.select__value-container').click().type('Whole book{Enter}')

//     // Publishing Settings
//     cy.get('[data-test-id="index-book-in-pubmed-lock"]').click({
//       force: true,
//     })

//     cy.get('[data-test-id="index-chapters-in-pubmed-lock"]').click({
//       force: true,
//     })

//     cy.get('input[name="indexInPubmed"]').click({ force: true })
//     cy.get('input[name="indexChaptersInPubmed"]').click({ force: true })

//     cy.get('[data-test-id="index-book-in-pubmed-lock"]').click({
//       force: true,
//     })

//     cy.get('[data-test-id="index-chapters-in-pubmed-lock"]').click({
//       force: true,
//     })

//     // Landing Page: Links
//     cy.get('input[name="publisherUrl"]').type(
//       'http://www.testOrgTemplatePub.com',
//     )

//     cy.get('input[name="citationSelfUrl"]').type(
//       'http://www.testOrgTemplateCit.com',
//     )

//     cy.get('input[name="specialPublisherLinkUrl"]').type(
//       'http://www.testOrgTemplateSpPub.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').type(
//       'Test Org Template Special Pub',
//     )

//     // Landing Page: Table of Contents
//     cy.get('div[max="4"] > input').clear().type('2')
//     cy.get('div[max="5"] > input').clear().type('4')

//     // Display metadata on TOC
//     cy.get('input[name="tocContributors"]').click({ force: true })
//     cy.get('input[name="tocChapterLevelDates"]').click({ force: true })
//     cy.get('input[name="tocSubtitle"]').click({ force: true })
//     cy.get('input[name="tocAltTitle"]').click({ force: true })

//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').click({ force: true })

//     cy.get('[data-test-id="question-answer-style-select"]')
//       .click({ force: true })
//       .type('FAQ with TOC{enter}')

//     cy.get('[data-test-id="display-objects-location-select"]')
//       .click({ force: true })
//       .type('In place{enter}')

//     // Display: Metadata
//     cy.get('[data-test-id="chapter-level-affiliation-select"]')
//       .click({ force: true })
//       .type('By contrib{enter}')

//     cy.get('[data-test-id="book-level-affiliation-select"]')
//       .click({ force: true })
//       .type('By contrib{enter}')

//     // Display: Reference Citations
//     cy.get('[data-test-id="reference-list-select"]')
//       .click({ force: true })
//       .type('Disc{enter}')

//     cy.get('[data-test-id="xref-anchor-select"]')
//       .click({ force: true })
//       .type('Blue triangle{enter}')

//     // Save changes for the Wholebook Template
//     cy.get('button[type="submit"]').click()

//     cy.contains('Template successfully saved', {
//       timeout: 10000,
//     })
//   })

//   it('Creating a wholebook', () => {
//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a complete book

//     cy.get('button[data-test-id="create-new-book"]').click()
//     cy.get('input[name="title"]').type('Test for ORG wholebook template')

//     cy.contains('span', 'Word').click({ force: true })
//     cy.contains('span', 'Complete books and documents').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')

//     /*
//         cy.get('input[name="requireApprovalByOrgAdminOrEditor"]').should(
//           'have.value',
//           'true',
//         )
//     */
//     //  cy.get('input[name="displayHeadingLevel"]').should('have.value', '2')
//     //  cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'false')
//     cy.get('input[name="citationType"]').should(
//       'have.value',
//       'numberedParentheses',
//     )

//     cy.get('[data-test-id="save-settings"]').click({ force: true })
//     cy.wait('@waitForSave')
//   })

//   it('Checking that a new Complete book out of the collection members matches with settings of the ORG templates', () => {
//     // Check if the settings of the book match with the settings of the default template
//     cy.checkBookSettings(
//       'word',
//       organisation.name,
//       'Test for ORG wholebook template', // bookTitle
//       'wholebook', // submissionType
//       'false', // openAccessStatus
//       'false', // UKPMC
//       'false', // supportMultiplePublishedVersions
//       'true', // indexInPubmed
//       'true', // indexChaptersInPubmed
//       'http://www.testOrgTemplatePub.com', // publisherUrl,
//       'http://www.testOrgTemplateCit.com', // citationSelfUrl,
//       'http://www.testOrgTemplateSpPub.com', // specialPublisherLinkUrl,
//       'Test Org Template Special Pub', // specialPublisherLinkText,
//       'false', // bookLevelLinks
//       // '', // bookLevelLinksMarkdown
//       'false', // createBookLevelPdf
//       'true', // displayBookLevelPdf
//       '', // createChapterLevelPdf
//       '', // displayChapterLevelPdf
//       '', // groupChaptersInParts
//       '', // orderChapters
//       '2', // displayHeadingLevel
//       '4', // tocExpansionLevel
//       'false', // tocContributors
//       'false', // tocChapterLevelDates
//       'false', // tocSubtitle
//       'true', // tocAltTitle
//       'true', // footnotesDecimal
//       'faq-with-toc', // QAStyle
//       'in-place', // displayObjectsLocation
//       'by-contrib', // chapterLevelAffiliation
//       'by-contrib', // bookLevelAffiliation
//       'numberedParentheses', // citation
//       'disc', // referenceList
//       'blue-triangle', // xrefAnchor
//       'false', // versionLink
//     )
//   })
// })

// Cypress.Commands.add(
//   'checkBookSettings',
//   (
//     word,
//     orgName,
//     bookTitle,
//     // eslint-disable-next-line no-shadow
//     submissionType,
//     openAccessStatus,
//     UKPMC,
//     supportMultiplePublishedVersions,
//     indexInPubmed,
//     indexChaptersInPubmed,
//     publisherUrl,
//     citationSelfUrl,
//     specialPublisherLinkUrl,
//     specialPublisherLinkText,
//     bookLevelLinks,
//     // bookLevelLinksMarkdown,
//     createBookLevelPdf,
//     displayBookLevelPdf,
//     createChapterLevelPdf,
//     displayChapterLevelPdf,
//     groupChaptersInParts,
//     orderChapters,
//     displayHeadingLevel,
//     tocExpansionLevel,
//     tocContributors,
//     tocChapterLevelDates,
//     tocSubtitle,
//     tocAltTitle,
//     footnotesDecimal,
//     QAStyle,
//     displayObjectsLocation,
//     chapterLevelAffiliation,
//     bookLevelAffiliation,
//     citation,
//     referenceList,
//     xrefAnchor,
//     versionLink,
//   ) => {
//     cy.contains(orgName).click()
//     cy.contains(bookTitle).click()
//     cy.get('[data-test-id="bookSettings"]').click()
//     cy.get('[name="openAccessStatus"]').should('have.value', openAccessStatus)
//     cy.get('[name="UKPMC"]').should('have.value', UKPMC)

//     if (word) {
//       cy.get('[name="supportMultiplePublishedVersions"]').should(
//         'have.value',
//         supportMultiplePublishedVersions,
//       )
//     }

//     cy.get('[name="indexInPubmed"]').should('have.value', indexInPubmed)

//     cy.get('[name="indexChaptersInPubmed"]').should(
//       'have.value',
//       indexChaptersInPubmed,
//     )

//     cy.get('[name="publisherUrl"]').should('have.value', publisherUrl)
//     cy.get('[name="citationSelfUrl"]').should('have.value', citationSelfUrl)

//     cy.get('[name="specialPublisherLinkUrl"]').should(
//       'have.value',
//       specialPublisherLinkUrl,
//     )

//     cy.get('[name="specialPublisherLinkText"]').should(
//       'have.value',
//       specialPublisherLinkText,
//     )

//     cy.get('[name="bookLevelLinks"]').should('have.value', bookLevelLinks)

//     /*    if (cy.get('[name="bookLevelLinks"]').eq('true')) {
//       cy.get('input[name="bookLevelLinksMarkdown"]').should(
//         'have.value',
//         bookLevelLinksMarkdown,
//       )
//     } */

//     if (word) {
//       cy.get('[name="createBookLevelPdf"]').should(
//         'have.value',
//         createBookLevelPdf,
//       )
//     }

//     cy.get('[name="displayBookLevelPdf"]').should(
//       'have.value',
//       displayBookLevelPdf,
//     )

//     if (submissionType === 'individualChapters') {
//       if (word) {
//         cy.get('[name="createChapterLevelPdf"]').should(
//           'have.value',
//           createChapterLevelPdf,
//         )

//         cy.get('[name="displayChapterLevelPdf"]').should(
//           'have.value',
//           displayChapterLevelPdf,
//         )
//       }

//       cy.get('[name="groupChaptersInParts"]').should(
//         'have.value',
//         groupChaptersInParts,
//       )

//       cy.get('input[name="orderChaptersBy"]').should(
//         'have.value',
//         orderChapters,
//       )

//       cy.get('[name="displayHeadingLevel"]').should(
//         'have.value',
//         displayHeadingLevel,
//       )

//       cy.get('[name="tocExpansionLevel"]').should(
//         'have.value',
//         tocExpansionLevel,
//       )

//       cy.get('[name="tocExpansionLevel"]').scrollIntoView()
//       cy.get('[name="tocContributors"]').should('have.value', tocContributors)

//       cy.get('[name="tocChapterLevelDates"]').should(
//         'have.value',
//         tocChapterLevelDates,
//       )

//       cy.get('[name="tocSubtitle"]').should('have.value', tocSubtitle)
//       cy.get('[name="tocAltTitle"]').should('have.value', tocAltTitle)
//     }

//     cy.get('[name="footnotesDecimal"]').should('have.value', footnotesDecimal)
//     cy.get('input[name="questionAnswerStyle"]').should('have.value', QAStyle)

//     cy.get('input[name="displayObjectsLocation"]').should(
//       'have.value',
//       displayObjectsLocation,
//     )

//     cy.get('input[name="chapterLevelAffiliationStyle"]').should(
//       'have.value',
//       chapterLevelAffiliation,
//     )

//     cy.get('input[name="bookLevelAffiliationStyle"]').should(
//       'have.value',
//       bookLevelAffiliation,
//     )

//     if (word) {
//       cy.get('input[name="citationType"]').should('have.value', citation)
//     }

//     cy.get('input[name="referenceListStyle"]').should(
//       'have.value',
//       referenceList,
//     )

//     cy.get('input[name="xrefAnchorStyle"]').should('have.value', xrefAnchor)

//     if (submissionType === 'wholebook') {
//       cy.get('[name="createVersionLink"]').should('have.value', versionLink)
//     }
//   },
// )
