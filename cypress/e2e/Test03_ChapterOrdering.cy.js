/* eslint-disable jest/no-commented-out-tests */
import { admin, organisation, workflows } from '../support/credentials'

describe('Creating a new chapter-processed Word book', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/seeds/index.js')
  })

  beforeEach(() => {
    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
  })

  it('creating an organisation with word wokflow', () => {
    cy.addOrganisation(
      organisation,
      workflows[1],
      'name="settings.type.publisher"',
    )
  })

  it('creating an Word chapter processed book', () => {
    cy.contains(organisation.name).click()
    cy.addBook('Order chapters book', workflows[1].name, 'Individual chapters')
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('Adding first chapter', () => {
    cy.contains(organisation.name).click()
    cy.contains('Order chapters book').click()
    cy.get('[data-test-id="bookUpload"]').click()
    cy.get('input[type="file"]').attachFile(['chapter_02.docx'])
    cy.get('button[data-test-id="book-uplaod"]').click()

    cy.get('[title="Untitled"] span a').click({ force: true })
    cy.get('div[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile([
      'chapter_02.xml',
    ])

    cy.contains('Save').click()
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('Adding second chapter', () => {
    cy.contains(organisation.name).click()
    cy.contains('Order chapters book').click()
    cy.get('[data-test-id="bookUpload"]').click()
    cy.get('input[type="file"]').attachFile(['chapter_03.docx'])
    cy.get('button[data-test-id="book-uplaod"]').click()

    cy.get('[title="Untitled"] span a').click({ force: true })
    cy.get('div[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile([
      'chapter_03.xml',
    ])

    cy.contains('Save').click()
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('Adding third chapter', () => {
    cy.contains(organisation.name).click()
    cy.contains('Order chapters book').click()
    cy.get('[data-test-id="bookUpload"]').click()
    cy.get('input[type="file"]').attachFile(['chapter_01.docx'])
    cy.get('button[data-test-id="book-uplaod"]').click()

    cy.get('[title="Untitled"] span a').click({ force: true })
    cy.get('div[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile([
      'chapter_01.xml',
    ])

    cy.contains('Save').click()
  })
})

describe('Ordering chapters', () => {
  beforeEach(() => {
    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
    cy.contains(organisation.name).click()
    cy.contains('Order chapters book').click()
  })

  it('Ordering by Title', () => {
    cy.get('[data-test-id="bookSettings"]').click()

    cy.get('[data-test-id="order-chapters-select"] .select__control').click({
      force: true,
    })

    cy.get('.select__option:nth(1)').click()
    cy.get('[data-test-id="save-book-settings"]').click()
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('Validate Order by Title', () => {
    cy.get('.ant-tree-list-holder-inner > :nth-child(1)').contains(
      'B sample chapter',
    )

    cy.get('.ant-tree-list-holder-inner > :nth-child(2)').contains(
      'C sample chapter',
    )

    cy.get('.ant-tree-list-holder-inner > :nth-child(3)').contains(
      'New title for chapter 1',
    )
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('Ordering by label ascending', () => {
    cy.get('[data-test-id="bookSettings"]').click()

    cy.get('[data-test-id="order-chapters-select"] .select__control').click({
      force: true,
    })

    cy.get('.select__option:nth(2)').click()

    cy.get('[data-test-id="save-book-settings"]').click()
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('Validate Order by label ascending', () => {
    cy.get('.ant-tree-list-holder-inner > :nth-child(1)').contains(
      'New title for chapter 1',
    )

    cy.get('.ant-tree-list-holder-inner > :nth-child(2)').contains(
      'B sample chapter',
    )

    cy.get('.ant-tree-list-holder-inner > :nth-child(3)').contains(
      'C sample chapter',
    )
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('Ordering by document label descending', () => {
    cy.get('[data-test-id="bookSettings"]').click()

    cy.get('[data-test-id="order-chapters-select"] .select__control').click({
      force: true,
    })

    cy.get('.select__option:nth(3)').click()

    cy.get('[data-test-id="save-book-settings"]').click()

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(2000)

    cy.get('.ant-tree-list-holder-inner > :nth-child(1)').contains(
      'C sample chapter',
    )

    cy.get('.ant-tree-list-holder-inner > :nth-child(2)').contains(
      'B sample chapter',
    )

    cy.get('.ant-tree-list-holder-inner > :nth-child(3)').contains(
      'New title for chapter 1',
    )
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('Ordering by date', () => {
    cy.get('[data-test-id="bookSettings"]').click()

    cy.get('[data-test-id="order-chapters-select"] .select__control').click({
      force: true,
    })

    cy.get('.select__option:nth(4)').click()

    cy.get('[data-test-id="save-book-settings"]').click()

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(2000)

    cy.get('.ant-tree-list-holder-inner > :nth-child(1)').contains(
      'New title for chapter 1',
    )

    cy.get('.ant-tree-list-holder-inner > :nth-child(2)').contains(
      'C sample chapter',
    )

    cy.get('.ant-tree-list-holder-inner > :nth-child(3)').contains(
      'B sample chapter',
    )
  })
})
