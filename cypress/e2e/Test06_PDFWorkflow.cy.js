/* eslint-disable jest/no-disabled-tests */
/* eslint-disable jest/no-commented-out-tests */
import {
  admin,
  chapterProcessedBook,
  // generalUser,
  organisation,
  workflows,
} from '../support/credentials'
import { aliasQuery } from '../utils/graphql-test-utils'

describe('Testing PDF workflow', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/seeds/index.js')

    cy.exec(`node ./scripts/createOrg.js ${organisation.name}`)
  })

  beforeEach(() => {
    cy.intercept('POST', 'http://localhost:3000/graphql', req => {
      // Queries
      aliasQuery(req, 'GetOrganisation')
      aliasQuery(req, 'getBook')
    })

    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
  })

  it('creating a chapter processed PDF book', () => {
    cy.contains(organisation.name).click()

    cy.addBook(
      chapterProcessedBook.title,
      workflows[0].name,
      'Individual chapters',
    )

    cy.get('[data-test-id="addBookPart"]').should('not.exist')
    cy.contains('button', 'Collapse all').should('not.exist')
  })

  it('checking bulk uploads', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookUpload"]').click()

    cy.get('input[type="file"]').attachFile([
      'Type document.docx',
      'Type_of_Document_You_Wish_to_Create.docx',
      'Type$doc.docx',
      '1typedoc.docx',

      'ack.docx',
      'chapter_01.xml',
      'appg-et1.xlsx',
      'chapter.doc',
      'app5-Table3.pdf',
    ])

    cy.get(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
    ).scrollIntoView()

    cy.get('[data-test-id="Type document.docx"]').scrollIntoView()

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    cy.get('[data-test-id="Type$doc.docx"]').scrollIntoView()

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.contains('[data-test-id="1typedoc.docx"]', 'Ready for Upload')
    cy.contains('[data-test-id="ack.docx"]', 'Ready for Upload')
    cy.contains('[data-test-id="chapter_01.xml"]', 'Ready for Upload')
    cy.contains('[data-test-id="appg-et1.xlsx"]', 'Ready for Upload')
    cy.contains('[data-test-id="chapter.doc"]', 'Ready for Upload')
    cy.contains('[data-test-id="app5-Table3.pdf"]', 'Ready for Upload')
  })

  it('Book metadata updates', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookMetadata"]').click()
    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.get('[name="publisherName"]').scrollIntoView()
    cy.contains('div', 'Publisher name is required')
    cy.contains('div', 'Publisher location is required')
    cy.contains('div', 'Publication date type is required')
    cy.get('[data-test-id="ncbi-custom-metadata"]').scrollIntoView()
    cy.contains('div', 'Book source type is required')

    cy.updateChapterProcessedMetadata(
      false,
      organisation.name,
      chapterProcessedBook.title,
    )
  })

  it('Checking updated book metadata', () => {
    cy.checkChapterProcessedMetadata(
      false,
      organisation.name,
      chapterProcessedBook.title,
    )
  })

  it('book settings update', () => {
    cy.updateChapterProcessedSettings(
      false,
      organisation.name,
      chapterProcessedBook.title,
    )
  })

  it('checking if the values are retained', () => {
    cy.checkChapterProcessedSettings(
      false,
      organisation.name,
      chapterProcessedBook.title,
    )
  })

  it('checking files tab in book manager', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookUpload"]').click()

    cy.get('[type="file"]').attachFile(
      '/pdfwf/chapterProcessed/eh0072_3dprinting.pdf',
    )

    cy.get('[data-test-id="book-uplaod"]').click()

    cy.get('[class*=Status__Root]').click()
    cy.get('[data-test-id="files-tab"]').click()

    cy.log('Source')

    cy.get('[data-test-id="upload-source-input"]').attachFile(
      'Type_of_Document_You_Wish_to_Create.docx',
    )

    cy.get(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
    ).scrollIntoView()

    cy.contains(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
      'Filename does not match previous version (eh0072_3dprinting.pdf)',
    )

    cy.get('[data-test-id="remove-btn"]').click()
    cy.contains('Source').click()

    // cy.contains('Converted').click()
    cy.log('converted')

    cy.get('[data-test-id="upload-converted-input"]').attachFile(
      'Type_of_Document_You_Wish_to_Create.docx',
    )

    cy.get(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
    ).scrollIntoView()

    cy.contains(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
      'File extension should be .xml or .bxml',
    )

    cy.get('[data-test-id="remove-btn"]').click()

    cy.contains('Converted').click()

    cy.contains('Bookshelf Display PDFs').click()
    cy.log('Bookshelf PDFs validation')

    // cy.get('[data-test-id="upload-displayPDFs-input"]').attachFile([
    //   'Type document.docx',
    //   'Type$doc.docx',
    // ])

    // cy.contains(
    //   '[data-test-id="Type document.docx"]',
    //   'Filenames must not contain spaces',
    // )

    // cy.contains(
    //   '[data-test-id="Type$doc.docx"]',
    //   'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    // )

    // cy.get('[data-test-id="remove-btn"]').click()
    cy.contains('Bookshelf Display PDFs').click()

    cy.contains('Supplementary').click()
    cy.log('supplementary')
    cy.get('[data-test-id="upload-supplementary-input"]').scrollIntoView()

    cy.get('[data-test-id="upload-supplementary-input"]').attachFile([
      'Type document.docx',
      'Type$doc.docx',
    ])

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.get('[data-test-id="remove-btn"]').click()

    cy.contains('Supplementary').click()

    cy.contains('Images').click()

    cy.log('supplementary')
    cy.get('[data-test-id="upload-images-input"]').scrollIntoView()

    cy.get('[data-test-id="upload-images-input"]').attachFile([
      'Type document.docx',
      'Type$doc.docx',
    ])

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.get('[data-test-id="remove-btn"]').click()
    cy.contains('Images').click()

    cy.contains('Support').click()
    cy.log('support')

    // checking support file and upload button

    cy.contains('a', 'vendor-meta.xml')
    cy.get('[data-test-id="upload-support-input"]').should('exist')

    cy.get('[data-test-id="upload-support-input"]').attachFile([
      'Type document.docx',
      'Type$doc.docx',
    ])

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.get('[data-test-id="remove-btn"]').click()
    cy.contains('Support').click()

    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()
    cy.get('[data-test-id="file-input-feedback"]').attachFile('ack.docx')
    cy.get('[data-test-id="send-feedback"]').click()
    cy.get('[data-test-id="files-tab"]').click()
    cy.contains('Review').click()
    cy.contains('div', 'ack.docx')
  })

  it('uploading converted file and checking metadata', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlgetBookQuery')
    cy.get('[class*=Status__Root]').click()
    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile(
      '/pdfwf/chapterProcessed/eh0072_3dprinting.xml',
    )

    cy.get('[data-test-id="save-btn"]').click()
    cy.reload()
    cy.get('[data-test-id="metadata-tab"]').click()

    cy.get('[name="title"]')
      .should(
        'have.value',
        'An Overview of Clinical Applications of 3-D Printing and Bioprinting',
      )
      .should('be.disabled')

    cy.get('[name="subTitle"]')
      .should('have.value', 'clininal application subtitle')
      .should('be.disabled')

    cy.get('[name="altTitle"]')
      .should('have.value', 'clinical application alternative')
      .should('be.disabled')

    cy.get('[name="label"]').should('have.value', '175').should('be.disabled')
    cy.get('[name="language"]').should('have.value', 'en').should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="MM"]')
      .should('have.value', '11')
      .should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="DD"]')
      .should('have.value', '09')
      .should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="YYYY"]')
      .should('have.value', '2020')
      .should('be.disabled')

    cy.get('[data-test-id="date-created"] input[placeholder="MM"]')
      .should('have.value', '11')
      .should('be.disabled')

    cy.get('[data-test-id="date-created"] input[placeholder="DD"]')
      .should('have.value', '21')
      .should('be.disabled')

    cy.get('[data-test-id="date-created"] input[placeholder="YYYY"]')
      .should('have.value', '2013')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="MM"]')
      .should('have.value', '4')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="DD"]')
      .should('have.value', '16')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="YYYY"]')
      .should('have.value', '2020')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="MM"]')
      .should('have.value', '8')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="DD"]')
      .should('have.value', '20')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="YYYY"]')
      .should('have.value', '2020')
      .should('be.disabled')

    cy.get('[name="authors[0].surname"]')
      .should('have.value', 'Mason')
      .should('be.disabled')

    cy.get('[name="authors[0].givenName"]')
      .should('have.value', 'Jeff')
      .should('be.disabled')

    cy.get('[name="authors[1].surname"]')
      .should('have.value', 'Visintini')
      .should('be.disabled')

    cy.get('[name="authors[1].givenName"]')
      .should('have.value', 'Sarah')
      .should('be.disabled')

    cy.get('[name="authors[2].surname"]')
      .should('have.value', 'Quay')
      .should('be.disabled')

    cy.get('[name="authors[2].givenName"]')
      .should('have.value', 'Teo')
      .should('be.disabled')
  })

  it('check versions,delete supplementary,bookshelf files and images ', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlgetBookQuery')
    cy.contains('div', 'New Upload')
    cy.contains('div', 'New Upload').click()
    cy.get('[data-test-id="files-tab"]').click()
    cy.contains('Source').click()
    cy.contains('Converted').click()
    cy.checkFileSection('Bookshelf Display PDFs', 'pdf-chap.pdf')
    cy.checkFileSection('Supplementary', 'ack.docx', 3)
    cy.checkFileSection('Images', 'img12.png', 4)
  })

  // it('Errors tab and help pannel', () => {
  //   cy.contains(organisation.name).click()
  //   cy.contains(chapterProcessedBook.title).click()
  //   cy.contains('eh0072_3dprinting.xml').click()
  //   cy.get('[data-test-id="errors-tab"]').click()
  //   cy.get('[data-test-id="show-feedback"]').click()
  //   cy.get('.mentions__input').type('query 1')
  //   cy.get('[data-test-id="send-feedback"]').click()

  //   cy.contains(
  //     '[data-test-id="feedback-container"] [data-test-id="author-name"]',
  //     admin.username.charAt(0).toUpperCase() + admin.username.slice(1),
  //   )

  //   cy.contains(
  //     '[data-test-id="feedback-container"] [data-test-id="message-content"]',
  //     'query 1',
  //   )
  // })

  // it('Checking download and download all', () => {
  //   var bcmsId = 'bcmsId'

  //   cy.contains('TestOrganisation164').click()
  //   cy.contains('chapterProcessedBook164').click()
  //   cy.contains('frontmatterpart.xml').click()
  //   cy.get('[data-test-id="metadata-tab"]').click()

  //   cy.get('[name="bcmsId"]').then($input => {
  //     console.log($input.val())
  //     bcmsId = $input.val()
  //     bcmsId = bcmsId.substr(0, 4) + '-' + bcmsId.substr(4, 7)
  //     // localStorage.setItem("bcmdIdnew",bcmsId)
  //   })

  //   const curDate = new Date(new Date().toUTCString()).toISOString()
  //   const tempArr = curDate.split('T')
  //   cy.get('[data-test-id="files-tab"]').click()
  //   cy.get('[data-test-id="files-tab-downloader"]').click()

  //   cy.contains('Download all')
  //     .click()
  //     .then(() => {
  //       const downloadsFolder = Cypress.config('downloadsFolder')
  //       let rd =
  //         bcmsId +
  //         '-version-1-' +
  //         tempArr[0].split('-').join('_') +
  //         '-' +
  //         tempArr[1].split(':').join('_').substr(0, 8) +
  //         '.zip'
  //         console.log(rd)
  //         console.log(downloadsFolder)
  //       const downloadedFilename = path.join(downloadsFolder,rd)

  //       cy.readFile(downloadedFilename,{timeout:60000}).should("exist")

  //       cy.verifyDownload(
  //         bcmsId +
  //           '-version-1-' +
  //           tempArr[0].split('-').join('_') +
  //           '-' +
  //           tempArr[1].split(':').join('_').substr(0, 8) +
  //           '.zip',
  //       )
  //     })
  // })
})

describe('Parts - Create Parts and add/remove editors', () => {
  beforeEach(() => {
    cy.intercept('POST', 'http://localhost:3000/graphql', req => {
      // Queries
      aliasQuery(req, 'GetOrganisation')
      aliasQuery(req, 'getBook')
    })

    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlGetOrganisationQuery')
    cy.wait('@gqlgetBookQuery')
  })

  it('Adding part', () => {
    cy.get('[data-test-id="addBookPart"]').click()
    cy.get('[data-test-id="title-editor"] div.ProseMirror').type('Part32')
    cy.get('[data-test-id="add-metadata.editor"]').click()
    cy.get('[name="metadata.editor[0].surname"]').type('doe')
    cy.get('[name="metadata.editor[0].givenName"]').type('john')
    cy.get('[name="metadata.editor[0].suffix"]').type('1010')
    cy.get('[data-test-id="save-add-part"]').click()
    cy.wait('@gqlGetOrganisationQuery')
    cy.contains('div', 'Part32')
  })

  it('checking if values are retained part data', () => {
    cy.contains('div', 'Part32').click({ force: true })
    cy.contains('[data-test-id="title-editor"] .ProseMirror', 'Part32')

    cy.get('[name="metadata.editor[0].surname"]').should('have.value', 'doe')

    cy.get('[name="metadata.editor[0].givenName"]').should('have.value', 'john')

    cy.get('[name="metadata.editor[0].suffix"]').should('have.value', '1010')

    // No contributors added yet
  })

  // it('nested part', () => {
  //   cy.contains(organisation.name).click()
  //   cy.contains(chapterProcessedBook.title).click()
  //   const dataTransfer = new DataTransfer()

  //   cy.get('[data-test-id="chapter-title-Part32"]').trigger('dragstart', {
  //     dataTransfer,
  //   })

  //   cy.get('[data-test-id="Part32-wrapper"]').trigger('drop', {
  //     dataTransfer,
  //   })
  // })

  it('updating parts', () => {
    cy.contains('div', 'Part32').click({ force: true })

    cy.get('[data-test-id="title-editor"] div.ProseMirror').type(
      `{backspace}{backspace}45`,
    )

    cy.get('[name="metadata.editor[0].surname"]').clear().type('williams')
    cy.get('[name="metadata.editor[0].givenName"]').clear().type('serina')
    cy.get('[name="metadata.editor[0].suffix"]').clear().type('1010')
    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.wait('@gqlGetOrganisationQuery')
  })

  it('checking updated file and deleting contributors', () => {
    cy.contains('div', 'Part45').click({ force: true })

    cy.get('[name="metadata.editor[0].surname"]').should(
      'have.value',
      'williams',
    )

    cy.get('[name="metadata.editor[0].givenName"]').should(
      'have.value',
      'serina',
    )

    cy.get('[name="metadata.editor[0].suffix"]').should('have.value', '1010')

    cy.get('[data-test-id="delete-1"]').click()

    cy.contains('No contributors added yet')
    cy.get('[data-test-id="save-book-metadata"]').click()
  })
})

// describe('User tests', () => {
//   let componentUrl = ''

//   it('creating a new user', () => {
//     cy.addUser(generalUser)
//   })

//   it('request access to an org', () => {
//     cy.login(generalUser)
//     cy.get('[data-test-id="org-access-btn"]').click({ force: true })
//     cy.contains('div', 'Organization Access')
//     cy.get('.select__control ').type(`${organisation.name} {enter}`)
//     cy.get('[data-test-id="request-access"]').click()
//     cy.contains('div', 'Request Submitted')
//   })

//   it('accepting the user', () => {
//     cy.login(admin)
//     cy.get('[data-test-id="organizations-tab"]').click()
//     cy.wait(500)
//     cy.contains(organisation.name).click()
//     cy.get('[data-test-id="users-tab"]').click()
//     cy.contains('div', generalUser.username).click()

//     cy.intercept({ method: 'POST', url: 'http://localhost:3000/graphql' }).as(
//       'waitForReq',
//     )

//     cy.get('[data-test-id="accept-user-modal"]').click()
//     cy.wait('@waitForReq')
//     cy.get('[data-test-id="saveUserModal"]').click()
//     cy.wait('@waitForReq')
//     cy.contains('button', 'Cancel').click()
//     cy.contains('div', generalUser.username).click()

//     cy.get('input[name="editor"]').click({ force: true })
//     cy.get('[data-test-id="saveUserModal"]').click()
//     cy.wait('@waitForReq')

//     cy.contains('div', `User ${generalUser.givenName} modified succesfully`)
//     cy.contains('button', 'Cancel').click()
//     cy.get('[data-test-id="books-and collections-tab"]').click()
//     cy.contains(chapterProcessedBook.title).click()
//     cy.contains('span', 'eh0072_3dprinting.xml').click()
//     cy.get('[data-test-id="team-tab" ]').click()
//     cy.get('.select__control ').type(`${generalUser.givenName} {enter}`)
//     cy.contains('button', 'Add Member').click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForCompTeam')

//     cy.get('[data-test-id="save-component-team"]').click()
//     cy.wait('@waitForCompTeam')
//     cy.get('[data-test-id="files-tab"]').click()

//     cy.url().then(urlString => {
//       componentUrl = urlString
//     })
//   })

//   it('checking book component as an editor of the component', () => {
//     cy.visit(componentUrl)
//     cy.get('input[name="username"]').type(generalUser.username)
//     cy.get('input[name="password"]').type(generalUser.password)
//     cy.get('button[type="submit"]').click()
//     cy.get('[data-test-id="preview-tab"]').should('exist')
//     cy.get('[data-test-id="team-tab"]').should('exist')
//     cy.get('[data-test-id="files-tab"]').should('exist')
//     cy.get('[data-test-id="files-tab"]').click()
//     cy.contains('a', 'eh0072_3dprinting.pdf')
//     cy.get('[data-test-id="metadata-tab"]').should('exist')
//     cy.get('[data-test-id="errors-tab"]').should('exist')
//     cy.get('[data-test-id="back-link"]').click()
//     // cy.contains('You are not assigned any role yet')
//     cy.contains(chapterProcessedBook.title)
//     cy.contains('front Matter')
//     cy.contains('body')
//     cy.contains('back Matter')
//     cy.get('[data-test-id="bookMetadata"]').should('exist')
//     cy.get('[data-test-id="bookSettings"]').should('exist')
//     cy.get('[data-test-id="book-toc"]').should('exist')
//   })

//   it('adding user to the book team', () => {
//     cy.login(admin)
//     // cy.contains(organisation.name).click()
//     cy.contains(chapterProcessedBook.title).click()
//     cy.get('[data-test-id="bookTeams"]').click()
//     cy.get('.select__control ').type(`${generalUser.givenName} {enter}`)
//     cy.contains('button', 'Add Member').click()
//     cy.get('[data-test-id="save-book-team"]').click()

//     cy.login(generalUser)
//     cy.contains('div', organisation.name).click()
//     cy.contains('div', chapterProcessedBook.title).click()
//     cy.get('[data-test-id="bookMetadata"]').should('exist')
//     cy.get('[data-test-id="bookSettings"]').should('exist')
//     cy.get('[data-test-id="bookTeams"]').should('exist')
//     cy.get('[data-test-id="book-toc"]').should('exist')
//   })

//   it('Errors tab and help pannel', () => {
//     cy.login(admin)
//     cy.contains(organisation.name).click()
//     cy.contains(chapterProcessedBook.title).click()
//     cy.contains('eh0072_3dprinting.xml').click()
//     cy.get('[data-test-id="errors-tab"]').click()
//     cy.get('[data-test-id="show-feedback"]').click()
//     cy.get('.mentions__input').type(`hi @${generalUser.givenName}{enter}`)
//     cy.get('[data-test-id="send-feedback"]').click()
//     cy.get('[data-test-id="preview-tab"]').click()
//     cy.get('[data-test-id="show-feedback"]').click()
//     cy.get('.mentions__input').type(`hi @${generalUser.givenName}{enter}`)
//     cy.get('[data-test-id="send-feedback"]').click()

//     cy.contains(
//       '[data-test-id="feedback-container"] [data-test-id="author-name"]',
//       admin.username.charAt(0).toUpperCase() + admin.username.slice(1),
//     )

//     cy.login(generalUser)
//     cy.contains(organisation.name).click()
//     cy.contains(chapterProcessedBook.title).click()
//     cy.contains('eh0072_3dprinting.xml').click()
//     cy.get('[data-test-id="errors-tab"]').click()
//     cy.get('[data-test-id="show-feedback"]').click()

//     cy.contains(
//       '[data-test-id="feedback-container"] [data-test-id="message-content"]',
//       `hi @${generalUser.givenName} ${generalUser.surname}`,
//     )

//     cy.get('[data-test-id="send-feedback"]').click()
//     cy.get('[data-test-id="preview-tab"]').click()
//     cy.get('[data-test-id="show-feedback"]').click()

//     cy.contains(
//       '[data-test-id="feedback-container"] [data-test-id="message-content"]',
//       `hi @${generalUser.givenName} ${generalUser.surname}`,
//     )
//   })
// })
