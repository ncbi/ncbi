/* eslint-disable jest/no-commented-out-tests */
// /* eslint-disable jest/no-commented-out-tests */
// const {
//   admin,
//   organisation,
//   workflows,
//   collection,
// } = require('../support/credentials')

// describe('There are no custom templates', () => {
//   //   before(() => {
//   //     cy.exec('node ./scripts/truncateDB.js')cy.addOrganisation(organisation, [workflows[0], workflows[1],workflows[2]);
//   //     cy.exec('node ./scripts/seeds/index.js')
//   //   })
//   /* const wfIndex1 = Math.floor(Math.random() * (2 - 0 + 1)) + 0
//   let wfIndex2 = wfIndex1

//   if ((wfIndex1 >= 1 && wfIndex1 < 2) || wfIndex1 === 0) {
//     wfIndex2 = wfIndex1 + 1
//   } else if (wfIndex1 > 0 && wfIndex1 <= 2) {
//     wfIndex2 = wfIndex1 - 1
//   } */

//   beforeEach(() => {
//     cy.login(admin)
//     // cy.visit('/dashboard')
//     // cy.url().should('eq', `${Cypress.config().baseUrl}/dashboard`)

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('getOrganisations')

//     cy.get('[data-test-id="organizations-tab"]').click()
//     cy.wait('@getOrganisations')
//   })

//   it('Creating a new organisation ', () => {
//     cy.addOrganisation(
//       organisation,
//       workflows,
//       'name="settings.type.publisher"',
//       'name="settings.collections.bookSeries"',
//     )
//   })

//   it('Create an collection', () => {
//     cy.contains(organisation.name).click()

//     /*
//     cy.addCollection(
//       'testCollection',
//       collection.publisher,
//       collection.location,
//     ) */
//     cy.get('button[data-test-id="create-new-collection"]').click()
//     cy.get('[data-test-id="collection-type"]').click()
//     cy.get('.ProseMirror').click()
//     cy.contains('div', 'Collection type is required')
//     cy.get('[name="publisherName"]').focus().blur()
//     cy.contains('div', 'Publisher name is required')
//     cy.get('[name="publisherLocation"]').focus().blur()
//     cy.get('[data-test-id="pubdate-type-select"]').click()
//     cy.get('[data-test-id="wholeBookSourceType-select"]').click()

//     cy.get(
//       '[data-test-id="chapterProcessedSourceType-select"]',
//     ).scrollIntoView()

//     cy.get('input[data-test-id="start-month"]').focus().blur()
//     cy.contains('div', 'Date of publication is required')
//     cy.get('[data-test-id="chapterProcessedSourceType-select"]').click()
//     cy.contains('button', 'Next').click({ force: true })
//     cy.contains('div', 'Publication date type is required')
//     cy.contains('div', 'Whole book source type is required')
//     cy.contains('div', 'Chapter processed source type is required')
//     cy.get('[data-test-id="collection-type"]').scrollIntoView()
//     cy.contains('div', 'Title is required')
//     cy.get('[data-test-id="collection-type"]').click()
//     cy.contains('div', 'Book Series Collection').click()
//     cy.get('.ProseMirror').type('testCollection')
//     cy.get('[name="publisherName"]').type(collection.publisher)
//     cy.get('[name="publisherLocation"]').type(collection.location)

//     cy.get(
//       '[data-test-id="chapterProcessedSourceType-select"]',
//     ).scrollIntoView()

//     cy.get('[data-test-id="pubdate-type-select"]').type(
//       `Print publication range {enter}`,
//     )

//     cy.get('[color="#8a8383"]').click()
//     cy.get('[data-test-id="wholeBookSourceType-select"]').type(`Book {enter}`)
//     cy.contains('div', 'Book').click()

//     cy.get('[data-test-id="chapterProcessedSourceType-select"]').type(
//       `Book{enter}`,
//     )

//     cy.contains('div', 'Book').click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForCollection')

//     cy.contains('button', 'Next').click()
//     cy.wait('@waitForCollection')
//     // cy.contains("div",title);

//     cy.get('[data-test-id="org-admin-or-editor-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="requireApprovalByOrgAdminOrEditor"]').should(
//       'have.value',
//       'true',
//     )

//     cy.get('[name="openAccess"]').should('have.value', 'false')
//     cy.get('[name="UKPMC"]').should('have.value', 'false')
//     cy.get('input[name="publisherUrl"]').should('be.empty')
//     cy.get('input[name="citationSelfUrl"]').should('be.empty')
//     cy.get('input[name="groupBooksOnTOC"]').should('have.value', 'false')
//     cy.get('[data-test-id="order-books-by"]').type(`Title {enter}`)
//     cy.get('input[name="tocContributors"]').should('have.value', 'false')
//     cy.get('input[name="tocFullBookCitations"]').should('have.value', 'false')
//     cy.contains('button', 'Submit').click()
//   })

//   it('Checking that a Chapter-Processed book has the default settings when there are no templates yet', () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a chapter-processed book in the collection
//     cy.get('button[data-test-id="create-new-book"]').click()

//     cy.get('input[name="title"]').type(
//       'Test for chapter-processed default settings book',
//     )

//     cy.get('[data-test-id="collection-select"]')
//       .click({ force: true })
//       .type('testCollection{enter}')

//     cy.contains('span', 'Word').click({ force: true })
//     cy.contains('span', 'Individual chapters').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')

//     cy.get('input[name="orderChaptersBy"]').should('have.value', 'manual')
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '4')
//     cy.get('input[name="tocContributors"]').should('have.value', 'true')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'true')

//     cy.get('input[name="citationType"]').should(
//       'have.value',
//       'numberedParentheses',
//     )

//     cy.get('[data-test-id="save-settings"]').click({ force: true })
//     cy.wait('@waitForSave')

//     // Check if the settings of the book match with the settings of the default template
//     cy.contains('Test for chapter-processed default settings book').click()
//     cy.contains('Settings').click()
//     cy.contains('testCollection')
//     // Publishing Settings
//     cy.get('[name="openAccessStatus"]').should('have.value', 'false')
//     cy.get('[data-test-id="ukpmc-lock"]').scrollIntoView()
//     cy.get('[name="UKPMC"]').should('have.value', 'false')

//     cy.get('[name="supportMultiplePublishedVersions"]').should(
//       'have.value',
//       'false',
//     )

//     cy.get('[data-test-id="open-access-status-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[data-test-id="ukpmc-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[data-test-id="support-multiple-published-versions-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('input[name="indexInPubmed"]').should('have.value', 'false')
//     cy.get('input[name="indexChaptersInPubmed"]').should('have.value', 'false')
//     // Landing Page: Links
//     cy.get('input[name="publisherUrl"]').should('be.empty')
//     cy.get('input[name="citationSelfUrl"]').should('be.empty')
//     cy.get('input[name="specialPublisherLinkUrl"]').should('be.empty')
//     cy.get('input[name="specialPublisherLinkText"]').should('be.empty')
//     cy.get('input[name="bookLevelLinks"]').should('have.value', 'false')
//     // Landing Page: Downloads
//     cy.get('[name="createBookLevelPdf"]').should('have.value', 'false')
//     cy.get('[name="displayBookLevelPdf"]').should('have.value', 'false')
//     cy.get('[name="createChapterLevelPdf"]').should('have.value', 'true')
//     cy.get('[name="displayChapterLevelPdf"]').should('have.value', 'true')

//     cy.get('[data-test-id="create-book-level-pdf-for-download-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[data-test-id="create-chapter-level-pdf-for-download-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     // Landing Page: Table of Contents
//     cy.get('input[name="groupChaptersInParts"]').should('have.value', 'false')
//     cy.get('input[name="orderChaptersBy"]').should('have.value', 'manual')
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '4')
//     cy.get('input[name="tocExpansionLevel"]').should('have.value', '4')

//     cy.get('[data-test-id="group-chapters-into-parts-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     // Display Metadata in TOC
//     cy.get('input[name="tocContributors"]').should('have.value', 'true')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'true')
//     cy.get('input[name="tocSubtitle"]').should('have.value', 'true')
//     cy.get('input[name="tocAltTitle"]').should('have.value', 'false')
//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').should('have.value', 'false')
//     cy.get('input[name="questionAnswerStyle"]').should('have.value', 'normal')

//     cy.get('input[name="displayObjectsLocation"]').should(
//       'have.value',
//       'at-xref',
//     )

//     // Display Metadata
//     cy.get('input[name="chapterLevelAffiliationStyle"]').should(
//       'have.value',
//       'at-end',
//     )

//     cy.get('input[name="bookLevelAffiliationStyle"]').should(
//       'have.value',
//       'at-end',
//     )

//     // Display Reference Citation
//     cy.get('input[name="citationType"]').should(
//       'have.value',
//       'numberedParentheses',
//     )

//     cy.get('input[name="referenceListStyle"]').should('have.value', 'decimal')
//     cy.get('input[name="xrefAnchorStyle"]').should('have.value', 'default')
//   })

//   it('Checking that a Complete book has the default settings when there are no templates yet', () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a complete book in the collection
//     cy.get('button[data-test-id="create-new-book"]').click()
//     cy.get('input[name="title"]').type('Test for wholebook default settings')

//     cy.get('[data-test-id="collection-select"]').type('testCollection{enter}')

//     cy.contains('span', 'PDF').click({ force: true })
//     cy.contains('span', 'Complete books and documents').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')

//     /*
//     cy.get('input[name="requireApprovalByOrgAdminOrEditor"]').should(
//       'have.value',
//       'true',
//     )
// */
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '4')
//     cy.get('input[name="tocContributors"]').should('have.value', 'true')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'true')
//     cy.get('[data-test-id="save-settings"]').click({ force: true })
//     cy.wait('@waitForSave')

//     cy.contains('Test for wholebook default settings').click()

//     // Check if the settings of the wholebook match with the settings of the default template
//     cy.contains('Settings').click()
//     cy.contains('testCollection')
//     // Publishing Settings

//     cy.get('input[name="requireApprovalByOrgAdminOrEditor"]').should(
//       'have.value',
//       'true',
//     )

//     cy.get('[data-test-id="open-access-status-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[data-test-id="ukpmc-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="openAccessStatus"]').should('have.value', 'false')
//     cy.get('[name="UKPMC"]').should('have.value', 'false')

//     cy.get('input[name="indexInPubmed"]').should('have.value', 'false')
//     cy.get('input[name="indexChaptersInPubmed"]').should('have.value', 'false')
//     // Landing Page: Links
//     cy.get('input[name="publisherUrl"]').should('be.empty')
//     cy.get('input[name="citationSelfUrl"]').should('be.empty')
//     cy.get('input[name="specialPublisherLinkUrl"]').should('be.empty')
//     cy.get('input[name="specialPublisherLinkText"]').should('be.empty')
//     cy.get('input[name="bookLevelLinks"]').should('have.value', 'false')

//     // Landing Page: Downloads
//     cy.get('[data-test-id="display-book-level-pdf-for-download-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[data-test-id="display-chapter-level-pdf-for-download-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="displayBookLevelPdf"]').should('have.value', 'true')
//     cy.get('[name="displayChapterLevelPdf"]').should('have.value', 'false')
//     // Landing Page: Table of Contents
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '4')
//     cy.get('input[name="tocExpansionLevel"]').should('have.value', '4')
//     // Display Metadata in TOC
//     cy.get('input[name="tocContributors"]').should('have.value', 'true')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'true')
//     cy.get('input[name="tocSubtitle"]').should('have.value', 'true')
//     cy.get('input[name="tocAltTitle"]').should('have.value', 'false')
//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').should('have.value', 'false')
//     cy.get('input[name="questionAnswerStyle"]').should('have.value', 'normal')

//     cy.get('input[name="displayObjectsLocation"]').should(
//       'have.value',
//       'at-xref',
//     )

//     // Display Metadata
//     cy.get('input[name="chapterLevelAffiliationStyle"]').should(
//       'have.value',
//       'at-end',
//     )

//     cy.get('input[name="bookLevelAffiliationStyle"]').should(
//       'have.value',
//       'at-end',
//     )

//     // Display Reference Citation
//     cy.get('input[name="referenceListStyle"]').should('have.value', 'decimal')
//     cy.get('input[name="xrefAnchorStyle"]').should('have.value', 'default')
//     // Archiving and Updating
//     cy.get('input[name="createVersionLink"]').should('have.value', 'false')
//   })
// })

// describe('Checking when there are custom collection templates', () => {
//   beforeEach(() => {
//     cy.login(admin)

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('getOrganisations')

//     cy.get('[data-test-id="organizations-tab"]').click()
//     cy.wait('@getOrganisations')
//   })

//   it('Create a new Chapter-processed Collection Template', () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     cy.contains('testCollection').click()
//     cy.contains('Settings').click()

//     cy.get(
//       'div[data-test-id="collection-members\' settings templates-tab"]',
//     ).click()

//     // Create template for Chapter-processed book
//     cy.contains('Choose a type of template')
//       .click()
//       .type('Chapter-processed{Enter}')

//     cy.get('[data-test-id="support-multiple-published-versions-lock"]').click({
//       force: true,
//     })

//     cy.get('input[name="supportMultiplePublishedVersions"]').click({
//       force: true,
//     })

//     cy.get('[data-test-id="support-multiple-published-versions-lock"]').click({
//       force: true,
//     })

//     cy.get('input[name="indexInPubmed"]').click({ force: true })
//     cy.get('input[name="indexChaptersInPubmed"]').click({ force: true })
//     cy.get('input[name="publisherUrl"]').type('http://www.testpublisher.com')
//     cy.get('input[name="citationSelfUrl"]').type('http://www.testcitation.com')

//     cy.get('input[name="specialPublisherLinkUrl"]').type(
//       'http://www.testspecial.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').type(
//       'Test Special Publisher Text',
//     )

//     cy.get('[data-test-id="group-chapters-into-parts-lock"]').click({
//       force: true,
//     })

//     cy.get('input[name="groupChaptersInParts"]').click({
//       force: true,
//     })

//     cy.get('[data-test-id="group-chapters-into-parts-lock"]').click({
//       force: true,
//     })

//     cy.get('[data-test-id="order-chapters-select"]').type('A > Z{enter}')
//     cy.get('div[max="4"] > input').clear().type('3')
//     cy.get('div[max="5"] > input').clear().type('3')
//     cy.get('input[name="tocContributors"]').click({ force: true })
//     cy.get('input[name="tocChapterLevelDates"]').click({ force: true })
//     cy.get('input[name="tocSubtitle"]').click({ force: true })
//     cy.get('input[name="tocAltTitle"]').click({ force: true })

//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').click({ force: true })

//     cy.get('[data-test-id="question-answer-style-select"]')
//       .click({ force: true })
//       .type('FAQ{enter}')

//     cy.get('[data-test-id="display-objects-location-select"]')
//       .click({ force: true })
//       .type('In place{enter}')

//     cy.get('[data-test-id="chapter-level-affiliation-select"]')
//       .click({ force: true })
//       .type('By contrib{enter}')

//     cy.get('[data-test-id="book-level-affiliation-select"]')
//       .click({ force: true })
//       .type('By contrib{enter}')

//     cy.get('[data-test-id="citiation-select"]')
//       .click({ force: true })
//       .type('Harvard{enter}')

//     cy.get('[data-test-id="reference-list-select"]')
//       .click({ force: true })
//       .type('None{enter}')

//     cy.get('[data-test-id="xref-anchor-select"]')
//       .click({ force: true })
//       .type('Superscripted{enter}')

//     // Save changes
//     cy.get('button[data-test-id="save-collection-settings"]').click()

//     cy.contains("Collection members' template successfully saved", {
//       timeout: 10000,
//     })
//   })

//   it("Checking that a new Chapter-Processed book has the collection members' template settings", () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a chapter-processed book in the collection
//     cy.get('button[data-test-id="create-new-book"]').click()

//     cy.get('input[name="title"]').type('Test for chapter-processed template')

//     cy.get('[data-test-id="collection-select"]')
//       .click({ force: true })
//       .type('testCollection{enter}')

//     cy.contains('span', 'Word').click({ force: true })
//     cy.contains('span', 'Individual chapters').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')

//     cy.get('[data-test-id="support-multiple-published-versions-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="supportMultiplePublishedVersions"]').should(
//       'have.value',
//       'true',
//     )

//     cy.get('[data-test-id="group-chapters-into-parts-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="groupChaptersInParts"]').should('have.value', 'true')
//     cy.get('input[name="orderChaptersBy"]').should('have.value', 'title')
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '3')
//     cy.get('input[name="tocContributors"]').should('have.value', 'false')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'false')
//     cy.get('input[name="citationType"]').should('have.value', 'harvard')

//     cy.get('[data-test-id="save-settings"]').click({ force: true })
//     cy.wait('@waitForSave')

//     cy.contains('Test for chapter-processed template').click()

//     // Check if the settings of the book match with the settings of custom collection template
//     cy.contains('Settings').click()
//     cy.contains('testCollection')

//     // Publishing Settings
//     cy.get('[data-test-id="open-access-status-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="openAccessStatus"]').should('have.value', 'false')

//     cy.get('[data-test-id="ukpmc-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="UKPMC"]').should('have.value', 'false')

//     cy.get('[data-test-id="support-multiple-published-versions-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="supportMultiplePublishedVersions"]').should(
//       'have.value',
//       'true',
//     )

//     cy.get('input[name="indexInPubmed"]').should('have.value', 'true')
//     cy.get('input[name="indexChaptersInPubmed"]').should('have.value', 'true')

//     // Landing Page: Links
//     cy.get('input[name="publisherUrl"]').should(
//       'have.value',
//       'http://www.testpublisher.com',
//     )

//     cy.get('input[name="citationSelfUrl"]').should(
//       'have.value',
//       'http://www.testcitation.com',
//     )

//     cy.get('input[name="specialPublisherLinkUrl"]').should(
//       'have.value',
//       'http://www.testspecial.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').should(
//       'have.value',
//       'Test Special Publisher Text',
//     )

//     cy.get('input[name="bookLevelLinks"]').should('have.value', 'false')

//     // Landing Page: Table of Contents
//     cy.get('[data-test-id="group-chapters-into-parts-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="groupChaptersInParts"]').should('have.value', 'true')
//     cy.get('input[name="orderChaptersBy"]').should('have.value', 'title')
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '3')
//     cy.get('input[name="tocExpansionLevel"]').should('have.value', '3')
//     // Display Metadata in TOC
//     cy.get('input[name="tocContributors"]').should('have.value', 'false')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'false')
//     cy.get('input[name="tocSubtitle"]').should('have.value', 'false')
//     cy.get('input[name="tocAltTitle"]').should('have.value', 'true')

//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').should('have.value', 'true')
//     cy.get('input[name="questionAnswerStyle"]').should('have.value', 'faq')

//     cy.get('input[name="displayObjectsLocation"]').should(
//       'have.value',
//       'in-place',
//     )

//     // Display Metadata
//     cy.get('input[name="chapterLevelAffiliationStyle"]').should(
//       'have.value',
//       'by-contrib',
//     )

//     cy.get('input[name="bookLevelAffiliationStyle"]').should(
//       'have.value',
//       'by-contrib',
//     )

//     // Display Reference Citation
//     cy.get('input[name="citationType"]').should('have.value', 'harvard')
//     cy.get('input[name="referenceListStyle"]').should('have.value', 'none')

//     cy.get('input[name="xrefAnchorStyle"]').should(
//       'have.value',
//       'superscripted',
//     )
//   })

//   it('Create a new Wholebook Collection Template', () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     cy.contains('testCollection').click()
//     cy.contains('Settings').click()

//     cy.get(
//       'div[data-test-id="collection-members\' settings templates-tab"]',
//     ).click()

//     // Create template for Chapter-processed book
//     cy.contains('Choose a type of template').click().type('Whole book{Enter}')

//     cy.get('input[name="indexInPubmed"]').click({ force: true })
//     cy.get('input[name="indexChaptersInPubmed"]').click({ force: true })

//     cy.get('input[name="publisherUrl"]').type(
//       'http://www.testPublisherOneDoc.com',
//     )

//     cy.get('input[name="citationSelfUrl"]').type(
//       'http://www.testcitationOneDoc.com',
//     )

//     cy.get('input[name="specialPublisherLinkUrl"]').type(
//       'http://www.www.testspecialOneDoc.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').type(
//       'Test Special Pub One Doc',
//     )

//     cy.get('div[max="4"] > input').clear().type('3')
//     cy.get('div[max="5"] > input').clear().type('3')

//     cy.get('input[name="tocContributors"]').click({ force: true })
//     cy.get('input[name="tocChapterLevelDates"]').click({ force: true })
//     cy.get('input[name="tocSubtitle"]').click({ force: true })
//     cy.get('input[name="tocAltTitle"]').click({ force: true })

//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').click({ force: true })

//     cy.get('[data-test-id="question-answer-style-select"]')
//       .click({ force: true })
//       .type('FAQ{enter}')

//     cy.get('[data-test-id="display-objects-location-select"]')
//       .click({ force: true })
//       .type('In place{enter}')

//     cy.get('[data-test-id="chapter-level-affiliation-select"]')
//       .click({ force: true })
//       .type('By contrib{enter}')

//     cy.get('[data-test-id="book-level-affiliation-select"]')
//       .click({ force: true })
//       .type('By contrib{enter}')

//     cy.get('[data-test-id="reference-list-select"]')
//       .click({ force: true })
//       .type('None{enter}')

//     cy.get('[data-test-id="xref-anchor-select"]')
//       .click({ force: true })
//       .type('Superscripted{enter}')

//     // Save changes for the Wholebook Template
//     cy.get('button[data-test-id="save-collection-settings"]').click()

//     cy.contains("Collection members' template successfully saved", {
//       timeout: 10000,
//     })
//   })

//   it("Checking that a new wholebook has the collection members' template settings", () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a complete book in the collection
//     cy.get('button[data-test-id="create-new-book"]').click()
//     cy.get('input[name="title"]').type('Test for wholebook custom template')

//     cy.get('[data-test-id="collection-select"]').type('testCollection{enter}')

//     cy.contains('span', 'PDF').click({ force: true })
//     cy.contains('span', 'Complete books and documents').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')

//     /*
//     cy.get('[data-test-id="org-admin-or-editor-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('input[name="requireApprovalByOrgAdminOrEditor"]').should(
//       'have.value',
//       'true',
//     )
//     */

//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '3')
//     cy.get('input[name="tocContributors"]').should('have.value', 'false')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'false')
//     cy.get('[data-test-id="save-settings"]').click({ force: true })
//     cy.wait('@waitForSave')

//     cy.contains('Test for wholebook custom template').click()

//     // Check if the settings of the book match with the settings of the default template
//     cy.contains('Settings').click()
//     cy.contains('testCollection')

//     // Publishing Settings
//     cy.get('input[name="requireApprovalByOrgAdminOrEditor"]').should(
//       'have.value',
//       'true',
//     )

//     cy.get('[data-test-id="open-access-status-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="openAccessStatus"]').should('have.value', 'false')

//     cy.get('[data-test-id="ukpmc-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="UKPMC"]').should('have.value', 'false')

//     cy.get('input[name="indexInPubmed"]').should('have.value', 'true')
//     cy.get('input[name="indexChaptersInPubmed"]').should('have.value', 'true')

//     // Landing Page: Links
//     cy.get('input[name="publisherUrl"]').should(
//       'have.value',
//       'http://www.testPublisherOneDoc.com',
//     )

//     cy.get('input[name="citationSelfUrl"]').should(
//       'have.value',
//       'http://www.testcitationOneDoc.com',
//     )

//     cy.get('input[name="specialPublisherLinkUrl"]').should(
//       'have.value',
//       'http://www.www.testspecialOneDoc.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').should(
//       'have.value',
//       'Test Special Pub One Doc',
//     )

//     // Landing Page: Downloads
//     cy.get('[name="displayBookLevelPdf"]').should('have.value', 'true')
//     cy.get('[name="displayChapterLevelPdf"]').should('have.value', 'false')

//     cy.get('[data-test-id="display-book-level-pdf-for-download-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[data-test-id="display-chapter-level-pdf-for-download-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     // Landing Page: Table of Contents
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '3')
//     cy.get('input[name="tocExpansionLevel"]').should('have.value', '3')

//     // Display Metadata in TOC
//     cy.get('input[name="tocContributors"]').should('have.value', 'false')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'false')
//     cy.get('input[name="tocSubtitle"]').should('have.value', 'false')
//     cy.get('input[name="tocAltTitle"]').should('have.value', 'true')

//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').should('have.value', 'true')
//     cy.get('input[name="questionAnswerStyle"]').should('have.value', 'faq')

//     cy.get('input[name="displayObjectsLocation"]').should(
//       'have.value',
//       'in-place',
//     )

//     // Display Metadata
//     cy.get('input[name="chapterLevelAffiliationStyle"]').should(
//       'have.value',
//       'by-contrib',
//     )

//     cy.get('input[name="bookLevelAffiliationStyle"]').should(
//       'have.value',
//       'by-contrib',
//     )

//     // Display Reference Citation
//     cy.get('input[name="referenceListStyle"]').should('have.value', 'none')

//     cy.get('input[name="xrefAnchorStyle"]').should(
//       'have.value',
//       'superscripted',
//     )

//     // Archiving and Updating
//     cy.get('input[name="createVersionLink"]').should('have.value', 'false')
//   })
// })

// describe('Checking when there are custom organisation templates and custom collection templates', () => {
//   beforeEach(() => {
//     cy.login(admin)

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('getOrganisations')

//     cy.get('[data-test-id="organizations-tab"]').click()
//     cy.wait('@getOrganisations')
//   })

//   it('Create a new Chapter-processed ORGANISATION Template', () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     cy.get("div[data-test-id='book-templates-tab']").click()

//     // Create template for Chapter-processed book
//     cy.get('.select__value-container').click().type('Chapter-processed{Enter}')

//     cy.get('input[name="publisherUrl"]').type(
//       'http://www.testOrgTemplatePub.com',
//     )

//     cy.get('input[name="citationSelfUrl"]').type(
//       'http://www.testOrgTemplateCit.com',
//     )

//     cy.get('input[name="specialPublisherLinkUrl"]').type(
//       'http://www.testOrgTemplateSpPub.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').type(
//       'Test Org Template Special Pub',
//     )

//     cy.get('[data-test-id="order-chapters-select"]').type('Chapter{enter}')

//     cy.get('div[max="4"] > input').clear().type('1')
//     cy.get('div[max="5"] > input').clear().type('1')

//     // Display Content
//     cy.get('[data-test-id="question-answer-style-select"]')
//       .click({ force: true })
//       .type('FAQ with TOC{enter}')

//     cy.get('[data-test-id="citiation-select"]')
//       .click({ force: true })
//       .type('Numbered square{enter}')

//     cy.get('[data-test-id="reference-list-select"]')
//       .click({ force: true })
//       .type('Lower alpha{enter}')

//     cy.get('[data-test-id="xref-anchor-select"]')
//       .click({ force: true })
//       .type('Autodetect{enter}')

//     // Save changes
//     cy.get('button[type="submit"]').click()

//     cy.contains('Template successfully saved', {
//       timeout: 10000,
//     })
//   })

//   it('Checking that a new Chapter-Processed book out of the collection members matches with settings of the ORG templates', () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a chapter-processed book out of collection
//     cy.get('button[data-test-id="create-new-book"]').click()

//     cy.get('input[name="title"]').type(
//       'Test for ORG chapter-processed template',
//     )

//     cy.contains('span', 'Word').click({ force: true })
//     cy.contains('span', 'Individual chapters').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')

//     cy.get('[data-test-id="support-multiple-published-versions-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="supportMultiplePublishedVersions"]').should(
//       'have.value',
//       'false',
//     )

//     cy.get('[data-test-id="group-chapters-into-parts-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="groupChaptersInParts"]').should('have.value', 'false')
//     cy.get('input[name="orderChaptersBy"]').should('have.value', 'number')
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '1')
//     cy.get('input[name="tocContributors"]').should('have.value', 'true')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'true')
//     cy.get('input[name="citationType"]').should('have.value', 'numberedSquare')

//     cy.get('[data-test-id="save-settings"]').click({ force: true })
//     cy.wait('@waitForSave')

//     cy.contains('Test for ORG chapter-processed template').click()

//     // Check if the settings of the book match with the settings of the organisation template
//     cy.contains('Settings').click()

//     // Publishing Settings
//     cy.get('[data-test-id="open-access-status-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="openAccessStatus"]').should('have.value', 'false')

//     cy.get('[data-test-id="ukpmc-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="UKPMC"]').should('have.value', 'false')

//     cy.get('[data-test-id="support-multiple-published-versions-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="supportMultiplePublishedVersions"]').should(
//       'have.value',
//       'false',
//     )

//     cy.get('input[name="indexInPubmed"]').should('have.value', 'false')
//     cy.get('input[name="indexChaptersInPubmed"]').should('have.value', 'false')

//     // Landing Page: Links
//     cy.get('input[name="publisherUrl"]').should(
//       'have.value',
//       'http://www.testOrgTemplatePub.com',
//     )

//     cy.get('input[name="citationSelfUrl"]').should(
//       'have.value',
//       'http://www.testOrgTemplateCit.com',
//     )

//     cy.get('input[name="specialPublisherLinkUrl"]').should(
//       'have.value',
//       'http://www.testOrgTemplateSpPub.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').should(
//       'have.value',
//       'Test Org Template Special Pub',
//     )

//     cy.get('input[name="bookLevelLinks"]').should('have.value', 'false')

//     // Landing Page: Downloads
//     cy.get('[name="createBookLevelPdf"]').should('have.value', 'false')
//     cy.get('[name="displayBookLevelPdf"]').should('have.value', 'false')
//     cy.get('[name="createChapterLevelPdf"]').should('have.value', 'true')
//     cy.get('[name="displayChapterLevelPdf"]').should('have.value', 'true')

//     cy.get('[data-test-id="create-book-level-pdf-for-download-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[data-test-id="create-chapter-level-pdf-for-download-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     // Landing Page: Table of Contents
//     cy.get('input[name="orderChaptersBy"]').should('have.value', 'number')

//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '1')
//     cy.get('input[name="tocExpansionLevel"]').should('have.value', '1')
//     // Display Metadata in TOC
//     cy.get('input[name="tocContributors"]').should('have.value', 'true')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'true')
//     cy.get('input[name="tocSubtitle"]').should('have.value', 'true')
//     cy.get('input[name="tocAltTitle"]').should('have.value', 'false')
//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').should('have.value', 'false')

//     cy.get('input[name="questionAnswerStyle"]').should(
//       'have.value',
//       'faq-with-toc',
//     )

//     cy.get('input[name="displayObjectsLocation"]').should(
//       'have.value',
//       'at-xref',
//     )

//     // Display Metadata
//     cy.get('input[name="chapterLevelAffiliationStyle"]').should(
//       'have.value',
//       'at-end',
//     )

//     cy.get('input[name="bookLevelAffiliationStyle"]').should(
//       'have.value',
//       'at-end',
//     )

//     // Display Reference Citation
//     cy.get('input[name="citationType"]').should('have.value', 'numberedSquare')

//     cy.get('input[name="referenceListStyle"]').should(
//       'have.value',
//       'lower-alpha',
//     )

//     cy.get('input[name="xrefAnchorStyle"]').should('have.value', 'autodetect')
//   })

//   it("Checking that a new Chapter-Processed book inside the collection matches the settings of the collection members' template settings", () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a chapter-processed book in the collection
//     cy.get('button[data-test-id="create-new-book"]').click()

//     cy.get('input[name="title"]').type(
//       'Test chapter-processed book belonging to the collection',
//     )

//     cy.get('[data-test-id="collection-select"]')
//       .click({ force: true })
//       .type('testCollection{enter}')

//     cy.contains('span', 'Word').click({ force: true })
//     cy.contains('span', 'Individual chapters').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')

//     cy.get('input[name="orderChaptersBy"]').should('have.value', 'title')
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '3')
//     cy.get('input[name="tocContributors"]').should('have.value', 'false')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'false')

//     cy.get('input[name="citationType"]').should('have.value', 'harvard')

//     cy.get('[data-test-id="save-settings"]').click({ force: true })
//     cy.wait('@waitForSave')

//     // Check if the settings of the book match with the settings of the default template
//     cy.contains(
//       'Test chapter-processed book belonging to the collection',
//     ).click()

//     cy.contains('Settings').click()
//     cy.contains('testCollection')

//     // Publishing Settings
//     cy.get('[data-test-id="open-access-status-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="openAccessStatus"]').should('have.value', 'false')

//     cy.get('[data-test-id="ukpmc-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="UKPMC"]').should('have.value', 'false')

//     cy.get('[data-test-id="support-multiple-published-versions-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="supportMultiplePublishedVersions"]').should(
//       'have.value',
//       'true',
//     )

//     cy.get('input[name="indexInPubmed"]').should('have.value', 'true')
//     cy.get('input[name="indexChaptersInPubmed"]').should('have.value', 'true')

//     // Landing Page: Links
//     cy.get('input[name="publisherUrl"]').should(
//       'have.value',
//       'http://www.testpublisher.com',
//     )

//     cy.get('input[name="citationSelfUrl"]').should(
//       'have.value',
//       'http://www.testcitation.com',
//     )

//     cy.get('input[name="specialPublisherLinkUrl"]').should(
//       'have.value',
//       'http://www.testspecial.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').should(
//       'have.value',
//       'Test Special Publisher Text',
//     )

//     cy.get('input[name="bookLevelLinks"]').should('have.value', 'false')

//     // Landing Page: Table of Contents
//     cy.get('[data-test-id="group-chapters-into-parts-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="groupChaptersInParts"]').should('have.value', 'true')
//     cy.get('input[name="orderChaptersBy"]').should('have.value', 'title')
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '3')
//     cy.get('input[name="tocExpansionLevel"]').should('have.value', '3')
//     // Display Metadata in TOC
//     cy.get('input[name="tocContributors"]').should('have.value', 'false')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'false')
//     cy.get('input[name="tocSubtitle"]').should('have.value', 'false')
//     cy.get('input[name="tocAltTitle"]').should('have.value', 'true')

//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').should('have.value', 'true')
//     cy.get('input[name="questionAnswerStyle"]').should('have.value', 'faq')

//     cy.get('input[name="displayObjectsLocation"]').should(
//       'have.value',
//       'in-place',
//     )

//     // Display Metadata
//     cy.get('input[name="chapterLevelAffiliationStyle"]').should(
//       'have.value',
//       'by-contrib',
//     )

//     cy.get('input[name="bookLevelAffiliationStyle"]').should(
//       'have.value',
//       'by-contrib',
//     )

//     // Display Reference Citation
//     cy.get('input[name="citationType"]').should('have.value', 'harvard')
//     cy.get('input[name="referenceListStyle"]').should('have.value', 'none')

//     cy.get('input[name="xrefAnchorStyle"]').should(
//       'have.value',
//       'superscripted',
//     )

//     // Display Metadata
//     cy.get('input[name="chapterLevelAffiliationStyle"]').should(
//       'have.value',
//       'by-contrib',
//     )

//     cy.get('input[name="bookLevelAffiliationStyle"]').should(
//       'have.value',
//       'by-contrib',
//     )

//     // Display Reference Citation
//     cy.get('input[name="citationType"]').should(
//       'have.value',
//       'numberedParentheses',
//     )

//     cy.get('input[name="referenceListStyle"]').should('have.value', 'decimal')
//     cy.get('input[name="xrefAnchorStyle"]').should('have.value', 'default')
//   })
// })

// describe('Complete books - Checking when there are custom organisation templates and custom collection templates ', () => {
//   beforeEach(() => {
//     cy.login(admin)

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('getOrganisations')

//     cy.get('[data-test-id="organizations-tab"]').click()
//     cy.wait('@getOrganisations')
//   })

//   it('Create a new whole book ORGANISATION Template', () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     cy.get("div[data-test-id='book-templates-tab']").click()

//     // Create template for completed book
//     cy.get('.select__value-container').click().type('Whole book{Enter}')

//     cy.get('input[name="publisherUrl"]').type(
//       'http://www.testOrgTemplatePub.com',
//     )

//     cy.get('input[name="citationSelfUrl"]').type(
//       'http://www.testOrgTemplateCit.com',
//     )

//     cy.get('input[name="specialPublisherLinkUrl"]').type(
//       'http://www.testOrgTemplateSpPub.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').type(
//       'Test Org Template Special Pub',
//     )

//     cy.get('div[max="4"] > input').clear().type('2')
//     cy.get('div[max="5"] > input').clear().type('4')

//     // Display Content
//     cy.get('[data-test-id="question-answer-style-select"]')
//       .click({ force: true })
//       .type('FAQ with TOC{enter}')

//     cy.get('[data-test-id="reference-list-select"]')
//       .click({ force: true })
//       .type('Blue triangle{enter}')

//     /*
//     cy.contains('Template successfully saved', {
//       timeout: 10000,
//     })
//     */
//   })

//   it('Checking that a new Complete book out of the collection members matches with settings of the ORG templates', () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a complete book out of collection

//     cy.get('button[data-test-id="create-new-book"]').click()
//     cy.get('input[name="title"]').type('Test for ORG wholebook template')

//     cy.contains('span', 'PDF').click({ force: true })
//     cy.contains('span', 'Complete books and documents').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')

//     /*
//         cy.get('input[name="requireApprovalByOrgAdminOrEditor"]').should(
//           'have.value',
//           'true',
//         )
//     */
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '2')
//     cy.get('input[name="tocContributors"]').should('have.value', 'true')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'true')
//     cy.get('[data-test-id="save-settings"]').click({ force: true })
//     cy.wait('@waitForSave')

//     cy.contains('Test for ORG wholebook template').click()

//     // Check if the settings of the book match with the settings of the default template
//     cy.contains('Settings').click()

//     // Publishing Settings
//     cy.get('input[name="indexInPubmed"]').should('have.value', 'false')
//     cy.get('input[name="indexChaptersInPubmed"]').should('have.value', 'false')

//     // Landing Page: Links
//     cy.get('input[name="publisherUrl"]').should(
//       'have.value',
//       'http://www.testOrgTemplatePub.com',
//     )

//     cy.get('input[name="citationSelfUrl"]').should(
//       'have.value',
//       'http://www.testOrgTemplateCit.com',
//     )

//     cy.get('input[name="specialPublisherLinkUrl"]').should(
//       'have.value',
//       'http://www.testOrgTemplateSpPub.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').should(
//       'have.value',
//       'Test Org Template Special Pub',
//     )

//     cy.get('input[name="bookLevelLinks"]').should('have.value', 'false')

//     // Landing Page: Table of Contents
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '2')
//     cy.get('input[name="tocExpansionLevel"]').should('have.value', '4')
//     // Display Metadata in TOC
//     cy.get('input[name="tocContributors"]').should('have.value', 'true')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'true')
//     cy.get('input[name="tocSubtitle"]').should('have.value', 'true')
//     cy.get('input[name="tocAltTitle"]').should('have.value', 'false')

//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').should('have.value', 'false')

//     cy.get('input[name="questionAnswerStyle"]').should(
//       'have.value',
//       'faq-with-toc',
//     )

//     cy.get('input[name="displayObjectsLocation"]').should(
//       'have.value',
//       'at-xref',
//     )

//     // Display Metadata
//     cy.get('input[name="chapterLevelAffiliationStyle"]').should(
//       'have.value',
//       'at-end',
//     )

//     cy.get('input[name="bookLevelAffiliationStyle"]').should(
//       'have.value',
//       'at-end',
//     )

//     // Display Reference Citation
//     cy.get('input[name="referenceListStyle"]').should('have.value', 'decimal')

//     cy.get('input[name="xrefAnchorStyle"]').should('have.value', 'default')
//   })

//   it("Checking that a new Complete book inside the collection matches the settings of the collection members' template settings", () => {
//     cy.contains(organisation.name).click()

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql',
//     }).as('waitForbooks')

//     // Create a complete book in the collection
//     cy.get('button[data-test-id="create-new-book"]').click()

//     cy.get('input[name="title"]').type(
//       'Test wholebook belonging to a collection',
//     )

//     cy.get('[data-test-id="collection-select"]').type('testCollection{enter}')

//     cy.contains('span', 'PDF').click({ force: true })
//     cy.contains('span', 'Complete books and documents').click()
//     cy.get('[data-test-id="next-step"]').click({ force: true })

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('createBook')

//     cy.contains('button', 'Yes, create book').click()
//     cy.wait('@createBook')

//     cy.intercept({
//       method: 'POST',
//       url: 'http://localhost:3000/graphql*',
//     }).as('waitForSave')

//     /*
//       cy.get('[data-test-id="org-admin-or-editor-lock"]')
//         .invoke('attr', 'color')
//         .should('eq', 'black')

//       cy.get('input[name="requireApprovalByOrgAdminOrEditor"]').should(
//         'have.value',
//         'true',
//       )
//       */

//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '3')
//     cy.get('input[name="tocContributors"]').should('have.value', 'false')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'false')
//     cy.get('[data-test-id="save-settings"]').click({ force: true })
//     cy.wait('@waitForSave')

//     cy.contains('Test wholebook belonging to a collection').click()

//     // Check if the settings of the book match with the settings of the default template
//     cy.contains('Settings').click()
//     cy.contains('testCollection')

//     // Publishing Settings
//     cy.get('input[name="requireApprovalByOrgAdminOrEditor"]').should(
//       'have.value',
//       'true',
//     )

//     cy.get('[data-test-id="open-access-status-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="openAccessStatus"]').should('have.value', 'false')

//     cy.get('[data-test-id="ukpmc-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[name="UKPMC"]').should('have.value', 'false')

//     cy.get('input[name="indexInPubmed"]').should('have.value', 'true')
//     cy.get('input[name="indexChaptersInPubmed"]').should('have.value', 'true')

//     // Landing Page: Links
//     cy.get('input[name="publisherUrl"]').should(
//       'have.value',
//       'http://www.testPublisherOneDoc.com',
//     )

//     cy.get('input[name="citationSelfUrl"]').should(
//       'have.value',
//       'http://www.testcitationOneDoc.com',
//     )

//     cy.get('input[name="specialPublisherLinkUrl"]').should(
//       'have.value',
//       'http://www.www.testspecialOneDoc.com',
//     )

//     cy.get('input[name="specialPublisherLinkText"]').should(
//       'have.value',
//       'Test Special Pub One Doc',
//     )

//     // Landing Page: Downloads
//     cy.get('[name="displayBookLevelPdf"]').should('have.value', 'true')
//     cy.get('[name="displayChapterLevelPdf"]').should('have.value', 'false')

//     cy.get('[data-test-id="display-book-level-pdf-for-download-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     cy.get('[data-test-id="display-chapter-level-pdf-for-download-lock"]')
//       .invoke('attr', 'color')
//       .should('eq', 'black')

//     // Landing Page: Table of Contents
//     cy.get('input[name="displayHeadingLevel"]').should('have.value', '3')
//     cy.get('input[name="tocExpansionLevel"]').should('have.value', '3')

//     // Display Metadata in TOC
//     cy.get('input[name="tocContributors"]').should('have.value', 'false')
//     cy.get('input[name="tocChapterLevelDates"]').should('have.value', 'false')
//     cy.get('input[name="tocSubtitle"]').should('have.value', 'false')
//     cy.get('input[name="tocAltTitle"]').should('have.value', 'true')

//     // Display Content
//     cy.get('input[name="footnotesDecimal"]').should('have.value', 'true')
//     cy.get('input[name="questionAnswerStyle"]').should('have.value', 'faq')

//     cy.get('input[name="displayObjectsLocation"]').should(
//       'have.value',
//       'in-place',
//     )

//     // Display Metadata
//     cy.get('input[name="chapterLevelAffiliationStyle"]').should(
//       'have.value',
//       'by-contrib',
//     )

//     cy.get('input[name="bookLevelAffiliationStyle"]').should(
//       'have.value',
//       'by-contrib',
//     )

//     // Display Reference Citation
//     cy.get('input[name="referenceListStyle"]').should('have.value', 'none')

//     cy.get('input[name="xrefAnchorStyle"]').should(
//       'have.value',
//       'superscripted',
//     )

//     // Archiving and Updating
//     cy.get('input[name="createVersionLink"]').should('have.value', 'false')
//   })
// })
