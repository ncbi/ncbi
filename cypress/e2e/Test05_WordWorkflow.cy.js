import {
  admin,
  chapterProcessedBook,
  organisation,
  generalUser,
} from '../support/credentials'
import { aliasQuery } from '../utils/graphql-test-utils'

describe('Testing word workflow', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/seeds/index.js')

    cy.exec(
      `node ./scripts/createOrg.js ${organisation.name} ${chapterProcessedBook.title}`,
    )
  })

  beforeEach(() => {
    cy.intercept('POST', 'http://localhost:3000/graphql', req => {
      // Queries
      aliasQuery(req, 'getDashboard')
      aliasQuery(req, 'getOrganisations')
      aliasQuery(req, 'GetOrganisation')
      aliasQuery(req, 'getBook')
      aliasQuery(req, 'updateBookComponent')
    })

    cy.login(admin)
    cy.wait('@gqlgetDashboardQuery')
    cy.get('[data-test-id="organizations-tab"]').click()
    cy.wait('@gqlgetOrganisationsQuery')
  })

  it('Checking bulk uploads', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookUpload"]').click()

    cy.get('input[type="file"]').attachFile([
      'Type document.docx',
      'Type_of_Document_You_Wish_to_Create.docx',
      'Type$doc.docx',
    ])

    cy.get(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
    ).scrollIntoView()

    cy.get('[data-test-id="Type document.docx"]').scrollIntoView()

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    cy.get('[data-test-id="Type$doc.docx"]').scrollIntoView()

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )
  })

  it('Book metadata modal updates', () => {
    cy.contains('TestOrganisation').click()
    cy.contains('chapterProcessedBook').click()
    cy.get('[data-test-id="bookMetadata"]').click()
    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.get('[name="publisherName"]').scrollIntoView()
    cy.contains('div', 'Publisher name is required')
    cy.contains('div', 'Publisher location is required')
    cy.contains('div', 'Publication date type is required')
    cy.get('[data-test-id="ncbi-custom-metadata"]').scrollIntoView()
    cy.contains('div', 'Book source type is required')

    cy.updateChapterProcessedMetadata(
      true,
      organisation.name,
      chapterProcessedBook.title,
    )
  })

  it('Checking updated metadata of the book', () => {
    cy.checkChapterProcessedMetadata(
      true,
      organisation.name,
      chapterProcessedBook.title,
    )
  })

  it('Checking files tab in book components page', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookUpload"]').click()

    cy.get('[type="file"]').attachFile(
      '/wordwf/chapterProcessed/bgd-biotin.docx',
    )

    cy.get('[data-test-id="book-uplaod"]').click()
    cy.get('[class*=Status__Root]').click()

    cy.log('Adding files to review tab')

    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()
    cy.get('[data-test-id="file-input-feedback"]').attachFile('ack.docx')
    cy.get('[data-test-id="send-feedback"]').click()
    cy.get('[data-test-id="files-tab"]').click()

    cy.log('Source')

    cy.get('[data-test-id="upload-source-input"]').attachFile(
      'Type_of_Document_You_Wish_to_Create.docx',
    )

    cy.get(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
    ).scrollIntoView()

    cy.contains(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
      'Filename does not match previous version (bgd-biotin.docx)',
    )

    cy.get('[data-test-id="remove-btn"]').click()
    cy.contains('Source').click()

    cy.log('converted')

    cy.get('[data-test-id="upload-converted-input"]').attachFile(
      'Type_of_Document_You_Wish_to_Create.docx',
    )

    cy.get(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
    ).scrollIntoView()

    cy.contains(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
      'Filename should be bgd-biotin.bxml or bgd-biotin.xml',
    )

    cy.get('[data-test-id="remove-btn"]').click()

    cy.contains('Converted').click()

    cy.contains('Supplementary').click()
    cy.log('supplementary')
    cy.get('[data-test-id="upload-supplementary-input"]').scrollIntoView()

    cy.get('[data-test-id="upload-supplementary-input"]').attachFile([
      'Type document.docx',
      'Type$doc.docx',
    ])

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.get('[data-test-id="remove-btn"]').click()

    cy.contains('Supplementary').click()
    cy.contains('Review').click()
    cy.contains('div', 'ack.docx')
  })

  it('Book settings modal update', () => {
    cy.updateChapterProcessedSettings(
      true,
      organisation.name,
      chapterProcessedBook.title,
    )
  })

  it('Checking if settings values are retained', () => {
    cy.checkChapterProcessedSettings(
      true,
      organisation.name,
      chapterProcessedBook.title,
    )
  })

  it('Adding part', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlgetBookQuery')
    cy.get('[data-test-id="addBookPart"]').click()
    cy.get('[data-test-id="title-editor"] div.ProseMirror').type('Part32')
    cy.get('[data-test-id="add-metadata.editor"]').click()
    cy.get('[name="metadata.editor[0].surname"]').type('doe')
    cy.get('[name="metadata.editor[0].givenName"]').type('john')
    cy.get('[name="metadata.editor[0].suffix"]').type('1010')
    cy.get('[data-test-id="save-add-part"]').click()
    cy.wait('@gqlGetOrganisationQuery')
    cy.contains('div', 'Part32')
  })

  it('checking if values are retained part data', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.get('[title="Part32"] span a').click({ force: true })
    cy.contains('[data-test-id="title-editor"] .ProseMirror', 'Part32')

    cy.get('[name="metadata.editor[0].surname"]').should('have.value', 'doe')

    cy.get('[name="metadata.editor[0].givenName"]').should('have.value', 'john')

    cy.get('[name="metadata.editor[0].suffix"]').should('have.value', '1010')

    // No contributors added yet
  })

  it('updating parts', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlgetBookQuery')
    // cy.contains('div', 'Part32').click()
    cy.get('[title="Part32"] span a').click({ force: true })
    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(1000)

    cy.get('[data-test-id="title-editor"] div.ProseMirror').type(
      `{backspace}{backspace}45`,
    )

    cy.get('[name="metadata.editor[0].surname"]').clear().type('williams')
    cy.get('[name="metadata.editor[0].givenName"]').clear().type('serina')
    cy.get('[name="metadata.editor[0].suffix"]').clear().type('1010')
    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.wait('@gqlupdateBookComponentQuery')
    cy.wait('@gqlGetOrganisationQuery')
  })

  it('checking updated file and deleting contributors', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    // cy.contains('div', 'Part45').click({ force: true })
    cy.get('[title="Part45"] span a').click({ force: true })

    cy.get('[name="metadata.editor[0].surname"]').should(
      'have.value',
      'williams',
    )

    cy.get('[name="metadata.editor[0].givenName"]').should(
      'have.value',
      'serina',
    )

    cy.get('[name="metadata.editor[0].suffix"]').should('have.value', '1010')

    cy.get('[data-test-id="delete-1"]').click()

    cy.contains('No contributors added yet')
    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.wait('@gqlupdateBookComponentQuery')
    cy.wait('@gqlGetOrganisationQuery')
  })

  it('uploading converted file and checking metadata', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlgetBookQuery')
    cy.get('[class*=Status__Root]').click({ force: true })
    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile(
      '/wordwf/chapterProcessed/bgd-biotin.xml',
    )

    cy.get('[data-test-id="save-btn"]').click()

    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="files-tab"]').click()
    cy.reload()
    cy.get('[data-test-id="metadata-tab"]').click()

    cy.get('[name="title"]')
      .should(
        'have.value',
        'Biotin-Thiamine-Responsive Basal Ganglia Disease edit and upload converted file',
      )
      .should('be.disabled')

    cy.get('[name="subTitle"]')
      .should('have.value', 'Biotene-thiamine')
      .should('be.disabled')

    cy.get('[name="altTitle"]')
      .should(
        'have.value',
        'Synonyms: Biotin-Responsive Basal Ganglia Disease (BBGD), BTBGD, Thiamine Metabolism Dysfunction Syndrome-2, Thiamine Transporter-2 Deficiency',
      )
      .should('be.disabled')

    cy.get('[name="label"]')
      .should('have.value', 'biotin-label')
      .should('be.disabled')

    cy.get('[name="chapterId"]')
      .should('have.value', 'bgd-biotin-NoAbHead')
      .should('be.disabled')
    // cy.get('[name="bcmsId"]').should('not.be.empty')

    cy.get('[name="language"]').should('have.value', 'en').should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="MM"]')
      .should('have.value', '11')
      .should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="DD"]')
      .should('have.value', '09')
      .should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="YYYY"]')
      .should('have.value', '2020')
      .should('be.disabled')

    cy.get('[data-test-id="date-created"] input[placeholder="MM"]')
      .should('have.value', '11')
      .should('be.disabled')

    cy.get('[data-test-id="date-created"] input[placeholder="DD"]')
      .should('have.value', '21')
      .should('be.disabled')

    cy.get('[data-test-id="date-created"] input[placeholder="YYYY"]')
      .should('have.value', '2013')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="MM"]')
      .should('have.value', '4')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="DD"]')
      .should('have.value', '16')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="YYYY"]')
      .should('have.value', '2020')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="MM"]')
      .should('have.value', '8')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="DD"]')
      .should('have.value', '20')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="YYYY"]')
      .should('have.value', '2020')
      .should('be.disabled')

    cy.get('[name="authors[0].surname"]')
      .should('have.value', 'Tabarki')
      .should('be.disabled')

    cy.get('[name="authors[0].givenName"]')
      .should('have.value', 'Brahim')
      .should('be.disabled')

    cy.get('[name="authors[1].surname"]')
      .should('have.value', 'Al-Hashem')
      .should('be.disabled')

    cy.get('[name="authors[1].givenName"]')
      .should('have.value', 'Amal')
      .should('be.disabled')

    cy.get('[name="authors[2].surname"]')
      .should('have.value', 'Alfadhel')
      .should('be.disabled')

    cy.get('[name="authors[2].givenName"]')
      .should('have.value', 'Majid')
      .should('be.disabled')
  })

  /* eslint-disable-next-line jest/no-disabled-tests */
  it('checking if files are uploaded into their sections properly', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlgetBookQuery')
    cy.get('[data-test-id="bookUpload"]').click()

    cy.get('[type="file"]').attachFile([
      '/wordwf/chapterProcessed/frontmatterpart.docx',
      '/wordwf/chapterProcessed/glossary.docx',
      '/wordwf/chapterProcessed/bgd-biotin.docx',
    ])

    cy.get('[data-test-id="book-uplaod"]').click()
    cy.get('[data-test-id="modalBody"]').should('not.exist')
    // cy.get('[data-test-id="close-modal"]').click()

    cy.wait('@gqlGetOrganisationQuery')

    cy.get('[title="Untitled"] span a:nth(1)').click({ force: true })

    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile(
      '/wordwf/chapterProcessed/frontmatterpart.xml',
    )

    cy.get('[data-test-id="save-btn"]').click()
    cy.get('[data-test-id="back-link"]').click()

    cy.get(
      '[title="Biotin-Thiamine-Responsive Basal Ganglia Disease edit and upload converted file"] span a',
    ).click({ force: true })

    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile(
      '/wordwf/chapterProcessed/bgd-biotin.xml',
    )

    cy.get('[data-test-id="save-btn"]').click()
    cy.get('[data-test-id="back-link"]').click()
    cy.get('[title="Untitled"] span a').click({ force: true })
    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile(
      '/wordwf/chapterProcessed/glossary.xml',
    )

    cy.get('[data-test-id="save-btn"]').click()
    cy.get('[data-test-id="back-link"]').click()

    cy.get('[data-test-id="-tab"]')
      .first()
      .should('have.text', 'front Matter')
      .click()

    cy.get('[title="Frontmatter Part Test"]').should('exist')

    cy.get('[data-test-id="-tab"]:nth(1)').should('have.text', 'body').click()
    cy.reload()

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(3000)

    cy.get(
      '[title="Biotin-Thiamine-Responsive Basal Ganglia Disease edit and upload converted file"]',
    ).should('exist')

    cy.get('[data-test-id="-tab"]')
      .last()
      .should('have.text', 'back Matter')
      .click()

    cy.get('[title="Glossary Test"]').should('exist')
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('checking versions in supplementary files', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlgetBookQuery')

    cy.get(
      '[title="Biotin-Thiamine-Responsive Basal Ganglia Disease edit and upload converted file"] span a',
    ).click({ force: true })

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(1000)

    cy.get('[data-test-id="files-tab"]').click()
    cy.contains('span', 'Source').click()
    cy.contains('span', 'Converted').click()

    cy.checkFileSection(
      'Supplementary',
      'wordwf/chapterProcessed/bgd-biotin.docx',
    )
  })
})

describe('User tests', () => {
  let componentUrl = ''

  beforeEach(() => {
    cy.intercept('POST', 'http://localhost:3000/graphql', req => {
      // Queries
      aliasQuery(req, 'GetOrganisation')
      aliasQuery(req, 'getBook')
      aliasQuery(req, 'getOrganisations')
      aliasQuery(req, 'getDashboard')
      aliasQuery(req, 'VerifyUser')
      aliasQuery(req, 'OrganisationUsers')
      aliasQuery(req, 'updateBookTeamMembers')
    })
  })

  it('creating a new user', () => {
    cy.addUser(generalUser)
  })

  it('user request access', () => {
    cy.login(generalUser)
    cy.get('[data-test-id="org-access-btn"]').click({ force: true })
    cy.contains('div', 'Organization Access')
    cy.get('.select__control ').type(`${organisation.name} {enter}`)
    cy.get('[data-test-id="request-access"]').click()
    cy.contains('div', 'Request Submitted')
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('accepting the user', () => {
    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
    cy.wait('@gqlgetOrganisationsQuery')
    cy.contains(organisation.name).click()
    cy.wait('@gqlgetDashboardQuery')
    cy.get('[data-test-id="users-tab"]').click()
    cy.contains('div', generalUser.username).click()
    cy.get('[data-test-id="accept-user-modal"]').click()
    cy.wait('@gqlVerifyUserQuery')
    cy.get('[data-test-id="saveUserModal"]').click()
    cy.wait('@gqlOrganisationUsersQuery')
    cy.contains('button', 'Cancel').click()
    cy.contains('div', generalUser.username).click()
    cy.get('input[name="editor"]').click({ force: true })
    cy.get('[data-test-id="saveUserModal"]').click()
    cy.wait('@gqlOrganisationUsersQuery')
    cy.contains('div', `User ${generalUser.givenName} modified succesfully`)
    cy.contains('button', 'Cancel').click()
    cy.get('[data-test-id="books-and collections-tab"]').click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlgetBookQuery')

    cy.get(
      '[title="Biotin-Thiamine-Responsive Basal Ganglia Disease edit and upload converted file"] span a',
    ).click({ force: true })

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(2000)
    cy.get('[data-test-id="team-tab" ]').click()
    cy.get('.select__control ').type(`${generalUser.givenName} {enter}`)
    cy.contains('button', 'Add Member').click({ force: true })
    cy.get('[data-test-id="save-component-team"]').click()
    cy.wait('@gqlupdateBookTeamMembersQuery')
    cy.get('[data-test-id="files-tab"]').click()

    cy.url().then(urlString => {
      componentUrl = urlString
    })
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('checking book component as an editor of the component', () => {
    cy.visit(componentUrl)
    cy.get('input[name="username"]').type(generalUser.username)
    cy.get('input[name="password"]').type(generalUser.password)
    cy.get('button[type="submit"]').click()
    // cy.get('[data-test-id="preview-tab"]').should('exist')
    cy.get('[data-test-id="team-tab"]').should('exist')
    cy.get('[data-test-id="files-tab"]').should('exist')
    cy.get('[data-test-id="files-tab"]').click({ force: true })
    cy.contains('a', 'bgd-biotin.docx')
    cy.get('[data-test-id="metadata-tab"]').should('exist')
    cy.get('[data-test-id="errors-tab"]').should('exist')
    cy.get('[data-test-id="back-link"]').click()
    // cy.contains('You are not assigned any role yet')
    cy.contains(chapterProcessedBook.title)
    cy.contains('front Matter')
    cy.contains('body')
    cy.contains('back Matter')
    cy.get('[data-test-id="bookMetadata"]').should('exist')
    cy.get('[data-test-id="bookSettings"]').should('exist')
    cy.get('[data-test-id="book-toc"]').should('exist')
  })

  it('adding user to the book team', () => {
    cy.login(admin)
    cy.contains(organisation.name)
    cy.contains(chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookTeams"]').click()
    cy.get('.select__control ').type(`${generalUser.givenName} {enter}`)
    cy.contains('button', 'Add Member').click()
    cy.get('[data-test-id="save-book-team"]').click()

    cy.login(generalUser)
    cy.contains('div', organisation.name).click()
    cy.contains('div', chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookMetadata"]').should('exist')
    cy.get('[data-test-id="bookSettings"]').should('exist')
    cy.get('[data-test-id="bookTeams"]').should('exist')
    cy.get('[data-test-id="book-toc"]').should('exist')
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('Errors tab and help pannel', () => {
    cy.login(admin)
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlgetBookQuery')
    cy.wait('@gqlgetBookQuery')

    cy.get(
      '[title="Biotin-Thiamine-Responsive Basal Ganglia Disease edit and upload converted file"] span a',
    ).click({ force: true })

    cy.get('[data-test-id="errors-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()
    cy.get('.mentions__input').type(`hi @${generalUser.givenName}{enter}`)
    cy.get('[data-test-id="send-feedback"]').click()
    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()
    cy.get('.mentions__input').type(`hi @${generalUser.givenName}{enter}`)
    cy.get('[data-test-id="send-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="author-name"]',
      admin.username.charAt(0).toUpperCase() + admin.username.slice(1),
    )

    cy.login(generalUser)
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlgetBookQuery')

    cy.get(
      '[title="Biotin-Thiamine-Responsive Basal Ganglia Disease edit and upload converted file"] span a',
    ).click({ force: true })

    cy.get('[data-test-id="errors-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="message-content"]',
      `hi @${generalUser.givenName} ${generalUser.surname}`,
    )

    cy.get('[data-test-id="send-feedback"]').click()
    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="message-content"]',
      `hi @${generalUser.givenName} ${generalUser.surname}`,
    )
  })
})
