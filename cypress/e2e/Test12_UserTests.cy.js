import {
  admin,
  chapterProcessedBook,
  organisation,
  generalUser,
} from '../support/credentials'

describe('User permissions', () => {
  let componentUrl = ''

  before(() => {
    cy.exec('node ./scripts/truncateDB.js')

    cy.exec('node ./scripts/seeds/index.js')

    cy.exec(
      `node ./scripts/createOrg.js ${organisation.name} ${chapterProcessedBook.title}`,
    )
  })

  it('Register a user', () => {
    cy.addUser(generalUser)
  })

  it('Request organisation', () => {
    cy.login(generalUser)
    cy.get('[data-test-id="org-access-btn"]').click({ force: true })
    cy.contains('div', 'Organization Access')
    cy.get('.select__control ').type(`${organisation.name} {enter}`)
    // cy.contains('.select__option ', organisation.name)
    cy.get('[data-test-id="request-access"]').click()
    cy.contains('div', 'Request Submitted')
    // cy.get('[data-test-id="org-access-btn"]').click({ force: true })
    // cy.get('.select__control').type(organisation2.name)
    // cy.contains('div', 'No options')
  })

  it('accepting user into the org', () => {
    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
    cy.contains(organisation.name).click()
    cy.get('[data-test-id="users-tab"]').click()
    cy.contains('div', generalUser.username).click()

    cy.intercept({ method: 'POST', url: 'http://localhost:3000/graphql' }).as(
      'waitForReq',
    )

    cy.get('[data-test-id="accept-user-modal"]').click()
    cy.wait('@waitForReq')
  })

  it('user without role permissions ', () => {
    cy.login(generalUser)
    cy.get('[data-test-id="books-and collections-tab"]').should('exist')
    cy.contains('div', organisation.name).click()
    cy.get('[data-test-id="create-new-book"]').should('not.exist')
    cy.get('[data-test-id="users-tab"]').should('not.exist')
    cy.get('[data-test-id="create-new-collection"]').should('not.exist')
    cy.contains('div', chapterProcessedBook.title).click()
    cy.contains('div', "You don't have sufficient permissions")
    cy.get('[data-test-id="bookMetadata"]').should('not.exist')
    cy.get('[data-test-id="bookSettings"]').should('not.exist')
    cy.get('[data-test-id="bookTeams"]').should('not.exist')
  })

  it('updating user privelage', () => {
    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
    cy.contains(organisation.name).click()
    cy.get('[data-test-id="users-tab"]').click()
    cy.contains('div', generalUser.username).click()
    cy.get('input[name="editor"]').click({ force: true })
    cy.get('[data-test-id="saveUserModal"]').click()
    cy.contains('div', 'Success')
    cy.contains('div', `User ${generalUser.givenName} modified succesfully`)
  })

  it('user with editor privelages', () => {
    cy.login(generalUser)
    cy.get('[data-test-id="create-new-collection"]').should('not.exist')
    cy.get('[data-test-id="create-new-book"]').should('exist')
    cy.get('[data-test-id="books-and collections-tab"]').should('exist')
    cy.get('[data-test-id="create-new-collection"]').should('not.exist')
    cy.get('[data-test-id="settings-tab"]').should('exist')
    cy.get('[data-test-id="book-templates-tab"]').should('not.exist')
    cy.get('[data-test-id="metadata-content-tab"]').should('not.exist')
    cy.get('[data-test-id="users-tab"]').should('exist')
    // cy.contains(collection.title).click()
    // cy.contains('You do not have sufficient permissions to access this page')
  })

  it('making an editor a member of the book', () => {
    cy.login(admin)
    // cy.contains('div', organisation.name).click()
    cy.contains('div', chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookTeams"]').click()
    cy.get('.select__control ').type(`${generalUser.givenName} {enter}`)
    cy.contains('button', 'Add Member').click()
    cy.get('[data-test-id="save-book-team"]').click()
  })

  it('loging in as member of the book', () => {
    cy.login(generalUser)
    cy.contains('div', organisation.name).click()
    cy.contains('div', chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookMetadata"]').should('exist')
    cy.get('[data-test-id="bookSettings"]').should('exist')
    cy.get('[data-test-id="bookTeams"]').should('exist')
    cy.get('[data-test-id="book-toc"]').should('exist')
  })

  it('creating a book component', () => {
    cy.intercept({ method: 'POST', url: 'http://localhost:3000/graphql' }).as(
      'waitForFetch',
    )

    cy.login(admin)
    cy.wait('@waitForFetch')
    cy.get('[data-test-id="organizations-tab"]').click()
    cy.contains('div', organisation.name).click()
    cy.contains('div', chapterProcessedBook.title).click()

    //  removing a book editor
    cy.log(' removing a book editor ')
    cy.get('[data-test-id="bookTeams"]').click()
    cy.get(`[data-test-id="remove-${generalUser.username}"]`).click()

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForTeam')

    cy.get('[data-test-id="save-book-team"]').click()
    cy.wait('@waitForTeam')
    cy.get('[data-test-id="cancel-book-metadata"]').click()
    //

    // creating a component
    cy.log(' creating a component ')
    cy.get('[data-test-id="bookUpload"]').click()
    cy.get('input[type="file"]').first().attachFile(['introduction.docx'])
    cy.get('[data-test-id="book-uplaod"]').click()

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForTeam')

    cy.contains('div', 'New Upload').click()
    cy.wait('@waitForTeam')
    //

    // adding an editor to the component
    cy.log(' adding an editor to the component ')
    cy.get('[data-test-id="team-tab"]').click()
    cy.get('.select__control ').type(`${generalUser.givenName} {enter}`)
    cy.contains('button', 'Add Member').click()

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForCompTeam')

    cy.get('[data-test-id="save-component-team"]').click()
    cy.wait('@waitForCompTeam')
    cy.get('[data-test-id="files-tab"]').click()

    cy.url().then(urlString => {
      componentUrl = urlString
    })
  })

  it('checking book component as an editor for the component', () => {
    cy.visit(componentUrl)
    cy.get('input[name="username"]').type(generalUser.username)
    cy.get('input[name="password"]').type(generalUser.password)
    cy.get('button[type="submit"]').click()
    cy.get('[data-test-id="preview-tab"]').should('exist')
    cy.get('[data-test-id="team-tab"]').should('exist')
    cy.get('[data-test-id="files-tab"]').should('exist')
    cy.get('[data-test-id="files-tab"]').click()
    cy.contains('a', 'introduction.docx')
    cy.get('[data-test-id="metadata-tab"]').should('exist')
    cy.get('[data-test-id="errors-tab"]').should('exist')
    cy.get('[data-test-id="back-link"]').click()
    cy.contains(chapterProcessedBook.title)
    cy.contains('front Matter')
    cy.contains('body')
    cy.contains('back Matter')
    cy.get('[data-test-id="bookMetadata"]').should('exist')
    cy.get('[data-test-id="bookSettings"]').should('exist')
    cy.get('[data-test-id="book-toc"]').should('exist')
  })
})
