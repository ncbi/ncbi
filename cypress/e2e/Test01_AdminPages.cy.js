/* eslint-disable jest/no-commented-out-tests */
const {
  admin,
  organisation,
  organisation2,
  workflows,
  completeBook,
  chapterProcessedBook,
  collection,
} = require('../support/credentials')

describe('System Admin pages (Organisation)', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/seeds/index.js')
  })

  const wfIndex1 = Math.floor(Math.random() * (2 - 0 + 1)) + 0
  let wfIndex2 = wfIndex1

  if ((wfIndex1 >= 1 && wfIndex1 < 2) || wfIndex1 === 0) {
    wfIndex2 = wfIndex1 + 1
  } else if (wfIndex1 > 0 && wfIndex1 <= 2) {
    wfIndex2 = wfIndex1 - 1
  }

  beforeEach(() => {
    cy.login(admin)

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('getOrganisations')

    cy.get('[data-test-id="organizations-tab"]').click()
    cy.wait('@getOrganisations')
  })

  it('validating create organisation', () => {
    cy.get('button[data-test-id="create-organisation"]').click()
    cy.get('input[name="name"]').focus().blur()
    cy.get('input[name="abbreviation"]').focus().blur()
    cy.get('input[name="settings.type.funder"]').should('have.value', 'false')

    cy.get('input[name="settings.type.publisher"]').should(
      'have.value',
      'false',
    )

    cy.get('button[data-test-id="save-new-organisation"]').click()
    cy.contains('Name is required')
    cy.contains('Abbreviated name is required')
  })

  it('Creating a new organisation ', () => {
    cy.addOrganisation(organisation, [workflows[wfIndex1], workflows[wfIndex2]])
  })

  it('Searching a organisation ', () => {
    cy.addOrganisation(organisation2)
    cy.get('[placeholder="Search"]').type(organisation2.name)
    cy.contains(organisation2.name)
    cy.contains(organisation.name).should('not.exist')
  })

  it('Updating organisation settings', () => {
    cy.contains(organisation.name).click()
    cy.contains('div', 'Settings').click()
    cy.get('input[name="settings.collections.funded"]').click({ force: true })

    cy.get('input[name="settings.collections.bookSeries"]').click({
      force: true,
    })

    cy.get('input[name="settings.type.funder"]').click({ force: true })
    cy.get('input[name="settings.type.publisher"]').click({ force: true })

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForSave')

    cy.get('[data-test-id="save-edit-organisation"]').click({ force: true })
    cy.wait('@waitForSave')
    cy.contains('.notification-title', 'Success')

    cy.contains(
      '.notification-container-top-right',
      'Organization was updated succesfully',
    )
  })

  it('Checking the organisation settings are retained', () => {
    cy.contains(organisation.name).click()
    cy.contains('div', 'Settings').click()
    cy.get(`[${workflows[wfIndex1].selector}]`).should('have.value', 'true')

    cy.get('input[name="settings.collections.bookSeries"]').should(
      'have.value',
      'true',
    )

    cy.get(`input[${workflows[wfIndex2].selector}]`).should(
      'have.value',
      'true',
    )

    cy.get('input[name="settings.type.publisher"]').should('have.value', 'true')
  })

  // it("Select and Delete all",()=>{
  //   cy.get('div[data-test-id="select-all"]').click()
  //   cy.get('[data-test-id="bulk-delete-organisations"]').click()
  //   cy.contains("h4","Are you sure you want to delete organizations?");
  //   cy.get('[data-test-id="confirmation-confirm"]').click();
  //   cy.contains(".notification-title","Success")
  //   cy.contains(".notification-container-top-right","Organizations were deleted");
  // });

  it('validating create book form', () => {
    cy.contains(organisation.name).click()
    cy.get('[data-test-id="create-new-book"]').click()

    cy.get('[name="title"]').focus().blur()
    cy.contains('div', 'Title is required')

    if (
      workflows[wfIndex1].name !== 'Word' &&
      workflows[wfIndex2].name !== 'Word'
    ) {
      cy.contains('span', 'Word').should('have.attr', 'disabled')
    } else {
      cy.contains('span', 'Word').should('not.have.attr', 'disabled')
    }

    if (
      workflows[wfIndex1].name !== 'PDF' &&
      workflows[wfIndex2].name !== 'PDF'
    ) {
      cy.contains('span', 'PDF').should('have.attr', 'disabled')
    } else {
      cy.contains('span', 'PDF').should('not.have.attr', 'disabled')
    }

    if (
      workflows[wfIndex1].name !== 'XML' &&
      workflows[wfIndex2].name !== 'XML'
    ) {
      cy.contains('span', 'XML').should('have.attr', 'disabled')
    } else {
      cy.contains('span', 'XML').should('not.have.attr', 'disabled')
    }

    cy.contains('button', 'Next').click()
    cy.contains('div', 'Conversion workflow is required')
    cy.contains('div', 'Submission type is required')
  })

  it('creating a complete book', () => {
    cy.contains(organisation.name).click()

    cy.addBook(
      completeBook.title,
      workflows[wfIndex1].name,
      'Complete books and documents',
    )
  })

  it('creating a chapter processed book', () => {
    cy.contains(organisation.name).click()

    cy.addBook(
      chapterProcessedBook.title,
      workflows[wfIndex2].name,
      'Individual chapters',
    )
  })

  it('validating custom collection', () => {
    cy.contains(organisation.name).click()
    cy.get('button[data-test-id="create-new-collection"]').click()
    cy.get('[data-test-id="collection-type"]').click()
    cy.get('.ProseMirror').click()
    cy.contains('div', 'Collection type is required')
    cy.get('[name="publisherName"]').focus().blur()
    cy.contains('div', 'Publisher name is required')
    cy.get('[name="publisherLocation"]').focus().blur()
    cy.get('[data-test-id="pubdate-type-select"]').click()
    cy.get('[data-test-id="wholeBookSourceType-select"]').click()

    cy.get(
      '[data-test-id="chapterProcessedSourceType-select"]',
    ).scrollIntoView()

    cy.get('input[data-test-id="start-month"]').focus().blur()
    cy.contains('div', 'Date of publication is required')
    cy.get('[data-test-id="chapterProcessedSourceType-select"]').click()
    cy.contains('button', 'Next').click({ force: true })
    cy.contains('div', 'Publication date type is required')
    cy.contains('div', 'Whole book source type is required')
    cy.contains('div', 'Chapter processed source type is required')
    cy.get('[data-test-id="collection-type"]').scrollIntoView()
    cy.contains('div', 'Title is required')
  })

  it('create a collection', () => {
    cy.contains(organisation.name).click()

    cy.addCollection(
      collection.title,
      collection.publisher,
      collection.location,
    )
  })

  it('adding book to a collection', () => {
    cy.contains(organisation.name).click()
    cy.get('[data-test-id="books-and collections-tab"]').click()
    cy.contains(chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookSettings"]').click()

    cy.get('[data-test-id="collection-select"]').type(
      `${collection.title} {enter}`,
    )

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('updateSettings')

    cy.get('[data-test-id="save-book-settings"]').click()
    cy.wait('@updateSettings')
    cy.contains('div', chapterProcessedBook.title)
  })

  it('search and filtering books', () => {
    cy.contains(organisation.name).click()
    cy.contains('div', 'Books and collections').click()
    cy.get('[placeholder="Search"]').type(completeBook.title)
    cy.get('[data-test-id="show-filter-checkbox"]').click()

    cy.get('[data-test-id="source-select"]').type(
      `${workflows[wfIndex1].name}{enter}`,
    )

    cy.get('[data-test-id="type-select"]').type('Book{enter}')
    cy.get('[data-test-id="status-select"]').type('New Book{enter}')
    cy.get(`${workflows[wfIndex2].name.toLowerCase()}`).should('not.exist')
    cy.get('[data-test-id="collection"]').should('not.exist')
    // cy.contains(completeBook.title)

    cy.get(
      `div[aria-rowindex="1"] div[aria-colindex="2"] div[data-test-id="${workflows[
        wfIndex1
      ].name.toLowerCase()}"]`,
    ).should('exist')

    cy.contains(
      `div[aria-rowindex="1"] div[aria-colindex="4"]`,
      completeBook.title,
    )

    cy.get(
      `div[aria-rowindex="1"] div[aria-colindex="4"] div[data-test-id="${organisation.name}"]`,
    ).should('exist')

    cy.contains('div[aria-rowindex="1"] div[aria-colindex="7"]', 'New Book')

    cy.get('[data-test-id="show-filter-checkbox"]').click()
    cy.get('[data-test-id="show-filter-checkbox"]').click()
    cy.get('[placeholder="Search"]').clear().type('{enter}')

    cy.get('[data-test-id="type-select"]').type('Collection{enter}')

    cy.contains(
      `div[aria-rowindex="1"] div[aria-colindex="4"]`,
      collection.title,
    )
  })

  it('collection settings', () => {
    cy.contains(organisation.name).click()
    cy.contains(collection.title).click()
    cy.get('[data-test-id="collectionSettings"]').click()
    cy.get('[name="publisherUrl"]').clear().type('https://www.google.com/')
    cy.get('[name="citationSelfUrl"]').clear().type('https://www.yahoo.com/')
    cy.get('[name="groupBooksOnTOC"]').click({ force: true })
    cy.get('[data-test-id="group-books-by"]').click()
    cy.contains('div', 'Custom Group Titles').click()
    cy.get('[data-test-id="order-books-by"]').type(`Title {enter}`)
    cy.get('input[name="tocContributors"]').click({ force: true })
    cy.get('[name="tocFullBookCitations"]').click({ force: true })

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForSave')

    cy.get('[data-test-id="save-collection-settings"]').click()
    cy.wait('@waitForSave')
  })

  it('checking if the collection settings values are retained', () => {
    cy.contains(organisation.name).click()

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForbooks')

    cy.contains(collection.title).click()
    cy.wait('@waitForbooks')
    cy.get('[data-test-id="collectionSettings"]').click()

    cy.get('[name="publisherUrl"]').should(
      'have.value',
      'https://www.google.com/',
    )

    cy.get('[name="citationSelfUrl"]').should(
      'have.value',
      'https://www.yahoo.com/',
    )

    cy.get('[name="groupBooksOnTOC"]').should('have.value', 'true')
    cy.get('[data-test-id="group-books-by"]').contains('Custom Group Titles')
    cy.get('[data-test-id="order-books-by"]').contains('Title — A–Z')
    cy.get('input[name="tocContributors"]').should('have.value', 'true')
    cy.get('[name="tocFullBookCitations"]').should('have.value', 'true')
    cy.contains('button', 'Save').click({ force: true })
  })

  it('Testing custom groups', () => {
    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForCol')

    cy.contains(organisation.name).click()
    cy.wait('@waitForCol')
    cy.contains(collection.title).click()
    cy.get('[data-test-id="addGroup"]').click({ force: true })
    cy.get('input[name="title"]').type(`group1`)
    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.contains('div', 'Success')

    cy.get(`[data-test-id="checkbox-${chapterProcessedBook.title}"]`).click({
      force: true,
    })

    cy.get('[data-test-id="move-chapters-to"]').click()
    cy.contains('Select...').type(`group1 {enter}`)
    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.contains('div', chapterProcessedBook.title)

    cy.get('[data-test-id="collapse-icon"]').click({
      multiple: true,
    })

    cy.contains(chapterProcessedBook.title).should('not.exist')
  })

  it('Checking navigation on organisation, collection and book pages', () => {
    // Navigation book-org page
    cy.contains(organisation.name).click()
    cy.get('.AppBar__Item-sc-1tmi0jv-4').contains('TestOrganisation')
    cy.contains(completeBook.title).click()

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('getOrganisationPage')

    cy.get('.BackLink__StyledLink-rm04mb-0 > :nth-child(2)')
      .contains('Back to TestOrganisation')
      .click()

    cy.wait('@getOrganisationPage')

    // Navigation org page-collection-book
    cy.contains(collection.title).click()

    cy.get('.BackLink__StyledLink-rm04mb-0 > :nth-child(2)').contains(
      'Back to TestOrganisation',
    )

    cy.contains(chapterProcessedBook.title).click()

    cy.get('.BackLink__StyledLink-rm04mb-0 > :nth-child(2)')
      .contains('Back to testCollection')
      .click()

    cy.get('div[data-test-id="testCollection"]')

    cy.get('.BackLink__StyledLink-rm04mb-0 > :nth-child(2)')
      .contains('Back to TestOrganisation')
      .click()
  })

  it('creating book 1', () => {
    cy.contains(organisation.name).click()

    cy.addBook(
      'Book123w12',
      workflows[wfIndex2].name,
      'Complete books and documents',
      'Final full-text',
      collection.title,
    )
  })

  it('creating book 2', () => {
    cy.contains(organisation.name).click()

    cy.addBook(
      'Book312ad4',
      workflows[wfIndex1].name,
      'Individual chapters',
      'Final full-text',
      collection.title,
    )
  })

  it('creating book 3', () => {
    cy.contains(organisation.name).click()

    cy.addBook(
      'Book2ddr42',
      workflows[wfIndex1].name,
      'Individual chapters',
      'Final full-text',
      collection.title,
    )
  })

  it('Order by Title', () => {
    cy.contains(organisation.name).click()
    cy.contains(collection.title).click()
    cy.contains('Settings').click()

    cy.get(
      ':nth-child(3) > :nth-child(3) > .Select__Wrapper-figd2c-0 > .Select__SelectWrapper-figd2c-1 > .css-2b097c-container > .select__control > .select__value-container',
    ).type('Title{enter}')

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForSave')

    cy.get('[data-test-id="save-collection-settings"]').click()
    cy.wait('@waitForSave')

    cy.contains('div', 'Cancel').click()
  })

  it('Validate Order by Title', () => {
    cy.contains(organisation.name).click()
    cy.contains(collection.title).click()

    cy.get('.CollectionRow__BookComponentStyled-sc-1wctinz-1:nth(0)').contains(
      'Book1',
    )

    cy.get('.CollectionRow__BookComponentStyled-sc-1wctinz-1:nth(1)').contains(
      'Book2',
    )

    cy.get('.CollectionRow__BookComponentStyled-sc-1wctinz-1:nth(2)').contains(
      'Book3',
    )
  })
})
