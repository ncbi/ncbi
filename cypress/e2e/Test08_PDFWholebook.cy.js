import {
  admin,
  completeBook,
  organisation,
  workflows,
  seededUser,
} from '../support/credentials'

describe('Test for PDF wholebooks', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/seeds/index.js')
    cy.exec(`node ./scripts/createOrg.js ${organisation.name}`)

    cy.exec(`node ./scripts/seedUser.js create seededUser`)
  })

  beforeEach(() => {
    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
  })

  it('Creating a PDF complete book', () => {
    cy.contains(organisation.name).click()

    cy.addBook(
      completeBook.title,
      workflows[0].name,
      'Complete books and documents',
    )
  })

  it('checking files tab in book manager', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()
    cy.get('[data-test-id="files-tab"]').click()

    cy.log('Source')

    cy.get('[data-test-id="upload-source-input"]').attachFile([
      'Type_of_Document_You_Wish_to_Create.docx',
      'Type$doc.docx',
      '1typedoc.docx',
      'Type document.docx',
      'ack.docx',
      'chapter_01.xml',
      'appg-et1.xlsx',
      'chapter.doc',
      'app5-Table3.pdf',
    ])

    cy.get(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
    ).scrollIntoView()

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.contains('[data-test-id="1typedoc.docx"]', 'Ready for Upload')
    cy.contains('[data-test-id="ack.docx"]', 'Ready for Upload')
    cy.contains('[data-test-id="chapter_01.xml"]', 'Ready for Upload')
    cy.contains('[data-test-id="appg-et1.xlsx"]', 'Ready for Upload')
    cy.contains('[data-test-id="chapter.doc"]', 'Ready for Upload')
    cy.contains('[data-test-id="app5-Table3.pdf"]', 'Ready for Upload')

    cy.get('[data-test-id="remove-btn"]').click()
    cy.contains('Source').click()

    cy.log('converted')

    // No validations for converted files

    cy.contains('Converted').click()

    cy.contains('Bookshelf Display PDFs').click()
    cy.log('Bookshelf PDFs validation')

    cy.get('[data-test-id="upload-displayPDFs-input"]').attachFile([
      'Type document.docx',
      'Type$doc.docx',
    ])

    // cy.contains(
    //   '[data-test-id="Type document.docx"]',
    //   'Filenames must not contain spaces',
    // )

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.get('[data-test-id="remove-btn"]').click()
    cy.contains('Bookshelf Display PDFs').click()

    cy.contains('Supplementary').click()
    cy.log('supplementary')
    cy.get('[data-test-id="upload-supplementary-input"]').scrollIntoView()

    cy.get('[data-test-id="upload-supplementary-input"]').attachFile([
      'Type document.docx',
      'Type$doc.docx',
    ])

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.get('[data-test-id="remove-btn"]').click()

    cy.contains('Supplementary').click()

    cy.contains('Images').click()

    cy.log('supplementary')
    cy.get('[data-test-id="upload-images-input"]').scrollIntoView()

    cy.get('[data-test-id="upload-images-input"]').attachFile([
      'Type document.docx',
      'Type$doc.docx',
    ])

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.get('[data-test-id="remove-btn"]').click()
    cy.contains('Images').click()

    cy.contains('Support').click()
    cy.log('support')

    cy.get('[data-test-id="upload-support-input"]').attachFile([
      'Type document.docx',
      'Type$doc.docx',
    ])

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.get('[data-test-id="remove-btn"]').click()
    cy.contains('Support').click()
    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()
    cy.get('[data-test-id="file-input-feedback"]').attachFile('ack.docx')

    // I commented out this part until chat is fixed because it crashes the app
    cy.get('[data-test-id="send-feedback"]').click()
    cy.get('[data-test-id="files-tab"]').click()
    cy.reload()
    cy.contains('Review').click()
    cy.contains('div', 'ack.docx')
  })

  it('Update settings', () => {
    cy.updateCompleteBookSettings(false, organisation.name, completeBook.title)
  })

  it('check settings', () => {
    cy.checkCompleteBookSettings(false, organisation.name, completeBook.title)
  })

  it('uploading converted file and checking metadata', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()
    cy.get('[data-test-id="files-tab"]').click()

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForUpload')

    cy.get('[data-test-id="upload-source-input"]').attachFile(
      '/pdfwf/wholebook/cer242.pdf',
    )

    cy.get('[data-test-id="save-btn"]').click()
    cy.wait('@waitForUpload')
    cy.wait('@waitForUpload')

    cy.get('[data-test-id="upload-converted-input"]').attachFile(
      '/pdfwf/wholebook/rethinking.xml',
    )

    cy.get('[data-test-id="save-btn"]').click()
    cy.wait('@waitForUpload')
    cy.wait('@waitForUpload')
    cy.reload()
    cy.get('[data-test-id="metadata-tab"]').click()
    cy.get('[name="title"]').should('have.value', 'Rethinking Moral Status')

    cy.get('[name="subtitle"]').should(
      'have.value',
      'rethinking moral status subtitle',
    )

    cy.get('[name="alternativeTitle"]').should(
      'have.value',
      'Rethinking moral staus alt title',
    )

    cy.get('[name="edition"]').should('have.value', '10')

    cy.get('[name="publisherName"]').should(
      'have.value',
      'Oxford University Press',
    )

    cy.get('[name="publisherLocation"]').should('have.value', 'Oxford (UK)')

    cy.get('[data-test-id="publication-date"] input[placeholder="MM"]').should(
      'have.value',
      '08',
    )

    cy.get(
      '[data-test-id="publication-date"] input[placeholder="YYYY"]',
    ).should('have.value', '2021')

    cy.get('[data-test-id="back-link"]').click()
    cy.contains('div', 'Rethinking Moral Status')
  })

  it('check versions,delete supplementary files and images ', () => {
    cy.contains(organisation.name).click()
    cy.contains('Rethinking Moral Status').click()
    cy.get('[data-test-id="files-tab"]').click()
    cy.contains('Source').click()
    cy.contains('Converted').click()
    cy.contains('Supplementary').click()
    cy.get('[data-test-id="upload-supplementary-input"]').attachFile('ack.docx')

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForUpload')

    cy.get('[data-test-id="save-btn"]').click()
    cy.wait('@waitForUpload')
    cy.contains('[aria-rowindex="1"] [aria-colindex="3"]', 'V1.1')
    cy.get('[data-test-id="upload-supplementary-input"]').attachFile('ack.docx')

    cy.get('[data-test-id="save-btn"]').click()
    cy.wait('@waitForUpload')

    cy.contains('[aria-rowindex="1"] [aria-colindex="3"]', 'V1.2')
    cy.contains('button', 'Show all versions').click()
    cy.contains('[aria-rowindex="1"] [aria-colindex="3"]', 'V1.2')
    cy.contains('[aria-rowindex="2"] [aria-colindex="3"]', 'V1.1')
    cy.get('[data-test-id="book-comp-delete"]').click()
    cy.contains('Current Version (V2)').click()
    cy.get('[id=react-select-2-option-1]').click()
    cy.get('[data-test-id="confirm"]').click()
    cy.contains('No files available')
    cy.contains('Supplementary').click()
    cy.contains('Images').click()
    cy.get('[data-test-id="upload-images-input"]').attachFile('img12.png')
    cy.get('[data-test-id="save-btn"]').click()
    cy.wait('@waitForUpload')
    cy.contains('[aria-rowindex="1"] [aria-colindex="4"]', 'V.1')
    cy.get('[data-test-id="upload-images-input"]').attachFile('img12.png')

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForUpload1')

    cy.get('[data-test-id="save-btn"]').click()
    cy.wait('@waitForUpload1')

    cy.contains('button', 'Show all versions').click()
    // cy.contains('button', 'Show all versions').click()

    // This needs checking after the issue with File Versions is fixed
    cy.contains('[aria-rowindex="1"] [aria-colindex="4"]', 'V.2')
    // cy.contains('[aria-rowindex="2"] [aria-colindex="4"]', 'V.1')

    cy.get('[data-test-id="book-comp-delete"]').click()
    cy.contains('Current Version (V2)').click()
    cy.get('#react-select-3-option-1').click()
    cy.get('[data-test-id="confirm"]').click()
    cy.contains('No files available')
  })

  it('Errors tab and help pannel', () => {
    cy.contains(organisation.name).click()
    cy.contains('Rethinking Moral Status').click()

    // eslint-disable-next-line jest/valid-expect-in-promise
    cy.url().then(url => {
      const orgId = url.split('/')[4]
      const bookId = url.split('/')[6]

      cy.exec(
        `node ./scripts/seedUser.js addToTeams ${seededUser.username} ${bookId} editor`,
      )

      cy.exec(
        `node ./scripts/seedUser.js addToTeams ${seededUser.username} ${orgId} editor`,
      )

      cy.exec(
        `node ./scripts/seedUser.js addToTeams ${seededUser.username} ${orgId} user`,
      )
    })

    cy.reload()
    cy.get('[data-test-id="errors-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()
    cy.get('.mentions__input').type(`hi @${seededUser.givenName}{enter}`)
    cy.get('[data-test-id="send-feedback"]').click()
    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()
    cy.get('.mentions__input').type(`hi @${seededUser.givenName}{enter}`)
    cy.get('[data-test-id="send-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="author-name"]',
      admin.username.charAt(0).toUpperCase() + admin.username.slice(1),
    )

    cy.login(seededUser)
    cy.contains(organisation.name).click()
    cy.contains('Rethinking Moral Status').click()
    cy.get('[data-test-id="errors-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="message-content"]',
      `hi @${seededUser.givenName} seed`,
    )

    cy.get('[data-test-id="send-feedback"]').click()
    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="message-content"]',
      `hi @${seededUser.givenName} seed`,
    )
  })
})
