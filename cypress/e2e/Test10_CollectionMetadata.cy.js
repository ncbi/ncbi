import {
  admin,
  organisation,
  workflows,
  collection,
} from '../support/credentials'

describe('Check Metadata fields of a Collection', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/seeds/index.js')
  })

  beforeEach(() => {
    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
  })

  it('Creating an organisation', () => {
    cy.addOrganisation(
      organisation,
      workflows[1],
      'name="settings.type.publisher"',
    )
  })

  it('Creating a collection', () => {
    cy.contains(organisation.name).click()

    cy.get('button[data-test-id="create-new-collection"]').click()
    cy.get('[data-test-id="collection-type"]').click()
    cy.get('.ProseMirror').click()
    cy.contains('div', 'Collection type is required')
    cy.get('[name="publisherName"]').focus().blur()
    cy.contains('div', 'Publisher name is required')
    cy.get('[name="publisherLocation"]').focus().blur()
    cy.get('[data-test-id="pubdate-type-select"]').click()
    cy.get('[data-test-id="wholeBookSourceType-select"]').click()

    cy.get(
      '[data-test-id="chapterProcessedSourceType-select"]',
    ).scrollIntoView()

    cy.get('input[data-test-id="start-month"]').focus().blur()
    cy.contains('div', 'Date of publication is required')
    cy.get('[data-test-id="chapterProcessedSourceType-select"]').click()
    cy.contains('button', 'Next').click({ force: true })
    cy.contains('div', 'Publication date type is required')
    cy.contains('div', 'Whole book source type is required')
    cy.contains('div', 'Chapter processed source type is required')
    cy.get('[data-test-id="collection-type"]').scrollIntoView()
    cy.contains('div', 'Title is required')
    cy.get('[data-test-id="collection-type"]').click()
    cy.contains('div', 'Book Series Collection').click()
    cy.get('.ProseMirror').type('testCollection')

    cy.get('[data-test-id="collection-type"]')
      .click({ force: true })
      .type('Funded{enter}')

    cy.get('[name="publisherName"]').type(collection.publisher)
    cy.get('[name="publisherLocation"]').type(collection.location)

    cy.get(
      '[data-test-id="chapterProcessedSourceType-select"]',
    ).scrollIntoView()

    cy.get('[data-test-id="pubdate-type-select"]').type(
      `Print publication range {enter}`,
    )

    cy.get('[data-test-id="start-month"]').type('06')
    cy.get('[data-test-id="start-year"]').type('2022')
    cy.get('[data-test-id="end-month"]').type('06')
    cy.get('[data-test-id="end-year"]').type('2022')

    cy.get('[data-test-id="wholeBookSourceType-select"]')
      .click({ force: true })
      .type(`Book{enter}`)

    cy.get('[data-test-id="chapterProcessedSourceType-select"]')
      .click({ force: true })
      .type(`Book{enter}`)

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForCollection')

    cy.get('[type="submit"]').click()
    cy.wait('@waitForCollection')
    // cy.contains("div",title);

    cy.get('[data-test-id="org-admin-or-editor-lock"]')
      .invoke('attr', 'color')
      .should('eq', 'black')

    cy.get('[name="requireApprovalByOrgAdminOrEditor"]').should(
      'have.value',
      'true',
    )

    cy.get('[name="openAccess"]').should('have.value', 'false')
    cy.get('[name="UKPMC"]').should('have.value', 'false')
    cy.get('input[name="publisherUrl"]').should('be.empty')
    cy.get('input[name="citationSelfUrl"]').should('be.empty')
    cy.get('input[name="groupBooksOnTOC"]').should('have.value', 'false')
    cy.get('[data-test-id="order-books-by"]').type(`Title {enter}`)
    cy.get('input[name="tocContributors"]').should('have.value', 'false')
    cy.get('input[name="tocFullBookCitations"]').should('have.value', 'false')
    cy.contains('button', 'Submit').click()
  })

  it('Checking Metadata of the Collection for the default values', () => {
    cy.contains(organisation.name).click()

    cy.contains('testCollection').click()
    cy.get('button[data-test-id="collectionMetadata"]').click()
    cy.contains('.ProseMirror', 'testCollection')
    cy.get('input[name="issn"]').should('be.empty')

    // check of abstract value missing

    cy.get('input[name="publisherName"]').should(
      'have.value',
      collection.publisher,
    )

    cy.get('input[name="publisherLocation"]').should(
      'have.value',
      collection.location,
    )

    cy.get('input[name="publicationDateType"]').should(
      'have.value',
      'pubr-print',
    )

    cy.get('[data-test-id="start-month"]').should('have.value', '06')
    cy.get('[data-test-id="start-year"]').should('have.value', '2022')
    cy.get('[data-test-id="end-month"]').should('have.value', '06')
    cy.get('[data-test-id="end-year"]').should('have.value', '2022')
    cy.get('input[name="sourceType"]').should('have.value', 'Collection')

    // The following elements are hidden and cannot check the value
    /* cy.get('[data-test-id="wholeBookSourceType-select"]').should(
      'have.value',
      'Book',
    )

    cy.get('[data-test-id="chapterProcessedSourceType-select"]').should(
      'have.value',
      'Book',
    ) */

    // Contributors check missing
    // Copyright statement check missing
    cy.get('[name="openAccessLicense"]').should('have.value', 'false')
    cy.get('[name="licenseType"]').should('be.empty')
  })

  it('Updating Metadata of the Collection', () => {
    cy.contains(organisation.name).click()

    cy.contains('testCollection').click()
    cy.get('button[data-test-id="collectionMetadata"]').click()
    cy.contains('.ProseMirror', 'testCollection')
    cy.get('input[name="issn"]').type('11110000')
    cy.get('input[name="eissn"]').type('22')

    cy.get('[role="alert"]').contains(
      'ISSNs need to be provided in the format 0000-0000 where the real digits replace the zeroes and are separated by a hyphen',
    )

    cy.get('input[name="issn"]').clear().type('1111-0000')

    cy.get('[role="alert"]').contains(
      'ISSNs need to be provided in the format 0000-0000 where the real digits replace the zeroes and are separated by a hyphen',
    )

    cy.get('input[name="eissn"]').type('22-3333')

    cy.get('[data-test-id="abstract"] div.ProseMirror').type(
      'abstract of collection',
    )

    cy.get('input[name="publisherName"]').clear().type('Pub Collection Name')

    cy.get('input[name="publisherLocation"]')
      .clear()
      .type('Pub Collection Location')

    cy.get('[data-test-id="publication-date-type-select"]').type(
      'Electronic{enter}',
    )

    cy.get('[data-test-id="start-month"]').clear().type('05')
    cy.get('[data-test-id="start-year"]').clear().type('2005')
    cy.get('[data-test-id="end-month"]').clear().type('10')
    cy.get('[data-test-id="end-year"]').clear().type('2006')

    /* Cannot click the following buttons
    cy.get('input[name="wholeBookSourceType"]')
      .click({ force: true })
      .type('Database{enter}')

    cy.get('input[name="chapterProcessedSourceType"]')
      .click({ force: true })
      .type('Report{enter}')
*/
    // Adding Contributors
    cy.get('[data-test-id="add-seriesEditors"]').click()

    cy.get('input[name="seriesEditors[0].surname"]').click().type('TestSurname')

    cy.get('input[name="seriesEditors[0].givenName"]').click().type('Editor')
    cy.get('input[name="seriesEditors[0].role"]').click().type('Role')
    cy.get('input[name="seriesEditors[0].degrees"]').click().type('QATester')
    // Adding Collaborative Authors
    cy.get('[data-test-id="add-collaborativeAuthors"]').click()

    cy.get('input[name="collaborativeAuthors[0].givenName"]')
      .click()
      .type('Author in collection meta')

    // Copyright statement check missing
    cy.get('[data-test-id="open-access-license-lock"]').click()
    cy.get('input[name="openAccessLicense"]').click({ force: true })
    cy.get('[data-test-id="open-access-license-lock"]').click()

    // Can not be clicked
    cy.get('[data-test-id="permissions"] > :nth-child(4) .select__control')
      .click({ force: true })
      .type('CC BY{enter}')

    cy.get('[data-test-id="license_statement"]').scrollIntoView()

    cy.get('[data-test-id="copyright_statement"] div.ProseMirror').type(
      'Permissive license',
    )

    cy.get('[data-test-id="license_statement"] div.ProseMirror').type(
      'Open License',
    )

    // this line is if we would have licenseurl for the book metadata.
    // cy.get('[data-test-id="permissions"] :nth-child(6) > :nth-child(2)').type("https://coko.foundation")

    // Add data-test-id for Add note
    // grants to be fixed
    // cy.get('[data-test-id="add-grant"]').click()
    // cy.contains('Granthub').type('1')

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForCollectionMetadata')

    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.wait('@waitForCollectionMetadata')
  })

  it('Checking Metadata of the Collection for the updated values', () => {
    cy.contains(organisation.name).click()

    cy.contains('testCollection').click()
    cy.get('button[data-test-id="collectionMetadata"]').click()
    cy.contains('.ProseMirror', 'testCollection')
    cy.get('input[name="issn"]').should('have.value', '1111-0000')
    cy.get('input[name="eissn"]').should('have.value', '2222-3333')

    cy.contains(
      '[data-test-id="abstract"] div.ProseMirror > .paragraph',
      'abstract of collection',
    )

    cy.get('input[name="publisherName"]').should(
      'have.value',
      'Pub Collection Name',
    )

    cy.get('input[name="publisherLocation"]').should(
      'have.value',
      'Pub Collection Location',
    )

    cy.get('input[name="publicationDateType"]').should(
      'have.value',
      'pubr-electronic',
    )

    cy.get('[data-test-id="start-month"]').should('have.value', '05')
    cy.get('[data-test-id="start-year"]').should('have.value', '2005')
    cy.get('[data-test-id="end-month"]').should('have.value', '10')
    cy.get('[data-test-id="end-year"]').should('have.value', '2006')
    cy.get('input[name="sourceType"]').should('have.value', 'Collection')

    // The following elements are hidden and cannot check the value
    /* cy.get('[data-test-id="wholeBookSourceType-select"]').should(
      'have.value',
      'Book',
    )

    cy.get('[data-test-id="chapterProcessedSourceType-select"]').should(
      'have.value',
      'Book',
    ) */

    cy.get('input[name="seriesEditors[0].surname"]').should(
      'have.value',
      'TestSurname',
    )

    cy.get('input[name="seriesEditors[0].givenName"]').should(
      'have.value',
      'Editor',
    )

    cy.get('input[name="seriesEditors[0].role"]').should('have.value', 'Role')

    cy.get('input[name="seriesEditors[0].degrees"]').should(
      'have.value',
      'QATester',
    )

    cy.get('input[name="collaborativeAuthors[0].givenName"]').should(
      'have.value',
      'Author in collection meta',
    )

    // Copyright statement check missing
    cy.get('[name="openAccessLicense"]').should('have.value', 'true')

    cy.contains(
      '[data-test-id="permissions"] > :nth-child(4) .select__control',
      'CC BY',
    )

    cy.contains(
      '[data-test-id="copyright_statement"] div.ProseMirror',
      'Permissive license',
    )

    cy.contains(
      '[data-test-id="license_statement"] div.ProseMirror',
      'Open License',
    )

    // cy.get('input[name="funding[0].number"]').should('have.value', '1')
  })

  it('Adding notes on the metadata collection', () => {
    cy.contains(organisation.name).click()
    cy.contains('testCollection').click()
    cy.get('button[data-test-id="collectionMetadata"]').click()
    cy.get('[data-test-id="add-notes"]').click()

    cy.get('[data-test-id="typeOfNote-select"]')
      .click({ force: true })
      .type('Author note{enter}')

    cy.get('input[name="notes[0].title"]').type('Author note 1')

    cy.get('[data-test-id="description"] div.ProseMirror').type(
      'Author note description',
    )

    cy.get('[data-test-id="add-notes"]').click()

    cy.get('[data-test-id="typeOfNote-select"]:last')
      .click({ force: true })
      .type('Disclaimer{enter}')

    cy.get('input[name="notes[1].title"]').scrollIntoView()
    cy.get('input[name="notes[1].title"]').type('Disclaimer note 1')

    cy.get('[data-test-id="description"] div.ProseMirror:last').type(
      'Disclaimer note description',
    )

    cy.get('[data-test-id="add-notes"]').click()

    cy.get('[data-test-id="typeOfNote-select"]:last')
      .click({ force: true })
      .type('Generic{enter}')

    cy.get('input[name="notes[2].title"]').scrollIntoView()
    cy.get('input[name="notes[2].title"]').type('Generic note 1')

    cy.get('[data-test-id="description"] div.ProseMirror:last').type(
      'Generic note description',
    )

    cy.intercept({
      method: 'POST',
      url: 'http://localhost:3000/graphql',
    }).as('waitForCollectionMetadata')

    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.wait('@waitForCollectionMetadata')
  })

  it('Checking the saved notes in collection metadata', () => {
    cy.contains(organisation.name).click()

    cy.contains('testCollection').click()
    cy.get('button[data-test-id="collectionMetadata"]').click()
    cy.contains('.ProseMirror', 'testCollection')
    cy.contains('[data-test-id="typeOfNote-select"]:first', 'Author note')
    cy.get('input[name="notes[0].title"]').should('have.value', 'Author note 1')

    cy.contains(
      '[data-test-id="description"] div.ProseMirror:first',
      'Author note description',
    )

    cy.get('[data-test-id="delete-note"]:first').click()

    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('Successfully updated metadata')
    cy.reload()
    cy.contains('testCollection').click()
    cy.get('button[data-test-id="collectionMetadata"]').click()
    cy.contains('Author note 1').should('not.exist')
    // cy.get('[data-test-id="typeOfNote-select"]:first').scrollIntoView()
    cy.contains('[data-test-id="typeOfNote-select"]:first', 'Disclaimer')

    cy.get('input[name="notes[0].title"]').should(
      'have.value',
      'Disclaimer note 1',
    )

    cy.contains(
      '[data-test-id="description"]:first div.ProseMirror',
      'Disclaimer note description',
    )

    cy.contains('[data-test-id="typeOfNote-select"]:last', 'Generic')

    cy.get('input[name="notes[1].title"]').should(
      'have.value',
      'Generic note 1',
    )

    cy.contains(
      '[data-test-id="description"] div.ProseMirror:last',
      'Generic note description',
    )
  })

  it('Testing different formats for Date of Publication', () => {
    cy.contains(organisation.name).click()

    cy.contains('testCollection').click()
    cy.get('button[data-test-id="collectionMetadata"]').click()
    cy.contains('.ProseMirror', 'testCollection')

    cy.get('[data-test-id="publication-date-type-select"]').type(
      'Electronic{enter}',
    )

    // Case null/null-null/null
    cy.clearCollectionPubDate()
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('Date of publication is required')

    // Case null/null-null/YEAR
    cy.get('[data-test-id="end-year"]').type('2000')
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('The year the publication range starts is required')

    // Case null/null-MM/YEAR
    cy.get('[data-test-id="end-month"]').type('9')
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('The year the publication range starts is required')

    // Case null/YEAR-MM/YEAR
    cy.get('[data-test-id="start-year"]').type('1999')

    cy.contains(
      'If a closing month for the range is provided, a start month must be provided too',
    )

    // Case null/YEAR-MM/null
    cy.get('[data-test-id="end-year"]').clear()
    cy.contains('If month is provided, year must be provided too')

    // Case null/null-MM/null
    cy.get('[data-test-id="start-year"]').clear()
    cy.contains('Year of publication is required')

    // Case null/YEAR-null/null
    cy.get('[data-test-id="end-month"]').clear()
    cy.get('[data-test-id="start-year"]').type('2000')
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('Successfully updated metadata')

    // Case null/YEAR-null/YEAR
    cy.get('[data-test-id="end-year"]').type('2001')
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('Successfully updated metadata')

    // Case MM/null-null/null
    cy.clearCollectionPubDate()
    cy.get('[data-test-id="start-month"]').type('12')
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('The year the publication range starts is required')
    // cy.contains('Year of publication is required.')

    // Case MM/null-null/YEAR
    cy.get('[data-test-id="end-year"]').type('2001')
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('If month is provided, year must be provided too')

    // Case MM/null-MM/YEAR
    cy.get('[data-test-id="end-month"]').type('4')
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('If month is provided, year must be provided too')

    // Case MM/null-MM/null
    cy.get('[data-test-id="end-year"]').clear()
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('Year of publication is required')

    // Case MM/YEAR-null/null
    cy.clearCollectionPubDate()
    cy.get('[data-test-id="start-month"]').type('12')
    cy.get('[data-test-id="start-year"]').type('2000')
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('Successfully updated metadata')

    // Case MM/YEAR-null/YEAR
    cy.get('[data-test-id="end-year"]').type('2001')
    cy.get('[data-test-id="save-collection-metadata"]').click()

    cy.contains(
      'If a start month for the range is provided, a closing month must be provided too',
    )

    // Case MM/YEAR-MM/null
    cy.get('[data-test-id="end-year"]').clear()
    cy.get('[data-test-id="end-month"]').type('12')
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('If month is provided, year must be provided too')

    // Case MM/YEAR-MM/YEAR
    cy.get('[data-test-id="end-year"]').type('2001')
    cy.get('[data-test-id="save-collection-metadata"]').click()
    cy.contains('Successfully updated metadata')
  })
})

Cypress.Commands.add('clearCollectionPubDate', () => {
  cy.get('[data-test-id="start-month"]').clear()
  cy.get('[data-test-id="start-year"]').clear()
  cy.get('[data-test-id="end-month"]').clear()
  cy.get('[data-test-id="end-year"]').clear()
})
