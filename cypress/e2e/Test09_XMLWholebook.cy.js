import {
  admin,
  completeBook,
  organisation,
  workflows,
  seededUser,
} from '../support/credentials'
import { aliasQuery } from '../utils/graphql-test-utils'

describe('Testing XML workflow', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/seeds/index.js')
    cy.exec(`node ./scripts/createOrg.js ${organisation.name}`)

    cy.exec(`node ./scripts/seedUser.js create seededUser`)
  })

  beforeEach(() => {
    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
  })

  it('Creating an XML complete book', () => {
    cy.contains(organisation.name).click()

    cy.addBook(
      completeBook.title,
      workflows[2].name,
      'Complete books and documents',
    )
  })

  it('Updating book settings', () => {
    cy.updateCompleteBookSettings(false, organisation.name, completeBook.title)
  })

  it('Checking if the values are retained', () => {
    cy.checkCompleteBookSettings(false, organisation.name, completeBook.title)
  })

  it('Updating book team', () => {
    /* There is nothing here yet */
  })
})

describe('Files Tab Upload', () => {
  beforeEach(() => {
    cy.intercept('POST', 'http://localhost:3000/graphql', req => {
      // Queries
      aliasQuery(req, 'getBook')
      aliasQuery(req, 'deleteFile')
    })

    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
  })

  it('Validating filenames for Source files', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()

    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-source-input"]').attachFile([
      'Type document.docx',
      'Type_of_Document_You_Wish_to_Create.docx',
      'Type$doc.docx',
      'chapter.doc',
      'chapter_01.docx',
      'pdf-chap.pdf',
      '26119.xml',
      'appg-et1.xlsx',
      'PB-0016-Bland.bxml',
      'img12.png',
    ])

    // cy.get('[data-test-id="Type document.docx"]').scrollIntoView()

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    // The limit on the number of characters was removed
    cy.contains(
      '[data-test-id="Type_of_Document_You_Wish_to_Create.docx"]',
      'Ready for Upload',
    )

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.contains('[data-test-id="chapter.doc"]', 'Ready for Upload')
    cy.contains('[data-test-id="chapter_01.docx"]', 'Ready for Upload')
    cy.contains('[data-test-id="pdf-chap.pdf"]', 'Ready for Upload')
    cy.contains('[data-test-id="26119.xml"]', 'Ready for Upload')
    cy.contains('[data-test-id="appg-et1.xlsx"]', 'Ready for Upload')

    cy.contains(
      '[data-test-id="PB-0016-Bland.bxml"]',
      'File extension should be a .docx, .doc, .pdf , .xlsx, .xml',
    )

    cy.contains(
      '[data-test-id="img12.png"]',
      'File extension should be a .docx, .doc, .pdf , .xlsx, .xml',
    )
  })

  it('Uploading Source file', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()

    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-source-input"]').attachFile(['ukhta2501.xml'])
    cy.get('button[data-test-id="save-btn"]').click()

    cy.contains('add tag').type('main_xml{enter}')
  })

  it('Uploading Bookshelf Display PDFs file', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()

    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-displayPDFs-input"]').attachFile([
      'pdf-chap.pdf',
    ])

    cy.contains('Bookshelf Display PDFs').click()
    cy.contains('Save').scrollIntoView().click()

    cy.contains('add tag').type('book_pdf{enter}')
  })

  it('Uploading a single file in Supplementary', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()

    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-supplementary-input"]').attachFile([
      'piroxicam.docx',
    ])

    cy.contains('Supplementary').click()
    cy.contains('Save').scrollIntoView().click()
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it('Deleting a file from Supplementary', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()

    cy.get('[data-test-id="files-tab"]').click()

    cy.contains('Supplementary').click()

    cy.get('button[data-test-id="book-comp-delete"]').click()
    cy.get('button[data-test-id="confirm"]').click()
    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait('@gqldeleteFileQuery')
    cy.contains('No files available').scrollIntoView()
  })

  it('Uploading multiple Supplementary files', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()

    cy.get('[data-test-id="files-tab"]').click()

    // There are no validations for supplementary files
    cy.get('[data-test-id="upload-supplementary-input"]').attachFile([
      'piroxicam.docx',
      'img12.png',
      'app5-Table3.pdf',
      'appg-et1.xlsx',
    ])

    cy.contains('Supplementary').click()
    cy.contains('Save').scrollIntoView().click()
  })

  it('Uploading one Image', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()

    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-images-input"]').attachFile(['img13.jpg'])

    cy.contains('Images').click()
    cy.contains('Save').scrollIntoView().click()
  })

  /* eslint-disable-next-line jest/no-disabled-tests */
  it('Deleting a file from Images', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()

    cy.get('[data-test-id="files-tab"]').click()
    cy.contains('Converted').click()

    cy.contains('Images').click()

    cy.get('[data-test-id="book-comp-delete"]').click()
    cy.get('button[data-test-id="confirm"]').click()
    cy.contains('No files available').scrollIntoView().click()
  })

  it('Uploading Images', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()

    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-images-input"]').attachFile([
      'img13.jpg',
      'img12.png',
    ])

    cy.contains('Images').click()
    cy.contains('Save').scrollIntoView().click()
  })

  it('Uploading Converted file', () => {
    cy.contains(organisation.name).click()
    cy.contains(completeBook.title).click()

    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile([
      'chapter_01.docx',
    ])

    cy.contains(
      '[data-test-id="chapter_01.docx"]',
      'File extension should be .xml or .bxml',
    )

    cy.get('button[data-test-id="remove-btn"]').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile([
      'ukhta2501.bxml',
    ])

    cy.contains('Save').click()
    cy.wait('@gqlgetBookQuery')
  })

  it('Checking Metadata', () => {
    cy.contains(organisation.name).click()

    cy.contains(
      'Non-benzodiazepine hypnotic use for sleep disturbance in people aged over 55 years living with dementia: a series of cohort studies',
    ).click()

    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="metadata-tab"]').click()

    cy.get('[name="title"]').should(
      'have.value',
      'Non-benzodiazepine hypnotic use for sleep disturbance in people aged over 55 years living with dementia: a series of cohort studies',
    )

    cy.get('[name="subtitle"]').should('have.value', 'Systematic Review')

    cy.get('[name="alternativeTitle"]').should(
      'have.value',
      'Non-benzodiazepine hypnotic use for sleep disturbance',
    )

    cy.get('[name="edition"]').should('have.value', '2nd')
    cy.get('[name="volume"]').should('have.value', '25.1')
    cy.get('[name="doi"]').should('have.value', '10.3310/hta25010')
    cy.get('[name="bookSubmitId"]').should('be.empty')

    cy.get('[name="publisherName"]').should(
      'have.value',
      'NIHR Journals Library',
    )

    cy.get('[name="publisherLocation"]').should(
      'have.value',
      'Southampton (UK)',
    )

    cy.contains(
      '[data-test-id="publication-date-type-select"]',
      'Print publication',
    )

    cy.get('[data-test-id="publication-date"] input[placeholder="MM"]').should(
      'have.value',
      '1',
    )

    cy.get('[data-test-id="publication-date"] input[placeholder="DD"]').should(
      'be.empty',
    )

    cy.get(
      '[data-test-id="publication-date"] input[placeholder="YYYY"]',
    ).should('have.value', '2021')

    // Permissions
    // Must add data-test-ids for the fields in permissions
    cy.get('input[name="openAccessLicense"]').should('have.value', 'false')

    cy.get('[data-test-id="back-link"]').click()

    cy.contains(
      'div',
      'Non-benzodiazepine hypnotic use for sleep disturbance in people aged over 55 years living with dementia: a series of cohort studies',
    )
  })

  it('Errors tab and help pannel', () => {
    cy.contains(organisation.name).click()

    cy.contains(
      'Non-benzodiazepine hypnotic use for sleep disturbance in people aged over 55 years living with dementia: a series of cohort studies',
    ).click()

    // eslint-disable-next-line jest/valid-expect-in-promise
    cy.url().then(url => {
      const orgId = url.split('/')[4]
      const bookId = url.split('/')[6]

      cy.exec(
        `node ./scripts/seedUser.js addToTeams ${seededUser.username} ${bookId} editor`,
      )

      cy.exec(
        `node ./scripts/seedUser.js addToTeams ${seededUser.username} ${orgId} editor`,
      )

      cy.exec(
        `node ./scripts/seedUser.js addToTeams ${seededUser.username} ${orgId} user`,
      )
    })

    cy.reload()
    cy.get('[data-test-id="errors-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()
    cy.get('.mentions__input').type(`hi @${seededUser.givenName}{enter}`)
    cy.get('[data-test-id="send-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="author-name"]',
      admin.username.charAt(0).toUpperCase() + admin.username.slice(1),
    )

    cy.login(seededUser)
    cy.contains(organisation.name).click()

    cy.contains(
      'Non-benzodiazepine hypnotic use for sleep disturbance in people aged over 55 years living with dementia: a series of cohort studies',
    ).click()

    cy.get('[data-test-id="errors-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="message-content"]',
      `hi @${seededUser.givenName} seed`,
    )

    cy.get('[data-test-id="send-feedback"]').click()
  })

  it('Preview tab', () => {
    cy.contains(organisation.name).click()

    cy.contains(
      'Non-benzodiazepine hypnotic use for sleep disturbance in people aged over 55 years living with dementia: a series of cohort studies',
    ).click()

    // eslint-disable-next-line jest/valid-expect-in-promise
    cy.url().then(url => {
      const orgId = url.split('/')[4]
      const bookId = url.split('/')[6]

      cy.exec(
        `node ./scripts/seedUser.js addToTeams ${seededUser.username} ${bookId} editor`,
      )

      cy.exec(
        `node ./scripts/seedUser.js addToTeams ${seededUser.username} ${orgId} editor`,
      )

      cy.exec(
        `node ./scripts/seedUser.js addToTeams ${seededUser.username} ${orgId} user`,
      )
    })

    cy.reload()
    // cy.get('[data-test-id="errors-tab"]').click()
    // cy.get('[data-test-id="show-feedback"]').click()
    // cy.get('.mentions__input').type(`hi @${seededUser.givenName}{enter}`)
    // cy.get('[data-test-id="send-feedback"]').click()
    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()
    cy.get('.mentions__input').type(`hi @${seededUser.givenName}{enter}`)
    cy.get('[data-test-id="send-feedback"]').click()

    // Attach a file to review
    cy.get('[data-test-id="file-input-feedback"]').attachFile('chapter_01.docx')
    cy.get('[data-test-id="send-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="author-name"]',
      admin.username.charAt(0).toUpperCase() + admin.username.slice(1),
    )

    cy.login(seededUser)
    cy.contains(organisation.name).click()

    cy.contains(
      'Non-benzodiazepine hypnotic use for sleep disturbance in people aged over 55 years living with dementia: a series of cohort studies',
    ).click()

    // cy.get('[data-test-id="errors-tab"]').click()
    // cy.get('[data-test-id="show-feedback"]').click()

    // cy.contains(
    //   '[data-test-id="feedback-container"] [data-test-id="message-content"]',
    //   `hi @${seededUser.givenName} seed`,
    // )

    // cy.get('[data-test-id="send-feedback"]').click()
    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="message-content"]',
      `hi @${seededUser.givenName} seed`,
    )

    // Check if the file appears in Review section
    cy.get('div[data-test-id="files-tab"]').click()
    cy.contains('Review').click()
    cy.contains('chapter_01.docx')
  })
})
