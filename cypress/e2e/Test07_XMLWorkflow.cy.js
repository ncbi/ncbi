import {
  admin,
  chapterProcessedBook,
  organisation,
  workflows,
} from '../support/credentials'
import { aliasQuery } from '../utils/graphql-test-utils'

describe('Testing XML workflow', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/seeds/index.js')

    cy.exec(`node ./scripts/createOrg.js ${organisation.name}`)
  })

  beforeEach(() => {
    cy.intercept('POST', 'http://localhost:3000/graphql', req => {
      // Queries
      aliasQuery(req, 'GetOrganisation')
      aliasQuery(req, 'getBook')
    })

    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
  })

  it('creating an XML chapter processed book', () => {
    cy.contains(organisation.name).click()

    cy.addBook(
      chapterProcessedBook.title,
      workflows[2].name,
      'Individual chapters',
    )
  })

  it('checking bulk uploads', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookUpload"]').click()

    cy.get('input[type="file"]').attachFile([
      'Type document.docx',
      'Type_of_Document_You_Wish_to_Create.docx',
      'Type$doc.docx',
      'chapter.doc',
      'pdf-chap.pdf',
      'appg-et1.xlsx',
      'PB-0016-Bland.bxml',
    ])

    cy.get('[data-test-id="Type document.docx"]').scrollIntoView()

    cy.contains(
      '[data-test-id="Type document.docx"]',
      'Filenames must not contain spaces',
    )

    cy.get('[data-test-id="Type$doc.docx"]').scrollIntoView()

    cy.contains(
      '[data-test-id="Type$doc.docx"]',
      'The filename includes unsupported characters. Filenames must only contain letters, numbers, hyphens, dashes, periods and underscores',
    )

    cy.get('[data-test-id="PB-0016-Bland.bxml"]').scrollIntoView()

    cy.contains(
      '[data-test-id="PB-0016-Bland.bxml"]',
      'File Extension should be a .docx, .doc, .pdf , .xlsx, .xml',
    )
  })

  it('Book metadata modal form validation', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookMetadata"]').click()
    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.get('[name="publisherName"]').scrollIntoView()
    cy.contains('div', 'Publisher name is required')
    cy.contains('div', 'Publisher location is required')
    cy.contains('div', 'Publication date type is required')
    cy.get('[data-test-id="ncbi-custom-metadata"]').scrollIntoView()
    cy.contains('div', 'Book source type is required')
  })

  it('Book metadata updates', () => {
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.get('[data-test-id="bookMetadata"]').click()
    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.get('[name="publisherName"]').scrollIntoView()
    cy.contains('div', 'Publisher name is required')
    cy.contains('div', 'Publisher location is required')
    cy.contains('div', 'Publication date type is required')
    cy.get('[data-test-id="ncbi-custom-metadata"]').scrollIntoView()
    cy.contains('div', 'Book source type is required')

    cy.updateChapterProcessedMetadata(
      false,
      organisation.name,
      chapterProcessedBook.title,
    )
  })

  it('Checking updated book metadata', () => {
    cy.checkChapterProcessedMetadata(
      false,
      organisation.name,
      chapterProcessedBook.title,
    )
  })

  it('Book settings update', () => {
    cy.updateChapterProcessedSettings(
      false,
      organisation.name,
      chapterProcessedBook.title,
    )
  })

  it('checking if the values for updated settings are retained', () => {
    cy.checkChapterProcessedSettings(
      false,
      organisation.name,
      chapterProcessedBook.title,
    )
  })
})

describe('Parts - Create Parts and add/remove editors', () => {
  beforeEach(() => {
    cy.intercept('POST', 'http://localhost:3000/graphql', req => {
      // Queries
      aliasQuery(req, 'GetOrganisation')
      aliasQuery(req, 'getBook')
      aliasQuery(req, 'updateBookComponent')
    })

    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlGetOrganisationQuery')
    cy.wait('@gqlgetBookQuery')
  })

  it('Adding part', () => {
    cy.get('[data-test-id="addBookPart"]').click()
    cy.get('[data-test-id="title-editor"] div.ProseMirror').type('Part32')
    cy.get('[data-test-id="add-metadata.editor"]').click()
    cy.get('[name="metadata.editor[0].surname"]').type('doe')
    cy.get('[name="metadata.editor[0].givenName"]').type('john')
    cy.get('[name="metadata.editor[0].suffix"]').type('1010')
    cy.get('[data-test-id="save-add-part"]').click({ force: true })
    cy.wait('@gqlGetOrganisationQuery')
    cy.get('[title="Part32"] span a').should('exist')
  })

  it('checking if values are retained part data', () => {
    cy.get('[title="Part32"] span a').click({ force: true })
    cy.contains('[data-test-id="title-editor"] .ProseMirror', 'Part32')

    cy.get('[name="metadata.editor[0].surname"]').should('have.value', 'doe')

    cy.get('[name="metadata.editor[0].givenName"]').should('have.value', 'john')

    cy.get('[name="metadata.editor[0].suffix"]').should('have.value', '1010')

    // No contributors added yet
    cy.wait('@gqlGetOrganisationQuery')
  })

  it('updating parts', () => {
    cy.get('[title="Part32"] span a').click({ force: true })

    cy.get('[data-test-id="title-editor"] div.ProseMirror')
      .type('{selectall}{del}')
      .type('Part45')

    cy.get('[name="metadata.editor[0].surname"]').clear().type('williams')
    cy.get('[name="metadata.editor[0].givenName"]').clear().type('serina')
    cy.get('[name="metadata.editor[0].suffix"]').clear().type('1010')
    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.wait('@gqlupdateBookComponentQuery')
    cy.wait('@gqlGetOrganisationQuery')
  })

  it('checking updated file and deleting contributors', () => {
    cy.get('[title="Part45"] span a').click({ force: true })

    cy.get('[name="metadata.editor[0].surname"]').should(
      'have.value',
      'williams',
    )

    cy.get('[name="metadata.editor[0].givenName"]').should(
      'have.value',
      'serina',
    )

    cy.get('[name="metadata.editor[0].suffix"]').should('have.value', '1010')

    cy.get('[data-test-id="delete-1"]').click()

    cy.contains('No contributors added yet')
    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.wait('@gqlGetOrganisationQuery')
  })

  it('Checking parts styling', () => {
    cy.get('[title="Part45"] span a').click({ force: true })

    cy.get('button[title="Toggle strong"]').click()
    cy.get('[data-test-id="title-editor"] div.ProseMirror').type('Bold')
    cy.get('button[title="Toggle emphasis"]').click()
    cy.get('[data-test-id="title-editor"] div.ProseMirror').type('Italic')
    cy.get('button[title="Toggle subscript"]').click()
    cy.get('[data-test-id="title-editor"] div.ProseMirror').type('Subscripted')
    cy.get('button[title="Toggle superscript"]').click()

    cy.get('[data-test-id="title-editor"] div.ProseMirror').type(
      'Superscripted',
    )

    cy.get('[data-test-id="save-book-metadata"]').click()
    cy.wait('@gqlGetOrganisationQuery')
  })
})

describe('Files Tab - Upload check for all the sections', () => {
  beforeEach(() => {
    cy.intercept('POST', 'http://localhost:3000/graphql', req => {
      // Queries
      aliasQuery(req, 'getBook')
    })

    cy.login(admin)
    cy.get('[data-test-id="organizations-tab"]').click()
    cy.contains(organisation.name).click()
    cy.contains(chapterProcessedBook.title).click()
    cy.wait('@gqlgetBookQuery')
  })

  /* eslint-disable-next-line jest/no-disabled-tests */
  it('Uploading Source files', () => {
    // cy.wait(5000)
    cy.get('[data-test-id="bookUpload"]').click()

    cy.get(
      'input[type="file"]',
    ).selectFile('./cypress/fixtures/PB-0016-Bland.xml', { force: true })

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    // cy.wait(2000)
    cy.get('button[data-test-id="book-uplaod"]').click({ force: true })
    cy.get('[title="Untitled"] span a').click({ force: true })
    cy.get('[data-test-id="metadata-tab"]').click()

    cy.get('[name="title"]')
      .should('have.value', 'Untitled')
      .should('be.disabled')

    // Title of information
    cy.get('[name="subTitle"]').should('be.empty').should('be.disabled')
    cy.get('[name="altTitle"]').should('be.empty').should('be.disabled')
    cy.get('[name="label"]').should('be.empty').should('be.disabled')
    cy.get('[name="chapterId"]').should('be.empty').should('be.disabled')
    cy.get('[name="language"]').should('have.value', 'en').should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="MM"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="DD"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="YYYY"]')
      .should('be.empty')
      .should('be.disabled')

    // Publication History
    cy.get('[data-test-id="date-created"] input[placeholder="MM"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-created"] input[placeholder="DD"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-created"] input[placeholder="YYYY"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="MM"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="DD"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="YYYY"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="MM"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="DD"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="YYYY"]')
      .should('be.empty')
      .should('be.disabled')

    // Contributors
    cy.contains('No contributors')

    // Funding
    cy.get('[data-test-id="add-grant"]').should('be.enabled')
  })

  it('Uploading Bookshelf Display PDFs file', () => {
    // cy.get('[data-test-id="Untitled-wrapper"]').click()
    cy.get('[class*=Status__Root]').click()
    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-displayPDFs-input"]').attachFile([
      'pdf-chap.pdf',
    ])

    cy.contains('Bookshelf Display PDFs').click()
    cy.contains('Save').scrollIntoView().click()
  })

  it('Uploading a single file in Supplementary', () => {
    // cy.get('[data-test-id="Untitled-wrapper"]').click()
    cy.get('[class*=Status__Root]').click()
    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-supplementary-input"]').attachFile([
      'piroxicam.docx',
    ])

    cy.contains('Supplementary').click()
    cy.contains('Save').scrollIntoView().click()
    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(3000)
  })

  it('Deleting a file from Supplementary', () => {
    // cy.get('[data-test-id="Untitled-wrapper"]').click({ force: true })
    cy.get('[class*=Status__Root]').click()
    // cy.wait('@gqlgetBookQuery')
    cy.get('[data-test-id="files-tab"]').click()

    cy.contains('Supplementary').click()

    cy.get('[data-test-id="book-comp-delete"]').click()
    cy.get('[data-test-id="confirm"]').click()
    cy.contains('No files available').scrollIntoView().click()
  })

  it('Uploading multiple Supplementary files', () => {
    // cy.get('[data-test-id="Untitled-wrapper"]').click({ force: true })
    cy.get('[class*=Status__Root]').click()
    // cy.wait('@gqlgetBookQuery')
    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-supplementary-input"]').attachFile([
      'piroxicam.docx',
      'img12.png',
      'app5-Table3.pdf',
      'appg-et1.xlsx',
    ])

    cy.contains('Supplementary').click()
    cy.contains('Save').scrollIntoView().click()
  })

  it('Uploading one Image', () => {
    // cy.get('[data-test-id="Untitled-wrapper"]').click()?
    cy.get('[class*=Status__Root]').click()
    // cy.wait('@gqlgetBookQuery')
    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-images-input"]').attachFile(['img13.jpg'])

    cy.contains('Images').click()
    cy.contains('Save').scrollIntoView().click()
  })

  it('Deleting a file from Images', () => {
    // cy.get('[data-test-id="Untitled-wrapper"]').click()
    cy.get('[class*=Status__Root]').click({ force: true })
    // cy.wait('@gqlgetBookQuery')
    cy.get('[data-test-id="files-tab"]').click()

    cy.contains('Images').click()

    cy.get('[data-test-id="book-comp-delete"]').click()
    cy.get('[data-test-id="confirm"]').click()
    cy.contains('No files available').scrollIntoView().click()
  })

  it('Uploading Images', () => {
    // cy.get('[data-test-id="Untitled-wrapper"]').click()
    cy.get('[class*=Status__Root]').click({ force: true })
    // cy.wait('@gqlgetBookQuery')
    cy.get('[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-images-input"]').attachFile([
      'img13.jpg',
      'img12.png',
    ])

    cy.contains('Images').click()
    cy.contains('Save').scrollIntoView().click()
  })

  it('Errors tab and help pannel', () => {
    // cy.get('[data-test-id="Untitled-wrapper"]').click({ force: true })
    cy.get('[class*=Status__Root]').click()
    cy.get('[data-test-id="errors-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()
    cy.get('.mentions__input').type('Message from Admin')
    cy.get('[data-test-id="send-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="author-name"]',
      admin.username.charAt(0).toUpperCase() + admin.username.slice(1),
    )

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="message-content"]',
      'Message from Admin',
    )
  })

  it('Preview tab and Review files', () => {
    // cy.get('[data-test-id="Untitled-wrapper"]').click()
    cy.get('[class*=Status__Root]').click()
    cy.get('[data-test-id="preview-tab"]').click()
    cy.get('[data-test-id="show-feedback"]').click()
    cy.get('.mentions__input').type('Review from Admin')
    cy.get('[data-test-id="send-feedback"]').click()

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="author-name"]',
      admin.username.charAt(0).toUpperCase() + admin.username.slice(1),
    )

    cy.contains(
      '[data-test-id="feedback-container"] [data-test-id="message-content"]',
      'Review from Admin',
    )

    // Attach a file to review
    cy.get('[data-test-id="file-input-feedback"]').attachFile('chapter_01.docx')

    cy.get('[data-test-id="send-feedback"]').click()

    // Check if the file appears in Review section
    cy.get('[data-test-id="files-tab"]').click()
    cy.contains('Review').click()
    cy.contains('chapter_01.docx')
  })

  /* eslint-disable-next-line jest/no-disabled-tests */
  it('Uploading Converted file and Checking Metadata', () => {
    // cy.get('[data-test-id="Untitled-wrapper"]').click()
    cy.get('[class*=Status__Root]').click()

    cy.get('div[data-test-id="files-tab"]').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile([
      'introduction.docx',
    ])

    cy.contains('File extension should be .xml or .bxml')
    cy.contains('Remove').click()

    cy.get('[data-test-id="upload-converted-input"]').attachFile([
      'PB-0016-Bland.bxml',
    ])

    cy.contains('Save').click()
    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(2000)

    cy.contains(
      'Innovations for Improving Access to Quality Health Care: The Prospects for Municipal Health Insurance in Guatemala',
    )

    cy.get('[data-test-id="metadata-tab"]').click()
    cy.reload()

    cy.get('[name="title"]')
      .should(
        'have.value',
        'Innovations for Improving Access to Quality Health Care: The Prospects for Municipal Health Insurance inGuatemala',
      )
      .should('be.disabled')

    // Title of information
    cy.get('[name="subTitle"]').should('be.empty').should('be.disabled')
    cy.get('[name="altTitle"]').should('be.empty').should('be.disabled')
    cy.get('[name="label"]').should('be.empty').should('be.disabled')

    cy.get('[name="chapterId"]')
      .should('have.value', 'PB-0016-Bland')
      .should('be.disabled')

    cy.get('[name="language"]').should('have.value', 'en').should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="MM"]')
      .should('have.value', '12')
      .should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="DD"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-of-publication"] input[placeholder="YYYY"]')
      .should('have.value', '2017')
      .should('be.disabled')

    // Publication History
    cy.get('[data-test-id="date-created"] input[placeholder="MM"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-created"] input[placeholder="DD"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-created"] input[placeholder="YYYY"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="MM"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="DD"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-updated"] input[placeholder="YYYY"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="MM"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="DD"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[data-test-id="date-revised"] input[placeholder="YYYY"]')
      .should('be.empty')
      .should('be.disabled')

    // Contributors
    cy.get('[name="authors[0].surname"]')
      .should('have.value', 'Bland')
      .should('be.disabled')

    cy.get('[name="authors[0].givenName"]')
      .should('have.value', 'Gary')
      .should('be.disabled')

    cy.get('[name="authors[0].suffix"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[name="authors[1].surname"]')
      .should('have.value', 'Peinado')
      .should('be.disabled')

    cy.get('[name="authors[1].givenName"]')
      .should('have.value', 'Lucrecia')
      .should('be.disabled')

    cy.get('[name="authors[1].suffix"]')
      .should('be.empty')
      .should('be.disabled')

    cy.get('[name="authors[2].surname"]')
      .should('have.value', 'Stewart')
      .should('be.disabled')

    cy.get('[name="authors[2].givenName"]')
      .should('have.value', 'Christin')
      .should('be.disabled')

    cy.get('[name="authors[2].suffix"]')
      .should('be.empty')
      .should('be.disabled')
  })
})
