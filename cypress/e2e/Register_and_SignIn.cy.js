const { admin, generalUser } = require('../support/credentials')

describe('Ncbi Register  ', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/seeds/index.js')
  })

  it('Loads sign-up page', () => {
    cy.visit({ url: '/signup', options: { log: true, failOnStatusCode: true } })
    cy.contains('Register')
  })

  it('Creates a user', () => {
    cy.visit('/signup')
    cy.get('input[name="givenName"]').type(generalUser.givenName)
    cy.get('input[name="surname"]').type(generalUser.surname)
    cy.get('input[name="username"]').type(generalUser.username)
    cy.get('input[name="email"]').type(generalUser.email)
    cy.get('input[name="password"]').type(generalUser.password)
    cy.get('input[name="confirmPassword"]').type(generalUser.password)
    cy.get('button[type="submit"]').click({ force: true })
    cy.contains('User has been created successfully!')
  })

  it('Fails if required fields not filled in', () => {
    cy.visit('/signup')
    cy.get('input[name="username"]').focus().blur()
    cy.get('input[name="email"]').focus().blur()
    cy.get('input[name="password"]').focus().blur()
    cy.get('button[type="submit"]').should('be.disabled')
    cy.get('button[type="submit"]').click({ force: true })
    cy.contains('Register')
  })

  it('Doesn`t logs in the user with wrong credentials', () => {
    cy.visit('/login')

    cy.get('input[name="username"]').type(admin.username)
    cy.get('input[name="password"]').type(`-----`)
    cy.get('button[type="submit"]').click()

    cy.url().should('eq', `${Cypress.config().baseUrl}/login`)
    cy.contains('Something went wrong')
  })
})
