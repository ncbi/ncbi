## 1. Steps to perform on Grafana Loki Server

As Grafana Loki does not come with any included authentication layer, we are using nginx as authenticating reverse proxy. We have setup basic authentication. Use the following steps to create a new set of user credentials.

1. Create a password file and **a first user**. Run the **htpasswd** utility with the `-c` flag (to create a new file), the file pathname as the first argument, and the username as the second argument:
   `$ sudo htpasswd -c /etc/nginx/.htpasswd user1`
2. Create additional user-password pairs. Omit the -c flag because the file already exists:
   `$ sudo htpasswd /etc/nginx/.htpasswd user2`

## 2. Configure BCMS Application to use Grafana Loki

1. The Grafana Loki configurations are stored in `promtail-local-config.yaml` file. It needs the following environment variables

| Variable         | Description                               | Sample Value                           |
| ---------------- | ----------------------------------------- | -------------------------------------- |
| LOKI_URL         | Loki client URL.                          | http://localhost:3100/loki/api/v1/push |
| LOKI_USER_NAME   | Loki user name created above              | user1                                  |
| LOKI_USER_PASS   | Loki user password created above          | password                               |
| LOKI_TENANT_NAME | A unique name to identify the application | bcms                                   |

## 3. Configure Grafana Data Source

1. Login as `Admin` to Grafana UI.
2. Navigate to the `Add new connection` section and choose `Loki` as the data source.
3. Fill in the necessary details, like

   1. URL - exampel `http://localhost:3100`
   2. Enable `Basic auth`. Under the `Basic Auth Details` add the `User` and `Password` created earlier.
   3. Under the `Custom HTTP Headers` section add the following headers

   | Header Name   | Value                                                                        |
   | ------------- | ---------------------------------------------------------------------------- |
   | X-Scope-OrgID | The is the unique application name for which we are logging, example: `bcms` |
   | Connection    | Upgrade                                                                      |
   | Upgrade       | websocket                                                                    |

   4. Click `Save & Test`
   5. Click `Explore data` button on the top right to query the logs.

##### Visit the following link for basic details of querying the logs

https://grafana.com/docs/loki/latest/query/log_queries/
