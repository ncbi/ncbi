<!-- Required. Provide a general summary of the issue in the title above -->

## Context

<!--  Give the necessary context for your proposal. For example, what problem will this feature solve for users? What are the use cases, benefits, and goals? -->

## Proposal

<!-- A precise statement of the proposed feature. -->

## Design

<!-- Include sketches or wireframes of the UI suggested for this feature. -->

## Acceptance criteria

<!-- Provide the criteria that should be met for this feature. These criteria must be clearly defined customer requirements aligned to NCBI’s user stories in its Statement of Work. These criteria must be testable by either user testing, unit tests or integration tests.-->

## Definition of ready

<!-- A checklist of what needs to be done to a product backlog item before the team can start implementing it in the next sprint. --> 

- [ ] BCMS User Story / Context has been well defined
- [ ] The priority of the user story is specified and agreed
- [ ] Digital assets added (design, database scheme, mockups etc if relevant)
- [ ] Coko Technical Proposal approved by NCBI
- [ ] Testable Acceptance Criteria approved by NCBI
- [ ] Estimate of effort to complete (time or points)
- [ ] The issue has been broken down into development tasks (if necessary)
- [ ] Requirements Clarified
- [ ] The product owner and development team agree that the user story is ready for development
- [ ] NCBI adds “Dev_Ready”

## Definition of done

<!-- A checklist of criteria that must be completed for a story to be considered “done.” --> 

- [ ] All coding tasks are finished and implemented
- [ ] QA approved
- [ ] Deployed and tested on “ncbidev” (by Coko team)
- [ ] Deployed and tested on “ncbi” (by NCBI team)
- [ ] Acceptance Criteria Met

## Implementation 

<!-- A description of the steps to implement the feature. To be completed by the lead dev. If there are multiple tasks, then break these down into "task" below.-->

## Alternative approaches (if applicable)

<!-- Include any alternatives to meet this use case. -->

## Scheduling

* [ ] Milestone is linked 
* [ ] Iteration is linked 
* [ ] Dependencies: ("None" or list issue numbers if relevant)
* [ ] Development estimate is added to issue time tracking


---
/label ~"feature::new"
/label ~"Backlog triage" 
/assign @suelaP

