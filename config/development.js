const { deferConfig } = require('config/defer')

const fs = require('fs')

const logDir = `logs/`

if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir, { recursive: true })
}

module.exports = {
  'pubsweet-client': {
    baseUrl: deferConfig(
      cfg =>
        `http://${cfg['pubsweet-server'].host}:${cfg['pubsweet-server'].port}`,
    ),
  },
  'pubsweet-server': {
    host: 'localhost',
    port: 3000,
    secret: 'secret-string',
    db: {
      database: 'ncbi_dev',
      host: 'localhost',
      password: 'secretpassword',
      port: 5400,
      user: 'dev',
    },
  },
}
