module.exports = {
  dbManager: {
    admin: true,
    email: 'admin@admin.com',
    password: 'testpassword',
    username: 'testusername',
  },
  'pubsweet-server': {
    db: {
      database: 'ncbi_dev_test',
      password: 'secretpassword',
      port: 5432,
      user: 'dev',
    },
    host: 'http://localhost',
    pool: {
      idleTimeoutMillis: 1000,
      max: 1,
      min: 0,
    },
    port: 3000,
    secret: 'somesecret',
  },
}
