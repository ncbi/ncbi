module.exports = [
  '@pubsweet/model-team',
  '@pubsweet/model-user',
  './server/models/book',
  './server/models/bookComponent',
  './server/models/fileVersion',
  './server/models/division',
  './server/models/error',
  './server/models/file',
  './server/models/organisation',
  './server/models/user',
  './server/models/review',
  './server/models/team',
  './server/models/activityLog',
  './server/models/channel',
  './server/models/message',
  './server/models/ncbiNotificationMessage',
  './server/models/collection',
  './server/models/toc',
  './server/models/dashboardView',
  './server/models/sourceConvertedFile',
  './server/models/issue',
  './server/models/comment',
  './server/models/bookSettingsTemplate',
  './server/services/events/eventService',
  './server/api/rest/book',
  './server/api/rest/bookComponentVersion',
  './server/api/rest/ncbiDomainService',
  './server/api/rest/collection',
  './server/api/graphql',
]
