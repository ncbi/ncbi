const permissions = {
  create: (userId, operation, object, context) => true,
  delete: async (userId, operation, object, context) => true,
  read: async (userId, operation, object, context) => true,
  update: async (userId, operation, object, context) => true,
}

module.exports = permissions
