const path = require('path')
const winston = require('winston')

const components = require('./components')
const permissions = require('../server/api/graphql/_permissions')
const bookSettings = require('./bookSettings')

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      colorize: true,
      handleExceptions: true,
      humanReadableUnhandledException: true,
      prettyPrint: true,
      timestamp: true,
    }),
  ],
})

module.exports = {
  authsome: {
    mode: path.join(__dirname, 'authsome.js'),
  },
  mailer: {
    transport: {
      secure: false,
    },
  },
  ncbi: {
    ftp: {
      host: '',
      password: '',
      user: '',
    },
  },
  apex: {
    ftp: {
      host: '',
      password: '',
      user: '',
    },
  },
  publicKeys: ['pubsweet-client'],
  pubsweet: { components },
  'pubsweet-client': {
    'login-redirect': '/organizations',
    myNcbi: 'https://www.ncbi.nlm.nih.gov/labs/account/?back_url=',
  },
  'pubsweet-server': {
    app: path.resolve(__dirname, '..', 'server', 'server.js'),
    enableExperimentalGraphql: true,
    events: [
      { readyToPublish: ['CreateBotComment'] },
      { newFileVersion: ['CreateBotComment'] },
      { messageSend: ['NotifyUsers'] },
      { BookComponentUpdated: ['CreateTocXml'] },
      { BookComponentOrderUpdated: ['CreateTocXml'] },
      { wordConversionEnd: ['CreatePreview'] },
      { xmlConversionEnd: ['CreatePreview'] },
      { pdfConversionEnd: ['CreatePreview', 'CoverLoadToPMC'] },
      { tocPreviewEnd: [] },
      { bookComponentPreviewEnd: ['Publish'] },
      { bookComponentPublishEnd: ['CreateBotComment', 'CreateTocXml'] },
      { bookPreviewEnd: ['Publish'] },
      { bookPublishEnd: ['CreateBotComment', 'CreateTocXml'] },
      { tocPublishEnd: [] },
      { SubmitEnd: [] },
      { BookUpdated: ['CreateTocXml', 'CoverLoadToPMC', 'OrderCollection'] },
      { CollectionUpdated: ['CreateTocXml', 'CoverLoadToPMC'] },
      { CollectionCreated: ['CreateTocXml'] },
      { RepeatBookComponent: ['CreateTocXml'] },
    ],
    logger,
    pool: {
      idleTimeoutMillis: 20000,
      max: 20,
      min: 0,
      createTimeoutMillis: 30000,
      acquireTimeoutMillis: 300000,
      reapIntervalMillis: 1000,
      createRetryIntervalMillis: 100,
      propagateCreateError: false,
    },
    acquireConnectionTimeout: 850000,
    tokenExpiresIn: '360 days',
    useJobQueue: false,
  },
  permissions,
  bookSettings,
  useRandomStringForDomainNames: false,
}
