/**
 * List of settings that need to be stored on our side
 *
 * You can filter using the flattenKeys function in the _helpers file:
 * pickBy(flattenKeys(values), (value, key) => BCMSSettings.includes(key))
 */

const BCMSTemplateSettings = [
  'settings.approvalOrgAdminEditor',
  'settings.approvalPreviewer',
  'settings.citationType',
  'settings.toc.add_body_to_parts',
  'settings.toc.group_chapters_in_parts',
  'settings.toc.order_chapters_by',
]
// chapterIndependently: boolean,
// createVersionLink
// formatChapterDate: stringNullable,
// formatPublicationDate: stringNullable,
// conversionWorkflow: null,
// submissionType: null,

// const BCMSBookSettings = [...BCMSTemplateSettings]

/**
 * Default values for book settings that are NOT part of the domain
 * Book settings have a longer list of acceptable values compared
 * to the templates
 */

const BCMSTemplateDefaults = {
  settings: {
    approvalOrgAdminEditor: true,
    approvalPreviewer: false,
    citationType: 'numberedParentheses',
    toc: {
      add_body_to_parts: false,
      group_chapters_in_parts: false,
      order_chapters_by: 'manual',
    },
  },
}

const BCMSBookDefaults = {
  ...BCMSTemplateDefaults,
}

const citationTypeIds = {
  harvard: 0,
  numberedSquare: 1,
  numberedParentheses: 2,
  harvardNumberedSquare: 3,
  harvardNumberedParentheses: 4,
}

/**
 * These are the default templates as provided by NCBI
 *
 * Whole-book:
 * https://preview.ncbi.nlm.nih.gov/books/domain-service/domains/11452
 *
 * Chapter-processed:
 * https://preview.ncbi.nlm.nih.gov/books/domain-service/domains/11453
 *
 */

const wholeBookTemplate = {
  id: 11452,
  properties: {
    entire_journal_open_access: false,
  },
  attributes: [
    {
      restrictions: [
        {
          id: 1,
          value: 'none',
        },
        {
          id: 2,
          value: 'disc',
        },
        {
          id: 3,
          value: 'circle',
        },
        {
          id: 4,
          value: 'square',
        },
        {
          id: 5,
          value: 'decimal',
        },
        {
          id: 6,
          value: 'lower-roman',
        },
        {
          id: 7,
          value: 'upper-roman',
        },
        {
          id: 8,
          value: 'lower-alpha',
        },
        {
          id: 9,
          value: 'upper-alpha',
        },
        {
          id: 10,
          value: 'use-source-label',
        },
        {
          id: 11,
          value: '1st-line-shifted-left',
        },
      ],
      name: 'back-reference-list-style',
      label: 'Back Reference List Style',
      description:
        'Sets the label style for reference lists without labels in the XML',
      datatype: 'controlled_list',
      value: 'decimal',
      value_id: 5,
      attribute_id: 12,
    },
    {
      restrictions: [],
      name: 'journal-url',
      label: 'Journal URL',
      description:
        'Links the cover thumb on TOC pages as well as the publisher name on TOC / chapter pages to the indicated URL',
      datatype: 'string',
      value: '',
      value_id: 16795,
      attribute_id: 21,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'blue-triangle',
        },
        {
          id: 2,
          value: 'superscripted',
        },
        {
          id: 3,
          value: 'default',
        },
        {
          id: 4,
          value: 'autodetect',
        },
      ],
      name: 'xref-ancor-style',
      label: 'Xref Anchor Style',
      description:
        'Render footnote and reference superscript if set to "superscripted"',
      datatype: 'controlled_list',
      value: 'default',
      value_id: 3,
      attribute_id: 31,
    },
    {
      restrictions: [],
      name: 'on-behalf-of-label',
      label: 'Prefix string for on-behalf-of element',
      description:
        'Overrides "On behalf of" as the prefix string for on-behalf-of element content in book or chapter level contributor information',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 44,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'navigation',
      label: 'Display links from the TOC to the book parts.',
      description:
        'Indicates restricted access: Removes TOC links, chapter cross-links, book navigation, PubReader view etc.',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 48,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'alternate-versions-pdf-book',
      label: 'Has PDF version (book level)',
      description:
        'Shows a link to the book-level PDF on TOC and chapter pages',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 49,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'alternate-versions-pdf',
      label: 'Has PDF version (chapter level)',
      description: 'Shows links to the chapter-level PDFs on chapter pages',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 50,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'alternate-versions-epub-book',
      label: 'Has EPub version (book level)',
      description: 'Not used',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 51,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'alternate-versions-epub',
      label: 'Has EPub version (chapter level)',
      description: 'Not used',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 52,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'show-corresp-icon',
      label: 'Render envelope icon for @corresp="yes"',
      description:
        'Render a small envelope icon for contribs with @corresp="yes"',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 54,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'at-end',
        },
        {
          id: 2,
          value: 'by-contrib',
        },
      ],
      name: 'aff-style',
      label: 'Affiliation style. "by-contrib" or "at-end"',
      description:
        'Sets the presentation for chapter contributor information: "At-end" produces a string of all contributors followed by all affiliations; "by contrib" shows each contributor separately and is directly followed by the respective affiliation',
      datatype: 'controlled_list',
      value: 'at-end',
      value_id: 1,
      attribute_id: 55,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'relabel-footnotes',
      label: 'Relabel footnotes',
      description:
        'Re-labels book-part level footnotes and their callouts in decimal style',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 58,
    },
    {
      restrictions: [],
      name: 'toc-max-depth',
      label: 'Maximum TOC depth',
      description: 'Indicates the level down to which TOC entries are rendered',
      datatype: 'string',
      value: '4',
      value_id: 4133,
      attribute_id: 59,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'suppress-page-toc',
      label: 'Suppress page-TOC',
      description: 'Suppresses rendering of the "In this page" portlets',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 61,
    },
    {
      restrictions: [],
      name: 'toc-link-label',
      label: 'TOC link label',
      description:
        'Overrides the label "Contents" within the navigation drop-down on chapter pages',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 64,
    },
    {
      restrictions: [],
      name: 'toc-title',
      label: 'TOC title',
      description:
        'Overrides the label "Contents" as a heading on the TOC pages and as the label for the top-left navigation drop-down on chapter pages',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 65,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'create-pdf',
      label: 'Create PDF',
      description:
        'Triggers the automatic chapter-level PDF build process during data ingest',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 66,
    },
    {
      restrictions: [],
      name: 'external-xml-source-base',
      label: 'External XML source base',
      description:
        'Indicates the path to external XML data that is to be processed and rendered together with the main book-part during request time',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 67,
    },
    {
      restrictions: [],
      name: 'external-xml-source-name',
      label: 'External XML source name',
      description:
        'Indicates the name of an external XML file that is to be processed and rendered during request time',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 68,
    },
    {
      restrictions: [],
      name: 'created-date-text',
      label: 'Created date text',
      description:
        'Sets a label for creation dates in chapter publication history on chapter pages and on TOC entries',
      datatype: 'string',
      value: 'Created:',
      value_id: 1,
      attribute_id: 70,
    },
    {
      restrictions: [],
      name: 'updated-date-text',
      label: 'Updated date text',
      description:
        'Sets a label for update dates in chapter publication history on chapter pages and on TOC entries.',
      datatype: 'string',
      value: 'Last Update:',
      value_id: 1,
      attribute_id: 71,
    },
    {
      restrictions: [],
      name: 'revised-date-text',
      label: 'Revised date text',
      description:
        'Sets a label for revision dates in chapter publication history on chapter pages and on TOC entries',
      datatype: 'string',
      value: 'Last Revision:',
      value_id: 1,
      attribute_id: 72,
    },
    {
      restrictions: [],
      name: 'navigation-panel-text',
      label: 'Navigation panel text',
      description: 'Sets the title for the "In this page" portlet',
      datatype: 'string',
      value: 'In this Page',
      value_id: 1,
      attribute_id: 73,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'create-version-link',
      label: 'Create version link',
      description: 'Indicates that a link to another version is to be rendered',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 74,
    },
    {
      restrictions: [],
      name: 'version-link-text',
      label: 'Version link text',
      description: 'Provides the link-text of a version link',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 75,
    },
    {
      restrictions: [],
      name: 'version-link-uri',
      label: 'Version link URI',
      description:
        'Provides the domain of the newer version based on which the link is to be built',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 76,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'index-book-in-pubmed',
      label: 'Index book in PubMed',
      description:
        'Indicates whether a book-level record has been submitted to PubMed or triggers submission of the record',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 77,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'index-chapter-level',
      label: 'Index chapter level in PubMed',
      description:
        'Indicates whether chapter-level records have been submitted to PubMed or triggers submission of the records',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 78,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'book-level-links',
      label: 'Book level links',
      description:
        'Indicates that an external XML file with set(s) of links is extant and to be rendered on the TOC page and every chapter page',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 79,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-contributors',
      label: 'Show contributors on TOC',
      description:
        'Renders contributor information for each chapter TOC entry on the TOC page.',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 80,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-all-title',
      label: 'Show alt-titles on TOC',
      description:
        'Renders chapter alternative titles for each chapter TOC entry on the TOC page',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 81,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-document-history',
      label: 'Show document history on TOC',
      description:
        'Renders publication history for each chapter TOC entry on the TOC page',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 82,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-pdf-icon',
      label: 'Show PDF icons on TOC',
      description:
        'Creates a link to a pdf version for each chapter TOC entry on the TOC page',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 83,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'by-contrib',
        },
        {
          id: 2,
          value: 'at-end',
        },
      ],
      name: 'toc-aff-style',
      label: 'TOC affiliation style',
      description:
        'Sets the presentation for book contributor information: "At-end" produces a string of all contributors followed by all affiliations; "by contrib" shows each contributor separately and is directly followed by the respective affiliation',
      datatype: 'controlled_list',
      value: 'at-end',
      value_id: 2,
      attribute_id: 84,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'NONE',
        },
        {
          id: 2,
          value: 'derpcollect',
        },
        {
          id: 3,
          value: 'frontcollect',
        },
        {
          id: 4,
          value: 'helpcollect',
        },
        {
          id: 5,
          value: 'jatsconcollect',
        },
        {
          id: 6,
          value: 'malariacollect',
        },
        {
          id: 7,
          value: 'napcollect',
        },
        {
          id: 8,
          value: 'nicecollect',
        },
        {
          id: 9,
          value: 'hsahcprcollect',
        },
        {
          id: 10,
          value: 'hsertacollect',
        },
        {
          id: 11,
          value: 'hsevidsyncollect',
        },
        {
          id: 12,
          value: 'apscollect',
        },
        {
          id: 13,
          value: 'hssamhsatipcollect',
        },
        {
          id: 14,
          value: 'hscompeffcollect',
        },
        {
          id: 15,
          value: 'hssurggencollect',
        },
        {
          id: 16,
          value: 'hstechrevcollect',
        },
        {
          id: 17,
          value: 'healthuscollect',
        },
        {
          id: 18,
          value: 'vaespcollect',
        },
        {
          id: 19,
          value: 'methodscollect',
        },
        {
          id: 20,
          value: 'hspepcollect',
        },
        {
          id: 21,
          value: 'hcupffcollect',
        },
        {
          id: 22,
          value: 'mcdebcollect',
        },
        {
          id: 23,
          value: 'mcispcollect',
        },
        {
          id: 24,
          value: 'hscpscollect',
        },
        {
          id: 25,
          value: 'ukhtacollect',
        },
        {
          id: 26,
          value: 'whocollect',
        },
        {
          id: 27,
          value: 'genallcollect',
        },
        {
          id: 28,
          value: 'tpcollect',
        },
        {
          id: 29,
          value: 'cadthhtacollect',
        },
        {
          id: 30,
          value: 'cadthtrcollect',
        },
        {
          id: 31,
          value: 'cadthrdcollect',
        },
        {
          id: 32,
          value: 'cadthopcollect',
        },
        {
          id: 33,
          value: 'mhuscollect',
        },
        {
          id: 34,
          value: 'cadthcdrcollect',
        },
        {
          id: 35,
          value: 'ukphrcollect',
        },
        {
          id: 36,
          value: 'ukhsdrcollect',
        },
        {
          id: 37,
          value: 'hsfrncollect',
        },
        {
          id: 38,
          value: 'hstechbriefcollect',
        },
        {
          id: 39,
          value: 'ukpgfarcollect',
        },
        {
          id: 40,
          value: 'hsahrqtacollect',
        },
        {
          id: 41,
          value: 'ukemecollect',
        },
        {
          id: 42,
          value: 'iarcmonocollect',
        },
        {
          id: 43,
          value: 'frengcollect',
        },
        {
          id: 44,
          value: 'wtcollect',
        },
        {
          id: 45,
          value: 'iarcwgrcollect',
        },
        {
          id: 46,
          value: 'whohencollect',
        },
        {
          id: 47,
          value: 'whohpscollect',
        },
        {
          id: 48,
          value: 'whopbcollect',
        },
        {
          id: 49,
          value: 'dancollect',
        },
        {
          id: 50,
          value: 'iarctpcollect',
        },
        {
          id: 51,
          value: 'rtimrcollect',
        },
        {
          id: 52,
          value: 'rtiopcollect',
        },
        {
          id: 53,
          value: 'rtirrcollect',
        },
        {
          id: 54,
          value: 'ntprrcollect',
        },
        {
          id: 55,
          value: 'ntpmonocollect',
        },
        {
          id: 56,
          value: 'ntpgmmrcollect',
        },
        {
          id: 57,
          value: 'ntpcarcollect',
        },
        {
          id: 58,
          value: 'ntptechcollect',
        },
        {
          id: 59,
          value: 'ntptoxcollect',
        },
        {
          id: 60,
          value: 'cbhsqmrcollect',
        },
        {
          id: 61,
          value: 'ahrqmfrncollect',
        },
        {
          id: 62,
          value: 'repcollect',
        },
        {
          id: 63,
          value: 'umichcollect',
        },
        {
          id: 64,
          value: 'iqwigextractcollect',
        },
        {
          id: 65,
          value: 'clsicollect',
        },
        {
          id: 66,
          value: 'ihesrcollect',
        },
        {
          id: 67,
          value: 'iarchbcollect',
        },
        {
          id: 68,
          value: 'niehsaidscollect',
        },
        {
          id: 69,
          value: 'idkdspringercollect',
        },
        {
          id: 70,
          value: 'pcoricollect',
        },
        {
          id: 71,
          value: 'nycaidscollect',
        },
        {
          id: 72,
          value: 'nichdbpcacollect',
        },
        {
          id: 73,
          value: 'ibcollect',
        },
        {
          id: 74,
          value: 'njrcollect',
        },
        {
          id: 75,
          value: 'ntpimmcollect',
        },
        {
          id: 76,
          value: 'ntpdartcollect',
        },
        {
          id: 77,
          value: 'asmpcolloqcollect',
        },
        {
          id: 78,
          value: 'consexpocollect',
        },
        {
          id: 79,
          value: 'asmpfaqcollect',
        },
        {
          id: 80,
          value: 'iarcspcollect',
        },
        {
          id: 81,
          value: 'sbuhtacollect',
        },
        {
          id: 82,
          value: 'test10collect',
        },
        {
          id: 83,
          value: 'testdfcollect',
        },
        {
          id: 84,
          value: 'testdddcollect',
        },
        {
          id: 85,
          value: 'bcmsundefinedcollect',
        },
      ],
      name: 'book-collection-name',
      label: 'Collection name',
      description:
        'Stores the name of the main collection to which the current domain belongs',
      datatype: 'controlled_list',
      value: 'NONE',
      value_id: 1,
      attribute_id: 85,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'is-test-book',
      label: 'Is test book',
      description: 'Indicates whether the current domain is only a test domain',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 86,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'is-featured-book',
      label: 'Is featured book',
      description: 'Includes the domain under "featured" books on the homepage',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 87,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'display-new-chapters',
      label: 'Display new chapters',
      description:
        'Includes new chapters added to the domain on the New & Updated page',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 88,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'archive-version',
      label: 'Archive version',
      description: 'Indicates that the book is an archived version',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 89,
    },
    {
      restrictions: [],
      name: 'archive-version-notification-text',
      label: 'Archive version notification text',
      description:
        'Overrides the generic warning message if a book is set to be archived',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 90,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'is-searchable',
      label: 'Is searchable',
      description:
        'PubMed Health only: Indicates whether the book is to be indexed',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 91,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-pub-info',
      label: 'Show publication info on TOC',
      description:
        'Renders publication statements on collection pages for each book TOC entry',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 92,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-subtitle',
      label: 'Show subtitle on TOC',
      description:
        'Renders subtitles for each chapter TOC entry on the TOC page',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 93,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'book-level-application',
      label: 'Has book level application',
      description:
        'Triggers rendering of external XHTML data for features such as a custom search box',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 94,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'table',
        },
        {
          id: 2,
          value: 'list',
        },
        {
          id: 3,
          value: 'two-col',
        },
        {
          id: 4,
          value: 'text',
        },
      ],
      name: 'toc-style',
      label: 'TOC style',
      description:
        'Indicates whether the TOC is presented in list-form or in tabular form',
      datatype: 'controlled_list',
      value: 'list',
      value_id: 2,
      attribute_id: 95,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'faq',
        },
        {
          id: 2,
          value: 'faq-with-toc',
        },
        {
          id: 3,
          value: 'normal',
        },
      ],
      name: 'question-answer-style',
      label: 'Question answer style',
      description: 'Sets the presentation style for question-answer sets',
      datatype: 'controlled_list',
      value: 'normal',
      value_id: 3,
      attribute_id: 96,
    },
    {
      restrictions: [],
      name: 'toc-expansion-level',
      label: 'TOC expansion level',
      description: 'Sets the level down to which TOC entries appear expanded',
      datatype: 'string',
      value: '4',
      value_id: 1336,
      attribute_id: 97,
    },
    {
      restrictions: [],
      name: 'book-level-links-file',
      label: 'Book level links file',
      description:
        'Provides the name of an external XML file with set(s) of links, which are to be rendered on the TOC page and every chapter page',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 98,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'book',
        },
        {
          id: 2,
          value: 'book-part',
        },
      ],
      name: 'domain-type',
      label: 'Domain type',
      description:
        'PubMed Health only: Indicates whether the domain represents one single title (book) or a collection of self-contained articles (book-part)',
      datatype: 'controlled_list',
      value: 'book',
      value_id: 1,
      attribute_id: 99,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'in-page-navigation',
      label: 'In-page navigation',
      description:
        'PubMed Health only: Shows the "Go-to" vertical in-page navigation tool',
      datatype: 'controlled_list',
      value: 'yes',
      value_id: 1,
      attribute_id: 100,
    },
    {
      restrictions: [],
      name: 'provider-logo',
      label: 'Content provider logo',
      description:
        'PubMed Health only: Provides the name of an image with the provider logo that is to be rendered on the bottom of article-like content',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 101,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'valid',
        },
        {
          id: 2,
          value: 'expired',
        },
        {
          id: 3,
          value: 'withdrawn',
        },
      ],
      name: 'validity',
      label: 'Validity',
      description: 'Indicates the archival status of the domain',
      datatype: 'controlled_list',
      value: 'valid',
      value_id: 1,
      attribute_id: 102,
    },
    {
      restrictions: [],
      name: 'citation-self-url',
      label: 'Citation self-URL',
      description:
        'Links the thumbnail on TOC pages to the book on the publisher site',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 103,
    },
    {
      restrictions: [],
      name: 'special-publisher-link-url',
      label: 'Special publisher link URL',
      description:
        'Indicates a special external link URL requested by the content provider',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 104,
    },
    {
      restrictions: [],
      name: 'special-publisher-link-text',
      label: 'Special publisher link text',
      description:
        'Indicates the link text for a special external link requested by the content provider',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 105,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'ignore-bookorder',
      label: 'Ignore bookorder file',
      description:
        'Indicates to the loader whether to use the "BookOrder file" when deleting articles',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 106,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'issue-versions',
      label: 'Has issue versions',
      description:
        'Indicates whether the book consists of issues each constituting a new version of the resource',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 107,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'ukpmc-journal',
      label: 'UKPMC Book',
      description: 'Indicates whether the book is to be included in UKPMC',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 108,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'capmc-journal',
      label: 'PMC Canada Book',
      description: 'Not used',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 109,
    },
    {
      restrictions: [],
      name: 'courtesy-note',
      label: 'Courtesy note',
      description:
        'Provides the text of a courtesy note referring to the participant who makes the content available',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 110,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'colon',
        },
        {
          id: 2,
          value: 'period',
        },
      ],
      name: 'inline-heading-separator',
      label: 'Inline heading separator',
      description:
        'Indicates the punctuation used to separate inline section headings from the section body',
      datatype: 'controlled_list',
      value: 'colon',
      value_id: 1,
      attribute_id: 111,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'document-level-indexing',
      label: 'Document level indexing',
      description:
        'Indicates that only one Entrez record is to be created per book-part (no indexing of sections and tables)',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 112,
    },
    {
      restrictions: [],
      name: 'document-collection-ids',
      label: 'Document collections ids',
      description:
        'PubMed Health only: Indicates the collection domain name that current domain belongs to',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 113,
    },
    {
      restrictions: [],
      name: 'document-publisher-id',
      label: 'Document publisher id',
      description:
        'PubMed Health only: Indicate the publisher domain name that current domain belongs to',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 114,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'default',
        },
        {
          id: 2,
          value: 'yes',
        },
        {
          id: 3,
          value: 'no',
        },
      ],
      name: 'show-pubreader',
      label: 'Show PubReader view',
      description:
        'Overrides the default behavior for when to show PubReader view',
      datatype: 'controlled_list',
      value: 'default',
      value_id: 1,
      attribute_id: 115,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'NONE',
        },
        {
          id: 2,
          value: 'Research Methodology',
        },
        {
          id: 3,
          value: 'Systematic Reviews',
        },
        {
          id: 4,
          value: 'Consumer Information',
        },
        {
          id: 5,
          value: 'Clinician Information',
        },
        {
          id: 6,
          value: 'Quality Assessments of Systematic Reviews',
        },
        {
          id: 7,
          value: 'Additional Titles',
        },
        {
          id: 9,
          value: 'Archives',
        },
      ],
      name: 'document-collection-category',
      label: 'Document collection category',
      description:
        'PubMed Health only: Indicates the collection category on a PMH publisher page',
      datatype: 'controlled_list',
      value: 'NONE',
      value_id: 1,
      attribute_id: 116,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'create-wholebook-pdf',
      label: 'Create book-level PDF',
      description:
        'Triggers the automatic book-level PDF build process during data ingest',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 117,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'unknown',
        },
        {
          id: 2,
          value: 'word',
        },
        {
          id: 3,
          value: 'pdf',
        },
        {
          id: 4,
          value: 'xml',
        },
        {
          id: 5,
          value: 'hybrid',
        },
        {
          id: 6,
          value: 'pmh',
        },
      ],
      name: 'source-data',
      label: 'Source data',
      description: 'Indicates the source format of the domain',
      datatype: 'controlled_list',
      value: 'unknown',
      value_id: 1,
      attribute_id: 118,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'show-video-player',
      label: 'Show video player',
      description: 'Shows an interactive video-player for movies',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 119,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'at-xref',
        },
        {
          id: 2,
          value: 'in-place',
        },
      ],
      name: 'display-objects-location',
      label: 'Display objects location',
      description:
        'Indicates where figures, tables, or boxes are to be rendered',
      datatype: 'controlled_list',
      value: 'at-xref',
      value_id: 1,
      attribute_id: 120,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'unknown',
        },
        {
          id: 2,
          value: 'lathrops',
        },
        {
          id: 3,
          value: 'douglassue',
        },
        {
          id: 4,
          value: 'saimacel',
        },
        {
          id: 5,
          value: 'jordandc',
        },
      ],
      name: 'owner',
      label: 'Owner',
      description: 'Indicates Bookshelf staff responsible for the domain',
      datatype: 'controlled_list',
      value: 'unknown',
      value_id: 1,
      attribute_id: 121,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'collection-auto-rebuild',
      label: 'Collection: auto-rebuild',
      description: 'Triggers nightly rebuild of collection lists',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 122,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'DEFAULT',
        },
        {
          id: 2,
          value: 'title',
        },
        {
          id: 3,
          value: 'date',
        },
        {
          id: 4,
          value: 'volume',
        },
      ],
      name: 'collection-sort',
      label: 'Collection: sort by',
      description: 'Specifies how collection lists are sorted',
      datatype: 'controlled_list',
      value: 'DEFAULT',
      value_id: 1,
      attribute_id: 123,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'NONE',
        },
        {
          id: 2,
          value: 'year',
        },
        {
          id: 3,
          value: 'volume',
        },
        {
          id: 4,
          value: 'status',
        },
        {
          id: 5,
          value: 'year-status',
        },
      ],
      name: 'collection-group',
      label: 'Collection: group by',
      description: 'Specifies how collection lists are grouped',
      datatype: 'controlled_list',
      value: 'NONE',
      value_id: 1,
      attribute_id: 124,
    },
    {
      restrictions: [
        {
          id: 1,
          value: '',
        },
        {
          id: 2,
          value: 'Monographs',
        },
        {
          id: 3,
          value: 'Systematic Reviews',
        },
        {
          id: 4,
          value: 'Clinical Guidelines',
        },
        {
          id: 5,
          value: 'Textbooks',
        },
        {
          id: 6,
          value: 'Medical Genetics Resources',
        },
        {
          id: 7,
          value: 'Toxicology Resources',
        },
        {
          id: 8,
          value: 'Documentation',
        },
        {
          id: 9,
          value: 'Methods Resources',
        },
        {
          id: 10,
          value: 'Microbiology Resources',
        },
        {
          id: 11,
          value: 'Reference Works',
        },
        {
          id: 12,
          value: 'Statistical Works',
        },
        {
          id: 101,
          value: 'Clinical Guidelines;Systematic Reviews',
        },
        {
          id: 102,
          value: 'Monographs;Toxicology Resources',
        },
        {
          id: 103,
          value: 'Medical Genetics Resources;Monographs',
        },
        {
          id: 104,
          value: 'Microbiology Resources;Monographs',
        },
        {
          id: 105,
          value: 'Medical Genetics Resources;Reference Works',
        },
        {
          id: 106,
          value: 'Microbiology Resources;Reference Works',
        },
        {
          id: 107,
          value: 'Microbiology Resources;Textbooks',
        },
        {
          id: 108,
          value: 'Systematic Reviews;Toxicology Resources',
        },
        {
          id: 109,
          value: 'Medical Genetics Resources;Systematic Reviews',
        },
        {
          id: 110,
          value: 'Reference Works;Toxicology Resources',
        },
        {
          id: 111,
          value: 'Methods Resources;Reference Works;Textbooks',
        },
        {
          id: 112,
          value: 'Reference Works;Textbooks',
        },
      ],
      name: 'bookshelf-categories',
      label: 'Bookshelf categories',
      description: 'Indicates the Bookshelf categories of the domain',
      datatype: 'controlled_list',
      value: '',
      value_id: 1,
      attribute_id: 125,
    },
    {
      restrictions: [],
      name: 'book-level-links-md',
      label: 'Book level links markdown',
      description: 'Contains the book level links as markdown',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 126,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'archive.png',
        },
        {
          id: 2,
          value: 'partially_updated.png',
        },
      ],
      name: 'archive-image',
      label: 'Archive image',
      description: 'Sets the background image on archived pages ',
      datatype: 'controlled_list',
      value: 'archive.png',
      value_id: 1,
      attribute_id: 127,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'publisher-version',
      label: 'Has publisher versions',
      description:
        'Indicates that chapter version numbers are supplied by the publisher',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 128,
    },
  ],
  domain: 'clonebookprocess',
  journal_title: 'Clone for whole-book processed book',
  pubmed_journal_title: 'Clone for whole-book processed book',
  toc_journal_title: 'Clone for whole-book processed book',
  publisher: 'bookshelf',
  nlm_id: '',
  live_in_pmc: false,
  journal_qa_status: 'preview',
}

const chapterProcessedTemplate = {
  id: 11453,
  properties: {
    entire_journal_open_access: false,
  },
  attributes: [
    {
      restrictions: [
        {
          id: 1,
          value: 'none',
        },
        {
          id: 2,
          value: 'disc',
        },
        {
          id: 3,
          value: 'circle',
        },
        {
          id: 4,
          value: 'square',
        },
        {
          id: 5,
          value: 'decimal',
        },
        {
          id: 6,
          value: 'lower-roman',
        },
        {
          id: 7,
          value: 'upper-roman',
        },
        {
          id: 8,
          value: 'lower-alpha',
        },
        {
          id: 9,
          value: 'upper-alpha',
        },
        {
          id: 10,
          value: 'use-source-label',
        },
        {
          id: 11,
          value: '1st-line-shifted-left',
        },
      ],
      name: 'back-reference-list-style',
      label: 'Back Reference List Style',
      description:
        'Sets the label style for reference lists without labels in the XML',
      datatype: 'controlled_list',
      value: 'decimal',
      value_id: 5,
      attribute_id: 12,
    },
    {
      restrictions: [],
      name: 'journal-url',
      label: 'Journal URL',
      description:
        'Links the cover thumb on TOC pages as well as the publisher name on TOC / chapter pages to the indicated URL',
      datatype: 'string',
      value: '',
      value_id: 16799,
      attribute_id: 21,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'blue-triangle',
        },
        {
          id: 2,
          value: 'superscripted',
        },
        {
          id: 3,
          value: 'default',
        },
        {
          id: 4,
          value: 'autodetect',
        },
      ],
      name: 'xref-ancor-style',
      label: 'Xref Anchor Style',
      description:
        'Render footnote and reference superscript if set to "superscripted"',
      datatype: 'controlled_list',
      value: 'default',
      value_id: 3,
      attribute_id: 31,
    },
    {
      restrictions: [],
      name: 'on-behalf-of-label',
      label: 'Prefix string for on-behalf-of element',
      description:
        'Overrides "On behalf of" as the prefix string for on-behalf-of element content in book or chapter level contributor information',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 44,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'navigation',
      label: 'Display links from the TOC to the book parts.',
      description:
        'Indicates restricted access: Removes TOC links, chapter cross-links, book navigation, PubReader view etc.',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 48,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'alternate-versions-pdf-book',
      label: 'Has PDF version (book level)',
      description:
        'Shows a link to the book-level PDF on TOC and chapter pages',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 49,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'alternate-versions-pdf',
      label: 'Has PDF version (chapter level)',
      description: 'Shows links to the chapter-level PDFs on chapter pages',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 50,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'alternate-versions-epub-book',
      label: 'Has EPub version (book level)',
      description: 'Not used',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 51,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'alternate-versions-epub',
      label: 'Has EPub version (chapter level)',
      description: 'Not used',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 52,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'show-corresp-icon',
      label: 'Render envelope icon for @corresp="yes"',
      description:
        'Render a small envelope icon for contribs with @corresp="yes"',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 54,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'at-end',
        },
        {
          id: 2,
          value: 'by-contrib',
        },
      ],
      name: 'aff-style',
      label: 'Affiliation style. "by-contrib" or "at-end"',
      description:
        'Sets the presentation for chapter contributor information: "At-end" produces a string of all contributors followed by all affiliations; "by contrib" shows each contributor separately and is directly followed by the respective affiliation',
      datatype: 'controlled_list',
      value: 'at-end',
      value_id: 1,
      attribute_id: 55,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'relabel-footnotes',
      label: 'Relabel footnotes',
      description:
        'Re-labels book-part level footnotes and their callouts in decimal style',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 58,
    },
    {
      restrictions: [],
      name: 'toc-max-depth',
      label: 'Maximum TOC depth',
      description: 'Indicates the level down to which TOC entries are rendered',
      datatype: 'string',
      value: '4',
      value_id: 4154,
      attribute_id: 59,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'suppress-page-toc',
      label: 'Suppress page-TOC',
      description: 'Suppresses rendering of the "In this page" portlets',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 61,
    },
    {
      restrictions: [],
      name: 'toc-link-label',
      label: 'TOC link label',
      description:
        'Overrides the label "Contents" within the navigation drop-down on chapter pages',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 64,
    },
    {
      restrictions: [],
      name: 'toc-title',
      label: 'TOC title',
      description:
        'Overrides the label "Contents" as a heading on the TOC pages and as the label for the top-left navigation drop-down on chapter pages',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 65,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'create-pdf',
      label: 'Create PDF',
      description:
        'Triggers the automatic chapter-level PDF build process during data ingest',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 66,
    },
    {
      restrictions: [],
      name: 'external-xml-source-base',
      label: 'External XML source base',
      description:
        'Indicates the path to external XML data that is to be processed and rendered together with the main book-part during request time',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 67,
    },
    {
      restrictions: [],
      name: 'external-xml-source-name',
      label: 'External XML source name',
      description:
        'Indicates the name of an external XML file that is to be processed and rendered during request time',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 68,
    },
    {
      restrictions: [],
      name: 'created-date-text',
      label: 'Created date text',
      description:
        'Sets a label for creation dates in chapter publication history on chapter pages and on TOC entries',
      datatype: 'string',
      value: 'Created:',
      value_id: 1,
      attribute_id: 70,
    },
    {
      restrictions: [],
      name: 'updated-date-text',
      label: 'Updated date text',
      description:
        'Sets a label for update dates in chapter publication history on chapter pages and on TOC entries.',
      datatype: 'string',
      value: 'Last Update:',
      value_id: 1,
      attribute_id: 71,
    },
    {
      restrictions: [],
      name: 'revised-date-text',
      label: 'Revised date text',
      description:
        'Sets a label for revision dates in chapter publication history on chapter pages and on TOC entries',
      datatype: 'string',
      value: 'Last Revision:',
      value_id: 1,
      attribute_id: 72,
    },
    {
      restrictions: [],
      name: 'navigation-panel-text',
      label: 'Navigation panel text',
      description: 'Sets the title for the "In this page" portlet',
      datatype: 'string',
      value: 'In this Page',
      value_id: 1,
      attribute_id: 73,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'create-version-link',
      label: 'Create version link',
      description: 'Indicates that a link to another version is to be rendered',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 74,
    },
    {
      restrictions: [],
      name: 'version-link-text',
      label: 'Version link text',
      description: 'Provides the link-text of a version link',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 75,
    },
    {
      restrictions: [],
      name: 'version-link-uri',
      label: 'Version link URI',
      description:
        'Provides the domain of the newer version based on which the link is to be built',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 76,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'index-book-in-pubmed',
      label: 'Index book in PubMed',
      description:
        'Indicates whether a book-level record has been submitted to PubMed or triggers submission of the record',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 77,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'index-chapter-level',
      label: 'Index chapter level in PubMed',
      description:
        'Indicates whether chapter-level records have been submitted to PubMed or triggers submission of the records',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 78,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'book-level-links',
      label: 'Book level links',
      description:
        'Indicates that an external XML file with set(s) of links is extant and to be rendered on the TOC page and every chapter page',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 79,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-contributors',
      label: 'Show contributors on TOC',
      description:
        'Renders contributor information for each chapter TOC entry on the TOC page.',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 80,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-all-title',
      label: 'Show alt-titles on TOC',
      description:
        'Renders chapter alternative titles for each chapter TOC entry on the TOC page',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 81,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-document-history',
      label: 'Show document history on TOC',
      description:
        'Renders publication history for each chapter TOC entry on the TOC page',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 82,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-pdf-icon',
      label: 'Show PDF icons on TOC',
      description:
        'Creates a link to a pdf version for each chapter TOC entry on the TOC page',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 83,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'by-contrib',
        },
        {
          id: 2,
          value: 'at-end',
        },
      ],
      name: 'toc-aff-style',
      label: 'TOC affiliation style',
      description:
        'Sets the presentation for book contributor information: "At-end" produces a string of all contributors followed by all affiliations; "by contrib" shows each contributor separately and is directly followed by the respective affiliation',
      datatype: 'controlled_list',
      value: 'at-end',
      value_id: 2,
      attribute_id: 84,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'NONE',
        },
        {
          id: 2,
          value: 'derpcollect',
        },
        {
          id: 3,
          value: 'frontcollect',
        },
        {
          id: 4,
          value: 'helpcollect',
        },
        {
          id: 5,
          value: 'jatsconcollect',
        },
        {
          id: 6,
          value: 'malariacollect',
        },
        {
          id: 7,
          value: 'napcollect',
        },
        {
          id: 8,
          value: 'nicecollect',
        },
        {
          id: 9,
          value: 'hsahcprcollect',
        },
        {
          id: 10,
          value: 'hsertacollect',
        },
        {
          id: 11,
          value: 'hsevidsyncollect',
        },
        {
          id: 12,
          value: 'apscollect',
        },
        {
          id: 13,
          value: 'hssamhsatipcollect',
        },
        {
          id: 14,
          value: 'hscompeffcollect',
        },
        {
          id: 15,
          value: 'hssurggencollect',
        },
        {
          id: 16,
          value: 'hstechrevcollect',
        },
        {
          id: 17,
          value: 'healthuscollect',
        },
        {
          id: 18,
          value: 'vaespcollect',
        },
        {
          id: 19,
          value: 'methodscollect',
        },
        {
          id: 20,
          value: 'hspepcollect',
        },
        {
          id: 21,
          value: 'hcupffcollect',
        },
        {
          id: 22,
          value: 'mcdebcollect',
        },
        {
          id: 23,
          value: 'mcispcollect',
        },
        {
          id: 24,
          value: 'hscpscollect',
        },
        {
          id: 25,
          value: 'ukhtacollect',
        },
        {
          id: 26,
          value: 'whocollect',
        },
        {
          id: 27,
          value: 'genallcollect',
        },
        {
          id: 28,
          value: 'tpcollect',
        },
        {
          id: 29,
          value: 'cadthhtacollect',
        },
        {
          id: 30,
          value: 'cadthtrcollect',
        },
        {
          id: 31,
          value: 'cadthrdcollect',
        },
        {
          id: 32,
          value: 'cadthopcollect',
        },
        {
          id: 33,
          value: 'mhuscollect',
        },
        {
          id: 34,
          value: 'cadthcdrcollect',
        },
        {
          id: 35,
          value: 'ukphrcollect',
        },
        {
          id: 36,
          value: 'ukhsdrcollect',
        },
        {
          id: 37,
          value: 'hsfrncollect',
        },
        {
          id: 38,
          value: 'hstechbriefcollect',
        },
        {
          id: 39,
          value: 'ukpgfarcollect',
        },
        {
          id: 40,
          value: 'hsahrqtacollect',
        },
        {
          id: 41,
          value: 'ukemecollect',
        },
        {
          id: 42,
          value: 'iarcmonocollect',
        },
        {
          id: 43,
          value: 'frengcollect',
        },
        {
          id: 44,
          value: 'wtcollect',
        },
        {
          id: 45,
          value: 'iarcwgrcollect',
        },
        {
          id: 46,
          value: 'whohencollect',
        },
        {
          id: 47,
          value: 'whohpscollect',
        },
        {
          id: 48,
          value: 'whopbcollect',
        },
        {
          id: 49,
          value: 'dancollect',
        },
        {
          id: 50,
          value: 'iarctpcollect',
        },
        {
          id: 51,
          value: 'rtimrcollect',
        },
        {
          id: 52,
          value: 'rtiopcollect',
        },
        {
          id: 53,
          value: 'rtirrcollect',
        },
        {
          id: 54,
          value: 'ntprrcollect',
        },
        {
          id: 55,
          value: 'ntpmonocollect',
        },
        {
          id: 56,
          value: 'ntpgmmrcollect',
        },
        {
          id: 57,
          value: 'ntpcarcollect',
        },
        {
          id: 58,
          value: 'ntptechcollect',
        },
        {
          id: 59,
          value: 'ntptoxcollect',
        },
        {
          id: 60,
          value: 'cbhsqmrcollect',
        },
        {
          id: 61,
          value: 'ahrqmfrncollect',
        },
        {
          id: 62,
          value: 'repcollect',
        },
        {
          id: 63,
          value: 'umichcollect',
        },
        {
          id: 64,
          value: 'iqwigextractcollect',
        },
        {
          id: 65,
          value: 'clsicollect',
        },
        {
          id: 66,
          value: 'ihesrcollect',
        },
        {
          id: 67,
          value: 'iarchbcollect',
        },
        {
          id: 68,
          value: 'niehsaidscollect',
        },
        {
          id: 69,
          value: 'idkdspringercollect',
        },
        {
          id: 70,
          value: 'pcoricollect',
        },
        {
          id: 71,
          value: 'nycaidscollect',
        },
        {
          id: 72,
          value: 'nichdbpcacollect',
        },
        {
          id: 73,
          value: 'ibcollect',
        },
        {
          id: 74,
          value: 'njrcollect',
        },
        {
          id: 75,
          value: 'ntpimmcollect',
        },
        {
          id: 76,
          value: 'ntpdartcollect',
        },
        {
          id: 77,
          value: 'asmpcolloqcollect',
        },
        {
          id: 78,
          value: 'consexpocollect',
        },
        {
          id: 79,
          value: 'asmpfaqcollect',
        },
        {
          id: 80,
          value: 'iarcspcollect',
        },
        {
          id: 81,
          value: 'sbuhtacollect',
        },
        {
          id: 82,
          value: 'test10collect',
        },
        {
          id: 83,
          value: 'testdfcollect',
        },
        {
          id: 84,
          value: 'testdddcollect',
        },
        {
          id: 85,
          value: 'bcmsundefinedcollect',
        },
      ],
      name: 'book-collection-name',
      label: 'Collection name',
      description:
        'Stores the name of the main collection to which the current domain belongs',
      datatype: 'controlled_list',
      value: 'NONE',
      value_id: 1,
      attribute_id: 85,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'is-test-book',
      label: 'Is test book',
      description: 'Indicates whether the current domain is only a test domain',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 86,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'is-featured-book',
      label: 'Is featured book',
      description: 'Includes the domain under "featured" books on the homepage',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 87,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'display-new-chapters',
      label: 'Display new chapters',
      description:
        'Includes new chapters added to the domain on the New & Updated page',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 88,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'archive-version',
      label: 'Archive version',
      description: 'Indicates that the book is an archived version',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 89,
    },
    {
      restrictions: [],
      name: 'archive-version-notification-text',
      label: 'Archive version notification text',
      description:
        'Overrides the generic warning message if a book is set to be archived',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 90,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'is-searchable',
      label: 'Is searchable',
      description:
        'PubMed Health only: Indicates whether the book is to be indexed',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 91,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-pub-info',
      label: 'Show publication info on TOC',
      description:
        'Renders publication statements on collection pages for each book TOC entry',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 92,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'toc-subtitle',
      label: 'Show subtitle on TOC',
      description:
        'Renders subtitles for each chapter TOC entry on the TOC page',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 93,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'book-level-application',
      label: 'Has book level application',
      description:
        'Triggers rendering of external XHTML data for features such as a custom search box',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 94,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'table',
        },
        {
          id: 2,
          value: 'list',
        },
        {
          id: 3,
          value: 'two-col',
        },
        {
          id: 4,
          value: 'text',
        },
      ],
      name: 'toc-style',
      label: 'TOC style',
      description:
        'Indicates whether the TOC is presented in list-form or in tabular form',
      datatype: 'controlled_list',
      value: 'list',
      value_id: 2,
      attribute_id: 95,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'faq',
        },
        {
          id: 2,
          value: 'faq-with-toc',
        },
        {
          id: 3,
          value: 'normal',
        },
      ],
      name: 'question-answer-style',
      label: 'Question answer style',
      description: 'Sets the presentation style for question-answer sets',
      datatype: 'controlled_list',
      value: 'normal',
      value_id: 3,
      attribute_id: 96,
    },
    {
      restrictions: [],
      name: 'toc-expansion-level',
      label: 'TOC expansion level',
      description: 'Sets the level down to which TOC entries appear expanded',
      datatype: 'string',
      value: '4',
      value_id: 1341,
      attribute_id: 97,
    },
    {
      restrictions: [],
      name: 'book-level-links-file',
      label: 'Book level links file',
      description:
        'Provides the name of an external XML file with set(s) of links, which are to be rendered on the TOC page and every chapter page',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 98,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'book',
        },
        {
          id: 2,
          value: 'book-part',
        },
      ],
      name: 'domain-type',
      label: 'Domain type',
      description:
        'PubMed Health only: Indicates whether the domain represents one single title (book) or a collection of self-contained articles (book-part)',
      datatype: 'controlled_list',
      value: 'book',
      value_id: 1,
      attribute_id: 99,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'in-page-navigation',
      label: 'In-page navigation',
      description:
        'PubMed Health only: Shows the "Go-to" vertical in-page navigation tool',
      datatype: 'controlled_list',
      value: 'yes',
      value_id: 1,
      attribute_id: 100,
    },
    {
      restrictions: [],
      name: 'provider-logo',
      label: 'Content provider logo',
      description:
        'PubMed Health only: Provides the name of an image with the provider logo that is to be rendered on the bottom of article-like content',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 101,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'valid',
        },
        {
          id: 2,
          value: 'expired',
        },
        {
          id: 3,
          value: 'withdrawn',
        },
      ],
      name: 'validity',
      label: 'Validity',
      description: 'Indicates the archival status of the domain',
      datatype: 'controlled_list',
      value: 'valid',
      value_id: 1,
      attribute_id: 102,
    },
    {
      restrictions: [],
      name: 'citation-self-url',
      label: 'Citation self-URL',
      description:
        'Links the thumbnail on TOC pages to the book on the publisher site',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 103,
    },
    {
      restrictions: [],
      name: 'special-publisher-link-url',
      label: 'Special publisher link URL',
      description:
        'Indicates a special external link URL requested by the content provider',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 104,
    },
    {
      restrictions: [],
      name: 'special-publisher-link-text',
      label: 'Special publisher link text',
      description:
        'Indicates the link text for a special external link requested by the content provider',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 105,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'ignore-bookorder',
      label: 'Ignore bookorder file',
      description:
        'Indicates to the loader whether to use the "BookOrder file" when deleting articles',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 106,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'issue-versions',
      label: 'Has issue versions',
      description:
        'Indicates whether the book consists of issues each constituting a new version of the resource',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 107,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'ukpmc-journal',
      label: 'UKPMC Book',
      description: 'Indicates whether the book is to be included in UKPMC',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 108,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'capmc-journal',
      label: 'PMC Canada Book',
      description: 'Not used',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 109,
    },
    {
      restrictions: [],
      name: 'courtesy-note',
      label: 'Courtesy note',
      description:
        'Provides the text of a courtesy note referring to the participant who makes the content available',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 110,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'colon',
        },
        {
          id: 2,
          value: 'period',
        },
      ],
      name: 'inline-heading-separator',
      label: 'Inline heading separator',
      description:
        'Indicates the punctuation used to separate inline section headings from the section body',
      datatype: 'controlled_list',
      value: 'colon',
      value_id: 1,
      attribute_id: 111,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'document-level-indexing',
      label: 'Document level indexing',
      description:
        'Indicates that only one Entrez record is to be created per book-part (no indexing of sections and tables)',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 112,
    },
    {
      restrictions: [],
      name: 'document-collection-ids',
      label: 'Document collections ids',
      description:
        'PubMed Health only: Indicates the collection domain name that current domain belongs to',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 113,
    },
    {
      restrictions: [],
      name: 'document-publisher-id',
      label: 'Document publisher id',
      description:
        'PubMed Health only: Indicate the publisher domain name that current domain belongs to',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 114,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'default',
        },
        {
          id: 2,
          value: 'yes',
        },
        {
          id: 3,
          value: 'no',
        },
      ],
      name: 'show-pubreader',
      label: 'Show PubReader view',
      description:
        'Overrides the default behavior for when to show PubReader view',
      datatype: 'controlled_list',
      value: 'default',
      value_id: 1,
      attribute_id: 115,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'NONE',
        },
        {
          id: 2,
          value: 'Research Methodology',
        },
        {
          id: 3,
          value: 'Systematic Reviews',
        },
        {
          id: 4,
          value: 'Consumer Information',
        },
        {
          id: 5,
          value: 'Clinician Information',
        },
        {
          id: 6,
          value: 'Quality Assessments of Systematic Reviews',
        },
        {
          id: 7,
          value: 'Additional Titles',
        },
        {
          id: 9,
          value: 'Archives',
        },
      ],
      name: 'document-collection-category',
      label: 'Document collection category',
      description:
        'PubMed Health only: Indicates the collection category on a PMH publisher page',
      datatype: 'controlled_list',
      value: 'NONE',
      value_id: 1,
      attribute_id: 116,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'create-wholebook-pdf',
      label: 'Create book-level PDF',
      description:
        'Triggers the automatic book-level PDF build process during data ingest',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 117,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'unknown',
        },
        {
          id: 2,
          value: 'word',
        },
        {
          id: 3,
          value: 'pdf',
        },
        {
          id: 4,
          value: 'xml',
        },
        {
          id: 5,
          value: 'hybrid',
        },
        {
          id: 6,
          value: 'pmh',
        },
      ],
      name: 'source-data',
      label: 'Source data',
      description: 'Indicates the source format of the domain',
      datatype: 'controlled_list',
      value: 'unknown',
      value_id: 1,
      attribute_id: 118,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'show-video-player',
      label: 'Show video player',
      description: 'Shows an interactive video-player for movies',
      datatype: 'boolean',
      value: 'yes',
      value_id: 1,
      attribute_id: 119,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'at-xref',
        },
        {
          id: 2,
          value: 'in-place',
        },
      ],
      name: 'display-objects-location',
      label: 'Display objects location',
      description:
        'Indicates where figures, tables, or boxes are to be rendered',
      datatype: 'controlled_list',
      value: 'at-xref',
      value_id: 1,
      attribute_id: 120,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'unknown',
        },
        {
          id: 2,
          value: 'lathrops',
        },
        {
          id: 3,
          value: 'douglassue',
        },
        {
          id: 4,
          value: 'saimacel',
        },
        {
          id: 5,
          value: 'jordandc',
        },
      ],
      name: 'owner',
      label: 'Owner',
      description: 'Indicates Bookshelf staff responsible for the domain',
      datatype: 'controlled_list',
      value: 'unknown',
      value_id: 1,
      attribute_id: 121,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'collection-auto-rebuild',
      label: 'Collection: auto-rebuild',
      description: 'Triggers nightly rebuild of collection lists',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 122,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'DEFAULT',
        },
        {
          id: 2,
          value: 'title',
        },
        {
          id: 3,
          value: 'date',
        },
        {
          id: 4,
          value: 'volume',
        },
      ],
      name: 'collection-sort',
      label: 'Collection: sort by',
      description: 'Specifies how collection lists are sorted',
      datatype: 'controlled_list',
      value: 'DEFAULT',
      value_id: 1,
      attribute_id: 123,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'NONE',
        },
        {
          id: 2,
          value: 'year',
        },
        {
          id: 3,
          value: 'volume',
        },
        {
          id: 4,
          value: 'status',
        },
        {
          id: 5,
          value: 'year-status',
        },
      ],
      name: 'collection-group',
      label: 'Collection: group by',
      description: 'Specifies how collection lists are grouped',
      datatype: 'controlled_list',
      value: 'NONE',
      value_id: 1,
      attribute_id: 124,
    },
    {
      restrictions: [
        {
          id: 1,
          value: '',
        },
        {
          id: 2,
          value: 'Monographs',
        },
        {
          id: 3,
          value: 'Systematic Reviews',
        },
        {
          id: 4,
          value: 'Clinical Guidelines',
        },
        {
          id: 5,
          value: 'Textbooks',
        },
        {
          id: 6,
          value: 'Medical Genetics Resources',
        },
        {
          id: 7,
          value: 'Toxicology Resources',
        },
        {
          id: 8,
          value: 'Documentation',
        },
        {
          id: 9,
          value: 'Methods Resources',
        },
        {
          id: 10,
          value: 'Microbiology Resources',
        },
        {
          id: 11,
          value: 'Reference Works',
        },
        {
          id: 12,
          value: 'Statistical Works',
        },
        {
          id: 101,
          value: 'Clinical Guidelines;Systematic Reviews',
        },
        {
          id: 102,
          value: 'Monographs;Toxicology Resources',
        },
        {
          id: 103,
          value: 'Medical Genetics Resources;Monographs',
        },
        {
          id: 104,
          value: 'Microbiology Resources;Monographs',
        },
        {
          id: 105,
          value: 'Medical Genetics Resources;Reference Works',
        },
        {
          id: 106,
          value: 'Microbiology Resources;Reference Works',
        },
        {
          id: 107,
          value: 'Microbiology Resources;Textbooks',
        },
        {
          id: 108,
          value: 'Systematic Reviews;Toxicology Resources',
        },
        {
          id: 109,
          value: 'Medical Genetics Resources;Systematic Reviews',
        },
        {
          id: 110,
          value: 'Reference Works;Toxicology Resources',
        },
        {
          id: 111,
          value: 'Methods Resources;Reference Works;Textbooks',
        },
        {
          id: 112,
          value: 'Reference Works;Textbooks',
        },
      ],
      name: 'bookshelf-categories',
      label: 'Bookshelf categories',
      description: 'Indicates the Bookshelf categories of the domain',
      datatype: 'controlled_list',
      value: '',
      value_id: 1,
      attribute_id: 125,
    },
    {
      restrictions: [],
      name: 'book-level-links-md',
      label: 'Book level links markdown',
      description: 'Contains the book level links as markdown',
      datatype: 'string',
      value: '',
      value_id: 1,
      attribute_id: 126,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'archive.png',
        },
        {
          id: 2,
          value: 'partially_updated.png',
        },
      ],
      name: 'archive-image',
      label: 'Archive image',
      description: 'Sets the background image on archived pages ',
      datatype: 'controlled_list',
      value: 'archive.png',
      value_id: 1,
      attribute_id: 127,
    },
    {
      restrictions: [
        {
          id: 1,
          value: 'yes',
        },
        {
          id: 2,
          value: 'no',
        },
      ],
      name: 'publisher-version',
      label: 'Has publisher versions',
      description:
        'Indicates that chapter version numbers are supplied by the publisher',
      datatype: 'boolean',
      value: 'no',
      value_id: 2,
      attribute_id: 128,
    },
  ],
  domain: 'clonechapterprocess',
  journal_title: 'Clone for chapter-process books',
  pubmed_journal_title: 'Clone for chapter-process books',
  toc_journal_title: 'Clone for chapter-process books',
  publisher: 'bookshelf',
  nlm_id: '',
  live_in_pmc: false,
  journal_qa_status: 'preview',
}

module.exports = {
  BCMSTemplateDefaults,
  BCMSBookDefaults,
  BCMSTemplateSettings,
  citationTypeIds,
  domainTemplates: {
    chapterProcessed: chapterProcessedTemplate,
    wholeBook: wholeBookTemplate,
  },
  domainTemplatePublisher: 'bookshelf',
}
