const { deferConfig } = require('config/defer')

module.exports = {
  'pubsweet-client': {
    baseUrl: deferConfig(cfg => cfg['pubsweet-server'].publicUrl),
  },
}
