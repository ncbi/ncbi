/* eslint sort-keys: ["error", "asc", {caseSensitive: false}] */

module.exports = {
  book: {
    addBodyContentToParts: 'Add body content to parts',
    addBookToCollection: 'Add book to collection',
    altTitle: 'Alternative title',
    approvalOrgAdminEditor: 'Org Admin or Editor',
    approvalPreviewer: 'Previewers',
    authors: 'Author(s)',
    bookLevelAffiliationStyle: {
      'at-end': 'At end',
      'by-contrib': 'By contrib',
      name: 'Book-level affiliation style',
    },
    chapterId: 'Chapter ID',
    chapterLevelAffiliationStyle: {
      'at-end': 'At end',
      'by-contrib': 'By contrib',
      name: 'Chapter-level affiliation style',
    },
    chapterleveldates: 'Chapter level dates',
    chapterProcessed: 'Chapter-processed',
    citationType: {
      harvard: 'Harvard',
      harvardNumberedParentheses: 'Harvard numbered parentheses',
      harvardNumberedSquare: 'Harvard numbered square',
      name: 'Citation style',
      numberedParentheses: 'Numbered parentheses',
      numberedSquare: 'Numbered square',
    },
    contentSubmition: 'Content submission',
    contentType: {
      authorManuscript: 'Author manuscript',
      name: 'Funded content type',
      prepublicationDraft: 'Prepublication draft',
      publishedPDF: 'Published PDF',
    },
    contributors: 'Contributors',
    conversionWorkflow: {
      name: 'Conversion workflow',
      pdf: 'PDF',
      word: 'Word',
      xml: 'XML',
    },
    createdisplayPDF: 'Create and display PDF for download',
    dateFormats: 'Date Formats',
    displayChapterMetadata: 'Display metadata on TOC',
    displayHeadings: {
      chapterTitle: 'Chapter title only',
      chapterTitleMainSectionHeadings:
        'Chapter title and main section headings',
      name: 'Display headings depth',
    },
    displayObjectsLocation: {
      'at-xref': 'At xref',
      'in-place': 'In place',
      name: 'Display objects location',
    },
    displayPDF: 'Display PDF for download',
    eachChapter: 'Each chapter',
    formatChapterDate: 'Chapter publication date format',
    formatPublicationDate: 'Book publication date format',
    groupChaptersIntoParts: 'Group chapters into parts',
    individualchapters: ' Individual chapters',
    label: 'Label',
    orderChaptersBy: {
      chapterNumber: 'Document label – ascending',
      chapterNumberDesc: 'Document label – descending',
      dateCreated: 'Date created',
      dateDesc: 'Most recent',
      dateUpdated: 'Date updated',
      manual: 'Manual order',
      name: 'Order chapters by',
      title: 'A > Z',
    },
    publishChaptersIndependently: 'Publish chapters independently',
    publishingSettings: 'Publishing Settings',
    questionAnswerStyle: {
      faq: 'FAQ',
      'faq-with-toc': 'FAQ with TOC',
      name: 'Question and answer style',
      normal: 'Normal',
    },
    referenceListStyle: {
      '1st-line-shifted-left': '1st line shifted-left',
      circle: 'Circle',
      decimal: 'decimal',
      disc: 'Disc',
      'lower-alpha': 'Lower alpha',
      'lower-roman': 'Lower roman',
      name: 'Reference list style',
      none: 'None',
      square: 'Square',
      'upper-alpha': 'Upper alpha',
      'upper-roman': 'Upper roman',
      'use-source-label': 'Use source label',
    },
    requireApprovalPreview: 'Require approval before publishing',
    submissionType: {
      // wholeBook and chapterProcessed also exist on the root level of book
      chapterProcessed: 'Individual chapters',
      name: 'Submission type',
      wholeBook: 'Complete books and documents',
    },
    subtitle: 'Subtitle',
    supportMultiplePublishedVersions: 'Support multiple published versions',
    tableOfContents: 'Table of contents settings',
    title: 'Book title',
    titleInformation: 'Title information',
    wholeBook: 'Whole book',
    xrefAnchorStyle: {
      autodetect: 'Autodetect',
      'blue-triangle': 'Blue triangle',
      default: 'Default',
      name: 'Xref anchor style',
      superscripted: 'Superscripted',
    },
  },
  bookComponent: {
    status: {
      approve: 'Approve',
      converting: 'Converting',
      error: 'Errors',
      'in-review': 'Request Review',
      'loading-preview': 'Loading preview',
      preview: 'Previewing',
      published: 'Published',
      requestChanges: 'Request Changes',
    },
  },
  confirm: {
    deleteOrg: 'Are you sure you want to delete organization?',
    deleteOrgs: 'Are you sure you want to delete organizations?',
    publishBook: 'Are you sure you want to publish',
  },
  descending: 'Descending',
  emDash: '<em>&mdash;</em>',
  enDash: '<em>&ndash;</em>',
  fieldDescriptions: {
    bookSettingsForm: {
      addBodyToParts:
        'Manually add body content at the part-level. Note this content will not be included in PubMed as substantive content records',
      applyAwardBook:
        'When on, the related field is not editable at the chapter level.',
      applyAwardCollection:
        'When on, the related field is not editable at the book level.',
      bookLevelAffiliationStyle:
        'Sets the presentation for book contributor information: "At-end" produces a string of all contributors followed by all affiliations; "by contrib" shows each contributor separately and is directly followed by the respective affiliation',
      bookLevelLinks:
        'Displays links of related documents and resources to  appear on the table of contents and chapter pages',
      bookLevelLinksMarkdown: 'Contains the book level links as markdown',
      chapterLevelAffiliationStyle:
        'Sets the presentation for chapter contributor information: "At-end" produces a string of all contributors followed by all affiliations; "by contrib" shows each contributor separately and is directly followed by the respective affiliation',
      citationSelfUrl:
        'Links the thumbnail on TOC pages to the book on the publisher site',
      citationType:
        'Sets the citiation style. Only relevant to books in the Word workflow.',
      collection:
        'Stores the name of the main collection to which the current domain belongs',
      contentType:
        'The  format and publishing status of the submitted content in the publisher’s workflow',
      conversionWorkflow:
        'Whether conversion to the Bookshelf stylized XML is done from submitted source XML, PDF, or Word file(s)',
      createBookLevelPdf:
        'Triggers the automatic book-level PDF build process during data ingest. This setting is only relevant to books in the Word conversion workflow and funded Author manuscripts.',
      createChapterLevelPdf:
        'Triggers the automatic chapter-level PDF build process during data ingest. This setting is only relevant to books in the Word conversion workflow and funded Author manuscripts.',
      createVersionLink:
        'Indicates that a link to another version is to be rendered',
      displayBookLevelPdf:
        'Shows a link to the book-level PDF on TOC and chapter pages',
      displayChapterLevelPdf:
        'Shows links to the chapter-level PDFs on chapter pages',
      displayHeadingLevel:
        'Indicates the level down to which TOC entries are rendered',
      displayObjectsLocation:
        'Indicates where figures, tables, or boxes are to be rendered',
      footnotesDecimal:
        'Re-labels chapter-level footnotes and their callouts in decimal style',
      groupChaptersInParts: `Manually create part-level headings in the body of a book's table of contents. Chapters are sorted within parts per the selected chapter ordering setting.`,
      indexChaptersInPubmed:
        'Indicates whether chapter-level records have been submitted to PubMed or triggers submission of the records',
      indexInPubmed:
        'Indicates whether a book-level record has been submitted to PubMed or triggers submission of the record',
      openAccessStatus:
        'Whether submitted content is permitted by a Bookshelf agreement to be available to the public for download and reuse in the Bookshelf Open Access Subset',
      orderChaptersBy:
        "Sets the value for how chapters are ordered within the body of a book's table of contents, or within a part in the body of a book's table of contents",
      publisherUrl:
        'Links the cover image and the publisher name on the table of contents and chapter pages to the indicated URL',
      questionAnswerStyle:
        'Sets the presentation style for question-answer sets',
      referenceListStyle:
        'Sets the display style for reference citations in reference lists, such as indented or numbered (decimal) lists',
      releaseStatus:
        'Whether content is only available on Preview site(s) for QA or is released to the live Bookshelf website for public access',
      requireApprovalByOrgAdminOrEditor:
        'An editor or a participating organization or publisher is required to approve a preview of submitted content before it is released live to the Bookshelf site for public access',
      requireApprovalByPreviewers:
        'A previewer selected by a submitter is required to approve a preview of submitted content before it is released live to the Bookshelf site for public access',
      specialPublisherLinkText:
        'Indicates the text for the special publisher link URL, such as a DOI, requested by the content provider to display under the bibliographic information on Bookshelf',
      specialPublisherLinkUrl:
        'Indicates a special publisher link URL, such as a DOI, requested by the content provider to display under the bibliographic information on Bookshelf',
      submissionType:
        'Whether content is submitted, converted, and added to Bookshelf as individual chapters or as whole books',
      supportMultiplePublishedVersions:
        'Indicates that publisher will communicate published versions of chapters through the BCMS or in supplied FTP deposits',
      tocAltTitle:
        'Renders chapter alternative titles for each chapter on the table of contents',
      tocChapterLevelDates:
        'Renders publication history for each chapter on the table of contents',
      tocContributors:
        'Renders contributor information for each chapter on the table of contents',
      tocExpansionLevel:
        'Sets the level down to which TOC entries appear expanded',
      tocSubtitle:
        'Renders subtitles for each chapter on the table of contents',
      UKPMC: 'Indicates whether the book is to be included in UKPMC',
      versionInput:
        'The version number of the scientific update of the content in the publisher’s publishing workflow. Not the version number on the Bookshelf website.',
      versionLinkText: 'Provides the link-text of a version link',
      versionLinkUri:
        'Provides the domain of the newer version based on which the link is to be built',
      xrefAnchorStyle:
        'Displays style of cross-references to footnotes and citations, such as superscripted',
    },
    collectionForm: {
      citationSelfUrl:
        'Links the thumbnail on TOC pages to the book on the publisher site ',
      groupBooksBy:
        'Select the category by which you would like to group the contents of your collection. Options include auto headings (Year, Volume, Current – Archived, or Year and Current-Archived), or the ability to manually customize your own headings',
      groupBooksOnTOC:
        'Permits the ability to organize your collection contents by grouping them under the heading type of your choice',
      openAccess:
        'Whether submitted content is permitted by a Bookshelf agreement to be available to the public for download and reuse in the Bookshelf Open Access Subset',
      orderBooksBy:
        'Sets the value by which books are ordered within the collection’s contents, or within a group in the collection’s contents',
      publisherUrl:
        'The cover image and the publisher name on the table of contents and chapter pages will be linked to the provided URL',
      tocContributors:
        'Displays contributor information (book authors and editors) under each book title on its collection page',
      tocFullBookCitations:
        'Displays book publisher name, location, and publication date under each book title on its collection page',
      UKPMC:
        'Indicates whether the book is to be included in UK PubMed Central',
    },
    metadataForm: {
      applyCoverToBooks:
        'When ON, the related field is not editable at the book level',
      applyPublisherToBooks:
        'When ON, the related field is not editable at the chapter-processed book level',
      applyToPDFBooks:
        'When ON, the related field is not editable at the PDF book level',
    },
  },
  form: {
    abbreviatedName: 'Abbreviated name',
    abstract: 'Abstract',
    abstractGraphic: 'Abstract graphic',
    acceptedSourceSubmissions: 'Accepted workflow submissions',
    add: 'Add',
    addAnotherNote: 'Add another note',
    addNote: 'Add note',
    addUser: 'Add User',
    alternativeTitle: 'Alternative title',
    applyCoverToBooks: 'Apply to all books in this collection',
    applyPublisherToBooks:
      'Apply to all chapter processed books in this collection',
    applyToPDFBooksCollection:
      'Apply to all PDF workflow books in this collection',
    assignNcbiSysAdmins:
      'Assign NCBI System Admins to the organisation. Notifications are sent to these users.',
    authorManuscript: 'Author manuscript',
    authorName: 'Author name',
    bookShelDdefault: 'BookShelf default',
    bookSourceType: 'Book source type',
    bookSubmitId: 'Book submit ID',
    cancel: 'Cancel',
    chapterProcessedSourceType:
      'Collection members: Chapter-processed source type',
    clear: 'Clear',
    close: 'Close',
    collection: 'Collection',
    collectionIssn: 'Print ISSN',
    collections: 'Collections',
    collectionType: 'Collection type',
    contentType: 'Content type',
    contributors: 'Contributors',
    copyrightStatement: 'Copyright statement',
    cover: 'Cover',
    createcollection: 'Create collection',
    currentArchived: 'Current--Archived',
    customGroups: 'Custom groups',
    customGroupTitles: 'Custom Group Titles',
    dateCreated: 'Date created',
    dateOfPublication: 'Date of publication',
    dateRevised: 'Date revised',
    dateUpdated: 'Date updated',
    deleteNote: 'Delete note',
    displayBookMetdataTOC: 'Display book metadata on TOC',
    doi: 'DOI',
    dropCover: 'Drop cover here',
    edition: 'Edition',
    editUser: 'Edit User',
    eissn: 'Electronic ISSN',
    email: 'e-Mail',
    givenNames: 'Given names',
    groupBooksBy: 'Group books by',
    groupBooksOnTOC: 'Group books on TOC',
    hideHelp: 'Hide Help',
    inviteAdmin: 'Invite Admin',
    inviteOrgAdmin: 'Invite Organization Admin',
    inviteUser: 'Invite User',
    isbn: 'ISBN',
    issn: 'ISSN',
    label: {
      bookSeriesCollection: 'Book Series collection',
      fundedCollection: 'Funded collection',
      funder: 'Funder',
      orgEmail: 'Email',
      orgName: 'Name',
      publisher: 'Publisher',
    },
    language: 'Language',
    licenseStatement: 'License statement',
    licenseType: 'License type',
    licenseUrl: 'License url',
    metadata: 'Metadata',
    NCBICustomMeta: 'NCBI Custom metadata',
    ncbiSysAdmins: 'NCBI System Admins',
    NCBISystemAdmins: 'NCBI System Admins',
    NCBISystemAdminsDescription:
      'Assign System Admins to the organization. Notifications are sent to these users',
    newCollection: 'New collection',
    next: 'Next',
    objectId: 'BCMS ID',
    openAccessLicense: 'Open access license',
    orderBooksBy: 'Order books by',
    orgSettings: 'Organization Settings',
    pdf2xmlVendor: 'PDF2XML vendor',
    pdfBook: 'PDF workflow',
    pendingUserRequest: 'Pending user request',
    permissions: 'Permissions',
    pmcDomainName: 'PMCID / Domain name',
    prepublicationDraft: 'Prepublication draft',
    publicationDate: 'Publication date',
    publicationDateFormat: 'Publication date format',
    publishedPDF: 'Published PDF',
    publisher: 'Publisher',
    publisherBookSeriesTitle: "Publisher's Book Series title",
    publisherDate: 'Publication date',
    publisherID: 'Publisher ID',
    publisherLocation: 'Publisher location',
    publisherName: 'Publisher name',
    requestAccess: 'Request access',
    save: 'Save',
    saving: 'Saving...',
    savingCollectionMeta: 'Saving collection metadata...',
    search: 'Search',
    selectTypeNote: 'Select type of note',
    selectUser: 'Select User',
    selectUsers: 'Select Users',
    sendInvitation: 'Send Invitation',
    showHelp: 'Show Help',
    sourceSubmissions: 'Source submissions',
    sourceType: 'Source type',
    subtitle: 'Subtitle',
    suffix: 'Suffix',
    surname: 'Surname',
    sysAdmin: 'System admin',
    tableOfContents: 'Table of Contents',
    title: 'Title',
    titleAndIds: 'Title and Ids',
    type: 'Type',
    typesOrganisation: 'Type of Organization',
    updatecollection: 'Set up collection',
    username: 'Username',
    validation: {
      reqEmail: 'Email is required',
      reqName: 'Name is required',
      reqOrgName: 'Organization name is required',
      reqTitle: 'Title is required',
    },
    volume: 'Volume / Issue number in series',
    wholeBookSourceType: 'Collection members: Whole book source type',
    wordBook: 'Word workflow',
    xmlBook: 'XML workflow',
    year: 'Year',
    yearCurrentArchived: 'Year and Current--Archived',
  },
  notifications: {
    assignRolesWarning: 'Only active users can be assigned roles',
    errorDeleteOrg: "Organization can't be deleted",
    errorDeleteOrgs: "Organizations couldn't be deleted",
    errorDeleteOrgTitle: "Can't delete organization",
    errorNewOrg: 'Organization was not created',
    errorUpdateMetadata: 'Metadata could not update',
    errorUpdateOrg: 'Organization was not updated',
    orgsRelated: 'Organization may be related to other data',
    successDeleteOrg: 'Organization was deleted',
    successDeleteOrgs: 'Organizations were deleted',
    successNewOrg: 'Organization was created succesfully',
    successRejectUser: "User $name's access request has been rejected",
    successUpdateOrg: 'Organization was updated succesfully',
    successVerifyUser: "User $name's access request has been verified",
    succesUpdateMetadata: 'Metadata updated successfully',
    succesUpdateUser: ' User $name modified succesfully',
    usersStatusChange:
      'Only pending requests to join the organization can be accepted or rejected',
  },
  pages: {
    admin: 'Admin',
    button: {
      acceptUser: ' Accept User',
      deleteOrg: 'Delete Organization',
      newOrg: 'New Organization',
    },
    noData: 'No data available',
    noOrg: 'No organizations available',
    noOrgAssigned: 'You are not assigned in any organizations yet',
    noUsers: 'No users available',
    organization: 'Organization',
    organizations: 'Organizations',
    publish: 'Publish',
    status: 'Status',
    users: 'Users',
    version: 'Version',
  },
  status: {
    approve: 'Approved',
    archived: 'Archived',
    bookshelf: 'Bookshelf',
    bookshelfSubmitter: 'Bookshelf/Submitter',
    conversionErrors: 'Conversion Errors',
    converting: 'Converting',
    error: 'Error',
    errors: 'Errors',
    failed: 'Failed',
    inProduction: 'In Production',
    inReview: 'In Review',
    loadingErrors: 'Loading Errors',
    newBook: 'New Book',
    newCollection: 'New Collection',
    newFtp: 'New FTP',
    newUpload: 'New Upload',
    newVersion: 'New Version',
    oldBookVersion: 'Not the latest version',
    openIssues: 'Vendor queries',
    preview: 'Previewing',
    previewErrors: 'Preview Errors',
    published: 'Published',
    publishing: 'Publishing',
    publishingFailed: 'Publishing Failed',
    query: 'Query',
    reviewRequested: 'Review Requested',
    revise: 'Revise',
    submissionErrors: 'Submission Errors',
    submitter: 'Submitter',
    tagging: 'Tagging',
    taggingErrors: 'Tagging Errors',
    technicalReview: 'Technical Review',
    unpublished: 'Unpublished',
    vendor: 'Vendor queries',
    warning: 'Warning',
  },
  upload: {
    readyToUpload: 'Ready for Upload',
    uploaded: 'File Uploaded',
    uploading: 'Uploading File ...',
  },
  utils: {
    loading: 'Loading...',
  },
}
