module.exports = {
  'pubsweet-client': {
    protocol: 'CLIENT_PROTOCOL',
    host: 'CLIENT_HOST',
    port: 'CLIENT_PORT',
  },
  'pubsweet-server': {
    host: 'SERVER_HOST',
    port: 'SERVER_PORT',
    secret: 'PUBSWEET_SECRET',
    serveClient: 'SERVER_SERVE_CLIENT',
    publicUrl: 'SERVER_PUBLIC_URL',
    db: {
      user: 'POSTGRES_USER',
      password: 'POSTGRES_PASSWORD',
      host: 'POSTGRES_HOST',
      database: 'POSTGRES_DB',
      port: 'POSTGRES_PORT',
    },
  },
  mailer: {
    from: 'MAILER_FROM',
    transport: {
      host: 'SMTP_HOST',
      port: 'SMTP_PORT',
      auth: {
        user: 'SMTP_USERNAME',
        pass: 'SMTP_PASSWORD',
      },
    },
  },
  useRandomStringForDomainNames: 'USE_RANDOM_STRING_FOR_DOMAIN_NAMES',
}
