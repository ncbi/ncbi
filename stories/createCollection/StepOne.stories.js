import React from 'react'
// import { lorem } from 'faker'

import StepOne from '../../ui/Pages/CreateCollection/StepOne'

export const Base = () => (
  <StepOne bookSeriesCollectionsEnabled fundedCollectionsEnabled />
)

export default {
  component: StepOne,
  title: 'Create Collection/Step One',
}
