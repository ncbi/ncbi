import React, { useState } from 'react'
// import { lorem } from 'faker'

import CreateCollection from '../../ui/Pages/CreateCollection/CreateCollection'

export const Base = args => {
  const [step, setStep] = useState(0)
  const [isSaving, setIsSaving] = useState(false)

  const handleStepOneSubmit = vals => {
    setIsSaving(true)

    setTimeout(() => {
      setIsSaving(false)
      setStep(1)
    }, 2000)
  }

  return (
    <CreateCollection
      currentStep={step}
      isSaving={isSaving}
      onStepOneSubmit={handleStepOneSubmit}
      stepTwoInitialValues={{}}
      {...args}
    />
  )
}

Base.args = {
  bookSeriesCollectionsEnabled: true,
  // currentStep: 0,
  fundedCollectionsEnabled: true,
}

export default {
  component: CreateCollection,
  title: 'Create Collection/Create Collection',
}
