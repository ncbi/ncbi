import React from 'react'
import styled from 'styled-components'
import Dropdown from '../ui/components/Dropdown'

const LargeDiv = styled.div`
  display: flex;
  height: 300px;
  margin-top: auto;
`

const itemsList = [
  {
    value: 1,
    onClick: () => {},
    title: 'item 1n  asfasf asfkaskfh  ',
  },
  { value: 2, title: 'item 2' },
]

export const DropDownButtonPrimary = () => (
  <Dropdown itemsList={itemsList} primary>
    Test
  </Dropdown>
)

export const DropDownButtonSecondary = () => (
  <Dropdown itemsList={itemsList}>Test Test dsfsdgs dsgdsgdsg</Dropdown>
)

export const DropDownButtonUp = () => (
  <LargeDiv>
    <Dropdown direction="up" itemsList={itemsList} primary>
      Test Test dsfsdgs dsgdsgdsg
    </Dropdown>
  </LargeDiv>
)

export const DropDownButtonPrimaryIcon = () => (
  <Dropdown icon="user" itemsList={itemsList} primary>
    Test
  </Dropdown>
)

export const DropDownButtonIconEnd = () => (
  <Dropdown icon="user" iconPosition="end" itemsList={itemsList} secondary>
    Test dsfsdgs dsgdsgdsg Test dsfsdgs dsgdsgdsg
  </Dropdown>
)

export default { component: Dropdown, title: 'Common/Dropdown Button' }
