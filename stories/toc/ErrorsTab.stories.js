import React from 'react'
import styled from 'styled-components'

import ErrorsTab from '../../ui/Pages/Toc/Tabs/ErrorsTab'

const Wrapper = styled.div`
  height: 500px;
`

const dummyData = [
  {
    id: '1291ab13-8846-48e5-a830-f641663f09b4',
    jobId: '0eeea4',
    sessionId: '11334664',
    url:
      'http://ipmc-prod.be-md.ncbi.nlm.nih.gov:5701/internal/utils/tm/index.fcgi?sessid=11334662',
    status: '3',
    isCurrent: true,
    updated: '2021-11-01T20:26:16.499Z',
    errors: [
      {
        id: '0d123013-ab74-4e77-8629-c33e053bebd8',
        noticeTypeName: 'word_conversion',
        severity: 'error',
        assignee: 'Publisher',
        message: 'Cannot determine parent document: rid="ch03.F3" type="fig" ',
        objectId: '060bcb3f-f41c-495c-8f2f-20c56ac29986',
        __typename: 'Error',
      },
      {
        id: 'f3062fcd-ea45-45e0-93c1-4b1688237b0e',
        noticeTypeName: 'UNKNOWN',
        severity: 'warning',
        assignee: 'Unknown',
        message: 'Conversion errors were encountered, aborting ...<br>',
        objectId: '060bcb3f-f41c-495c-8f2f-20c56ac29986',
        __typename: 'Error',
      },
    ],
    __typename: 'JobReportError',
  },
  {
    id: '1291ab13-8846-48e5-a830-f641663f09b1',
    jobId: '0eeea3',
    sessionId: '11334662',
    url:
      'http://ipmc-prod.be-md.ncbi.nlm.nih.gov:5701/internal/utils/tm/index.fcgi?sessid=11334662',
    status: '3',
    isCurrent: false,
    updated: '2021-11-01T20:26:15.499Z',
    errors: [
      {
        id: '0d123013-ab74-4e77-8629-c33e053bebd8',
        noticeTypeName: 'word_conversion',
        severity: 'error',
        assignee: 'Publisher',
        message:
          'Another Cannot determine parent document: rid="ch03.F3" type="fig" ',
        objectId: '060bcb3f-f41c-495c-8f2f-20c56ac29986',
        __typename: 'Error',
      },
      {
        id: 'e7b5c8c6-d94c-4de0-a5c7-403e0efe871b',
        noticeTypeName: 'word_conversion',
        severity: 'error',
        assignee: 'Publisher',
        message: 'Cannot determine parent document: rid="ch06.F1" type="fig"',
        objectId: '060bcb3f-f41c-495c-8f2f-20c56ac29986',
        __typename: 'Error',
      },
      {
        id: 'f3062fcd-ea45-45e0-93c1-4b1688237b0e',
        noticeTypeName: 'UNKNOWN',
        severity: 'warning',
        assignee: 'Unknown',
        message: 'Conversion errors were encountered, aborting ...<br>',
        objectId: '060bcb3f-f41c-495c-8f2f-20c56ac29986',
        __typename: 'Error',
      },
    ],
    __typename: 'JobReportError',
  },
]

export const Base = args => {
  return (
    <Wrapper>
      <ErrorsTab {...args} errorData={dummyData} />
    </Wrapper>
  )
}

Base.args = {
  isUpdating: false,
}

export const NoErrors = () => {
  return (
    <Wrapper>
      <ErrorsTab errorData={[]} />
    </Wrapper>
  )
}

export default {
  component: ErrorsTab,
  title: 'Book TOC/Errors Tab',
}
