import React, { useState } from 'react'
import styled from 'styled-components'

import TOCPage from '../../ui/Pages/Toc/TOCPage'

const Wrapper = styled.div`
  height: 500px;
`

const getMessage = status => {
  if (status === 'unpublished')
    return 'To create a table of contents page for the book, chapters must be published. This page is updated with the published table of contents page on Bookshelf only once overnight when new chapters are published.'

  if (status === 'publish-failed') {
    return 'Not available.  Resolve errors to generate the live view.'
  }

  return null
}

export const Base = args => {
  const [isSaving, setIsSaving] = useState(false)
  const { status, canViewTabs, disableDownloadToc, previewLink } = args

  const handleUpdate = () => {
    setIsSaving(true)

    setTimeout(() => {
      setIsSaving(false)
    }, 3000)
  }

  const handleDownload = () => {
    setTimeout(() => {
      // eslint-disable-next-line no-console
      console.log('Dowloaded')
    }, 3000)
  }

  return (
    <Wrapper>
      <TOCPage
        canPublishToc
        canViewTabs={canViewTabs}
        disableDownloadToc={disableDownloadToc}
        disablePublishToc={status.includes('unpublished')}
        errorData={[]}
        handleDownloadToc={handleDownload}
        handleRepublishToc={handleUpdate}
        isUpdateingToc={isSaving}
        noPreviewMessage={getMessage(status)}
        previewLink={previewLink}
        status={status}
        {...args}
      />
    </Wrapper>
  )
}

Base.args = {
  status: 'unpublished',
  canViewTabs: true,
  disableDownloadToc: true,
  previewLink: null,
}

export default {
  component: TOCPage,
  title: 'Book TOC/Main Page',
}
