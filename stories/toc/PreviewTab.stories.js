import React from 'react'
import styled from 'styled-components'

import PreviewTab from '../../ui/Pages/Toc/Tabs/PreviewTab'

const Wrapper = styled.div`
  height: 500px;
`

const PUBLISHING = 'publishing'
const UNPUBLISHED = 'unpublished'
const PUBLISHFAILED = 'publish-failed'
const PUBLISHED = 'published'

const getMessage = status => {
  if (status === PUBLISHING)
    return 'Preview will display when TOC loads successfully.'
  if (status === UNPUBLISHED)
    return 'To create a table of contents page for the book, chapters must be published. This page is updated with the published table of contents page on Bookshelf only once overnight when new chapters are published.'

  if (status === PUBLISHFAILED) {
    return 'Not available.  Resolve errors to generate the live view.'
  }

  return null
}

const handleUpdate = () => {}

const handleDownload = () => {}

export const UnpublishedPreview = args => {
  const status = UNPUBLISHED
  return (
    <Wrapper>
      <PreviewTab
        canPublishToc
        disableDownloadToc
        disablePublishToc={status.includes('unpublished')}
        handleDownloadToc={handleDownload}
        handleRepublishToc={handleUpdate}
        isUpdatingToc={false}
        noPreviewMessage={getMessage(status)}
        previewLink={null}
        {...args}
      />
    </Wrapper>
  )
}

export const PublishingPreview = args => {
  const status = PUBLISHING
  return (
    <Wrapper>
      <PreviewTab
        canPublishToc
        disableDownloadToc
        disablePublishToc
        handleDownloadToc={handleDownload}
        handleRepublishToc={handleUpdate}
        isUpdatingToc={false}
        noPreviewMessage={getMessage(status)}
        previewLink={null}
        {...args}
      />
    </Wrapper>
  )
}

export const Published = args => {
  const { disableDownloadToc, previewLink } = args
  const status = PUBLISHED
  return (
    <Wrapper>
      <PreviewTab
        canPublishToc
        disableDownloadToc={disableDownloadToc}
        disablePublishToc={status.includes('unpublished')}
        handleDownloadToc={handleDownload}
        handleRepublishToc={handleUpdate}
        isUpdatingToc={false}
        noPreviewMessage={getMessage(status)}
        previewLink={previewLink}
        {...args}
      />
    </Wrapper>
  )
}

Published.args = {
  canViewTabs: true,
  disableDownloadToc: true,
  previewLink:
    'https://preview.ncbi.nlm.nih.gov/bookshelf/coko/br.fcgi?accid=NBK125781.1',
}

export default {
  component: PreviewTab,
  title: 'Book TOC/Live Tab',
}
