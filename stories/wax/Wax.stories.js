import React from 'react'
import Editor from '../../ui/components/wax/EditorCustom'

export const Inline = () => <Editor editorType="inline" value="Title" />

export const Abstract = () => <Editor editorType="abstract" value="Abstract" />

export const Statement = () => (
  <Editor editorType="statement" value="Copyright Statement" />
)
export const LicenseStatement = () => (
  <Editor editorType="licenseStatement" value="License Statement" />
)

export default { component: Editor, title: 'Wax/Editor' }
