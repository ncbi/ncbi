import React from 'react'
// import { lorem } from 'faker'

import ContributorsComponent from '../../ui/components/Contributors/ContributorsComponent'
import DragAndDrop from '../../ui/components/DragAndDrop'
import Form from '../../ui/components/FormElements/Form'

export const Base = args => {
  return (
    <DragAndDrop>
      <Form initialValues={{ test: [] }}>
        {formProps => {
          const handleChange = val => {
            formProps.setFieldValue('test', val)
          }

          return (
            <ContributorsComponent
              {...args}
              name="test"
              onChange={handleChange}
              value={formProps.values.test}
            />
          )
        }}
      </Form>
    </DragAndDrop>
  )
}

Base.args = {
  variant: 'seriesEditors',
}

export default {
  component: ContributorsComponent,
  title: 'Metadata/ContributorsComponent',
}
