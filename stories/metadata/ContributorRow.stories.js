/* eslint-disable no-console */

import React, { useRef } from 'react'
import { capitalize, range } from 'lodash'
import { lorem } from 'faker'

import ContributorRow from '../../ui/components/Contributors/ContributorRow'
import Form from '../../ui/components/FormElements/Form'

const makeFields = n =>
  range(n).map(() => {
    const text = lorem.word()
    const upperCase = capitalize(text)

    return {
      label: upperCase,
      placeholder: upperCase,
      value: '',
      name: text,
      key: text,
    }
  })

export const Base = () => {
  const fields = makeFields(3)
  const initialValues = {}

  fields.forEach(field => {
    initialValues[field.key] = ''
  })

  return (
    <Form initialValues={initialValues}>
      {formProps => {
        return (
          <ContributorRow
            dragRef={useRef(null)}
            fields={fields}
            index={0}
            onDelete={() => console.log('delete')}
          />
        )
      }}
    </Form>
  )
}

export const Single = () => {
  const fields = makeFields(1)
  const initialValues = {}

  fields.forEach(field => {
    initialValues[field.key] = ''
  })

  return (
    <Form initialValues={initialValues}>
      {formProps => {
        return (
          <ContributorRow
            dragRef={useRef(null)}
            fields={fields}
            index={3}
            onDelete={() => console.log('delete')}
          />
        )
      }}
    </Form>
  )
}

export const Six = () => {
  const fields = makeFields(6)
  const initialValues = {}

  fields.forEach(field => {
    initialValues[field.key] = ''
  })

  return (
    <Form initialValues={initialValues}>
      {formProps => {
        return (
          <ContributorRow
            dragRef={useRef(null)}
            fields={fields}
            index={6}
            onDelete={() => console.log('delete')}
          />
        )
      }}
    </Form>
  )
}

export const Disabled = () => {
  const fields = makeFields(6)
  const initialValues = {}

  fields.forEach(field => {
    initialValues[field.key] = ''
  })

  return (
    <Form initialValues={initialValues}>
      {formProps => {
        return (
          <ContributorRow
            disabled
            dragRef={useRef(null)}
            fields={fields}
            index={9}
            onDelete={() => console.log('delete')}
          />
        )
      }}
    </Form>
  )
}

export default {
  component: ContributorRow,
  title: 'Metadata/Contributor Row',
}
