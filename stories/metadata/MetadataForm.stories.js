/* eslint-disable no-console */

import React, { useRef } from 'react'
// import { lorem } from 'faker'

import MetadataForm from '../../ui/components/MetadataForm'
import DragAndDrop from '../../ui/components/DragAndDrop'

const fakeOptions = [
  {
    value: 'this',
    label: 'This',
  },
  {
    value: 'other',
    label: 'Other',
  },
]

const submitFn = values => console.log(values)

const initialFormValues = {
  sourceType: 'Collection',
}

export const Base = args => (
  <DragAndDrop>
    <MetadataForm
      {...args}
      initialFormValues={initialFormValues}
      onSubmit={submitFn}
      searchGranthub={() => {}}
    />
  </DragAndDrop>
)

Base.args = {
  isFundedCollection: false,
  isSaving: false,
  submissionType: 'chapterProcessed',
  variant: 'collection',
  // variant: 'book',
  showSubmitButton: true,
}

export const TriggerFromOutsideForm = () => {
  const formRef = useRef(null)

  const triggerSubmit = () => {
    formRef.current.onSubmit()
  }

  return (
    <>
      <MetadataForm
        coverAcceptedFileTypes="image/*"
        initialFormValues={initialFormValues}
        licenseTypeOptions={fakeOptions}
        onSubmit={submitFn}
        searchGranthub={() => {}}
      />

      <button onClick={triggerSubmit} type="submit">
        Trigger submit
      </button>
    </>
  )
}

export default {
  component: MetadataForm,
  title: 'Metadata/Metadata Form',
}
