import React from 'react'

import { Search } from '../../ui/components/common'

export const Base = args => <Search {...args} />

export default {
  component: Search,
  title: 'Common/Search',
}
