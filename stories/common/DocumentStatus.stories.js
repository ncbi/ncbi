import React from 'react'
import Status from '../../ui/components/Status'

export const NewVersion = () => <Status status="new-version" />
export const Error = () => <Status status="error" />
export const Errors = () => <Status status="errors" />
export const Query = () => <Status status="query" />
export const Warning = () => <Status status="warning" />
export const Submitter = () => <Status status="submitter" />
export const Bookshelf = () => <Status status="bookshelf" />
export const BookShelfSubmitter = () => <Status status="bookshelfSubmitter" />
export const TechnicalReview = () => <Status status="technicalReview" />
export const InProduction = () => <Status status="in-production" />
export const NewFtp = () => <Status status="newFtp" />
export const Preview = () => <Status status="preview" />
export const InReview = () => <Status status="in-review" />
export const InReviewApprovalAndReview = () => (
  <Status approved={12} revise={21} status="in-review" />
)
export const Approve = () => <Status status="approve" />
export const Published = () => <Status status="published" />
export const Publishing = () => <Status status="publishing" />
export const Archived = () => <Status status="archived" />
export const ReviewRequested = () => <Status status="reviewRequested" />
export const RequestChanges = () => <Status status="requestChanges" />
export const Converting = () => <Status status="converting" />
export const LoadingPreview = () => <Status status="loading-preview" />
export const NewUpload = () => <Status status="new-upload" />
export const NewBook = () => <Status status="new-book" />
export const Progress = () => <Status status="progress" />
export const SubmissionErrors = () => <Status status="submission-errors" />
export const Tagging = () => <Status status="tagging" />
export const Failed = () => <Status status="failed" />
export const ConversionErrors = () => <Status status="conversion-errors" />
export const PreviewErrors = () => <Status status="preview-errors" />
export const PublishFailed = () => <Status status="publish-failed" />
export const LoadingErrors = () => <Status status="loading-errors" />
export const NewCollection = () => <Status status="new-collection" />
export const Unpublished = () => <Status status="unpublished" />
export const TaggingErrors = () => <Status status="tagging-errors" />

export default {
  component: Status,
  title: 'common/Statuses',
}
