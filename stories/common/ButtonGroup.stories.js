import React from 'react'

import Button from '../../ui/components/common/Button'
import ButtonGroup from '../../ui/components/common/ButtonGroup'

export const Base = args => (
  <ButtonGroup {...args}>
    <Button status="primary">Publish</Button>
    <Button icon="plus-circle" outlined>
      Add
    </Button>
    <Button icon="trash-2" status="danger" />
  </ButtonGroup>
)

export default {
  component: ButtonGroup,
  title: 'Common/Button Group',
}
