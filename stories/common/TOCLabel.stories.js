import React from 'react'

import TOCLabel from '../../ui/components/common/TOCLabel'

// expected one of those props: excluded, duplicate or missing

export const ExcludedChapter = () => <TOCLabel excluded />

export const DublicateChapter = () => <TOCLabel duplicate />

export const MissingTOC = () => <TOCLabel missing />

export const MissingAndExcludedTOC = () => <TOCLabel excluded missing />

export default {
  component: TOCLabel,
  title: 'Common/Toc Label ',
}
