import React from 'react'
import styled from 'styled-components'
import { lorem } from 'faker'

import FormSection from '../../ui/components/common/FormSection'

const Child = styled.div`
  background-color: ${props => props.theme.colorPrimary};
  border-radius: 3px;
  height: 100px;
`

export const Base = args => (
  <FormSection label={lorem.words(3)} {...args}>
    <Child />
    <Child />
  </FormSection>
)

export default {
  component: FormSection,
  title: 'Common/Form Section',
}
