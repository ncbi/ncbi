/* eslint-disable no-console */

import React from 'react'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'
// import { lorem } from 'faker'

import Granthub from '../../ui/components/Granthub/Granthub'
import Form from '../../ui/components/FormElements/Form'

const Values = styled.div`
  margin-bottom: 24px;

  > div:not(:first-child) {
    background-color: gainsboro;
    border-radius: 3px;
    display: inline-flex;
    padding: 2px 4px;

    &:not(:last-child) {
      margin-right: 8px;
    }
  }
`

const options = [
  'CLSLWS2PUHY3K2X8',
  '3QRHW34MR6PV9JA5',
  'CNYDU5J3P23J6LV6',
  'Y7W2F3ZDCK8Y5WJY',
]

const handleSearch = val => {
  return new Promise(resolve => {
    setTimeout(() => {
      const result = options.find(option => option === val)
      resolve(result)
    }, 1000)
  })
}

export const Base = () => (
  <div>
    <Values>
      <div>Valid values:</div>
      {options.map(option => (
        <div key={uuid()}>{option}</div>
      ))}
    </Values>

    <Form
      initialValues={{ granthub: [] }}
      onSubmit={values => console.log(values)}
    >
      {formProps => {
        return (
          <>
            <Granthub
              awards={formProps.values.granthub}
              name="granthub"
              onSearch={handleSearch}
            />

            <button type="submit">Submit</button>
          </>
        )
      }}
    </Form>
  </div>
)

export default {
  component: Granthub,
  title: 'Common/Granthub',
}
