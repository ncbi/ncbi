import React, { useState } from 'react'
import Checkbox from '../../ui/components/common/Checkbox'

export const Base = args => {
  const [isChecked, setIsChecked] = useState(false)

  return (
    <Checkbox
      checked={isChecked}
      onClick={() => {
        setIsChecked(!isChecked)
      }}
      {...args}
    />
  )
}

Base.args = {
  disabled: false,
  label: 'something',
  name: 'checkbox1',
}

export default {
  component: Checkbox,
  title: 'Common/Checkbox',
}
