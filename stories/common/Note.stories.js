import React from 'react'
import { lorem } from 'faker'

import Note from '../../ui/components/common/Note'

export const Base = () => <Note content={lorem.words(10)} />
export const Compact = () => <Note compact content={lorem.words(10)} />
export const LongText = () => <Note content={lorem.sentences(10)} />

export default {
  component: Note,
  title: 'Common/Note',
}
