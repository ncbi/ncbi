import React from 'react'
import styled from 'styled-components'
import { lorem } from 'faker'

import Select from '../../ui/components/common/Select'

const Wrapper = styled.div`
  height: 200px;
`

// #region options
const options = [
  {
    value: 'choiceA',
    label: 'Choice A',
  },
  {
    value: 'choiceB',
    label: 'Choice B',
  },
]

const optionsWithTags = [
  {
    value: 'choiceA',
    label: 'Choice A',
  },
  {
    value: 'choiceB',
    label: 'Choice B',
  },
  {
    value: 'choiceC',
    label: 'Choice C',
    tag: lorem.words(3),
  },
  {
    value: 'choiceD',
    label: 'Choice D',
    tag: lorem.words(3),
  },
]

const optionsWithHTMLLabels = [
  {
    value: 'choiceA',
    label: `<p>${lorem.words(3)}</p>`,
  },
  {
    value: 'choiceB',
    label: `<p>${lorem.word()} <strong>${lorem.word()}</strong></p>`,
  },
  {
    value: 'choiceC',
    label: `<p>${lorem.word()} <em>${lorem.word()}</em></p>`,
    tag: lorem.words(3),
  },
  {
    value: 'choiceD',
    label: `<p>${lorem.word()} <sup>${lorem.word()}</sup></p>`,
    tag: lorem.words(3),
  },
]

const asyncOptions = [
  {
    value: 'green',
    label: 'Green',
  },
  {
    value: 'blue',
    label: 'Blue',
  },
  {
    value: 'red',
    label: 'Red',
  },
  {
    value: 'yellow',
    label: 'Yellow',
  },
]
// #endregion options

export const Base = args => (
  <Wrapper>
    <Select
      defaultValue={options[0]}
      description={lorem.sentences(3)}
      label={lorem.words(3)}
      options={options}
      required
      {...args}
    />
  </Wrapper>
)

export const ErrorState = () => (
  <Wrapper>
    <Select
      defaultValue={options[0]}
      description={lorem.sentences(3)}
      hasError
      label={lorem.words(3)}
      options={options}
      required
    />
  </Wrapper>
)

export const OptionsWithTags = () => (
  <Wrapper>
    <Select
      description={lorem.sentences(3)}
      label={lorem.words(3)}
      options={optionsWithTags}
      required
    />
  </Wrapper>
)

export const OptionsWithHTMLLabels = () => (
  <Wrapper>
    <Select
      description={lorem.sentences(3)}
      hasHtmlLabels
      label={lorem.words(3)}
      options={optionsWithHTMLLabels}
      required
    />
  </Wrapper>
)

export const Async = args => {
  const loadOptions = val => {
    const regex = new RegExp(val, 'i')

    return new Promise(resolve => {
      setTimeout(() => {
        const results = asyncOptions.filter(option => option.label.match(regex))
        resolve(results)
      }, 1000)
    })
  }

  return (
    <Wrapper>
      <Select
        async
        label={lorem.words(3)}
        loadOptions={loadOptions}
        {...args}
      />
    </Wrapper>
  )
}

export default {
  component: Select,
  title: 'Common/Select',
}
