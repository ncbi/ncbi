import React from 'react'
import { lorem } from 'faker'

import Label from '../../ui/components/common/Label'

export const Base = args => <Label {...args} />

Base.args = {
  value: lorem.words(3),
  required: true,
  description: lorem.sentences(2),
}

export const Inline = args => (
  <Label
    description={lorem.sentences(3)}
    inline
    required
    value={lorem.words(3)}
  />
)

export default {
  component: Label,
  title: 'Common/Label',
}
