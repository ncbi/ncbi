import React from 'react'

import NavBar from '../../ui/components/common/NavBar'
import Button from '../../ui/components/common/Button'
import ButtonGroup from '../../ui/components/common/ButtonGroup'
import SvgIcon from '../../ui/components/common/SvgIcon'

export const Base = () => (
  <NavBar
    rightComponent={
      <ButtonGroup>
        <Button outlined status="primary">
          Preview TOC{' '}
        </Button>
        <Button icon="filter" status="primary" />
      </ButtonGroup>
    }
  >
    Collections and books
  </NavBar>
)

export const BookManagerNavBar = () => (
  <NavBar
    rightComponent={
      <ButtonGroup>
        <Button outlined status="primary">
          Preview TOC{' '}
        </Button>
        <Button icon="filter" status="primary" />
      </ButtonGroup>
    }
  >
    <SvgIcon name="collection" />
    Book Title
  </NavBar>
)

export default { component: NavBar, title: 'common/Nav Bar' }
