import React from 'react'
import styled from 'styled-components'
import { lorem } from 'faker'

import Tooltip from '../../ui/components/common/Tooltip'

const Box = styled.div`
  background-color: mediumseagreen;
  border-radius: 3px;
  height: 50px;
  width: 50px;
`

export const Base = () => (
  <>
    <Box data-for="1" data-tip />
    <Tooltip id="1" place="right">
      {lorem.words(4)}
    </Tooltip>
  </>
)

export const WithLongText = () => (
  <>
    <Box data-for="1" data-tip />
    <Tooltip id="1" place="right">
      {lorem.sentences(5)}
    </Tooltip>
  </>
)

export default {
  component: Tooltip,
  title: 'Common/Tooltip',
}
