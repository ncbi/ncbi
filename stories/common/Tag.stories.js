import React from 'react'
import Tag from '../../ui/components/common/Tag'

export const Base = () => {
  return <Tag label="tag" />
}

export default {
  component: Tag,
  title: 'Common/Tag',
}
