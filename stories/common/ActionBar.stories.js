import React from 'react'

import ActionBar from '../../ui/components/common/ActionBar'
import Button from '../../ui/components/common/Button'
import ButtonGroup from '../../ui/components/common/ButtonGroup'

export const Base = () => (
  <ActionBar
    leftComponent={<Button icon="book" key="book" status="primary" />}
  />
)

export const SingleButton = () => (
  <ActionBar
    leftComponent={<Button icon="book" key="book" status="primary" />}
  />
)

export const MultiButton = () => (
  <ActionBar
    leftComponent={
      <ButtonGroup>
        <Button icon="book" key="book" status="primary" />
        <Button key="add" status="primary">
          Add{' '}
        </Button>
        <Button key="edit" status="primary">
          Edit
        </Button>
      </ButtonGroup>
    }
  />
)

export const RightSide = () => (
  <ActionBar
    rightComponent={
      <ButtonGroup>
        <Button key="edit" status="primary">
          Edit
        </Button>
        <Button key="delete" status="primary">
          Delete
        </Button>
      </ButtonGroup>
    }
  />
)

export const Center = () => (
  <ActionBar
    middleComponent={<Button icon="book" key="book" status="primary" />}
  />
)

export const MultipleSides = () => (
  <ActionBar
    leftComponent={
      <ButtonGroup>
        <Button icon="book" key="book" status="primary" />
        <Button key="add" primary>
          Add{' '}
        </Button>
      </ButtonGroup>
    }
    middleComponent={<Button icon="book" key="book" status="primary" />}
    rightComponent={<Button icon="book" key="book" status="primary" />}
  />
)

export default { component: ActionBar, title: 'Common/ActionBar' }
