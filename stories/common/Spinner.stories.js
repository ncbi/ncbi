import React from 'react'
import Spinner from '../../ui/components/common/Spinner'

export const Base = args => <Spinner {...args} />

Base.args = {
  label: 'This is a small spinner',
}

export const PageBlockingSpinner = () => <Spinner pageLoader />

export default {
  component: Spinner,
  title: 'Common/Spinner',
}
