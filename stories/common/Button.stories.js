import React from 'react'
import Button from '../../ui/components/common/Button'
import { PublishedIcon } from '../../ui/components/Icons'

export const Base = args => <Button {...args}>Publish</Button>

Base.args = {
  disabled: false,
  outlined: false,
  status: 'primary',
  icon: 'plus-circle',
  iconPosition: 'start',
  loading: false,
}

export const Primary = () => <Button status="primary">Publish</Button>

export const Secondary = () => <Button>Ready to publish</Button>

export const SecondaryDisabled = () => (
  <Button disabled>Ready to publish</Button>
)

export const PrimaryWithIcon = () => (
  <Button icon="plus-circle" status="primary">
    Add
  </Button>
)

export const Danger = () => (
  <Button icon="x" status="danger">
    Reject user
  </Button>
)

export const DangerDisabled = () => (
  <Button disabled icon="x" status="danger">
    Reject user
  </Button>
)

export const OutlinedPrimary = () => <Button outlined>Collapse all</Button>

export const IconSeparator = () => (
  <Button icon="plus-circle" iconPosition="end" outlined separator>
    Separator
  </Button>
)

export const CustomIconSeparator = () => (
  <Button icon={<PublishedIcon />} iconPosition="end" outlined separator>
    TOC
  </Button>
)

export const CustomIconStartSeparator = () => (
  <Button icon={<PublishedIcon />} iconPosition="start" outlined separator>
    TOC
  </Button>
)

export const OutlinedPrimaryDisabled = () => (
  <Button disabled outlined>
    Collapse all
  </Button>
)

export const OutlinedWithIcon = () => (
  <Button icon="download" outlined>
    Download
  </Button>
)

export const IconOnlyButton = () => <Button icon="download" outlined />

export const FilterButton = () => <Button icon="filter" outlined />

export const IconOnlyButtonDanger = () => (
  <Button icon="trash-2" outlined status="danger" />
)

export const LoadingPrimary = () => (
  <Button loading status="primary">
    Saving
  </Button>
)

export const FullWidth = () => (
  <Button status="primary" style={{ width: '100%' }}>
    Yes
  </Button>
)

export default {
  component: Button,
  title: 'Common/Button',
  argTypes: {
    onClick: {
      action: 'clicked',
    },
  },
}
