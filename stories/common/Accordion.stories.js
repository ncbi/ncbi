import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import Accordion from '../../ui/components/common/Accordion'
import Button from '../../ui/components/common/Button'
import ButtonGroup from '../../ui/components/common/ButtonGroup'

const Content = styled.div`
  align-items: center;
  background: ${th('colorBackgroundHue')};
  display: flex;
  height: 144px;
  justify-content: center;
`

export const Base = args => (
  <Accordion {...args}>
    <Content>Accordion content here ...</Content>
  </Accordion>
)

Base.args = {
  label: 'Suplementary default',
  icon: 'chevron-down',
  startExpanded: true,
}

const RightComponent = (
  <ButtonGroup>
    <Button
      icon="plus"
      key="1"
      onClick={e => {
        e.stopPropagation()
      }}
      status="primary"
    >
      Upload Files
    </Button>

    <Button
      icon="plus"
      key="2"
      onClick={e => {
        e.stopPropagation()
      }}
      outlined
    >
      Another button
    </Button>

    <Button
      icon="trash-2"
      key="3"
      onClick={e => {
        e.stopPropagation()
      }}
      status="danger"
    />
  </ButtonGroup>
)

export const AccordionWithButtons = () => {
  return (
    <Accordion
      headerRightComponent={RightComponent}
      label="Supplementary with component"
      startExpanded
    >
      <Content>Accordion content here ...</Content>
    </Accordion>
  )
}

export default {
  component: Accordion,
  title: 'Common/Accordion',
}
