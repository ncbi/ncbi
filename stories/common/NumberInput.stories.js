import React, { useState } from 'react'
import { lorem } from 'faker'

import NumberInput from '../../ui/components/common/NumberInput'

const label = lorem.words(2)
const description = lorem.sentences(3)

export const Base = args => {
  const [currentValue, setCurrentValue] = useState('1')

  return (
    <NumberInput
      onChange={v => {
        setCurrentValue(v)
      }}
      value={currentValue}
      {...args}
    />
  )
}

Base.args = {
  description,
  label,
  max: 10,
  min: 1,
}

export default {
  component: NumberInput,
  title: 'Common/NumberInput',
}
