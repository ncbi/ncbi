import React from 'react'
import { lorem } from 'faker'

import FieldDescription from '../../ui/components/common/FieldDescription'

export const Base = () => <FieldDescription content={lorem.sentences(3)} />

export default {
  component: FieldDescription,
  title: 'Common/Field Description',
}
