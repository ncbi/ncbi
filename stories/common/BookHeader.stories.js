import React from 'react'
import styled from 'styled-components'
import { lorem } from 'faker'

import BookHeader from '../../ui/components/common/BookHeader'

const Box = styled.div`
  height: 100%;
  line-height: 32px;
  min-width: 50px;
  text-align: center;
  vertical-align: center;
`

const LeftBox = styled(Box)`
  background-color: salmon;
`

const RightBox = styled(Box)`
  background-color: cornflowerblue;
`

const title = lorem.words(7)

export const Base = args => (
  <BookHeader
    childrenLeft={<LeftBox>Left</LeftBox>}
    childrenRight={<RightBox>Right</RightBox>}
    {...args}
  />
)

Base.args = {
  title,
  type: 'book',
  workflow: 'pdf',
}

export const WithoutWorkflow = args => (
  <BookHeader
    childrenLeft={<LeftBox>Left</LeftBox>}
    childrenRight={<RightBox>Right</RightBox>}
    {...args}
  />
)

WithoutWorkflow.args = {
  title,
  type: 'collection',
}

export default {
  component: BookHeader,
  title: 'Common/BookHeader',
}
