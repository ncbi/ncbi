import React, { useState } from 'react'
import DateRangePicker from '../../ui/components/common/DateRangePicker'

import 'react-datepicker/dist/react-datepicker.css'

export const Base = () => {
  const [startDate, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(null)

  const handleDateChange = date => {
    if (!startDate && !endDate) {
      setStartDate(date)
    } else if (startDate && !endDate) {
      setEndDate(date)
    }

    if (startDate && endDate) {
      setStartDate(date)
      setEndDate(null)
    }
  }

  return (
    <DateRangePicker
      dateFormat="yyyy-MM-dd"
      inline
      label="Date picker"
      onDateChange={date => handleDateChange(date)}
      selectedEndDate={endDate}
      selectedStartDate={startDate}
      selectsEnd={Boolean(startDate)}
      selectsRange
    />
  )
}

export default {
  component: DateRangePicker,
  title: 'Common/DateRangePicker',
}
