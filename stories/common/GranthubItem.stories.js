import React, { useState } from 'react'
import styled from 'styled-components'
import { v4 as uuid } from 'uuid'
// import { lorem } from 'faker'

import GranthubItem from '../../ui/components/Granthub/GranthubItem'

const Wrapper = styled.div``

const Values = styled.div`
  margin-bottom: 24px;

  > div:not(:first-child) {
    background-color: gainsboro;
    border-radius: 3px;
    display: inline-flex;
    padding: 2px 4px;

    &:not(:last-child) {
      margin-right: 8px;
    }
  }
`

const options = [
  'CLSLWS2PUHY3K2X8',
  '3QRHW34MR6PV9JA5',
  'CNYDU5J3P23J6LV6',
  'Y7W2F3ZDCK8Y5WJY',
]

export const Base = () => {
  const [value, setValue] = useState('')

  const handleSearch = val => {
    return new Promise(resolve => {
      setTimeout(() => {
        const result = options.find(option => option === val)
        resolve(result)
      }, 1000)
    })
  }

  const handleValueChange = val => {
    setValue(val)
  }

  return (
    <Wrapper>
      <Values>
        <div>Valid values:</div>
        {options.map(option => (
          <div key={uuid()}>{option}</div>
        ))}
      </Values>

      <GranthubItem
        awardNumber={value}
        onAwardNumberChange={handleValueChange}
        onDelete={() => {}}
        onSearch={handleSearch}
      />
    </Wrapper>
  )
}

export default {
  component: GranthubItem,
  title: 'Common/Granthub Item',
}
