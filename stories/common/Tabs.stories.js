import React from 'react'

import Tabs from '../../ui/components/common/Tabs'
import Button from '../../ui/components/common/Button'

export const Base = args => {
  return <Tabs {...args} />
}

const sections = [
  {
    key: '1',
    label: 'Settings',
    content: <div>Settings Tab content</div>,
  },
  {
    key: '2',
    label: 'Metadata',
    content: <div>Metadata Tab content</div>,
  },
]

Base.args = {
  activeKey: '1',
  tabItems: sections,
}

export const TabsDynamic = () => {
  const dynamicSections = [
    {
      key: '1',
      label: (
        <>
          Editors
          <Button icon="plus" status="primary">
            Upload Files
          </Button>
        </>
      ),
      content: (
        <div>
          <div>Editors Content</div>
        </div>
      ),
    },
    {
      key: '2',
      label: (
        <>
          Test tab
          <Button icon="plus" status="primary">
            Test
          </Button>
        </>
      ),
      content: (
        <div>
          <div>Test content</div>
        </div>
      ),
    },
  ]

  return <Tabs activeKey="1" outlined tabItems={dynamicSections} />
}

export const TabsRightComponent = () => {
  return (
    <Tabs
      activeKey="1"
      rightComponent={
        <Button outlined status="primary">
          Preview
        </Button>
      }
      tabItems={sections}
    />
  )
}

export default { component: Tabs, title: 'common/Tabs' }
