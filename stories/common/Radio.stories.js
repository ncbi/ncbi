import React from 'react'
import { lorem } from 'faker'
import { range, camelCase } from 'lodash'

import Radio from '../../ui/components/common/Radio'

const options = range(3).map(() => {
  const text = lorem.words(5)

  return {
    label: text,
    value: camelCase(text),
  }
})

export const Base = () => (
  <Radio
    description={lorem.sentences(3)}
    label={lorem.words(3)}
    options={options}
    required
  />
)

export default {
  component: Radio,
  title: 'Common/Radio',
}
