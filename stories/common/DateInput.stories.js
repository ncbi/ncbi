import React, { useState } from 'react'

import DateInput from '../../ui/components/common/DateInput'

export const Base = args => {
  const [isChecked, setIsChecked] = useState(false)

  return (
    <DateInput
      checked={isChecked}
      name="testName"
      onClick={() => {
        setIsChecked(!isChecked)
      }}
      {...args}
    />
  )
}

Base.args = {
  disabled: false,
  label: 'Published date',
}

export default { component: DateInput, title: 'common/DateInput' }
