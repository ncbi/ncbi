import React from 'react'
import { lorem } from 'faker'

import CoverComponent from '../../ui/components/CoverComponent'

export const Base = args => <CoverComponent {...args} label={lorem.words(3)} />

export const WithExistingImage = () => (
  <CoverComponent
    imageName={`${lorem.word()}.png`}
    imageUrl="https://i.picsum.photos/id/486/200/300.jpg?hmac=yDvKMocLz1Sxg1XI9BgCJRlIyKqiBTdI9RZDij_z8xM"
    label={lorem.words(3)}
  />
)

export default {
  component: CoverComponent,
  title: 'Common/CoverComponent',
}
