import React from 'react'
import DocumentIcon from '../../ui/components/common/DocumentIcon'

export const Base = args => <DocumentIcon {...args} />

Base.args = {
  format: 'word',
}

export const Word = () => <DocumentIcon format="word" />
export const Pdf = () => <DocumentIcon format="pdf" />
export const Xml = () => <DocumentIcon format="xml" />
export const Other = () => <DocumentIcon format="jpg" />

export default { component: DocumentIcon, title: 'Common/Document Icon' }
