import React from 'react'
import { lorem } from 'faker'

import DisplayInlineEditor from '../../ui/components/wax/DisplayInlneEditor'

const content = `
  <p>
    ${lorem.sentence()} 
    <strong>${lorem.words(3)}</strong>
    <em>${lorem.words(3)}</em>
    <sup>${lorem.words(3)}</sup>
  </p>`

export const Base = () => <DisplayInlineEditor content={content} />

export default {
  component: DisplayInlineEditor,
  title: 'Common/DisplayInlineEditor',
}
