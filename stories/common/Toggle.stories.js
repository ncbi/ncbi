import React, { useState } from 'react'
import styled from 'styled-components'
import { lorem } from 'faker'

import Toggle from '../../ui/components/common/Toggle'

const FullWidthToggle = styled(Toggle)`
  width: 100%;
`

export const Base = args => {
  const [isChecked, setIsChecked] = useState(false)

  return (
    <Toggle
      checked={isChecked}
      onClick={() => {
        setIsChecked(!isChecked)
      }}
      {...args}
    >
      Publish
    </Toggle>
  )
}

Base.args = {
  disabled: false,
  labelChecked: 'Published',
  label: 'Not Published',
}

export const Locked = () => {
  const [isChecked, setIsChecked] = useState(false)

  return (
    <Toggle
      checked={isChecked}
      label="Not published"
      labelChecked="Is Published"
      locked
      onClick={() => {
        setIsChecked(!isChecked)
      }}
    />
  )
}

export const DisabledChecked = () => {
  return <Toggle checked disabled label="Enable access" labelPosition="left" />
}

export const DisabledNotChecked = () => {
  return (
    <Toggle
      checked={false}
      disabled
      label="Enable access"
      labelPosition="left"
    />
  )
}

export const WithLabelRight = () => {
  const [isChecked, setIsChecked] = useState(false)

  return (
    <Toggle
      checked={isChecked}
      label="Enable access"
      labelPosition="right"
      onClick={() => {
        setIsChecked(!isChecked)
      }}
    />
  )
}

export const FullWidthWithLabelLeft = () => {
  const [isChecked, setIsChecked] = useState(false)

  return (
    <FullWidthToggle
      checked={isChecked}
      label="Enable access"
      labelPosition="left"
      onClick={() => {
        setIsChecked(!isChecked)
      }}
    />
  )
}

export const WithDescription = () => {
  return (
    <Toggle
      checked={false}
      description={lorem.sentences(2)}
      label="Enable access"
      labelPosition="left"
    />
  )
}

export const WithNote = () => {
  return (
    <Toggle
      checked={false}
      label="Enable access"
      labelPosition="left"
      note={lorem.sentences(2)}
    />
  )
}

export default { component: Toggle, title: 'common/Toggle' }
