import React, { useState } from 'react'
// import { lorem } from 'faker'

import Tags from '../../ui/components/common/Tags'

const tagOptions = [
  {
    label: 'Red',
    value: 'red',
    color: 'red',
  },
  {
    label: 'Green',
    value: 'green',
    color: 'green',
  },
  {
    label: 'A long one',
    value: 'alongone',
  },
  {
    label: 'Teal',
    value: 'teal',
    color: 'teal',
  },
]

export const Base = () => {
  const [values, setValues] = useState(tagOptions.slice(0, 2))

  const handleChange = vals => {
    setValues(vals)
  }

  return <Tags onChange={handleChange} options={tagOptions} values={values} />
}

export default {
  component: Tags,
  title: 'Common/Tags',
}
