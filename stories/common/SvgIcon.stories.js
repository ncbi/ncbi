import React from 'react'

import SvgIcon from '../../ui/components/common/SvgIcon'

export const Base = args => <SvgIcon {...args} />

Base.args = {
  name: 'bookWithChapters',
}

export const Book = () => <SvgIcon name="book" />
export const BookWithChapters = () => <SvgIcon name="bookWithChapters" />
export const Collection = () => <SvgIcon name="collection" />

export default {
  component: SvgIcon,
  title: 'Common/SvgIcon',
}
