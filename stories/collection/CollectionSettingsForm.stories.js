/* eslint-disable no-console */

import React, { useRef } from 'react'
// import { lorem } from 'faker'

import CollectionSettingsForm from '../../ui/Pages/Collectionmanager/Form/CollectionSettingsForm'

export const Base = args => (
  <CollectionSettingsForm
    {...args}
    onSubmit={values => console.log(values)}
    showSubmitButton
  />
)

export const TriggerFormFromOutside = () => {
  const ref = useRef()

  const save = () => ref.current.handleSubmit()

  return (
    <>
      <CollectionSettingsForm
        innerRef={ref}
        onSubmit={values => console.log(values)}
        showSubmitButton={false}
      />

      <button onClick={save} type="submit">
        Save from outside form
      </button>
    </>
  )
}

export default {
  component: CollectionSettingsForm,
  title: 'Collection/Settings Form',
}
