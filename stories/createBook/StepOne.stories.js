/* eslint-disable no-console */

import React from 'react'
import { lorem } from 'faker'
import { range, camelCase } from 'lodash'

import StepOne from '../../ui/Pages/CreateBook/StepOne'

const collectionOptions = range(5).map(() => {
  const text = lorem.words(5)
  return {
    label: text,
    value: camelCase(text),
  }
})

export const Base = args => (
  <StepOne
    acceptedWorkflows={['word', 'pdf', 'xml']}
    collectionOptions={collectionOptions}
    onSubmit={values => console.log(values)}
    publisherOptions={[]}
    showFundedContentType
    {...args}
  />
)

export default {
  component: StepOne,
  title: 'Create Book/Step One',
}
