import React, { useState } from 'react'
// import { lorem } from 'faker'

import CreateBook from '../../ui/Pages/CreateBook/CreateBook'

export const Base = args => {
  const [done, setDone] = useState(false)
  const [currentStep, setCurrentStep] = useState(0)
  const [isSaving, setIsSaving] = useState(false)

  const handleCreate = () => {
    setIsSaving(true)

    setTimeout(() => {
      setIsSaving(false)
      setCurrentStep(1)
    }, 3000)
  }

  const handleUpdate = () => {
    setIsSaving(true)

    setTimeout(() => {
      setIsSaving(false)
      setDone(true)
    }, 3000)
  }

  const handleReset = () => {
    setDone(false)
    setCurrentStep(0)
    setIsSaving(false)
  }

  if (done)
    return (
      <>
        <div>Redirect to new book page</div>
        <div>
          <button onClick={handleReset} type="button">
            reset
          </button>
        </div>
      </>
    )

  return (
    <CreateBook
      createBook={handleCreate}
      currentStep={currentStep}
      isSaving={isSaving}
      isSysAdmin
      updateBookSettings={handleUpdate}
      {...args}
    />
  )
}

Base.args = {
  loading: false,
}

export default {
  component: CreateBook,
  title: 'Create Book/Create Book',
}
