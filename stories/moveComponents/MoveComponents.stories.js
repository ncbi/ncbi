/* eslint-disable no-console */

import React, { useState } from 'react'
import { MoveComponents } from '../../ui/components'

const isDisabled = false
const isLoading = false
const autoSorted = false
const bodyDivisionId = 'abc123'
const divisionId = bodyDivisionId
const groupChaptersInParts = true

const selectedBookComponents = [{ id: 1, title: '  Book Title 1' }]

const listOfParts = [
  {
    id: 1,
    title: '  Part title level 1',
    componentType: 'part',
    isDuplicate: false,
    get key() {
      return this.id
    },
    parentId: null,
    parentTitle: null,
    parentIsBody: true,
  },
  {
    id: 2,
    title: '  Part <b> title level,</b>  2',
    componentType: 'part',
    isDuplicate: false,
    get key() {
      return this.id
    },
    parentId: null,
    parentTitle: null,
    parentIsBody: true,
  },
  {
    id: 3,
    title: '  Part  title level 2.1',
    componentType: 'part',
    isDuplicate: false,
    get key() {
      return `${this.parentId}.${this.id}`
    },
    parentId: 2,
    parentTitle: '  Part <b> title level,</b>  2',
    parentIsBody: false,
  },
  {
    id: 4,
    title: '  Part  title level 2.2',
    componentType: 'part',
    isDuplicate: false,
    get key() {
      return `${this.parentId}.${this.id}`
    },
    parentId: 2,
    parentTitle: '  Part <b> title level,</b>  2',
    parentIsBody: false,
  },
  {
    id: 5,
    title: '  Part  title level 2.3',
    componentType: 'part',
    isDuplicate: false,
    get key() {
      return `${this.parentId}.${this.id}`
    },
    parentId: 2,
    parentTitle: '  Part <b> title level,</b>  2',
    parentIsBody: false,
  },
  {
    id: 11,
    title: 'Chapter Title B',
    componentType: 'front-matter',
    isDuplicate: true,
    get key() {
      return this.id
    },
    parentId: null,
    parentTitle: null,
    parentIsBody: true,
  },
  {
    id: 6,
    title: '  Part  title level 3',
    parts: [],
    componentType: 'part',
    isDuplicate: false,
    get key() {
      return this.id
    },
    parentId: null,
    parentTitle: null,
    parentIsBody: true,
  },
  {
    id: 7,
    title: '  Part title level  4',
    componentType: 'part',
    isDuplicate: false,
    get key() {
      return this.id
    },
    parentId: null,
    parentTitle: null,
    parentIsBody: true,
  },
  {
    id: 8,
    title: '  Part title level  4.1',
    componentType: 'part',
    get key() {
      return `${this.parentId}.${this.id}`
    },
    parentId: 7,
    parentTitle: '  Part title level  4',
    parentIsBody: false,
  },
  {
    id: 9,
    title: '  Part title level  4.2',
    componentType: 'part',
    get key() {
      return `${this.parentId}.${this.id}`
    },
    parentId: 7,
    parentTitle: '  Part title level  4',
    parentIsBody: false,
  },
  {
    id: 10,
    title: 'Chapter Title A',
    componentType: 'front-matter',
    isDuplicate: false,
    get key() {
      return this.id
    },
    parentId: null,
    parentTitle: null,
    parentIsBody: true,
  },
  {
    id: 11,
    title: 'Chapter Title B',
    componentType: 'front-matter',
    isDuplicate: true,
    get key() {
      return `${this.parentId}.${this.id}`
    },
    parentId: 6,
    parentTitle: '  Part  title level 3',
    parentIsBody: false,
  },
  {
    id: 12,
    title: 'Chapter Title c',
    componentType: 'front-matter',
    isDuplicate: false,
    get key() {
      return this.id
    },
    parentId: null,
    parentTitle: null,
    parentIsBody: true,
  },
]

export const Base = args => {
  const [searching, setSearching] = useState(false)
  const [components, setComponents] = useState([])

  const submitFn = values => console.log(values)

  const handleSearch = val => {
    console.log('searching', val)
    setSearching(true)

    setComponents(listOfParts)

    setTimeout(() => {
      setSearching(false)
    }, 2000)
  }

  return (
    <MoveComponents
      {...args}
      autoSorted={autoSorted}
      bodyId={bodyDivisionId}
      divisionId={divisionId}
      groupInParts={groupChaptersInParts}
      isDisabled={isDisabled}
      isMoving={isLoading}
      key="move-book-component"
      loadingSearchResults={searching}
      onReorder={submitFn}
      onSearch={handleSearch}
      searchResults={components}
      selectedBookComponents={selectedBookComponents}
    />
  )
}

export default {
  component: MoveComponents,
  title: 'Book Component Footer Actions/Move',
}
