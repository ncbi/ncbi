/* eslint-disable no-console */

import React, { useState } from 'react'
import { RepeatComponent } from '../../ui/components'

const isDisabled = false

const selectedBc = {
  id: 'abc123',
  key: 'abc123',
  title: '  Book Title 1',
  componentType: 'front-matter',
  children: [],
}

const listOfParts = [
  {
    id: '1',
    key: '1',
    title: '  Part title level 1',
    children: [],
    componentType: 'part',
  },
  {
    id: '2',
    key: '2',
    title: '  Part <b> title level,</b>  2',
    children: [
      {
        id: 'abc123',
        key: '2-abc123',
        title: '  Book Title 1',
        componentType: 'front-matter',
        children: [],
      },
      {
        id: '3',
        key: '3',
        title: '  Part  title level 2.1',
        componentType: 'part',
        children: [],
      },
      {
        id: '4',
        key: '4',
        title: '  Part  title level 2.2',
        componentType: 'part',
        children: [],
      },
      {
        id: '5',
        key: '5',
        title: '  Part  title level 2.3',
        componentType: 'part',
        children: [
          {
            id: 'abc123',
            key: '5-abc123',
            title: '  Book Title 1',
            componentType: 'front-matter',
            children: [],
          },
        ],
      },
    ],
    componentType: 'part',
  },
  {
    id: '6',
    key: '6',
    title: '  Part  title level 3',
    children: [],
    componentType: 'part',
  },
  {
    id: '7',
    key: '7',
    title: '  Part title level  4',
    children: [
      { id: '8', key: '8', title: '  Part title level  4.1', children: [] },
      { id: '9', key: '9', title: '  Part title level  4.2', children: [] },
      {
        id: 'abc123',
        key: '7-abc123',
        title: '  Book Title 1',
        componentType: 'front-matter',
        children: [],
      },
    ],
    componentType: 'part',
  },
]

const defaultCheckedParts = ['5', '2', '7']

export const Base = args => {
  const [defaultParts, setDefaultProps] = useState(defaultCheckedParts)

  const successfulSave = ({ sourceId, targetIds }) => {
    setDefaultProps(targetIds)
    console.log(`successfullySaved ${sourceId} -> ${targetIds}`)

    return Promise.resolve()
  }

  return (
    <RepeatComponent
      {...args}
      isDisabled={isDisabled}
      onRepeat={successfulSave}
      partsTree={listOfParts}
      selectedBookComponent={selectedBc}
      selectedPartIdsForRepeatedComponent={defaultParts}
    />
  )
}

export const Failed = args => {
  const failedSave = ({ sourceId, targetIds }) => {
    console.log(`failedSave ${sourceId} -> ${targetIds}`)

    return Promise.reject()
  }

  return (
    <RepeatComponent
      {...args}
      isDisabled={isDisabled}
      listOfParts={listOfParts}
      onRepeat={failedSave}
      selectedBookComponent={selectedBc}
      selectedPartIdsForRepeatedComponent={defaultCheckedParts}
    />
  )
}

export default {
  component: RepeatComponent,
  title: 'Book Component Footer Actions/Repeat',
}
