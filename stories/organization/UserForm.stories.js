import React from 'react'
// import { lorem } from 'faker'

import UserForm from '../../ui/Pages/Dashboard/Form/UserForm'

export const Base = () => (
  <UserForm isFunderOrganization isPublisherOrganization isVerified />
)

export const ReadOnly = () => (
  <UserForm isFunderOrganization isPublisherOrganization isVerified={false} />
)

export default {
  component: UserForm,
  title: 'Organization/User Form',
}
