import React from 'react'
import { lorem, random } from 'faker'
import { v4 as uuid } from 'uuid'
import styled from 'styled-components'

import ChapterRow from '../../ui/Pages/Bookmanager/ChapterRow'

const getRandomValueFromArray = array =>
  array[Math.floor(Math.random() * array.length)]

const ListWrapper = styled.div`
  > a > div {
    border-bottom: 1px solid gray;
    margin: 4px 0;
  }

  > a:nth-child(2) > div {
    margin-left: 20px;
  }

  > a:nth-child(3) > div {
    margin-left: 40px;
  }

  > a:nth-child(4) > div {
    margin-left: 60px;
  }
`

const Row = props => {
  const status = getRandomValueFromArray(['new-upload', 'conversion-errors'])
  const showAssigneeErrors = status === 'conversion-errors'

  return (
    <ChapterRow
      assigneeErrors={['PMC', 'Server']}
      chapterNumber={random.number({ min: 1, max: 50 })}
      filename={`${lorem.words(3)}.docx`}
      // href="/demo"
      hasIssues={random.boolean()}
      hasWarnings={random.boolean()}
      isApproved={random.boolean()}
      isDuplicate={random.boolean()}
      isHiddenInTOC={random.boolean()}
      isMissingFromTOC={random.boolean()}
      isPart={random.boolean()}
      isUnderRevision={random.boolean()}
      lastPublished={new Date()}
      lastUpdated={new Date()}
      showAssigneeErrors={showAssigneeErrors}
      status={status}
      title={lorem.words(10)}
      version={random.number({ min: 1, max: 3 })}
      {...props}
    />
  )
}

export const Base = () => <Row isPart={false} />

export const Part = () => <Row isPart />

export const HTMLTitle = () => (
  <Row isPart={false} title="This title is <sup>up</sup> and <sub>down</sub>" />
)

export const List = () => {
  return (
    <ListWrapper>
      {Array.from(Array(10)).map((_, i) => (
        <Row key={uuid()} />
      ))}
    </ListWrapper>
  )
}

export default {
  component: ChapterRow,
  title: 'Book Manager/ChapterRow',
}
