/* eslint-disable no-console, no-param-reassign, consistent-return */

import React, { useState } from 'react'
import styled from 'styled-components'

import Tree from '../../ui/Pages/Bookmanager/Tree'
import { makeRealisticData } from './_helpers'

const Wrapper = styled.div`
  border: 1px solid gray;
  height: 700px;
  overflow-y: auto;
`

const statusesThatAllowRowMove = [
  'loading-preview',
  'loading-errors',
  'preview',
  'conversion-errors',
  'publish-failed',
  'published',
]

// Loop through items recursively and apply callback
const loop = (data, key, callback) => {
  data.forEach((item, i) => {
    if (data[i].key === key) {
      return callback(data[i], i, data)
    }

    if (data[i].children) {
      loop(data[i].children, key, callback)
    }
  })
}

const updateLevels = (dragObj, dropNodeLevel, dropPosition) => {
  const updated = { ...dragObj }

  if (dropPosition === 0) {
    updated.level = dropNodeLevel + 1
  } else {
    updated.level = dropNodeLevel
  }

  const updateChildrenLevels = obj => {
    if (obj.children) {
      obj.children = obj.children.map(c => {
        if (c.isPart) console.log('before', c)

        const updatedChild = {
          ...c,
          level: obj.level + 1,
        }

        if (c.children) updateChildrenLevels(updatedChild)

        if (c.isPart) console.log('after', updatedChild)

        return updatedChild
      })
    }
  }

  updateChildrenLevels(updated)

  return updated
}

export const Base = () => {
  const [gData, setGData] = useState(
    makeRealisticData(
      null,
      10, // list size
      0, // children
      false, // with parts
    ),
  )

  const onDrop = info => {
    const dragKey = info.dragNode.key
    const dropKey = info.node.key

    const dropPos = info.node.pos.split('-')
    const dropPosition = info.dropPosition - Number(dropPos[dropPos.length - 1])

    const data = [...gData]

    // Find dragObject and remove it from data
    let dragObj

    loop(data, dragKey, (item, index, arr) => {
      arr.splice(index, 1)
      dragObj = item
    })

    dragObj = updateLevels(dragObj, info.node.level, dropPosition)

    if (!info.dropToGap) {
      // Drop on the content
      loop(data, dropKey, item => {
        item.children = item.children || []
        // where to insert. New item was inserted to the start of the array in this example, but can be anywhere
        item.children.unshift(dragObj)
      })
    } else if (
      (info.node.props.children || []).length > 0 &&
      // Has children
      info.node.props.expanded &&
      // Is expanded
      dropPosition === 1 // On the bottom gap
    ) {
      loop(data, dropKey, item => {
        item.children = item.children || []
        // where to insert. New item was inserted to the start of the array in this example, but can be anywhere
        item.children.unshift(dragObj)
        // in previous version, we use item.children.push(dragObj) to insert the
        // item to the tail of the children
      })
    } else {
      let ar = []
      let i

      loop(data, dropKey, (_item, index, arr) => {
        ar = arr
        i = index
      })

      if (dropPosition === -1) {
        ar.splice(i, 0, dragObj)
      } else {
        ar.splice(i + 1, 0, dragObj)
      }
    }

    console.log(data)

    setGData(data)
  }

  // const updateTreeData = (list, key, children) =>
  //   list.map(node => {
  //     if (node.key === key) {
  //       return {
  //         ...node,
  //         children,
  //       }
  //     }

  //     if (node.children) {
  //       return {
  //         ...node,
  //         children: updateTreeData(node.children, key, children),
  //       }
  //     }

  //     return node
  //   })

  // const onLoadData = ({ key, children }) => {
  //   // console.log('on load')
  //   return new Promise(resolve => {
  //     if (children) {
  //       resolve()
  //       return
  //     }

  //     const nestedData = makeRealisticData(key, 2)
  //     console.log('nested calculated')

  //     setTimeout(() => {
  //       setGData(origin => updateTreeData(origin, key, nestedData))
  //       console.log('data set')
  //       resolve()
  //     }, 1000)
  //   })
  // }

  return (
    <Wrapper>
      <Tree
        canDrag
        data={gData}
        maxNestingLevel={3}
        onDrop={onDrop}
        // onLoadNestedData={onLoadData}
        statusesThatAllowRowMove={statusesThatAllowRowMove}
      />
    </Wrapper>
  )
}

export default {
  component: Tree,
  title: 'Book Manager/Tree',
}
