import { lorem, random } from 'faker'
import { v4 as uuid } from 'uuid'

export const getRandomValueFromArray = array =>
  array[Math.floor(Math.random() * array.length)]

export const makeRealisticData = (
  prefix,
  listSize = 5,
  children = 2,
  withParts = true,
) => {
  let data = Array.from(Array(listSize)).map((_, i) => {
    const isPart = withParts ? i % 2 === 0 : false

    const status = getRandomValueFromArray(['new-upload', 'conversion-errors'])
    const showAssigneeErrors = status === 'conversion-errors'
    const id = uuid()

    return {
      assigneeErrors: ['PMC'],
      filename: !isPart ? `${lorem.words(3)}.docx` : null,
      hasIssues: random.boolean(),
      hasWarnings: random.boolean(),
      href: '',
      id,
      isApproved: random.boolean(),
      isDuplicate: random.boolean(),
      isHiddenInTOC: random.boolean(),
      isLeaf: !isPart,
      isMissingFromTOC: random.boolean(),
      isPart,
      isUnderRevision: random.boolean(),
      key: id,
      lastPublished: new Date(),
      lastUpdated: new Date(),
      level: 0,
      parentId: 'root',
      showAssigneeErrors,
      status,
      title: lorem.words(10),
      version: i + 1,
    }
  })

  if (children > 0) {
    data = data.map((d, i) => {
      // console.log(d)
      const kids = d.isPart
        ? makeRealisticData(prefix, children, 0, false).map((kid, j) => {
            return {
              ...kid,
              level: d.level + 1,
              parentId: d.id,
            }
          })
        : []

      const withKids = {
        ...d,
        children: kids,
      }

      return withKids
    })
  }

  return data
}
