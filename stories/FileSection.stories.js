// /* eslint-disable no-console */
// /* eslint-disable react/prop-types */

// import React from 'react'
// import moment from 'moment'

// import FileSection from '../ui/components/FileSection'
// import TableWithToolbar from '../ui/components/TableWithToolbar'
// import Button from '../ui/components/common/Button'

// const columns = [
//   {
//     dataKey: 'name',
//     disableSort: true,
//     label: 'Filename ',
//   },
//   {
//     cellRenderer: () => ({ cellData }) => cellData.replace('image/', ''),
//     dataKey: 'mimeType',
//     disableSort: true,
//     label: 'Type',
//     width: 250,
//   },
//   {
//     cellRenderer: () => ({ cellData }) => <>V{cellData}</>,
//     dataKey: 'versionName',
//     disableSort: true,
//     label: 'File Version',
//     width: 280,
//   },
//   {
//     cellRenderer: () => ({ cellData, rowData }) => (
//       <>
//         {' '}
//         {moment
//           .utc(cellData)
//           .utcOffset(moment().utcOffset())
//           .format('L LT')}{' '}
//       </>
//     ),
//     dataKey: 'created',
//     disableSort: true,
//     label: 'Uploaded ',
//     width: 900,
//   },
// ]

// export const base = () => (
//   <FileSection
//     columns={columns}
//     data={[
//       { name: 'te', created: '23/09/2020', versionName: '1', mimeType: 'pdf' },
//     ]}
//     startExpanded
//     title="Supplementary"
//   />
// )

// export const showVersions = () => (
//   <FileSection
//     columns={columns}
//     data={[
//       { name: 'te', created: '23/09/2020', versionName: '1', mimeType: 'pdf' },
//     ]}
//     showLessFn={() => {
//       return console.log('showMoreFn')
//     }}
//     showMoreFn={() => {
//       return console.log('showMoreFn')
//     }}
//     startExpanded
//     title="Supplementary"
//   />
// )

// export const uploadFile = () => (
//   <FileSection
//     columns={columns}
//     data={[
//       {
//         id: 123,
//         name: 'te',
//         created: '23/09/2020',
//         versionName: '1',
//         mimeType: 'pdf',
//       },
//     ]}
//     headerRightComponent={
//       <Button
//         icon="plus"
//         onClick={e => {
//           e.stopPropagation()
//         }}
//         status="primary"
//       >
//         Upload file version
//       </Button>
//     }
//     showLessFn={() => {
//       return console.log('showMoreFn')
//     }}
//     showMoreFn={() => {
//       return console.log('showMoreFn')
//     }}
//     startExpanded
//     title="Supplementary"
//   />
// )

// export const multiFileSections = () => {
//   return (
//     <TableWithToolbar
//       tools={{
//         right: [],
//         left: [],
//       }}
//     >
//       {({ checkboxColumn, selectedRows, setSelectedRows }) => {
//         return (
//           <>
//             <FileSection
//               columns={[checkboxColumn].concat(columns)}
//               data={[
//                 {
//                   id: 123,
//                   name: 'te',
//                   created: '23/09/2020',
//                   versionName: '1',
//                   mimeType: 'pdf',
//                 },
//               ]}
//               headerRightComponent={
//                 <Button
//                   icon="plus"
//                   onClick={e => {
//                     e.stopPropagation()
//                   }}
//                   status="primary"
//                 >
//                   Upload file
//                 </Button>
//               }
//               selectedRows={selectedRows}
//               setSelectedRows={setSelectedRows}
//               showLessFn={() => {
//                 return console.log('showMoreFn')
//               }}
//               showMoreFn={() => {
//                 return console.log('showMoreFn')
//               }}
//               startExpanded
//               title="Supplementary"
//               uploadingFiles={[
//                 {
//                   name: 'testFile.pdf',
//                   errors: ['All file names must be unique within a package'],
//                 },
//                 {
//                   name: 'testFile.pdf',
//                   errors: ['All file names must be unique within a package'],
//                 },
//                 {
//                   name: 'testFile.pdf',
//                   errors: ['All file names must be unique within a package'],
//                 },
//               ]}
//               validateUploadFile="All files names must contain a valid file type extension"
//             />
//             <FileSection
//               columns={[checkboxColumn].concat(columns)}
//               data={[
//                 {
//                   id: 127,
//                   name: 'te',
//                   created: '23/09/2020',
//                   versionName: '1',
//                   mimeType: 'pdf',
//                 },
//               ]}
//               selectedRows={selectedRows}
//               setSelectedRows={setSelectedRows}
//               startExpanded
//               title="Images"
//             />
//             <FileSection
//               columns={[checkboxColumn].concat(columns)}
//               data={[
//                 {
//                   id: 128,
//                   name: 'te',
//                   created: '23/09/2020',
//                   versionName: '1',
//                   mimeType: 'pdf',
//                 },
//               ]}
//               selectedRows={selectedRows}
//               setSelectedRows={setSelectedRows}
//               startExpanded
//               title="
//               Other files"
//             />
//           </>
//         )
//       }}
//     </TableWithToolbar>
//   )
// }

// export default {
//   component: FileSection,
//   title: 'FileSection',
// }
