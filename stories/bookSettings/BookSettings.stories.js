/* eslint-disable no-console */

import React, { useRef, useState } from 'react'
// import { lorem } from 'faker'

import DomainTemplates from '../../ui/components/BookSettings'

const templateOptions = [
  {
    label: 'Chapter-processed book',
    value: 'chapterProcessed',
  },
  {
    label: 'Whole book',
    value: 'wholeBook',
  },
]

const publisherOptions = [
  {
    label: 'Publisher A',
    value: 'publisherA',
  },
  {
    label: 'Publisher B',
    value: 'publisherB',
  },
]

export const Base = args => (
  <DomainTemplates
    onSubmit={v => console.log(v)}
    publisherOptions={publisherOptions}
    templateOptions={templateOptions}
    {...args}
  />
)

Base.args = {
  // type: 'chapterProcessed',
  type: 'wholeBook',
  // variant: 'template',
  // variant: 'newBook',
  variant: 'bookSettings',
  isSysAdmin: true,
  workflow: 'word',
  // contentType: 'authorManuscript',
  showSubmitButton: true,
  initialFormValues: { conversionWorkflow: 'word' },
}

export const SubmitTriggeredOutsideForm = () => {
  const formRef = useRef()

  const triggerSubmit = () => {
    formRef.current.handleSubmit()
  }

  return (
    <>
      <DomainTemplates
        innerRef={formRef}
        onSubmit={values => console.log(values)}
        publisherOptions={publisherOptions}
        templateOptions={templateOptions}
        type="wholeBook"
        variant="newBook"
      />

      <button onClick={triggerSubmit} type="button">
        Trigger submit from outside the form
      </button>
    </>
  )
}

export const SavingState = () => {
  const [isSaving, setIsSaving] = useState(false)

  const handleSubmit = () => {
    setIsSaving(true)

    setTimeout(() => {
      setIsSaving(false)
    }, 3000)
  }

  return (
    <DomainTemplates
      isSaving={isSaving}
      isSysAdmin
      onSubmit={handleSubmit}
      publisherOptions={publisherOptions}
      showSubmitButton
      submitButtonLabel={isSaving ? 'Saving...' : 'Save'}
      templateOptions={templateOptions}
      type="wholeBook"
      variant="newBook"
      workflow="word"
    />
  )
}

export default {
  component: DomainTemplates,
  title: 'Book Settings/Book Settings',
}
