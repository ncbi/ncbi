import React from 'react'
import styled from 'styled-components'
// import { lorem } from 'faker'

import BookSettingsTemplateChooser from '../../ui/Pages/Dashboard/BookSettingsTemplateChooser'

const Wrapper = styled.div`
  min-height: 200px;
`

export const Base = () => (
  <Wrapper>
    <BookSettingsTemplateChooser
      chapterProcessedTemplate={{}}
      isSysAdmin
      wholeBookTemplate={{}}
    />
  </Wrapper>
)

export default {
  component: BookSettingsTemplateChooser,
  title: 'Book Settings/Template Chooser',
}
