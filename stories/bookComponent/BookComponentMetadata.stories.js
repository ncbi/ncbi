/* eslint-disable no-console */

import React from 'react'
import BookComponentMetadataForm from '../../ui/Pages/Bookmanager/BookComponent/Metadata'

const submitFn = values => console.log(values)

const initialFormValues = {
  title: 'Book component title',
  subTitle: 'sub',
  altTitle: 'alterative Title',
  label: 'on-hold',
  chapterId: 1231,
  objectId: 'asdsad_ASf',
  language: 'en',
  dateOfPublication: { startYear: 1998, endYear: 2019 },
  dateCreated: { startYear: 2001, endYear: '' },
  dateUpdated: { startYear: 2019, endYear: '' },
  dateRevised: { startYear: 2019, endYear: '' },
  authors: [{ givenNames: 'Edi', surname: 'Hoxha' }],
  collaborativeAuthors: [{ givenName: 'Blena Hoxha' }],
  editors: [
    { givenNames: 'Danjela', surname: 'Shehi' },
    { givenNames: 'Arsen', surname: 'Shehi' },
  ],
}

export const Base = () => (
  <BookComponentMetadataForm
    initialFormValues={initialFormValues}
    onSubmit={submitFn}
  />
)

export const EmptyForm = () => <BookComponentMetadataForm onSubmit={submitFn} />

export default {
  component: BookComponentMetadataForm,
  title: 'Book Component/Metadata Form',
}
