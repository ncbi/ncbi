/* eslint-disable no-console */
import React, { useState, useEffect } from 'react'
import faker from 'faker'
import styled from 'styled-components'

import SearchResultList from '../../ui/Pages/Bookmanager/Search/SearchResultList'

const Wrapper = styled.div`
  border: 1px solid gainsboro;
  height: 700px;
`

const range = n => Array.from({ length: n }, (_, i) => i)

const createData = (n, callback) => range(n).map(callback)

const data = n =>
  createData(n, () => ({
    id: faker.random.uuid(),
    divisionId: faker.random.uuid(),
    title: faker.lorem.sentence(),
    versionName: faker.random.number(10).toString(),
    metadata: {
      filename: faker.system.fileName(),
      chapter_number: faker.random.arrayElement([faker.random.number(25), '']),
      hideInTOC: faker.random.arrayElement([true, false]),
    },
    updated: faker.date.past(),
    componentType: faker.random.arrayElement(['part', 'component']),
    divisionName: faker.random.arrayElement([
      'frontMatter',
      'body',
      'backMatter',
    ]),
    status: faker.random.arrayElement([
      'publishing',
      'new-upload',
      'tagging',
      'failed',
    ]),
    publishedDate: faker.random.arrayElement([faker.date.past(), '']),
  }))

const mockSearchCall = (count = 50) => {
  const totalResults = count
  const perPage = 10

  const currentPage = faker.random.number({
    min: 1,
    max: Math.ceil(totalResults / perPage),
  })

  const fromResult = totalResults > 0 ? (currentPage - 1) * perPage + 1 : 0
  const toResult = Math.min(fromResult + perPage - 1, totalResults)

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        data: data(totalResults),
        total: totalResults,
        perPage,
        currentPage,
        from: fromResult,
        to: toResult,
        sortBy: 'updatedAt',
      })
    }, 1000)
  })
}

export const Base = () => {
  const [selectedRows, setSelectedRows] = useState([])

  const [searchResults, setSearchResults] = useState({
    data: [],
    total: 0,
    from: 0,
    to: 0,
    sortBy: 'updatedAt',
  })

  const [loading, setLoading] = useState(true)

  useEffect(() => {
    console.log('Selected rows changed: ', selectedRows)
  }, [selectedRows])

  useEffect(() => {
    const fetchSearchResults = () => {
      mockSearchCall().then(res => {
        setSearchResults(res)
        setLoading(false)
      })
    }

    fetchSearchResults()
  }, [])

  const handleClickResult = result => {
    console.log('result: ', result)
  }

  return (
    <Wrapper>
      <SearchResultList
        endIndex={searchResults.to}
        loading={loading}
        onClickResult={handleClickResult}
        searchResults={searchResults.data}
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        sortOrder={searchResults.sortBy}
        startIndex={searchResults.from}
        totalResults={searchResults.total}
      />
    </Wrapper>
  )
}

export const NoResults = () => {
  const [selectedRows, setSelectedRows] = useState([])

  const [searchResults, setSearchResults] = useState({
    data: [],
    total: 0,
    from: 0,
    to: 0,
    sortBy: 'updatedAt',
  })

  const [loading, setLoading] = useState(true)

  useEffect(() => {
    console.log('Selected rows changed: ', selectedRows)
  }, [selectedRows])

  useEffect(() => {
    const fetchSearchResults = () => {
      mockSearchCall(0).then(res => {
        setSearchResults(res)
        setLoading(false)
      })
    }

    fetchSearchResults()
  }, [])

  const handleClickResult = result => {
    console.log('result: ', result)
  }

  return (
    <Wrapper>
      <SearchResultList
        endIndex={searchResults.to}
        loading={loading}
        onClickResult={handleClickResult}
        searchResults={searchResults.data}
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        sortOrder={searchResults.sortBy}
        startIndex={searchResults.from}
        totalResults={searchResults.total}
      />
    </Wrapper>
  )
}

export default {
  component: SearchResultList,
  title: 'Search/Search Result List',
}
