import React, { useContext } from 'react'

import ConstantsContext from '../app/constantsContext'
import SelectList from '../ui/components/FormElements/SelectList'

export const SelectListSysAdmins = () => {
  const { s } = useContext(ConstantsContext)

  const options = [
    { label: 'Option 1', value: 'new' },
    { label: 'Option 2', value: 'replacement' },
    { label: 'Option 3', value: 'replacement3' },
    { label: 'Option 4', value: 'replacement4' },
  ]

  return (
    <div style={{ width: '50%' }}>
      <SelectList
        description={s('NCBISystemAdminsDescription')}
        label="NCBI System Admins"
        options={options}
        placeholder="Select org"
        secondaryLabel="Organisations"
      />
    </div>
  )
}

export default { component: SelectListSysAdmins, title: 'Common/Select List' }
