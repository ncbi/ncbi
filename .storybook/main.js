const { main } = require('@coko/storybook')

main.stories = ['../stories/**/*.stories.js']

module.exports = main
