import React from 'react'
import { addDecorator } from '@coko/storybook'
import { createGlobalStyle, ThemeProvider } from 'styled-components'
import { MemoryRouter } from 'react-router-dom'
import { th } from '@pubsweet/ui-toolkit'

import theme from '../app/theme'

// export const parameters = {
//   viewMode: 'docs',
// }

const GlobalStyle = createGlobalStyle`
  body {
    background-color: ${th('colorBackground')};
    color: ${th('colorText')};
    font-family: ${th('fontInterface')}, sans-serif;
    font-size: ${th('fontSizeBase')};
    line-height: ${th('lineHeightBase')};

    * {
      box-sizing: border-box;
    }
  }
`

addDecorator(storyFn => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyle />
      {storyFn()}
    </>
  </ThemeProvider>
))

addDecorator(storyFn => <MemoryRouter>{storyFn()}</MemoryRouter>)
